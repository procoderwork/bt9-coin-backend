webpackJsonp(["security.module"],{

/***/ "../../../../../src/app/pages/security/security.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\n    <div class=\"container-fluid\">\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n                <div class=\"card\">\n                    <div class=\"card-header card-title\">\n                        Account security - Security is very important\n                    </div>\n                    <div class=\"card-content\">\n                        <ul class=\"nav nav-pills nav-menu-security nav-pills-warning\">\n                            <li class=\"active\">\n                                <a data-toggle=\"tab\" href=\"#twofa\">Two-factor authentication</a>\n                            </li>\n                            <li>\n                                <a data-toggle=\"tab\" href=\"#login_history\">Login history</a>\n                            </li>\n                            <li>\n                                <a data-toggle=\"tab\" href=\"#login_notification\">Login notification</a>\n                            </li>\n                            <li>\n                                <a data-toggle=\"tab\" href=\"#withdraw_mail\">Withdraw notification</a>\n                            </li>\n                            <li>\n                                <a data-toggle=\"tab\" href=\"#change_password\">Change Password</a>\n                            </li>\n                        </ul>\n                        <div class=\"tab-content\">\n                            <div class=\"tab-pane active\" id=\"twofa\">\n                                <div class=\"col-md-12\">\n                                    <h3 class=\"dib\">Two-factor authentication</h3>\n                                    <span class=\"label label-danger label-sm mr-340\" *ngIf=\"allow_g2f == 0\">Disable</span>\n                                    <span class=\"label label-success label-sm mr-340\" *ngIf=\"allow_g2f == 1\">Enable</span>\n                                    <p class=\"text-black mr-340\">\n                                        Protect your account in just few minutes by enabling following security\n                                        option.<br>\n                                        Required when you send coin from main wallet.\n                                    </p>\n                                    <div class=\"bs-component app-two-factor-div mt10\">\n                                        <div class=\"panel-group accordion\" id=\"accordion\">\n                                            <div class=\"panel\">\n                                                <div>\n                                                    <div class=\"row p10\" style=\"margin-top: 0;\">\n                                                        <div class=\"col-sm-12\">\n                                                            <div *ngIf=\"allow_g2f == 0\">\n                                                                <img class=\"img img-example pull-right\"\n                                                                     src=\"assets/img/2fa_google_ex.jpeg\" *ngIf=\"isMobileMenu()\">\n                                                                <h3>Mobile APP based two-factor authentication</h3>\n                                                                <ol>\n                                                                    <li>\n                                                                        <p>Download &amp; install the \"Google\n                                                                            Authenticator\" app for your mobile\n                                                                            phone: </p>\n                                                                        <p><i class=\"fa fa-android\"></i> <a\n                                                                                href=\"https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2\"\n                                                                                target=\"_blank\">Google Authenticator for\n                                                                            Android (Play Store)</a></p>\n                                                                        <p><i class=\"fa fa-apple\"></i>\n                                                                            <a href=\"https://itunes.apple.com/en/app/google-authenticator/id388497605?mt=8\"\n                                                                               target=\"_blank\">Google Authenticator for\n                                                                                iOS (Apple App Store)</a></p>\n                                                                        <p><i class=\"fa fa-windows\"></i>\n                                                                            <a href=\"http://www.windowsphone.com/en-us/store/app/authenticator/e7994dbc-2336-4950-91ba-ca22d653759b\"\n                                                                               target=\"_blank\">Google Authenticator for\n                                                                                Windows Phone</a></p>\n                                                                    </li>\n                                                                    <li>\n                                                                        Write down this key <strong class=\"auth-key\">{{g2f_code}}</strong>\n                                                                        on the paper and store it safe. You will need it\n                                                                        if you lose or change your device.\n                                                                    </li>\n                                                                    <li>\n                                                                        Open the \"Google Authenticator\" app. <b> Scan QR\n                                                                        barcode </b> revealed below with mobile app.\n                                                                    </li>\n                                                                    <li>\n                                                                        Or enter the Authentication code manual given by\n                                                                        your mobile app in the box.\n                                                                    </li>\n                                                                </ol>\n                                                                <div class=\"enable_app_based_twofactor_area\">\n                                                                    <img *ngIf=\"g2f_code != ''\"\n                                                                         src=\"https://chart.googleapis.com/chart?chs=200x200&chld=M|0&cht=qr&chl=otpauth://totp/{{email}}?secret={{g2f_code}}\"/>\n\n                                                                    <div class=\"form-group is-empty\">\n                                                                        <label class=\"control-label\"\n                                                                               for=\"id_secret_key\">Your Manual\n                                                                            Authentication key </label>\n                                                                        <div class=\"controls \">\n                                                                            <input aria-invalid=\"false\"\n                                                                                   aria-required=\"true\"\n                                                                                   class=\"form-control valid\"\n                                                                                   id=\"id_secret_key\" name=\"secret_key\"\n                                                                                   value=\"{{g2f_code}}\"\n                                                                                   readonly=\"\" type=\"text\">\n                                                                        </div>\n                                                                    </div>\n\n                                                                    <div class=\"form-group\">\n                                                                        <!--<a class=\"cp\">Print a backup of your recovery code.</a><br >-->\n                                                                        <label>\n                                                                            <input aria-invalid=\"false\"\n                                                                                   aria-required=\"true\" class=\"valid\"\n                                                                                   id=\"verify_auth_key\"\n                                                                                   name=\"verify_auth_key\"\n                                                                                   type=\"checkbox\">\n                                                                            <label class=\"error\"\n                                                                                   for=\"verify_auth_key\"\n                                                                                   style=\"display: none;\">\n                                                                                Please verify this </label> I have\n                                                                            written down this backup code <strong\n                                                                                class=\"text-black\">{{g2f_code}}</strong>\n                                                                            on paper\n                                                                        </label>\n                                                                        <small class=\"text-danger\"\n                                                                               style=\"display: none;\" id=\"required\">\n                                                                            Required\n                                                                        </small>\n\n                                                                        <div class=\"response\"></div>\n                                                                        <button class=\"btn btn-primary btn-sm\"\n                                                                                (click)=\"enableG2F()\">\n                                                                            <i class=\"fa fa-check fa-lg progress-icon\"\n                                                                               data-icon=\"fa-check\"></i>\n                                                                            Enable Two-factor Authentication\n                                                                        </button>\n                                                                    </div>\n                                                                </div>\n                                                                <div class=\"dn\" id=\"printContent-qrcode\">\n                                                                    <h2> ETHCONNECT 2FA BACKUP CODE </h2>\n                                                                    <h3> If your phone is lost, stolen, or erased you'll\n                                                                        need to restore your authenticator application\n                                                                        from this backup. Please store it in a safe\n                                                                        place and never share with any one. </h3>\n                                                                    <div id=\"qrCodePrint\">\n                                                                        <img *ngIf=\"g2f_code != ''\"\n                                                                             src=\"https://chart.googleapis.com/chart?chs=200x200&chld=M|0&cht=qr&chl=otpauth://totp/{{email}}?secret={{g2f_code}}\"/>\n                                                                    </div>\n                                                                    <h3> 16-Digit Code (user :\n                                                                        {{settings.getUserSetting('username')}}) :\n                                                                        {{g2f_code}}</h3>\n                                                                    <p> Note: If you disable 2FA, this code will no\n                                                                        longer be valid. </p>\n                                                                </div>\n                                                            </div>\n                                                            <div class=\"form-group\" *ngIf=\"allow_g2f == 1\">\n                                                                <button class=\"btn btn-primary btn-sm\"\n                                                                        (click)=\"disableG2F()\">\n                                                                    <i class=\"fa fa-check fa-lg progress-icon\"\n                                                                       data-icon=\"fa-check\"></i>\n                                                                    Disable Two-factor Authentication\n                                                                </button>\n                                                            </div>\n                                                            <div class=\"well p5-h-small\">\n                                                                <h4>What is Google authenticator?</h4>\n\n                                                                <div class=\"alert alert-info p5-h-small\">\n\n                                                                    <ul>\n                                                                        <li> Google Authenticator protects your\n                                                                            EthConnect account from keyloggers and\n                                                                            password theft. With two-factor\n                                                                            authentication, you'll need both your\n                                                                            password and an authentication code to log\n                                                                            in.\n                                                                        </li>\n                                                                        <li> Once you turn on Google Authentication,\n                                                                            you'll get a Authentication code to your\n                                                                            authenticator app every time you sign in on\n                                                                            a device.\n                                                                        </li>\n                                                                    </ul>\n                                                                </div>\n                                                                <img src=\"assets/img/google-authenticator-featured.png\"\n                                                                     style=\"width:300px\">\n                                                                <h4>Why You Should Use Google Authenticator App?</h4>\n                                                                <ul>\n                                                                    <li>It Works without Internet and Network coverage\n                                                                    </li>\n                                                                    <li>Manage Multiple Accounts in One Place</li>\n                                                                    <li>It takes only few minutes to set it up and EASY\n                                                                        to use\n                                                                    </li>\n                                                                    <li>You can use it without need of Google account\n                                                                    </li>\n                                                                    <li>Protects your EthConnect account from keyloggers\n                                                                        and password theft.\n                                                                    </li>\n                                                                    <li>Adds a second layer of security to prevent\n                                                                        unauthorized login access.\n                                                                    </li>\n                                                                </ul>\n                                                            </div>\n                                                        </div>\n                                                    </div>\n                                                </div>\n                                            </div>\n                                        </div>\n                                    </div>\n                                </div>\n                            </div>\n                            <div class=\"tab-pane\" id=\"login_history\">\n                                <div class=\"material-datatables table-responsive\">\n                                    <table cellspacing=\"0\"\n                                           class=\"table table-striped table-no-bordered table-hover custom-table\"\n                                           id=\"listLoginHistory\" style=\"width:100%\" width=\"100%\">\n                                        <thead>\n                                        <tr>\n                                            <td style=\"width: 150px;\">TIME LOGIN (LOCAL)</td>\n                                            <td>IP</td>\n                                            <td>USER AGENT</td>\n                                            <td>BROWSER/DEVICE</td>\n                                            <td>LOCATION</td>\n                                        </tr>\n                                        </thead>\n                                        <tbody>\n                                        <tr *ngFor=\"let history of loginHistory; let last = last\">\n                                            <td style=\"width: 150px;\">{{history['created_at'] | date: 'yyyy-MM-dd H:m:s'}}</td>\n                                            <td>{{history['ip_address']}}</td>\n                                            <td>{{history['user_agent']}}</td>\n                                            <td>{{history['browser'] + '/' + history['device']}}</td>\n                                            <td>{{history['ip_location']}}</td>\n                                        </tr>\n                                        </tbody>\n                                    </table>\n                                </div>\n\n                            </div>\n                            <div class=\"tab-pane\" id=\"login_notification\">\n                                <div class=\"col-md-12\">\n                                    <h3 class=\"dib\">Login notification</h3> <!----><!---->\n                                    <span class=\"label label-success label-sm\" *ngIf=\"settings.getUserSetting('enable_loginmsg') == 1\">Enable</span>\n                                    <span class=\"label label-warning label-sm\" *ngIf=\"settings.getUserSetting('enable_loginmsg') == 0\">Disable</span>\n                                    <p class=\"text-black\">\n                                        Send a email notification to your email when you login.\n                                    </p>\n                                    <div>\n                                        <button class=\"btn btn-warning btn-sm\" *ngIf=\"settings.getUserSetting('enable_loginmsg') == 1\" (click)=\"changeLoginMsg()\">\n                                            <i class=\"fa fa-check fa-lg progress-icon\" data-icon=\"fa-check\"></i>\n                                            Disable\n                                        </button>\n                                        <button class=\"btn btn-success btn-sm\" *ngIf=\"settings.getUserSetting('enable_loginmsg') == 0\" (click)=\"changeLoginMsg()\">\n                                            <i class=\"fa fa-check fa-lg progress-icon\" data-icon=\"fa-check\"></i>\n                                            Enable\n                                        </button>\n                                    </div>\n                                </div>\n                            </div>\n                            <div class=\"tab-pane\" id=\"withdraw_mail\">\n                                <div class=\"col-md-12\">\n                                    <h3 class=\"dib\">Withdraw notification</h3> <!----><!---->\n                                    <span class=\"label label-success label-sm\" *ngIf=\"settings.getUserSetting('enable_withdrawmail') == 1\">Enable</span>\n                                    <span class=\"label label-warning label-sm\" *ngIf=\"settings.getUserSetting('enable_withdrawmail') == 0\">Disable</span>\n                                    <p class=\"text-black\">\n                                        Send a email notification to your email when you withdraw.\n                                    </p>\n                                    <div>\n                                        <button class=\"btn btn-warning btn-sm\" *ngIf=\"settings.getUserSetting('enable_withdrawmail') == 1\" (click)=\"changeWithdrawMail()\">\n                                            <i class=\"fa fa-check fa-lg progress-icon\" data-icon=\"fa-check\"></i>\n                                            Disable\n                                        </button>\n                                        <button class=\"btn btn-success btn-sm\" *ngIf=\"settings.getUserSetting('enable_withdrawmail') == 0\" (click)=\"changeWithdrawMail()\">\n                                            <i class=\"fa fa-check fa-lg progress-icon\" data-icon=\"fa-check\"></i>\n                                            Enable\n                                        </button>\n                                    </div>\n                                </div>\n                            </div>\n                            <div class=\"tab-pane\" id=\"change_password\">\n                                <div class=\"col-md-4\">\n                                    <form class=\"form-horizontal\" [formGroup]=\"changepwd\">\n                                        <div class=\"input-group\">\n                                            <span class=\"input-group-addon\">\n                                                <i class=\"material-icons\">lock_outline</i>\n                                            </span>\n                                            <div class=\"form-group label-floating\"\n                                                 [ngClass]=\"validate.displayFieldCss(changepwd, 'password')\">\n                                                <label class=\"control-label\"></label>\n                                                <input type=\"password\"\n                                                       name=\"password\"\n                                                       class=\"form-control\"\n                                                       formControlName=\"password\"\n                                                       placeholder=\"New Password...\"\n                                                       [(ngModel)]=\"pwdInfo.password\">\n                                                <app-field-error-display\n                                                        [displayError]=\"validate.isFieldValid(changepwd, 'password')\"\n                                                        errorMsg=\"Enter a valid password.\">\n                                                </app-field-error-display>\n                                            </div>\n                                        </div>\n                                        <div class=\"input-group\">\n                                            <span class=\"input-group-addon\">\n                                                <i class=\"material-icons\">lock_outline</i>\n                                            </span>\n                                            <div class=\"form-group label-floating\"\n                                                 [ngClass]=\"validate.displayFieldCss(changepwd, 'confirmPassword')\">\n                                                <label class=\"control-label\"></label>\n                                                <input\n                                                        type=\"password\"\n                                                        class=\"form-control\"\n                                                        name=\"confirmPassword\"\n                                                        formControlName=\"confirmPassword\"\n                                                        placeholder=\"Retype Password...\">\n                                                <app-field-error-display\n                                                        [displayError]=\"validate.isFieldValid(changepwd, 'confirmPassword')\"\n                                                        errorMsg=\"These passwords don't match. Try again!\">\n                                                </app-field-error-display>\n                                            </div>\n                                        </div>\n\n                                        <div class=\"input-group\">\n                                            <span class=\"input-group-addon\">\n                                                <i class=\"material-icons\">lock_outline</i>\n                                            </span>\n                                            <div class=\"form-group label-floating\"\n                                                 [ngClass]=\"validate.displayFieldCss(changepwd, 'oldPassword')\">\n                                                <label class=\"control-label\"></label>\n                                                <input\n                                                        type=\"password\"\n                                                        class=\"form-control\"\n                                                        name=\"oldPassword\"\n                                                        formControlName=\"oldPassword\"\n                                                        placeholder=\"Current Password...\"\n                                                        [(ngModel)]=\"pwdInfo.oldPassword\">\n                                                <app-field-error-display\n                                                        [displayError]=\"validate.isFieldValid(changepwd, 'oldPassword')\"\n                                                        errorMsg=\"Required\">\n                                                </app-field-error-display>\n                                            </div>\n                                        </div>\n                                        <button class=\"btn btn-fill btn-primary\" type=\"button\" (click)=\"changePwd()\">\n                                            Submit\n                                        </button>\n                                    </form>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n\n<modal-dialog [(visible)]=\"showAuthModal\">\n    <form class=\"form-horizontal\">\n        <h2>\n            INPUT YOUR AUTHENTICATOR CODE\n        </h2>\n        <input type=\"text\" class=\"form-control\" style=\"text-align: center; font-size: 20px;\" name=\"authCode\"\n               [(ngModel)]=\"authCode\">\n\n        <div class=\"swal2-validationerror\" style=\"display: block;\" *ngIf=\"!codeMatch\">The two-factor authentication code\n            you specified is incorrect. Please install Google Authentication and add the two-factor code\n        </div>\n\n        <div class=\"text-center\" style=\"margin-top: 20px;\">\n            <button type=\"button\" class=\"btn btn-primary\" (click)=\"submitCode()\">Submit</button>\n            <button type=\"button\" class=\"btn btn-default\" (click)=\"showAuthModal = !showAuthModal\">Cancel</button>\n        </div>\n    </form>\n</modal-dialog>"

/***/ }),

/***/ "../../../../../src/app/pages/security/security.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".dib {\n  display: inline-block; }\n\n.card img {\n  width: auto;\n  height: auto; }\n\nimg.img-example {\n  margin-top: -140px; }\n\n.mr-340 {\n  margin-right: 340px; }\n\n.dn {\n  display: none; }\n\n.swal2-validationerror {\n  background-color: #f0f0f0;\n  margin: 0 -20px;\n  overflow: hidden;\n  padding: 10px;\n  color: gray;\n  font-size: 16px;\n  font-weight: 300;\n  display: none; }\n\n.swal2-validationerror::before {\n  content: '!';\n  display: inline-block;\n  width: 24px;\n  height: 24px;\n  border-radius: 50%;\n  background-color: #ea7d7d;\n  color: #fff;\n  line-height: 24px;\n  text-align: center;\n  margin-right: 10px; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/pages/security/security.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SecurityComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_settings_settings_service__ = __webpack_require__("../../../../../src/app/services/settings/settings.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_api_service__ = __webpack_require__("../../../../../src/app/services/api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_notifications_service__ = __webpack_require__("../../../../../src/app/services/notifications.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_validate_service__ = __webpack_require__("../../../../../src/app/services/validate.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__shared_components_password_validator_component__ = __webpack_require__("../../../../../src/app/shared/components/password-validator.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var SecurityComponent = (function () {
    function SecurityComponent(formBuilder, settings, api, notify, validate) {
        this.formBuilder = formBuilder;
        this.settings = settings;
        this.api = api;
        this.notify = notify;
        this.validate = validate;
        this.pwdInfo = {};
        this.authCode = '';
        this.codeMatch = true;
        this.showAuthModal = false;
        this.allow_g2f = 0;
        this.g2f_code = '';
        this.email = '';
        this.value = 0;
        this.loginHistory = [];
    }
    SecurityComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.email = this.settings.getUserSetting('email');
        this.allow_g2f = this.settings.getUserSetting('allow_g2f');
        console.log(this.allow_g2f);
        if (this.settings.getUserSetting('g2f_code') == null || this.settings.getUserSetting('g2f_code') == '') {
            this.loadG2FCode();
        }
        else {
            this.g2f_code = this.settings.getUserSetting('g2f_code');
        }
        this.api.getLoginHistory({}).subscribe(function (res) {
            if (res.success) {
                _this.loginHistory = res.data;
                var _parent_1 = _this;
                var timerID = setInterval(function () {
                    if ($('#listLoginHistory tbody tr').length == res.data.length) {
                        clearInterval(timerID);
                        _parent_1.buildLoginHistoryTable();
                    }
                }, 200);
            }
        }, function (err) {
        });
        this.changepwd = this.formBuilder.group({
            // To add a validator, we must first convert the string value into an array. The first item in the array is the default value if any, then the next item in the array is the validator. Here we are adding a required validator meaning that the firstName attribute must have a value in it.
            // We can use more than one validator per field. If we want to use more than one validator we have to wrap our array of validators with a Validators.compose function. Here we are using a required, minimum length and maximum length validator.
            password: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required],
            confirmPassword: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required],
            oldPassword: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required],
        }, {
            validator: __WEBPACK_IMPORTED_MODULE_6__shared_components_password_validator_component__["a" /* PasswordValidation */].MatchPassword // your validation method
        });
    };
    SecurityComponent.prototype.buildLoginHistoryTable = function () {
        $('#listLoginHistory').DataTable({
            'pagingType': 'full_numbers',
            'lengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
            responsive: true,
            language: {
                search: '_INPUT_',
                searchPlaceholder: 'Search records',
            }
        });
    };
    SecurityComponent.prototype.loadG2FCode = function () {
        var _this = this;
        this.api.getG2FCode({}).subscribe(function (res) {
            if (res.success) {
                _this.g2f_code = res.code;
                _this.settings.setUserSetting('g2f_code', res.code);
            }
        }, function (err) {
        });
    };
    SecurityComponent.prototype.enableG2F = function () {
        if ($('#verify_auth_key').prop('checked') == false) {
            $('#required').show();
            return;
        }
        this.value = 1;
        this.showAuthModal = true;
    };
    SecurityComponent.prototype.disableG2F = function () {
        this.value = 0;
        this.showAuthModal = true;
    };
    SecurityComponent.prototype.submitCode = function () {
        var _this = this;
        if (this.authCode == '') {
            return;
        }
        this.codeMatch = true;
        this.api.confirmG2FCode({
            code: this.authCode
        }).subscribe(function (res) {
            if (res.success) {
                _this.api.setG2F({ value: _this.value }).subscribe(function (res) {
                    _this.allow_g2f = _this.value;
                    _this.settings.setUserSetting('allow_g2f', _this.value);
                    if (_this.value == 0) {
                        _this.loadG2FCode();
                    }
                }, function (err) {
                });
                _this.showAuthModal = false;
            }
            else {
                _this.codeMatch = false;
            }
        }, function (err) {
            _this.codeMatch = false;
        });
    };
    SecurityComponent.prototype.changePwd = function () {
        var _this = this;
        if (this.changepwd.valid) {
            this.api.chagePwd(this.pwdInfo).subscribe(function (res) {
                if (res.success) {
                    _this.notify.showNotification('Password has changed', 'top', 'center', 'success');
                }
                else {
                    _this.notify.showNotification(res.error, 'top', 'center', 'warning');
                }
            }, function (err) {
                _this.notify.showNotification('Error', 'top', 'center', 'danger');
            });
        }
        else {
            this.validate.validateAllFormFields(this.changepwd);
        }
    };
    SecurityComponent.prototype.changeLoginMsg = function () {
        var _this = this;
        this.api.changeUserInfo({
            status: (Number(this.settings.getUserSetting('enable_loginmsg')) + 1) % 2
        }).subscribe(function (res) {
            if (res.success) {
                _this.settings.setUserSetting('enable_loginmsg', (Number(_this.settings.getUserSetting('enable_loginmsg')) + 1) % 2);
            }
        });
    };
    SecurityComponent.prototype.changeWithdrawMail = function () {
        var _this = this;
        this.api.changeWithdrawMailStatus({
            status: (Number(this.settings.getUserSetting('enable_withdrawmail')) + 1) % 2
        }).subscribe(function (res) {
            if (res.success) {
                _this.settings.setUserSetting('enable_withdrawmail', (Number(_this.settings.getUserSetting('enable_withdrawmail')) + 1) % 2);
            }
        });
    };
    SecurityComponent.prototype.isMobileMenu = function () {
        if ($(window).width() < 991) {
            return false;
        }
        return true;
    };
    SecurityComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-security',
            template: __webpack_require__("../../../../../src/app/pages/security/security.component.html"),
            styles: [__webpack_require__("../../../../../src/app/pages/security/security.component.scss")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormBuilder"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormBuilder"]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__services_settings_settings_service__["a" /* SettingsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_settings_settings_service__["a" /* SettingsService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__services_api_service__["a" /* Api */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services_api_service__["a" /* Api */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_4__services_notifications_service__["a" /* Notifications */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__services_notifications_service__["a" /* Notifications */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_5__services_validate_service__["a" /* Validate */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__services_validate_service__["a" /* Validate */]) === "function" && _e || Object])
    ], SecurityComponent);
    return SecurityComponent;
    var _a, _b, _c, _d, _e;
}());

//# sourceMappingURL=security.component.js.map

/***/ }),

/***/ "../../../../../src/app/pages/security/security.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SecurityModule", function() { return SecurityModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__md_md_module__ = __webpack_require__("../../../../../src/app/md/md.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__security_component__ = __webpack_require__("../../../../../src/app/pages/security/security.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__security_routing__ = __webpack_require__("../../../../../src/app/pages/security/security.routing.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__shared_shared_module__ = __webpack_require__("../../../../../src/app/shared/shared.module.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var SecurityModule = (function () {
    function SecurityModule() {
    }
    SecurityModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_2__angular_common__["b" /* CommonModule */],
                __WEBPACK_IMPORTED_MODULE_1__angular_router__["d" /* RouterModule */].forChild(__WEBPACK_IMPORTED_MODULE_7__security_routing__["a" /* SecurityRoutes */]),
                __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormsModule"],
                __WEBPACK_IMPORTED_MODULE_4__md_md_module__["a" /* MdModule */],
                __WEBPACK_IMPORTED_MODULE_5__app_module__["b" /* MaterialModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_forms__["ReactiveFormsModule"],
                __WEBPACK_IMPORTED_MODULE_8__shared_shared_module__["a" /* SharedModule */]
            ],
            declarations: [__WEBPACK_IMPORTED_MODULE_6__security_component__["a" /* SecurityComponent */]]
        })
    ], SecurityModule);
    return SecurityModule;
}());

//# sourceMappingURL=security.module.js.map

/***/ }),

/***/ "../../../../../src/app/pages/security/security.routing.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SecurityRoutes; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__security_component__ = __webpack_require__("../../../../../src/app/pages/security/security.component.ts");

var SecurityRoutes = [
    {
        path: '',
        component: __WEBPACK_IMPORTED_MODULE_0__security_component__["a" /* SecurityComponent */]
    }
];
//# sourceMappingURL=security.routing.js.map

/***/ })

});
//# sourceMappingURL=security.module.chunk.js.map