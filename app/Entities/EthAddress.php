<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class EthAddress extends Model
{
    //
    protected $table = 'eth_address';
}
