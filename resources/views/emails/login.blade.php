<!DOCTYPE html>
<html>
<head>
    <title>Login Notification</title>
</head>
<style>
    body {
        font-size: 16px;
    }

</style>
<body>
    Dear <b>{{$user->fullname}}</b>
    <br>
    <br>
    This is to notify you of a successful login to your account.
    <br>
    <br>
    Login Time: {{$loginInfo->created_at}}
    <br>
    <br>
    IP Address: {{$loginInfo->ip_address}}
    <br>
    <br>
    IP Location: {{$loginInfo->ip_location}}
    <br>
    <br>
    User Agent: {{$loginInfo->user_agent}}
    <br>
    <br>
    If you are not the one who logged in or this login is suspicious, please disable your account by support own support. If your account is disabled in this way, it will remain disabled for at least 24 hours after you contact support.
    <br>
    <br>
    To disable further login notifications, go to Security -> Notifications and opt out of login notifications
</body>
</html>