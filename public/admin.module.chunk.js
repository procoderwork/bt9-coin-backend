webpackJsonp(["admin.module"],{

/***/ "../../../../../src/app/admin/admin.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminModule", function() { return AdminModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__admin_routing__ = __webpack_require__("../../../../../src/app/admin/admin.routing.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__dashboard_dashboard_component__ = __webpack_require__("../../../../../src/app/admin/dashboard/dashboard.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__shared_shared_module__ = __webpack_require__("../../../../../src/app/shared/shared.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__adminuser_adminuser_component__ = __webpack_require__("../../../../../src/app/admin/adminuser/adminuser.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__users_users_component__ = __webpack_require__("../../../../../src/app/admin/users/users.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__users_detail_detail_component__ = __webpack_require__("../../../../../src/app/admin/users/detail/detail.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__settings_settings_component__ = __webpack_require__("../../../../../src/app/admin/settings/settings.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__agenda_agenda_component__ = __webpack_require__("../../../../../src/app/admin/agenda/agenda.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__lending_lending_component__ = __webpack_require__("../../../../../src/app/admin/lending/lending.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__withdraw_withdraw_component__ = __webpack_require__("../../../../../src/app/admin/withdraw/withdraw.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__transaction_transaction_component__ = __webpack_require__("../../../../../src/app/admin/transaction/transaction.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__staking_staking_component__ = __webpack_require__("../../../../../src/app/admin/staking/staking.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
















var AdminModule = (function () {
    function AdminModule() {
    }
    AdminModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_common__["b" /* CommonModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_router__["d" /* RouterModule */].forChild(__WEBPACK_IMPORTED_MODULE_3__admin_routing__["a" /* AdminRoutes */]),
                __WEBPACK_IMPORTED_MODULE_5__shared_shared_module__["a" /* SharedModule */],
                __WEBPACK_IMPORTED_MODULE_7__app_module__["b" /* MaterialModule */]
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_4__dashboard_dashboard_component__["a" /* AdminDashboardComponent */],
                __WEBPACK_IMPORTED_MODULE_6__adminuser_adminuser_component__["a" /* AdminUserComponent */],
                __WEBPACK_IMPORTED_MODULE_8__users_users_component__["a" /* UsersComponent */],
                __WEBPACK_IMPORTED_MODULE_9__users_detail_detail_component__["a" /* UserDetailComponent */],
                __WEBPACK_IMPORTED_MODULE_10__settings_settings_component__["a" /* SettingsComponent */],
                __WEBPACK_IMPORTED_MODULE_11__agenda_agenda_component__["a" /* AgendaComponent */],
                __WEBPACK_IMPORTED_MODULE_12__lending_lending_component__["a" /* AdminLendingComponent */],
                __WEBPACK_IMPORTED_MODULE_13__withdraw_withdraw_component__["a" /* AdminWithdrawComponent */],
                __WEBPACK_IMPORTED_MODULE_14__transaction_transaction_component__["a" /* AdminTransactionComponent */],
                __WEBPACK_IMPORTED_MODULE_15__staking_staking_component__["a" /* AdminStakingComponent */]
            ]
        })
    ], AdminModule);
    return AdminModule;
}());

//# sourceMappingURL=admin.module.js.map

/***/ }),

/***/ "../../../../../src/app/admin/admin.routing.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminRoutes; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__dashboard_dashboard_component__ = __webpack_require__("../../../../../src/app/admin/dashboard/dashboard.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__adminuser_adminuser_component__ = __webpack_require__("../../../../../src/app/admin/adminuser/adminuser.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__users_users_component__ = __webpack_require__("../../../../../src/app/admin/users/users.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__users_detail_detail_component__ = __webpack_require__("../../../../../src/app/admin/users/detail/detail.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__settings_settings_component__ = __webpack_require__("../../../../../src/app/admin/settings/settings.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__agenda_agenda_component__ = __webpack_require__("../../../../../src/app/admin/agenda/agenda.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__lending_lending_component__ = __webpack_require__("../../../../../src/app/admin/lending/lending.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__withdraw_withdraw_component__ = __webpack_require__("../../../../../src/app/admin/withdraw/withdraw.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__transaction_transaction_component__ = __webpack_require__("../../../../../src/app/admin/transaction/transaction.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__staking_staking_component__ = __webpack_require__("../../../../../src/app/admin/staking/staking.component.ts");










var AdminRoutes = [
    {
        path: '',
        component: __WEBPACK_IMPORTED_MODULE_0__dashboard_dashboard_component__["a" /* AdminDashboardComponent */]
    },
    {
        path: 'dashboard',
        component: __WEBPACK_IMPORTED_MODULE_0__dashboard_dashboard_component__["a" /* AdminDashboardComponent */]
    },
    {
        path: 'withdraw',
        component: __WEBPACK_IMPORTED_MODULE_7__withdraw_withdraw_component__["a" /* AdminWithdrawComponent */]
    },
    {
        path: 'transaction',
        component: __WEBPACK_IMPORTED_MODULE_8__transaction_transaction_component__["a" /* AdminTransactionComponent */]
    },
    {
        path: 'agenda',
        component: __WEBPACK_IMPORTED_MODULE_5__agenda_agenda_component__["a" /* AgendaComponent */]
    },
    {
        path: 'lending',
        component: __WEBPACK_IMPORTED_MODULE_6__lending_lending_component__["a" /* AdminLendingComponent */]
    },
    {
        path: 'staking',
        component: __WEBPACK_IMPORTED_MODULE_9__staking_staking_component__["a" /* AdminStakingComponent */]
    },
    {
        path: 'adminuser',
        component: __WEBPACK_IMPORTED_MODULE_1__adminuser_adminuser_component__["a" /* AdminUserComponent */]
    },
    {
        path: 'users',
        component: __WEBPACK_IMPORTED_MODULE_2__users_users_component__["a" /* UsersComponent */]
    },
    {
        path: 'users/detail/:userid',
        component: __WEBPACK_IMPORTED_MODULE_3__users_detail_detail_component__["a" /* UserDetailComponent */]
    },
    {
        path: 'settings',
        component: __WEBPACK_IMPORTED_MODULE_4__settings_settings_component__["a" /* SettingsComponent */]
    },
    {
        path: 'support',
        loadChildren: './ticket/ticket.module#AdminTicketModule'
    },
];
//# sourceMappingURL=admin.routing.js.map

/***/ }),

/***/ "../../../../../src/app/admin/adminuser/adminuser.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\n    <div class=\"container-fluid\">\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n                <div class=\"card\">\n                    <div class=\"card-content\">\n                        <h3 class=\"card-title\">\n                            Admin Users\n                        </h3>\n                        <div *ngIf=\"settings.getUserSetting('permission') == 1\">\n                            <button type=\"button\" class=\"btn btn-round btn-success\" (click)=\"onAdd()\"><i\n                                    class=\"material-icons\">add</i></button>\n                        </div>\n                        <div class=\"table-responsive\">\n                            <table class=\"table\">\n                                <thead>\n                                <tr>\n                                    <th>Email</th>\n                                    <th>Full Name</th>\n                                    <th>UserName</th>\n                                    <th>Permission</th>\n                                    <th class=\"text-center\">Actions</th>\n                                </tr>\n                                </thead>\n                                <tbody>\n                                <tr *ngFor=\"let user of adminUsers;\">\n                                    <td>{{user.email}}</td>\n                                    <td>{{user.fullname}}</td>\n                                    <td>{{user.username}}</td>\n                                    <td>{{user.permission == 0 ? 'Read' : 'Read/Write'}}</td>\n                                    <td class=\"td-actions text-center\">\n                                        <div *ngIf=\"settings.getUserSetting('permission') == 1\">\n                                            <button type=\"button\" class=\"btn btn-success btn-round\" *ngIf=\"user.id != 1\"\n                                                    (click)=\"onEdit(user)\">\n                                                <i class=\"material-icons\">edit</i>\n                                            </button>\n                                            <button type=\"button\" class=\"btn btn-danger btn-round\" *ngIf=\"user.id != 1\"\n                                                    (click)=\"onDelete(user)\">\n                                                <i class=\"material-icons\">close</i>\n                                            </button>\n                                        </div>\n                                    </td>\n                                </tr>\n                                </tbody>\n                            </table>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n\n\n<modal-dialog [(visible)]=\"showAddModal\" [closable]=\"false\">\n    <form class=\"form-horizontal\">\n        <h2 class=\"text-left\">\n            Input Admin Information\n        </h2>\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n                <form class=\"form-horizontal\" [formGroup]=\"signup\">\n                    <div class=\"input-group\">\n                            <span class=\"input-group-addon\">\n                                <i class=\"material-icons\">face</i>\n                            </span>\n                        <div class=\"form-group label-floating\"\n                             [ngClass]=\"validate.displayFieldCss(signup, 'fullname')\">\n                            <label class=\"control-label\"></label>\n                            <input type=\"text\" name=\"fullname\" class=\"form-control\" formControlName=\"fullname\"\n                                   [readOnly]=\"user.id > 0\"\n                                   placeholder=\"Full name...\" [(ngModel)]=\"user.fullname\">\n                            <app-field-error-display [displayError]=\"validate.isFieldValid(signup, 'fullname')\"\n                                                     errorMsg=\"Full name is required.\">\n                            </app-field-error-display>\n                        </div>\n                    </div>\n                    <div class=\"input-group\">\n                            <span class=\"input-group-addon\">\n                                <i class=\"material-icons\">account_box</i>\n                            </span>\n                        <div class=\"form-group label-floating\"\n                             [ngClass]=\"validate.displayFieldCss(signup, 'username')\">\n                            <label class=\"control-label\"></label>\n                            <input type=\"text\" name=\"username\" class=\"form-control\" formControlName=\"username\"\n                                   [readOnly]=\"user.id > 0\"\n                                   placeholder=\"Username...\" [(ngModel)]=\"user.username\">\n                            <app-field-error-display [displayError]=\"validate.isFieldValid(signup, 'username')\"\n                                                     errorMsg=\"Username is required and must be with letter and number.\">\n                            </app-field-error-display>\n                        </div>\n                    </div>\n                    <div class=\"input-group\">\n                            <span class=\"input-group-addon\">\n                                <i class=\"material-icons\">email</i>\n                            </span>\n                        <div class=\"form-group label-floating\"\n                             [ngClass]=\"validate.displayFieldCss(signup, 'email')\">\n                            <label class=\"control-label\"></label>\n                            <input type=\"text\" name=\"email\" class=\"form-control\" formControlName=\"email\"\n                                   [readOnly]=\"user.id > 0\"\n                                   placeholder=\"Email...\"\n                                   pattern=\"^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$\" [(ngModel)]=\"user.email\">\n                            <app-field-error-display [displayError]=\"validate.isFieldValid(signup, 'email')\"\n                                                     errorMsg=\"Email is required and format should be john@doe.com.\">\n                            </app-field-error-display>\n                        </div>\n                    </div>\n                    <div class=\"input-group\" *ngIf=\"user.id == 0\">\n                            <span class=\"input-group-addon\">\n                                <i class=\"material-icons\">lock_outline</i>\n                            </span>\n                        <div class=\"form-group label-floating\"\n                             [ngClass]=\"validate.displayFieldCss(signup, 'password')\">\n                            <label class=\"control-label\"></label>\n                            <input type=\"password\"\n                                   name=\"password\"\n                                   class=\"form-control\"\n                                   formControlName=\"password\"\n                                   placeholder=\"Password...\"\n                                   [(ngModel)]=\"user.password\">\n                            <app-field-error-display [displayError]=\"validate.isFieldValid(signup, 'password')\"\n                                                     errorMsg=\"Enter a valid password.\">\n                            </app-field-error-display>\n                        </div>\n                    </div>\n                    <div class=\"input-group\" *ngIf=\"user.id == 0\">\n                            <span class=\"input-group-addon\">\n                                <i class=\"material-icons\">lock_outline</i>\n                            </span>\n                        <div class=\"form-group label-floating\"\n                             [ngClass]=\"validate.displayFieldCss(signup, 'confirmPassword')\">\n                            <label class=\"control-label\"></label>\n                            <input\n                                    type=\"password\"\n                                    class=\"form-control\"\n                                    name=\"confirmPassword\"\n                                    formControlName=\"confirmPassword\"\n                                    placeholder=\"Retype Password...\">\n                            <app-field-error-display\n                                    [displayError]=\"validate.isFieldValid(signup, 'confirmPassword')\"\n                                    errorMsg=\"These passwords don't match. Try again!\">\n                            </app-field-error-display>\n                        </div>\n                    </div>\n                    <div class=\"row\">\n                        <div class=\"col-md-offset-1 col-md-11\">\n                            <mat-form-field>\n                                <mat-select placeholder=\"Permssion\" name=\"permission\" formControlName=\"permission\"\n                                            [(ngModel)]=\"user.permission\">\n                                    <mat-option [value]=\"0\">Read</mat-option>\n                                    <mat-option [value]=\"1\">Read/Write</mat-option>\n                                </mat-select>\n                            </mat-form-field>\n                        </div>\n                    </div>\n                    <div class=\"footer text-center\">\n                        <button class=\"btn btn-info \" (click)=\"addUser()\">SAVE</button>\n                        <button class=\"btn btn-danger \" (click)=\"showAddModal = false\">Close</button>\n                    </div>\n                </form>\n            </div>\n        </div>\n    </form>\n</modal-dialog>"

/***/ }),

/***/ "../../../../../src/app/admin/adminuser/adminuser.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/admin/adminuser/adminuser.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminUserComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_api_service__ = __webpack_require__("../../../../../src/app/services/api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_settings_settings_service__ = __webpack_require__("../../../../../src/app/services/settings/settings.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_validate_service__ = __webpack_require__("../../../../../src/app/services/validate.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__shared_components_password_validator_component__ = __webpack_require__("../../../../../src/app/shared/components/password-validator.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_notifications_service__ = __webpack_require__("../../../../../src/app/services/notifications.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var AdminUserComponent = (function () {
    function AdminUserComponent(formBuilder, api, settings, validate, notify) {
        this.formBuilder = formBuilder;
        this.api = api;
        this.settings = settings;
        this.validate = validate;
        this.notify = notify;
        this.showAddModal = false;
        this.adminUsers = [];
        this.user = {
            permission: 0
        };
    }
    AdminUserComponent.prototype.ngOnInit = function () {
        this.loadAdminUsers();
        this.signup = this.formBuilder.group({
            // To add a validator, we must first convert the string value into an array. The first item in the array is the default value if any, then the next item in the array is the validator. Here we are adding a required validator meaning that the firstName attribute must have a value in it.
            username: [null, [__WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required, __WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].pattern('^[a-zA-Z0-9]+$')]],
            fullname: [null, __WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required],
            email: [null, [__WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required, __WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]],
            // We can use more than one validator per field. If we want to use more than one validator we have to wrap our array of validators with a Validators.compose function. Here we are using a required, minimum length and maximum length validator.
            password: ['', __WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required],
            confirmPassword: ['', __WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required],
            permission: [''],
        }, {
            validator: __WEBPACK_IMPORTED_MODULE_5__shared_components_password_validator_component__["a" /* PasswordValidation */].MatchPassword // your validation method
        });
    };
    AdminUserComponent.prototype.loadAdminUsers = function () {
        var _this = this;
        this.api.getAdminUsers({}).subscribe(function (res) {
            _this.adminUsers = res.data;
        });
    };
    AdminUserComponent.prototype.ngAfterViewInit = function () {
    };
    AdminUserComponent.prototype.onEdit = function (user) {
        this.user = Object.assign({}, user);
        this.showAddModal = true;
    };
    AdminUserComponent.prototype.onDelete = function (user) {
        var _parent = this;
        swal({
            title: 'Are you sure?',
            text: 'This user will be deleted permanently.',
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            confirmButtonText: 'Yes, delete it!',
            buttonsStyling: false
        }).then(function () {
            _parent.api.deleteAdmin({ id: user.id }).subscribe(function (res) {
                if (res.success) {
                    _parent.notify.showNotification('Success', 'top', 'center', 'success');
                    _parent.loadAdminUsers();
                }
                else {
                    _parent.notify.showNotification(res.error, 'top', 'center', 'warning');
                }
            }, function (err) {
                _parent.notify.showNotification('Error', 'top', 'center', 'danger');
            });
        });
    };
    AdminUserComponent.prototype.onAdd = function () {
        this.user = {
            id: 0,
            permission: 0
        };
        this.showAddModal = true;
    };
    AdminUserComponent.prototype.addUser = function () {
        var _this = this;
        if ((this.user.id == 0 && this.signup.valid)
            || (this.user.id > 0)) {
            this.api.addAdmin(this.user).subscribe(function (res) {
                if (res.success) {
                    _this.notify.showNotification('Success', 'top', 'center', 'success');
                    _this.showAddModal = false;
                    _this.loadAdminUsers();
                }
                else {
                    _this.notify.showNotification(res.error, 'top', 'center', 'warning');
                }
            }, function (err) {
                _this.notify.showNotification('Error', 'top', 'center', 'danger');
            });
        }
        else {
            this.validate.validateAllFormFields(this.signup);
        }
    };
    AdminUserComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-admin-dashboard',
            template: __webpack_require__("../../../../../src/app/admin/adminuser/adminuser.component.html"),
            styles: [__webpack_require__("../../../../../src/app/admin/adminuser/adminuser.component.scss")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_4__angular_forms__["FormBuilder"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__angular_forms__["FormBuilder"]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__services_api_service__["a" /* Api */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_api_service__["a" /* Api */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__services_settings_settings_service__["a" /* SettingsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_settings_settings_service__["a" /* SettingsService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_3__services_validate_service__["a" /* Validate */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services_validate_service__["a" /* Validate */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_6__services_notifications_service__["a" /* Notifications */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__services_notifications_service__["a" /* Notifications */]) === "function" && _e || Object])
    ], AdminUserComponent);
    return AdminUserComponent;
    var _a, _b, _c, _d, _e;
}());

//# sourceMappingURL=adminuser.component.js.map

/***/ }),

/***/ "../../../../../src/app/admin/agenda/agenda.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\n    <div class=\"container-fluid\">\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n                <div class=\"card\">\n                    <div class=\"card-header card-header-icon\" data-background-color=\"rose\">\n                        <i class=\"material-icons\">assignment</i>\n                    </div>\n                    <div class=\"card-content\">\n                        <h4 class=\"card-title\">ICO Agenda</h4>\n                        <div *ngIf=\"settings.getUserSetting('permission') == 1\">\n                            <a class=\"btn btn-info btn-round\" (click)=\"onAdd()\"><i class=\"fa fa-plus\"></i></a>\n                        </div>\n                        <div class=\"table-responsive\">\n                            <table class=\"table\">\n                                <thead>\n                                <tr>\n                                    <th class=\"text-center\">Date</th>\n                                    <th class=\"text-center\">Total BT9</th>\n                                    <th class=\"text-center\">Price(USD)</th>\n                                    <th class=\"text-center\">Limit per acc</th>\n                                    <th class=\"text-center\">Status</th>\n                                    <th class=\"text-center\">Action</th>\n                                </tr>\n                                </thead>\n                                <tbody>\n                                <tr *ngFor=\"let agenda of agendas\">\n                                    <td class=\"text-center\">{{agenda.from | date:'yyyy-MM-dd'}} ~ {{agenda.to | date:'yyyy-MM-dd'}}</td>\n                                    <td class=\"text-center\">{{agenda.total | number:'1.0-2'}}</td>\n                                    <td class=\"text-center\">{{agenda.price | number:'1.0-2'}}</td>\n                                    <td class=\"text-center\">{{agenda.per_account | number:'1.0-2'}}</td>\n                                    <td class=\"text-center\">{{agenda.status == 1 ? 'Active' : 'Inactive'}}</td>\n                                    <td class=\"td-actions text-center\">\n                                        <button type=\"button\" class=\"btn btn-success btn-round\" (click)=\"onEdit(agenda)\" *ngIf=\"settings.getUserSetting('permission') == 1\">\n                                            <i class=\"material-icons\">edit</i>\n                                        </button>\n                                        <button type=\"button\" class=\"btn btn-danger btn-round\" (click)=\"onDelete(agenda.id)\" *ngIf=\"settings.getUserSetting('permission') == 1\">\n                                            <i class=\"material-icons\">close</i>\n                                        </button>\n                                    </td>\n                                </tr>\n                                <tr *ngIf=\"agendas?.length == 0\">\n                                    <td class=\"text-center\" colspan=\"4\">No Agenda</td>\n                                </tr>\n                                </tbody>\n                            </table>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n                <div class=\"card\">\n                    <div class=\"card-header card-header-icon\" data-background-color=\"green\">\n                        <i class=\"material-icons\">donut_small</i>\n                    </div>\n                    <div class=\"card-content\">\n                        <h4 class=\"card-title\">ICO History</h4>\n                        <div class=\"table-responsive\">\n                            <table class=\"table datatable\" id=\"historytable\">\n                                <thead>\n                                <tr>\n                                    <th class=\"text-center\">#</th>\n                                    <th class=\"text-center\">User</th>\n                                    <th class=\"text-center\">Date</th>\n                                    <th class=\"text-center\">Amount</th>\n                                    <th class=\"text-center\">Coin</th>\n                                    <th class=\"text-center\">Coin Amount</th>\n                                </tr>\n                                </thead>\n                                <tbody>\n                                <tr *ngFor=\"let history of histories; let i=index;\">\n                                    <td class=\"text-center\">{{i + 1}}</td>\n                                    <td class=\"text-center\">{{history.email}}</td>\n                                    <td class=\"text-center\">{{history.date | date:'yyyy-MM-dd HH:mm:ss'}}</td>\n                                    <td class=\"text-center\">{{history.dest_amount | number:'1.8-8'}}</td>\n                                    <td class=\"text-center\">{{history.src_currency}}</td>\n                                    <td class=\"text-center\">{{history.src_amount | number:'1.8-8'}}</td>\n                                </tr>\n                                <tr *ngIf=\"histories?.length == 0\">\n                                    <td class=\"text-center\" colspan=\"5\">No History</td>\n                                </tr>\n                                </tbody>\n                            </table>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n\n<modal-dialog [(visible)]=\"showModal\" class=\"card\" [closable]=\"false\">\n    <h2 class=\"text-left\">\n        Input Admin Information\n    </h2>\n    <div class=\"row\">\n        <div class=\"col-md-12\">\n            <form class=\"form-horizontal\">\n                <div class=\"row\">\n                    <label class=\"col-md-3 label-on-left\">Date</label>\n                    <div class=\"col-md-9\">\n                        <mat-datepicker-toggle matSuffix [for]=\"fromPicker\">\n                        </mat-datepicker-toggle>\n                        <mat-input-container>\n                            <input matInput\n                                   name=\"from\"\n                                   [(ngModel)]=\"agenda.from\"\n                                   [matDatepicker]=\"fromPicker\">\n                        </mat-input-container>\n                        <mat-datepicker\n                                #fromPicker\n                                [touchUi]=\"touch\"\n                                [startView]=\"'month'\">\n                        </mat-datepicker>\n\n                        <label>~</label>\n\n                        <mat-datepicker-toggle matSuffix [for]=\"toPicker\">\n                        </mat-datepicker-toggle>\n                        <mat-input-container>\n                            <input matInput\n                                   name=\"to\"\n                                   [(ngModel)]=\"agenda.to\"\n                                   [matDatepicker]=\"toPicker\">\n                        </mat-input-container>\n                        <mat-datepicker\n                                #toPicker\n                                [touchUi]=\"touch\"\n                                [startView]=\"'month'\">\n                        </mat-datepicker>\n                    </div>\n                </div>\n                <div class=\"row\">\n                    <label class=\"col-md-3 label-on-left\">Total</label>\n                    <div class=\"col-md-9\">\n                        <div class=\"form-group label-floating is-empty\">\n                            <label class=\"control-label\"></label>\n                            <input type=\"number\" class=\"form-control\" name=\"total\" [(ngModel)]=\"agenda.total\">\n                        </div>\n                    </div>\n                </div>\n                <div class=\"row\">\n                    <label class=\"col-md-3 label-on-left\">Price(USD)</label>\n                    <div class=\"col-md-9\">\n                        <div class=\"form-group label-floating is-empty\">\n                            <label class=\"control-label\"></label>\n                            <input type=\"number\" class=\"form-control\" step=\"0.1\" name=\"price\" [(ngModel)]=\"agenda.price\">\n                        </div>\n                    </div>\n                </div>\n                <div class=\"row\">\n                    <label class=\"col-md-3 label-on-left\">Per Account</label>\n                    <div class=\"col-md-9\">\n                        <div class=\"form-group label-floating is-empty\">\n                            <label class=\"control-label\"></label>\n                            <input type=\"number\" class=\"form-control\" name=\"per_account\" [(ngModel)]=\"agenda.per_account\">\n                        </div>\n                    </div>\n                </div>\n                <div class=\"row\">\n                    <label class=\"col-md-3 label-on-left\">Status</label>\n                    <div class=\"col-md-9\">\n                        <mat-form-field>\n                            <mat-select name=\"status\" [(ngModel)]=\"agenda.status\">\n                                <mat-option [value]=\"0\">Inactive</mat-option>\n                                <mat-option [value]=\"1\">Active</mat-option>\n                            </mat-select>\n                        </mat-form-field>\n                    </div>\n                </div>\n                <div class=\"row\">\n                    <label class=\"col-md-3\"></label>\n                    <div class=\"col-md-9\">\n                        <div class=\"form-group form-button\">\n                            <button type=\"button\" class=\"btn btn-fill btn-info\" (click)=\"saveICOAgenda()\">Save</button>\n                            <button type=\"button\" class=\"btn btn-fill btn-danger\" (click)=\"showModal = false;\">Close</button>\n                        </div>\n                    </div>\n                </div>\n            </form>\n        </div>\n    </div>\n</modal-dialog>"

/***/ }),

/***/ "../../../../../src/app/admin/agenda/agenda.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".font-size-17 {\n  font-size: 17px; }\n\n.datatable tr {\n  height: 50px; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/admin/agenda/agenda.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AgendaComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_api_service__ = __webpack_require__("../../../../../src/app/services/api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_settings_settings_service__ = __webpack_require__("../../../../../src/app/services/settings/settings.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_notifications_service__ = __webpack_require__("../../../../../src/app/services/notifications.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AgendaComponent = (function () {
    function AgendaComponent(api, settings, notify) {
        this.api = api;
        this.settings = settings;
        this.notify = notify;
        this.agendas = [];
        this.agenda = {};
        this.histories = [];
        this.showModal = false;
        this.datatable = null;
    }
    AgendaComponent.prototype.ngOnInit = function () {
        this.loadICOAgenda();
        this.loadICOHistory();
    };
    AgendaComponent.prototype.loadICOAgenda = function () {
        var _this = this;
        this.api.getICOAgenda({}).subscribe(function (res) {
            if (res.success) {
                _this.agendas = res.data;
            }
        });
    };
    AgendaComponent.prototype.loadICOHistory = function () {
        var _this = this;
        if (this.datatable) {
            this.datatable.destroy();
        }
        this.api.getAllICOHistory({}).subscribe(function (res) {
            if (res.success) {
                _this.histories = res.data;
                var _parent_1 = _this;
                var timerID = setInterval(function () {
                    if ($('#historytable tbody tr').length == res.data.length) {
                        clearInterval(timerID);
                        _parent_1.datatable = $('#historytable').DataTable({});
                    }
                }, 200);
            }
        });
    };
    AgendaComponent.prototype.onAdd = function () {
        this.agenda = {
            id: 0
        };
        this.showModal = true;
    };
    AgendaComponent.prototype.onEdit = function (agenda) {
        this.agenda = agenda;
        this.showModal = true;
    };
    AgendaComponent.prototype.onDelete = function (id) {
        var _this = this;
        this.api.deleteICOAgenda({
            id: id
        }).subscribe(function (res) {
            if (res.success) {
                _this.notify.showNotification('Deleted Successfully', 'top', 'center', 'success');
                _this.loadICOAgenda();
            }
            else {
                _this.notify.showNotification('Failed', 'top', 'center', 'warning');
            }
        }, function (err) {
            _this.notify.showNotification('Failed', 'top', 'center', 'warning');
        });
    };
    AgendaComponent.prototype.saveICOAgenda = function () {
        var _this = this;
        if (this.agenda.from.getFullYear) {
            var date = this.agenda.from.getFullYear() + '-' + (this.agenda.from.getMonth() + 1) + '-' + (this.agenda.from.getDate());
            this.agenda.from = date;
        }
        if (this.agenda.to.getFullYear) {
            var date = this.agenda.to.getFullYear() + '-' + (this.agenda.to.getMonth() + 1) + '-' + (this.agenda.to.getDate());
            this.agenda.to = date;
        }
        this.api.saveICOAgenda(this.agenda).subscribe(function (res) {
            if (res.success) {
                _this.notify.showNotification('Saved Succesfully', 'top', 'center', 'success');
                _this.showModal = false;
                _this.loadICOAgenda();
            }
            else {
                _this.notify.showNotification(res.error, 'top', 'center', 'warning');
            }
        }, function (err) {
            _this.notify.showNotification('Failed', 'top', 'center', 'warning');
        });
    };
    AgendaComponent.prototype.ngAfterViewInit = function () {
    };
    AgendaComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-admin-settings',
            template: __webpack_require__("../../../../../src/app/admin/agenda/agenda.component.html"),
            styles: [__webpack_require__("../../../../../src/app/admin/agenda/agenda.component.scss")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_api_service__["a" /* Api */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_api_service__["a" /* Api */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__services_settings_settings_service__["a" /* SettingsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_settings_settings_service__["a" /* SettingsService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__services_notifications_service__["a" /* Notifications */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services_notifications_service__["a" /* Notifications */]) === "function" && _c || Object])
    ], AgendaComponent);
    return AgendaComponent;
    var _a, _b, _c;
}());

//# sourceMappingURL=agenda.component.js.map

/***/ }),

/***/ "../../../../../src/app/admin/dashboard/dashboard.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\n    <div class=\"container-fluid\">\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n                <h3 class=\"text-left\">\n                    User Statistics\n                </h3>\n            </div>\n            <div class=\"col-md-4\">\n                <div class=\"card\">\n                    <div class=\"card-content\">\n                        <div class=\"row\">\n                            <div class=\"col-md-3\">\n                                <button type=\"button\" class=\"btn btn-primary btn-just-icon btn-round\">\n                                    <i class=\"fa fa-users\"></i>\n                                </button>\n                            </div>\n                            <div class=\"col-md-9\">\n                                <h3 class=\"m-b-0 font-light\">{{usersts.total_user}}</h3>\n                                <h5 class=\"text-muted m-b-0\">Total Users</h5>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-md-4\">\n                <div class=\"card\">\n                    <div class=\"card-content\">\n                        <div class=\"row\">\n                            <div class=\"col-md-3\">\n                                <button type=\"button\" class=\"btn btn-success btn-just-icon btn-round\">\n                                    <i class=\"fa fa-users\"></i>\n                                </button>\n                            </div>\n                            <div class=\"col-md-9\">\n                                <h3 class=\"m-b-0 font-light\">{{usersts.confirmed_user}}</h3>\n                                <h5 class=\"text-muted m-b-0\">Confirmed Users</h5>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-md-4\">\n                <div class=\"card\">\n                    <div class=\"card-content\">\n                        <div class=\"row\">\n                            <div class=\"col-md-3\">\n                                <button type=\"button\" class=\"btn btn-danger btn-just-icon btn-round\">\n                                    <i class=\"fa fa-users\"></i>\n                                </button>\n                            </div>\n                            <div class=\"col-md-9\">\n                                <h3 class=\"m-b-0 font-light\">{{usersts.unconfirmed_user}}</h3>\n                                <h5 class=\"text-muted m-b-0\">Unconfirmed Users</h5>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n                <h3 class=\"text-left\">\n                    ICO Statistics\n                </h3>\n            </div>\n            <div class=\"col-md-4\">\n                <div class=\"card\">\n                    <div class=\"card-content\">\n                        <div class=\"row\">\n                            <div class=\"col-md-3\">\n                                <button type=\"button\" class=\"btn btn-danger btn-just-icon btn-round\">\n                                    <i class=\"material-icons\">donut_small</i>\n                                </button>\n                            </div>\n                            <div class=\"col-md-9\">\n                                <h3 class=\"m-b-0 font-light\">{{icosts.total_sold | number:'1.8-8'}} BT9</h3>\n                                <h5 class=\"text-muted m-b-0\">Total ICO Sold</h5>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-md-4\">\n                <div class=\"card\">\n                    <div class=\"card-content\">\n                        <div class=\"row\">\n                            <div class=\"col-md-3\">\n                                <button type=\"button\" class=\"btn btn-primary btn-just-icon btn-round\">\n                                    <i class=\"material-icons\">donut_small</i>\n                                </button>\n                            </div>\n                            <div class=\"col-md-9\">\n                                <h3 class=\"m-b-0 font-light\">{{icosts.btc_purchase | number:'1.8-8'}} BTC</h3>\n                                <h5 class=\"text-muted m-b-0\">BTC Purchase</h5>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-md-4\">\n                <div class=\"card\">\n                    <div class=\"card-content\">\n                        <div class=\"row\">\n                            <div class=\"col-md-3\">\n                                <button type=\"button\" class=\"btn btn-success btn-just-icon btn-round\">\n                                    <i class=\"material-icons\">donut_small</i>\n                                </button>\n                            </div>\n                            <div class=\"col-md-9\">\n                                <h3 class=\"m-b-0 font-light\">{{icosts.eth_purchase | number:'1.8-8'}} ETH</h3>\n                                <h5 class=\"text-muted m-b-0\">ETH Purchase</h5>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n                <h3 class=\"text-left\">\n                    Wallet Statistics\n                </h3>\n            </div>\n            <div class=\"col-md-4\">\n                <div class=\"card\">\n                    <div class=\"card-content\">\n                        <div class=\"row\">\n                            <div class=\"col-md-3\">\n                                <button type=\"button\" class=\"btn btn-info btn-just-icon btn-round\">\n                                    <img src=\"assets/img/B9_color.png\" style=\"width: 30px;\"/>\n                                </button>\n                            </div>\n                            <div class=\"col-md-9\">\n                                <h3 class=\"m-b-0 font-light\">{{wallet.bt9_balance | number:'1.8-8'}} BT9</h3>\n                                <h5 class=\"text-muted m-b-0\">BT9 Balance</h5>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-md-4\">\n                <div class=\"card\">\n                    <div class=\"card-content\">\n                        <div class=\"row\">\n                            <div class=\"col-md-3\">\n                                <button type=\"button\" class=\"btn btn-warning btn-just-icon btn-round\">\n                                    <i class=\"fa fa-bitcoin\"></i>\n                                </button>\n                            </div>\n                            <div class=\"col-md-9\">\n                                <h3 class=\"m-b-0 font-light\">{{wallet.btc_balance  | number:'1.8-8'}} BTC</h3>\n                                <h5 class=\"text-muted m-b-0\">BTC Balance</h5>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-md-4\">\n                <div class=\"card\">\n                    <div class=\"card-content\">\n                        <div class=\"row\">\n                            <div class=\"col-md-3\">\n                                <button type=\"button\" class=\"btn btn-success btn-just-icon btn-round\">\n                                    <img src=\"assets/img/ethereum.png\" style=\"width: 30px;\"/>\n                                </button>\n                            </div>\n                            <div class=\"col-md-9\">\n                                <h3 class=\"m-b-0 font-light\">{{wallet.eth_balance  | number:'1.8-8'}} ETH</h3>\n                                <h5 class=\"text-muted m-b-0\">ETH Balance</h5>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n                <h3 class=\"text-left\">\n                    FEE Statistics\n                </h3>\n            </div>\n            <div class=\"col-md-4\">\n                <div class=\"card\">\n                    <div class=\"card-content\">\n                        <div class=\"row\">\n                            <div class=\"col-md-3\">\n                                <button type=\"button\" class=\"btn btn-warning btn-just-icon btn-round\">\n                                    <img src=\"assets/img/B9_color.png\" style=\"width: 30px;\"/>\n                                </button>\n                            </div>\n                            <div class=\"col-md-9\">\n                                <h3 class=\"m-b-0 font-light\">{{fee.bt9_fee | number:'1.8-8'}} BT9</h3>\n                                <h5 class=\"text-muted m-b-0\">BT9 FEE</h5>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-md-4\">\n                <div class=\"card\">\n                    <div class=\"card-content\">\n                        <div class=\"row\">\n                            <div class=\"col-md-3\">\n                                <button type=\"button\" class=\"btn btn-danger btn-just-icon btn-round\">\n                                    <i class=\"fa fa-bitcoin\"></i>\n                                </button>\n                            </div>\n                            <div class=\"col-md-9\">\n                                <h3 class=\"m-b-0 font-light\">{{fee.btc_fee  | number:'1.8-8'}} BTC</h3>\n                                <h5 class=\"text-muted m-b-0\">BTC FEE</h5>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-md-4\">\n                <div class=\"card\">\n                    <div class=\"card-content\">\n                        <div class=\"row\">\n                            <div class=\"col-md-3\">\n                                <button type=\"button\" class=\"btn btn-info btn-just-icon btn-round\">\n                                    <img src=\"assets/img/ethereum.png\" style=\"width: 30px;\"/>\n                                </button>\n                            </div>\n                            <div class=\"col-md-9\">\n                                <h3 class=\"m-b-0 font-light\">{{fee.eth_fee | number:'1.8-8'}} ETH</h3>\n                                <h5 class=\"text-muted m-b-0\">ETH FEE</h5>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n                <h3 class=\"text-left\">\n                    Lending Statistics\n                </h3>\n            </div>\n            <div class=\"col-md-4\">\n                <div class=\"card\">\n                    <div class=\"card-content\">\n                        <div class=\"row\">\n                            <div class=\"col-md-3\">\n                                <button type=\"button\" class=\"btn btn-success btn-just-icon btn-round\">\n                                    <i class=\"material-icons\">device_hub</i>\n                                </button>\n                            </div>\n                            <div class=\"col-md-9\">\n                                <h3 class=\"m-b-0 font-light\">{{lendingsts.total_amount | number:'1.2-2'}} USD</h3>\n                                <h5 class=\"text-muted m-b-0\">Total Lending</h5>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-md-4\">\n                <div class=\"card\">\n                    <div class=\"card-content\">\n                        <div class=\"row\">\n                            <div class=\"col-md-3\">\n                                <button type=\"button\" class=\"btn btn-info btn-just-icon btn-round\">\n                                    <i class=\"material-icons\">device_hub</i>\n                                </button>\n                            </div>\n                            <div class=\"col-md-9\">\n                                <h3 class=\"m-b-0 font-light\">{{lendingsts.locked_amount | number:'1.2-2'}} USD</h3>\n                                <h5 class=\"text-muted m-b-0\">Locked Lending</h5>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-md-4\">\n                <div class=\"card\">\n                    <div class=\"card-content\">\n                        <div class=\"row\">\n                            <div class=\"col-md-3\">\n                                <button type=\"button\" class=\"btn btn-primary btn-just-icon btn-round\">\n                                    <i class=\"material-icons\">device_hub</i>\n                                </button>\n                            </div>\n                            <div class=\"col-md-9\">\n                                <h3 class=\"m-b-0 font-light\">{{lendingsts.daily_amount | number:'1.2-2'}} USD</h3>\n                                <h5 class=\"text-muted m-b-0\">Total Daily Interest</h5>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n                <h3 class=\"text-left\">\n                    Staking Statistics\n                </h3>\n            </div>\n            <div class=\"col-md-4\">\n                <div class=\"card\">\n                    <div class=\"card-content\">\n                        <div class=\"row\">\n                            <div class=\"col-md-3\">\n                                <button type=\"button\" class=\"btn btn-info btn-just-icon btn-round\">\n                                    <i class=\"material-icons\">play_for_work</i>\n                                </button>\n                            </div>\n                            <div class=\"col-md-9\">\n                                <h3 class=\"m-b-0 font-light\">{{stakingsts.total_amount | number:'1.8-8'}} BT9</h3>\n                                <h5 class=\"text-muted m-b-0\">Total Staking</h5>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-md-4\">\n                <div class=\"card\">\n                    <div class=\"card-content\">\n                        <div class=\"row\">\n                            <div class=\"col-md-3\">\n                                <button type=\"button\" class=\"btn btn-warning btn-just-icon btn-round\">\n                                    <i class=\"material-icons\">play_for_work</i>\n                                </button>\n                            </div>\n                            <div class=\"col-md-9\">\n                                <h3 class=\"m-b-0 font-light\">{{stakingsts.locked_amount | number:'1.8-8'}} BT9</h3>\n                                <h5 class=\"text-muted m-b-0\">Locked Staking</h5>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-md-4\">\n                <div class=\"card\">\n                    <div class=\"card-content\">\n                        <div class=\"row\">\n                            <div class=\"col-md-3\">\n                                <button type=\"button\" class=\"btn btn-success btn-just-icon btn-round\">\n                                    <i class=\"material-icons\">play_for_work</i>\n                                </button>\n                            </div>\n                            <div class=\"col-md-9\">\n                                <h3 class=\"m-b-0 font-light\">{{stakingsts.earned_amount | number:'1.8-8'}} BT9</h3>\n                                <h5 class=\"text-muted m-b-0\">Total Staking Interest</h5>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/admin/dashboard/dashboard.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".card-content button {\n  width: 60px;\n  height: 60px; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/admin/dashboard/dashboard.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminDashboardComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_api_service__ = __webpack_require__("../../../../../src/app/services/api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_settings_settings_service__ = __webpack_require__("../../../../../src/app/services/settings/settings.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_balance_service__ = __webpack_require__("../../../../../src/app/services/balance.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AdminDashboardComponent = (function () {
    function AdminDashboardComponent(api, settings, balance) {
        this.api = api;
        this.settings = settings;
        this.balance = balance;
        this.usersts = {};
        this.icosts = {};
        this.lendingsts = {};
        this.stakingsts = {};
        this.wallet = {};
        this.fee = {};
    }
    AdminDashboardComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.api.getUserStatistics({}).subscribe(function (res) {
            if (res.success) {
                _this.usersts = res.data;
            }
        });
        this.api.getICOStatistics({}).subscribe(function (res) {
            if (res.success) {
                _this.icosts = res.data;
            }
        });
        this.api.getLendingStatistics({}).subscribe(function (res) {
            if (res.success) {
                _this.lendingsts = res.data;
            }
        });
        this.api.getStakingStatistics({}).subscribe(function (res) {
            if (res.success) {
                _this.stakingsts = res.data;
            }
        });
        this.api.getWalletTotalBalance('BTC').subscribe(function (res) {
            if (res.success) {
                _this.wallet.btc_balance = res.balance;
            }
        });
        this.api.getWalletTotalBalance('ETH').subscribe(function (res) {
            if (res.success) {
                _this.wallet.eth_balance = res.balance;
            }
        });
        this.api.getWalletTotalBalance('BT9').subscribe(function (res) {
            if (res.success) {
                _this.wallet.bt9_balance = res.balance;
            }
        });
        this.api.getFeeStatistics({}).subscribe(function (res) {
            if (res.success) {
                _this.fee = res.data;
            }
        });
    };
    AdminDashboardComponent.prototype.ngAfterViewInit = function () {
    };
    AdminDashboardComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-admin-dashboard',
            template: __webpack_require__("../../../../../src/app/admin/dashboard/dashboard.component.html"),
            styles: [__webpack_require__("../../../../../src/app/admin/dashboard/dashboard.component.scss")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_api_service__["a" /* Api */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_api_service__["a" /* Api */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__services_settings_settings_service__["a" /* SettingsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_settings_settings_service__["a" /* SettingsService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__services_balance_service__["a" /* BalanceService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services_balance_service__["a" /* BalanceService */]) === "function" && _c || Object])
    ], AdminDashboardComponent);
    return AdminDashboardComponent;
    var _a, _b, _c;
}());

//# sourceMappingURL=dashboard.component.js.map

/***/ }),

/***/ "../../../../../src/app/admin/lending/lending.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\n    <div class=\"container-fluid\">\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n                <div class=\"card\">\n                    <div class=\"card-header card-header-icon\" data-background-color=\"rose\">\n                        <i class=\"material-icons\">settings</i>\n                    </div>\n                    <div class=\"card-content\">\n                        <h4 class=\"card-title\">Lending Settings</h4>\n                        <form>\n                            <div class=\"row\">\n                                <label class=\"col-md-3\">Lending Start Date</label>\n                                <div class=\"col-md-3\">\n                                    <mat-datepicker-toggle matSuffix [for]=\"picker\">\n                                    </mat-datepicker-toggle>\n                                    <mat-input-container>\n                                        <input matInput\n                                               name=\"to\"\n                                               [(ngModel)]=\"lendingStartDate\"\n                                               [matDatepicker]=\"picker\">\n                                    </mat-input-container>\n                                    <mat-datepicker\n                                            #picker\n                                            [touchUi]=\"touch\"\n                                            [startView]=\"'month'\">\n                                    </mat-datepicker>\n                                </div>\n                                <div class=\"col-md-6\">\n                                    <button type=\"button\" class=\"btn btn-info\" (click)=\"saveStartDate()\">Save</button>\n                                </div>\n                            </div>\n                        </form>\n                        <div *ngIf=\"settings.getUserSetting('permission') == 1\">\n                            <a class=\"btn btn-info btn-round\" (click)=\"onAdd()\"><i class=\"fa fa-plus\"></i></a>\n                        </div>\n                        <div class=\"table-responsive\">\n                            <table class=\"table\">\n                                <thead>\n                                <tr>\n                                    <th class=\"text-center\">Lending Amount</th>\n                                    <th class=\"text-center\">Interest</th>\n                                    <th class=\"text-center\">Volatility daily</th>\n                                    <th class=\"text-center\">Capital Back After</th>\n                                    <th class=\"text-center\">Action</th>\n                                </tr>\n                                </thead>\n                                <tbody>\n                                <tr *ngFor=\"let setting of lendingSettings\">\n                                    <td class=\"text-center\"><i class=\"fa fa-usd\"></i>{{setting.min | number:'1.0-0'}} to <i class=\"fa fa-usd\"></i> {{setting.max | number:'1.0-0'}} </td>\n                                    <td class=\"text-center\">Up to {{setting.interest | number:'1.0-0'}}% per month</td>\n                                    <td class=\"text-center\">+{{setting.daily | number:'1.2-2'}}% daily</td>\n                                    <td class=\"text-center\">{{setting.after | number:'1.0-0'}} days</td>\n                                    <td class=\"td-actions text-center\">\n                                        <button type=\"button\" class=\"btn btn-success btn-round\" (click)=\"onEdit(setting)\" *ngIf=\"settings.getUserSetting('permission') == 1\">\n                                            <i class=\"material-icons\">edit</i>\n                                        </button>\n                                        <button type=\"button\" class=\"btn btn-danger btn-round\" (click)=\"onDelete(setting.id)\" *ngIf=\"settings.getUserSetting('permission') == 1\">\n                                            <i class=\"material-icons\">close</i>\n                                        </button>\n                                    </td>\n                                </tr>\n                                </tbody>\n                            </table>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n                <div class=\"card\">\n                    <div class=\"card-header card-header-icon\" data-background-color=\"rose\">\n                        <i class=\"material-icons\">device_hub</i>\n                    </div>\n                    <div class=\"card-content\">\n                        <h4 class=\"card-title\">Lendings</h4>\n                        <div>\n                            <button type=\"button\" class=\"btn btn-info\" (click)=\"releaseAllLending()\">Release ALL Lending-capital</button>\n                        </div>\n                        <div class=\"table-responsive\">\n                            <table class=\"table\">\n                                <thead>\n                                <tr>\n                                    <th class=\"text-center\">User Email</th>\n                                    <th class=\"text-center\">Lending Amount</th>\n                                    <th class=\"text-center\">BT9Coin Amount</th>\n                                    <th class=\"text-center\">Date</th>\n                                    <th class=\"text-center\">Finish Date</th>\n                                </tr>\n                                </thead>\n                                <tbody>\n                                <tr *ngFor=\"let lending of lendings; let i=index\">\n                                    <td class=\"text-center\">{{lending.email}}</td>\n                                    <td class=\"text-center\">{{lending.dest_amount | number:'1.2-2'}}</td>\n                                    <td class=\"text-center\">{{lending.src_amount | number:'1.8-8'}}</td>\n                                    <td class=\"text-center\">{{lending.date | date:'yyyy-MM-dd'}}</td>\n                                    <td class=\"text-center\">{{lending.finish_date | date:'yyyy-MM-dd'}}</td>\n                                </tr>\n                                <!---->\n                                <tr *ngIf=\"lendings?.length == 0\">\n                                    <td class=\"text-center\" colspan=\"4\">\n                                        No Lending found.\n                                    </td>\n                                </tr>\n                                </tbody>\n                            </table>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n\n<modal-dialog [(visible)]=\"showModal\" class=\"card\" [closable]=\"false\">\n    <h2 class=\"text-left\">\n        Lending Setting\n    </h2>\n    <div class=\"row\">\n        <div class=\"col-md-12\">\n            <form class=\"form-horizontal\">\n                <div class=\"row\">\n                    <label class=\"col-md-3 label-on-left\">Amount</label>\n                    <div class=\"col-md-4\">\n                        <div class=\"form-group label-floating is-empty\">\n                            <input type=\"number\" class=\"form-control\" name=\"min\" [(ngModel)]=\"lendingSetting.min\">\n                        </div>\n                    </div>\n                    <label class=\"col-md-1 text-center label-on-left\" style=\"text-align: center;\">~</label>\n                    <div class=\"col-md-4\">\n                        <div class=\"form-group label-floating is-empty\">\n                            <input type=\"number\" class=\"form-control\" name=\"max\" [(ngModel)]=\"lendingSetting.max\">\n                        </div>\n                    </div>\n                </div>\n                <div class=\"row\">\n                    <label class=\"col-md-3 label-on-left\">Interest</label>\n                    <div class=\"col-md-9\">\n                        <div class=\"form-group label-floating is-empty\">\n                            <label class=\"control-label\"></label>\n                            <input type=\"number\" class=\"form-control\" step=\"1\" name=\"interest\" [(ngModel)]=\"lendingSetting.interest\">\n                        </div>\n                    </div>\n                </div>\n                <div class=\"row\">\n                    <label class=\"col-md-3 label-on-left\">Volatility daily</label>\n                    <div class=\"col-md-9\">\n                        <div class=\"form-group label-floating is-empty\">\n                            <label class=\"control-label\"></label>\n                            <input type=\"number\" class=\"form-control\" step=\"0.01\" name=\"daily\" [(ngModel)]=\"lendingSetting.daily\">\n                        </div>\n                    </div>\n                </div>\n                <div class=\"row\">\n                    <label class=\"col-md-3 label-on-left\">Capital Back After</label>\n                    <div class=\"col-md-9\">\n                        <div class=\"form-group label-floating is-empty\">\n                            <label class=\"control-label\"></label>\n                            <input type=\"number\" class=\"form-control\" step=\"1\" name=\"after\" [(ngModel)]=\"lendingSetting.after\">\n                        </div>\n                    </div>\n                </div>\n                <div class=\"row\">\n                    <label class=\"col-md-3\"></label>\n                    <div class=\"col-md-9\">\n                        <div class=\"form-group form-button\">\n                            <button type=\"button\" class=\"btn btn-fill btn-info\" (click)=\"saveLendingSetting()\">Save</button>\n                            <button type=\"button\" class=\"btn btn-fill btn-danger\" (click)=\"showModal = false;\">Close</button>\n                        </div>\n                    </div>\n                </div>\n            </form>\n        </div>\n    </div>\n</modal-dialog>"

/***/ }),

/***/ "../../../../../src/app/admin/lending/lending.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".font-size-17 {\n  font-size: 17px; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/admin/lending/lending.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminLendingComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_api_service__ = __webpack_require__("../../../../../src/app/services/api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_settings_settings_service__ = __webpack_require__("../../../../../src/app/services/settings/settings.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_notifications_service__ = __webpack_require__("../../../../../src/app/services/notifications.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AdminLendingComponent = (function () {
    function AdminLendingComponent(api, settings, notify) {
        this.api = api;
        this.settings = settings;
        this.notify = notify;
        this.lendings = [];
        this.lendingSettings = [];
        this.lendingSetting = {};
        this.showModal = false;
        this.sysSettings = {};
    }
    AdminLendingComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.loadLendings();
        this.loadLendingSettings();
        this.api.getSettings().subscribe(function (res) {
            _this.lendingStartDate = res.data.lending_start_date ? res.data.lending_start_date : '';
        });
    };
    AdminLendingComponent.prototype.loadLendings = function () {
        var _this = this;
        this.api.getAllLending({}).subscribe(function (res) {
            if (res.success) {
                _this.lendings = res.data;
            }
        });
    };
    AdminLendingComponent.prototype.loadLendingSettings = function () {
        var _this = this;
        this.api.getLendingSetting({}).subscribe(function (res) {
            if (res.success) {
                _this.lendingSettings = res.data;
            }
        });
    };
    AdminLendingComponent.prototype.onAdd = function () {
        this.lendingSetting = {
            id: 0
        };
        this.showModal = true;
    };
    AdminLendingComponent.prototype.onEdit = function (setting) {
        this.lendingSetting = setting;
        this.showModal = true;
    };
    AdminLendingComponent.prototype.onDelete = function (id) {
        var _this = this;
        this.api.deleteLendingSetting({
            id: id
        }).subscribe(function (res) {
            if (res.success) {
                _this.notify.showNotification('Deleted Successfully', 'top', 'center', 'success');
                _this.loadLendingSettings();
            }
            else {
                _this.notify.showNotification('Failed', 'top', 'center', 'warning');
            }
        }, function (err) {
            _this.notify.showNotification('Failed', 'top', 'center', 'warning');
        });
    };
    AdminLendingComponent.prototype.saveLendingSetting = function () {
        var _this = this;
        this.api.saveLendingSetting(this.lendingSetting).subscribe(function (res) {
            if (res.success) {
                _this.notify.showNotification('Saved Succesfully', 'top', 'center', 'success');
                _this.showModal = false;
                _this.loadLendingSettings();
            }
            else {
                _this.notify.showNotification(res.error, 'top', 'center', 'warning');
            }
        }, function (err) {
            _this.notify.showNotification('Failed', 'top', 'center', 'warning');
        });
    };
    AdminLendingComponent.prototype.saveStartDate = function () {
        var _this = this;
        if (this.lendingStartDate == '') {
            return;
        }
        var year = this.lendingStartDate.getFullYear();
        var month = this.lendingStartDate.getMonth() + 1;
        var date = this.lendingStartDate.getDate();
        if (month < 10)
            month = '0' + month;
        if (date < 10)
            date = '0' + date;
        var start_date = year + '-' + month + '-' + date;
        this.api.saveSettings({
            lending_start_date: start_date
        }).subscribe(function (res) {
            if (res.success) {
                _this.notify.showNotification('Success', 'top', 'center', 'success');
            }
            else {
                _this.notify.showNotification(res.error, 'top', 'center', 'warning');
            }
        });
    };
    AdminLendingComponent.prototype.ngAfterViewInit = function () {
    };
    AdminLendingComponent.prototype.releaseAllLending = function () {
        var _parent = this;
        swal({
            title: 'Are you sure?',
            text: 'Release All Lending-Capital?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            confirmButtonText: 'Yes',
            buttonsStyling: false
        }).then(function () {
            _parent.settings.loading = true;
            _parent.api.releaseAllLending({}).subscribe(function (res) {
                _parent.settings.loading = false;
                if (res.success) {
                    _parent.notify.showNotification('Success', 'top', 'center', 'success');
                }
                else {
                    _parent.notify.showNotification(res.error, 'top', 'center', 'warning');
                }
            }, function (err) {
                _parent.settings.loading = false;
            });
        });
    };
    AdminLendingComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-admin-settings',
            template: __webpack_require__("../../../../../src/app/admin/lending/lending.component.html"),
            styles: [__webpack_require__("../../../../../src/app/admin/lending/lending.component.scss")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_api_service__["a" /* Api */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_api_service__["a" /* Api */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__services_settings_settings_service__["a" /* SettingsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_settings_settings_service__["a" /* SettingsService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__services_notifications_service__["a" /* Notifications */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services_notifications_service__["a" /* Notifications */]) === "function" && _c || Object])
    ], AdminLendingComponent);
    return AdminLendingComponent;
    var _a, _b, _c;
}());

//# sourceMappingURL=lending.component.js.map

/***/ }),

/***/ "../../../../../src/app/admin/settings/settings.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\n    <div class=\"container-fluid\">\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n                <div class=\"card\">\n                    <!--<div class=\"card-header\" data-background-color=\"orange\">-->\n                    <!--<i class=\"fa fa-chain\"></i>-->\n                    <!--</div>-->\n                    <div class=\"card-content\">\n                        <div class=\"card-title\">\n                            <h4 class=\"card-title\">System Setting</h4>\n\n                            <form class=\"form-horizontal\">\n                                <div class=\"row\">\n                                    <label class=\"col-md-3 label-on-left\">Blockcypher Token</label>\n                                    <div class=\"col-md-9\">\n                                        <div class=\"form-group label-floating is-empty\">\n                                            <label class=\"control-label\"></label>\n                                            <input type=\"text\" class=\"form-control\" name=\"blockcypher_token\"\n                                                   [(ngModel)]=\"sysSettings.blockcypher_token\">\n                                        </div>\n                                    </div>\n                                </div>\n                                <div class=\"row\">\n                                    <label class=\"col-md-3 label-on-left\">Disconnect Time(min)</label>\n                                    <div class=\"col-md-9\">\n                                        <div class=\"form-group label-floating is-empty\">\n                                            <label class=\"control-label\"></label>\n                                            <input type=\"text\" class=\"form-control\" name=\"disconnect_time\"\n                                                   [(ngModel)]=\"sysSettings.disconnect_time\">\n                                        </div>\n                                    </div>\n                                </div>\n                                <div class=\"row\">\n                                    <div class=\"togglebutton col-md-12\" style=\"margin-top: 20px;\">\n                                        <label class=\"font-size-17\">\n                                            Registration Notification (Disable/Enable)\n                                            <input type=\"checkbox\" name=\"enable_reg_notification\" [(ngModel)]=\"sysSettings.enable_reg_notification\">\n                                        </label>\n                                    </div>\n                                </div>\n                                <div class=\"row\">\n                                    <div class=\"togglebutton col-md-12\" style=\"margin-top: 20px;\">\n                                        <label class=\"font-size-17\">\n                                            ICO (Disable/Enable)\n                                            <input type=\"checkbox\" name=\"enable_staking\" [(ngModel)]=\"sysSettings.enable_ico\">\n                                        </label>\n                                    </div>\n                                </div>\n                                <div class=\"row\">\n                                    <div class=\"togglebutton col-md-12\" style=\"margin-top: 20px;\">\n                                        <label class=\"font-size-17\">\n                                            Lending (Disable/Enable)\n                                            <input type=\"checkbox\" name=\"enable_lending\" [(ngModel)]=\"sysSettings.enable_lending\">\n                                        </label>\n                                    </div>\n                                </div>\n                                <div class=\"row\">\n                                    <div class=\"togglebutton col-md-12\" style=\"margin-top: 20px;\">\n                                        <label class=\"font-size-17\">\n                                            Staking (Disable/Enable)\n                                            <input type=\"checkbox\" name=\"enable_staking\" [(ngModel)]=\"sysSettings.enable_staking\">\n                                        </label>\n                                    </div>\n                                </div>\n                                <div class=\"row\">\n                                    <div class=\"togglebutton col-md-12\" style=\"margin-top: 20px;\">\n                                        <label class=\"font-size-17\">\n                                            Support (Disable/Enable)\n                                            <input type=\"checkbox\" name=\"enable_support\" [(ngModel)]=\"sysSettings.enable_support\">\n                                        </label>\n                                    </div>\n                                </div>\n                                <div class=\"row\">\n                                    <div class=\"col-md-3\"></div>\n                                    <div class=\"col-md-9\" *ngIf=\"settings.getUserSetting('permission') == 1\">\n                                        <div class=\"form-group form-button\">\n                                            <button type=\"button\" class=\"btn btn-info\" (click)=\"saveSystemSetting()\">\n                                                Save\n                                            </button>\n                                        </div>\n                                    </div>\n                                </div>\n                            </form>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n                <div class=\"card\">\n                    <div class=\"card-content\">\n                        <div class=\"card-title\">\n                            <h4 class=\"card-title\">Wallet Setting</h4>\n                        </div>\n                        <form class=\"form-horizontal\">\n                            <div class=\"row\">\n                                <div class=\"col-md-6\">\n                                    <div class=\"row\">\n                                        <div class=\"togglebutton col-md-12\" style=\"margin-top: 20px;\">\n                                            <label class=\"font-size-17\">\n                                                BTC Wallet (Disable/Enable) <input type=\"checkbox\"\n                                                                                   name=\"enable_btc_wallet\"\n                                                                                   [(ngModel)]=\"sysSettings.enable_btc_wallet\">\n                                            </label>\n                                        </div>\n                                    </div>\n                                    <div class=\"row\">\n                                        <div class=\"togglebutton col-md-12\" style=\"margin-top: 20px;\">\n                                            <label class=\"font-size-17\">\n                                                ETH Wallet (Disable/Enable) <input type=\"checkbox\"\n                                                                                   name=\"enable_eth_wallet\"\n                                                                                   [(ngModel)]=\"sysSettings.enable_eth_wallet\">\n                                            </label>\n                                        </div>\n                                    </div>\n                                    <div class=\"row\">\n                                        <div class=\"togglebutton col-md-12\" style=\"margin-top: 20px;\">\n                                            <label class=\"font-size-17\">\n                                                BT9 Wallet (Disable/Enable) <input type=\"checkbox\"\n                                                                                   name=\"enable_bt9_wallet\"\n                                                                                   [(ngModel)]=\"sysSettings.enable_bt9_wallet\">\n                                            </label>\n                                        </div>\n                                    </div>\n                                    <div class=\"row\">\n                                        <div class=\"togglebutton col-md-12\" style=\"margin-top: 20px;\">\n                                            <label class=\"font-size-17\">\n                                                BTC Withdraw (Disable/Enable) <input type=\"checkbox\"\n                                                                                     name=\"enable_btc_withdraw\"\n                                                                                     [(ngModel)]=\"sysSettings.enable_btc_withdraw\">\n                                            </label>\n                                        </div>\n                                    </div>\n                                    <div class=\"row\">\n                                        <div class=\"togglebutton col-md-12\" style=\"margin-top: 20px;\">\n                                            <label class=\"font-size-17\">\n                                                ETH Withdraw (Disable/Enable) <input type=\"checkbox\"\n                                                                                     name=\"enable_eth_withdraw\"\n                                                                                     [(ngModel)]=\"sysSettings.enable_eth_withdraw\">\n                                            </label>\n                                        </div>\n                                    </div>\n                                    <div class=\"row\">\n                                        <div class=\"togglebutton col-md-12\" style=\"margin-top: 20px;\">\n                                            <label class=\"font-size-17\">\n                                                BT9 Withdraw (Disable/Enable) <input type=\"checkbox\"\n                                                                                     name=\"enable_bt9_withdraw\"\n                                                                                     [(ngModel)]=\"sysSettings.enable_bt9_withdraw\">\n                                            </label>\n                                        </div>\n                                    </div>\n                                    <!--<div class=\"row\">-->\n                                        <!--<div class=\"togglebutton col-md-12\" style=\"margin-top: 20px;\">-->\n                                            <!--<label class=\"font-size-17\">-->\n                                                <!--BTC Wallet (Test Mode/Real Mode) <input type=\"checkbox\"-->\n                                                                                        <!--name=\"btc_wallet_mode\"-->\n                                                                                        <!--[(ngModel)]=\"sysSettings.btc_wallet_mode\">-->\n                                            <!--</label>-->\n                                        <!--</div>-->\n                                    <!--</div>-->\n                                </div>\n                                <div class=\"col-md-6\">\n                                    <div class=\"row\">\n                                        <label class=\"col-md-6 label-on-left\">Volume Withdraw per Day for BTC</label>\n                                        <div class=\"col-md-6\">\n                                            <div class=\"form-group label-floating is-empty\">\n                                                <label class=\"control-label\"></label>\n                                                <input type=\"number\" step=\"0.01\" class=\"form-control\"\n                                                       name=\"withdraw_btc_volume_day\"\n                                                       [(ngModel)]=\"sysSettings.withdraw_btc_volume_day\">\n                                            </div>\n                                        </div>\n                                    </div>\n                                    <div class=\"row\">\n                                        <label class=\"col-md-6 label-on-left\">Volume Withdraw per Month for BTC</label>\n                                        <div class=\"col-md-6\">\n                                            <div class=\"form-group label-floating is-empty\">\n                                                <label class=\"control-label\"></label>\n                                                <input type=\"number\" step=\"0.01\" class=\"form-control\"\n                                                       name=\"withdraw_btc_volume_month\"\n                                                       [(ngModel)]=\"sysSettings.withdraw_btc_volume_month\">\n                                            </div>\n                                        </div>\n                                    </div>\n                                    <div class=\"row\">\n                                        <label class=\"col-md-6 label-on-left\">Volume Withdraw per Day for ETH</label>\n                                        <div class=\"col-md-6\">\n                                            <div class=\"form-group label-floating is-empty\">\n                                                <label class=\"control-label\"></label>\n                                                <input type=\"number\" step=\"0.01\" class=\"form-control\"\n                                                       name=\"withdraw_eth_volume_day\"\n                                                       [(ngModel)]=\"sysSettings.withdraw_eth_volume_day\">\n                                            </div>\n                                        </div>\n                                    </div>\n                                    <div class=\"row\">\n                                        <label class=\"col-md-6 label-on-left\">Volume Withdraw per Month for ETH</label>\n                                        <div class=\"col-md-6\">\n                                            <div class=\"form-group label-floating is-empty\">\n                                                <label class=\"control-label\"></label>\n                                                <input type=\"number\" step=\"0.01\" class=\"form-control\"\n                                                       name=\"withdraw_eth_volume_month\"\n                                                       [(ngModel)]=\"sysSettings.withdraw_eth_volume_month\">\n                                            </div>\n                                        </div>\n                                    </div>\n                                    <div class=\"row\">\n                                        <label class=\"col-md-6 label-on-left\">Volume Withdraw per Day for BT9</label>\n                                        <div class=\"col-md-6\">\n                                            <div class=\"form-group label-floating is-empty\">\n                                                <label class=\"control-label\"></label>\n                                                <input type=\"number\" step=\"0.01\" class=\"form-control\"\n                                                       name=\"withdraw_myaltcoin_volume_day\"\n                                                       [(ngModel)]=\"sysSettings.withdraw_myaltcoin_volume_day\">\n                                            </div>\n                                        </div>\n                                    </div>\n                                    <div class=\"row\">\n                                        <label class=\"col-md-6 label-on-left\">Volume Withdraw per Month for BT9</label>\n                                        <div class=\"col-md-6\">\n                                            <div class=\"form-group label-floating is-empty\">\n                                                <label class=\"control-label\"></label>\n                                                <input type=\"number\" step=\"0.01\" class=\"form-control\"\n                                                       name=\"withdraw_myaltcoin_volume_month\"\n                                                       [(ngModel)]=\"sysSettings.withdraw_myaltcoin_volume_month\">\n                                            </div>\n                                        </div>\n                                    </div>\n                                    <div class=\"row\" *ngIf=\"settings.getUserSetting('permission') == 1\">\n                                        <div class=\"col-md-6\"></div>\n                                        <div class=\"col-md-6\">\n                                            <div class=\"form-group form-button\">\n                                                <button type=\"button\" class=\"btn btn-info\"\n                                                        (click)=\"saveWalletSetting()\">Save\n                                                </button>\n                                            </div>\n                                        </div>\n                                    </div>\n                                </div>\n                            </div>\n                        </form>\n                    </div>\n                </div>\n            </div>\n        </div>\n        <div class=\"row\">\n            <div class=\"col-md-3\">\n                <div class=\"card\">\n                    <div class=\"card-content\">\n                        <div class=\"card-title\">\n                            <h4 class=\"card-title\">Bonus For MLM</h4>\n                            <form class=\"form-horizontal\">\n                                <div class=\"row\" *ngFor=\"let bonus of bonusLists; let i= index\">\n                                    <label class=\"col-md-3 label-on-left\">Level {{i+1}}</label>\n                                    <div class=\"col-md-9\">\n                                        <div class=\"form-group label-floating is-empty\">\n                                            <label class=\"control-label\"></label>\n                                            <input type=\"number\" step=\"0.1\" class=\"form-control\" name=\"level{{i+1}}\"\n                                                   [(ngModel)]=\"bonus.bonus\">\n                                        </div>\n                                    </div>\n                                </div>\n                                <div class=\"row\" *ngIf=\"settings.getUserSetting('permission') == 1\">\n                                    <label class=\"col-md-3\"></label>\n                                    <div class=\"col-md-9\">\n                                        <div class=\"form-group form-button\">\n                                            <button type=\"button\" class=\"btn btn-info\" (click)=\"saveBonus()\">Save\n                                            </button>\n                                        </div>\n                                    </div>\n                                </div>\n                            </form>\n                        </div>\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-md-3\">\n                <div class=\"card\">\n                    <div class=\"card-content\">\n                        <div class=\"card-title\">\n                            <h4 class=\"card-title\">Fee Setting</h4>\n                            <form class=\"form-horizontal\">\n                                <div class=\"row\">\n                                    <label class=\"col-md-5 label-on-left\">Withdraw for BTC (%)</label>\n                                    <div class=\"col-md-7\">\n                                        <div class=\"form-group label-floating is-empty\">\n                                            <label class=\"control-label\"></label>\n                                            <input type=\"number\" step=\"0.1\" class=\"form-control\" name=\"withdraw_btc_fee\"\n                                                   [(ngModel)]=\"sysSettings.withdraw_btc_fee\">\n                                        </div>\n                                    </div>\n                                </div>\n                                <div class=\"row\">\n                                    <label class=\"col-md-5 label-on-left\">Withdraw for ETH (%)</label>\n                                    <div class=\"col-md-7\">\n                                        <div class=\"form-group label-floating is-empty\">\n                                            <label class=\"control-label\"></label>\n                                            <input type=\"number\" step=\"0.1\" class=\"form-control\" name=\"withdraw_eth_fee\"\n                                                   [(ngModel)]=\"sysSettings.withdraw_eth_fee\">\n                                        </div>\n                                    </div>\n                                </div>\n                                <div class=\"row\">\n                                    <label class=\"col-md-5 label-on-left\">Withdraw for BT9 (%)</label>\n                                    <div class=\"col-md-7\">\n                                        <div class=\"form-group label-floating is-empty\">\n                                            <label class=\"control-label\"></label>\n                                            <input type=\"number\" step=\"0.1\" class=\"form-control\" name=\"withdraw_bt9_fee\"\n                                                   [(ngModel)]=\"sysSettings.withdraw_bt9_fee\">\n                                        </div>\n                                    </div>\n                                </div>\n                                <div class=\"row\">\n                                    <label class=\"col-md-5 label-on-left\">Exchange Buy (%)</label>\n                                    <div class=\"col-md-7\">\n                                        <div class=\"form-group label-floating is-empty\">\n                                            <label class=\"control-label\"></label>\n                                            <input type=\"number\" step=\"0.1\" class=\"form-control\" name=\"exchange_buy_fee\"\n                                                   [(ngModel)]=\"sysSettings.exchange_buy_fee\">\n                                        </div>\n                                    </div>\n                                </div>\n                                <div class=\"row\">\n                                    <label class=\"col-md-5 label-on-left\">Exchange Sell (%)</label>\n                                    <div class=\"col-md-7\">\n                                        <div class=\"form-group label-floating is-empty\">\n                                            <label class=\"control-label\"></label>\n                                            <input type=\"number\" step=\"0.1\" class=\"form-control\"\n                                                   name=\"exchange_sell_fee\"\n                                                   [(ngModel)]=\"sysSettings.exchange_sell_fee\">\n                                        </div>\n                                    </div>\n                                </div>\n                                <div class=\"row\" *ngIf=\"settings.getUserSetting('permission') == 1\">\n                                    <label class=\"col-md-3\"></label>\n                                    <div class=\"col-md-9\">\n                                        <div class=\"form-group form-button\">\n                                            <button type=\"button\" class=\"btn btn-info\" (click)=\"saveFeeSetting()\">Save\n                                            </button>\n                                        </div>\n                                    </div>\n                                </div>\n                            </form>\n                        </div>\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-md-3\">\n                <div class=\"card\">\n                    <div class=\"card-content\">\n                        <div class=\"card-title\">\n                            <h4 class=\"card-title\">Recaptcha Setting</h4>\n                            <form class=\"form-horizontal\">\n                                <div class=\"togglebutton\" style=\"margin-top: 20px;\">\n                                    <label class=\"font-size-17\">\n                                        Login Recaptcha <input type=\"checkbox\" name=\"login_recaptcha\"\n                                                               [(ngModel)]=\"sysSettings.login_recaptcha\">\n                                    </label>\n                                </div>\n                                <div class=\"togglebutton\" style=\"margin-top: 20px;\">\n                                    <label class=\"font-size-17\">\n                                        Register Recaptcha <input type=\"checkbox\" name=\"register_recaptcha\"\n                                                                  [(ngModel)]=\"sysSettings.register_recaptcha\">\n                                    </label>\n                                </div>\n                                <div class=\"togglebutton\" style=\"margin-top: 20px;\">\n                                    <label class=\"font-size-17\">\n                                        ICO Recaptcha <input type=\"checkbox\" name=\"register_recaptcha\"\n                                                             [(ngModel)]=\"sysSettings.ico_recaptcha\">\n                                    </label>\n                                </div>\n                                <div class=\"row\" *ngIf=\"settings.getUserSetting('permission') == 1\">\n                                    <label class=\"col-md-3\"></label>\n                                    <div class=\"col-md-9\">\n                                        <div class=\"form-group form-button\">\n                                            <button type=\"button\" class=\"btn btn-info\"\n                                                    (click)=\"saveRecaptchaSetting()\">\n                                                Save\n                                            </button>\n                                        </div>\n                                    </div>\n                                </div>\n                            </form>\n                        </div>\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-md-3\">\n                <div class=\"card\">\n                    <div class=\"card-content\">\n                        <div class=\"card-title\">\n                            <h4 class=\"card-title\">Exchange Setting</h4>\n                            <form class=\"form-horizontal\">\n                                <div class=\"togglebutton\" style=\"margin-top: 20px;\">\n                                    <label class=\"font-size-17\">\n                                        BTC Exchange<input type=\"checkbox\" name=\"enable_btc_exchange\"\n                                                               [(ngModel)]=\"sysSettings.enable_btc_exchange\">\n                                    </label>\n                                </div>\n                                <div class=\"togglebutton\" style=\"margin-top: 20px;\">\n                                    <label class=\"font-size-17\">\n                                        ETH Exchange<input type=\"checkbox\" name=\"enable_btc_exchange\"\n                                                                  [(ngModel)]=\"sysSettings.enable_eth_exchange\">\n                                    </label>\n                                </div>\n                                <div class=\"row\" *ngIf=\"settings.getUserSetting('permission') == 1\">\n                                    <label class=\"col-md-3\"></label>\n                                    <div class=\"col-md-9\">\n                                        <div class=\"form-group form-button\">\n                                            <button type=\"button\" class=\"btn btn-info\"\n                                                    (click)=\"saveExchangeSetting()\">\n                                                Save\n                                            </button>\n                                        </div>\n                                    </div>\n                                </div>\n                            </form>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n        <div class=\"row\">\n            <div class=\"col-md-3\">\n                <div class=\"card\">\n                    <div class=\"card-content\">\n                        <div class=\"card-title\">\n                            <h4 class=\"card-title\">Staking Setting</h4>\n                            <form class=\"form-horizontal\">\n                                <div class=\"row\">\n                                    <label class=\"col-md-4 label-on-left\">Locked Days</label>\n                                    <div class=\"col-md-8\">\n                                        <div class=\"form-group label-floating is-empty\">\n                                            <label class=\"control-label\"></label>\n                                            <input type=\"number\" step=\"1\" class=\"form-control\" name=\"staking_locked_days\"\n                                                   [(ngModel)]=\"sysSettings.staking_locked_days\">\n                                        </div>\n                                    </div>\n                                </div>\n\n                                <div class=\"row\">\n                                    <label class=\"col-md-4 label-on-left\">Interest Month (%)</label>\n                                    <div class=\"col-md-8\">\n                                        <div class=\"form-group label-floating is-empty\">\n                                            <label class=\"control-label\"></label>\n                                            <input type=\"number\" step=\"1\" class=\"form-control\" name=\"staking_interest_month\"\n                                                   [(ngModel)]=\"sysSettings.staking_interest_month\">\n                                        </div>\n                                    </div>\n                                </div>\n\n                                <div class=\"row\">\n                                    <label class=\"col-md-4 label-on-left\">Minimum Amount(BT9)</label>\n                                    <div class=\"col-md-8\">\n                                        <div class=\"form-group label-floating is-empty\">\n                                            <label class=\"control-label\"></label>\n                                            <input type=\"number\" step=\"1\" class=\"form-control\" name=\"staking_minimum_amount\"\n                                                   [(ngModel)]=\"sysSettings.staking_minimum_amount\">\n                                        </div>\n                                    </div>\n                                </div>\n                                <div class=\"row\" *ngIf=\"settings.getUserSetting('permission') == 1\">\n                                    <label class=\"col-md-4\"></label>\n                                    <div class=\"col-md-8\">\n                                        <div class=\"form-group form-button\">\n                                            <button type=\"button\" class=\"btn btn-info\"\n                                                    (click)=\"saveStakingSetting()\">\n                                                Save\n                                            </button>\n                                        </div>\n                                    </div>\n                                </div>\n                            </form>\n                        </div>\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-md-3\">\n                <div class=\"card\">\n                    <div class=\"card-content\">\n                        <div class=\"card-title\">\n                            <h4 class=\"card-title\">Lending Setting</h4>\n                            <form class=\"form-horizontal\">\n                                <div class=\"row\">\n                                    <label class=\"col-md-4 label-on-left\">Daily Interest Min(%)</label>\n                                    <div class=\"col-md-8\">\n                                        <div class=\"form-group label-floating is-empty\">\n                                            <label class=\"control-label\"></label>\n                                            <input type=\"number\" step=\"0.01\" class=\"form-control\" name=\"lending_interest_min\"\n                                                   [(ngModel)]=\"sysSettings.lending_interest_min\">\n                                        </div>\n                                    </div>\n                                </div>\n\n                                <div class=\"row\">\n                                    <label class=\"col-md-4 label-on-left\">Daily Interest Max(%)</label>\n                                    <div class=\"col-md-8\">\n                                        <div class=\"form-group label-floating is-empty\">\n                                            <label class=\"control-label\"></label>\n                                            <input type=\"number\" step=\"0.01\" class=\"form-control\" name=\"lending_interest_max\"\n                                                   [(ngModel)]=\"sysSettings.lending_interest_max\">\n                                        </div>\n                                    </div>\n                                </div>\n\n                                <div class=\"row\" *ngIf=\"settings.getUserSetting('permission') == 1\">\n                                    <label class=\"col-md-4\"></label>\n                                    <div class=\"col-md-8\">\n                                        <div class=\"form-group form-button\">\n                                            <button type=\"button\" class=\"btn btn-info\"\n                                                    (click)=\"saveLendingSetting()\">\n                                                Save\n                                            </button>\n                                        </div>\n                                    </div>\n                                </div>\n                            </form>\n                        </div>\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-md-3\">\n                <div class=\"card\">\n                    <div class=\"card-content\">\n                        <div class=\"card-title\">\n                            <h4 class=\"card-title\">BT9 RPC Setting</h4>\n                            <form class=\"form-horizontal\">\n                                <div class=\"row\">\n                                    <label class=\"col-md-4 label-on-left\">RPC Server</label>\n                                    <div class=\"col-md-8\">\n                                        <div class=\"form-group label-floating is-empty\">\n                                            <label class=\"control-label\"></label>\n                                            <input type=\"text\" class=\"form-control\" name=\"bt9_rpcserver\"\n                                                   [(ngModel)]=\"sysSettings.bt9_rpcserver\">\n                                        </div>\n                                    </div>\n                                </div>\n\n                                <div class=\"row\">\n                                    <label class=\"col-md-4 label-on-left\">RPC User</label>\n                                    <div class=\"col-md-8\">\n                                        <div class=\"form-group label-floating is-empty\">\n                                            <label class=\"control-label\"></label>\n                                            <input type=\"text\" class=\"form-control\" name=\"bt9_rpcuser\"\n                                                   [(ngModel)]=\"sysSettings.bt9_rpcuser\">\n                                        </div>\n                                    </div>\n                                </div>\n\n                                <div class=\"row\">\n                                    <label class=\"col-md-4 label-on-left\">RPC Password</label>\n                                    <div class=\"col-md-8\">\n                                        <div class=\"form-group label-floating is-empty\">\n                                            <label class=\"control-label\"></label>\n                                            <input type=\"password\" class=\"form-control\" name=\"bt9_rpcpassword\"\n                                                   [(ngModel)]=\"sysSettings.bt9_rpcpassword\">\n                                        </div>\n                                    </div>\n                                </div>\n\n                                <div class=\"row\">\n                                    <label class=\"col-md-4 label-on-left\">RPC Port</label>\n                                    <div class=\"col-md-8\">\n                                        <div class=\"form-group label-floating is-empty\">\n                                            <label class=\"control-label\"></label>\n                                            <input type=\"text\" class=\"form-control\" name=\"bt9_rpcport\"\n                                                   [(ngModel)]=\"sysSettings.bt9_rpcport\">\n                                        </div>\n                                    </div>\n                                </div>\n\n                                <div class=\"row\" *ngIf=\"settings.getUserSetting('permission') == 1\">\n                                    <label class=\"col-md-4\"></label>\n                                    <div class=\"col-md-8\">\n                                        <div class=\"form-group form-button\">\n                                            <button type=\"button\" class=\"btn btn-info\"\n                                                    (click)=\"saveBT9RPCSetting()\">\n                                                Save\n                                            </button>\n                                        </div>\n                                    </div>\n                                </div>\n                            </form>\n                        </div>\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-md-3\">\n                <div class=\"card\">\n                    <div class=\"card-content\">\n                        <div class=\"card-title\">\n                            <h4 class=\"card-title\">BTC RPC Setting</h4>\n                            <form class=\"form-horizontal\">\n                                <div class=\"row\">\n                                    <label class=\"col-md-4 label-on-left\">RPC Server</label>\n                                    <div class=\"col-md-8\">\n                                        <div class=\"form-group label-floating is-empty\">\n                                            <label class=\"control-label\"></label>\n                                            <input type=\"text\" class=\"form-control\" name=\"bt9_rpcserver\"\n                                                   [(ngModel)]=\"sysSettings.btc_rpcserver\">\n                                        </div>\n                                    </div>\n                                </div>\n\n                                <div class=\"row\">\n                                    <label class=\"col-md-4 label-on-left\">RPC User</label>\n                                    <div class=\"col-md-8\">\n                                        <div class=\"form-group label-floating is-empty\">\n                                            <label class=\"control-label\"></label>\n                                            <input type=\"text\" class=\"form-control\" name=\"bt9_rpcuser\"\n                                                   [(ngModel)]=\"sysSettings.btc_rpcuser\">\n                                        </div>\n                                    </div>\n                                </div>\n\n                                <div class=\"row\">\n                                    <label class=\"col-md-4 label-on-left\">RPC Password</label>\n                                    <div class=\"col-md-8\">\n                                        <div class=\"form-group label-floating is-empty\">\n                                            <label class=\"control-label\"></label>\n                                            <input type=\"password\" class=\"form-control\" name=\"bt9_rpcpassword\"\n                                                   [(ngModel)]=\"sysSettings.btc_rpcpassword\">\n                                        </div>\n                                    </div>\n                                </div>\n\n                                <div class=\"row\">\n                                    <label class=\"col-md-4 label-on-left\">RPC Port</label>\n                                    <div class=\"col-md-8\">\n                                        <div class=\"form-group label-floating is-empty\">\n                                            <label class=\"control-label\"></label>\n                                            <input type=\"text\" class=\"form-control\" name=\"bt9_rpcport\"\n                                                   [(ngModel)]=\"sysSettings.btc_rpcport\">\n                                        </div>\n                                    </div>\n                                </div>\n\n                                <div class=\"row\" *ngIf=\"settings.getUserSetting('permission') == 1\">\n                                    <label class=\"col-md-4\"></label>\n                                    <div class=\"col-md-8\">\n                                        <div class=\"form-group form-button\">\n                                            <button type=\"button\" class=\"btn btn-info\"\n                                                    (click)=\"saveBTCRPCSetting()\">\n                                                Save\n                                            </button>\n                                        </div>\n                                    </div>\n                                </div>\n                            </form>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/admin/settings/settings.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".font-size-17 {\n  font-size: 17px; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/admin/settings/settings.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SettingsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_api_service__ = __webpack_require__("../../../../../src/app/services/api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_settings_settings_service__ = __webpack_require__("../../../../../src/app/services/settings/settings.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_notifications_service__ = __webpack_require__("../../../../../src/app/services/notifications.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SettingsComponent = (function () {
    function SettingsComponent(api, settings, notify) {
        this.api = api;
        this.settings = settings;
        this.notify = notify;
        this.sysSettings = {};
        this.bonusLists = [];
        this.tokenRates = [];
    }
    SettingsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.api.getSettings().subscribe(function (res) {
            _this.sysSettings = res.data;
            _this.sysSettings.login_recaptcha = _this.sysSettings.login_recaptcha == 1 ? true : false;
            _this.sysSettings.register_recaptcha = _this.sysSettings.register_recaptcha == 1 ? true : false;
            _this.sysSettings.ico_recaptcha = _this.sysSettings.ico_recaptcha == 1 ? true : false;
            _this.sysSettings.enable_btc_wallet = _this.sysSettings.enable_btc_wallet == 1 ? true : false;
            _this.sysSettings.enable_eth_wallet = _this.sysSettings.enable_eth_wallet == 1 ? true : false;
            _this.sysSettings.enable_btc_withdraw = _this.sysSettings.enable_btc_withdraw == 1 ? true : false;
            _this.sysSettings.enable_eth_withdraw = _this.sysSettings.enable_eth_withdraw == 1 ? true : false;
            //this.sysSettings.btc_wallet_mode = this.sysSettings.btc_wallet_mode  == 1 ? true : false;
            //this.sysSettings.eth_wallet_mode = this.sysSettings.eth_wallet_mode  == 1 ? true : false;
            _this.sysSettings.enable_reg_notification = _this.sysSettings.enable_reg_notification == 1 ? true : false;
            _this.sysSettings.enable_btc_exchange = _this.sysSettings.enable_btc_exchange == 1 ? true : false;
            _this.sysSettings.enable_eth_exchange = _this.sysSettings.enable_eth_exchange == 1 ? true : false;
            _this.sysSettings.enable_bt9_wallet = _this.sysSettings.enable_bt9_wallet == 1 ? true : false;
            _this.sysSettings.enable_bt9_withdraw = _this.sysSettings.enable_bt9_wallet == 1 ? true : false;
            _this.sysSettings.enable_lending = _this.sysSettings.enable_lending == 1 ? true : false;
            _this.sysSettings.enable_staking = _this.sysSettings.enable_staking == 1 ? true : false;
            _this.sysSettings.enable_ico = _this.sysSettings.enable_ico == 1 ? true : false;
            _this.sysSettings.enable_support = _this.sysSettings.enable_support == 1 ? true : false;
        });
        this.api.getBonusList({}).subscribe(function (res) {
            _this.bonusLists = res.data;
        });
        this.api.getTokenRate({}).subscribe(function (res) {
            _this.tokenRates = res.data;
        });
    };
    SettingsComponent.prototype.ngAfterViewInit = function () {
    };
    SettingsComponent.prototype.saveBonus = function () {
        var _this = this;
        this.api.saveBonus(this.bonusLists).subscribe(function (res) {
            if (res.success) {
                _this.notify.showNotification('Success', 'top', 'center', 'success');
            }
            else {
                _this.notify.showNotification(res.error, 'top', 'center', 'warning');
            }
        });
    };
    SettingsComponent.prototype.saveRates = function () {
        var _this = this;
        this.api.saveRates(this.tokenRates).subscribe(function (res) {
            if (res.success) {
                _this.notify.showNotification('Success', 'top', 'center', 'success');
            }
            else {
                _this.notify.showNotification(res.error, 'top', 'center', 'warning');
            }
        });
    };
    SettingsComponent.prototype.saveRecaptchaSetting = function () {
        var _this = this;
        this.api.saveSettings({
            login_recaptcha: this.sysSettings.login_recaptcha ? 1 : 0,
            register_recaptcha: this.sysSettings.register_recaptcha ? 1 : 0,
            ico_recaptcha: this.sysSettings.ico_recaptcha ? 1 : 0
        }).subscribe(function (res) {
            if (res.success) {
                _this.notify.showNotification('Success', 'top', 'center', 'success');
            }
            else {
                _this.notify.showNotification(res.error, 'top', 'center', 'warning');
            }
        });
    };
    SettingsComponent.prototype.saveSystemSetting = function () {
        var _this = this;
        this.api.saveSettings({
            blockcypher_token: this.sysSettings.blockcypher_token,
            disconnect_time: this.sysSettings.disconnect_time,
            enable_lending: this.sysSettings.enable_lending ? 1 : 0,
            enable_staking: this.sysSettings.enable_staking ? 1 : 0,
            enable_ico: this.sysSettings.enable_ico ? 1 : 0,
            enable_support: this.sysSettings.enable_support ? 1 : 0,
            enable_reg_notification: this.sysSettings.enable_reg_notification ? 1 : 0
        }).subscribe(function (res) {
            if (res.success) {
                _this.notify.showNotification('Success', 'top', 'center', 'success');
            }
            else {
                _this.notify.showNotification(res.error, 'top', 'center', 'warning');
            }
        });
    };
    SettingsComponent.prototype.saveFeeSetting = function () {
        var _this = this;
        this.api.saveSettings({
            withdraw_btc_fee: this.sysSettings.withdraw_btc_fee,
            withdraw_eth_fee: this.sysSettings.withdraw_eth_fee,
            withdraw_bt9_fee: this.sysSettings.withdraw_bt9_fee,
            exchange_buy_fee: this.sysSettings.exchange_buy_fee,
            exchange_sell_fee: this.sysSettings.exchange_sell_fee
        }).subscribe(function (res) {
            if (res.success) {
                _this.notify.showNotification('Success', 'top', 'center', 'success');
            }
            else {
                _this.notify.showNotification(res.error, 'top', 'center', 'warning');
            }
        });
    };
    SettingsComponent.prototype.saveWalletSetting = function () {
        var _this = this;
        this.api.saveSettings({
            enable_btc_wallet: this.sysSettings.enable_btc_wallet ? 1 : 0,
            enable_eth_wallet: this.sysSettings.enable_eth_wallet ? 1 : 0,
            enable_bt9_wallet: this.sysSettings.enable_bt9_wallet ? 1 : 0,
            enable_btc_withdraw: this.sysSettings.enable_btc_withdraw ? 1 : 0,
            enable_eth_withdraw: this.sysSettings.enable_eth_withdraw ? 1 : 0,
            enable_bt9_withdraw: this.sysSettings.enable_bt9_withdraw ? 1 : 0,
            // btc_wallet_mode: this.sysSettings.btc_wallet_mode ? 1 : 0,
            // eth_wallet_mode: this.sysSettings.eth_wallet_mode ? 1 : 0,
            withdraw_btc_volume_day: this.sysSettings.withdraw_btc_volume_day,
            withdraw_btc_volume_month: this.sysSettings.withdraw_btc_volume_month,
            withdraw_eth_volume_day: this.sysSettings.withdraw_eth_volume_day,
            withdraw_eth_volume_month: this.sysSettings.withdraw_eth_volume_month,
            withdraw_myaltcoin_volume_day: this.sysSettings.withdraw_myaltcoin_volume_day,
            withdraw_myaltcoin_volume_month: this.sysSettings.withdraw_myaltcoin_volume_month
        }).subscribe(function (res) {
            if (res.success) {
                _this.notify.showNotification('Success', 'top', 'center', 'success');
            }
            else {
                _this.notify.showNotification(res.error, 'top', 'center', 'warning');
            }
        });
    };
    SettingsComponent.prototype.saveExchangeSetting = function () {
        var _this = this;
        this.api.saveSettings({
            enable_btc_exchange: this.sysSettings.enable_btc_exchange ? 1 : 0,
            enable_eth_exchange: this.sysSettings.enable_eth_exchange ? 1 : 0
        }).subscribe(function (res) {
            if (res.success) {
                _this.notify.showNotification('Success', 'top', 'center', 'success');
            }
            else {
                _this.notify.showNotification(res.error, 'top', 'center', 'warning');
            }
        });
    };
    SettingsComponent.prototype.saveStakingSetting = function () {
        var _this = this;
        this.api.saveSettings({
            staking_locked_days: this.sysSettings.staking_locked_days,
            staking_interest_month: this.sysSettings.staking_interest_month,
            staking_minimum_amount: this.sysSettings.staking_minimum_amount
        }).subscribe(function (res) {
            if (res.success) {
                _this.notify.showNotification('Success', 'top', 'center', 'success');
            }
            else {
                _this.notify.showNotification(res.error, 'top', 'center', 'warning');
            }
        });
    };
    SettingsComponent.prototype.saveLendingSetting = function () {
        var _this = this;
        this.api.saveSettings({
            lending_interest_min: this.sysSettings.lending_interest_min,
            lending_interest_max: this.sysSettings.lending_interest_max
        }).subscribe(function (res) {
            if (res.success) {
                _this.notify.showNotification('Success', 'top', 'center', 'success');
            }
            else {
                _this.notify.showNotification(res.error, 'top', 'center', 'warning');
            }
        });
    };
    SettingsComponent.prototype.saveBT9RPCSetting = function () {
        var _this = this;
        this.api.saveSettings({
            bt9_rpcserver: this.sysSettings.bt9_rpcserver,
            bt9_rpcuser: this.sysSettings.bt9_rpcuser,
            bt9_rpcpassword: this.sysSettings.bt9_rpcpassword,
            bt9_rpcport: this.sysSettings.bt9_rpcport
        }).subscribe(function (res) {
            if (res.success) {
                _this.notify.showNotification('Success', 'top', 'center', 'success');
            }
            else {
                _this.notify.showNotification(res.error, 'top', 'center', 'warning');
            }
        });
    };
    SettingsComponent.prototype.saveBTCRPCSetting = function () {
        var _this = this;
        this.api.saveSettings({
            btc_rpcserver: this.sysSettings.btc_rpcserver,
            btc_rpcuser: this.sysSettings.btc_rpcuser,
            btc_rpcpassword: this.sysSettings.btc_rpcpassword,
            btc_rpcport: this.sysSettings.btc_rpcport
        }).subscribe(function (res) {
            if (res.success) {
                _this.notify.showNotification('Success', 'top', 'center', 'success');
            }
            else {
                _this.notify.showNotification(res.error, 'top', 'center', 'warning');
            }
        });
    };
    SettingsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-admin-settings',
            template: __webpack_require__("../../../../../src/app/admin/settings/settings.component.html"),
            styles: [__webpack_require__("../../../../../src/app/admin/settings/settings.component.scss")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_api_service__["a" /* Api */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_api_service__["a" /* Api */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__services_settings_settings_service__["a" /* SettingsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_settings_settings_service__["a" /* SettingsService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__services_notifications_service__["a" /* Notifications */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services_notifications_service__["a" /* Notifications */]) === "function" && _c || Object])
    ], SettingsComponent);
    return SettingsComponent;
    var _a, _b, _c;
}());

//# sourceMappingURL=settings.component.js.map

/***/ }),

/***/ "../../../../../src/app/admin/staking/staking.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\n    <div class=\"container-fluid\">\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n                <div class=\"card\">\n                    <div class=\"card-header card-header-icon\" data-background-color=\"rose\">\n                        <i class=\"material-icons\">play_for_work</i>\n                    </div>\n                    <div class=\"card-content\">\n                        <h4 class=\"card-title\">Stakings</h4>\n                        <div>\n                            <button type=\"button\" class=\"btn btn-info\" (click)=\"releaseAllStaking()\">Release ALL Staking-capital</button>\n                        </div>\n                        <div class=\"table-responsive\">\n                            <table class=\"table\">\n                                <thead>\n                                <tr>\n                                    <th class=\"text-center\">User Email</th>\n                                    <th class=\"text-center\">Staking Amount(BT9)</th>\n                                    <th class=\"text-center\">Date</th>\n                                    <th class=\"text-center\">Finish Date</th>\n                                </tr>\n                                </thead>\n                                <tbody>\n                                <tr *ngFor=\"let staking of stakings; let i=index\">\n                                    <td class=\"text-center\">{{staking.email}}</td>\n                                    <td class=\"text-center\">{{staking.src_amount | number:'1.8-8'}}</td>\n                                    <td class=\"text-center\">{{staking.date | date:'yyyy-MM-dd'}}</td>\n                                    <td class=\"text-center\">{{staking.finish_date | date:'yyyy-MM-dd'}}</td>\n                                </tr>\n                                <!---->\n                                <tr *ngIf=\"stakings?.length == 0\">\n                                    <td class=\"text-center\" colspan=\"4\">\n                                        No Staking found.\n                                    </td>\n                                </tr>\n                                </tbody>\n                            </table>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/admin/staking/staking.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".font-size-17 {\n  font-size: 17px; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/admin/staking/staking.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminStakingComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_api_service__ = __webpack_require__("../../../../../src/app/services/api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_settings_settings_service__ = __webpack_require__("../../../../../src/app/services/settings/settings.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_notifications_service__ = __webpack_require__("../../../../../src/app/services/notifications.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AdminStakingComponent = (function () {
    function AdminStakingComponent(api, settings, notify) {
        this.api = api;
        this.settings = settings;
        this.notify = notify;
        this.stakings = [];
    }
    AdminStakingComponent.prototype.ngOnInit = function () {
        this.loadStakings();
    };
    AdminStakingComponent.prototype.loadStakings = function () {
        var _this = this;
        this.api.getAllStaking({}).subscribe(function (res) {
            if (res.success) {
                _this.stakings = res.data;
            }
        });
    };
    AdminStakingComponent.prototype.ngAfterViewInit = function () {
    };
    AdminStakingComponent.prototype.releaseAllStaking = function () {
        var _parent = this;
        swal({
            title: 'Are you sure?',
            text: 'Release All Staking-Capital?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            confirmButtonText: 'Yes',
            buttonsStyling: false
        }).then(function () {
            _parent.settings.loading = true;
            _parent.api.releaseAllStaking({}).subscribe(function (res) {
                _parent.settings.loading = false;
                if (res.success) {
                    _parent.notify.showNotification('Success', 'top', 'center', 'success');
                }
                else {
                    _parent.notify.showNotification(res.error, 'top', 'center', 'warning');
                }
            }, function (err) {
                _parent.settings.loading = false;
            });
        });
    };
    AdminStakingComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-admin-settings',
            template: __webpack_require__("../../../../../src/app/admin/staking/staking.component.html"),
            styles: [__webpack_require__("../../../../../src/app/admin/staking/staking.component.scss")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_api_service__["a" /* Api */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_api_service__["a" /* Api */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__services_settings_settings_service__["a" /* SettingsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_settings_settings_service__["a" /* SettingsService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__services_notifications_service__["a" /* Notifications */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services_notifications_service__["a" /* Notifications */]) === "function" && _c || Object])
    ], AdminStakingComponent);
    return AdminStakingComponent;
    var _a, _b, _c;
}());

//# sourceMappingURL=staking.component.js.map

/***/ }),

/***/ "../../../../../src/app/admin/transaction/transaction.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\n    <div class=\"container-fluid\">\n\n        <div class=\"row\"  style=\"margin-top: 20px;\">\n            <div class=\"col-md-12\">\n                <div class=\"card\">\n                    <div class=\"card-header card-header-tabs\" data-background-color=\"blue\">\n                        <div class=\"nav-tabs-navigation\">\n                            <div class=\"nav-tabs-wrapper\">\n                                <ul class=\"nav nav-tabs\" data-tabs=\"tabs\">\n                                    <li class=\"active\">\n                                        <a href=\"#token\" data-toggle=\"tab\">\n                                            <img src=\"/assets/img/B9_color.png\" style=\"width: 25px; \"/> BT9COIN\n                                            <div class=\"ripple-container\"></div>\n                                        </a>\n                                    </li>\n                                    <li class=\"\">\n                                        <a href=\"#btc\" data-toggle=\"tab\">\n                                            <img src=\"/assets/img/BTC.png\" style=\"width: 25px; \"/> Bitcoin\n                                            <div class=\"ripple-container\"></div>\n                                        </a>\n                                    </li>\n                                    <li class=\"\">\n                                        <a href=\"#eth\" data-toggle=\"tab\">\n                                            <img src=\"/assets/img/ethereum.png\" style=\"width: 25px; \"/> Ethereum\n                                            <div class=\"ripple-container\"></div>\n                                        </a>\n                                    </li>\n                                </ul>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"card-content\">\n                        <div class=\"tab-content\">\n                            <div class=\"tab-pane active\" id=\"token\">\n                                <div class=\"tx-list\">\n                                    <h4 class=\"card-title\"><b>Transaction List</b></h4>\n                                    <div class=\"material-datatables table-responsive\">\n                                        <table class=\"table table-striped table-no-bordered table-hover\"\n                                               cellspacing=\"0\" width=\"100%\" style=\"width:100%\">\n                                            <thead>\n                                            <tr>\n                                                <th>#</th>\n                                                <th style=\"width: 200px;\">TIME</th>\n                                                <th>LABEL</th>\n                                                <th>CATEGORY</th>\n                                                <th>AMOUNT</th>\n                                                <th>ADDRESS</th>\n                                                <th>TXID</th>\n                                            </tr>\n                                            </thead>\n                                            <tbody>\n                                            <tr *ngFor=\"let tx of bt9TxList; let i = index\">\n                                                <td>{{i + 1}}</td>\n                                                <td>{{tx.time * 1000 | date:'yyyy-MM-dd HH:mm:ss'}}</td>\n                                                <td>{{tx.account}}</td>\n                                                <td>{{tx.category}}</td>\n                                                <td>{{tx.amount | number: '1.8-8'}}</td>\n                                                <td>{{tx.address}}</td>\n                                                <td>{{tx.txid}}</td>\n                                            </tr>\n\n                                            <tr *ngIf=\"bt9TxList?.length == 0\">\n                                                <td class=\"text-center\" colspan=\"7\">\n                                                    No Transaction found.\n                                                </td>\n                                            </tr>\n                                            </tbody>\n                                        </table>\n                                    </div>\n                                </div>\n                            </div>\n                            <div class=\"tab-pane\" id=\"btc\">\n                                <div class=\"tx-list\">\n                                    <h4 class=\"card-title\"><b>Transaction List</b></h4>\n                                    <div class=\"material-datatables table-responsive\">\n                                        <table class=\"table table-striped table-no-bordered table-hover\"\n                                               cellspacing=\"0\" width=\"100%\" style=\"width:100%\">\n                                            <thead>\n                                            <tr>\n                                                <th>#</th>\n                                                <th style=\"width: 200px;\">TIME</th>\n                                                <th>LABEL</th>\n                                                <th>CATEGORY</th>\n                                                <th>AMOUNT</th>\n                                                <th>ADDRESS</th>\n                                                <th>TXID</th>\n                                            </tr>\n                                            </thead>\n                                            <tbody>\n                                            <tr *ngFor=\"let tx of btcTxList; let i = index\">\n                                                <td>{{i + 1}}</td>\n                                                <td>{{tx.time * 1000 | date:'yyyy-MM-dd HH:mm:ss'}}</td>\n                                                <td>{{tx.account}}</td>\n                                                <td>{{tx.category}}</td>\n                                                <td>{{tx.amount | number: '1.8-8'}}</td>\n                                                <td>{{tx.address}}</td>\n                                                <td>{{tx.txid}}</td>\n                                            </tr>\n\n                                            <tr *ngIf=\"btcTxList?.length == 0\">\n                                                <td class=\"text-center\" colspan=\"7\">\n                                                    No Transaction found.\n                                                </td>\n                                            </tr>\n                                            </tbody>\n                                        </table>\n                                    </div>\n                                </div>\n                            </div>\n                            <div class=\"tab-pane\" id=\"eth\">\n                                <div class=\"tx-list\">\n                                    <h4 class=\"card-title\"><b>Transaction List</b></h4>\n                                    <div class=\"material-datatables table-responsive\">\n                                        <table class=\"table table-striped table-no-bordered table-hover\"\n                                               cellspacing=\"0\" width=\"100%\" style=\"width:100%\">\n                                            <thead>\n                                            <tr>\n                                                <th>#</th>\n                                                <th style=\"width: 200px;\">TIME</th>\n                                                <th>LABEL</th>\n                                                <th>CATEGORY</th>\n                                                <th>AMOUNT</th>\n                                                <th>ADDRESS</th>\n                                                <th>TXID</th>\n                                            </tr>\n                                            </thead>\n                                            <tbody>\n                                            <tr *ngFor=\"let tx of ethTxList; let i = index\">\n                                                <td>{{i + 1}}</td>\n                                                <td>{{tx.time * 1000 | date:'yyyy-MM-dd HH:mm:ss'}}</td>\n                                                <td>{{tx.account}}</td>\n                                                <td>{{tx.category}}</td>\n                                                <td>{{tx.amount | number: '1.8-8'}}</td>\n                                                <td>{{tx.address}}</td>\n                                                <td>{{tx.txid}}</td>\n                                            </tr>\n\n                                            <tr *ngIf=\"ethTxList?.length == 0\">\n                                                <td class=\"text-center\" colspan=\"7\">\n                                                    No Transaction found.\n                                                </td>\n                                            </tr>\n                                            </tbody>\n                                        </table>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/admin/transaction/transaction.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".card-content button {\n  width: 60px;\n  height: 60px; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/admin/transaction/transaction.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminTransactionComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_api_service__ = __webpack_require__("../../../../../src/app/services/api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_settings_settings_service__ = __webpack_require__("../../../../../src/app/services/settings/settings.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_balance_service__ = __webpack_require__("../../../../../src/app/services/balance.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AdminTransactionComponent = (function () {
    function AdminTransactionComponent(api, settings, balance) {
        this.api = api;
        this.settings = settings;
        this.balance = balance;
        this.bt9TxList = [];
        this.btcTxList = [];
        this.ethTxList = [];
    }
    AdminTransactionComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.api.getAllTransactionHistory('BT9', {}).subscribe(function (res) {
            if (res.success) {
                _this.bt9TxList = res.data;
            }
        });
        this.api.getAllTransactionHistory('BTC', {}).subscribe(function (res) {
            if (res.success) {
                _this.btcTxList = res.data;
            }
        });
        this.api.getAllTransactionHistory('ETH', {}).subscribe(function (res) {
            if (res.success) {
                _this.ethTxList = res.data;
            }
        });
    };
    AdminTransactionComponent.prototype.ngAfterViewInit = function () {
    };
    AdminTransactionComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-admin-transaction',
            template: __webpack_require__("../../../../../src/app/admin/transaction/transaction.component.html"),
            styles: [__webpack_require__("../../../../../src/app/admin/transaction/transaction.component.scss")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_api_service__["a" /* Api */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_api_service__["a" /* Api */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__services_settings_settings_service__["a" /* SettingsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_settings_settings_service__["a" /* SettingsService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__services_balance_service__["a" /* BalanceService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services_balance_service__["a" /* BalanceService */]) === "function" && _c || Object])
    ], AdminTransactionComponent);
    return AdminTransactionComponent;
    var _a, _b, _c;
}());

//# sourceMappingURL=transaction.component.js.map

/***/ }),

/***/ "../../../../../src/app/admin/users/detail/detail.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\n    <div class=\"container-fluid\">\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n                <h3>User email: <b>{{user.email}}</b></h3>\n            </div>\n        </div>\n        <div class=\"row\">\n            <div class=\"col-md-4\">\n                <h4>BT9 Balance: {{user.token_balance | number:'1.8-8'}}</h4>\n            </div>\n\n            <div class=\"col-md-4\">\n                <h4>BTC Balance: {{user.btc_balance | number:'1.8-8'}} BTC</h4>\n            </div>\n\n            <div class=\"col-md-4\">\n                <h4>ETH Balance: {{user.eth_balance | number:'1.8-8'}} ETH</h4>\n            </div>\n        </div>\n        <div class=\"row\"  style=\"margin-top: 20px;\">\n            <div class=\"col-md-12\">\n                <div class=\"card\">\n                    <div class=\"card-header card-header-tabs\" data-background-color=\"blue\">\n                        <div class=\"nav-tabs-navigation\">\n                            <div class=\"nav-tabs-wrapper\">\n                                <ul class=\"nav nav-tabs\" data-tabs=\"tabs\">\n                                    <li class=\"active\">\n                                        <a href=\"#token\" data-toggle=\"tab\">\n                                            <img src=\"/assets/img/B9_color.png\" style=\"width: 25px; \"/> BT9COIN\n                                            <div class=\"ripple-container\"></div>\n                                        </a>\n                                    </li>\n                                    <li class=\"\" (click)=\"loadBTCHistory()\">\n                                        <a href=\"#btc\" data-toggle=\"tab\">\n                                            <img src=\"/assets/img/BTC.png\" style=\"width: 25px; \"/> Bitcoin\n                                            <div class=\"ripple-container\"></div>\n                                        </a>\n                                    </li>\n                                    <li class=\"\" (click)=\"loadETHHistory()\">\n                                        <a href=\"#eth\" data-toggle=\"tab\">\n                                            <img src=\"/assets/img/ethereum.png\" style=\"width: 25px; \"/> Ethereum\n                                            <div class=\"ripple-container\"></div>\n                                        </a>\n                                    </li>\n                                </ul>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"card-content\">\n                        <div class=\"tab-content\">\n                            <div class=\"tab-pane active\" id=\"token\">\n                                <!--<div class=\"row\">-->\n                                    <!--<div class=\"col-md-12\">-->\n                                        <!--<div class=\"float-right\">-->\n                                            <!--<button type=\"button\" class=\"btn btn-info btn-round\" (click)=\"addMyAltCoin()\">-->\n                                                <!--<i class=\"fa fa-plus\"></i>-->\n                                            <!--</button>-->\n                                        <!--</div>-->\n                                    <!--</div>-->\n                                <!--</div>-->\n                                <div class=\"token-history\">\n                                    <h4 class=\"card-title\"><b>BT9 History</b></h4>\n                                    <div class=\"material-datatables table-responsive\">\n                                        <table id=\"myaltcoindatatable\"\n                                               class=\"table table-striped table-no-bordered table-hover\"\n                                               cellspacing=\"0\" width=\"100%\" style=\"width:100%\">\n                                            <thead>\n                                            <tr>\n                                                <th style=\"width: 200px;\">DATETIME</th>\n                                                <th>AMOUNT</th>\n                                                <th>TYPE</th>\n                                                <th>STATUS</th>\n                                                <th>DESCRIPTION</th>\n                                            </tr>\n                                            </thead>\n                                            <tbody>\n                                            <tr *ngFor=\"let tx of tokenHistory; let last = last\">\n                                                <td style=\"width: 200px;\">{{tx['date'] | date: 'yyyy-MM-dd HH:mm:ss'}}</td>\n                                                <td>{{(tx['category'] == 'send' ? (-1) * tx['src_amount'] : tx['dest_amount']) | number:'1.0-8'}}</td>\n                                                <td [ngSwitch]=\"tx['type']\">\n                                                    <span *ngSwitchCase=\"1\">PURCHASE TOKEN</span>\n                                                    <span *ngSwitchCase=\"2\">\n                                                            {{tx['exchange_type'] == 0 ? 'EXCHANGE/BUY' : 'EXCHANGE/SELL'}}\n                                                        </span>\n                                                    <span *ngSwitchCase=\"3\">BONUS</span>\n                                                    <span *ngSwitchCase=\"4\">LENDING</span>\n                                                    <span *ngSwitchCase=\"5\">WITHDRAW</span>\n                                                    <span *ngSwitchCase=\"6\">LENDING/TRANSFER</span>\n                                                    <span *ngSwitchCase=\"7\">STAKING</span>\n                                                    <span *ngSwitchCase=\"8\">STAKING/UNLOCK</span>\n                                                    <span *ngSwitchCase=\"9\">DEPOSIT</span>\n                                                    <span *ngSwitchCase=\"10\">STAKING/EARN</span>\n                                                </td>\n                                                <td [ngSwitch]=\"tx['status']\">\n                                                    <label class=\"label label-primary\" *ngSwitchCase=\"0\">PENDING</label>\n                                                    <label class=\"label label-success\" *ngSwitchCase=\"1\">SUCCESS</label>\n                                                    <label class=\"label label-warning\" *ngSwitchCase=\"4\">OPENING</label>\n                                                </td>\n                                                <td>{{tx['txid']}}</td>\n                                            </tr>\n                                            </tbody>\n                                        </table>\n                                    </div>\n                                </div>\n                            </div>\n                            <div class=\"tab-pane\" id=\"btc\">\n                                <div class=\"deposit-history\">\n                                    <h4 class=\"card-title\"><b>BTC History</b></h4>\n                                    <div class=\"material-datatables table-responsive\">\n                                        <table id=\"btcdatatable\"\n                                               class=\"table table-striped table-no-bordered table-hover\"\n                                               cellspacing=\"0\" width=\"100%\" style=\"width:100%\">\n                                            <thead>\n                                            <tr>\n                                                <th style=\"width: 200px;\">DATETIME</th>\n                                                <th>AMOUNT</th>\n                                                <th>TYPE</th>\n                                                <th>STATUS</th>\n                                                <th>DESCRIPTION</th>\n                                            </tr>\n                                            </thead>\n                                            <tbody>\n                                            <tr *ngFor=\"let tx of btcHistory; let last = last\">\n                                                <td style=\"width: 200px;\">{{tx['date'] | date: 'yyyy-MM-dd HH:mm:ss'}}</td>\n                                                <td>{{(tx['category'] == 'send' ? (-1) * tx['src_amount'] : tx['dest_amount']) | number:'1.0-8'}}</td>\n                                                <td [ngSwitch]=\"tx['type']\">\n                                                    <span *ngSwitchCase=\"1\">PURCHASE TOKEN</span>\n                                                    <span *ngSwitchCase=\"2\">\n                                                            {{tx['exchange_type'] == 0 ? 'EXCHANGE/BUY' : 'EXCHANGE/SELL'}}\n                                                        </span>\n                                                    <span *ngSwitchCase=\"3\">BONUS</span>\n                                                    <span *ngSwitchCase=\"4\">LENDING</span>\n                                                    <span *ngSwitchCase=\"5\">WITHDRAW</span>\n                                                    <span *ngSwitchCase=\"6\">LENDING/TRANSFER</span>\n                                                    <span *ngSwitchCase=\"7\">STAKING</span>\n                                                    <span *ngSwitchCase=\"8\">STAKING/UNLOCK</span>\n                                                    <span *ngSwitchCase=\"9\">DEPOSIT</span>\n                                                    <span *ngSwitchCase=\"10\">STAKING/EARN</span>\n                                                </td>\n                                                <td [ngSwitch]=\"tx['status']\">\n                                                    <label class=\"label label-primary\" *ngSwitchCase=\"0\">PENDING</label>\n                                                    <label class=\"label label-success\" *ngSwitchCase=\"1\">SUCCESS</label>\n                                                    <label class=\"label label-warning\" *ngSwitchCase=\"4\">OPENING</label>\n                                                </td>\n                                                <td>{{tx['txid']}}</td>\n                                            </tr>\n                                            </tbody>\n                                        </table>\n                                    </div>\n                                </div>\n                            </div>\n                            <div class=\"tab-pane\" id=\"eth\">\n                                <div class=\"deposit-history\">\n                                    <h4 class=\"card-title\"><b>ETH History</b></h4>\n                                    <div class=\"material-datatables table-responsive\">\n                                        <table id=\"ethdatatable\"\n                                               class=\"table table-striped table-no-bordered table-hover\"\n                                               cellspacing=\"0\" width=\"100%\" style=\"width:100%\">\n                                            <thead>\n                                            <tr>\n                                                <th style=\"width: 200px;\">DATETIME</th>\n                                                <th>AMOUNT</th>\n                                                <th>TYPE</th>\n                                                <th>STATUS</th>\n                                                <th>DESCRIPTION</th>\n                                            </tr>\n                                            </thead>\n                                            <tbody>\n                                            <tr *ngFor=\"let tx of ethHistory; let last = last\">\n                                                <td style=\"width: 200px;\">{{tx['date'] | date: 'yyyy-MM-dd HH:mm:ss'}}</td>\n                                                <td>{{(tx['category'] == 'send' ? (-1) * tx['src_amount'] : tx['dest_amount']) | number:'1.0-8'}}</td>\n                                                <td [ngSwitch]=\"tx['type']\">\n                                                    <span *ngSwitchCase=\"1\">PURCHASE TOKEN</span>\n                                                    <span *ngSwitchCase=\"2\">\n                                                            {{tx['exchange_type'] == 0 ? 'EXCHANGE/BUY' : 'EXCHANGE/SELL'}}\n                                                        </span>\n                                                    <span *ngSwitchCase=\"3\">BONUS</span>\n                                                    <span *ngSwitchCase=\"4\">LENDING</span>\n                                                    <span *ngSwitchCase=\"5\">WITHDRAW</span>\n                                                    <span *ngSwitchCase=\"6\">LENDING/TRANSFER</span>\n                                                    <span *ngSwitchCase=\"7\">STAKING</span>\n                                                    <span *ngSwitchCase=\"8\">STAKING/UNLOCK</span>\n                                                    <span *ngSwitchCase=\"9\">DEPOSIT</span>\n                                                    <span *ngSwitchCase=\"10\">STAKING/EARN</span>\n                                                </td>\n                                                <td [ngSwitch]=\"tx['status']\">\n                                                    <label class=\"label label-primary\" *ngSwitchCase=\"0\">PENDING</label>\n                                                    <label class=\"label label-success\" *ngSwitchCase=\"1\">SUCCESS</label>\n                                                    <label class=\"label label-warning\" *ngSwitchCase=\"4\">OPENING</label>\n                                                </td>\n                                                <td>{{tx['txid']}}</td>\n                                            </tr>\n                                            </tbody>\n                                        </table>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n\n\n<modal-dialog [(visible)]=\"showModal\">\n    <h2>Add BT9</h2>\n    <div class=\"dialog\">\n        <form class=\"form-horizontal\">\n            <div class=\"row\">\n                <div class=\"col-md-12\">\n                    <mat-datepicker-toggle matSuffix [for]=\"resultPicker\">\n                    </mat-datepicker-toggle>\n                    <mat-input-container>\n                        <input matInput\n                               [matDatepicker]=\"resultPicker\"\n                               name=\"date\"\n                               [(ngModel)]=\"tx.date\"\n                               placeholder=\"Date\">\n                    </mat-input-container>\n                    <mat-datepicker\n                            #resultPicker\n                            [touchUi]=\"touch\"\n                            [startView]=\"'month'\">\n                    </mat-datepicker>\n                </div>\n            </div>\n            <div class=\"row\">\n                <div class=\"col-md-12\">\n                    <mat-form-field>\n                        <mat-select placeholder=\"Coin\" [(ngModel)]=\"tx.coin\" name=\"coin\">\n                            <mat-option value=\"BTC\">BTC</mat-option>\n                            <mat-option value=\"ETH\">ETH</mat-option>\n                        </mat-select>\n                    </mat-form-field>\n                </div>\n            </div>\n            <div class=\"row\">\n                <div class=\"col-md-12\">\n                    <div class=\"form-group label-floating is-empty\">\n                        <label class=\"control-label\">Coin Amount</label>\n                        <input type=\"text\" class=\"form-control\" [(ngModel)]=\"tx.coin_amount\" name=\"coin_amount\">\n                    </div>\n                </div>\n            </div>\n            <div class=\"row\">\n                <div class=\"col-md-12\">\n                    <div class=\"form-group label-floating is-empty\">\n                        <label class=\"control-label\">BT9 Amount</label>\n                        <input type=\"text\" class=\"form-control\" name=\"myaltcoin_amount\" [(ngModel)]=\"tx.myaltcoin_amount\">\n                    </div>\n                </div>\n            </div>\n            <div class=\"text-center\" style=\"margin-top: 20px;\">\n                <button type=\"button\" class=\"btn btn-primary\" (click)=\"submit()\">Submit</button>\n                <button type=\"button\" class=\"btn btn-default\" (click)=\"showModal = !showModal\">Cancel</button>\n            </div>\n        </form>\n    </div>\n</modal-dialog>"

/***/ }),

/***/ "../../../../../src/app/admin/users/detail/detail.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".withdraw-history {\n  margin-top: 50px; }\n\ntable tr td:first-child {\n  width: 100px; }\n\n.dialog {\n  padding: 0px 20px; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/admin/users/detail/detail.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserDetailComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_api_service__ = __webpack_require__("../../../../../src/app/services/api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_settings_settings_service__ = __webpack_require__("../../../../../src/app/services/settings/settings.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_balance_service__ = __webpack_require__("../../../../../src/app/services/balance.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var UserDetailComponent = (function () {
    function UserDetailComponent(api, settings, balance, route, router) {
        this.api = api;
        this.settings = settings;
        this.balance = balance;
        this.route = route;
        this.router = router;
        // constructor(private navbarTitleService: NavbarTitleService, private notificationService: NotificationService) { }
        this.user = {};
        this.tokenHistory = [];
        this.btcHistory = [];
        this.ethHistory = [];
        this.tx = {
            date: new Date()
        };
        this.showModal = false;
        this.tokendatatable = null;
        this.btcdatatable = null;
        this.ethdatatable = null;
    }
    UserDetailComponent.prototype.ngOnInit = function () {
        var _this = this;
        var user_id = this.route.snapshot.params['userid'];
        this.api.getUser({
            id: user_id
        }).subscribe(function (res) {
            if (res.success) {
                _this.user = res.data;
                _this.loadTokenHistory();
                // this.loadBTCReceivedTxList();
                // this.loadETHReceivedTxList();
                // this.loadBTCSentTxList();
                /// this.loadETHSentTxList();
            }
            else {
                _this.router.navigate(['/admin/dashboard']);
            }
        }, function (err) {
            _this.router.navigate(['/admin/dashboard']);
        });
    };
    UserDetailComponent.prototype.buildTokenDataTable = function () {
        this.tokendatatable = $('#myaltcoindatatable').DataTable({
            'pagingType': 'full_numbers',
            'lengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
            responsive: true,
            language: {
                search: '_INPUT_',
                searchPlaceholder: 'Search records',
            }
        });
    };
    UserDetailComponent.prototype.loadTokenHistory = function () {
        var _this = this;
        if (this.tokendatatable) {
            this.tokendatatable.destroy();
        }
        this.api.getAdminUserTokenHistory({
            user_id: this.user.id
        }).subscribe(function (res) {
            if (res.success) {
                _this.tokenHistory = res.data;
                var _parent_1 = _this;
                var timerID = setInterval(function () {
                    if ($('#myaltcoindatatable tbody tr').length == res.data.length) {
                        clearInterval(timerID);
                        _parent_1.tokendatatable = $('#myaltcoindatatable').DataTable({});
                    }
                }, 200);
            }
        });
    };
    UserDetailComponent.prototype.loadBTCHistory = function () {
        var _this = this;
        if (this.btcdatatable) {
            this.btcdatatable.destroy();
        }
        this.api.getAdminUserBTCHistory({
            user_id: this.user.id
        }).subscribe(function (res) {
            if (res.success) {
                _this.btcHistory = res.data;
                var _parent_2 = _this;
                var timerID = setInterval(function () {
                    if ($('#btcdatatable tbody tr').length == res.data.length) {
                        clearInterval(timerID);
                        _parent_2.btcdatatable = $('#btcdatatable').DataTable({});
                    }
                }, 200);
            }
        });
    };
    UserDetailComponent.prototype.loadETHHistory = function () {
        var _this = this;
        if (this.ethdatatable) {
            this.ethdatatable.destroy();
        }
        this.api.getAdminUserETHHistory({
            user_id: this.user.id
        }).subscribe(function (res) {
            if (res.success) {
                _this.ethHistory = res.data;
                var _parent_3 = _this;
                var timerID = setInterval(function () {
                    if ($('#ethdatatable tbody tr').length == res.data.length) {
                        clearInterval(timerID);
                        _parent_3.ethdatatable = $('#ethdatatable').DataTable({});
                    }
                }, 200);
            }
        });
    };
    UserDetailComponent.prototype.ngAfterViewInit = function () {
    };
    UserDetailComponent.prototype.addMyAltCoin = function () {
        this.showModal = true;
    };
    UserDetailComponent.prototype.submit = function () {
        var _this = this;
        if (this.tx.coin == null || this.tx.coin == '') {
            return;
        }
        if (this.tx.coin_amount == null || this.tx.coin_amount == '') {
            return;
        }
        if (this.tx.myaltcoin_amount == null || this.tx.myaltcoin_amount == '') {
            return;
        }
        var date = this.tx.date.getFullYear() + '-' + (this.tx.date.getMonth() + 1) + '-' + (this.tx.date.getDate());
        this.api.addMyAltCoin({
            user_id: this.user.id,
            date: date,
            src_currency: this.tx.coin,
            src_amount: this.tx.coin_amount,
            dest_amount: this.tx.myaltcoin_amount
        }).subscribe(function (res) {
            if (res.success) {
                _this.showModal = false;
                _this.loadTokenHistory();
            }
        }, function (err) {
        });
    };
    UserDetailComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-coming',
            template: __webpack_require__("../../../../../src/app/admin/users/detail/detail.component.html"),
            styles: [__webpack_require__("../../../../../src/app/admin/users/detail/detail.component.scss")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_api_service__["a" /* Api */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_api_service__["a" /* Api */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__services_settings_settings_service__["a" /* SettingsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_settings_settings_service__["a" /* SettingsService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__services_balance_service__["a" /* BalanceService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services_balance_service__["a" /* BalanceService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_4__angular_router__["a" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__angular_router__["a" /* ActivatedRoute */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_4__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__angular_router__["c" /* Router */]) === "function" && _e || Object])
    ], UserDetailComponent);
    return UserDetailComponent;
    var _a, _b, _c, _d, _e;
}());

//# sourceMappingURL=detail.component.js.map

/***/ }),

/***/ "../../../../../src/app/admin/users/users.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\n    <div class=\"container-fluid\">\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n                <div class=\"card\">\n                    <div class=\"card-content\">\n                        <h4 class=\"card-title\"><b>Users</b></h4>\n                        <div class=\"material-datatables table-responsive\">\n                            <table id=\"userstable\"\n                                   class=\"table table-striped table-no-bordered table-hover\"\n                                   cellspacing=\"0\" width=\"100%\" style=\"width:100%\">\n                                <thead>\n                                <tr>\n                                    <th>Email</th>\n                                    <th class=\"text-center\">Username</th>\n                                    <th class=\"text-center\">Full Name</th>\n                                    <th class=\"text-center\">Confirmed Email</th>\n                                    <th class=\"text-center\">Enable 2FA</th>\n                                    <th class=\"text-center\">Register At</th>\n                                    <th class=\"text-right\">Actions</th>\n                                </tr>\n                                </thead>\n                                <tbody>\n                                <tr *ngFor=\"let user of users;\">\n                                    <td>{{user.email}}</td>\n                                    <td class=\"text-center\">{{user.username}}</td>\n                                    <td class=\"text-center\">{{user.fullname}}</td>\n                                    <td class=\"text-center\">{{user.confirmed == 1 ? 'Yes' : 'No'}}</td>\n                                    <td class=\"text-center\">{{user.allow_g2f == 1 ? 'Yes' : 'No'}}</td>\n                                    <td class=\"text-center\">{{user.created_at | date: 'yyyy-MM-dd'}}</td>\n                                    <td class=\"text-right\">\n                                        <button class=\"btn btn-simple btn-warning btn-icon view\"  matTooltip=\"Transaction View\" [matTooltipPosition]=\"'left'\">\n                                            <i class=\"material-icons\">dvr</i>\n                                        </button>\n                                        <button class=\"btn btn-simple btn-danger btn-icon remove\"  matTooltip=\"Remove\" [matTooltipPosition]=\"'left'\" *ngIf=\"settings.getUserSetting('role') == 'admin'\">\n                                            <i class=\"material-icons\">close</i>\n                                        </button>\n                                    </td>\n                                </tr>\n                                </tbody>\n                            </table>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/admin/users/users.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/admin/users/users.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UsersComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_api_service__ = __webpack_require__("../../../../../src/app/services/api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_settings_settings_service__ = __webpack_require__("../../../../../src/app/services/settings/settings.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_balance_service__ = __webpack_require__("../../../../../src/app/services/balance.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var UsersComponent = (function () {
    function UsersComponent(router, api, settings, balance) {
        this.router = router;
        this.api = api;
        this.settings = settings;
        this.balance = balance;
        // constructor(private navbarTitleService: NavbarTitleService, private notificationService: NotificationService) { }
        this.users = [];
    }
    UsersComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.api.getUsers().subscribe(function (res) {
            _this.users = res.data;
            var _parent = _this;
            var timerID = setInterval(function () {
                if ($('#userstable tbody tr').length == res.data.length) {
                    clearInterval(timerID);
                    _parent.buildUsersDataTable();
                }
            }, 200);
        });
    };
    UsersComponent.prototype.buildUsersDataTable = function () {
        $('#userstable').DataTable({
            'pagingType': 'full_numbers',
            'lengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
            responsive: true,
            language: {
                search: '_INPUT_',
                searchPlaceholder: 'Search records',
            }
        });
        var table = $('#userstable').DataTable();
        // Edit record
        table.on('click', '.view', function () {
            var $tr = $(this).closest('tr');
            var data = table.row($tr).data();
            _parent.api.getUserIDByEmail({
                email: data[0]
            }).subscribe(function (res) {
                if (res.success) {
                    _parent.router.navigate(['/admin/users/detail/' + res.id]);
                }
            });
        });
        var _parent = this;
        // Delete a record
        table.on('click', '.remove', function (e) {
            var $tr = $(this).closest('tr');
            var data = table.row($tr).data();
            swal({
                title: 'Are you sure?',
                text: 'This user will be deleted permanently.',
                type: 'warning',
                showCancelButton: true,
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                confirmButtonText: 'Yes, delete it!',
                buttonsStyling: false
            }).then(function () {
                _parent.api.deleteUser({
                    email: data[0]
                }).subscribe(function (res) {
                    if (res.success) {
                        table.row($tr).remove().draw();
                        e.preventDefault();
                        swal({
                            title: 'Deleted!',
                            text: 'Your file has been deleted.',
                            type: 'success',
                            confirmButtonClass: 'btn btn-success',
                            buttonsStyling: false
                        });
                    }
                    else {
                        swal({
                            title: 'Failed!',
                            text: 'This user doest not exist.',
                            type: 'warning',
                            confirmButtonClass: 'btn btn-success',
                            buttonsStyling: false
                        });
                    }
                });
            });
        });
    };
    UsersComponent.prototype.ngAfterViewInit = function () {
    };
    UsersComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-users',
            template: __webpack_require__("../../../../../src/app/admin/users/users.component.html"),
            styles: [__webpack_require__("../../../../../src/app/admin/users/users.component.scss")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_4__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__angular_router__["c" /* Router */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__services_api_service__["a" /* Api */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_api_service__["a" /* Api */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__services_settings_settings_service__["a" /* SettingsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_settings_settings_service__["a" /* SettingsService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_3__services_balance_service__["a" /* BalanceService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services_balance_service__["a" /* BalanceService */]) === "function" && _d || Object])
    ], UsersComponent);
    return UsersComponent;
    var _a, _b, _c, _d;
}());

//# sourceMappingURL=users.component.js.map

/***/ }),

/***/ "../../../../../src/app/admin/withdraw/withdraw.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\n    <div class=\"container-fluid\">\n\n        <div class=\"row\"  style=\"margin-top: 20px;\">\n            <div class=\"col-md-12\">\n                <div class=\"card\">\n                    <div class=\"card-header card-header-tabs\" data-background-color=\"blue\">\n                        <div class=\"nav-tabs-navigation\">\n                            <div class=\"nav-tabs-wrapper\">\n                                <ul class=\"nav nav-tabs\" data-tabs=\"tabs\">\n                                    <li class=\"active\">\n                                        <a href=\"#token\" data-toggle=\"tab\">\n                                            <img src=\"/assets/img/B9_color.png\" style=\"width: 25px; \"/> BT9COIN\n                                            <div class=\"ripple-container\"></div>\n                                        </a>\n                                    </li>\n                                    <li class=\"\" (click)=\"loadBTCPending()\">\n                                        <a href=\"#btc\" data-toggle=\"tab\">\n                                            <img src=\"/assets/img/BTC.png\" style=\"width: 25px; \"/> Bitcoin\n                                            <div class=\"ripple-container\"></div>\n                                        </a>\n                                    </li>\n                                    <li class=\"\" (click)=\"loadETHPending()\">\n                                        <a href=\"#eth\" data-toggle=\"tab\">\n                                            <img src=\"/assets/img/ethereum.png\" style=\"width: 25px; \"/> Ethereum\n                                            <div class=\"ripple-container\"></div>\n                                        </a>\n                                    </li>\n                                </ul>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"card-content\">\n                        <div class=\"tab-content\">\n                            <div class=\"tab-pane active\" id=\"token\">\n                                <div class=\"withdraw-info\">\n                                    <h4 class=\"card-title\"><b>Withdraw Info</b></h4>\n                                    <div class=\"row\">\n                                        <div class=\"col-md-3\">\n                                            <label>Total Wallet Balance</label>\n                                        </div>\n                                        <div class=\"col-md-9\">\n                                            {{bt9info.total_balance | number:'1.8-8'}} BT9\n                                        </div>\n                                    </div>\n                                    <div class=\"row\">\n                                        <div class=\"col-md-3\">\n                                            <label>Total Withdraws</label>\n                                        </div>\n                                        <div class=\"col-md-9\">\n                                            {{bt9info.total_number | number:'1.8-8'}}\n                                        </div>\n                                    </div>\n                                    <div class=\"row\">\n                                        <div class=\"col-md-3\">\n                                            <label>Total Withdraw Amounts</label>\n                                        </div>\n                                        <div class=\"col-md-9\">\n                                            {{bt9info.total_amount | number:'1.8-8'}} BT9\n                                        </div>\n                                    </div>\n                                    <div class=\"row\">\n                                        <div class=\"col-md-3\">\n                                            <label>Total Fees</label>\n                                        </div>\n                                        <div class=\"col-md-9\">\n                                            {{bt9info.total_fee | number:'1.8-8'}} BT9\n                                        </div>\n                                    </div>\n                                    <!--<div class=\"row\">-->\n                                        <!--<div class=\"col-md-3\">-->\n                                            <!--<label>Estimated Fees</label>-->\n                                        <!--</div>-->\n                                        <!--<div class=\"col-md-9\">-->\n                                            <!--{{bt9info.estimated_fee | number:'1.8-8'}} BT9-->\n                                        <!--</div>-->\n                                        <!--&lt;!&ndash;<div class=\"col-md-9\">&ndash;&gt;-->\n                                            <!--&lt;!&ndash;<label>Set Priority: </label>&ndash;&gt;-->\n                                        <!--&lt;!&ndash;</div>&ndash;&gt;-->\n                                        <!--&lt;!&ndash;<div class=\"col-md-2\">&ndash;&gt;-->\n                                            <!--&lt;!&ndash;<div class=\"form-group\" style=\"margin-top: 0px;\">&ndash;&gt;-->\n                                                <!--&lt;!&ndash;<select class=\"form-control\" name=\"priority\" [(ngModel)]=\"bt9info.priority\">&ndash;&gt;-->\n                                                    <!--&lt;!&ndash;<option value=\"0\">HIGH</option>&ndash;&gt;-->\n                                                    <!--&lt;!&ndash;<option value=\"1\">MEDIUM</option>&ndash;&gt;-->\n                                                    <!--&lt;!&ndash;<option value=\"2\">LOW</option>&ndash;&gt;-->\n                                                <!--&lt;!&ndash;</select>&ndash;&gt;-->\n                                            <!--&lt;!&ndash;</div>&ndash;&gt;-->\n                                        <!--&lt;!&ndash;</div>&ndash;&gt;-->\n\n                                    <!--</div>-->\n                                    <!--<div class=\"row\">-->\n                                        <!--<div class=\"col-md-3\">-->\n                                            <!--<label>Fee Profits</label>-->\n                                        <!--</div>-->\n                                        <!--<div class=\"col-md-9\">-->\n                                            <!--{{bt9info.profit_fee | number:'1.8-8'}} BT9-->\n                                        <!--</div>-->\n                                    <!--</div>-->\n                                    <div class=\"row\">\n                                        <div class=\"col-md-3\">\n                                        </div>\n                                        <div class=\"col-md-9\">\n                                            <!--<button type=\"button\" class=\"btn btn-danger\" (click)=\"estimateBT9Fee()\">-->\n                                                <!--Check-->\n                                            <!--</button>-->\n                                            <button type=\"button\" class=\"btn btn-info\" (click)=\"withdrawBT9()\">\n                                                Withdraw\n                                            </button>\n                                        </div>\n                                    </div>\n                                </div>\n\n                                <div class=\"pending-list\">\n                                    <h4 class=\"card-title\"><b>Pending List</b></h4>\n                                    <div class=\"material-datatables table-responsive\">\n                                        <table class=\"table table-striped table-no-bordered table-hover\"\n                                               cellspacing=\"0\" width=\"100%\" style=\"width:100%\">\n                                            <thead>\n                                            <tr>\n                                                <th>#</th>\n                                                <th style=\"width: 200px;\">ADDRESS</th>\n                                                <th>AMOUNT</th>\n                                                <th>FEE</th>\n                                                <th>WITHDRAW AMOUNT</th>\n                                                <th>CREATED AT</th>\n                                            </tr>\n                                            </thead>\n                                            <tbody>\n                                            <tr *ngFor=\"let tx of bt9Pending; let i = index\">\n                                                <td>{{i + 1}}</td>\n                                                <td>{{tx.address}}</td>\n                                                <td>{{tx.src_amount | number:'1.8-8'}}</td>\n                                                <td>{{tx.fee | number:'1.8-8'}}</td>\n                                                <td>{{tx.src_amount - tx.fee | number: '1.8-8'}}</td>\n                                                <td style=\"width: 200px;\">{{tx['date'] | date: 'yyyy-MM-dd HH:mm:ss'}}</td>\n                                            </tr>\n\n                                            <tr *ngIf=\"bt9Pending?.length == 0\">\n                                                <td class=\"text-center\" colspan=\"6\">\n                                                    No Withdraw Pending found.\n                                                </td>\n                                            </tr>\n                                            </tbody>\n                                        </table>\n                                    </div>\n                                </div>\n                            </div>\n                            <div class=\"tab-pane\" id=\"btc\">\n                                <div class=\"withdraw-info\">\n                                    <h4 class=\"card-title\"><b>Withdraw Info</b></h4>\n                                    <div class=\"row\">\n                                        <div class=\"col-md-3\">\n                                            <label>Total Wallet Balance</label>\n                                        </div>\n                                        <div class=\"col-md-9\">\n                                            {{btcinfo.total_balance | number:'1.8-8'}} BTC\n                                        </div>\n                                    </div>\n                                    <div class=\"row\">\n                                        <div class=\"col-md-3\">\n                                            <label>Total Withdraws</label>\n                                        </div>\n                                        <div class=\"col-md-9\">\n                                            {{btcinfo.total_number | number:'1.8-8'}}\n                                        </div>\n                                    </div>\n                                    <div class=\"row\">\n                                        <div class=\"col-md-3\">\n                                            <label>Total Withdraw Amounts</label>\n                                        </div>\n                                        <div class=\"col-md-9\">\n                                            {{btcinfo.total_amount | number:'1.8-8'}} BTC\n                                        </div>\n                                    </div>\n                                    <div class=\"row\">\n                                        <div class=\"col-md-3\">\n                                            <label>Total Fees</label>\n                                        </div>\n                                        <div class=\"col-md-9\">\n                                            {{btcinfo.total_fee | number:'1.8-8'}} BTC\n                                        </div>\n                                    </div>\n                                    <div class=\"row\">\n                                        <div class=\"col-md-3\">\n                                            <label>Estimated Fees</label>\n                                        </div>\n                                        <div class=\"col-md-3\">\n                                            {{btcinfo.estimated_fee | number:'1.8-8'}} BTC\n                                        </div>\n                                        <div class=\"col-md-2\">\n                                            <label>Set Priority: </label>\n                                        </div>\n                                        <div class=\"col-md-2\">\n                                            <div class=\"form-group\" style=\"margin-top: 0px;\">\n                                                <select class=\"form-control\" name=\"priority\" [(ngModel)]=\"btcinfo.priority\">\n                                                    <option value=\"0\">HIGH</option>\n                                                    <option value=\"1\">MEDIUM</option>\n                                                    <option value=\"2\">LOW</option>\n                                                </select>\n                                            </div>\n                                        </div>\n                                        <div class=\"col-md-2\">\n                                            <button type=\"button\" class=\"btn btn-danger btn-sm\" (click)=\"estimateBTCFee()\">\n                                                ESTIMATE FEE\n                                            </button>\n                                        </div>\n                                    </div>\n                                    <div class=\"row\">\n                                        <div class=\"col-md-3\">\n                                            <label>Fee Profits</label>\n                                        </div>\n                                        <div class=\"col-md-9\">\n                                            {{btcinfo.profit_fee | number:'1.8-8'}} BT9\n                                        </div>\n                                    </div>\n                                    <div class=\"row\">\n                                        <div class=\"col-md-3\">\n                                        </div>\n                                        <div class=\"col-md-9\">\n                                            <button type=\"button\" class=\"btn btn-info\" (click)=\"withdrawBTC()\">\n                                                Withdraw\n                                            </button>\n                                        </div>\n                                    </div>\n                                </div>\n\n                                <div class=\"pending-list\">\n                                    <h4 class=\"card-title\"><b>Pending List</b></h4>\n                                    <div class=\"material-datatables table-responsive\">\n                                        <table class=\"table table-striped table-no-bordered table-hover\"\n                                               cellspacing=\"0\" width=\"100%\" style=\"width:100%\">\n                                            <thead>\n                                            <tr>\n                                                <th>#</th>\n                                                <th style=\"width: 200px;\">ADDRESS</th>\n                                                <th>AMOUNT</th>\n                                                <th>FEE</th>\n                                                <th>WITHDRAW AMOUNT</th>\n                                                <th>CREATED AT</th>\n                                            </tr>\n                                            </thead>\n                                            <tbody>\n                                            <tr *ngFor=\"let tx of btcPending; let i = index\">\n                                                <td>{{i + 1}}</td>\n                                                <td>{{tx.address}}</td>\n                                                <td>{{tx.src_amount | number:'1.8-8'}}</td>\n                                                <td>{{tx.fee | number:'1.8-8'}}</td>\n                                                <td>{{(tx.src_amount - tx.fee) | number: '1.8-8'}}</td>\n                                                <td style=\"width: 200px;\">{{tx['date'] | date: 'yyyy-MM-dd HH:mm:ss'}}</td>\n                                            </tr>\n\n                                            <tr *ngIf=\"btcPending?.length == 0\">\n                                                <td class=\"text-center\" colspan=\"6\">\n                                                    No Withdraw Pending found.\n                                                </td>\n                                            </tr>\n                                            </tbody>\n                                        </table>\n                                    </div>\n                                </div>\n                            </div>\n                            <div class=\"tab-pane\" id=\"eth\">\n                                <div>\n                                    <h4 class=\"card-title\"><b>Pending List</b></h4>\n                                    <div class=\"material-datatables table-responsive\">\n                                        <table class=\"table table-striped table-no-bordered table-hover\"\n                                               cellspacing=\"0\" width=\"100%\" style=\"width:100%\">\n                                            <thead>\n                                            <tr>\n                                                <th>#</th>\n                                                <th style=\"width: 200px;\">ADDRESS</th>\n                                                <th>AMOUNT</th>\n                                                <th>FEE</th>\n                                                <th>WITHDRAW AMOUNT</th>\n                                                <th>CREATED AT</th>\n                                            </tr>\n                                            </thead>\n                                            <tbody>\n                                            <tr *ngFor=\"let tx of ethPending; let i = index\">\n                                                <td>{{i + 1}}</td>\n                                                <td>{{tx.address}}</td>\n                                                <td>{{tx.src_amount | number:'1.8-8'}}</td>\n                                                <td>{{tx.fee | number:'1.8-8'}}</td>\n                                                <td>{{tx.src_amount - tx.fee | number: '1.8-8'}}</td>\n                                                <td style=\"width: 200px;\">{{tx['date'] | date: 'yyyy-MM-dd HH:mm:ss'}}</td>\n                                            </tr>\n\n                                            <tr *ngIf=\"ethPending?.length == 0\">\n                                                <td class=\"text-center\" colspan=\"6\">\n                                                    No Withdraw Pending found.\n                                                </td>\n                                            </tr>\n                                            </tbody>\n                                        </table>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/admin/withdraw/withdraw.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".pending-list {\n  margin-top: 10px; }\n\n.withdraw-info {\n  font-size: 15px;\n  line-height: 2em; }\n  .withdraw-info label {\n    font-size: 15px;\n    line-height: 2em; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/admin/withdraw/withdraw.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminWithdrawComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_api_service__ = __webpack_require__("../../../../../src/app/services/api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_settings_settings_service__ = __webpack_require__("../../../../../src/app/services/settings/settings.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_balance_service__ = __webpack_require__("../../../../../src/app/services/balance.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_notifications_service__ = __webpack_require__("../../../../../src/app/services/notifications.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AdminWithdrawComponent = (function () {
    function AdminWithdrawComponent(api, settings, balance, notify) {
        this.api = api;
        this.settings = settings;
        this.balance = balance;
        this.notify = notify;
        this.bt9Pending = [];
        this.btcPending = [];
        this.ethPending = [];
        this.bt9info = {
            total_balance: 0,
            estimated_fee: 0,
            profit_fee: 0,
            priority: 0,
            hex: ''
        };
        this.btcinfo = {
            total_balance: 0,
            estimated_fee: 0,
            profit_fee: 0,
            priority: 0,
            hex: ''
        };
    }
    AdminWithdrawComponent.prototype.ngOnInit = function () {
        this.loadBT9Pending();
    };
    AdminWithdrawComponent.prototype.loadBT9Pending = function () {
        var _this = this;
        this.api.getWithdrawPending('BT9').subscribe(function (res) {
            if (res.success) {
                _this.bt9Pending = res.data;
                _this.bt9info.total_number = res.data.length;
                _this.bt9info.total_amount = 0;
                _this.bt9info.total_fee = 0;
                for (var i = 0; i < _this.bt9Pending.length; i++) {
                    _this.bt9info.total_amount += Number(_this.bt9Pending[i].src_amount - _this.bt9Pending[i].fee);
                    _this.bt9info.total_fee += Number(_this.bt9Pending[i].fee);
                }
            }
        });
        this.api.getWalletTotalBalance('BT9').subscribe(function (res) {
            if (res.success) {
                _this.bt9info.total_balance = res.balance;
            }
        });
    };
    AdminWithdrawComponent.prototype.estimateBT9Fee = function () {
        var _this = this;
        this.settings.loading = true;
        this.api.estimateFee('BT9', {
            priority: this.bt9info.priority
        }).subscribe(function (res) {
            _this.settings.loading = false;
            if (res.success) {
                // this.bt9info.estimated_fee = res.fee;
                // this.bt9info.profit_fee = this.bt9info.total_fee - this.bt9info.estimated_fee;
                _this.bt9info.hex = res.hex;
                _this.notify.showNotification('You can withdraw now, Please click Withdraw Button', 'top', 'center', 'success');
            }
            else {
                _this.notify.showNotification(res.error, 'top', 'center', 'warning');
            }
        }, function (err) {
            _this.settings.loading = false;
            _this.notify.showNotification('Please try again.', 'top', 'center', 'danger');
        });
    };
    AdminWithdrawComponent.prototype.withdrawBT9 = function () {
        // if (this.bt9info.hex == '') {
        //     this.notify.showNotification('You have to check.', 'top', 'center', 'warning');
        //     return;
        // }
        var _this = this;
        this.settings.loading = true;
        this.api.withdrawPending('BT9', {
            // hex: this.bt9info.hex,
            // fee: this.bt9info.estimated_fee
            fee: 0
        }).subscribe(function (res) {
            _this.settings.loading = false;
            if (res.success) {
                _this.notify.showNotification('Withdraw Successfully', 'top', 'center', 'success');
                _this.loadBT9Pending();
            }
            else {
                _this.notify.showNotification(res.error, 'top', 'center', 'warning');
            }
        }, function (err) {
            _this.settings.loading = false;
            _this.notify.showNotification('Please try again', 'top', 'center', 'warning');
        });
    };
    AdminWithdrawComponent.prototype.loadBTCPending = function () {
        var _this = this;
        this.api.getWithdrawPending('BTC').subscribe(function (res) {
            if (res.success) {
                _this.btcPending = res.data;
                _this.btcinfo.total_number = res.data.length;
                _this.btcinfo.total_amount = 0;
                _this.btcinfo.total_fee = 0;
                for (var i = 0; i < _this.btcPending.length; i++) {
                    _this.btcinfo.total_amount += Number(_this.btcPending[i].src_amount - _this.btcPending[i].fee);
                    _this.btcinfo.total_fee += Number(_this.btcPending[i].fee);
                }
            }
        });
        this.api.getWalletTotalBalance('BTC').subscribe(function (res) {
            if (res.success) {
                _this.btcinfo.total_balance = res.balance;
            }
        });
    };
    AdminWithdrawComponent.prototype.estimateBTCFee = function () {
        var _this = this;
        this.settings.loading = true;
        this.api.estimateFee('BTC', {
            priority: this.btcinfo.priority
        }).subscribe(function (res) {
            _this.settings.loading = false;
            if (res.success) {
                _this.btcinfo.estimated_fee = res.fee;
                _this.btcinfo.profit_fee = _this.btcinfo.total_fee - _this.btcinfo.estimated_fee;
                _this.btcinfo.hex = res.hex;
            }
            else {
                _this.notify.showNotification(res.error, 'top', 'center', 'warning');
            }
        }, function (err) {
            _this.settings.loading = false;
            _this.notify.showNotification('Please try again.', 'top', 'center', 'danger');
        });
    };
    AdminWithdrawComponent.prototype.withdrawBTC = function () {
        var _this = this;
        if (this.btcinfo.hex == '') {
            this.notify.showNotification('You have to estimate fee', 'top', 'center', 'warning');
            return;
        }
        this.settings.loading = true;
        this.api.withdrawPending('BTC', {
            hex: this.btcinfo.hex,
            fee: this.btcinfo.estimated_fee
        }).subscribe(function (res) {
            _this.settings.loading = false;
            if (res.success) {
                _this.notify.showNotification('Withdraw Successfully', 'top', 'center', 'success');
                _this.loadBT9Pending();
            }
            else {
                _this.notify.showNotification(res.error, 'top', 'center', 'warning');
            }
        }, function (err) {
            _this.settings.loading = false;
            _this.notify.showNotification('Please try again', 'top', 'center', 'warning');
        });
    };
    AdminWithdrawComponent.prototype.loadETHPending = function () {
        var _this = this;
        this.api.getWithdrawPending('ETH').subscribe(function (res) {
            if (res.success) {
                _this.ethPending = res.data;
            }
        });
    };
    AdminWithdrawComponent.prototype.ngAfterViewInit = function () {
    };
    AdminWithdrawComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-admin-withdraw',
            template: __webpack_require__("../../../../../src/app/admin/withdraw/withdraw.component.html"),
            styles: [__webpack_require__("../../../../../src/app/admin/withdraw/withdraw.component.scss")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_api_service__["a" /* Api */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_api_service__["a" /* Api */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__services_settings_settings_service__["a" /* SettingsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_settings_settings_service__["a" /* SettingsService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__services_balance_service__["a" /* BalanceService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services_balance_service__["a" /* BalanceService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_4__services_notifications_service__["a" /* Notifications */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__services_notifications_service__["a" /* Notifications */]) === "function" && _d || Object])
    ], AdminWithdrawComponent);
    return AdminWithdrawComponent;
    var _a, _b, _c, _d;
}());

//# sourceMappingURL=withdraw.component.js.map

/***/ })

});
//# sourceMappingURL=admin.module.chunk.js.map