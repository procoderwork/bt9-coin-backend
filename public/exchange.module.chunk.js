webpackJsonp(["exchange.module"],{

/***/ "../../../../../src/app/pages/exchange/btc/exchangebtc.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\n    <div class=\"container-fluid\" *ngIf=\"settings.getSysSetting('enable_btc_exchange') == 0\">\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n                <div class=\"alert alert-info\">\n                    BITCOIN Exchange is disabled.\n                </div>\n            </div>\n        </div>\n    </div>\n    <div class=\"container-fluid\" *ngIf=\"settings.getSysSetting('enable_btc_exchange') == 1\">\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n                <div class=\"card\">\n                    <div class=\"card-header card-header-icon\" data-background-color=\"blue\">\n                        <i class=\"material-icons\">timeline</i>\n                    </div>\n                    <div class=\"card-content\">\n                        <h4 class=\"card-title\">Market Price<br>\n                            <small>(BT9/BTC)</small>\n                        </h4>\n                    </div>\n                    <div class=\"exchange-info\" style=\"padding-left: 15px; padding-right: 15px;\">\n                        <div class=\"row\">\n                            <div class=\"col-md-1\">\n                                LAST\n                            </div>\n                            <div class=\"text-success col-md-3\" >\n                                {{exchangeInfo.lastPrice | number:'1.8-8'}} BTC (${{(exchangeInfo.lastPrice * btc_usd) | number:'1.2-2'}} )\n                            </div>\n                            <div class=\"col-md-1\">\n                                LAST BID\n                            </div>\n                            <div class=\"text-success col-md-3\" >\n                                {{exchangeInfo.lastBid | number:'1.8-8'}} BTC (${{(exchangeInfo.lastBid * btc_usd) | number:'1.2-2'}} )\n                            </div>\n                            <div class=\"col-md-1\">\n                                24H HIGH\n                            </div>\n                            <div class=\"text-success col-md-3\" >\n                                {{exchangeInfo.high | number:'1.8-8'}} BTC (${{(exchangeInfo.high * btc_usd) | number:'1.2-2'}} )\n                            </div>\n                            <div class=\"col-md-1\">\n                                24H VOL\n                            </div>\n                            <div class=\"text-success col-md-3\" >\n                                {{exchangeInfo.volume | number:'1.8-8'}} BTC (${{(exchangeInfo.volume * btc_usd) | number:'1.2-2'}} )\n                            </div>\n                            <div class=\"col-md-1\">\n                                LAST ASK\n                            </div>\n                            <div class=\"text-success col-md-3\" >\n                                {{exchangeInfo.lastAsk | number:'1.8-8'}} BTC (${{(exchangeInfo.lastAsk * btc_usd) | number:'1.2-2'}} )\n                            </div>\n                            <div class=\"col-md-1\">\n                                24H LOW\n                            </div>\n                            <div class=\"text-success col-md-3\" >\n                                {{exchangeInfo.low | number:'1.8-8'}} BTC (${{(exchangeInfo.low * btc_usd) | number:'1.2-2'}} )\n                            </div>\n                        </div>\n                    </div>\n                    <div id=\"marketchart\" [chart]=\"stockChart\" class=\"ct-chart\"></div>\n                </div>\n            </div>\n        </div>\n        <div class=\"row\">\n            <div class=\"col-md-6\">\n                <div class=\"card\">\n                    <div class=\"card-header card-header-icon\" data-background-color=\"green\">\n                        <i class=\"fa fa-plus\"></i>\n                    </div>\n                    <form class=\"form-horizontal\">\n                        <div class=\"card-content\">\n                            <h4 class=\"card-title\">Buy BT9</h4>\n                            <div class=\"text-center\">\n                                Available balance: <b>{{settings.getUserSetting('btc_balance') | number:'1.8-8'}}</b>\n                                BTC\n                            </div>\n                            <div class=\"row\">\n                                <label class=\"col-md-2 label-on-left\">Units</label>\n                                <div class=\"col-md-10\">\n                                    <div class=\"form-group label-floating is-empty\">\n                                        <label class=\"control-label\"></label>\n                                        <input type=\"number\" class=\"form-control\" name=\"amount\" step=\"0.00001\"\n                                               [(ngModel)]=\"buy.amount\">\n                                        <span class=\"help-block\">BT9</span>\n                                    </div>\n                                </div>\n                            </div>\n                            <div class=\"row\">\n                                <label class=\"col-md-2 label-on-left\">Bid</label>\n                                <div class=\"col-md-10\">\n                                    <div class=\"form-group label-floating is-empty\">\n                                        <label class=\"control-label\"></label>\n                                        <input type=\"number\" class=\"form-control\" name=\"rate\" [(ngModel)]=\"buy.rate\"\n                                               step=\"0.00001\">\n                                        <span class=\"help-block\">BTC/BT9</span>\n                                    </div>\n                                </div>\n                            </div>\n                            <div class=\"row\">\n                                <label class=\"col-md-2 label-on-left\">Total</label>\n                                <div class=\"col-md-10\">\n                                    <div class=\"form-group label-floating is-empty\">\n                                        <label class=\"control-label\"></label>\n                                        <input type=\"text\" class=\"form-control\" name=\"total\" readonly\n                                               value=\"{{(buy.amount * buy.rate ? buy.amount * buy.rate : 0) | number: '1.8-8'}}\">\n                                        <span class=\"help-block\">BTC</span>\n                                    </div>\n                                </div>\n                            </div>\n                            <div class=\"row\">\n                                <label class=\"col-md-2 label-on-left\">Fee</label>\n                                <label class=\"col-md-10 label-on-right\">{{settings.getSysSetting('exchange_buy_fee') |\n                                    number:'1.0-2'}}%</label>\n                            </div>\n                            <div class=\"row\">\n                                <div class=\"col-md-12 \">\n                                    <button type=\"button\" class=\"btn btn-info pull-right\" (click)=\"newBuyOrder()\"\n                                            *ngIf=\"!buyloading\"><i class=\"fa fa-plus\"></i>\n                                        Buy BT9Coin\n                                    </button>\n                                </div>\n                                <loader *ngIf=\"buyloading\"></loader>\n                            </div>\n                        </div>\n                    </form>\n                </div>\n            </div>\n            <div class=\"col-md-6\">\n                <div class=\"card\">\n                    <div class=\"card-header card-header-icon\" data-background-color=\"rose\">\n                        <i class=\"fa fa-minus\"></i>\n                    </div>\n\n                    <form class=\"form-horizontal\">\n                        <div class=\"card-content\">\n                            <h4 class=\"card-title\">Sell BT9</h4>\n                            <div class=\"text-center\">\n                                Available balance: <b>{{settings.getUserSetting('token_balance') | number:'1.8-8'}}</b>\n                                BT9\n                            </div>\n                            <div class=\"row\">\n                                <label class=\"col-md-2 label-on-left\">Units</label>\n                                <div class=\"col-md-10\">\n                                    <div class=\"form-group label-floating is-empty\">\n                                        <label class=\"control-label\"></label>\n                                        <input type=\"number\" class=\"form-control\" name=\"amount\" step=\"0.00001\"\n                                               [(ngModel)]=\"sell.amount\">\n                                        <span class=\"help-block\">BT9</span>\n                                    </div>\n                                </div>\n                            </div>\n                            <div class=\"row\">\n                                <label class=\"col-md-2 label-on-left\">Ask</label>\n                                <div class=\"col-md-10\">\n                                    <div class=\"form-group label-floating is-empty\">\n                                        <label class=\"control-label\"></label>\n                                        <input type=\"number\" class=\"form-control\" name=\"rate\" [(ngModel)]=\"sell.rate\"\n                                               step=\"0.00001\">\n                                        <span class=\"help-block\">BTC/BT9</span>\n                                    </div>\n                                </div>\n                            </div>\n                            <div class=\"row\">\n                                <label class=\"col-md-2 label-on-left\">Total</label>\n                                <div class=\"col-md-10\">\n                                    <div class=\"form-group label-floating is-empty\">\n                                        <label class=\"control-label\"></label>\n                                        <input type=\"text\" class=\"form-control\" name=\"total\" readonly\n                                               value=\"{{(sell.amount * sell.rate ? sell.amount * sell.rate : 0) | number: '1.8-8'}}\">\n                                        <span class=\"help-block\">BTC</span>\n                                    </div>\n                                </div>\n                            </div>\n                            <div class=\"row\">\n                                <label class=\"col-md-2 label-on-left\">Fee</label>\n                                <label class=\"col-md-10 label-on-right\">{{settings.getSysSetting('exchange_sell_fee') |\n                                    number:'1.0-2'}}%</label>\n                            </div>\n                            <div class=\"row\">\n                                <div class=\"col-md-12 \">\n                                    <button type=\"button\" class=\"btn btn-info pull-right\" (click)=\"newSellOrder()\"\n                                            *ngIf=\"!sellloading\"><i class=\"fa fa-minus\"></i>\n                                        Sell BT9Coin\n                                    </button>\n                                    <loader *ngIf=\"sellloading\"></loader>\n                                </div>\n                            </div>\n                        </div>\n                    </form>\n                </div>\n            </div>\n        </div>\n        <div class=\"row\">\n            <div class=\"col-md-6\">\n                <div class=\"card \">\n                    <div class=\"card-header card-header-icon\" data-background-color=\"default\">\n                        <i class=\"fa fa-bars\"></i>\n                    </div>\n                    <div class=\"card-content\">\n                        <div class=\"card-title\">\n                            <h4>Buying Orders</h4>\n                        </div>\n                        <div class=\"table-responsive height-500\">\n                            <table class=\"table color-table success-table table-hover table-right\">\n                                <thead>\n                                <tr>\n                                    <th>Total(BTC)</th>\n                                    <th>Size(BT9)</th>\n                                    <th>Bid(BTC)</th>\n                                </tr>\n                                </thead>\n                                <tbody>\n                                <tr *ngFor=\"let order of buyOrders; let i = index;\" (click)=\"onClickBuyOrder(i)\">\n                                    <td class=\"text-right\">{{order.amount * order.rate | number:'1.8-8'}}</td>\n                                    <td class=\"text-right\">{{order.amount | number:'1.8-8'}}</td>\n                                    <td class=\"text-right\">{{order.rate | number:'1.8-8'}}</td>\n                                </tr>\n                                <tr *ngIf=\"buyOrders?.length == 0\">\n                                    <td style=\"text-align: center !important;\" colspan=\"3\">\n                                        No Sell Orders\n                                    </td>\n                                </tr>\n                                </tbody>\n                            </table>\n                        </div>\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-md-6\">\n                <div class=\"card\">\n                    <div class=\"card-header card-header-icon\" data-background-color=\"default\">\n                        <i class=\"fa fa-bars\"></i>\n                    </div>\n                    <div class=\"card-content\">\n                        <div class=\"card-title\">\n                            <h4>Selling Orders</h4>\n                        </div>\n\n                        <div class=\"table-responsive height-500\">\n                            <table class=\"table color-table success-table table-hover table-right\">\n                                <thead>\n                                <tr>\n                                    <th>Total(BTC)</th>\n                                    <th>Size(BT9)</th>\n                                    <th>Bid(BTC)</th>\n                                </tr>\n                                </thead>\n                                <tbody>\n                                <tr *ngFor=\"let order of sellOrders; let i = index;\" (click)=\"onClickSellOrder(i)\">\n                                    <td class=\"text-right\">{{order.amount * order.rate | number:'1.8-8'}}</td>\n                                    <td class=\"text-right\">{{order.amount | number:'1.8-8'}}</td>\n                                    <td class=\"text-right\">{{order.rate | number:'1.8-8'}}</td>\n                                </tr>\n                                <tr *ngIf=\"sellOrders?.length == 0\">\n                                    <td style=\"text-align: center !important;\" colspan=\"3\">\n                                        No Buy Orders\n                                    </td>\n                                </tr>\n                                </tbody>\n                            </table>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n                <div class=\"card \">\n                    <div class=\"card-header card-header-icon\" data-background-color=\"green\">\n                        <i class=\"fa fa-history\"></i>\n                    </div>\n\n\n                    <div class=\"card-content\">\n                        <div class=\"card-title\">\n                            <h4>Market History</h4>\n                        </div>\n                        <ul class=\"nav nav-pills nav-menu-security nav-pills-warning\">\n                            <li class=\"active\">\n                                <a data-toggle=\"tab\" href=\"#open\">Open Orders</a>\n                            </li>\n                            <li>\n                                <a data-toggle=\"tab\" href=\"#past\">History</a>\n                            </li>\n                        </ul>\n                        <div class=\"tab-content\">\n                            <div class=\"tab-pane active\" id=\"open\">\n                                <div class=\"table-responsive height-500\">\n                                    <table class=\"table color-table success-table table-hover table-center\">\n                                        <thead>\n                                        <tr>\n                                            <th>Type</th>\n                                            <th style=\"width: 200px;\">Time</th>\n                                            <th>Amount</th>\n                                            <th>Ask/Bid</th>\n                                            <th>Total Price</th>\n                                            <th class=\"text-center\">Action</th>\n                                        </tr>\n                                        </thead>\n                                        <tbody>\n                                        <tr *ngFor=\"let order of openOrders\">\n                                            <td class=\"text-center\"><label class=\"label label-sm\"\n                                                                           [ngClass]=\"{'label-success': order.type == 0, 'label-info': order.type == 1}\">{{order.type\n                                                == 0 ? 'BUY' : 'SELL'}}</label></td>\n                                            <td style=\"width: 200px;\" class=\"text-center\">{{order.created_at |\n                                                date:'yyyy-MM-dd H:m:s'}}\n                                            </td>\n                                            <td class=\"text-center\">{{order.amount | number:'1.8-8'}}</td>\n                                            <td class=\"text-center\">{{order.rate | number:'1.8-8'}}</td>\n                                            <td class=\"text-center\">{{order.amount * order.rate | number:'1.8-8'}}</td>\n                                            <td class=\"text-center\">\n                                                <button type=\"button\" class=\"btn btn-sm btn-danger\"\n                                                        (click)=\"cancelOrder(order.id)\">\n                                                    <i class=\"fa fa-close\"></i></button>\n                                            </td>\n                                        </tr>\n                                        <tr *ngIf=\"openOrders?.length == 0\">\n                                            <td class=\"text-center\" colspan=\"6\">\n                                                No Open Orders\n                                            </td>\n                                        </tr>\n                                        </tbody>\n                                    </table>\n                                </div>\n                            </div>\n                            <div class=\"tab-pane\" id=\"past\">\n                                <div class=\"table-responsive height-500\">\n                                    <table class=\"table color-table success-table table-hover table-center\">\n                                        <thead>\n                                        <tr>\n                                            <th>Type</th>\n                                            <th style=\"width: 200px;\">Time</th>\n                                            <th>Units</th>\n                                            <th>Bid/Ask</th>\n                                            <th>Total Price</th>\n                                        </tr>\n                                        </thead>\n                                        <tbody>\n                                        <tr *ngFor=\"let order of pastOrders\">\n                                            <td><label class=\"label label-sm\"\n                                                       [ngClass]=\"{'label-success': order.exchange_type == 0, 'label-info': order.exchange_type == 1}\">\n                                                {{order.exchange_type == 0 ? 'BUY' : 'SELL'}}</label>\n                                            </td>\n                                            <td style=\"width: 200px;\">{{order.created_at | date:'yyyy-MM-dd H:m:s'}}\n                                            </td>\n                                            <td>{{order.exchange_type == 0 ? order.dest_amount : order.src_amount |\n                                                number:'1.8-8'}}\n                                            </td>\n                                            <td>{{order.rate | number:'1.2-2'}}</td>\n                                            <td>{{order.exchange_type == 0 ? order.src_amount : order.dest_amount |\n                                                number:'1.8-8'}}\n                                            </td>\n                                        </tr>\n                                        <tr *ngIf=\"pastOrders?.length == 0\">\n                                            <td class=\"text-center\" colspan=\"5\">\n                                                No Orders\n                                            </td>\n                                        </tr>\n                                        </tbody>\n                                    </table>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n\n    </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/pages/exchange/btc/exchangebtc.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".height-500 {\n  min-height: 450px;\n  max-height: 450px;\n  overflow-y: auto; }\n  .height-500 .table-responsive {\n    overflow-x: auto; }\n\n.table-right th, .table-right td {\n  text-align: right !important; }\n\n.table-center th, .table-center td {\n  text-align: center !important; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/pages/exchange/btc/exchangebtc.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ExchangeBTCComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_chartist__ = __webpack_require__("../../../../chartist/dist/chartist.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_chartist___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_chartist__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_api_service__ = __webpack_require__("../../../../../src/app/services/api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_settings_settings_service__ = __webpack_require__("../../../../../src/app/services/settings/settings.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_balance_service__ = __webpack_require__("../../../../../src/app/services/balance.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_notifications_service__ = __webpack_require__("../../../../../src/app/services/notifications.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_angular_highcharts__ = __webpack_require__("../../../../angular-highcharts/angular-highcharts.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var ExchangeBTCComponent = (function () {
    function ExchangeBTCComponent(api, settings, balance, notify) {
        this.api = api;
        this.settings = settings;
        this.balance = balance;
        this.notify = notify;
        this.icoChartData = [];
        this.buy = {};
        this.sell = {};
        this.buyOrders = [];
        this.sellOrders = [];
        this.openOrders = [];
        this.pastOrders = [];
        this.buyloading = false;
        this.sellloading = false;
        this.exchangeInfo = {};
        this.btc_usd = 0;
        this.usd_btc = 0;
    }
    ExchangeBTCComponent.prototype.startAnimationForLineChart = function (chart) {
        var seq, delays, durations;
        seq = 0;
        delays = 80;
        durations = 500;
        chart.on('draw', function (data) {
            if (data.type === 'line' || data.type === 'area') {
                data.element.animate({
                    d: {
                        begin: 600,
                        dur: 700,
                        from: data.path.clone().scale(1, 0).translate(0, data.chartRect.height()).stringify(),
                        to: data.path.clone().stringify(),
                        easing: __WEBPACK_IMPORTED_MODULE_1_chartist__["Svg"].Easing.easeOutQuint
                    }
                });
            }
            else if (data.type === 'point') {
                seq++;
                data.element.animate({
                    opacity: {
                        begin: seq * delays,
                        dur: durations,
                        from: 0,
                        to: 1,
                        easing: 'ease'
                    }
                });
            }
        });
        seq = 0;
    };
    ExchangeBTCComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.getBuyOrders();
        this.getSellOrders();
        this.getPastOrders();
        this.getOpenOrders();
        this.api.getCoinRate({}).subscribe(function (res) {
            if (res.success) {
                _this.btc_usd = res.btc_rate;
                _this.usd_btc = 1 / Number(res.btc_rate);
            }
        }, function (err) {
        });
        // this.stockChart = new StockChart({});
        this.api.getExchangeStatistics('BTC', {}).subscribe(function (res) {
            if (res.success) {
                _this.exchangeInfo = res.data;
                if (_this.exchangeInfo.lastPrice > 0) {
                    _this.buy.rate = Number(_this.exchangeInfo.lastPrice);
                    _this.sell.rate = Number(_this.exchangeInfo.lastPrice);
                }
            }
        });
        var _parent = this;
        this.api.getExchangeHistory('BTC', {}).subscribe(function (res) {
            if (res.success) {
                // split the data set into ohlc and volume
                var ohlc = [], volume = [], dataLength = res.data.length, 
                // set the allowed units for data grouping
                groupingUnits = [[
                        'minute',
                        [1, 2, 3, 4, 6] // allowed multiples
                    ], [
                        'hour',
                        [1, 2, 3, 4, 5, 6, 7]
                    ]], i = 0;
                for (i; i < res.data.length; i += 1) {
                    ohlc.push([
                        Number(res.data[i].date),
                        Number(res.data[i].open),
                        Number(res.data[i].high),
                        Number(res.data[i].low),
                        Number(res.data[i].close) // close
                    ]);
                    volume.push([
                        Number(res.data[i].date),
                        Number(res.data[i].volume) // the volume
                    ]);
                }
                // create the chart
                _parent.stockChart = new __WEBPACK_IMPORTED_MODULE_6_angular_highcharts__["c" /* StockChart */]({
                    rangeSelector: {
                        selected: 0,
                        buttons: [{
                                type: 'hour',
                                count: 6,
                                text: '6h',
                            }, {
                                type: 'hour',
                                count: 12,
                                text: '12h'
                            }, {
                                type: 'hour',
                                count: 24,
                                text: '24h'
                            }, {
                                type: 'day',
                                count: 3,
                                text: '3d'
                            }, {
                                type: 'day',
                                count: 7,
                                text: '1w'
                            }, {
                                type: 'all',
                                text: 'All'
                            }]
                    },
                    title: {
                        text: 'Market History'
                    },
                    yAxis: [{
                            labels: {
                                align: 'right',
                                x: -3
                            },
                            title: {
                                text: 'Price'
                            },
                            height: '60%',
                            lineWidth: 2,
                            resize: {
                                enabled: true
                            }
                        }, {
                            labels: {
                                align: 'right',
                                x: -3
                            },
                            title: {
                                text: 'Volume'
                            },
                            top: '65%',
                            height: '35%',
                            offset: 0,
                            lineWidth: 2
                        }],
                    tooltip: {
                        split: true
                    },
                    series: [{
                            type: 'candlestick',
                            name: 'BTC Exchange Price',
                            data: ohlc,
                            dataGrouping: {
                                units: groupingUnits
                            }
                        }, {
                            type: 'column',
                            name: 'Volume(BTC)',
                            data: volume,
                            yAxis: 1,
                            dataGrouping: {
                                units: groupingUnits
                            }
                        }]
                });
            }
        });
        // var theme = {
        //     //    colors: ['#2b908f', '#90ee7e', '#f45b5b', '#7798BF', '#aaeeee', '#ff0066', '#eeaaee',
        //     //       '#55BF3B', '#DF5353', '#7798BF', '#aaeeee'],
        //     chart: {
        //         backgroundColor: {
        //             linearGradient: { x1: 0, y1: 0, x2: 1, y2: 1 },
        //             stops: [
        //                 [0, '#2a2a2b'],
        //                 [1, '#3e3e40']
        //             ]
        //         },
        //         style: {
        //             fontFamily: '\'Unica One\', sans-serif'
        //         },
        //         plotBorderColor: '#606063'
        //     },
        //     title: {
        //         style: {
        //             color: '#E0E0E3',
        //             textTransform: 'uppercase',
        //             fontSize: '20px'
        //         }
        //     },
        //     subtitle: {
        //         style: {
        //             color: '#E0E0E3',
        //             textTransform: 'uppercase'
        //         }
        //     },
        //     xAxis: {
        //         gridLineColor: '#707073',
        //         labels: {
        //             style: {
        //                 color: '#E0E0E3'
        //             }
        //         },
        //         lineColor: '#707073',
        //         minorGridLineColor: '#505053',
        //         tickColor: '#707073',
        //         title: {
        //             style: {
        //                 color: '#A0A0A3'
        //
        //             }
        //         }
        //     },
        //     yAxis: {
        //         gridLineColor: '#707073',
        //         labels: {
        //             style: {
        //                 color: '#E0E0E3'
        //             }
        //         },
        //         lineColor: '#707073',
        //         minorGridLineColor: '#505053',
        //         tickColor: '#707073',
        //         tickWidth: 1,
        //         title: {
        //             style: {
        //                 color: '#A0A0A3'
        //             }
        //         }
        //     },
        //     tooltip: {
        //         backgroundColor: 'rgba(0, 0, 0, 0.85)',
        //         style: {
        //             color: '#F0F0F0'
        //         }
        //     },
        //     plotOptions: {
        //         series: {
        //             dataLabels: {
        //                 color: '#B0B0B3'
        //             },
        //             marker: {
        //                 lineColor: '#333'
        //             }
        //         },
        //         boxplot: {
        //             fillColor: '#505053'
        //         },
        //         candlestick: {
        //             lineColor: 'white'
        //         },
        //         errorbar: {
        //             color: 'white'
        //         }
        //     },
        //     legend: {
        //         itemStyle: {
        //             color: '#E0E0E3'
        //         },
        //         itemHoverStyle: {
        //             color: '#FFF'
        //         },
        //         itemHiddenStyle: {
        //             color: '#606063'
        //         }
        //     },
        //     credits: {
        //         style: {
        //             color: '#666'
        //         }
        //     },
        //     labels: {
        //         style: {
        //             color: '#707073'
        //         }
        //     },
        //
        //     drilldown: {
        //         activeAxisLabelStyle: {
        //             color: '#F0F0F3'
        //         },
        //         activeDataLabelStyle: {
        //             color: '#F0F0F3'
        //         }
        //     },
        //
        //     navigation: {
        //         buttonOptions: {
        //             symbolStroke: '#DDDDDD',
        //             theme: {
        //                 fill: '#505053'
        //             }
        //         }
        //     },
        //
        //     // scroll charts
        //     rangeSelector: {
        //         buttonTheme: {
        //             fill: '#505053',
        //             stroke: '#000000',
        //             style: {
        //                 color: '#CCC'
        //             },
        //             states: {
        //                 hover: {
        //                     fill: '#707073',
        //                     stroke: '#000000',
        //                     style: {
        //                         color: 'white'
        //                     }
        //                 },
        //                 select: {
        //                     fill: '#000003',
        //                     stroke: '#000000',
        //                     style: {
        //                         color: 'white'
        //                     }
        //                 }
        //             }
        //         },
        //         inputBoxBorderColor: '#505053',
        //         inputStyle: {
        //             backgroundColor: '#333',
        //             color: 'silver'
        //         },
        //         labelStyle: {
        //             color: 'silver'
        //         }
        //     },
        //
        //     navigator: {
        //         handles: {
        //             backgroundColor: '#666',
        //             borderColor: '#AAA'
        //         },
        //         outlineColor: '#CCC',
        //         maskFill: 'rgba(255,255,255,0.1)',
        //         series: {
        //             color: '#7798BF',
        //             lineColor: '#A6C7ED'
        //         },
        //         xAxis: {
        //             gridLineColor: '#505053'
        //         }
        //     },
        //
        //     scrollbar: {
        //         barBackgroundColor: '#808083',
        //         barBorderColor: '#808083',
        //         buttonArrowColor: '#CCC',
        //         buttonBackgroundColor: '#606063',
        //         buttonBorderColor: '#606063',
        //         rifleColor: '#FFF',
        //         trackBackgroundColor: '#404043',
        //         trackBorderColor: '#404043'
        //     },
        //
        //     // special colors for some of the
        //     legendBackgroundColor: 'rgba(0, 0, 0, 0.5)',
        //     background2: '#505053',
        //     dataLabelsColor: '#B0B0B3',
        //     textColor: '#C0C0C0',
        //     contrastTextColor: '#F0F0F3',
        //     maskColor: 'rgba(255,255,255,0.3)'
        // };
        // Highcharts.setOptions(theme);
        // this.api.getICOAgendaChart({}).subscribe(res => {
        //     if (res.success) {
        //         this.icoChartData = res.data;
        //
        //         let labels: any = [];
        //         let series: any = [];
        //         let high = 0;
        //         let low = 100000;
        //
        //         let size = 0;
        //         for (var i = 0; i < res.data.length; i++) {
        //             let from = res.data[i].from;
        //             let to = res.data[i].to;
        //
        //             let date = new Date(from);
        //             while (date <= new Date(to)) {
        //                 labels[size ++] = (date.getMonth() + 1) + '/' + (date.getDate());
        //                 series[size ++] = res.data[i].price;
        //
        //                 if (res.data[i].price > high) {
        //                     high = res.data[i].price;
        //                 }
        //                 if (res.data[i].price < low) {
        //                     low = res.data[i].price;
        //                 }
        //                 date.setDate(date.getDate() + 1);
        //             }
        //         }
        //         const optionsMarketChart: any = {
        //             lineSmooth: Chartist.Interpolation.cardinal({
        //                 tension: 10
        //             }),
        //             axisY: {
        //                 showGrid: true,
        //                 offset: 40
        //             },
        //             axisX: {
        //                 showGrid: true,
        //             },
        //             high: Number(high) + 0.1,
        //             showPoint: true,
        //             height: '250px'
        //         };
        //
        //         const marketChart = new Chartist.Line('#marketchart', {
        //                 labels: labels,
        //                 series: [series]
        //             },
        //             optionsMarketChart);
        //
        //         this.startAnimationForLineChart(marketChart);
        //     }
        // });
    };
    ExchangeBTCComponent.prototype.onClickSellOrder = function (index) {
        this.buy.rate = Number(this.sellOrders[index].rate);
        this.buy.amount = Number(this.sellOrders[index].amount);
    };
    ExchangeBTCComponent.prototype.onClickBuyOrder = function (index) {
        this.sell.rate = Number(this.buyOrders[index].rate);
        this.sell.amount = Number(this.buyOrders[index].amount);
    };
    ExchangeBTCComponent.prototype.newBuyOrder = function () {
        var _this = this;
        if (this.buy.rate == '' || this.buy.amount == '') {
            return;
        }
        if (this.buy.rate == 0 || this.buy.amount == 0) {
            return;
        }
        this.buyloading = true;
        this.api.makeBTCBuyOrder(this.buy).subscribe(function (res) {
            _this.buyloading = false;
            if (res.success) {
                _this.notify.showNotification('Your order was posted successfully.', 'top', 'center', 'success');
                _this.balance.getBtcBalance();
                _this.balance.getTokenBalance();
                _this.getOpenOrders();
                _this.getSellOrders();
                _this.getBuyOrders();
            }
            else {
                _this.notify.showNotification(res.error, 'top', 'center', 'warning');
            }
        }, function (err) {
            _this.buyloading = false;
            _this.notify.showNotification('Failed. Please try again.', 'top', 'center', 'warning');
        });
    };
    ExchangeBTCComponent.prototype.newSellOrder = function () {
        var _this = this;
        if (this.sell.rate == '' || this.sell.amount == '') {
            return;
        }
        if (this.sell.rate == 0 || this.sell.amount == 0) {
            return;
        }
        this.sellloading = true;
        this.api.makeBTCSellOrder(this.sell).subscribe(function (res) {
            _this.sellloading = false;
            if (res.success) {
                _this.notify.showNotification('Your order was posted successfully.', 'top', 'center', 'success');
                _this.balance.getBtcBalance();
                _this.balance.getTokenBalance();
                _this.getOpenOrders();
                _this.getSellOrders();
                _this.getBuyOrders();
            }
            else {
                _this.notify.showNotification(res.error, 'top', 'center', 'warning');
            }
        }, function (err) {
            _this.sellloading = false;
            _this.notify.showNotification('Failed. Please try again.', 'top', 'center', 'warning');
        });
    };
    ExchangeBTCComponent.prototype.getBuyOrders = function () {
        var _this = this;
        this.api.getBTCBuyOrders({}).subscribe(function (res) {
            if (res.success) {
                _this.buyOrders = res.data;
            }
        });
    };
    ExchangeBTCComponent.prototype.getSellOrders = function () {
        var _this = this;
        this.api.getBTCSellOrders({}).subscribe(function (res) {
            if (res.success) {
                _this.sellOrders = res.data;
            }
        });
    };
    ExchangeBTCComponent.prototype.getPastOrders = function () {
        var _this = this;
        this.api.getBTCPastOrders({}).subscribe(function (res) {
            if (res.success) {
                _this.pastOrders = res.data;
            }
        });
    };
    ExchangeBTCComponent.prototype.getOpenOrders = function () {
        var _this = this;
        this.api.getBTCOpenOrders({}).subscribe(function (res) {
            if (res.success) {
                _this.openOrders = res.data;
            }
        });
    };
    ExchangeBTCComponent.prototype.cancelOrder = function (id) {
        var _parent = this;
        swal({
            title: 'Are you sure?',
            text: 'Your order will be deleted permanently.',
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            confirmButtonText: 'Yes',
            buttonsStyling: false
        }).then(function () {
            _parent.api.cancelBTCOrder({
                id: id
            }).subscribe(function (res) {
                if (res.success) {
                    _parent.notify.showNotification('Your order was canceled successfully.', 'top', 'center', 'success');
                    _parent.balance.getBtcBalance();
                    _parent.balance.getTokenBalance();
                    _parent.getOpenOrders();
                    _parent.getSellOrders();
                    _parent.getBuyOrders();
                }
                else {
                    _parent.notify.showNotification(res.error, 'top', 'center', 'warning');
                }
            });
        });
    };
    ExchangeBTCComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-exchange',
            template: __webpack_require__("../../../../../src/app/pages/exchange/btc/exchangebtc.component.html"),
            styles: [__webpack_require__("../../../../../src/app/pages/exchange/btc/exchangebtc.component.scss")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__services_api_service__["a" /* Api */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_api_service__["a" /* Api */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__services_settings_settings_service__["a" /* SettingsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services_settings_settings_service__["a" /* SettingsService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_4__services_balance_service__["a" /* BalanceService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__services_balance_service__["a" /* BalanceService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_5__services_notifications_service__["a" /* Notifications */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__services_notifications_service__["a" /* Notifications */]) === "function" && _d || Object])
    ], ExchangeBTCComponent);
    return ExchangeBTCComponent;
    var _a, _b, _c, _d;
}());

//# sourceMappingURL=exchangebtc.component.js.map

/***/ }),

/***/ "../../../../../src/app/pages/exchange/eth/exchangeeth.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\n    <div class=\"container-fluid\" *ngIf=\"settings.getSysSetting('enable_eth_exchange') == 0\">\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n                <div class=\"alert alert-info\">\n                    ETHEREUM Exchange is disabled.\n                </div>\n            </div>\n        </div>\n    </div>\n    <div class=\"container-fluid\" *ngIf=\"settings.getSysSetting('enable_eth_exchange') == 1\">\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n                <div class=\"card\">\n                    <div class=\"card-header card-header-icon\" data-background-color=\"blue\">\n                        <i class=\"material-icons\">timeline</i>\n                    </div>\n                    <div class=\"card-content\">\n                        <h4 class=\"card-title\">Market PRICE<br>\n                            <small>(BT9/ETH)</small>\n                        </h4>\n                    </div>\n                    <div class=\"exchange-info\" style=\"padding-left: 15px; padding-right: 15px;\">\n                        <div class=\"row\">\n                            <div class=\"col-md-1\">\n                                LAST\n                            </div>\n                            <div class=\"text-success col-md-3\" >\n                                {{exchangeInfo.lastPrice | number:'1.8-8'}} ETH (${{(exchangeInfo.lastPrice * eth_usd) | number:'1.2-2'}} )\n                            </div>\n                            <div class=\"col-md-1\">\n                                LAST BID\n                            </div>\n                            <div class=\"text-success col-md-3\" >\n                                {{exchangeInfo.lastBid | number:'1.8-8'}} ETH (${{(exchangeInfo.lastBid * eth_usd) | number:'1.2-2'}} )\n                            </div>\n                            <div class=\"col-md-1\">\n                                24H HIGH\n                            </div>\n                            <div class=\"text-success col-md-3\" >\n                                {{exchangeInfo.high | number:'1.8-8'}} ETH (${{(exchangeInfo.high * eth_usd) | number:'1.2-2'}} )\n                            </div>\n                            <div class=\"col-md-1\">\n                                24H VOL\n                            </div>\n                            <div class=\"text-success col-md-3\" >\n                                {{exchangeInfo.volume | number:'1.8-8'}} ETH (${{(exchangeInfo.volume * eth_usd) | number:'1.2-2'}} )\n                            </div>\n                            <div class=\"col-md-1\">\n                                LAST ASK\n                            </div>\n                            <div class=\"text-success col-md-3\" >\n                                {{exchangeInfo.lastAsk | number:'1.8-8'}} ETH (${{(exchangeInfo.lastAsk * eth_usd) | number:'1.2-2'}} )\n                            </div>\n                            <div class=\"col-md-1\">\n                                24H LOW\n                            </div>\n                            <div class=\"text-success col-md-3\" >\n                                {{exchangeInfo.low | number:'1.8-8'}} ETH (${{(exchangeInfo.low * eth_usd) | number:'1.2-2'}} )\n                            </div>\n                        </div>\n                    </div>\n                    <div id=\"marketchart\" [chart]=\"stockChart\" class=\"ct-chart\"></div>\n                </div>\n            </div>\n        </div>\n        <div class=\"row\">\n            <div class=\"col-md-6\">\n                <div class=\"card\">\n                    <div class=\"card-header card-header-icon\" data-background-color=\"green\">\n                        <i class=\"fa fa-plus\"></i>\n                    </div>\n                    <form class=\"form-horizontal\">\n                        <div class=\"card-content\">\n                            <h4 class=\"card-title\">Buy BT9</h4>\n                            <div class=\"text-center\">\n                                Available balance: <b>{{settings.getUserSetting('eth_balance') | number:'1.8-8'}}</b>\n                                ETH\n                            </div>\n                            <div class=\"row\">\n                                <label class=\"col-md-2 label-on-left\">Units</label>\n                                <div class=\"col-md-10\">\n                                    <div class=\"form-group label-floating is-empty\">\n                                        <label class=\"control-label\"></label>\n                                        <input type=\"number\" class=\"form-control\" name=\"amount\" step=\"0.00001\"\n                                               [(ngModel)]=\"buy.amount\">\n                                        <span class=\"help-block\">BT9</span>\n                                    </div>\n                                </div>\n                            </div>\n                            <div class=\"row\">\n                                <label class=\"col-md-2 label-on-left\">Bid</label>\n                                <div class=\"col-md-10\">\n                                    <div class=\"form-group label-floating is-empty\">\n                                        <label class=\"control-label\"></label>\n                                        <input type=\"number\" class=\"form-control\" name=\"rate\" [(ngModel)]=\"buy.rate\"\n                                               step=\"0.00001\">\n                                        <span class=\"help-block\">ETH/BT9</span>\n                                    </div>\n                                </div>\n                            </div>\n                            <div class=\"row\">\n                                <label class=\"col-md-2 label-on-left\">Total</label>\n                                <div class=\"col-md-10\">\n                                    <div class=\"form-group label-floating is-empty\">\n                                        <label class=\"control-label\"></label>\n                                        <input type=\"text\" class=\"form-control\" name=\"total\" readonly\n                                               value=\"{{(buy.amount * buy.rate ? buy.amount * buy.rate : 0) | number: '1.8-8'}}\">\n                                        <span class=\"help-block\">ETH</span>\n                                    </div>\n                                </div>\n                            </div>\n                            <div class=\"row\">\n                                <label class=\"col-md-2 label-on-left\">Fee</label>\n                                <label class=\"col-md-10 label-on-right\">{{settings.getSysSetting('exchange_buy_fee') |\n                                    number:'1.0-2'}}%</label>\n                            </div>\n                            <div class=\"row\">\n                                <div class=\"col-md-12 \">\n                                    <button type=\"button\" class=\"btn btn-info pull-right\" (click)=\"newBuyOrder()\"\n                                            *ngIf=\"!buyloading\"><i class=\"fa fa-plus\"></i> Buy\n                                        BT9Coin\n                                    </button>\n                                </div>\n                                <loader *ngIf=\"buyloading\"></loader>\n                            </div>\n                        </div>\n                    </form>\n                </div>\n            </div>\n            <div class=\"col-md-6\">\n                <div class=\"card\">\n                    <div class=\"card-header card-header-icon\" data-background-color=\"rose\">\n                        <i class=\"fa fa-minus\"></i>\n                    </div>\n\n                    <form class=\"form-horizontal\">\n                        <div class=\"card-content\">\n                            <h4 class=\"card-title\">Sell BT9</h4>\n                            <div class=\"text-center\">\n                                Available balance: <b>{{settings.getUserSetting('token_balance') | number:'1.8-8'}}</b>\n                                BT9\n                            </div>\n                            <div class=\"row\">\n                                <label class=\"col-md-2 label-on-left\">Units</label>\n                                <div class=\"col-md-10\">\n                                    <div class=\"form-group label-floating is-empty\">\n                                        <label class=\"control-label\"></label>\n                                        <input type=\"number\" class=\"form-control\" name=\"amount\" step=\"0.00001\"\n                                               [(ngModel)]=\"sell.amount\">\n                                        <span class=\"help-block\">BT9</span>\n                                    </div>\n                                </div>\n                            </div>\n                            <div class=\"row\">\n                                <label class=\"col-md-2 label-on-left\">Ask</label>\n                                <div class=\"col-md-10\">\n                                    <div class=\"form-group label-floating is-empty\">\n                                        <label class=\"control-label\"></label>\n                                        <input type=\"number\" class=\"form-control\" name=\"rate\" [(ngModel)]=\"sell.rate\"\n                                               step=\"0.00001\">\n                                        <span class=\"help-block\">ETH/BT9</span>\n                                    </div>\n                                </div>\n                            </div>\n                            <div class=\"row\">\n                                <label class=\"col-md-2 label-on-left\">Total</label>\n                                <div class=\"col-md-10\">\n                                    <div class=\"form-group label-floating is-empty\">\n                                        <label class=\"control-label\"></label>\n                                        <input type=\"text\" class=\"form-control\" name=\"total\" readonly\n                                               value=\"{{(sell.amount * sell.rate ? sell.amount * sell.rate : 0) | number: '1.8-8'}}\">\n                                        <span class=\"help-block\">ETH</span>\n                                    </div>\n                                </div>\n                            </div>\n                            <div class=\"row\">\n                                <label class=\"col-md-2 label-on-left\">Fee</label>\n                                <label class=\"col-md-10 label-on-right\">{{settings.getSysSetting('exchange_sell_fee') |\n                                    number:'1.0-2'}}%</label>\n                            </div>\n                            <div class=\"row\">\n                                <div class=\"col-md-12 \">\n                                    <button type=\"button\" class=\"btn btn-info pull-right\" (click)=\"newSellOrder()\"\n                                            *ngIf=\"!sellloading\"><i class=\"fa fa-minus\"></i>\n                                        Sell BT9\n                                    </button>\n                                    <loader *ngIf=\"sellloading\"></loader>\n                                </div>\n                            </div>\n                        </div>\n                    </form>\n                </div>\n            </div>\n        </div>\n        <div class=\"row\">\n            <div class=\"col-md-6\">\n                <div class=\"card \">\n                    <div class=\"card-header card-header-icon\" data-background-color=\"default\">\n                        <i class=\"fa fa-bars\"></i>\n                    </div>\n                    <div class=\"card-content\">\n                        <div class=\"card-title\">\n                            <h4>Buying Orders</h4>\n                        </div>\n                        <div class=\"table-responsive height-500\">\n                            <table class=\"table color-table success-table table-hover table-right\">\n                                <thead>\n                                <tr>\n                                    <th>Total(ETH)</th>\n                                    <th>Size(BT9)</th>\n                                    <th>Bid(ETH)</th>\n                                </tr>\n                                </thead>\n                                <tbody>\n                                <tr *ngFor=\"let order of buyOrders; let i = index;\" (click)=\"onClickBuyOrder(i)\">\n                                    <td class=\"text-right\">{{order.amount * order.rate | number:'1.8-8'}}</td>\n                                    <td class=\"text-right\">{{order.amount | number:'1.8-8'}}</td>\n                                    <td class=\"text-right\">{{order.rate | number:'1.8-8'}}</td>\n                                </tr>\n                                <tr *ngIf=\"sellOrders?.length == 0\">\n                                    <td style=\"text-align: center !important;\" colspan=\"3\">\n                                        No Sell Orders\n                                    </td>\n                                </tr>\n                                </tbody>\n                            </table>\n                        </div>\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-md-6\">\n                <div class=\"card \">\n                    <div class=\"card-header card-header-icon\" data-background-color=\"default\">\n                        <i class=\"fa fa-bars\"></i>\n                    </div>\n                    <div class=\"card-content\">\n                        <div class=\"card-title\">\n                            <h4>Selling Orders</h4>\n                        </div>\n\n                        <div class=\"table-responsive height-500\">\n                            <table class=\"table color-table success-table table-hover table-right\">\n                                <thead>\n                                <tr>\n                                    <th>Total(ETH)</th>\n                                    <th>Size(BT9)</th>\n                                    <th>Bid(ETH)</th>\n                                </tr>\n                                </thead>\n                                <tbody>\n                                <tr *ngFor=\"let order of sellOrders; let i = index;\" (click)=\"onClickSellOrder(i)\">\n                                    <td class=\"text-right\">{{order.amount * order.rate | number:'1.8-8'}}</td>\n                                    <td class=\"text-right\">{{order.amount | number:'1.8-8'}}</td>\n                                    <td class=\"text-right\">{{order.rate | number:'1.8-8'}}</td>\n                                </tr>\n                                <tr *ngIf=\"buyOrders?.length == 0\">\n                                    <td style=\"text-align: center !important;\" colspan=\"3\">\n                                        No Buy Orders\n                                    </td>\n                                </tr>\n                                </tbody>\n                            </table>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n                <div class=\"card \">\n                    <div class=\"card-header card-header-icon\" data-background-color=\"green\">\n                        <i class=\"fa fa-history\"></i>\n                    </div>\n\n\n                    <div class=\"card-content\">\n                        <div class=\"card-title\">\n                            <h4>Market History</h4>\n                        </div>\n                        <ul class=\"nav nav-pills nav-menu-security nav-pills-warning\">\n                            <li class=\"active\">\n                                <a data-toggle=\"tab\" href=\"#open\">Open Orders</a>\n                            </li>\n                            <li>\n                                <a data-toggle=\"tab\" href=\"#past\">History</a>\n                            </li>\n                        </ul>\n                        <div class=\"tab-content\">\n                            <div class=\"tab-pane active\" id=\"open\">\n                                <div class=\"table-responsive height-500\">\n                                    <table class=\"table color-table success-table table-hover table-center\">\n                                        <thead>\n                                        <tr>\n                                            <th>Type</th>\n                                            <th style=\"width: 200px;\">Time</th>\n                                            <th>Amount</th>\n                                            <th>Rate</th>\n                                            <th>Total Price</th>\n                                            <th class=\"text-center\">Action</th>\n                                        </tr>\n                                        </thead>\n                                        <tbody>\n                                        <tr *ngFor=\"let order of openOrders\">\n                                            <td class=\"text-center\"><label class=\"label label-sm\"\n                                                                           [ngClass]=\"{'label-success': order.type == 0, 'label-info': order.type == 1}\">{{order.type\n                                                == 0 ? 'BUY' : 'SELL'}}</label></td>\n                                            <td style=\"width: 200px;\" class=\"text-center\">{{order.created_at |\n                                                date:'yyyy-MM-dd H:m:s'}}\n                                            </td>\n                                            <td class=\"text-center\">{{order.amount | number:'1.8-8'}}</td>\n                                            <td class=\"text-center\">{{order.rate | number:'1.2-2'}}</td>\n                                            <td class=\"text-center\">{{order.amount * order.rate | number:'1.2-2'}}</td>\n                                            <td class=\"text-center\">\n                                                <button type=\"button\" class=\"btn btn-sm btn-danger\"\n                                                        (click)=\"cancelOrder(order.id)\">\n                                                    <i class=\"fa fa-close\"></i></button>\n                                            </td>\n                                        </tr>\n                                        <tr *ngIf=\"openOrders?.length == 0\">\n                                            <td class=\"text-center\" colspan=\"6\">\n                                                No Open Orders\n                                            </td>\n                                        </tr>\n                                        </tbody>\n                                    </table>\n                                </div>\n                            </div>\n                            <div class=\"tab-pane\" id=\"past\">\n                                <div class=\"table-responsive height-500\">\n                                    <table class=\"table color-table success-table table-hover table-center\">\n                                        <thead>\n                                        <tr>\n                                            <th>Type</th>\n                                            <th style=\"width: 200px;\">Time</th>\n                                            <th>Units</th>\n                                            <th>Bid/Ask</th>\n                                            <th>Total Price</th>\n                                        </tr>\n                                        </thead>\n                                        <tbody>\n                                        <tr *ngFor=\"let order of pastOrders\">\n                                            <td><label class=\"label label-sm\"\n                                                       [ngClass]=\"{'label-success': order.exchange_type == 0, 'label-info': order.exchange_type == 1}\">\n                                                {{order.exchange_type == 0 ? 'BUY' : 'SELL'}}</label>\n                                            </td>\n                                            <td style=\"width: 200px;\">{{order.created_at | date:'yyyy-MM-dd H:m:s'}}\n                                            </td>\n                                            <td>{{order.exchange_type == 0 ? order.dest_amount : order.src_amount |\n                                                number:'1.8-8'}}\n                                            </td>\n                                            <td>{{order.rate | number:'1.2-2'}}</td>\n                                            <td>{{order.exchange_type == 0 ? order.src_amount : order.dest_amount |\n                                                number:'1.2-2'}}\n                                            </td>\n                                        </tr>\n                                        <tr *ngIf=\"pastOrders?.length == 0\">\n                                            <td class=\"text-center\" colspan=\"5\">\n                                                No Orders\n                                            </td>\n                                        </tr>\n                                        </tbody>\n                                    </table>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/pages/exchange/eth/exchangeeth.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".height-500 {\n  min-height: 450px;\n  max-height: 450px;\n  overflow-y: auto; }\n  .height-500 .table-responsive {\n    overflow-x: auto; }\n\n.table-right th, .table-right td {\n  text-align: right !important; }\n\n.table-center th, .table-center td {\n  text-align: center !important; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/pages/exchange/eth/exchangeeth.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ExchangeETHComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_chartist__ = __webpack_require__("../../../../chartist/dist/chartist.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_chartist___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_chartist__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_api_service__ = __webpack_require__("../../../../../src/app/services/api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_settings_settings_service__ = __webpack_require__("../../../../../src/app/services/settings/settings.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_balance_service__ = __webpack_require__("../../../../../src/app/services/balance.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_notifications_service__ = __webpack_require__("../../../../../src/app/services/notifications.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_angular_highcharts__ = __webpack_require__("../../../../angular-highcharts/angular-highcharts.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var ExchangeETHComponent = (function () {
    function ExchangeETHComponent(api, settings, balance, notify) {
        this.api = api;
        this.settings = settings;
        this.balance = balance;
        this.notify = notify;
        this.icoChartData = [];
        this.buy = {};
        this.sell = {};
        this.buyOrders = [];
        this.sellOrders = [];
        this.openOrders = [];
        this.pastOrders = [];
        this.buyloading = false;
        this.sellloading = false;
        this.exchangeInfo = {};
        this.eth_usd = 0;
        this.usd_eth = 0;
    }
    ExchangeETHComponent.prototype.startAnimationForLineChart = function (chart) {
        var seq, delays, durations;
        seq = 0;
        delays = 80;
        durations = 500;
        chart.on('draw', function (data) {
            if (data.type === 'line' || data.type === 'area') {
                data.element.animate({
                    d: {
                        begin: 600,
                        dur: 700,
                        from: data.path.clone().scale(1, 0).translate(0, data.chartRect.height()).stringify(),
                        to: data.path.clone().stringify(),
                        easing: __WEBPACK_IMPORTED_MODULE_1_chartist__["Svg"].Easing.easeOutQuint
                    }
                });
            }
            else if (data.type === 'point') {
                seq++;
                data.element.animate({
                    opacity: {
                        begin: seq * delays,
                        dur: durations,
                        from: 0,
                        to: 1,
                        easing: 'ease'
                    }
                });
            }
        });
        seq = 0;
    };
    ExchangeETHComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.getBuyOrders();
        this.getSellOrders();
        this.getPastOrders();
        this.getOpenOrders();
        this.api.getCoinRate({}).subscribe(function (res) {
            if (res.success) {
                _this.eth_usd = res.eth_rate;
                _this.usd_eth = 1 / Number(res.eth_rate);
            }
        }, function (err) {
        });
        this.api.getExchangeStatistics('ETH', {}).subscribe(function (res) {
            if (res.success) {
                _this.exchangeInfo = res.data;
                if (_this.exchangeInfo.lastPrice > 0) {
                    _this.buy.rate = Number(_this.exchangeInfo.lastPrice);
                    _this.sell.rate = Number(_this.exchangeInfo.lastPrice);
                }
            }
        });
        var _parent = this;
        this.api.getExchangeHistory('ETH', {}).subscribe(function (res) {
            if (res.success) {
                // split the data set into ohlc and volume
                var ohlc = [], volume = [], dataLength = res.data.length, 
                // set the allowed units for data grouping
                groupingUnits = [[
                        'hour',
                        [6, 12, 24] // allowed multiples
                    ], [
                        'day',
                        [3, 7]
                    ]], i = 0;
                for (i; i < res.data.length; i += 1) {
                    ohlc.push([
                        Number(res.data[i].date),
                        Number(res.data[i].open),
                        Number(res.data[i].high),
                        Number(res.data[i].low),
                        Number(res.data[i].close) // close
                    ]);
                    volume.push([
                        Number(res.data[i].date),
                        Number(res.data[i].volume) // the volume
                    ]);
                }
                // create the chart
                _parent.stockChart = new __WEBPACK_IMPORTED_MODULE_6_angular_highcharts__["c" /* StockChart */]({
                    rangeSelector: {
                        selected: 0,
                        buttons: [{
                                type: 'hour',
                                count: 6,
                                text: '6h',
                            }, {
                                type: 'hour',
                                count: 12,
                                text: '12h'
                            }, {
                                type: 'hour',
                                count: 24,
                                text: '24h'
                            }, {
                                type: 'day',
                                count: 3,
                                text: '3d'
                            }, {
                                type: 'day',
                                count: 7,
                                text: '1w'
                            }, {
                                type: 'all',
                                text: 'All'
                            }]
                    },
                    title: {
                        text: 'Market History'
                    },
                    yAxis: [{
                            labels: {
                                align: 'right',
                                x: -3
                            },
                            title: {
                                text: 'Price'
                            },
                            height: '60%',
                            lineWidth: 2,
                            resize: {
                                enabled: true
                            }
                        }, {
                            labels: {
                                align: 'right',
                                x: -3
                            },
                            title: {
                                text: 'Volume'
                            },
                            top: '65%',
                            height: '35%',
                            offset: 0,
                            lineWidth: 2
                        }],
                    tooltip: {
                        split: true
                    },
                    series: [{
                            type: 'candlestick',
                            name: 'ETH Exchange Price',
                            data: ohlc,
                            dataGrouping: {
                                units: groupingUnits
                            }
                        }, {
                            type: 'column',
                            name: 'Volume(ETH)',
                            data: volume,
                            yAxis: 1,
                            dataGrouping: {
                                units: groupingUnits
                            }
                        }]
                });
            }
        });
    };
    ExchangeETHComponent.prototype.onClickSellOrder = function (index) {
        this.buy.rate = Number(this.sellOrders[index].rate);
        this.buy.amount = Number(this.sellOrders[index].amount);
    };
    ExchangeETHComponent.prototype.onClickBuyOrder = function (index) {
        this.sell.rate = Number(this.buyOrders[index].rate);
        this.sell.amount = Number(this.buyOrders[index].amount);
    };
    ExchangeETHComponent.prototype.newBuyOrder = function () {
        var _this = this;
        if (this.buy.rate == '' || this.buy.amount == '') {
            return;
        }
        if (this.buy.rate == 0 || this.buy.amount == 0) {
            return;
        }
        this.buyloading = true;
        this.api.makeETHBuyOrder(this.buy).subscribe(function (res) {
            _this.buyloading = false;
            if (res.success) {
                _this.notify.showNotification('Your order was posted successfully.', 'top', 'center', 'success');
                _this.balance.getEthBalance();
                _this.balance.getTokenBalance();
                _this.getOpenOrders();
                _this.getSellOrders();
                _this.getBuyOrders();
            }
            else {
                _this.notify.showNotification(res.error, 'top', 'center', 'warning');
            }
        }, function (err) {
            _this.buyloading = false;
            _this.notify.showNotification('Failed. Please try again.', 'top', 'center', 'warning');
        });
    };
    ExchangeETHComponent.prototype.newSellOrder = function () {
        var _this = this;
        if (this.sell.rate == '' || this.sell.amount == '') {
            return;
        }
        if (this.sell.rate == 0 || this.sell.amount == 0) {
            return;
        }
        this.sellloading = true;
        this.api.makeETHSellOrder(this.sell).subscribe(function (res) {
            _this.sellloading = false;
            if (res.success) {
                _this.notify.showNotification('Your order was posted successfully.', 'top', 'center', 'success');
                _this.balance.getEthBalance();
                _this.balance.getTokenBalance();
                _this.getOpenOrders();
                _this.getSellOrders();
                _this.getBuyOrders();
            }
            else {
                _this.notify.showNotification(res.error, 'top', 'center', 'warning');
            }
        }, function (err) {
            _this.sellloading = false;
            _this.notify.showNotification('Failed. Please try again.', 'top', 'center', 'warning');
        });
    };
    ExchangeETHComponent.prototype.getBuyOrders = function () {
        var _this = this;
        this.api.getETHBuyOrders({}).subscribe(function (res) {
            if (res.success) {
                _this.buyOrders = res.data;
            }
        });
    };
    ExchangeETHComponent.prototype.getSellOrders = function () {
        var _this = this;
        this.api.getETHSellOrders({}).subscribe(function (res) {
            if (res.success) {
                _this.sellOrders = res.data;
            }
        });
    };
    ExchangeETHComponent.prototype.getPastOrders = function () {
        var _this = this;
        this.api.getETHPastOrders({}).subscribe(function (res) {
            if (res.success) {
                _this.pastOrders = res.data;
            }
        });
    };
    ExchangeETHComponent.prototype.getOpenOrders = function () {
        var _this = this;
        this.api.getETHOpenOrders({}).subscribe(function (res) {
            if (res.success) {
                _this.openOrders = res.data;
            }
        });
    };
    ExchangeETHComponent.prototype.cancelOrder = function (id) {
        var _parent = this;
        swal({
            title: 'Are you sure?',
            text: 'Your order will be deleted permanently.',
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            confirmButtonText: 'Yes',
            buttonsStyling: false
        }).then(function () {
            _parent.api.cancelETHOrder({
                id: id
            }).subscribe(function (res) {
                if (res.success) {
                    _parent.notify.showNotification('Your order was canceled successfully.', 'top', 'center', 'success');
                    _parent.balance.getEthBalance();
                    _parent.balance.getTokenBalance();
                    _parent.getOpenOrders();
                    _parent.getSellOrders();
                    _parent.getBuyOrders();
                }
                else {
                    _parent.notify.showNotification(res.error, 'top', 'center', 'warning');
                }
            });
        });
    };
    ExchangeETHComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-exchangeeth',
            template: __webpack_require__("../../../../../src/app/pages/exchange/eth/exchangeeth.component.html"),
            styles: [__webpack_require__("../../../../../src/app/pages/exchange/eth/exchangeeth.component.scss")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__services_api_service__["a" /* Api */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_api_service__["a" /* Api */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__services_settings_settings_service__["a" /* SettingsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services_settings_settings_service__["a" /* SettingsService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_4__services_balance_service__["a" /* BalanceService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__services_balance_service__["a" /* BalanceService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_5__services_notifications_service__["a" /* Notifications */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__services_notifications_service__["a" /* Notifications */]) === "function" && _d || Object])
    ], ExchangeETHComponent);
    return ExchangeETHComponent;
    var _a, _b, _c, _d;
}());

//# sourceMappingURL=exchangeeth.component.js.map

/***/ }),

/***/ "../../../../../src/app/pages/exchange/exchange.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["highchartsModules"] = highchartsModules;
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExchangeModule", function() { return ExchangeModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__md_md_module__ = __webpack_require__("../../../../../src/app/md/md.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__btc_exchangebtc_component__ = __webpack_require__("../../../../../src/app/pages/exchange/btc/exchangebtc.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__exchange_routing__ = __webpack_require__("../../../../../src/app/pages/exchange/exchange.routing.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__shared_shared_module__ = __webpack_require__("../../../../../src/app/shared/shared.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__eth_exchangeeth_component__ = __webpack_require__("../../../../../src/app/pages/exchange/eth/exchangeeth.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_angular_highcharts__ = __webpack_require__("../../../../angular-highcharts/angular-highcharts.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_highcharts_highcharts_more_src__ = __webpack_require__("../../../../highcharts/highcharts-more.src.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_highcharts_highcharts_more_src___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_11_highcharts_highcharts_more_src__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_highcharts_modules_exporting_src__ = __webpack_require__("../../../../highcharts/modules/exporting.src.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_highcharts_modules_exporting_src___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_12_highcharts_modules_exporting_src__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_highcharts_modules_stock_src__ = __webpack_require__("../../../../highcharts/modules/stock.src.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_highcharts_modules_stock_src___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_13_highcharts_modules_stock_src__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};














function highchartsModules() {
    return [__WEBPACK_IMPORTED_MODULE_11_highcharts_highcharts_more_src___default.a, __WEBPACK_IMPORTED_MODULE_12_highcharts_modules_exporting_src___default.a, __WEBPACK_IMPORTED_MODULE_13_highcharts_modules_stock_src___default.a];
}
var ExchangeModule = (function () {
    function ExchangeModule() {
    }
    ExchangeModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_2__angular_common__["b" /* CommonModule */],
                __WEBPACK_IMPORTED_MODULE_1__angular_router__["d" /* RouterModule */].forChild(__WEBPACK_IMPORTED_MODULE_7__exchange_routing__["a" /* ExchangeRoutes */]),
                __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormsModule"],
                __WEBPACK_IMPORTED_MODULE_4__md_md_module__["a" /* MdModule */],
                __WEBPACK_IMPORTED_MODULE_5__app_module__["b" /* MaterialModule */],
                __WEBPACK_IMPORTED_MODULE_8__shared_shared_module__["a" /* SharedModule */],
                __WEBPACK_IMPORTED_MODULE_10_angular_highcharts__["a" /* ChartModule */]
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_6__btc_exchangebtc_component__["a" /* ExchangeBTCComponent */],
                __WEBPACK_IMPORTED_MODULE_9__eth_exchangeeth_component__["a" /* ExchangeETHComponent */]
            ],
            providers: [
                {
                    provide: __WEBPACK_IMPORTED_MODULE_10_angular_highcharts__["b" /* HIGHCHARTS_MODULES */],
                    useFactory: highchartsModules
                }
            ]
        })
    ], ExchangeModule);
    return ExchangeModule;
}());

//# sourceMappingURL=exchange.module.js.map

/***/ }),

/***/ "../../../../../src/app/pages/exchange/exchange.routing.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ExchangeRoutes; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__btc_exchangebtc_component__ = __webpack_require__("../../../../../src/app/pages/exchange/btc/exchangebtc.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__eth_exchangeeth_component__ = __webpack_require__("../../../../../src/app/pages/exchange/eth/exchangeeth.component.ts");


var ExchangeRoutes = [
    {
        path: '',
        component: __WEBPACK_IMPORTED_MODULE_0__btc_exchangebtc_component__["a" /* ExchangeBTCComponent */]
    },
    {
        path: 'btc',
        component: __WEBPACK_IMPORTED_MODULE_0__btc_exchangebtc_component__["a" /* ExchangeBTCComponent */]
    },
    {
        path: 'eth',
        component: __WEBPACK_IMPORTED_MODULE_1__eth_exchangeeth_component__["a" /* ExchangeETHComponent */]
    }
];
//# sourceMappingURL=exchange.routing.js.map

/***/ }),

/***/ "../../../../angular-highcharts/angular-highcharts.es5.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChartModule; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return HIGHCHARTS_MODULES; });
/* unused harmony export Chart */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return StockChart; });
/* unused harmony export MapChart */
/* unused harmony export ɵb */
/* unused harmony export ɵa */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_highcharts__ = __webpack_require__("../../../../highcharts/highcharts.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_highcharts___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_highcharts__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");



/**
 * @license
 * Copyright Felix Itzenplitz. All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at
 * https://github.com/cebor/angular-highcharts/blob/master/LICENSE
 */
var Chart = (function () {
    /**
     * @param {?} options
     */
    function Chart(options) {
        this.options = options;
        // init series array if not set
        if (!this.options.series) {
            this.options.series = [];
        }
    }
    /**
     * @param {?} point
     * @param {?=} serieIndex
     * @param {?=} redraw
     * @param {?=} shift
     * @return {?}
     */
    Chart.prototype.addPoint = function (point, serieIndex, redraw, shift) {
        if (serieIndex === void 0) { serieIndex = 0; }
        if (redraw === void 0) { redraw = true; }
        if (shift === void 0) { shift = false; }
        ((this.options.series[serieIndex].data)).push(point);
        if (this.ref) {
            this.ref.series[serieIndex].addPoint(point, redraw, shift);
        }
    };
    /**
     * @param {?} pointIndex
     * @param {?=} serieIndex
     * @return {?}
     */
    Chart.prototype.removePoint = function (pointIndex, serieIndex) {
        if (serieIndex === void 0) { serieIndex = 0; }
        // TODO add try catch (empty)
        ((this.options.series[serieIndex].data)).splice(pointIndex, 1);
        if (this.ref) {
            this.ref.series[serieIndex].removePoint(pointIndex, true);
        }
    };
    /**
     * @param {?} serie
     * @return {?}
     */
    Chart.prototype.addSerie = function (serie) {
        // init data array if not set
        if (!serie.data) {
            serie.data = [];
        }
        this.options.series.push(serie);
        if (this.ref) {
            this.ref.addSeries(serie);
        }
    };
    /**
     * @param {?} serieIndex
     * @return {?}
     */
    Chart.prototype.removeSerie = function (serieIndex) {
        // TODO add try catch (empty)
        this.options.series.splice(serieIndex, 1);
        if (this.ref) {
            this.ref.series[serieIndex].remove(true);
        }
    };
    return Chart;
}());

/**
 * @license
 * Copyright Felix Itzenplitz. All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at
 * https://github.com/cebor/angular-highcharts/blob/master/LICENSE
 */
var MapChart = (function () {
    /**
     * @param {?} options
     */
    function MapChart(options) {
        this.options = options;
    }
    return MapChart;
}());

/**
 * @license
 * Copyright Felix Itzenplitz. All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at
 * https://github.com/cebor/angular-highcharts/blob/master/LICENSE
 */
var StockChart = (function () {
    /**
     * @param {?} options
     */
    function StockChart(options) {
        this.options = options;
    }
    return StockChart;
}());

/**
 * @license
 * Copyright Felix Itzenplitz. All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at
 * https://github.com/cebor/angular-highcharts/blob/master/LICENSE
 */
var ChartDirective = (function () {
    /**
     * @param {?} el
     */
    function ChartDirective(el) {
        this.el = el;
    }
    /**
     * @return {?}
     */
    ChartDirective.prototype.ngAfterViewInit = function () {
        this.init();
    };
    /**
     * @return {?}
     */
    ChartDirective.prototype.ngOnDestroy = function () {
        this.destroy();
    };
    /**
     * @param {?} changes
     * @return {?}
     */
    ChartDirective.prototype.ngOnChanges = function (changes) {
        if (!changes['chart'].isFirstChange()) {
            this.destroy();
            this.init();
        }
    };
    /**
     * @return {?}
     */
    ChartDirective.prototype.init = function () {
        if (this.chart instanceof Chart) {
            return this.chart.ref = __WEBPACK_IMPORTED_MODULE_0_highcharts__["chart"](this.el.nativeElement, this.chart.options);
        }
        if (this.chart instanceof StockChart) {
            return this.chart.ref = __WEBPACK_IMPORTED_MODULE_0_highcharts__["stockChart"](this.el.nativeElement, this.chart.options);
        }
        if (this.chart instanceof MapChart) {
            return this.chart.ref = __WEBPACK_IMPORTED_MODULE_0_highcharts__["mapChart"](this.el.nativeElement, this.chart.options);
        }
    };
    /**
     * @return {?}
     */
    ChartDirective.prototype.destroy = function () {
        if (this.chart && this.chart.ref) {
            this.chart.ref.destroy();
            delete this.chart.ref;
        }
    };
    ChartDirective.decorators = [
        { type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Directive"], args: [{
                    selector: '[chart]'
                },] },
    ];
    /**
     * @nocollapse
     */
    ChartDirective.ctorParameters = function () { return [
        { type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], },
    ]; };
    ChartDirective.propDecorators = {
        'chart': [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Input"] },],
    };
    return ChartDirective;
}());

/**
 * @license
 * Copyright Felix Itzenplitz. All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at
 * https://github.com/cebor/angular-highcharts/blob/master/LICENSE
 */
var HIGHCHARTS_MODULES = new __WEBPACK_IMPORTED_MODULE_1__angular_core__["InjectionToken"]('HighchartsModules');
var ChartService = (function () {
    /**
     * @param {?} chartModules
     */
    function ChartService(chartModules) {
        this.chartModules = chartModules;
    }
    /**
     * @return {?}
     */
    ChartService.prototype.initModules = function () {
        this.chartModules.forEach(function (chartModule) {
            chartModule(__WEBPACK_IMPORTED_MODULE_0_highcharts__);
        });
    };
    ChartService.decorators = [
        { type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"] },
    ];
    /**
     * @nocollapse
     */
    ChartService.ctorParameters = function () { return [
        { type: Array, decorators: [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Inject"], args: [HIGHCHARTS_MODULES,] },] },
    ]; };
    return ChartService;
}());

/**
 * @license
 * Copyright Felix Itzenplitz. All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at
 * https://github.com/cebor/angular-highcharts/blob/master/LICENSE
 */
var ChartModule = (function () {
    /**
     * @param {?} cs
     */
    function ChartModule(cs) {
        this.cs = cs;
        this.cs.initModules();
    }
    ChartModule.decorators = [
        { type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"], args: [{
                    exports: [ChartDirective],
                    declarations: [ChartDirective],
                    providers: [
                        { provide: HIGHCHARTS_MODULES, useValue: [] },
                        ChartService
                    ]
                },] },
    ];
    /**
     * @nocollapse
     */
    ChartModule.ctorParameters = function () { return [
        { type: ChartService, },
    ]; };
    return ChartModule;
}());

/**
 * @license
 * Copyright Felix Itzenplitz. All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at
 * https://github.com/cebor/angular-highcharts/blob/master/LICENSE
 */
/**
 * @module
 * @description
 * Entry point for all public APIs of this package.
 */

/**
 * Generated bundle index. Do not edit.
 */




/***/ }),

/***/ "../../../../highcharts/highcharts-more.src.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/**
 * @license Highcharts JS v6.0.4 (2017-12-15)
 *
 * (c) 2009-2016 Torstein Honsi
 *
 * License: www.highcharts.com/license
 */

(function(factory) {
    if (typeof module === 'object' && module.exports) {
        module.exports = factory;
    } else {
        factory(Highcharts);
    }
}(function(Highcharts) {
    (function(H) {
        /**
         * (c) 2010-2017 Torstein Honsi
         *
         * License: www.highcharts.com/license
         */
        var deg2rad = H.deg2rad,
            isNumber = H.isNumber,
            pick = H.pick,
            relativeLength = H.relativeLength;
        H.CenteredSeriesMixin = {
            /**
             * Get the center of the pie based on the size and center options relative to the
             * plot area. Borrowed by the polar and gauge series types.
             */
            getCenter: function() {

                var options = this.options,
                    chart = this.chart,
                    slicingRoom = 2 * (options.slicedOffset || 0),
                    handleSlicingRoom,
                    plotWidth = chart.plotWidth - 2 * slicingRoom,
                    plotHeight = chart.plotHeight - 2 * slicingRoom,
                    centerOption = options.center,
                    positions = [pick(centerOption[0], '50%'), pick(centerOption[1], '50%'), options.size || '100%', options.innerSize || 0],
                    smallestSize = Math.min(plotWidth, plotHeight),
                    i,
                    value;

                for (i = 0; i < 4; ++i) {
                    value = positions[i];
                    handleSlicingRoom = i < 2 || (i === 2 && /%$/.test(value));

                    // i == 0: centerX, relative to width
                    // i == 1: centerY, relative to height
                    // i == 2: size, relative to smallestSize
                    // i == 3: innerSize, relative to size
                    positions[i] = relativeLength(value, [plotWidth, plotHeight, smallestSize, positions[2]][i]) +
                        (handleSlicingRoom ? slicingRoom : 0);

                }
                // innerSize cannot be larger than size (#3632)
                if (positions[3] > positions[2]) {
                    positions[3] = positions[2];
                }
                return positions;
            },
            /**
             * getStartAndEndRadians - Calculates start and end angles in radians.
             * Used in series types such as pie and sunburst.
             *
             * @param  {Number} start Start angle in degrees.
             * @param  {Number} end Start angle in degrees.
             * @return {object} Returns an object containing start and end angles as
             * radians.
             */
            getStartAndEndRadians: function getStartAndEndRadians(start, end) {
                var startAngle = isNumber(start) ? start : 0, // must be a number
                    endAngle = (
                        (
                            isNumber(end) && // must be a number
                            end > startAngle && // must be larger than the start angle
                            // difference must be less than 360 degrees
                            (end - startAngle) < 360
                        ) ?
                        end :
                        startAngle + 360
                    ),
                    correction = -90;
                return {
                    start: deg2rad * (startAngle + correction),
                    end: deg2rad * (endAngle + correction)
                };
            }
        };

    }(Highcharts));
    (function(H) {
        /**
         * (c) 2010-2017 Torstein Honsi
         *
         * License: www.highcharts.com/license
         */
        var CenteredSeriesMixin = H.CenteredSeriesMixin,
            each = H.each,
            extend = H.extend,
            merge = H.merge,
            splat = H.splat;
        /**
         * The Pane object allows options that are common to a set of X and Y axes.
         *
         * In the future, this can be extended to basic Highcharts and Highstock.
         *
         */
        function Pane(options, chart) {
            this.init(options, chart);
        }

        // Extend the Pane prototype
        extend(Pane.prototype, {

            coll: 'pane', // Member of chart.pane

            /**
             * Initiate the Pane object
             */
            init: function(options, chart) {
                this.chart = chart;
                this.background = [];

                chart.pane.push(this);

                this.setOptions(options);
            },

            setOptions: function(options) {

                // Set options. Angular charts have a default background (#3318)
                this.options = options = merge(
                    this.defaultOptions,
                    this.chart.angular ? {
                        background: {}
                    } : undefined,
                    options
                );
            },

            /**
             * Render the pane with its backgrounds.
             */
            render: function() {

                var options = this.options,
                    backgroundOption = this.options.background,
                    renderer = this.chart.renderer,
                    len,
                    i;

                if (!this.group) {
                    this.group = renderer.g('pane-group')
                        .attr({
                            zIndex: options.zIndex || 0
                        })
                        .add();
                }

                this.updateCenter();

                // Render the backgrounds
                if (backgroundOption) {
                    backgroundOption = splat(backgroundOption);

                    len = Math.max(
                        backgroundOption.length,
                        this.background.length || 0
                    );

                    for (i = 0; i < len; i++) {
                        if (backgroundOption[i] && this.axis) { // #6641 - if axis exists, chart is circular and apply background
                            this.renderBackground(
                                merge(
                                    this.defaultBackgroundOptions,
                                    backgroundOption[i]
                                ),
                                i
                            );
                        } else if (this.background[i]) {
                            this.background[i] = this.background[i].destroy();
                            this.background.splice(i, 1);
                        }
                    }
                }
            },

            /**
             * Render an individual pane background.
             * @param  {Object} backgroundOptions Background options
             * @param  {number} i The index of the background in this.backgrounds
             */
            renderBackground: function(backgroundOptions, i) {
                var method = 'animate';

                if (!this.background[i]) {
                    this.background[i] = this.chart.renderer.path()
                        .add(this.group);
                    method = 'attr';
                }

                this.background[i][method]({
                    'd': this.axis.getPlotBandPath(
                        backgroundOptions.from,
                        backgroundOptions.to,
                        backgroundOptions
                    )
                }).attr({

                    'fill': backgroundOptions.backgroundColor,
                    'stroke': backgroundOptions.borderColor,
                    'stroke-width': backgroundOptions.borderWidth,

                    'class': 'highcharts-pane ' + (backgroundOptions.className || '')
                });

            },

            /**
             * The pane serves as a container for axes and backgrounds for circular 
             * gauges and polar charts.
             * @since 2.3.0
             * @optionparent pane
             */
            defaultOptions: {
                /**
                 * The center of a polar chart or angular gauge, given as an array
                 * of [x, y] positions. Positions can be given as integers that transform
                 * to pixels, or as percentages of the plot area size.
                 * 
                 * @type {Array<String|Number>}
                 * @sample {highcharts} highcharts/demo/gauge-vu-meter/
                 *         Two gauges with different center
                 * @default ["50%", "50%"]
                 * @since 2.3.0
                 * @product highcharts
                 */
                center: ['50%', '50%'],

                /**
                 * The size of the pane, either as a number defining pixels, or a
                 * percentage defining a percentage of the plot are.
                 * 
                 * @type {Number|String}
                 * @sample {highcharts} highcharts/demo/gauge-vu-meter/ Smaller size
                 * @default 85%
                 * @product highcharts
                 */
                size: '85%',

                /**
                 * The start angle of the polar X axis or gauge axis, given in degrees
                 * where 0 is north. Defaults to 0.
                 * 
                 * @type {Number}
                 * @sample {highcharts} highcharts/demo/gauge-vu-meter/
                 *         VU-meter with custom start and end angle
                 * @since 2.3.0
                 * @product highcharts
                 */
                startAngle: 0

                /**
                 * The end angle of the polar X axis or gauge value axis, given in degrees
                 * where 0 is north. Defaults to [startAngle](#pane.startAngle) + 360.
                 * 
                 * @type {Number}
                 * @sample {highcharts} highcharts/demo/gauge-vu-meter/
                 *         VU-meter with custom start and end angle
                 * @since 2.3.0
                 * @product highcharts
                 * @apioption pane.endAngle
                 */
            },

            /**
             * An array of background items for the pane.
             * @type Array.<Object>
             * @sample {highcharts} highcharts/demo/gauge-speedometer/
             *         Speedometer gauge with multiple backgrounds
             * @optionparent pane.background
             */
            defaultBackgroundOptions: {
                /**
                 * The class name for this background.
                 * 
                 * @type {String}
                 * @sample {highcharts} highcharts/css/pane/ Panes styled by CSS
                 * @sample {highstock} highcharts/css/pane/ Panes styled by CSS
                 * @sample {highmaps} highcharts/css/pane/ Panes styled by CSS
                 * @default highcharts-pane
                 * @since 5.0.0
                 * @apioption pane.background.className
                 */

                /**
                 * Tha shape of the pane background. When `solid`, the background
                 * is circular. When `arc`, the background extends only from the min
                 * to the max of the value axis.
                 * 
                 * @validvalue ["solid", "arc"]
                 * @type {String}
                 * @default solid
                 * @since 2.3.0
                 * @product highcharts
                 */
                shape: 'circle',


                /**
                 * The pixel border width of the pane background.
                 * 
                 * @type {Number}
                 * @default 1
                 * @since 2.3.0
                 * @product highcharts
                 */
                borderWidth: 1,

                /**
                 * The pane background border color.
                 * 
                 * @type {Color}
                 * @default #cccccc
                 * @since 2.3.0
                 * @product highcharts
                 */
                borderColor: '#cccccc',

                /**
                 * The background color or gradient for the pane.
                 * 
                 * @type {Color}
                 * @since 2.3.0
                 * @product highcharts
                 */
                backgroundColor: {
                    /**
                     * Definition of the gradient, similar to SVG: object literal holds
                     * start position (x1, y1) and the end position (x2, y2) relative
                     * to the shape, where 0 means top/left and 1 is bottom/right.
                     * All positions are floats between 0 and 1.
                     *
                     * @type {Object}
                     */
                    linearGradient: {
                        x1: 0,
                        y1: 0,
                        x2: 0,
                        y2: 1
                    },
                    /**
                     * The stops is an array of tuples, where the first
                     * item is a float between 0 and 1 assigning the relative position in
                     * the gradient, and the second item is the color.
                     *
                     * @default [[0, #ffffff], [1, #e6e6e6]]
                     * @type {Array<Array>}
                     */
                    stops: [
                        [0, '#ffffff'],
                        [1, '#e6e6e6']
                    ]
                },


                /** @ignore */
                from: -Number.MAX_VALUE, // corrected to axis min

                /**
                 * The inner radius of the pane background. Can be either numeric
                 * (pixels) or a percentage string.
                 * 
                 * @type {Number|String}
                 * @default 0
                 * @since 2.3.0
                 * @product highcharts
                 */
                innerRadius: 0,

                /** @ignore */
                to: Number.MAX_VALUE, // corrected to axis max

                /**
                 * The outer radius of the circular pane background. Can be either
                 * numeric (pixels) or a percentage string.
                 * 
                 * @type {Number|String}
                 * @default 105%
                 * @since 2.3.0
                 * @product highcharts
                 */
                outerRadius: '105%'
            },

            /**
             * Gets the center for the pane and its axis.
             */
            updateCenter: function(axis) {
                this.center = (axis || this.axis || {}).center =
                    CenteredSeriesMixin.getCenter.call(this);
            },

            /**
             * Destroy the pane item
             * /
            destroy: function () {
            	H.erase(this.chart.pane, this);
            	each(this.background, function (background) {
            		background.destroy();
            	});
            	this.background.length = 0;
            	this.group = this.group.destroy();
            },
            */

            /**
             * Update the pane item with new options
             * @param  {Object} options New pane options
             */
            update: function(options, redraw) {

                merge(true, this.options, options);
                this.setOptions(this.options);
                this.render();
                each(this.chart.axes, function(axis) {
                    if (axis.pane === this) {
                        axis.pane = null;
                        axis.update({}, redraw);
                    }
                }, this);
            }

        });

        H.Pane = Pane;

    }(Highcharts));
    (function(H) {
        /**
         * (c) 2010-2017 Torstein Honsi
         *
         * License: www.highcharts.com/license
         */
        var Axis = H.Axis,
            each = H.each,
            extend = H.extend,
            map = H.map,
            merge = H.merge,
            noop = H.noop,
            pick = H.pick,
            pInt = H.pInt,
            Tick = H.Tick,
            wrap = H.wrap,


            hiddenAxisMixin, // @todo Extract this to a new file
            radialAxisMixin, // @todo Extract this to a new file
            axisProto = Axis.prototype,
            tickProto = Tick.prototype;

        /**
         * Augmented methods for the x axis in order to hide it completely, used for the X axis in gauges
         */
        hiddenAxisMixin = {
            getOffset: noop,
            redraw: function() {
                this.isDirty = false; // prevent setting Y axis dirty
            },
            render: function() {
                this.isDirty = false; // prevent setting Y axis dirty
            },
            setScale: noop,
            setCategories: noop,
            setTitle: noop
        };

        /**
         * Augmented methods for the value axis
         */
        radialAxisMixin = {

            /**
             * The default options extend defaultYAxisOptions
             */
            defaultRadialGaugeOptions: {
                labels: {
                    align: 'center',
                    x: 0,
                    y: null // auto
                },
                minorGridLineWidth: 0,
                minorTickInterval: 'auto',
                minorTickLength: 10,
                minorTickPosition: 'inside',
                minorTickWidth: 1,
                tickLength: 10,
                tickPosition: 'inside',
                tickWidth: 2,
                title: {
                    rotation: 0
                },
                zIndex: 2 // behind dials, points in the series group
            },

            // Circular axis around the perimeter of a polar chart
            defaultRadialXOptions: {
                gridLineWidth: 1, // spokes
                labels: {
                    align: null, // auto
                    distance: 15,
                    x: 0,
                    y: null, // auto
                    style: {
                        textOverflow: 'none' // wrap lines by default (#7248)
                    }
                },
                maxPadding: 0,
                minPadding: 0,
                showLastLabel: false,
                tickLength: 0
            },

            // Radial axis, like a spoke in a polar chart
            defaultRadialYOptions: {
                gridLineInterpolation: 'circle',
                labels: {
                    align: 'right',
                    x: -3,
                    y: -2
                },
                showLastLabel: false,
                title: {
                    x: 4,
                    text: null,
                    rotation: 90
                }
            },

            /**
             * Merge and set options
             */
            setOptions: function(userOptions) {

                var options = this.options = merge(
                    this.defaultOptions,
                    this.defaultRadialOptions,
                    userOptions
                );

                // Make sure the plotBands array is instanciated for each Axis (#2649)
                if (!options.plotBands) {
                    options.plotBands = [];
                }

            },

            /**
             * Wrap the getOffset method to return zero offset for title or labels in a radial
             * axis
             */
            getOffset: function() {
                // Call the Axis prototype method (the method we're in now is on the instance)
                axisProto.getOffset.call(this);

                // Title or label offsets are not counted
                this.chart.axisOffset[this.side] = 0;

            },


            /**
             * Get the path for the axis line. This method is also referenced in the getPlotLinePath
             * method.
             */
            getLinePath: function(lineWidth, radius) {
                var center = this.center,
                    end,
                    chart = this.chart,
                    r = pick(radius, center[2] / 2 - this.offset),
                    path;

                if (this.isCircular || radius !== undefined) {
                    path = this.chart.renderer.symbols.arc(
                        this.left + center[0],
                        this.top + center[1],
                        r,
                        r, {
                            start: this.startAngleRad,
                            end: this.endAngleRad,
                            open: true,
                            innerR: 0
                        }
                    );

                    // Bounds used to position the plotLine label next to the line
                    // (#7117)
                    path.xBounds = [this.left + center[0]];
                    path.yBounds = [this.top + center[1] - r];

                } else {
                    end = this.postTranslate(this.angleRad, r);
                    path = ['M', center[0] + chart.plotLeft, center[1] + chart.plotTop, 'L', end.x, end.y];
                }
                return path;
            },

            /**
             * Override setAxisTranslation by setting the translation to the difference
             * in rotation. This allows the translate method to return angle for
             * any given value.
             */
            setAxisTranslation: function() {

                // Call uber method
                axisProto.setAxisTranslation.call(this);

                // Set transA and minPixelPadding
                if (this.center) { // it's not defined the first time
                    if (this.isCircular) {

                        this.transA = (this.endAngleRad - this.startAngleRad) /
                            ((this.max - this.min) || 1);


                    } else {
                        this.transA = (this.center[2] / 2) / ((this.max - this.min) || 1);
                    }

                    if (this.isXAxis) {
                        this.minPixelPadding = this.transA * this.minPointOffset;
                    } else {
                        // This is a workaround for regression #2593, but categories still don't position correctly.
                        this.minPixelPadding = 0;
                    }
                }
            },

            /**
             * In case of auto connect, add one closestPointRange to the max value right before
             * tickPositions are computed, so that ticks will extend passed the real max.
             */
            beforeSetTickPositions: function() {
                // If autoConnect is true, polygonal grid lines are connected, and one closestPointRange
                // is added to the X axis to prevent the last point from overlapping the first.
                this.autoConnect = this.isCircular && pick(this.userMax, this.options.max) === undefined &&
                    this.endAngleRad - this.startAngleRad === 2 * Math.PI;

                if (this.autoConnect) {
                    this.max += (this.categories && 1) || this.pointRange || this.closestPointRange || 0; // #1197, #2260
                }
            },

            /**
             * Override the setAxisSize method to use the arc's circumference as length. This
             * allows tickPixelInterval to apply to pixel lengths along the perimeter
             */
            setAxisSize: function() {

                axisProto.setAxisSize.call(this);

                if (this.isRadial) {

                    // Set the center array
                    this.pane.updateCenter(this);

                    // The sector is used in Axis.translate to compute the translation of reversed axis points (#2570)
                    if (this.isCircular) {
                        this.sector = this.endAngleRad - this.startAngleRad;
                    }

                    // Axis len is used to lay out the ticks
                    this.len = this.width = this.height = this.center[2] * pick(this.sector, 1) / 2;


                }
            },

            /**
             * Returns the x, y coordinate of a point given by a value and a pixel distance
             * from center
             */
            getPosition: function(value, length) {
                return this.postTranslate(
                    this.isCircular ? this.translate(value) : this.angleRad, // #2848
                    pick(this.isCircular ? length : this.translate(value), this.center[2] / 2) - this.offset
                );
            },

            /**
             * Translate from intermediate plotX (angle), plotY (axis.len - radius) to final chart coordinates.
             */
            postTranslate: function(angle, radius) {

                var chart = this.chart,
                    center = this.center;

                angle = this.startAngleRad + angle;

                return {
                    x: chart.plotLeft + center[0] + Math.cos(angle) * radius,
                    y: chart.plotTop + center[1] + Math.sin(angle) * radius
                };

            },

            /**
             * Find the path for plot bands along the radial axis
             */
            getPlotBandPath: function(from, to, options) {
                var center = this.center,
                    startAngleRad = this.startAngleRad,
                    fullRadius = center[2] / 2,
                    radii = [
                        pick(options.outerRadius, '100%'),
                        options.innerRadius,
                        pick(options.thickness, 10)
                    ],
                    offset = Math.min(this.offset, 0),
                    percentRegex = /%$/,
                    start,
                    end,
                    open,
                    isCircular = this.isCircular, // X axis in a polar chart
                    ret;

                // Polygonal plot bands
                if (this.options.gridLineInterpolation === 'polygon') {
                    ret = this.getPlotLinePath(from).concat(this.getPlotLinePath(to, true));

                    // Circular grid bands
                } else {

                    // Keep within bounds
                    from = Math.max(from, this.min);
                    to = Math.min(to, this.max);

                    // Plot bands on Y axis (radial axis) - inner and outer radius depend on to and from
                    if (!isCircular) {
                        radii[0] = this.translate(from);
                        radii[1] = this.translate(to);
                    }

                    // Convert percentages to pixel values
                    radii = map(radii, function(radius) {
                        if (percentRegex.test(radius)) {
                            radius = (pInt(radius, 10) * fullRadius) / 100;
                        }
                        return radius;
                    });

                    // Handle full circle
                    if (options.shape === 'circle' || !isCircular) {
                        start = -Math.PI / 2;
                        end = Math.PI * 1.5;
                        open = true;
                    } else {
                        start = startAngleRad + this.translate(from);
                        end = startAngleRad + this.translate(to);
                    }

                    radii[0] -= offset; // #5283
                    radii[2] -= offset; // #5283

                    ret = this.chart.renderer.symbols.arc(
                        this.left + center[0],
                        this.top + center[1],
                        radii[0],
                        radii[0], {
                            start: Math.min(start, end), // Math is for reversed yAxis (#3606)
                            end: Math.max(start, end),
                            innerR: pick(radii[1], radii[0] - radii[2]),
                            open: open
                        }
                    );
                }

                return ret;
            },

            /**
             * Find the path for plot lines perpendicular to the radial axis.
             */
            getPlotLinePath: function(value, reverse) {
                var axis = this,
                    center = axis.center,
                    chart = axis.chart,
                    end = axis.getPosition(value),
                    xAxis,
                    xy,
                    tickPositions,
                    ret;

                // Spokes
                if (axis.isCircular) {
                    ret = ['M', center[0] + chart.plotLeft, center[1] + chart.plotTop, 'L', end.x, end.y];

                    // Concentric circles
                } else if (axis.options.gridLineInterpolation === 'circle') {
                    value = axis.translate(value);
                    if (value) { // a value of 0 is in the center
                        ret = axis.getLinePath(0, value);
                    }
                    // Concentric polygons
                } else {
                    // Find the X axis in the same pane
                    each(chart.xAxis, function(a) {
                        if (a.pane === axis.pane) {
                            xAxis = a;
                        }
                    });
                    ret = [];
                    value = axis.translate(value);
                    tickPositions = xAxis.tickPositions;
                    if (xAxis.autoConnect) {
                        tickPositions = tickPositions.concat([tickPositions[0]]);
                    }
                    // Reverse the positions for concatenation of polygonal plot bands
                    if (reverse) {
                        tickPositions = [].concat(tickPositions).reverse();
                    }

                    each(tickPositions, function(pos, i) {
                        xy = xAxis.getPosition(pos, value);
                        ret.push(i ? 'L' : 'M', xy.x, xy.y);
                    });

                }
                return ret;
            },

            /**
             * Find the position for the axis title, by default inside the gauge
             */
            getTitlePosition: function() {
                var center = this.center,
                    chart = this.chart,
                    titleOptions = this.options.title;

                return {
                    x: chart.plotLeft + center[0] + (titleOptions.x || 0),
                    y: chart.plotTop + center[1] - ({
                            high: 0.5,
                            middle: 0.25,
                            low: 0
                        }[titleOptions.align] *
                        center[2]) + (titleOptions.y || 0)
                };
            }

        };

        /**
         * Override axisProto.init to mix in special axis instance functions and function overrides
         */
        wrap(axisProto, 'init', function(proceed, chart, userOptions) {
            var angular = chart.angular,
                polar = chart.polar,
                isX = userOptions.isX,
                isHidden = angular && isX,
                isCircular,
                options,
                chartOptions = chart.options,
                paneIndex = userOptions.pane || 0,
                pane = this.pane = chart.pane && chart.pane[paneIndex],
                paneOptions = pane && pane.options;

            // Before prototype.init
            if (angular) {
                extend(this, isHidden ? hiddenAxisMixin : radialAxisMixin);
                isCircular = !isX;
                if (isCircular) {
                    this.defaultRadialOptions = this.defaultRadialGaugeOptions;
                }

            } else if (polar) {
                extend(this, radialAxisMixin);
                isCircular = isX;
                this.defaultRadialOptions = isX ? this.defaultRadialXOptions : merge(this.defaultYAxisOptions, this.defaultRadialYOptions);

            }

            // Disable certain features on angular and polar axes
            if (angular || polar) {
                this.isRadial = true;
                chart.inverted = false;
                chartOptions.chart.zoomType = null;
            } else {
                this.isRadial = false;
            }

            // A pointer back to this axis to borrow geometry
            if (pane && isCircular) {
                pane.axis = this;
            }

            // Run prototype.init
            proceed.call(this, chart, userOptions);

            if (!isHidden && pane && (angular || polar)) {
                options = this.options;

                // Start and end angle options are
                // given in degrees relative to top, while internal computations are
                // in radians relative to right (like SVG).
                this.angleRad = (options.angle || 0) * Math.PI / 180; // Y axis in polar charts
                this.startAngleRad = (paneOptions.startAngle - 90) * Math.PI / 180; // Gauges
                this.endAngleRad = (pick(paneOptions.endAngle, paneOptions.startAngle + 360) - 90) * Math.PI / 180; // Gauges
                this.offset = options.offset || 0;

                this.isCircular = isCircular;

            }

        });

        /**
         * Wrap auto label align to avoid setting axis-wide rotation on radial axes (#4920)
         * @param   {Function} proceed
         * @returns {String} Alignment
         */
        wrap(axisProto, 'autoLabelAlign', function(proceed) {
            if (!this.isRadial) {
                return proceed.apply(this, [].slice.call(arguments, 1));
            } // else return undefined
        });

        /**
         * Add special cases within the Tick class' methods for radial axes.
         */
        wrap(tickProto, 'getPosition', function(proceed, horiz, pos, tickmarkOffset, old) {
            var axis = this.axis;

            return axis.getPosition ?
                axis.getPosition(pos) :
                proceed.call(this, horiz, pos, tickmarkOffset, old);
        });

        /**
         * Wrap the getLabelPosition function to find the center position of the label
         * based on the distance option
         */
        wrap(tickProto, 'getLabelPosition', function(proceed, x, y, label, horiz, labelOptions, tickmarkOffset, index, step) {
            var axis = this.axis,
                optionsY = labelOptions.y,
                ret,
                centerSlot = 20, // 20 degrees to each side at the top and bottom
                align = labelOptions.align,
                angle = ((axis.translate(this.pos) + axis.startAngleRad + Math.PI / 2) / Math.PI * 180) % 360;

            if (axis.isRadial) { // Both X and Y axes in a polar chart
                ret = axis.getPosition(this.pos, (axis.center[2] / 2) + pick(labelOptions.distance, -25));

                // Automatically rotated
                if (labelOptions.rotation === 'auto') {
                    label.attr({
                        rotation: angle
                    });

                    // Vertically centered
                } else if (optionsY === null) {
                    optionsY = axis.chart.renderer.fontMetrics(label.styles.fontSize).b - label.getBBox().height / 2;
                }

                // Automatic alignment
                if (align === null) {
                    if (axis.isCircular) { // Y axis
                        if (this.label.getBBox().width > axis.len * axis.tickInterval / (axis.max - axis.min)) { // #3506
                            centerSlot = 0;
                        }
                        if (angle > centerSlot && angle < 180 - centerSlot) {
                            align = 'left'; // right hemisphere
                        } else if (angle > 180 + centerSlot && angle < 360 - centerSlot) {
                            align = 'right'; // left hemisphere
                        } else {
                            align = 'center'; // top or bottom
                        }
                    } else {
                        align = 'center';
                    }
                    label.attr({
                        align: align
                    });
                }

                ret.x += labelOptions.x;
                ret.y += optionsY;

            } else {
                ret = proceed.call(this, x, y, label, horiz, labelOptions, tickmarkOffset, index, step);
            }
            return ret;
        });

        /**
         * Wrap the getMarkPath function to return the path of the radial marker
         */
        wrap(tickProto, 'getMarkPath', function(proceed, x, y, tickLength, tickWidth, horiz, renderer) {
            var axis = this.axis,
                endPoint,
                ret;

            if (axis.isRadial) {
                endPoint = axis.getPosition(this.pos, axis.center[2] / 2 + tickLength);
                ret = [
                    'M',
                    x,
                    y,
                    'L',
                    endPoint.x,
                    endPoint.y
                ];
            } else {
                ret = proceed.call(this, x, y, tickLength, tickWidth, horiz, renderer);
            }
            return ret;
        });

    }(Highcharts));
    (function(H) {
        /**
         * (c) 2010-2017 Torstein Honsi
         *
         * License: www.highcharts.com/license
         */
        var each = H.each,
            noop = H.noop,
            pick = H.pick,
            defined = H.defined,
            Series = H.Series,
            seriesType = H.seriesType,
            seriesTypes = H.seriesTypes,
            seriesProto = Series.prototype,
            pointProto = H.Point.prototype;

        /**
         * The area range series is a carteseian series with higher and lower values
         * for each point along an X axis, where the area between the values is shaded.
         * Requires `highcharts-more.js`.
         * 
         * @extends plotOptions.area
         * @product highcharts highstock
         * @sample {highcharts} highcharts/demo/arearange/ Area range chart
         * @sample {highstock} stock/demo/arearange/ Area range chart
         * @optionparent plotOptions.arearange
         */
        seriesType('arearange', 'area', {


            /**
             * Pixel width of the arearange graph line.
             * 
             * @type {Number}
             * @default 1
             * @since 2.3.0
             * @product highcharts highstock
             */
            lineWidth: 1,


            /**
             * @default null
             */
            threshold: null,

            tooltip: {


                pointFormat: '<span style="color:{series.color}">\u25CF</span> {series.name}: <b>{point.low}</b> - <b>{point.high}</b><br/>' // eslint-disable-line no-dupe-keys

            },

            /**
             * Whether the whole area or just the line should respond to mouseover
             * tooltips and other mouse or touch events.
             * 
             * @type {Boolean}
             * @default true
             * @since 2.3.0
             * @product highcharts highstock
             */
            trackByArea: true,

            /**
             * Extended data labels for range series types. Range series data labels
             * have no `x` and `y` options. Instead, they have `xLow`, `xHigh`,
             * `yLow` and `yHigh` options to allow the higher and lower data label
             * sets individually.
             * 
             * @type {Object}
             * @extends plotOptions.series.dataLabels
             * @excluding x,y
             * @since 2.3.0
             * @product highcharts highstock
             */
            dataLabels: {

                align: null,
                verticalAlign: null,

                /**
                 * X offset of the lower data labels relative to the point value.
                 * 
                 * @type {Number}
                 * @sample {highcharts} highcharts/plotoptions/arearange-datalabels/ Data labels on range series
                 * @sample {highstock} highcharts/plotoptions/arearange-datalabels/ Data labels on range series
                 * @default 0
                 * @since 2.3.0
                 * @product highcharts highstock
                 */
                xLow: 0,

                /**
                 * X offset of the higher data labels relative to the point value.
                 * 
                 * @type {Number}
                 * @sample {highcharts} highcharts/plotoptions/arearange-datalabels/ Data labels on range series
                 * @sample {highstock} highcharts/plotoptions/arearange-datalabels/ Data labels on range series
                 * @default 0
                 * @since 2.3.0
                 * @product highcharts highstock
                 */
                xHigh: 0,

                /**
                 * Y offset of the lower data labels relative to the point value.
                 * 
                 * @type {Number}
                 * @sample {highcharts} highcharts/plotoptions/arearange-datalabels/ Data labels on range series
                 * @sample {highstock} highcharts/plotoptions/arearange-datalabels/ Data labels on range series
                 * @default 16
                 * @since 2.3.0
                 * @product highcharts highstock
                 */
                yLow: 0,

                /**
                 * Y offset of the higher data labels relative to the point value.
                 * 
                 * @type {Number}
                 * @sample {highcharts} highcharts/plotoptions/arearange-datalabels/ Data labels on range series
                 * @sample {highstock} highcharts/plotoptions/arearange-datalabels/ Data labels on range series
                 * @default -6
                 * @since 2.3.0
                 * @product highcharts highstock
                 */
                yHigh: 0
            }

            /**
             * Whether to apply a drop shadow to the graph line. Since 2.3 the shadow
             * can be an object configuration containing `color`, `offsetX`, `offsetY`,
             *  `opacity` and `width`.
             * 
             * @type {Boolean|Object}
             * @product highcharts
             * @apioption plotOptions.arearange.shadow
             */

            // Prototype members
        }, {
            pointArrayMap: ['low', 'high'],
            dataLabelCollections: ['dataLabel', 'dataLabelUpper'],
            toYData: function(point) {
                return [point.low, point.high];
            },
            pointValKey: 'low',
            deferTranslatePolar: true,

            /**
             * Translate a point's plotHigh from the internal angle and radius measures to
             * true plotHigh coordinates. This is an addition of the toXY method found in
             * Polar.js, because it runs too early for arearanges to be considered (#3419).
             */
            highToXY: function(point) {
                // Find the polar plotX and plotY
                var chart = this.chart,
                    xy = this.xAxis.postTranslate(point.rectPlotX, this.yAxis.len - point.plotHigh);
                point.plotHighX = xy.x - chart.plotLeft;
                point.plotHigh = xy.y - chart.plotTop;
                point.plotLowX = point.plotX;
            },

            /**
             * Translate data points from raw values x and y to plotX and plotY
             */
            translate: function() {
                var series = this,
                    yAxis = series.yAxis,
                    hasModifyValue = !!series.modifyValue;

                seriesTypes.area.prototype.translate.apply(series);

                // Set plotLow and plotHigh
                each(series.points, function(point) {

                    var low = point.low,
                        high = point.high,
                        plotY = point.plotY;

                    if (high === null || low === null) {
                        point.isNull = true;
                        point.plotY = null;
                    } else {
                        point.plotLow = plotY;
                        point.plotHigh = yAxis.translate(
                            hasModifyValue ? series.modifyValue(high, point) : high,
                            0,
                            1,
                            0,
                            1
                        );
                        if (hasModifyValue) {
                            point.yBottom = point.plotHigh;
                        }
                    }
                });

                // Postprocess plotHigh
                if (this.chart.polar) {
                    each(this.points, function(point) {
                        series.highToXY(point);
                        point.tooltipPos = [
                            (point.plotHighX + point.plotLowX) / 2,
                            (point.plotHigh + point.plotLow) / 2
                        ];
                    });
                }
            },

            /**
             * Extend the line series' getSegmentPath method by applying the segment
             * path to both lower and higher values of the range
             */
            getGraphPath: function(points) {

                var highPoints = [],
                    highAreaPoints = [],
                    i,
                    getGraphPath = seriesTypes.area.prototype.getGraphPath,
                    point,
                    pointShim,
                    linePath,
                    lowerPath,
                    options = this.options,
                    connectEnds = this.chart.polar && options.connectEnds !== false,
                    connectNulls = options.connectNulls,
                    step = options.step,
                    higherPath,
                    higherAreaPath;

                points = points || this.points;
                i = points.length;

                // Create the top line and the top part of the area fill. The area fill compensates for 
                // null points by drawing down to the lower graph, moving across the null gap and 
                // starting again at the lower graph.
                i = points.length;
                while (i--) {
                    point = points[i];

                    if (!point.isNull &&
                        !connectEnds &&
                        !connectNulls &&
                        (!points[i + 1] || points[i + 1].isNull)
                    ) {
                        highAreaPoints.push({
                            plotX: point.plotX,
                            plotY: point.plotY,
                            doCurve: false // #5186, gaps in areasplinerange fill
                        });
                    }

                    pointShim = {
                        polarPlotY: point.polarPlotY,
                        rectPlotX: point.rectPlotX,
                        yBottom: point.yBottom,
                        plotX: pick(point.plotHighX, point.plotX), // plotHighX is for polar charts
                        plotY: point.plotHigh,
                        isNull: point.isNull
                    };

                    highAreaPoints.push(pointShim);

                    highPoints.push(pointShim);

                    if (!point.isNull &&
                        !connectEnds &&
                        !connectNulls &&
                        (!points[i - 1] || points[i - 1].isNull)
                    ) {
                        highAreaPoints.push({
                            plotX: point.plotX,
                            plotY: point.plotY,
                            doCurve: false // #5186, gaps in areasplinerange fill
                        });
                    }
                }

                // Get the paths
                lowerPath = getGraphPath.call(this, points);
                if (step) {
                    if (step === true) {
                        step = 'left';
                    }
                    options.step = {
                        left: 'right',
                        center: 'center',
                        right: 'left'
                    }[step]; // swap for reading in getGraphPath
                }
                higherPath = getGraphPath.call(this, highPoints);
                higherAreaPath = getGraphPath.call(this, highAreaPoints);
                options.step = step;

                // Create a line on both top and bottom of the range
                linePath = [].concat(lowerPath, higherPath);

                // For the area path, we need to change the 'move' statement into 'lineTo' or 'curveTo'
                if (!this.chart.polar && higherAreaPath[0] === 'M') {
                    higherAreaPath[0] = 'L'; // this probably doesn't work for spline			
                }

                this.graphPath = linePath;
                this.areaPath = lowerPath.concat(higherAreaPath);

                // Prepare for sideways animation
                linePath.isArea = true;
                linePath.xMap = lowerPath.xMap;
                this.areaPath.xMap = lowerPath.xMap;

                return linePath;
            },

            /**
             * Extend the basic drawDataLabels method by running it for both lower and higher
             * values.
             */
            drawDataLabels: function() {

                var data = this.data,
                    length = data.length,
                    i,
                    originalDataLabels = [],
                    dataLabelOptions = this.options.dataLabels,
                    align = dataLabelOptions.align,
                    verticalAlign = dataLabelOptions.verticalAlign,
                    inside = dataLabelOptions.inside,
                    point,
                    up,
                    inverted = this.chart.inverted;

                if (dataLabelOptions.enabled || this._hasPointLabels) {

                    // Step 1: set preliminary values for plotY and dataLabel and draw the upper labels
                    i = length;
                    while (i--) {
                        point = data[i];
                        if (point) {
                            up = inside ? point.plotHigh < point.plotLow : point.plotHigh > point.plotLow;

                            // Set preliminary values
                            point.y = point.high;
                            point._plotY = point.plotY;
                            point.plotY = point.plotHigh;

                            // Store original data labels and set preliminary label objects to be picked up
                            // in the uber method
                            originalDataLabels[i] = point.dataLabel;
                            point.dataLabel = point.dataLabelUpper;

                            // Set the default offset
                            point.below = up;
                            if (inverted) {
                                if (!align) {
                                    dataLabelOptions.align = up ? 'right' : 'left';
                                }
                            } else {
                                if (!verticalAlign) {
                                    dataLabelOptions.verticalAlign = up ? 'top' : 'bottom';
                                }
                            }

                            dataLabelOptions.x = dataLabelOptions.xHigh;
                            dataLabelOptions.y = dataLabelOptions.yHigh;
                        }
                    }

                    if (seriesProto.drawDataLabels) {
                        seriesProto.drawDataLabels.apply(this, arguments); // #1209
                    }

                    // Step 2: reorganize and handle data labels for the lower values
                    i = length;
                    while (i--) {
                        point = data[i];
                        if (point) {
                            up = inside ? point.plotHigh < point.plotLow : point.plotHigh > point.plotLow;

                            // Move the generated labels from step 1, and reassign the original data labels
                            point.dataLabelUpper = point.dataLabel;
                            point.dataLabel = originalDataLabels[i];

                            // Reset values
                            point.y = point.low;
                            point.plotY = point._plotY;

                            // Set the default offset
                            point.below = !up;
                            if (inverted) {
                                if (!align) {
                                    dataLabelOptions.align = up ? 'left' : 'right';
                                }
                            } else {
                                if (!verticalAlign) {
                                    dataLabelOptions.verticalAlign = up ? 'bottom' : 'top';
                                }

                            }

                            dataLabelOptions.x = dataLabelOptions.xLow;
                            dataLabelOptions.y = dataLabelOptions.yLow;
                        }
                    }
                    if (seriesProto.drawDataLabels) {
                        seriesProto.drawDataLabels.apply(this, arguments);
                    }
                }

                dataLabelOptions.align = align;
                dataLabelOptions.verticalAlign = verticalAlign;
            },

            alignDataLabel: function() {
                seriesTypes.column.prototype.alignDataLabel.apply(this, arguments);
            },

            drawPoints: function() {
                var series = this,
                    pointLength = series.points.length,
                    point,
                    i;

                // Draw bottom points
                seriesProto.drawPoints.apply(series, arguments);

                i = 0;
                while (i < pointLength) {
                    point = series.points[i];
                    point.lowerGraphic = point.graphic;
                    point.graphic = point.upperGraphic;
                    point._plotY = point.plotY;
                    point._plotX = point.plotX;
                    point.plotY = point.plotHigh;
                    if (defined(point.plotHighX)) {
                        point.plotX = point.plotHighX;
                    }
                    i++;
                }

                // Draw top points
                seriesProto.drawPoints.apply(series, arguments);

                i = 0;
                while (i < pointLength) {
                    point = series.points[i];
                    point.upperGraphic = point.graphic;
                    point.graphic = point.lowerGraphic;
                    point.plotY = point._plotY;
                    point.plotX = point._plotX;
                    i++;
                }
            },

            setStackedPoints: noop
        }, {
            setState: function() {
                var prevState = this.state,
                    series = this.series,
                    isPolar = series.chart.polar;


                if (!defined(this.plotHigh)) {
                    // Boost doesn't calculate plotHigh
                    this.plotHigh = series.yAxis.toPixels(this.high, true);
                }

                if (!defined(this.plotLow)) {
                    // Boost doesn't calculate plotLow
                    this.plotLow = this.plotY = series.yAxis.toPixels(this.low, true);
                }

                // Bottom state:
                pointProto.setState.apply(this, arguments);

                // Change state also for the top marker
                this.graphic = this.upperGraphic;
                this.plotY = this.plotHigh;

                if (isPolar) {
                    this.plotX = this.plotHighX;
                }

                this.state = prevState;

                if (series.stateMarkerGraphic) {
                    series.lowerStateMarkerGraphic = series.stateMarkerGraphic;
                    series.stateMarkerGraphic = series.upperStateMarkerGraphic;
                }

                pointProto.setState.apply(this, arguments);

                // Now restore defaults
                this.plotY = this.plotLow;
                this.graphic = this.lowerGraphic;

                if (isPolar) {
                    this.plotX = this.plotLowX;
                }

                if (series.stateMarkerGraphic) {
                    series.upperStateMarkerGraphic = series.stateMarkerGraphic;
                    series.stateMarkerGraphic = series.lowerStateMarkerGraphic;
                    // Lower marker is stored at stateMarkerGraphic
                    // to avoid reference duplication (#7021)
                    series.lowerStateMarkerGraphic = undefined;
                }
            },
            haloPath: function() {
                var isPolar = this.series.chart.polar,
                    path = [];

                // Bottom halo
                this.plotY = this.plotLow;
                if (isPolar) {
                    this.plotX = this.plotLowX;
                }

                path = pointProto.haloPath.apply(this, arguments);

                // Top halo
                this.plotY = this.plotHigh;
                if (isPolar) {
                    this.plotX = this.plotHighX;
                }
                path = path.concat(
                    pointProto.haloPath.apply(this, arguments)
                );

                return path;
            },
            destroy: function() {
                if (this.upperGraphic) {
                    this.upperGraphic = this.upperGraphic.destroy();
                }
                return pointProto.destroy.apply(this, arguments);
            }
        });


        /**
         * A `arearange` series. If the [type](#series.arearange.type) option
         * is not specified, it is inherited from [chart.type](#chart.type).
         * 
         * 
         * For options that apply to multiple series, it is recommended to add
         * them to the [plotOptions.series](#plotOptions.series) options structure.
         * To apply to all series of this specific type, apply it to [plotOptions.
         * arearange](#plotOptions.arearange).
         * 
         * @type {Object}
         * @extends series,plotOptions.arearange
         * @excluding dataParser,dataURL,stack
         * @product highcharts highstock
         * @apioption series.arearange
         */

        /**
         * An array of data points for the series. For the `arearange` series
         * type, points can be given in the following ways:
         * 
         * 1.  An array of arrays with 3 or 2 values. In this case, the values
         * correspond to `x,low,high`. If the first value is a string, it is
         * applied as the name of the point, and the `x` value is inferred.
         * The `x` value can also be omitted, in which case the inner arrays
         * should be of length 2\. Then the `x` value is automatically calculated,
         * either starting at 0 and incremented by 1, or from `pointStart`
         * and `pointInterval` given in the series options.
         * 
         *  ```js
         *     data: [
         *         [0, 8, 3],
         *         [1, 1, 1],
         *         [2, 6, 8]
         *     ]
         *  ```
         * 
         * 2.  An array of objects with named values. The objects are point
         * configuration objects as seen below. If the total number of data
         * points exceeds the series' [turboThreshold](#series.arearange.turboThreshold),
         * this option is not available.
         * 
         *  ```js
         *     data: [{
         *         x: 1,
         *         low: 9,
         *         high: 0,
         *         name: "Point2",
         *         color: "#00FF00"
         *     }, {
         *         x: 1,
         *         low: 3,
         *         high: 4,
         *         name: "Point1",
         *         color: "#FF00FF"
         *     }]
         *  ```
         * 
         * @type {Array<Object|Array>}
         * @extends series.line.data
         * @excluding marker,y
         * @sample {highcharts} highcharts/chart/reflow-true/ Numerical values
         * @sample {highcharts} highcharts/series/data-array-of-arrays/ Arrays of numeric x and y
         * @sample {highcharts} highcharts/series/data-array-of-arrays-datetime/ Arrays of datetime x and y
         * @sample {highcharts} highcharts/series/data-array-of-name-value/ Arrays of point.name and y
         * @sample {highcharts} highcharts/series/data-array-of-objects/ Config objects
         * @product highcharts highstock
         * @apioption series.arearange.data
         */

        /**
         * The high or maximum value for each data point.
         * 
         * @type {Number}
         * @product highcharts highstock
         * @apioption series.arearange.data.high
         */

        /**
         * The low or minimum value for each data point.
         * 
         * @type {Number}
         * @product highcharts highstock
         * @apioption series.arearange.data.low
         */

    }(Highcharts));
    (function(H) {
        /**
         * (c) 2010-2017 Torstein Honsi
         *
         * License: www.highcharts.com/license
         */

        var seriesType = H.seriesType,
            seriesTypes = H.seriesTypes;

        /**
         * The area spline range is a cartesian series type with higher and
         * lower Y values along an X axis. The area inside the range is colored, and
         * the graph outlining the area is a smoothed spline. Requires
         * `highcharts-more.js`.
         * 
         * @extends plotOptions.arearange
         * @excluding step
         * @since 2.3.0
         * @sample {highstock} stock/demo/areasplinerange/ Area spline range
         * @sample {highstock} stock/demo/areasplinerange/ Area spline range
         * @product highcharts highstock
         * @apioption plotOptions.areasplinerange
         */
        seriesType('areasplinerange', 'arearange', null, {
            getPointSpline: seriesTypes.spline.prototype.getPointSpline
        });

        /**
         * A `areasplinerange` series. If the [type](#series.areasplinerange.
         * type) option is not specified, it is inherited from [chart.type](#chart.
         * type).
         * 
         * For options that apply to multiple series, it is recommended to add
         * them to the [plotOptions.series](#plotOptions.series) options structure.
         * To apply to all series of this specific type, apply it to [plotOptions.
         * areasplinerange](#plotOptions.areasplinerange).
         * 
         * @type {Object}
         * @extends series,plotOptions.areasplinerange
         * @excluding dataParser,dataURL,stack
         * @product highcharts highstock
         * @apioption series.areasplinerange
         */

        /**
         * An array of data points for the series. For the `areasplinerange`
         * series type, points can be given in the following ways:
         * 
         * 1.  An array of arrays with 3 or 2 values. In this case, the values
         * correspond to `x,low,high`. If the first value is a string, it is
         * applied as the name of the point, and the `x` value is inferred.
         * The `x` value can also be omitted, in which case the inner arrays
         * should be of length 2\. Then the `x` value is automatically calculated,
         * either starting at 0 and incremented by 1, or from `pointStart`
         * and `pointInterval` given in the series options.
         * 
         *  ```js
         *     data: [
         *         [0, 0, 5],
         *         [1, 9, 1],
         *         [2, 5, 2]
         *     ]
         *  ```
         * 
         * 2.  An array of objects with named values. The objects are point
         * configuration objects as seen below. If the total number of data
         * points exceeds the series' [turboThreshold](#series.areasplinerange.
         * turboThreshold), this option is not available.
         * 
         *  ```js
         *     data: [{
         *         x: 1,
         *         low: 5,
         *         high: 0,
         *         name: "Point2",
         *         color: "#00FF00"
         *     }, {
         *         x: 1,
         *         low: 4,
         *         high: 1,
         *         name: "Point1",
         *         color: "#FF00FF"
         *     }]
         *  ```
         * 
         * @type {Array<Object|Array>}
         * @extends series.arearange.data
         * @sample {highcharts} highcharts/chart/reflow-true/ Numerical values
         * @sample {highcharts} highcharts/series/data-array-of-arrays/ Arrays of numeric x and y
         * @sample {highcharts} highcharts/series/data-array-of-arrays-datetime/ Arrays of datetime x and y
         * @sample {highcharts} highcharts/series/data-array-of-name-value/ Arrays of point.name and y
         * @sample {highcharts} highcharts/series/data-array-of-objects/ Config objects
         * @product highcharts highstock
         * @apioption series.areasplinerange.data
         */

    }(Highcharts));
    (function(H) {
        /**
         * (c) 2010-2017 Torstein Honsi
         *
         * License: www.highcharts.com/license
         */
        var defaultPlotOptions = H.defaultPlotOptions,
            each = H.each,
            merge = H.merge,
            noop = H.noop,
            pick = H.pick,
            seriesType = H.seriesType,
            seriesTypes = H.seriesTypes;

        var colProto = seriesTypes.column.prototype;
        /**
         * The column range is a cartesian series type with higher and lower
         * Y values along an X axis. Requires `highcharts-more.js`. To display
         * horizontal bars, set [chart.inverted](#chart.inverted) to `true`.
         *
         * @type {Object}
         * @extends plotOptions.column
         * @excluding negativeColor,stacking,softThreshold,threshold
         * @sample {highcharts} highcharts/demo/columnrange/
         *         Inverted column range
         * @sample {highstock} highcharts/demo/columnrange/
         *         Inverted column range
         * @since 2.3.0
         * @product highcharts highstock
         * @optionparent plotOptions.columnrange
         */
        var columnRangeOptions = {

            pointRange: null,
            marker: null,
            states: {
                hover: {
                    /**
                     * @ignore-option
                     */
                    halo: false
                }
            }

            /**
             * Extended data labels for range series types. Range series data labels
             * have no `x` and `y` options. Instead, they have `xLow`, `xHigh`,
             * `yLow` and `yHigh` options to allow the higher and lower data label
             * sets individually.
             *
             * @type {Object}
             * @extends plotOptions.arearange.dataLabels
             * @since 2.3.0
             * @product highcharts highstock
             * @apioption plotOptions.columnrange.dataLabels
             */
        };
        /**
         * The ColumnRangeSeries class
         */
        seriesType('columnrange', 'arearange', merge(
            defaultPlotOptions.column,
            defaultPlotOptions.arearange,
            columnRangeOptions

        ), {
            /**
             * Translate data points from raw values x and y to plotX and plotY
             */
            translate: function() {
                var series = this,
                    yAxis = series.yAxis,
                    xAxis = series.xAxis,
                    startAngleRad = xAxis.startAngleRad,
                    start,
                    chart = series.chart,
                    isRadial = series.xAxis.isRadial,
                    safeDistance = Math.max(chart.chartWidth, chart.chartHeight) + 999,
                    plotHigh;

                // Don't draw too far outside plot area (#6835)
                function safeBounds(pixelPos) {
                    return Math.min(Math.max(-safeDistance,
                        pixelPos
                    ), safeDistance);
                }


                colProto.translate.apply(series);

                // Set plotLow and plotHigh
                each(series.points, function(point) {
                    var shapeArgs = point.shapeArgs,
                        minPointLength = series.options.minPointLength,
                        heightDifference,
                        height,
                        y;

                    point.plotHigh = plotHigh = safeBounds(
                        yAxis.translate(point.high, 0, 1, 0, 1)
                    );
                    point.plotLow = safeBounds(point.plotY);

                    // adjust shape
                    y = plotHigh;
                    height = pick(point.rectPlotY, point.plotY) - plotHigh;

                    // Adjust for minPointLength
                    if (Math.abs(height) < minPointLength) {
                        heightDifference = (minPointLength - height);
                        height += heightDifference;
                        y -= heightDifference / 2;

                        // Adjust for negative ranges or reversed Y axis (#1457)
                    } else if (height < 0) {
                        height *= -1;
                        y -= height;
                    }

                    if (isRadial) {

                        start = point.barX + startAngleRad;
                        point.shapeType = 'path';
                        point.shapeArgs = {
                            d: series.polarArc(y + height, y, start, start + point.pointWidth)
                        };
                    } else {

                        shapeArgs.height = height;
                        shapeArgs.y = y;

                        point.tooltipPos = chart.inverted ? [
                            yAxis.len + yAxis.pos - chart.plotLeft - y - height / 2,
                            xAxis.len + xAxis.pos - chart.plotTop - shapeArgs.x -
                            shapeArgs.width / 2,
                            height
                        ] : [
                            xAxis.left - chart.plotLeft + shapeArgs.x +
                            shapeArgs.width / 2,
                            yAxis.pos - chart.plotTop + y + height / 2,
                            height
                        ]; // don't inherit from column tooltip position - #3372
                    }
                });
            },
            directTouch: true,
            trackerGroups: ['group', 'dataLabelsGroup'],
            drawGraph: noop,
            getSymbol: noop,
            crispCol: colProto.crispCol,
            drawPoints: colProto.drawPoints,
            drawTracker: colProto.drawTracker,
            getColumnMetrics: colProto.getColumnMetrics,
            pointAttribs: colProto.pointAttribs,

            // Overrides from modules that may be loaded after this module
            animate: function() {
                return colProto.animate.apply(this, arguments);
            },
            polarArc: function() {
                return colProto.polarArc.apply(this, arguments);
            },
            translate3dPoints: function() {
                return colProto.translate3dPoints.apply(this, arguments);
            },
            translate3dShapes: function() {
                return colProto.translate3dShapes.apply(this, arguments);
            }
        }, {
            setState: colProto.pointClass.prototype.setState
        });


        /**
         * A `columnrange` series. If the [type](#series.columnrange.type)
         * option is not specified, it is inherited from [chart.type](#chart.
         * type).
         *
         * For options that apply to multiple series, it is recommended to add
         * them to the [plotOptions.series](#plotOptions.series) options structure.
         * To apply to all series of this specific type, apply it to [plotOptions.
         * columnrange](#plotOptions.columnrange).
         *
         * @type {Object}
         * @extends series,plotOptions.columnrange
         * @excluding dataParser,dataURL,stack
         * @product highcharts highstock
         * @apioption series.columnrange
         */

        /**
         * An array of data points for the series. For the `columnrange` series
         * type, points can be given in the following ways:
         *
         * 1.  An array of arrays with 3 or 2 values. In this case, the values
         * correspond to `x,low,high`. If the first value is a string, it is
         * applied as the name of the point, and the `x` value is inferred.
         * The `x` value can also be omitted, in which case the inner arrays
         * should be of length 2\. Then the `x` value is automatically calculated,
         * either starting at 0 and incremented by 1, or from `pointStart`
         * and `pointInterval` given in the series options.
         *
         *  ```js
         *     data: [
         *         [0, 4, 2],
         *         [1, 2, 1],
         *         [2, 9, 10]
         *     ]
         *  ```
         *
         * 2.  An array of objects with named values. The objects are point
         * configuration objects as seen below. If the total number of data
         * points exceeds the series' [turboThreshold](#series.columnrange.
         * turboThreshold), this option is not available.
         *
         *  ```js
         *     data: [{
         *         x: 1,
         *         low: 0,
         *         high: 4,
         *         name: "Point2",
         *         color: "#00FF00"
         *     }, {
         *         x: 1,
         *         low: 5,
         *         high: 3,
         *         name: "Point1",
         *         color: "#FF00FF"
         *     }]
         *  ```
         *
         * @type {Array<Object|Array>}
         * @extends series.arearange.data
         * @excluding marker
         * @sample {highcharts} highcharts/chart/reflow-true/ Numerical values
         * @sample {highcharts} highcharts/series/data-array-of-arrays/ Arrays of numeric x and y
         * @sample {highcharts} highcharts/series/data-array-of-arrays-datetime/ Arrays of datetime x and y
         * @sample {highcharts} highcharts/series/data-array-of-name-value/ Arrays of point.name and y
         * @sample {highcharts} highcharts/series/data-array-of-objects/ Config objects
         * @product highcharts highstock
         * @apioption series.columnrange.data
         */


    }(Highcharts));
    (function(H) {
        /**
         * (c) 2010-2017 Torstein Honsi
         *
         * License: www.highcharts.com/license
         */
        var each = H.each,
            isNumber = H.isNumber,
            merge = H.merge,
            noop = H.noop,
            pick = H.pick,
            pInt = H.pInt,
            Series = H.Series,
            seriesType = H.seriesType,
            TrackerMixin = H.TrackerMixin;


        /** 
         * Gauges are circular plots displaying one or more values with a dial pointing
         * to values along the perimeter.
         *
         * @sample highcharts/demo/gauge-speedometer/ Gauge chart
         * @extends {plotOptions.line}
         * @excluding animationLimit,boostThreshold,connectEnds,connectNulls,cropThreshold,dashStyle,findNearestPointBy,getExtremesFromAll,marker,pointPlacement,softThreshold,stacking,step,threshold,turboThreshold,zoneAxis,zones
         * @product highcharts
         * @optionparent plotOptions.gauge
         */
        seriesType('gauge', 'line', {

            /**
             * Data labels for the gauge. For gauges, the data labels are enabled
             * by default and shown in a bordered box below the point.
             * 
             * @type {Object}
             * @extends plotOptions.series.dataLabels
             * @since 2.3.0
             * @product highcharts
             */
            dataLabels: {

                /**
                 * Enable or disable the data labels.
                 * 
                 * @type {Boolean}
                 * @since 2.3.0
                 * @product highcharts highmaps
                 */
                enabled: true,

                defer: false,

                /**
                 * The y position offset of the label relative to the center of the
                 * gauge.
                 * 
                 * @type {Number}
                 * @default 15
                 * @since 2.3.0
                 * @product highcharts highmaps
                 */
                y: 15,

                /**
                 * The border radius in pixels for the gauge's data label.
                 * 
                 * @type {Number}
                 * @default 3
                 * @since 2.3.0
                 * @product highcharts highmaps
                 */
                borderRadius: 3,

                crop: false,

                /**
                 * The vertical alignment of the data label.
                 * 
                 * @type {String}
                 * @default top
                 * @product highcharts highmaps
                 */
                verticalAlign: 'top',

                /**
                 * The Z index of the data labels. A value of 2 display them behind
                 * the dial.
                 * 
                 * @type {Number}
                 * @default 2
                 * @since 2.1.5
                 * @product highcharts highmaps
                 */
                zIndex: 2,

                // Presentational

                /**
                 * The border width in pixels for the gauge data label.
                 * 
                 * @type {Number}
                 * @default 1
                 * @since 2.3.0
                 * @product highcharts highmaps
                 */
                borderWidth: 1,

                /**
                 * The border color for the data label.
                 * 
                 * @type {Color}
                 * @default #cccccc
                 * @since 2.3.0
                 * @product highcharts highmaps
                 */
                borderColor: '#cccccc'

            },

            /**
             * Options for the dial or arrow pointer of the gauge.
             * 
             * In styled mode, the dial is styled with the `.highcharts-gauge-
             * series .highcharts-dial` rule.
             * 
             * @type {Object}
             * @sample {highcharts} highcharts/css/gauge/ Styled mode
             * @since 2.3.0
             * @product highcharts
             */


            dial: {

                /**
                 * The length of the dial's base part, relative to the total radius
                 * or length of the dial.
                 * 
                 * @type {String}
                 * @sample {highcharts} highcharts/plotoptions/gauge-dial/
                 *         Dial options demonstrated
                 * @default 70%
                 * @since 2.3.0
                 * @product highcharts
                 * @apioption plotOptions.gauge.dial.baseLength
                 */

                /**
                 * The pixel width of the base of the gauge dial. The base is the part
                 * closest to the pivot, defined by baseLength.
                 * 
                 * @type {Number}
                 * @sample {highcharts} highcharts/plotoptions/gauge-dial/
                 *         Dial options demonstrated
                 * @default 3
                 * @since 2.3.0
                 * @product highcharts
                 * @apioption plotOptions.gauge.dial.baseWidth
                 */

                /**
                 * The radius or length of the dial, in percentages relative to the
                 * radius of the gauge itself.
                 * 
                 * @type {String}
                 * @sample {highcharts} highcharts/plotoptions/gauge-dial/
                 *         Dial options demonstrated
                 * @default 80%
                 * @since 2.3.0
                 * @product highcharts
                 * @apioption plotOptions.gauge.dial.radius
                 */

                /**
                 * The length of the dial's rear end, the part that extends out on the
                 * other side of the pivot. Relative to the dial's length.
                 * 
                 * @type {String}
                 * @sample {highcharts} highcharts/plotoptions/gauge-dial/ Dial options demonstrated
                 * @default 10%
                 * @since 2.3.0
                 * @product highcharts
                 * @apioption plotOptions.gauge.dial.rearLength
                 */

                /**
                 * The width of the top of the dial, closest to the perimeter. The pivot
                 * narrows in from the base to the top.
                 * 
                 * @type {Number}
                 * @sample {highcharts} highcharts/plotoptions/gauge-dial/ Dial options demonstrated
                 * @default 1
                 * @since 2.3.0
                 * @product highcharts
                 * @apioption plotOptions.gauge.dial.topWidth
                 */



                /**
                 * The background or fill color of the gauge's dial.
                 * 
                 * @type {Color}
                 * @sample {highcharts} highcharts/plotoptions/gauge-dial/ Dial options demonstrated
                 * @default #000000
                 * @since 2.3.0
                 * @product highcharts
                 * @apioption plotOptions.gauge.dial.backgroundColor
                 */

                /**
                 * The border color or stroke of the gauge's dial. By default, the borderWidth
                 * is 0, so this must be set in addition to a custom border color.
                 * 
                 * @type {Color}
                 * @sample {highcharts} highcharts/plotoptions/gauge-dial/ Dial options demonstrated
                 * @default #cccccc
                 * @since 2.3.0
                 * @product highcharts
                 * @apioption plotOptions.gauge.dial.borderColor
                 */

                /**
                 * The width of the gauge dial border in pixels.
                 * 
                 * @type {Number}
                 * @sample {highcharts} highcharts/plotoptions/gauge-dial/ Dial options demonstrated
                 * @default 0
                 * @since 2.3.0
                 * @product highcharts
                 * @apioption plotOptions.gauge.dial.borderWidth
                 */


            },

            /**
             * Allow the dial to overshoot the end of the perimeter axis by this
             * many degrees. Say if the gauge axis goes from 0 to 60, a value of
             * 100, or 1000, will show 5 degrees beyond the end of the axis.
             * 
             * @type {Number}
             * @see [wrap](#plotOptions.gauge.wrap)
             * @sample {highcharts} highcharts/plotoptions/gauge-overshoot/
             *         Allow 5 degrees overshoot
             * @default 0
             * @since 3.0.10
             * @product highcharts
             * @apioption plotOptions.gauge.overshoot
             */

            /**
             * Options for the pivot or the center point of the gauge.
             * 
             * In styled mode, the pivot is styled with the `.highcharts-gauge-
             * series .highcharts-pivot` rule.
             * 
             * @type {Object}
             * @sample {highcharts} highcharts/css/gauge/ Styled mode
             * @since 2.3.0
             * @product highcharts
             */
            pivot: {

                /**
                 * The pixel radius of the pivot.
                 * 
                 * @type {Number}
                 * @sample {highcharts} highcharts/plotoptions/gauge-pivot/ Pivot options demonstrated
                 * @default 5
                 * @since 2.3.0
                 * @product highcharts
                 * @apioption plotOptions.gauge.pivot.radius
                 */



                /**
                 * The border or stroke width of the pivot.
                 * 
                 * @type {Number}
                 * @sample {highcharts} highcharts/plotoptions/gauge-pivot/ Pivot options demonstrated
                 * @default 0
                 * @since 2.3.0
                 * @product highcharts
                 * @apioption plotOptions.gauge.pivot.borderWidth
                 */

                /**
                 * The border or stroke color of the pivot. In able to change this,
                 * the borderWidth must also be set to something other than the default
                 * 0.
                 * 
                 * @type {Color}
                 * @sample {highcharts} highcharts/plotoptions/gauge-pivot/ Pivot options demonstrated
                 * @default #cccccc
                 * @since 2.3.0
                 * @product highcharts
                 * @apioption plotOptions.gauge.pivot.borderColor
                 */

                /**
                 * The background color or fill of the pivot.
                 * 
                 * @type {Color}
                 * @sample {highcharts} highcharts/plotoptions/gauge-pivot/ Pivot options demonstrated
                 * @default #000000
                 * @since 2.3.0
                 * @product highcharts
                 * @apioption plotOptions.gauge.pivot.backgroundColor
                 */

            },

            tooltip: {
                headerFormat: ''
            },

            /**
             * Whether to display this particular series or series type in the
             * legend. Defaults to false for gauge series.
             * 
             * @type {Boolean}
             * @since 2.3.0
             * @product highcharts
             */
            showInLegend: false

            /**
             * When this option is `true`, the dial will wrap around the axes. For
             * instance, in a full-range gauge going from 0 to 360, a value of 400
             * will point to 40\. When `wrap` is `false`, the dial stops at 360.
             * 
             * @type {Boolean}
             * @see [overshoot](#plotOptions.gauge.overshoot)
             * @default true
             * @since 3.0
             * @product highcharts
             * @apioption plotOptions.gauge.wrap
             */



            // Prototype members
        }, {
            // chart.angular will be set to true when a gauge series is present, and this will
            // be used on the axes
            angular: true,
            directTouch: true, // #5063
            drawGraph: noop,
            fixedBox: true,
            forceDL: true,
            noSharedTooltip: true,
            trackerGroups: ['group', 'dataLabelsGroup'],

            /**
             * Calculate paths etc
             */
            translate: function() {

                var series = this,
                    yAxis = series.yAxis,
                    options = series.options,
                    center = yAxis.center;

                series.generatePoints();

                each(series.points, function(point) {

                    var dialOptions = merge(options.dial, point.dial),
                        radius = (pInt(pick(dialOptions.radius, 80)) * center[2]) / 200,
                        baseLength = (pInt(pick(dialOptions.baseLength, 70)) * radius) / 100,
                        rearLength = (pInt(pick(dialOptions.rearLength, 10)) * radius) / 100,
                        baseWidth = dialOptions.baseWidth || 3,
                        topWidth = dialOptions.topWidth || 1,
                        overshoot = options.overshoot,
                        rotation = yAxis.startAngleRad + yAxis.translate(point.y, null, null, null, true);

                    // Handle the wrap and overshoot options
                    if (isNumber(overshoot)) {
                        overshoot = overshoot / 180 * Math.PI;
                        rotation = Math.max(yAxis.startAngleRad - overshoot, Math.min(yAxis.endAngleRad + overshoot, rotation));

                    } else if (options.wrap === false) {
                        rotation = Math.max(yAxis.startAngleRad, Math.min(yAxis.endAngleRad, rotation));
                    }

                    rotation = rotation * 180 / Math.PI;

                    point.shapeType = 'path';
                    point.shapeArgs = {
                        d: dialOptions.path || [
                            'M', -rearLength, -baseWidth / 2,
                            'L',
                            baseLength, -baseWidth / 2,
                            radius, -topWidth / 2,
                            radius, topWidth / 2,
                            baseLength, baseWidth / 2, -rearLength, baseWidth / 2,
                            'z'
                        ],
                        translateX: center[0],
                        translateY: center[1],
                        rotation: rotation
                    };

                    // Positions for data label
                    point.plotX = center[0];
                    point.plotY = center[1];
                });
            },

            /**
             * Draw the points where each point is one needle
             */
            drawPoints: function() {

                var series = this,
                    center = series.yAxis.center,
                    pivot = series.pivot,
                    options = series.options,
                    pivotOptions = options.pivot,
                    renderer = series.chart.renderer;

                each(series.points, function(point) {

                    var graphic = point.graphic,
                        shapeArgs = point.shapeArgs,
                        d = shapeArgs.d,
                        dialOptions = merge(options.dial, point.dial); // #1233

                    if (graphic) {
                        graphic.animate(shapeArgs);
                        shapeArgs.d = d; // animate alters it
                    } else {
                        point.graphic = renderer[point.shapeType](shapeArgs)
                            .attr({
                                rotation: shapeArgs.rotation, // required by VML when animation is false
                                zIndex: 1
                            })
                            .addClass('highcharts-dial')
                            .add(series.group);


                        // Presentational attributes
                        point.graphic.attr({
                            stroke: dialOptions.borderColor || 'none',
                            'stroke-width': dialOptions.borderWidth || 0,
                            fill: dialOptions.backgroundColor || '#000000'
                        });

                    }
                });

                // Add or move the pivot
                if (pivot) {
                    pivot.animate({ // #1235
                        translateX: center[0],
                        translateY: center[1]
                    });
                } else {
                    series.pivot = renderer.circle(0, 0, pick(pivotOptions.radius, 5))
                        .attr({
                            zIndex: 2
                        })
                        .addClass('highcharts-pivot')
                        .translate(center[0], center[1])
                        .add(series.group);


                    // Presentational attributes
                    series.pivot.attr({
                        'stroke-width': pivotOptions.borderWidth || 0,
                        stroke: pivotOptions.borderColor || '#cccccc',
                        fill: pivotOptions.backgroundColor || '#000000'
                    });

                }
            },

            /**
             * Animate the arrow up from startAngle
             */
            animate: function(init) {
                var series = this;

                if (!init) {
                    each(series.points, function(point) {
                        var graphic = point.graphic;

                        if (graphic) {
                            // start value
                            graphic.attr({
                                rotation: series.yAxis.startAngleRad * 180 / Math.PI
                            });

                            // animate
                            graphic.animate({
                                rotation: point.shapeArgs.rotation
                            }, series.options.animation);
                        }
                    });

                    // delete this function to allow it only once
                    series.animate = null;
                }
            },

            render: function() {
                this.group = this.plotGroup(
                    'group',
                    'series',
                    this.visible ? 'visible' : 'hidden',
                    this.options.zIndex,
                    this.chart.seriesGroup
                );
                Series.prototype.render.call(this);
                this.group.clip(this.chart.clipRect);
            },

            /**
             * Extend the basic setData method by running processData and generatePoints immediately,
             * in order to access the points from the legend.
             */
            setData: function(data, redraw) {
                Series.prototype.setData.call(this, data, false);
                this.processData();
                this.generatePoints();
                if (pick(redraw, true)) {
                    this.chart.redraw();
                }
            },

            /**
             * If the tracking module is loaded, add the point tracker
             */
            drawTracker: TrackerMixin && TrackerMixin.drawTrackerPoint

            // Point members
        }, {
            /**
             * Don't do any hover colors or anything
             */
            setState: function(state) {
                this.state = state;
            }
        });

        /**
         * A `gauge` series. If the [type](#series.gauge.type) option is not
         * specified, it is inherited from [chart.type](#chart.type).
         * 
         * For options that apply to multiple series, it is recommended to add
         * them to the [plotOptions.series](#plotOptions.series) options structure.
         * To apply to all series of this specific type, apply it to [plotOptions.
         * gauge](#plotOptions.gauge).
         * 
         * @type {Object}
         * @extends series,plotOptions.gauge
         * @excluding dataParser,dataURL,stack
         * @product highcharts
         * @apioption series.gauge
         */

        /**
         * An array of data points for the series. For the `gauge` series type,
         * points can be given in the following ways:
         * 
         * 1.  An array of numerical values. In this case, the numerical values
         * will be interpreted as `y` options. Example:
         * 
         *  ```js
         *  data: [0, 5, 3, 5]
         *  ```
         * 
         * 2.  An array of objects with named values. The objects are point
         * configuration objects as seen below. If the total number of data
         * points exceeds the series' [turboThreshold](#series.gauge.turboThreshold),
         * this option is not available.
         * 
         *  ```js
         *     data: [{
         *     y: 6,
         *     name: "Point2",
         *     color: "#00FF00"
         * }, {
         *     y: 8,
         *     name: "Point1",
         *     color: "#FF00FF"
         * }]</pre>
         * 
         * The typical gauge only contains a single data value.
         * 
         * @type {Array<Object|Number>}
         * @extends series.line.data
         * @excluding drilldown,marker,x
         * @sample {highcharts} highcharts/chart/reflow-true/ Numerical values
         * @sample {highcharts} highcharts/series/data-array-of-arrays/ Arrays of numeric x and y
         * @sample {highcharts} highcharts/series/data-array-of-arrays-datetime/ Arrays of datetime x and y
         * @sample {highcharts} highcharts/series/data-array-of-name-value/ Arrays of point.name and y
         * @sample {highcharts} highcharts/series/data-array-of-objects/ Config objects
         * @product highcharts
         * @apioption series.gauge.data
         */

    }(Highcharts));
    (function(H) {
        /**
         * (c) 2010-2017 Torstein Honsi
         *
         * License: www.highcharts.com/license
         */
        var each = H.each,
            noop = H.noop,
            pick = H.pick,
            seriesType = H.seriesType,
            seriesTypes = H.seriesTypes;

        /**
         * The boxplot series type.
         *
         * @constructor seriesTypes.boxplot
         * @augments seriesTypes.column
         */

        /**
         * A box plot is a convenient way of depicting groups of data through their
         * five-number summaries: the smallest observation (sample minimum), lower
         * quartile (Q1), median (Q2), upper quartile (Q3), and largest observation
         * (sample maximum).
         * 
         * @sample highcharts/demo/box-plot/ Box plot
         * @extends {plotOptions.column}
         * @product highcharts
         * @optionparent plotOptions.boxplot
         */
        seriesType('boxplot', 'column', {

            threshold: null,

            tooltip: {


                pointFormat: '<span style="color:{point.color}">\u25CF</span> <b> {series.name}</b><br/>' + // eslint-disable-line no-dupe-keys
                    'Maximum: {point.high}<br/>' +
                    'Upper quartile: {point.q3}<br/>' +
                    'Median: {point.median}<br/>' +
                    'Lower quartile: {point.q1}<br/>' +
                    'Minimum: {point.low}<br/>'

            },

            /**
             * The length of the whiskers, the horizontal lines marking low and
             * high values. It can be a numerical pixel value, or a percentage
             * value of the box width. Set `0` to disable whiskers.
             * 
             * @type {Number|String}
             * @sample {highcharts} highcharts/plotoptions/box-plot-styling/
             *         True by default
             * @default 50%
             * @since 3.0
             * @product highcharts
             */
            whiskerLength: '50%',


            /**
             * The fill color of the box.
             * 
             * @type {Color}
             * @see In styled mode, the fill color can be set with the
             * `.highcharts-boxplot-box` class.
             * @sample {highcharts} highcharts/plotoptions/box-plot-styling/
             *         Box plot styling
             * @default #ffffff
             * @since 3.0
             * @product highcharts
             */
            fillColor: '#ffffff',

            /**
             * The width of the line surrounding the box. If any of [stemWidth](#plotOptions.
             * boxplot.stemWidth), [medianWidth](#plotOptions.boxplot.medianWidth)
             * or [whiskerWidth](#plotOptions.boxplot.whiskerWidth) are `null`,
             *  the lineWidth also applies to these lines.
             * 
             * @type {Number}
             * @sample {highcharts} highcharts/plotoptions/box-plot-styling/
             *         Box plot styling
             * @sample {highcharts} highcharts/plotoptions/error-bar-styling/
             *         Error bar styling
             * @default 1
             * @since 3.0
             * @product highcharts
             */
            lineWidth: 1,

            /**
             * The color of the median line. If `null`, the general series color
             * applies.
             * 
             * @type {Color}
             * @see In styled mode, the median stroke width can be set with the
             * `.highcharts-boxplot-median` class.
             * 
             * @sample {highcharts} highcharts/plotoptions/box-plot-styling/
             *         Box plot styling
             * @sample {highcharts} highcharts/css/boxplot/
             *         Box plot in styled mode
             * @sample {highcharts} highcharts/plotoptions/error-bar-styling/
             *         Error bar styling
             * @default null
             * @since 3.0
             * @product highcharts
             * @apioption plotOptions.boxplot.medianColor
             */

            /**
             * The pixel width of the median line. If `null`, the [lineWidth](#plotOptions.
             * boxplot.lineWidth) is used.
             * 
             * @type {Number}
             * @see In styled mode, the median stroke width can be set with the
             * `.highcharts-boxplot-median` class.
             * @sample {highcharts} highcharts/plotoptions/box-plot-styling/
             *         Box plot styling
             * @sample {highcharts} highcharts/css/boxplot/
             *         Box plot in styled mode
             * @default 2
             * @since 3.0
             * @product highcharts
             */
            medianWidth: 2,

            states: {
                hover: {
                    brightness: -0.3
                }
            },
            /**
             * The color of the stem, the vertical line extending from the box to
             * the whiskers. If `null`, the series color is used.
             * 
             * @type {Color}
             * @see In styled mode, the stem stroke can be set with the `.highcharts-boxplot-stem` class.
             * @sample {highcharts} highcharts/plotoptions/box-plot-styling/
             *         Box plot styling
             * @sample {highcharts} highcharts/css/boxplot/
             *         Box plot in styled mode
             * @sample {highcharts} highcharts/plotoptions/error-bar-styling/
             *         Error bar styling
             * @default null
             * @since 3.0
             * @product highcharts
             * @apioption plotOptions.boxplot.stemColor
             */

            /**
             * The dash style of the stem, the vertical line extending from the
             * box to the whiskers.
             * 
             * @validvalue ["Solid", "ShortDash", "ShortDot", "ShortDashDot",
             *         "ShortDashDotDot", "Dot", "Dash" ,"LongDash", "DashDot",
             *         "LongDashDot", "LongDashDotDot"]
             * @type {String}
             * @sample {highcharts} highcharts/plotoptions/box-plot-styling/
             *         Box plot styling
             * @sample {highcharts} highcharts/css/boxplot/
             *         Box plot in styled mode
             * @sample {highcharts} highcharts/plotoptions/error-bar-styling/
             *         Error bar styling
             * @default Solid
             * @since 3.0
             * @product highcharts
             * @apioption plotOptions.boxplot.stemDashStyle
             */

            /**
             * The width of the stem, the vertical line extending from the box to
             * the whiskers. If `null`, the width is inherited from the [lineWidth](#plotOptions.
             * boxplot.lineWidth) option.
             * 
             * @type {Number}
             * @see In styled mode, the stem stroke width can be set with the `.
             * highcharts-boxplot-stem` class.
             * @sample {highcharts} highcharts/plotoptions/box-plot-styling/
             *         Box plot styling
             * @sample {highcharts} highcharts/css/boxplot/
             *         Box plot in styled mode
             * @sample {highcharts} highcharts/plotoptions/error-bar-styling/
             *         Error bar styling
             * @default null
             * @since 3.0
             * @product highcharts
             * @apioption plotOptions.boxplot.stemWidth
             */

            /**
             * The color of the whiskers, the horizontal lines marking low and high
             * values. When `null`, the general series color is used.
             * 
             * @type {Color}
             * @see In styled mode, the whisker stroke can be set with the `.highcharts-boxplot-whisker` class .
             * @sample {highcharts} highcharts/plotoptions/box-plot-styling/
             *         Box plot styling
             * @sample {highcharts} highcharts/css/boxplot/
             *         Box plot in styled mode
             * @default null
             * @since 3.0
             * @product highcharts
             * @apioption plotOptions.boxplot.whiskerColor
             */

            /**
             * The line width of the whiskers, the horizontal lines marking low
             * and high values. When `null`, the general [lineWidth](#plotOptions.
             * boxplot.lineWidth) applies.
             * 
             * @type {Number}
             * @see In styled mode, the whisker stroke width can be set with the
             * `.highcharts-boxplot-whisker` class.
             * 
             * @sample {highcharts} highcharts/plotoptions/box-plot-styling/
             *         Box plot styling
             * @sample {highcharts} highcharts/css/boxplot/
             *         Box plot in styled mode
             * @since 3.0
             * @product highcharts
             */
            whiskerWidth: 2


        }, /** @lends seriesTypes.boxplot */ {
            pointArrayMap: ['low', 'q1', 'median', 'q3', 'high'], // array point configs are mapped to this
            toYData: function(point) { // return a plain array for speedy calculation
                return [point.low, point.q1, point.median, point.q3, point.high];
            },
            pointValKey: 'high', // defines the top of the tracker


            /**
             * Get presentational attributes
             */
            pointAttribs: function(point) {
                var options = this.options,
                    color = (point && point.color) || this.color;

                return {
                    'fill': point.fillColor || options.fillColor || color,
                    'stroke': options.lineColor || color,
                    'stroke-width': options.lineWidth || 0
                };
            },


            /**
             * Disable data labels for box plot
             */
            drawDataLabels: noop,

            /**
             * Translate data points from raw values x and y to plotX and plotY
             */
            translate: function() {
                var series = this,
                    yAxis = series.yAxis,
                    pointArrayMap = series.pointArrayMap;

                seriesTypes.column.prototype.translate.apply(series);

                // do the translation on each point dimension
                each(series.points, function(point) {
                    each(pointArrayMap, function(key) {
                        if (point[key] !== null) {
                            point[key + 'Plot'] = yAxis.translate(point[key], 0, 1, 0, 1);
                        }
                    });
                });
            },

            /**
             * Draw the data points
             */
            drawPoints: function() {
                var series = this,
                    points = series.points,
                    options = series.options,
                    chart = series.chart,
                    renderer = chart.renderer,
                    q1Plot,
                    q3Plot,
                    highPlot,
                    lowPlot,
                    medianPlot,
                    medianPath,
                    crispCorr,
                    crispX = 0,
                    boxPath,
                    width,
                    left,
                    right,
                    halfWidth,
                    doQuartiles = series.doQuartiles !== false, // error bar inherits this series type but doesn't do quartiles
                    pointWiskerLength,
                    whiskerLength = series.options.whiskerLength;


                each(points, function(point) {

                    var graphic = point.graphic,
                        verb = graphic ? 'animate' : 'attr',
                        shapeArgs = point.shapeArgs; // the box


                    var boxAttr,
                        stemAttr = {},
                        whiskersAttr = {},
                        medianAttr = {},
                        color = point.color || series.color;


                    if (point.plotY !== undefined) {

                        // crisp vector coordinates
                        width = shapeArgs.width;
                        left = Math.floor(shapeArgs.x);
                        right = left + width;
                        halfWidth = Math.round(width / 2);
                        q1Plot = Math.floor(doQuartiles ? point.q1Plot : point.lowPlot);
                        q3Plot = Math.floor(doQuartiles ? point.q3Plot : point.lowPlot);
                        highPlot = Math.floor(point.highPlot);
                        lowPlot = Math.floor(point.lowPlot);

                        if (!graphic) {
                            point.graphic = graphic = renderer.g('point')
                                .add(series.group);

                            point.stem = renderer.path()
                                .addClass('highcharts-boxplot-stem')
                                .add(graphic);

                            if (whiskerLength) {
                                point.whiskers = renderer.path()
                                    .addClass('highcharts-boxplot-whisker')
                                    .add(graphic);
                            }
                            if (doQuartiles) {
                                point.box = renderer.path(boxPath)
                                    .addClass('highcharts-boxplot-box')
                                    .add(graphic);
                            }
                            point.medianShape = renderer.path(medianPath)
                                .addClass('highcharts-boxplot-median')
                                .add(graphic);
                        }






                        // Stem attributes
                        stemAttr.stroke = point.stemColor || options.stemColor || color;
                        stemAttr['stroke-width'] = pick(point.stemWidth, options.stemWidth, options.lineWidth);
                        stemAttr.dashstyle = point.stemDashStyle || options.stemDashStyle;
                        point.stem.attr(stemAttr);

                        // Whiskers attributes
                        if (whiskerLength) {
                            whiskersAttr.stroke = point.whiskerColor || options.whiskerColor || color;
                            whiskersAttr['stroke-width'] = pick(point.whiskerWidth, options.whiskerWidth, options.lineWidth);
                            point.whiskers.attr(whiskersAttr);
                        }

                        if (doQuartiles) {
                            boxAttr = series.pointAttribs(point);
                            point.box.attr(boxAttr);
                        }


                        // Median attributes
                        medianAttr.stroke = point.medianColor || options.medianColor || color;
                        medianAttr['stroke-width'] = pick(point.medianWidth, options.medianWidth, options.lineWidth);
                        point.medianShape.attr(medianAttr);





                        // The stem
                        crispCorr = (point.stem.strokeWidth() % 2) / 2;
                        crispX = left + halfWidth + crispCorr;
                        point.stem[verb]({
                            d: [
                                // stem up
                                'M',
                                crispX, q3Plot,
                                'L',
                                crispX, highPlot,

                                // stem down
                                'M',
                                crispX, q1Plot,
                                'L',
                                crispX, lowPlot
                            ]
                        });

                        // The box
                        if (doQuartiles) {
                            crispCorr = (point.box.strokeWidth() % 2) / 2;
                            q1Plot = Math.floor(q1Plot) + crispCorr;
                            q3Plot = Math.floor(q3Plot) + crispCorr;
                            left += crispCorr;
                            right += crispCorr;
                            point.box[verb]({
                                d: [
                                    'M',
                                    left, q3Plot,
                                    'L',
                                    left, q1Plot,
                                    'L',
                                    right, q1Plot,
                                    'L',
                                    right, q3Plot,
                                    'L',
                                    left, q3Plot,
                                    'z'
                                ]
                            });
                        }

                        // The whiskers
                        if (whiskerLength) {
                            crispCorr = (point.whiskers.strokeWidth() % 2) / 2;
                            highPlot = highPlot + crispCorr;
                            lowPlot = lowPlot + crispCorr;
                            pointWiskerLength = (/%$/).test(whiskerLength) ? halfWidth * parseFloat(whiskerLength) / 100 : whiskerLength / 2;
                            point.whiskers[verb]({
                                d: [
                                    // High whisker
                                    'M',
                                    crispX - pointWiskerLength,
                                    highPlot,
                                    'L',
                                    crispX + pointWiskerLength,
                                    highPlot,

                                    // Low whisker
                                    'M',
                                    crispX - pointWiskerLength,
                                    lowPlot,
                                    'L',
                                    crispX + pointWiskerLength,
                                    lowPlot
                                ]
                            });
                        }

                        // The median
                        medianPlot = Math.round(point.medianPlot);
                        crispCorr = (point.medianShape.strokeWidth() % 2) / 2;
                        medianPlot = medianPlot + crispCorr;

                        point.medianShape[verb]({
                            d: [
                                'M',
                                left,
                                medianPlot,
                                'L',
                                right,
                                medianPlot
                            ]
                        });
                    }
                });

            },
            setStackedPoints: noop // #3890


        });

        /**
         * A `boxplot` series. If the [type](#series.boxplot.type) option is
         * not specified, it is inherited from [chart.type](#chart.type).
         * 
         * For options that apply to multiple series, it is recommended to add
         * them to the [plotOptions.series](#plotOptions.series) options structure.
         * To apply to all series of this specific type, apply it to [plotOptions.
         * boxplot](#plotOptions.boxplot).
         * 
         * @type {Object}
         * @extends series,plotOptions.boxplot
         * @excluding dataParser,dataURL,stack
         * @product highcharts
         * @apioption series.boxplot
         */

        /**
         * An array of data points for the series. For the `boxplot` series
         * type, points can be given in the following ways:
         * 
         * 1.  An array of arrays with 6 or 5 values. In this case, the values
         * correspond to `x,low,q1,median,q3,high`. If the first value is a
         * string, it is applied as the name of the point, and the `x` value
         * is inferred. The `x` value can also be omitted, in which case the
         * inner arrays should be of length 5\. Then the `x` value is automatically
         * calculated, either starting at 0 and incremented by 1, or from `pointStart`
         * and `pointInterval` given in the series options.
         * 
         *  ```js
         *     data: [
         *         [0, 3, 0, 10, 3, 5],
         *         [1, 7, 8, 7, 2, 9],
         *         [2, 6, 9, 5, 1, 3]
         *     ]
         *  ```
         * 
         * 2.  An array of objects with named values. The objects are point
         * configuration objects as seen below. If the total number of data
         * points exceeds the series' [turboThreshold](#series.boxplot.turboThreshold),
         * this option is not available.
         * 
         *  ```js
         *     data: [{
         *         x: 1,
         *         low: 4,
         *         q1: 9,
         *         median: 9,
         *         q3: 1,
         *         high: 10,
         *         name: "Point2",
         *         color: "#00FF00"
         *     }, {
         *         x: 1,
         *         low: 5,
         *         q1: 7,
         *         median: 3,
         *         q3: 6,
         *         high: 2,
         *         name: "Point1",
         *         color: "#FF00FF"
         *     }]
         *  ```
         * 
         * @type {Array<Object|Array>}
         * @extends series.line.data
         * @excluding marker
         * @sample {highcharts} highcharts/chart/reflow-true/ Numerical values
         * @sample {highcharts} highcharts/series/data-array-of-arrays/ Arrays of numeric x and y
         * @sample {highcharts} highcharts/series/data-array-of-arrays-datetime/ Arrays of datetime x and y
         * @sample {highcharts} highcharts/series/data-array-of-name-value/ Arrays of point.name and y
         * @sample {highcharts} highcharts/series/data-array-of-objects/ Config objects
         * @product highcharts
         * @apioption series.boxplot.data
         */

        /**
         * The `high` value for each data point, signifying the highest value
         * in the sample set. The top whisker is drawn here.
         * 
         * @type {Number}
         * @product highcharts
         * @apioption series.boxplot.data.high
         */

        /**
         * The `low` value for each data point, signifying the lowest value
         * in the sample set. The bottom whisker is drawn here.
         * 
         * @type {Number}
         * @product highcharts
         * @apioption series.boxplot.data.low
         */

        /**
         * The median for each data point. This is drawn as a line through the
         * middle area of the box.
         * 
         * @type {Number}
         * @product highcharts
         * @apioption series.boxplot.data.median
         */

        /**
         * The lower quartile for each data point. This is the bottom of the
         * box.
         * 
         * @type {Number}
         * @product highcharts
         * @apioption series.boxplot.data.q1
         */

        /**
         * The higher quartile for each data point. This is the top of the box.
         * 
         * @type {Number}
         * @product highcharts
         * @apioption series.boxplot.data.q3
         */


    }(Highcharts));
    (function(H) {
        /**
         * (c) 2010-2017 Torstein Honsi
         *
         * License: www.highcharts.com/license
         */
        var each = H.each,
            noop = H.noop,
            seriesType = H.seriesType,
            seriesTypes = H.seriesTypes;

        /**  
         * Error bars are a graphical representation of the variability of data and are
         * used on graphs to indicate the error, or uncertainty in a reported
         * measurement.
         *
         * @sample highcharts/demo/error-bar/ Error bars
         * @extends {plotOptions.boxplot}
         * @product highcharts highstock
         * @optionparent plotOptions.errorbar
         */
        seriesType('errorbar', 'boxplot', {


            /**
             * The main color of the bars. This can be overridden by [stemColor](#plotOptions.
             * errorbar.stemColor) and [whiskerColor](#plotOptions.errorbar.whiskerColor)
             * individually.
             * 
             * @type {Color}
             * @sample {highcharts} highcharts/plotoptions/error-bar-styling/ Error bar styling
             * @default #000000
             * @since 3.0
             * @product highcharts
             */
            color: '#000000',


            grouping: false,

            /**
             * The parent series of the error bar. The default value links it to
             * the previous series. Otherwise, use the id of the parent series.
             * 
             * @type {String}
             * @default :previous
             * @since 3.0
             * @product highcharts
             */
            linkedTo: ':previous',

            tooltip: {
                pointFormat: '<span style="color:{point.color}">\u25CF</span> {series.name}: <b>{point.low}</b> - <b>{point.high}</b><br/>'
            },

            /**
             * The line width of the whiskers, the horizontal lines marking low
             * and high values. When `null`, the general [lineWidth](#plotOptions.
             * errorbar.lineWidth) applies.
             * 
             * @type {Number}
             * @sample {highcharts} highcharts/plotoptions/error-bar-styling/ Error bar styling
             * @default null
             * @since 3.0
             * @product highcharts
             */
            whiskerWidth: null

            // Prototype members
        }, {
            type: 'errorbar',
            pointArrayMap: ['low', 'high'], // array point configs are mapped to this
            toYData: function(point) { // return a plain array for speedy calculation
                return [point.low, point.high];
            },
            pointValKey: 'high', // defines the top of the tracker
            doQuartiles: false,
            drawDataLabels: seriesTypes.arearange ? function() {
                var valKey = this.pointValKey;
                seriesTypes.arearange.prototype.drawDataLabels.call(this);
                // Arearange drawDataLabels does not reset point.y to high, but to low after drawing. #4133 
                each(this.data, function(point) {
                    point.y = point[valKey];
                });
            } : noop,

            /**
             * Get the width and X offset, either on top of the linked series column
             * or standalone
             */
            getColumnMetrics: function() {
                return (this.linkedParent && this.linkedParent.columnMetrics) ||
                    seriesTypes.column.prototype.getColumnMetrics.call(this);
            }
        });

        /**
         * A `errorbar` series. If the [type](#series.errorbar.type) option
         * is not specified, it is inherited from [chart.type](#chart.type).
         * 
         * 
         * For options that apply to multiple series, it is recommended to add
         * them to the [plotOptions.series](#plotOptions.series) options structure.
         * To apply to all series of this specific type, apply it to [plotOptions.
         * errorbar](#plotOptions.errorbar).
         * 
         * @type {Object}
         * @extends series,plotOptions.errorbar
         * @excluding dataParser,dataURL,stack
         * @product highcharts
         * @apioption series.errorbar
         */

        /**
         * An array of data points for the series. For the `errorbar` series
         * type, points can be given in the following ways:
         * 
         * 1.  An array of arrays with 3 or 2 values. In this case, the values
         * correspond to `x,low,high`. If the first value is a string, it is
         * applied as the name of the point, and the `x` value is inferred.
         * The `x` value can also be omitted, in which case the inner arrays
         * should be of length 2\. Then the `x` value is automatically calculated,
         * either starting at 0 and incremented by 1, or from `pointStart`
         * and `pointInterval` given in the series options.
         * 
         *  ```js
         *     data: [
         *         [0, 10, 2],
         *         [1, 1, 8],
         *         [2, 4, 5]
         *     ]
         *  ```
         * 
         * 2.  An array of objects with named values. The objects are point
         * configuration objects as seen below. If the total number of data
         * points exceeds the series' [turboThreshold](#series.errorbar.turboThreshold),
         * this option is not available.
         * 
         *  ```js
         *     data: [{
         *         x: 1,
         *         low: 0,
         *         high: 0,
         *         name: "Point2",
         *         color: "#00FF00"
         *     }, {
         *         x: 1,
         *         low: 5,
         *         high: 5,
         *         name: "Point1",
         *         color: "#FF00FF"
         *     }]
         *  ```
         * 
         * @type {Array<Object|Array>}
         * @extends series.arearange.data
         * @excluding dataLabels,drilldown,marker
         * @sample {highcharts} highcharts/chart/reflow-true/ Numerical values
         * @sample {highcharts} highcharts/series/data-array-of-arrays/ Arrays of numeric x and y
         * @sample {highcharts} highcharts/series/data-array-of-arrays-datetime/ Arrays of datetime x and y
         * @sample {highcharts} highcharts/series/data-array-of-name-value/ Arrays of point.name and y
         * @sample {highcharts} highcharts/series/data-array-of-objects/ Config objects
         * @product highcharts
         * @apioption series.errorbar.data
         */

    }(Highcharts));
    (function(H) {
        /**
         * (c) 2010-2017 Torstein Honsi
         *
         * License: www.highcharts.com/license
         */
        var correctFloat = H.correctFloat,
            isNumber = H.isNumber,
            pick = H.pick,
            Point = H.Point,
            Series = H.Series,
            seriesType = H.seriesType,
            seriesTypes = H.seriesTypes;

        /**
         * A waterfall chart displays sequentially introduced positive or negative
         * values in cumulative columns.
         *
         * @sample highcharts/demo/waterfall/ Waterfall chart
         * @extends {plotOptions.column}
         * @product highcharts
         * @optionparent plotOptions.waterfall
         */
        seriesType('waterfall', 'column', {

            dataLabels: {
                inside: true
            },


            /**
             * The width of the line connecting waterfall columns.
             * 
             * @type {Number}
             * @default 1
             * @product highcharts
             */
            lineWidth: 1,

            /**
             * The color of the line that connects columns in a waterfall series.
             * 
             * 
             * In styled mode, the stroke can be set with the `.highcharts-graph` class.
             * 
             * @type {Color}
             * @default #333333
             * @since 3.0
             * @product highcharts
             */
            lineColor: '#333333',

            /**
             * A name for the dash style to use for the line connecting the columns
             * of the waterfall series. Possible values:
             * 
             * *   Solid
             * *   ShortDash
             * *   ShortDot
             * *   ShortDashDot
             * *   ShortDashDotDot
             * *   Dot
             * *   Dash
             * *   LongDash
             * *   DashDot
             * *   LongDashDot
             * *   LongDashDotDot
             * 
             * In styled mode, the stroke dash-array can be set with the `.
             * highcharts-graph` class.
             * 
             * @type {String}
             * @default Dot
             * @since 3.0
             * @product highcharts
             */
            dashStyle: 'dot',

            /**
             * The color of the border of each waterfall column.
             * 
             * In styled mode, the border stroke can be set with the `.highcharts-point` class.
             * 
             * @type {Color}
             * @default #333333
             * @since 3.0
             * @product highcharts
             */
            borderColor: '#333333',

            states: {
                hover: {
                    lineWidthPlus: 0 // #3126
                }
            }


            /**
             * The color used specifically for positive point columns. When not
             * specified, the general series color is used.
             * 
             * In styled mode, the waterfall colors can be set with the
             * `.highcharts-point-negative`, `.highcharts-sum` and
             * `.highcharts-intermediate-sum` classes.
             * 
             * @type {Color}
             * @sample {highcharts} highcharts/demo/waterfall/ Waterfall
             * @product highcharts
             * @apioption plotOptions.waterfall.upColor
             */

            // Prototype members
        }, {
            pointValKey: 'y',

            /**
             * Translate data points from raw values
             */
            translate: function() {
                var series = this,
                    options = series.options,
                    yAxis = series.yAxis,
                    len,
                    i,
                    points,
                    point,
                    shapeArgs,
                    stack,
                    y,
                    yValue,
                    previousY,
                    previousIntermediate,
                    range,
                    minPointLength = pick(options.minPointLength, 5),
                    halfMinPointLength = minPointLength / 2,
                    threshold = options.threshold,
                    stacking = options.stacking,
                    stackIndicator,
                    tooltipY;

                // run column series translate
                seriesTypes.column.prototype.translate.apply(series);

                previousY = previousIntermediate = threshold;
                points = series.points;

                for (i = 0, len = points.length; i < len; i++) {
                    // cache current point object
                    point = points[i];
                    yValue = series.processedYData[i];
                    shapeArgs = point.shapeArgs;

                    // get current stack
                    stack = stacking && yAxis.stacks[(series.negStacks && yValue < threshold ? '-' : '') + series.stackKey];
                    stackIndicator = series.getStackIndicator(
                        stackIndicator,
                        point.x,
                        series.index
                    );
                    range = stack ?
                        stack[point.x].points[stackIndicator.key] : [0, yValue];

                    // override point value for sums
                    // #3710 Update point does not propagate to sum
                    if (point.isSum) {
                        point.y = correctFloat(yValue);
                    } else if (point.isIntermediateSum) {
                        point.y = correctFloat(yValue - previousIntermediate); // #3840
                    }
                    // up points
                    y = Math.max(previousY, previousY + point.y) + range[0];
                    shapeArgs.y = yAxis.translate(y, 0, 1, 0, 1);

                    // sum points
                    if (point.isSum) {
                        shapeArgs.y = yAxis.translate(range[1], 0, 1, 0, 1);
                        shapeArgs.height = Math.min(yAxis.translate(range[0], 0, 1, 0, 1), yAxis.len) -
                            shapeArgs.y; // #4256

                    } else if (point.isIntermediateSum) {
                        shapeArgs.y = yAxis.translate(range[1], 0, 1, 0, 1);
                        shapeArgs.height = Math.min(yAxis.translate(previousIntermediate, 0, 1, 0, 1), yAxis.len) -
                            shapeArgs.y;
                        previousIntermediate = range[1];

                        // If it's not the sum point, update previous stack end position and get
                        // shape height (#3886)
                    } else {
                        shapeArgs.height = yValue > 0 ?
                            yAxis.translate(previousY, 0, 1, 0, 1) - shapeArgs.y :
                            yAxis.translate(previousY, 0, 1, 0, 1) - yAxis.translate(previousY - yValue, 0, 1, 0, 1);

                        previousY += stack && stack[point.x] ? stack[point.x].total : yValue;
                    }

                    // #3952 Negative sum or intermediate sum not rendered correctly
                    if (shapeArgs.height < 0) {
                        shapeArgs.y += shapeArgs.height;
                        shapeArgs.height *= -1;
                    }

                    point.plotY = shapeArgs.y = Math.round(shapeArgs.y) - (series.borderWidth % 2) / 2;
                    shapeArgs.height = Math.max(Math.round(shapeArgs.height), 0.001); // #3151
                    point.yBottom = shapeArgs.y + shapeArgs.height;

                    if (shapeArgs.height <= minPointLength && !point.isNull) {
                        shapeArgs.height = minPointLength;
                        shapeArgs.y -= halfMinPointLength;
                        point.plotY = shapeArgs.y;
                        if (point.y < 0) {
                            point.minPointLengthOffset = -halfMinPointLength;
                        } else {
                            point.minPointLengthOffset = halfMinPointLength;
                        }
                    } else {
                        point.minPointLengthOffset = 0;
                    }

                    // Correct tooltip placement (#3014)
                    tooltipY = point.plotY + (point.negative ? shapeArgs.height : 0);

                    if (series.chart.inverted) {
                        point.tooltipPos[0] = yAxis.len - tooltipY;
                    } else {
                        point.tooltipPos[1] = tooltipY;
                    }
                }
            },

            /**
             * Call default processData then override yData to reflect waterfall's extremes on yAxis
             */
            processData: function(force) {
                var series = this,
                    options = series.options,
                    yData = series.yData,
                    points = series.options.data, // #3710 Update point does not propagate to sum
                    point,
                    dataLength = yData.length,
                    threshold = options.threshold || 0,
                    subSum,
                    sum,
                    dataMin,
                    dataMax,
                    y,
                    i;

                sum = subSum = dataMin = dataMax = threshold;

                for (i = 0; i < dataLength; i++) {
                    y = yData[i];
                    point = points && points[i] ? points[i] : {};

                    if (y === 'sum' || point.isSum) {
                        yData[i] = correctFloat(sum);
                    } else if (y === 'intermediateSum' || point.isIntermediateSum) {
                        yData[i] = correctFloat(subSum);
                    } else {
                        sum += y;
                        subSum += y;
                    }
                    dataMin = Math.min(sum, dataMin);
                    dataMax = Math.max(sum, dataMax);
                }

                Series.prototype.processData.call(this, force);

                // Record extremes only if stacking was not set:
                if (!series.options.stacking) {
                    series.dataMin = dataMin;
                    series.dataMax = dataMax;
                }
            },

            /**
             * Return y value or string if point is sum
             */
            toYData: function(pt) {
                if (pt.isSum) {
                    return (pt.x === 0 ? null : 'sum'); // #3245 Error when first element is Sum or Intermediate Sum
                }
                if (pt.isIntermediateSum) {
                    return (pt.x === 0 ? null : 'intermediateSum'); // #3245
                }
                return pt.y;
            },


            /**
             * Postprocess mapping between options and SVG attributes
             */
            pointAttribs: function(point, state) {

                var upColor = this.options.upColor,
                    attr;

                // Set or reset up color (#3710, update to negative)
                if (upColor && !point.options.color) {
                    point.color = point.y > 0 ? upColor : null;
                }

                attr = seriesTypes.column.prototype.pointAttribs.call(this, point, state);

                // The dashStyle option in waterfall applies to the graph, not
                // the points
                delete attr.dashstyle;

                return attr;
            },


            /**
             * Return an empty path initially, because we need to know the stroke-width in order 
             * to set the final path.
             */
            getGraphPath: function() {
                return ['M', 0, 0];
            },

            /**
             * Draw columns' connector lines
             */
            getCrispPath: function() {

                var data = this.data,
                    length = data.length,
                    lineWidth = this.graph.strokeWidth() + this.borderWidth,
                    normalizer = Math.round(lineWidth) % 2 / 2,
                    reversedYAxis = this.yAxis.reversed,
                    path = [],
                    prevArgs,
                    pointArgs,
                    i,
                    d;

                for (i = 1; i < length; i++) {
                    pointArgs = data[i].shapeArgs;
                    prevArgs = data[i - 1].shapeArgs;

                    d = [
                        'M',
                        prevArgs.x + prevArgs.width,
                        prevArgs.y + data[i - 1].minPointLengthOffset + normalizer,
                        'L',
                        pointArgs.x,
                        prevArgs.y + data[i - 1].minPointLengthOffset + normalizer
                    ];

                    if (
                        (data[i - 1].y < 0 && !reversedYAxis) ||
                        (data[i - 1].y > 0 && reversedYAxis)
                    ) {
                        d[2] += prevArgs.height;
                        d[5] += prevArgs.height;
                    }

                    path = path.concat(d);
                }

                return path;
            },

            /**
             * The graph is initally drawn with an empty definition, then updated with
             * crisp rendering.
             */
            drawGraph: function() {
                Series.prototype.drawGraph.call(this);
                this.graph.attr({
                    d: this.getCrispPath()
                });
            },

            /**
             * Waterfall has stacking along the x-values too.
             */
            setStackedPoints: function() {
                var series = this,
                    options = series.options,
                    stackedYLength,
                    i;

                Series.prototype.setStackedPoints.apply(series, arguments);

                stackedYLength = series.stackedYData ? series.stackedYData.length : 0;

                // Start from the second point:
                for (i = 1; i < stackedYLength; i++) {
                    if (!options.data[i].isSum &&
                        !options.data[i].isIntermediateSum
                    ) {
                        // Sum previous stacked data as waterfall can grow up/down:
                        series.stackedYData[i] += series.stackedYData[i - 1];
                    }
                }
            },

            /**
             * Extremes for a non-stacked series are recorded in processData.
             * In case of stacking, use Series.stackedYData to calculate extremes.
             */
            getExtremes: function() {
                if (this.options.stacking) {
                    return Series.prototype.getExtremes.apply(this, arguments);
                }
            }


            // Point members
        }, {
            getClassName: function() {
                var className = Point.prototype.getClassName.call(this);

                if (this.isSum) {
                    className += ' highcharts-sum';
                } else if (this.isIntermediateSum) {
                    className += ' highcharts-intermediate-sum';
                }
                return className;
            },
            /**
             * Pass the null test in ColumnSeries.translate.
             */
            isValid: function() {
                return isNumber(this.y, true) || this.isSum || this.isIntermediateSum;
            }

        });

        /**
         * A `waterfall` series. If the [type](#series.waterfall.type) option
         * is not specified, it is inherited from [chart.type](#chart.type).
         * 
         * 
         * For options that apply to multiple series, it is recommended to add
         * them to the [plotOptions.series](#plotOptions.series) options structure.
         * To apply to all series of this specific type, apply it to [plotOptions.
         * waterfall](#plotOptions.waterfall).
         * 
         * @type {Object}
         * @extends series,plotOptions.waterfall
         * @excluding dataParser,dataURL
         * @product highcharts
         * @apioption series.waterfall
         */

        /**
         * An array of data points for the series. For the `waterfall` series
         * type, points can be given in the following ways:
         * 
         * 1.  An array of numerical values. In this case, the numerical values
         * will be interpreted as `y` options. The `x` values will be automatically
         * calculated, either starting at 0 and incremented by 1, or from `pointStart`
         * and `pointInterval` given in the series options. If the axis has
         * categories, these will be used. Example:
         * 
         *  ```js
         *  data: [0, 5, 3, 5]
         *  ```
         * 
         * 2.  An array of arrays with 2 values. In this case, the values correspond
         * to `x,y`. If the first value is a string, it is applied as the name
         * of the point, and the `x` value is inferred.
         * 
         *  ```js
         *     data: [
         *         [0, 7],
         *         [1, 8],
         *         [2, 3]
         *     ]
         *  ```
         * 
         * 3.  An array of objects with named values. The objects are point
         * configuration objects as seen below. If the total number of data
         * points exceeds the series' [turboThreshold](#series.waterfall.turboThreshold),
         * this option is not available.
         * 
         *  ```js
         *     data: [{
         *         x: 1,
         *         y: 8,
         *         name: "Point2",
         *         color: "#00FF00"
         *     }, {
         *         x: 1,
         *         y: 8,
         *         name: "Point1",
         *         color: "#FF00FF"
         *     }]
         *  ```
         * 
         * @type {Array<Object|Array|Number>}
         * @extends series.line.data
         * @excluding marker
         * @sample {highcharts} highcharts/chart/reflow-true/ Numerical values
         * @sample {highcharts} highcharts/series/data-array-of-arrays/ Arrays of numeric x and y
         * @sample {highcharts} highcharts/series/data-array-of-arrays-datetime/ Arrays of datetime x and y
         * @sample {highcharts} highcharts/series/data-array-of-name-value/ Arrays of point.name and y
         * @sample {highcharts} highcharts/series/data-array-of-objects/ Config objects
         * @product highcharts
         * @apioption series.waterfall.data
         */


        /**
         * When this property is true, the points acts as a summary column for
         * the values added or substracted since the last intermediate sum,
         * or since the start of the series. The `y` value is ignored.
         * 
         * @type {Boolean}
         * @sample {highcharts} highcharts/demo/waterfall/ Waterfall
         * @default false
         * @product highcharts
         * @apioption series.waterfall.data.isIntermediateSum
         */

        /**
         * When this property is true, the point display the total sum across
         * the entire series. The `y` value is ignored.
         * 
         * @type {Boolean}
         * @sample {highcharts} highcharts/demo/waterfall/ Waterfall
         * @default false
         * @product highcharts
         * @apioption series.waterfall.data.isSum
         */

    }(Highcharts));
    (function(H) {
        /**
         * (c) 2010-2017 Torstein Honsi
         *
         * License: www.highcharts.com/license
         */
        var LegendSymbolMixin = H.LegendSymbolMixin,
            noop = H.noop,
            Series = H.Series,
            seriesType = H.seriesType,
            seriesTypes = H.seriesTypes;

        /**
         * A polygon series can be used to draw any freeform shape in the cartesian
         * coordinate system. A fill is applied with the `color` option, and
         * stroke is applied through `lineWidth` and `lineColor` options. Requires
         * the `highcharts-more.js` file.
         * 
         * @type {Object}
         * @extends plotOptions.scatter
         * @excluding softThreshold,threshold
         * @sample {highcharts} highcharts/demo/polygon/ Polygon
         * @sample {highstock} highcharts/demo/polygon/ Polygon
         * @since 4.1.0
         * @product highcharts highstock
         * @optionparent plotOptions.polygon
         */
        seriesType('polygon', 'scatter', {
            marker: {
                enabled: false,
                states: {
                    hover: {
                        enabled: false
                    }
                }
            },
            stickyTracking: false,
            tooltip: {
                followPointer: true,
                pointFormat: ''
            },
            trackByArea: true

            // Prototype members
        }, {
            type: 'polygon',
            getGraphPath: function() {

                var graphPath = Series.prototype.getGraphPath.call(this),
                    i = graphPath.length + 1;

                // Close all segments
                while (i--) {
                    if ((i === graphPath.length || graphPath[i] === 'M') && i > 0) {
                        graphPath.splice(i, 0, 'z');
                    }
                }
                this.areaPath = graphPath;
                return graphPath;
            },
            drawGraph: function() {

                this.options.fillColor = this.color; // Hack into the fill logic in area.drawGraph

                seriesTypes.area.prototype.drawGraph.call(this);
            },
            drawLegendSymbol: LegendSymbolMixin.drawRectangle,
            drawTracker: Series.prototype.drawTracker,
            setStackedPoints: noop // No stacking points on polygons (#5310)
        });



        /**
         * A `polygon` series. If the [type](#series.polygon.type) option is
         * not specified, it is inherited from [chart.type](#chart.type).
         * 
         * For options that apply to multiple series, it is recommended to add
         * them to the [plotOptions.series](#plotOptions.series) options structure.
         * To apply to all series of this specific type, apply it to [plotOptions.
         * polygon](#plotOptions.polygon).
         * 
         * @type {Object}
         * @extends series,plotOptions.polygon
         * @excluding dataParser,dataURL,stack
         * @product highcharts highstock
         * @apioption series.polygon
         */

        /**
         * An array of data points for the series. For the `polygon` series
         * type, points can be given in the following ways:
         * 
         * 1.  An array of numerical values. In this case, the numerical values
         * will be interpreted as `y` options. The `x` values will be automatically
         * calculated, either starting at 0 and incremented by 1, or from `pointStart`
         * and `pointInterval` given in the series options. If the axis has
         * categories, these will be used. Example:
         * 
         *  ```js
         *  data: [0, 5, 3, 5]
         *  ```
         * 
         * 2.  An array of arrays with 2 values. In this case, the values correspond
         * to `x,y`. If the first value is a string, it is applied as the name
         * of the point, and the `x` value is inferred.
         * 
         *  ```js
         *     data: [
         *         [0, 10],
         *         [1, 3],
         *         [2, 1]
         *     ]
         *  ```
         * 
         * 3.  An array of objects with named values. The objects are point
         * configuration objects as seen below. If the total number of data
         * points exceeds the series' [turboThreshold](#series.polygon.turboThreshold),
         * this option is not available.
         * 
         *  ```js
         *     data: [{
         *         x: 1,
         *         y: 1,
         *         name: "Point2",
         *         color: "#00FF00"
         *     }, {
         *         x: 1,
         *         y: 8,
         *         name: "Point1",
         *         color: "#FF00FF"
         *     }]
         *  ```
         * 
         * @type {Array<Object|Array>}
         * @extends series.line.data
         * @sample {highcharts} highcharts/chart/reflow-true/ Numerical values
         * @sample {highcharts} highcharts/series/data-array-of-arrays/ Arrays of numeric x and y
         * @sample {highcharts} highcharts/series/data-array-of-arrays-datetime/ Arrays of datetime x and y
         * @sample {highcharts} highcharts/series/data-array-of-name-value/ Arrays of point.name and y
         * @sample {highcharts} highcharts/series/data-array-of-objects/ Config objects
         * @product highcharts highstock
         * @apioption series.polygon.data
         */

    }(Highcharts));
    (function(H) {
        /**
         * (c) 2010-2017 Torstein Honsi
         *
         * License: www.highcharts.com/license
         */
        var arrayMax = H.arrayMax,
            arrayMin = H.arrayMin,
            Axis = H.Axis,
            color = H.color,
            each = H.each,
            isNumber = H.isNumber,
            noop = H.noop,
            pick = H.pick,
            pInt = H.pInt,
            Point = H.Point,
            Series = H.Series,
            seriesType = H.seriesType,
            seriesTypes = H.seriesTypes;


        /**
         * A bubble series is a three dimensional series type where each point renders
         * an X, Y and Z value. Each points is drawn as a bubble where the position
         * along the X and Y axes mark the X and Y values, and the size of the bubble
         * relates to the Z value. Requires `highcharts-more.js`.
         *
         * @sample {highcharts} highcharts/demo/bubble/ Bubble chart
         * @extends plotOptions.scatter
         * @product highcharts highstock
         * @optionparent plotOptions.bubble
         */
        seriesType('bubble', 'scatter', {

            dataLabels: {
                formatter: function() { // #2945
                    return this.point.z;
                },
                inside: true,
                verticalAlign: 'middle'
            },

            /**
             * Whether to display negative sized bubbles. The threshold is given
             * by the [zThreshold](#plotOptions.bubble.zThreshold) option, and negative
             * bubbles can be visualized by setting [negativeColor](#plotOptions.
             * bubble.negativeColor).
             * 
             * @type {Boolean}
             * @sample {highcharts} highcharts/plotoptions/bubble-negative/
             *         Negative bubbles
             * @default true
             * @since 3.0
             * @product highcharts
             * @apioption plotOptions.bubble.displayNegative
             */

            /**
             * Options for the point markers of line-like series. Properties like
             * `fillColor`, `lineColor` and `lineWidth` define the visual appearance
             * of the markers. Other series types, like column series, don't have
             * markers, but have visual options on the series level instead.
             * 
             * In styled mode, the markers can be styled with the `.highcharts-point`, `.highcharts-point-hover` and `.highcharts-point-select`
             * class names.
             * 
             * @type {Object}
             * @extends plotOptions.series.marker
             * @excluding enabled,height,radius,width
             * @product highcharts
             */
            marker: {

                lineColor: null, // inherit from series.color
                lineWidth: 1,
                /**
                 * The fill opacity of the bubble markers.
                 * @type {Number}
                 * @default 0.5
                 * @product highcharts
                 */

                /**
                 * In bubble charts, the radius is overridden and determined based on 
                 * the point's data value.
                 */
                radius: null,
                states: {
                    hover: {
                        radiusPlus: 0
                    }
                },

                /**
                 * A predefined shape or symbol for the marker. Possible values are
                 * "circle", "square", "diamond", "triangle" and "triangle-down".
                 * 
                 * Additionally, the URL to a graphic can be given on the form
                 * `url(graphic.png)`. Note that for the image to be applied to exported
                 * charts, its URL needs to be accessible by the export server.
                 * 
                 * Custom callbacks for symbol path generation can also be added to
                 * `Highcharts.SVGRenderer.prototype.symbols`. The callback is then
                 * used by its method name, as shown in the demo.
                 * 
                 * @validvalue ["circle", "square", "diamond", "triangle", "triangle-down"]
                 * @type {String}
                 * @sample {highcharts} highcharts/plotoptions/bubble-symbol/
                 *         Bubble chart with various symbols
                 * @sample {highcharts} highcharts/plotoptions/series-marker-symbol/
                 *         General chart with predefined, graphic and custom markers
                 * @default circle
                 * @since 5.0.11
                 * @product highcharts
                 */
                symbol: 'circle'
            },

            /**
             * Minimum bubble size. Bubbles will automatically size between the
             * `minSize` and `maxSize` to reflect the `z` value of each bubble.
             * Can be either pixels (when no unit is given), or a percentage of
             * the smallest one of the plot width and height.
             * 
             * @type {String}
             * @sample {highcharts} highcharts/plotoptions/bubble-size/ Bubble size
             * @default 8
             * @since 3.0
             * @product highcharts
             */
            minSize: 8,

            /**
             * Maximum bubble size. Bubbles will automatically size between the
             * `minSize` and `maxSize` to reflect the `z` value of each bubble.
             * Can be either pixels (when no unit is given), or a percentage of
             * the smallest one of the plot width and height.
             * 
             * @type {String}
             * @sample {highcharts} highcharts/plotoptions/bubble-size/ Bubble size
             * @default 20%
             * @since 3.0
             * @product highcharts
             */
            maxSize: '20%',

            /**
             * When a point's Z value is below the [zThreshold](#plotOptions.bubble.
             * zThreshold) setting, this color is used.
             * 
             * @type {Color}
             * @sample {highcharts} highcharts/plotoptions/bubble-negative/
             *         Negative bubbles
             * @default null
             * @since 3.0
             * @product highcharts
             * @apioption plotOptions.bubble.negativeColor
             */

            /**
             * Whether the bubble's value should be represented by the area or the
             * width of the bubble. The default, `area`, corresponds best to the
             * human perception of the size of each bubble.
             * 
             * @validvalue ["area", "width"]
             * @type {String}
             * @sample {highcharts} highcharts/plotoptions/bubble-sizeby/
             *         Comparison of area and size
             * @default area
             * @since 3.0.7
             * @product highcharts
             * @apioption plotOptions.bubble.sizeBy
             */

            /**
             * When this is true, the absolute value of z determines the size of
             * the bubble. This means that with the default `zThreshold` of 0, a
             * bubble of value -1 will have the same size as a bubble of value 1,
             * while a bubble of value 0 will have a smaller size according to
             * `minSize`.
             * 
             * @type {Boolean}
             * @sample {highcharts} highcharts/plotoptions/bubble-sizebyabsolutevalue/
             *         Size by absolute value, various thresholds
             * @default false
             * @since 4.1.9
             * @product highcharts
             * @apioption plotOptions.bubble.sizeByAbsoluteValue
             */

            /**
             * When this is true, the series will not cause the Y axis to cross
             * the zero plane (or [threshold](#plotOptions.series.threshold) option)
             * unless the data actually crosses the plane.
             * 
             * For example, if `softThreshold` is `false`, a series of 0, 1, 2,
             * 3 will make the Y axis show negative values according to the `minPadding`
             * option. If `softThreshold` is `true`, the Y axis starts at 0.
             * 
             * @type {Boolean}
             * @default false
             * @since 4.1.9
             * @product highcharts
             */
            softThreshold: false,

            states: {
                hover: {
                    halo: {
                        size: 5
                    }
                }
            },

            tooltip: {
                pointFormat: '({point.x}, {point.y}), Size: {point.z}'
            },

            turboThreshold: 0,

            /**
             * When [displayNegative](#plotOptions.bubble.displayNegative) is `false`,
             * bubbles with lower Z values are skipped. When `displayNegative`
             * is `true` and a [negativeColor](#plotOptions.bubble.negativeColor)
             * is given, points with lower Z is colored.
             * 
             * @type {Number}
             * @sample {highcharts} highcharts/plotoptions/bubble-negative/
             *         Negative bubbles
             * @default 0
             * @since 3.0
             * @product highcharts
             */
            zThreshold: 0,

            zoneAxis: 'z'

            /**
             * The minimum for the Z value range. Defaults to the highest Z value
             * in the data.
             * 
             * @type {Number}
             * @see [zMax](#plotOptions.bubble.zMin)
             * @sample {highcharts} highcharts/plotoptions/bubble-zmin-zmax/
             *         Z has a possible range of 0-100
             * @default null
             * @since 4.0.3
             * @product highcharts
             * @apioption plotOptions.bubble.zMax
             */

            /**
             * The minimum for the Z value range. Defaults to the lowest Z value
             * in the data.
             * 
             * @type {Number}
             * @see [zMax](#plotOptions.bubble.zMax)
             * @sample {highcharts} highcharts/plotoptions/bubble-zmin-zmax/
             *         Z has a possible range of 0-100
             * @default null
             * @since 4.0.3
             * @product highcharts
             * @apioption plotOptions.bubble.zMin
             */

            // Prototype members
        }, {
            pointArrayMap: ['y', 'z'],
            parallelArrays: ['x', 'y', 'z'],
            trackerGroups: ['group', 'dataLabelsGroup'],
            specialGroup: 'group', // To allow clipping (#6296)
            bubblePadding: true,
            zoneAxis: 'z',
            directTouch: true,


            pointAttribs: function(point, state) {
                var markerOptions = this.options.marker,
                    fillOpacity = pick(markerOptions.fillOpacity, 0.5),
                    attr = Series.prototype.pointAttribs.call(this, point, state);

                if (fillOpacity !== 1) {
                    attr.fill = color(attr.fill).setOpacity(fillOpacity).get('rgba');
                }

                return attr;
            },


            /**
             * Get the radius for each point based on the minSize, maxSize and each point's Z value. This
             * must be done prior to Series.translate because the axis needs to add padding in
             * accordance with the point sizes.
             */
            getRadii: function(zMin, zMax, minSize, maxSize) {
                var len,
                    i,
                    pos,
                    zData = this.zData,
                    radii = [],
                    options = this.options,
                    sizeByArea = options.sizeBy !== 'width',
                    zThreshold = options.zThreshold,
                    zRange = zMax - zMin,
                    value,
                    radius;

                // Set the shape type and arguments to be picked up in drawPoints
                for (i = 0, len = zData.length; i < len; i++) {

                    value = zData[i];

                    // When sizing by threshold, the absolute value of z determines the size
                    // of the bubble.
                    if (options.sizeByAbsoluteValue && value !== null) {
                        value = Math.abs(value - zThreshold);
                        zMax = Math.max(zMax - zThreshold, Math.abs(zMin - zThreshold));
                        zMin = 0;
                    }

                    if (value === null) {
                        radius = null;
                        // Issue #4419 - if value is less than zMin, push a radius that's always smaller than the minimum size
                    } else if (value < zMin) {
                        radius = minSize / 2 - 1;
                    } else {
                        // Relative size, a number between 0 and 1
                        pos = zRange > 0 ? (value - zMin) / zRange : 0.5;

                        if (sizeByArea && pos >= 0) {
                            pos = Math.sqrt(pos);
                        }
                        radius = Math.ceil(minSize + pos * (maxSize - minSize)) / 2;
                    }
                    radii.push(radius);
                }
                this.radii = radii;
            },

            /**
             * Perform animation on the bubbles
             */
            animate: function(init) {
                var animation = this.options.animation;

                if (!init) { // run the animation
                    each(this.points, function(point) {
                        var graphic = point.graphic,
                            animationTarget;

                        if (graphic && graphic.width) { // URL symbols don't have width
                            animationTarget = {
                                x: graphic.x,
                                y: graphic.y,
                                width: graphic.width,
                                height: graphic.height
                            };

                            // Start values
                            graphic.attr({
                                x: point.plotX,
                                y: point.plotY,
                                width: 1,
                                height: 1
                            });

                            // Run animation
                            graphic.animate(animationTarget, animation);
                        }
                    });

                    // delete this function to allow it only once
                    this.animate = null;
                }
            },

            /**
             * Extend the base translate method to handle bubble size
             */
            translate: function() {

                var i,
                    data = this.data,
                    point,
                    radius,
                    radii = this.radii;

                // Run the parent method
                seriesTypes.scatter.prototype.translate.call(this);

                // Set the shape type and arguments to be picked up in drawPoints
                i = data.length;

                while (i--) {
                    point = data[i];
                    radius = radii ? radii[i] : 0; // #1737

                    if (isNumber(radius) && radius >= this.minPxSize / 2) {
                        // Shape arguments
                        point.marker = H.extend(point.marker, {
                            radius: radius,
                            width: 2 * radius,
                            height: 2 * radius
                        });

                        // Alignment box for the data label
                        point.dlBox = {
                            x: point.plotX - radius,
                            y: point.plotY - radius,
                            width: 2 * radius,
                            height: 2 * radius
                        };
                    } else { // below zThreshold
                        point.shapeArgs = point.plotY = point.dlBox = undefined; // #1691
                    }
                }
            },

            alignDataLabel: seriesTypes.column.prototype.alignDataLabel,
            buildKDTree: noop,
            applyZones: noop

            // Point class
        }, {
            haloPath: function(size) {
                return Point.prototype.haloPath.call(
                    this,
                    size === 0 ? 0 : (this.marker ? this.marker.radius || 0 : 0) + size // #6067
                );
            },
            ttBelow: false
        });

        /**
         * Add logic to pad each axis with the amount of pixels
         * necessary to avoid the bubbles to overflow.
         */
        Axis.prototype.beforePadding = function() {
            var axis = this,
                axisLength = this.len,
                chart = this.chart,
                pxMin = 0,
                pxMax = axisLength,
                isXAxis = this.isXAxis,
                dataKey = isXAxis ? 'xData' : 'yData',
                min = this.min,
                extremes = {},
                smallestSize = Math.min(chart.plotWidth, chart.plotHeight),
                zMin = Number.MAX_VALUE,
                zMax = -Number.MAX_VALUE,
                range = this.max - min,
                transA = axisLength / range,
                activeSeries = [];

            // Handle padding on the second pass, or on redraw
            each(this.series, function(series) {

                var seriesOptions = series.options,
                    zData;

                if (series.bubblePadding && (series.visible || !chart.options.chart.ignoreHiddenSeries)) {

                    // Correction for #1673
                    axis.allowZoomOutside = true;

                    // Cache it
                    activeSeries.push(series);

                    if (isXAxis) { // because X axis is evaluated first

                        // For each series, translate the size extremes to pixel values
                        each(['minSize', 'maxSize'], function(prop) {
                            var length = seriesOptions[prop],
                                isPercent = /%$/.test(length);

                            length = pInt(length);
                            extremes[prop] = isPercent ?
                                smallestSize * length / 100 :
                                length;

                        });
                        series.minPxSize = extremes.minSize;
                        // Prioritize min size if conflict to make sure bubbles are
                        // always visible. #5873
                        series.maxPxSize = Math.max(extremes.maxSize, extremes.minSize);

                        // Find the min and max Z
                        zData = series.zData;
                        if (zData.length) { // #1735
                            zMin = pick(seriesOptions.zMin, Math.min(
                                zMin,
                                Math.max(
                                    arrayMin(zData),
                                    seriesOptions.displayNegative === false ? seriesOptions.zThreshold : -Number.MAX_VALUE
                                )
                            ));
                            zMax = pick(seriesOptions.zMax, Math.max(zMax, arrayMax(zData)));
                        }
                    }
                }
            });

            each(activeSeries, function(series) {

                var data = series[dataKey],
                    i = data.length,
                    radius;

                if (isXAxis) {
                    series.getRadii(zMin, zMax, series.minPxSize, series.maxPxSize);
                }

                if (range > 0) {
                    while (i--) {
                        if (isNumber(data[i]) && axis.dataMin <= data[i] && data[i] <= axis.dataMax) {
                            radius = series.radii[i];
                            pxMin = Math.min(((data[i] - min) * transA) - radius, pxMin);
                            pxMax = Math.max(((data[i] - min) * transA) + radius, pxMax);
                        }
                    }
                }
            });

            if (activeSeries.length && range > 0 && !this.isLog) {
                pxMax -= axisLength;
                transA *= (axisLength + pxMin - pxMax) / axisLength;
                each([
                    ['min', 'userMin', pxMin],
                    ['max', 'userMax', pxMax]
                ], function(keys) {
                    if (pick(axis.options[keys[0]], axis[keys[1]]) === undefined) {
                        axis[keys[0]] += keys[2] / transA;
                    }
                });
            }
        };


        /**
         * A `bubble` series. If the [type](#series.bubble.type) option is
         * not specified, it is inherited from [chart.type](#chart.type).
         * 
         * For options that apply to multiple series, it is recommended to add
         * them to the [plotOptions.series](#plotOptions.series) options structure.
         * To apply to all series of this specific type, apply it to [plotOptions.
         * bubble](#plotOptions.bubble).
         * 
         * @type {Object}
         * @extends series,plotOptions.bubble
         * @excluding dataParser,dataURL,stack
         * @product highcharts
         * @apioption series.bubble
         */

        /**
         * An array of data points for the series. For the `bubble` series type,
         * points can be given in the following ways:
         * 
         * 1.  An array of arrays with 3 or 2 values. In this case, the values
         * correspond to `x,y,z`. If the first value is a string, it is applied
         * as the name of the point, and the `x` value is inferred. The `x`
         * value can also be omitted, in which case the inner arrays should
         * be of length 2\. Then the `x` value is automatically calculated,
         * either starting at 0 and incremented by 1, or from `pointStart` and
         * `pointInterval` given in the series options.
         * 
         *  ```js
         *     data: [
         *         [0, 1, 2],
         *         [1, 5, 5],
         *         [2, 0, 2]
         *     ]
         *  ```
         * 
         * 2.  An array of objects with named values. The objects are point
         * configuration objects as seen below. If the total number of data
         * points exceeds the series' [turboThreshold](#series.bubble.turboThreshold),
         * this option is not available.
         * 
         *  ```js
         *     data: [{
         *         x: 1,
         *         y: 1,
         *         z: 1,
         *         name: "Point2",
         *         color: "#00FF00"
         *     }, {
         *         x: 1,
         *         y: 5,
         *         z: 4,
         *         name: "Point1",
         *         color: "#FF00FF"
         *     }]
         *  ```
         * 
         * @type {Array<Object|Array>}
         * @extends series.line.data
         * @excluding marker
         * @sample {highcharts} highcharts/chart/reflow-true/ Numerical values
         * @sample {highcharts} highcharts/series/data-array-of-arrays/ Arrays of numeric x and y
         * @sample {highcharts} highcharts/series/data-array-of-arrays-datetime/ Arrays of datetime x and y
         * @sample {highcharts} highcharts/series/data-array-of-name-value/ Arrays of point.name and y
         * @sample {highcharts} highcharts/series/data-array-of-objects/ Config objects
         * @product highcharts
         * @apioption series.bubble.data
         */

        /**
         * The size value for each bubble. The bubbles' diameters are computed
         * based on the `z`, and controlled by series options like `minSize`,
         *  `maxSize`, `sizeBy`, `zMin` and `zMax`.
         * 
         * @type {Number}
         * @product highcharts
         * @apioption series.bubble.data.z
         */

    }(Highcharts));
    (function(H) {
        /**
         * (c) 2010-2017 Torstein Honsi
         *
         * License: www.highcharts.com/license
         */

        /**
         * Extensions for polar charts. Additionally, much of the geometry required for polar charts is
         * gathered in RadialAxes.js.
         *
         */

        var each = H.each,
            pick = H.pick,
            Pointer = H.Pointer,
            Series = H.Series,
            seriesTypes = H.seriesTypes,
            wrap = H.wrap,

            seriesProto = Series.prototype,
            pointerProto = Pointer.prototype,
            colProto;

        /**
         * Search a k-d tree by the point angle, used for shared tooltips in polar charts
         */
        seriesProto.searchPointByAngle = function(e) {
            var series = this,
                chart = series.chart,
                xAxis = series.xAxis,
                center = xAxis.pane.center,
                plotX = e.chartX - center[0] - chart.plotLeft,
                plotY = e.chartY - center[1] - chart.plotTop;

            return this.searchKDTree({
                clientX: 180 + (Math.atan2(plotX, plotY) * (-180 / Math.PI))
            });

        };

        /**
         * #6212 Calculate connectors for spline series in polar chart. 
         * @param {Boolean} calculateNeighbours - Check if connectors should be calculated for neighbour points as well
         * allows short recurence
         */
        seriesProto.getConnectors = function(segment, index, calculateNeighbours, connectEnds) {

            var i,
                prevPointInd,
                nextPointInd,
                previousPoint,
                nextPoint,
                previousX,
                previousY,
                nextX,
                nextY,
                plotX,
                plotY,
                ret,
                smoothing = 1.5, // 1 means control points midway between points, 2 means 1/3 from the point, 3 is 1/4 etc;
                denom = smoothing + 1,
                leftContX,
                leftContY,
                rightContX,
                rightContY,
                dLControlPoint, // distance left control point
                dRControlPoint,
                leftContAngle,
                rightContAngle,
                jointAngle,
                addedNumber = connectEnds ? 1 : 0;

            /** calculate final index of points depending on the initial index value.
             * Because of calculating neighbours, index may be outisde segment array.
             */
            if (index >= 0 && index <= segment.length - 1) {
                i = index;
            } else if (index < 0) {
                i = segment.length - 1 + index;
            } else {
                i = 0;
            }

            prevPointInd = (i - 1 < 0) ? segment.length - (1 + addedNumber) : i - 1;
            nextPointInd = (i + 1 > segment.length - 1) ? addedNumber : i + 1;
            previousPoint = segment[prevPointInd];
            nextPoint = segment[nextPointInd];
            previousX = previousPoint.plotX;
            previousY = previousPoint.plotY;
            nextX = nextPoint.plotX;
            nextY = nextPoint.plotY;
            plotX = segment[i].plotX; // actual point
            plotY = segment[i].plotY;
            leftContX = (smoothing * plotX + previousX) / denom;
            leftContY = (smoothing * plotY + previousY) / denom;
            rightContX = (smoothing * plotX + nextX) / denom;
            rightContY = (smoothing * plotY + nextY) / denom;
            dLControlPoint = Math.sqrt(Math.pow(leftContX - plotX, 2) + Math.pow(leftContY - plotY, 2));
            dRControlPoint = Math.sqrt(Math.pow(rightContX - plotX, 2) + Math.pow(rightContY - plotY, 2));
            leftContAngle = Math.atan2(leftContY - plotY, leftContX - plotX);
            rightContAngle = Math.atan2(rightContY - plotY, rightContX - plotX);
            jointAngle = (Math.PI / 2) + ((leftContAngle + rightContAngle) / 2);
            // Ensure the right direction, jointAngle should be in the same quadrant as leftContAngle
            if (Math.abs(leftContAngle - jointAngle) > Math.PI / 2) {
                jointAngle -= Math.PI;
            }
            // Find the corrected control points for a spline straight through the point
            leftContX = plotX + Math.cos(jointAngle) * dLControlPoint;
            leftContY = plotY + Math.sin(jointAngle) * dLControlPoint;
            rightContX = plotX + Math.cos(Math.PI + jointAngle) * dRControlPoint;
            rightContY = plotY + Math.sin(Math.PI + jointAngle) * dRControlPoint;

            // push current point's connectors into returned object

            ret = {
                rightContX: rightContX,
                rightContY: rightContY,
                leftContX: leftContX,
                leftContY: leftContY,
                plotX: plotX,
                plotY: plotY
            };

            // calculate connectors for previous and next point and push them inside returned object 
            if (calculateNeighbours) {
                ret.prevPointCont = this.getConnectors(segment, prevPointInd, false, connectEnds);
            }
            return ret;
        };

        /**
         * Wrap the buildKDTree function so that it searches by angle (clientX) in case of shared tooltip,
         * and by two dimensional distance in case of non-shared.
         */
        wrap(seriesProto, 'buildKDTree', function(proceed) {
            if (this.chart.polar) {
                if (this.kdByAngle) {
                    this.searchPoint = this.searchPointByAngle;
                } else {
                    this.options.findNearestPointBy = 'xy';
                }
            }
            proceed.apply(this);
        });

        /**
         * Translate a point's plotX and plotY from the internal angle and radius measures to
         * true plotX, plotY coordinates
         */
        seriesProto.toXY = function(point) {
            var xy,
                chart = this.chart,
                plotX = point.plotX,
                plotY = point.plotY,
                clientX;

            // Save rectangular plotX, plotY for later computation
            point.rectPlotX = plotX;
            point.rectPlotY = plotY;

            // Find the polar plotX and plotY
            xy = this.xAxis.postTranslate(point.plotX, this.yAxis.len - plotY);
            point.plotX = point.polarPlotX = xy.x - chart.plotLeft;
            point.plotY = point.polarPlotY = xy.y - chart.plotTop;

            // If shared tooltip, record the angle in degrees in order to align X points. Otherwise,
            // use a standard k-d tree to get the nearest point in two dimensions.
            if (this.kdByAngle) {
                clientX = ((plotX / Math.PI * 180) + this.xAxis.pane.options.startAngle) % 360;
                if (clientX < 0) { // #2665
                    clientX += 360;
                }
                point.clientX = clientX;
            } else {
                point.clientX = point.plotX;
            }
        };

        if (seriesTypes.spline) {
            /**
             * Overridden method for calculating a spline from one point to the next
             */
            wrap(seriesTypes.spline.prototype, 'getPointSpline', function(proceed, segment, point, i) {
                var ret,
                    connectors;

                if (this.chart.polar) {
                    // moveTo or lineTo
                    if (!i) {
                        ret = ['M', point.plotX, point.plotY];
                    } else { // curve from last point to this
                        connectors = this.getConnectors(segment, i, true, this.connectEnds);
                        ret = [
                            'C',
                            connectors.prevPointCont.rightContX,
                            connectors.prevPointCont.rightContY,
                            connectors.leftContX,
                            connectors.leftContY,
                            connectors.plotX,
                            connectors.plotY
                        ];
                    }
                } else {
                    ret = proceed.call(this, segment, point, i);
                }
                return ret;
            });

            // #6430 Areasplinerange series use unwrapped getPointSpline method, so we need to set this method again.
            if (seriesTypes.areasplinerange) {
                seriesTypes.areasplinerange.prototype.getPointSpline = seriesTypes.spline.prototype.getPointSpline;
            }
        }

        /**
         * Extend translate. The plotX and plotY values are computed as if the polar chart were a
         * cartesian plane, where plotX denotes the angle in radians and (yAxis.len - plotY) is the pixel distance from
         * center.
         */
        wrap(seriesProto, 'translate', function(proceed) {
            var chart = this.chart,
                points,
                i;

            // Run uber method
            proceed.call(this);

            // Postprocess plot coordinates
            if (chart.polar) {
                this.kdByAngle = chart.tooltip && chart.tooltip.shared;

                if (!this.preventPostTranslate) {
                    points = this.points;
                    i = points.length;

                    while (i--) {
                        // Translate plotX, plotY from angle and radius to true plot coordinates
                        this.toXY(points[i]);
                    }
                }
            }
        });

        /**
         * Extend getSegmentPath to allow connecting ends across 0 to provide a closed circle in
         * line-like series.
         */
        wrap(seriesProto, 'getGraphPath', function(proceed, points) {
            var series = this,
                i,
                firstValid,
                popLastPoint;

            // Connect the path
            if (this.chart.polar) {
                points = points || this.points;

                // Append first valid point in order to connect the ends
                for (i = 0; i < points.length; i++) {
                    if (!points[i].isNull) {
                        firstValid = i;
                        break;
                    }
                }


                /**
                 * Polar charts only. Whether to connect the ends of a line series plot
                 * across the extremes.
                 * 
                 * @type {Boolean}
                 * @sample {highcharts} highcharts/plotoptions/line-connectends-false/
                 *         Do not connect
                 * @since 2.3.0
                 * @product highcharts
                 * @apioption plotOptions.series.connectEnds
                 */
                if (this.options.connectEnds !== false && firstValid !== undefined) {
                    this.connectEnds = true; // re-used in splines
                    points.splice(points.length, 0, points[firstValid]);
                    popLastPoint = true;
                }

                // For area charts, pseudo points are added to the graph, now we need to translate these
                each(points, function(point) {
                    if (point.polarPlotY === undefined) {
                        series.toXY(point);
                    }
                });
            }

            // Run uber method
            var ret = proceed.apply(this, [].slice.call(arguments, 1));

            /** #6212 points.splice method is adding points to an array. In case of areaspline getGraphPath method is used two times
             * and in both times points are added to an array. That is why points.pop is used, to get unmodified points.
             */
            if (popLastPoint) {
                points.pop();
            }
            return ret;
        });


        function polarAnimate(proceed, init) {
            var chart = this.chart,
                animation = this.options.animation,
                group = this.group,
                markerGroup = this.markerGroup,
                center = this.xAxis.center,
                plotLeft = chart.plotLeft,
                plotTop = chart.plotTop,
                attribs;

            // Specific animation for polar charts
            if (chart.polar) {

                // Enable animation on polar charts only in SVG. In VML, the scaling is different, plus animation
                // would be so slow it would't matter.
                if (chart.renderer.isSVG) {

                    if (animation === true) {
                        animation = {};
                    }

                    // Initialize the animation
                    if (init) {

                        // Scale down the group and place it in the center
                        attribs = {
                            translateX: center[0] + plotLeft,
                            translateY: center[1] + plotTop,
                            scaleX: 0.001, // #1499
                            scaleY: 0.001
                        };

                        group.attr(attribs);
                        if (markerGroup) {
                            markerGroup.attr(attribs);
                        }

                        // Run the animation
                    } else {
                        attribs = {
                            translateX: plotLeft,
                            translateY: plotTop,
                            scaleX: 1,
                            scaleY: 1
                        };
                        group.animate(attribs, animation);
                        if (markerGroup) {
                            markerGroup.animate(attribs, animation);
                        }

                        // Delete this function to allow it only once
                        this.animate = null;
                    }
                }

                // For non-polar charts, revert to the basic animation
            } else {
                proceed.call(this, init);
            }
        }

        // Define the animate method for regular series
        wrap(seriesProto, 'animate', polarAnimate);


        if (seriesTypes.column) {

            colProto = seriesTypes.column.prototype;

            colProto.polarArc = function(low, high, start, end) {
                var center = this.xAxis.center,
                    len = this.yAxis.len;

                return this.chart.renderer.symbols.arc(
                    center[0],
                    center[1],
                    len - high,
                    null, {
                        start: start,
                        end: end,
                        innerR: len - pick(low, len)
                    }
                );
            };

            /**
             * Define the animate method for columnseries
             */
            wrap(colProto, 'animate', polarAnimate);


            /**
             * Extend the column prototype's translate method
             */
            wrap(colProto, 'translate', function(proceed) {

                var xAxis = this.xAxis,
                    startAngleRad = xAxis.startAngleRad,
                    start,
                    points,
                    point,
                    i;

                this.preventPostTranslate = true;

                // Run uber method
                proceed.call(this);

                // Postprocess plot coordinates
                if (xAxis.isRadial) {
                    points = this.points;
                    i = points.length;
                    while (i--) {
                        point = points[i];
                        start = point.barX + startAngleRad;
                        point.shapeType = 'path';
                        point.shapeArgs = {
                            d: this.polarArc(point.yBottom, point.plotY, start, start + point.pointWidth)
                        };
                        // Provide correct plotX, plotY for tooltip
                        this.toXY(point);
                        point.tooltipPos = [point.plotX, point.plotY];
                        point.ttBelow = point.plotY > xAxis.center[1];
                    }
                }
            });


            /**
             * Align column data labels outside the columns. #1199.
             */
            wrap(colProto, 'alignDataLabel', function(proceed, point, dataLabel, options, alignTo, isNew) {

                if (this.chart.polar) {
                    var angle = point.rectPlotX / Math.PI * 180,
                        align,
                        verticalAlign;

                    // Align nicely outside the perimeter of the columns
                    if (options.align === null) {
                        if (angle > 20 && angle < 160) {
                            align = 'left'; // right hemisphere
                        } else if (angle > 200 && angle < 340) {
                            align = 'right'; // left hemisphere
                        } else {
                            align = 'center'; // top or bottom
                        }
                        options.align = align;
                    }
                    if (options.verticalAlign === null) {
                        if (angle < 45 || angle > 315) {
                            verticalAlign = 'bottom'; // top part
                        } else if (angle > 135 && angle < 225) {
                            verticalAlign = 'top'; // bottom part
                        } else {
                            verticalAlign = 'middle'; // left or right
                        }
                        options.verticalAlign = verticalAlign;
                    }

                    seriesProto.alignDataLabel.call(this, point, dataLabel, options, alignTo, isNew);
                } else {
                    proceed.call(this, point, dataLabel, options, alignTo, isNew);
                }

            });
        }

        /**
         * Extend getCoordinates to prepare for polar axis values
         */
        wrap(pointerProto, 'getCoordinates', function(proceed, e) {
            var chart = this.chart,
                ret = {
                    xAxis: [],
                    yAxis: []
                };

            if (chart.polar) {

                each(chart.axes, function(axis) {
                    var isXAxis = axis.isXAxis,
                        center = axis.center,
                        x = e.chartX - center[0] - chart.plotLeft,
                        y = e.chartY - center[1] - chart.plotTop;

                    ret[isXAxis ? 'xAxis' : 'yAxis'].push({
                        axis: axis,
                        value: axis.translate(
                            isXAxis ?
                            Math.PI - Math.atan2(x, y) : // angle
                            Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2)), // distance from center
                            true
                        )
                    });
                });

            } else {
                ret = proceed.call(this, e);
            }

            return ret;
        });

        wrap(H.Chart.prototype, 'getAxes', function(proceed) {

            if (!this.pane) {
                this.pane = [];
            }
            each(H.splat(this.options.pane), function(paneOptions) {
                new H.Pane( // eslint-disable-line no-new
                    paneOptions,
                    this
                );
            }, this);

            proceed.call(this);
        });

        wrap(H.Chart.prototype, 'drawChartBox', function(proceed) {
            proceed.call(this);

            each(this.pane, function(pane) {
                pane.render();
            });
        });

        /**
         * Extend chart.get to also search in panes. Used internally in responsiveness
         * and chart.update.
         */
        wrap(H.Chart.prototype, 'get', function(proceed, id) {
            return H.find(this.pane, function(pane) {
                return pane.options.id === id;
            }) || proceed.call(this, id);
        });

    }(Highcharts));
}));


/***/ }),

/***/ "../../../../highcharts/highcharts.js":
/***/ (function(module, exports) {

/*
 Highcharts JS v6.0.4 (2017-12-15)

 (c) 2009-2016 Torstein Honsi

 License: www.highcharts.com/license
*/
(function(S,M){"object"===typeof module&&module.exports?module.exports=S.document?M(S):M:S.Highcharts=M(S)})("undefined"!==typeof window?window:this,function(S){var M=function(){var a="undefined"===typeof S?window:S,E=a.document,D=a.navigator&&a.navigator.userAgent||"",H=E&&E.createElementNS&&!!E.createElementNS("http://www.w3.org/2000/svg","svg").createSVGRect,p=/(edge|msie|trident)/i.test(D)&&!a.opera,f=/Firefox/.test(D),l=f&&4>parseInt(D.split("Firefox/")[1],10);return a.Highcharts?a.Highcharts.error(16,
!0):{product:"Highcharts",version:"6.0.4",deg2rad:2*Math.PI/360,doc:E,hasBidiBug:l,hasTouch:E&&void 0!==E.documentElement.ontouchstart,isMS:p,isWebKit:/AppleWebKit/.test(D),isFirefox:f,isTouchDevice:/(Mobile|Android|Windows Phone)/.test(D),SVG_NS:"http://www.w3.org/2000/svg",chartCount:0,seriesTypes:{},symbolSizes:{},svg:H,win:a,marginNames:["plotTop","marginRight","marginBottom","plotLeft"],noop:function(){},charts:[]}}();(function(a){a.timers=[];var E=a.charts,D=a.doc,H=a.win;a.error=function(p,
f){p=a.isNumber(p)?"Highcharts error #"+p+": www.highcharts.com/errors/"+p:p;if(f)throw Error(p);H.console&&console.log(p)};a.Fx=function(a,f,l){this.options=f;this.elem=a;this.prop=l};a.Fx.prototype={dSetter:function(){var a=this.paths[0],f=this.paths[1],l=[],r=this.now,n=a.length,w;if(1===r)l=this.toD;else if(n===f.length&&1>r)for(;n--;)w=parseFloat(a[n]),l[n]=isNaN(w)?f[n]:r*parseFloat(f[n]-w)+w;else l=f;this.elem.attr("d",l,null,!0)},update:function(){var a=this.elem,f=this.prop,l=this.now,r=
this.options.step;if(this[f+"Setter"])this[f+"Setter"]();else a.attr?a.element&&a.attr(f,l,null,!0):a.style[f]=l+this.unit;r&&r.call(a,l,this)},run:function(p,f,l){var r=this,n=r.options,w=function(a){return w.stopped?!1:r.step(a)},u=H.requestAnimationFrame||function(a){setTimeout(a,13)},e=function(){for(var h=0;h<a.timers.length;h++)a.timers[h]()||a.timers.splice(h--,1);a.timers.length&&u(e)};p===f?(delete n.curAnim[this.prop],n.complete&&0===a.keys(n.curAnim).length&&n.complete.call(this.elem)):
(this.startTime=+new Date,this.start=p,this.end=f,this.unit=l,this.now=this.start,this.pos=0,w.elem=this.elem,w.prop=this.prop,w()&&1===a.timers.push(w)&&u(e))},step:function(p){var f=+new Date,l,r=this.options,n=this.elem,w=r.complete,u=r.duration,e=r.curAnim;n.attr&&!n.element?p=!1:p||f>=u+this.startTime?(this.now=this.end,this.pos=1,this.update(),l=e[this.prop]=!0,a.objectEach(e,function(a){!0!==a&&(l=!1)}),l&&w&&w.call(n),p=!1):(this.pos=r.easing((f-this.startTime)/u),this.now=this.start+(this.end-
this.start)*this.pos,this.update(),p=!0);return p},initPath:function(p,f,l){function r(a){var b,c;for(k=a.length;k--;)b="M"===a[k]||"L"===a[k],c=/[a-zA-Z]/.test(a[k+3]),b&&c&&a.splice(k+1,0,a[k+1],a[k+2],a[k+1],a[k+2])}function n(a,b){for(;a.length<c;){a[0]=b[c-a.length];var e=a.slice(0,d);[].splice.apply(a,[0,0].concat(e));z&&(e=a.slice(a.length-d),[].splice.apply(a,[a.length,0].concat(e)),k--)}a[0]="M"}function w(a,k){for(var e=(c-a.length)/d;0<e&&e--;)b=a.slice().splice(a.length/B-d,d*B),b[0]=
k[c-d-e*d],m&&(b[d-6]=b[d-2],b[d-5]=b[d-1]),[].splice.apply(a,[a.length/B,0].concat(b)),z&&e--}f=f||"";var u,e=p.startX,h=p.endX,m=-1<f.indexOf("C"),d=m?7:3,c,b,k;f=f.split(" ");l=l.slice();var z=p.isArea,B=z?2:1,I;m&&(r(f),r(l));if(e&&h){for(k=0;k<e.length;k++)if(e[k]===h[0]){u=k;break}else if(e[0]===h[h.length-e.length+k]){u=k;I=!0;break}void 0===u&&(f=[])}f.length&&a.isNumber(u)&&(c=l.length+u*B*d,I?(n(f,l),w(l,f)):(n(l,f),w(f,l)));return[f,l]}};a.Fx.prototype.fillSetter=a.Fx.prototype.strokeSetter=
function(){this.elem.attr(this.prop,a.color(this.start).tweenTo(a.color(this.end),this.pos),null,!0)};a.extend=function(a,f){var l;a||(a={});for(l in f)a[l]=f[l];return a};a.merge=function(){var p,f=arguments,l,r={},n=function(l,p){"object"!==typeof l&&(l={});a.objectEach(p,function(e,h){!a.isObject(e,!0)||a.isClass(e)||a.isDOMElement(e)?l[h]=p[h]:l[h]=n(l[h]||{},e)});return l};!0===f[0]&&(r=f[1],f=Array.prototype.slice.call(f,2));l=f.length;for(p=0;p<l;p++)r=n(r,f[p]);return r};a.pInt=function(a,
f){return parseInt(a,f||10)};a.isString=function(a){return"string"===typeof a};a.isArray=function(a){a=Object.prototype.toString.call(a);return"[object Array]"===a||"[object Array Iterator]"===a};a.isObject=function(p,f){return!!p&&"object"===typeof p&&(!f||!a.isArray(p))};a.isDOMElement=function(p){return a.isObject(p)&&"number"===typeof p.nodeType};a.isClass=function(p){var f=p&&p.constructor;return!(!a.isObject(p,!0)||a.isDOMElement(p)||!f||!f.name||"Object"===f.name)};a.isNumber=function(a){return"number"===
typeof a&&!isNaN(a)&&Infinity>a&&-Infinity<a};a.erase=function(a,f){for(var l=a.length;l--;)if(a[l]===f){a.splice(l,1);break}};a.defined=function(a){return void 0!==a&&null!==a};a.attr=function(p,f,l){var r;a.isString(f)?a.defined(l)?p.setAttribute(f,l):p&&p.getAttribute&&(r=p.getAttribute(f)):a.defined(f)&&a.isObject(f)&&a.objectEach(f,function(a,l){p.setAttribute(l,a)});return r};a.splat=function(p){return a.isArray(p)?p:[p]};a.syncTimeout=function(a,f,l){if(f)return setTimeout(a,f,l);a.call(0,
l)};a.pick=function(){var a=arguments,f,l,r=a.length;for(f=0;f<r;f++)if(l=a[f],void 0!==l&&null!==l)return l};a.css=function(p,f){a.isMS&&!a.svg&&f&&void 0!==f.opacity&&(f.filter="alpha(opacity\x3d"+100*f.opacity+")");a.extend(p.style,f)};a.createElement=function(p,f,l,r,n){p=D.createElement(p);var w=a.css;f&&a.extend(p,f);n&&w(p,{padding:0,border:"none",margin:0});l&&w(p,l);r&&r.appendChild(p);return p};a.extendClass=function(p,f){var l=function(){};l.prototype=new p;a.extend(l.prototype,f);return l};
a.pad=function(a,f,l){return Array((f||2)+1-String(a).length).join(l||0)+a};a.relativeLength=function(a,f,l){return/%$/.test(a)?f*parseFloat(a)/100+(l||0):parseFloat(a)};a.wrap=function(a,f,l){var p=a[f];a[f]=function(){var a=Array.prototype.slice.call(arguments),f=arguments,u=this;u.proceed=function(){p.apply(u,arguments.length?arguments:f)};a.unshift(p);a=l.apply(this,a);u.proceed=null;return a}};a.getTZOffset=function(p){var f=a.Date;return 6E4*(f.hcGetTimezoneOffset&&f.hcGetTimezoneOffset(p)||
f.hcTimezoneOffset||0)};a.dateFormat=function(p,f,l){if(!a.defined(f)||isNaN(f))return a.defaultOptions.lang.invalidDate||"";p=a.pick(p,"%Y-%m-%d %H:%M:%S");var r=a.Date,n=new r(f-a.getTZOffset(f)),w=n[r.hcGetHours](),u=n[r.hcGetDay](),e=n[r.hcGetDate](),h=n[r.hcGetMonth](),m=n[r.hcGetFullYear](),d=a.defaultOptions.lang,c=d.weekdays,b=d.shortWeekdays,k=a.pad,r=a.extend({a:b?b[u]:c[u].substr(0,3),A:c[u],d:k(e),e:k(e,2," "),w:u,b:d.shortMonths[h],B:d.months[h],m:k(h+1),y:m.toString().substr(2,2),Y:m,
H:k(w),k:w,I:k(w%12||12),l:w%12||12,M:k(n[r.hcGetMinutes]()),p:12>w?"AM":"PM",P:12>w?"am":"pm",S:k(n.getSeconds()),L:k(Math.round(f%1E3),3)},a.dateFormats);a.objectEach(r,function(a,b){for(;-1!==p.indexOf("%"+b);)p=p.replace("%"+b,"function"===typeof a?a(f):a)});return l?p.substr(0,1).toUpperCase()+p.substr(1):p};a.formatSingle=function(p,f){var l=/\.([0-9])/,r=a.defaultOptions.lang;/f$/.test(p)?(l=(l=p.match(l))?l[1]:-1,null!==f&&(f=a.numberFormat(f,l,r.decimalPoint,-1<p.indexOf(",")?r.thousandsSep:
""))):f=a.dateFormat(p,f);return f};a.format=function(p,f){for(var l="{",r=!1,n,w,u,e,h=[],m;p;){l=p.indexOf(l);if(-1===l)break;n=p.slice(0,l);if(r){n=n.split(":");w=n.shift().split(".");e=w.length;m=f;for(u=0;u<e;u++)m&&(m=m[w[u]]);n.length&&(m=a.formatSingle(n.join(":"),m));h.push(m)}else h.push(n);p=p.slice(l+1);l=(r=!r)?"}":"{"}h.push(p);return h.join("")};a.getMagnitude=function(a){return Math.pow(10,Math.floor(Math.log(a)/Math.LN10))};a.normalizeTickInterval=function(p,f,l,r,n){var w,u=p;l=
a.pick(l,1);w=p/l;f||(f=n?[1,1.2,1.5,2,2.5,3,4,5,6,8,10]:[1,2,2.5,5,10],!1===r&&(1===l?f=a.grep(f,function(a){return 0===a%1}):.1>=l&&(f=[1/l])));for(r=0;r<f.length&&!(u=f[r],n&&u*l>=p||!n&&w<=(f[r]+(f[r+1]||f[r]))/2);r++);return u=a.correctFloat(u*l,-Math.round(Math.log(.001)/Math.LN10))};a.stableSort=function(a,f){var l=a.length,p,n;for(n=0;n<l;n++)a[n].safeI=n;a.sort(function(a,n){p=f(a,n);return 0===p?a.safeI-n.safeI:p});for(n=0;n<l;n++)delete a[n].safeI};a.arrayMin=function(a){for(var f=a.length,
l=a[0];f--;)a[f]<l&&(l=a[f]);return l};a.arrayMax=function(a){for(var f=a.length,l=a[0];f--;)a[f]>l&&(l=a[f]);return l};a.destroyObjectProperties=function(p,f){a.objectEach(p,function(a,r){a&&a!==f&&a.destroy&&a.destroy();delete p[r]})};a.discardElement=function(p){var f=a.garbageBin;f||(f=a.createElement("div"));p&&f.appendChild(p);f.innerHTML=""};a.correctFloat=function(a,f){return parseFloat(a.toPrecision(f||14))};a.setAnimation=function(p,f){f.renderer.globalAnimation=a.pick(p,f.options.chart.animation,
!0)};a.animObject=function(p){return a.isObject(p)?a.merge(p):{duration:p?500:0}};a.timeUnits={millisecond:1,second:1E3,minute:6E4,hour:36E5,day:864E5,week:6048E5,month:24192E5,year:314496E5};a.numberFormat=function(p,f,l,r){p=+p||0;f=+f;var n=a.defaultOptions.lang,w=(p.toString().split(".")[1]||"").split("e")[0].length,u,e,h=p.toString().split("e");-1===f?f=Math.min(w,20):a.isNumber(f)?f&&h[1]&&0>h[1]&&(u=f+ +h[1],0<=u?(h[0]=(+h[0]).toExponential(u).split("e")[0],f=u):(h[0]=h[0].split(".")[0]||0,
p=20>f?(h[0]*Math.pow(10,h[1])).toFixed(f):0,h[1]=0)):f=2;e=(Math.abs(h[1]?h[0]:p)+Math.pow(10,-Math.max(f,w)-1)).toFixed(f);w=String(a.pInt(e));u=3<w.length?w.length%3:0;l=a.pick(l,n.decimalPoint);r=a.pick(r,n.thousandsSep);p=(0>p?"-":"")+(u?w.substr(0,u)+r:"");p+=w.substr(u).replace(/(\d{3})(?=\d)/g,"$1"+r);f&&(p+=l+e.slice(-f));h[1]&&0!==+p&&(p+="e"+h[1]);return p};Math.easeInOutSine=function(a){return-.5*(Math.cos(Math.PI*a)-1)};a.getStyle=function(p,f,l){if("width"===f)return Math.min(p.offsetWidth,
p.scrollWidth)-a.getStyle(p,"padding-left")-a.getStyle(p,"padding-right");if("height"===f)return Math.min(p.offsetHeight,p.scrollHeight)-a.getStyle(p,"padding-top")-a.getStyle(p,"padding-bottom");H.getComputedStyle||a.error(27,!0);if(p=H.getComputedStyle(p,void 0))p=p.getPropertyValue(f),a.pick(l,"opacity"!==f)&&(p=a.pInt(p));return p};a.inArray=function(p,f){return(a.indexOfPolyfill||Array.prototype.indexOf).call(f,p)};a.grep=function(p,f){return(a.filterPolyfill||Array.prototype.filter).call(p,
f)};a.find=Array.prototype.find?function(a,f){return a.find(f)}:function(a,f){var l,r=a.length;for(l=0;l<r;l++)if(f(a[l],l))return a[l]};a.map=function(a,f){for(var l=[],r=0,n=a.length;r<n;r++)l[r]=f.call(a[r],a[r],r,a);return l};a.keys=function(p){return(a.keysPolyfill||Object.keys).call(void 0,p)};a.reduce=function(p,f,l){return(a.reducePolyfill||Array.prototype.reduce).call(p,f,l)};a.offset=function(a){var f=D.documentElement;a=a.parentElement?a.getBoundingClientRect():{top:0,left:0};return{top:a.top+
(H.pageYOffset||f.scrollTop)-(f.clientTop||0),left:a.left+(H.pageXOffset||f.scrollLeft)-(f.clientLeft||0)}};a.stop=function(p,f){for(var l=a.timers.length;l--;)a.timers[l].elem!==p||f&&f!==a.timers[l].prop||(a.timers[l].stopped=!0)};a.each=function(p,f,l){return(a.forEachPolyfill||Array.prototype.forEach).call(p,f,l)};a.objectEach=function(a,f,l){for(var r in a)a.hasOwnProperty(r)&&f.call(l,a[r],r,a)};a.addEvent=function(p,f,l){var r,n,w=p.addEventListener||a.addEventListenerPolyfill;p.hcEvents&&
!Object.prototype.hasOwnProperty.call(p,"hcEvents")&&(n={},a.objectEach(p.hcEvents,function(a,e){n[e]=a.slice(0)}),p.hcEvents=n);r=p.hcEvents=p.hcEvents||{};w&&w.call(p,f,l,!1);r[f]||(r[f]=[]);r[f].push(l);return function(){a.removeEvent(p,f,l)}};a.removeEvent=function(p,f,l){function r(e,m){var d=p.removeEventListener||a.removeEventListenerPolyfill;d&&d.call(p,e,m,!1)}function n(){var e,m;p.nodeName&&(f?(e={},e[f]=!0):e=u,a.objectEach(e,function(a,c){if(u[c])for(m=u[c].length;m--;)r(c,u[c][m])}))}
var w,u=p.hcEvents,e;u&&(f?(w=u[f]||[],l?(e=a.inArray(l,w),-1<e&&(w.splice(e,1),u[f]=w),r(f,l)):(n(),u[f]=[])):(n(),p.hcEvents={}))};a.fireEvent=function(p,f,l,r){var n;n=p.hcEvents;var w,u;l=l||{};if(D.createEvent&&(p.dispatchEvent||p.fireEvent))n=D.createEvent("Events"),n.initEvent(f,!0,!0),a.extend(n,l),p.dispatchEvent?p.dispatchEvent(n):p.fireEvent(f,n);else if(n)for(n=n[f]||[],w=n.length,l.target||a.extend(l,{preventDefault:function(){l.defaultPrevented=!0},target:p,type:f}),f=0;f<w;f++)(u=n[f])&&
!1===u.call(p,l)&&l.preventDefault();r&&!l.defaultPrevented&&r(l)};a.animate=function(p,f,l){var r,n="",w,u,e;a.isObject(l)||(e=arguments,l={duration:e[2],easing:e[3],complete:e[4]});a.isNumber(l.duration)||(l.duration=400);l.easing="function"===typeof l.easing?l.easing:Math[l.easing]||Math.easeInOutSine;l.curAnim=a.merge(f);a.objectEach(f,function(e,m){a.stop(p,m);u=new a.Fx(p,l,m);w=null;"d"===m?(u.paths=u.initPath(p,p.d,f.d),u.toD=f.d,r=0,w=1):p.attr?r=p.attr(m):(r=parseFloat(a.getStyle(p,m))||
0,"opacity"!==m&&(n="px"));w||(w=e);w&&w.match&&w.match("px")&&(w=w.replace(/px/g,""));u.run(r,w,n)})};a.seriesType=function(p,f,l,r,n){var w=a.getOptions(),u=a.seriesTypes;w.plotOptions[p]=a.merge(w.plotOptions[f],l);u[p]=a.extendClass(u[f]||function(){},r);u[p].prototype.type=p;n&&(u[p].prototype.pointClass=a.extendClass(a.Point,n));return u[p]};a.uniqueKey=function(){var a=Math.random().toString(36).substring(2,9),f=0;return function(){return"highcharts-"+a+"-"+f++}}();H.jQuery&&(H.jQuery.fn.highcharts=
function(){var p=[].slice.call(arguments);if(this[0])return p[0]?(new (a[a.isString(p[0])?p.shift():"Chart"])(this[0],p[0],p[1]),this):E[a.attr(this[0],"data-highcharts-chart")]})})(M);(function(a){var E=a.each,D=a.isNumber,H=a.map,p=a.merge,f=a.pInt;a.Color=function(l){if(!(this instanceof a.Color))return new a.Color(l);this.init(l)};a.Color.prototype={parsers:[{regex:/rgba\(\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]?(?:\.[0-9]+)?)\s*\)/,parse:function(a){return[f(a[1]),f(a[2]),
f(a[3]),parseFloat(a[4],10)]}},{regex:/rgb\(\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*\)/,parse:function(a){return[f(a[1]),f(a[2]),f(a[3]),1]}}],names:{none:"rgba(255,255,255,0)",white:"#ffffff",black:"#000000"},init:function(l){var f,n,w,u;if((this.input=l=this.names[l&&l.toLowerCase?l.toLowerCase():""]||l)&&l.stops)this.stops=H(l.stops,function(e){return new a.Color(e[1])});else if(l&&l.charAt&&"#"===l.charAt()&&(f=l.length,l=parseInt(l.substr(1),16),7===f?n=[(l&16711680)>>16,(l&65280)>>
8,l&255,1]:4===f&&(n=[(l&3840)>>4|(l&3840)>>8,(l&240)>>4|l&240,(l&15)<<4|l&15,1])),!n)for(w=this.parsers.length;w--&&!n;)u=this.parsers[w],(f=u.regex.exec(l))&&(n=u.parse(f));this.rgba=n||[]},get:function(a){var f=this.input,n=this.rgba,l;this.stops?(l=p(f),l.stops=[].concat(l.stops),E(this.stops,function(n,e){l.stops[e]=[l.stops[e][0],n.get(a)]})):l=n&&D(n[0])?"rgb"===a||!a&&1===n[3]?"rgb("+n[0]+","+n[1]+","+n[2]+")":"a"===a?n[3]:"rgba("+n.join(",")+")":f;return l},brighten:function(a){var l,n=this.rgba;
if(this.stops)E(this.stops,function(n){n.brighten(a)});else if(D(a)&&0!==a)for(l=0;3>l;l++)n[l]+=f(255*a),0>n[l]&&(n[l]=0),255<n[l]&&(n[l]=255);return this},setOpacity:function(a){this.rgba[3]=a;return this},tweenTo:function(a,f){var n=this.rgba,l=a.rgba;l.length&&n&&n.length?(a=1!==l[3]||1!==n[3],f=(a?"rgba(":"rgb(")+Math.round(l[0]+(n[0]-l[0])*(1-f))+","+Math.round(l[1]+(n[1]-l[1])*(1-f))+","+Math.round(l[2]+(n[2]-l[2])*(1-f))+(a?","+(l[3]+(n[3]-l[3])*(1-f)):"")+")"):f=a.input||"none";return f}};
a.color=function(l){return new a.Color(l)}})(M);(function(a){var E,D,H=a.addEvent,p=a.animate,f=a.attr,l=a.charts,r=a.color,n=a.css,w=a.createElement,u=a.defined,e=a.deg2rad,h=a.destroyObjectProperties,m=a.doc,d=a.each,c=a.extend,b=a.erase,k=a.grep,z=a.hasTouch,B=a.inArray,I=a.isArray,x=a.isFirefox,K=a.isMS,t=a.isObject,C=a.isString,N=a.isWebKit,q=a.merge,A=a.noop,F=a.objectEach,G=a.pick,g=a.pInt,v=a.removeEvent,Q=a.stop,L=a.svg,P=a.SVG_NS,J=a.symbolSizes,R=a.win;E=a.SVGElement=function(){return this};
c(E.prototype,{opacity:1,SVG_NS:P,textProps:"direction fontSize fontWeight fontFamily fontStyle color lineHeight width textAlign textDecoration textOverflow textOutline".split(" "),init:function(a,g){this.element="span"===g?w(g):m.createElementNS(this.SVG_NS,g);this.renderer=a},animate:function(y,g,b){g=a.animObject(G(g,this.renderer.globalAnimation,!0));0!==g.duration?(b&&(g.complete=b),p(this,y,g)):(this.attr(y,null,b),g.step&&g.step.call(this));return this},colorGradient:function(y,g,b){var v=
this.renderer,c,O,k,e,z,h,m,L,A,J,t=[],x;y.radialGradient?O="radialGradient":y.linearGradient&&(O="linearGradient");O&&(k=y[O],z=v.gradients,m=y.stops,J=b.radialReference,I(k)&&(y[O]=k={x1:k[0],y1:k[1],x2:k[2],y2:k[3],gradientUnits:"userSpaceOnUse"}),"radialGradient"===O&&J&&!u(k.gradientUnits)&&(e=k,k=q(k,v.getRadialAttr(J,e),{gradientUnits:"userSpaceOnUse"})),F(k,function(a,y){"id"!==y&&t.push(y,a)}),F(m,function(a){t.push(a)}),t=t.join(","),z[t]?J=z[t].attr("id"):(k.id=J=a.uniqueKey(),z[t]=h=v.createElement(O).attr(k).add(v.defs),
h.radAttr=e,h.stops=[],d(m,function(y){0===y[1].indexOf("rgba")?(c=a.color(y[1]),L=c.get("rgb"),A=c.get("a")):(L=y[1],A=1);y=v.createElement("stop").attr({offset:y[0],"stop-color":L,"stop-opacity":A}).add(h);h.stops.push(y)})),x="url("+v.url+"#"+J+")",b.setAttribute(g,x),b.gradient=t,y.toString=function(){return x})},applyTextOutline:function(y){var g=this.element,v,c,k,q,e;-1!==y.indexOf("contrast")&&(y=y.replace(/contrast/g,this.renderer.getContrast(g.style.fill)));y=y.split(" ");c=y[y.length-1];
if((k=y[0])&&"none"!==k&&a.svg){this.fakeTS=!0;y=[].slice.call(g.getElementsByTagName("tspan"));this.ySetter=this.xSetter;k=k.replace(/(^[\d\.]+)(.*?)$/g,function(a,y,g){return 2*y+g});for(e=y.length;e--;)v=y[e],"highcharts-text-outline"===v.getAttribute("class")&&b(y,g.removeChild(v));q=g.firstChild;d(y,function(a,y){0===y&&(a.setAttribute("x",g.getAttribute("x")),y=g.getAttribute("y"),a.setAttribute("y",y||0),null===y&&g.setAttribute("y",0));a=a.cloneNode(1);f(a,{"class":"highcharts-text-outline",
fill:c,stroke:c,"stroke-width":k,"stroke-linejoin":"round"});g.insertBefore(a,q)})}},attr:function(a,g,b,v){var y,c=this.element,k,d=this,O,q;"string"===typeof a&&void 0!==g&&(y=a,a={},a[y]=g);"string"===typeof a?d=(this[a+"Getter"]||this._defaultGetter).call(this,a,c):(F(a,function(y,g){O=!1;v||Q(this,g);this.symbolName&&/^(x|y|width|height|r|start|end|innerR|anchorX|anchorY)$/.test(g)&&(k||(this.symbolAttr(a),k=!0),O=!0);!this.rotation||"x"!==g&&"y"!==g||(this.doTransform=!0);O||(q=this[g+"Setter"]||
this._defaultSetter,q.call(this,y,g,c),this.shadows&&/^(width|height|visibility|x|y|d|transform|cx|cy|r)$/.test(g)&&this.updateShadows(g,y,q))},this),this.afterSetters());b&&b();return d},afterSetters:function(){this.doTransform&&(this.updateTransform(),this.doTransform=!1)},updateShadows:function(a,g,b){for(var y=this.shadows,v=y.length;v--;)b.call(y[v],"height"===a?Math.max(g-(y[v].cutHeight||0),0):"d"===a?this.d:g,a,y[v])},addClass:function(a,g){var y=this.attr("class")||"";-1===y.indexOf(a)&&
(g||(a=(y+(y?" ":"")+a).replace("  "," ")),this.attr("class",a));return this},hasClass:function(a){return-1!==B(a,(this.attr("class")||"").split(" "))},removeClass:function(a){return this.attr("class",(this.attr("class")||"").replace(a,""))},symbolAttr:function(a){var y=this;d("x y r start end width height innerR anchorX anchorY".split(" "),function(g){y[g]=G(a[g],y[g])});y.attr({d:y.renderer.symbols[y.symbolName](y.x,y.y,y.width,y.height,y)})},clip:function(a){return this.attr("clip-path",a?"url("+
this.renderer.url+"#"+a.id+")":"none")},crisp:function(a,g){var y;g=g||a.strokeWidth||0;y=Math.round(g)%2/2;a.x=Math.floor(a.x||this.x||0)+y;a.y=Math.floor(a.y||this.y||0)+y;a.width=Math.floor((a.width||this.width||0)-2*y);a.height=Math.floor((a.height||this.height||0)-2*y);u(a.strokeWidth)&&(a.strokeWidth=g);return a},css:function(a){var y=this.styles,b={},v=this.element,k,d="",q,e=!y,z=["textOutline","textOverflow","width"];a&&a.color&&(a.fill=a.color);y&&F(a,function(a,g){a!==y[g]&&(b[g]=a,e=!0)});
e&&(y&&(a=c(y,b)),k=this.textWidth=a&&a.width&&"auto"!==a.width&&"text"===v.nodeName.toLowerCase()&&g(a.width),this.styles=a,k&&!L&&this.renderer.forExport&&delete a.width,K&&!L?n(this.element,a):(q=function(a,y){return"-"+y.toLowerCase()},F(a,function(a,y){-1===B(y,z)&&(d+=y.replace(/([A-Z])/g,q)+":"+a+";")}),d&&f(v,"style",d)),this.added&&("text"===this.element.nodeName&&this.renderer.buildText(this),a&&a.textOutline&&this.applyTextOutline(a.textOutline)));return this},strokeWidth:function(){return this["stroke-width"]||
0},on:function(a,g){var y=this,b=y.element;z&&"click"===a?(b.ontouchstart=function(a){y.touchEventFired=Date.now();a.preventDefault();g.call(b,a)},b.onclick=function(a){(-1===R.navigator.userAgent.indexOf("Android")||1100<Date.now()-(y.touchEventFired||0))&&g.call(b,a)}):b["on"+a]=g;return this},setRadialReference:function(a){var y=this.renderer.gradients[this.element.gradient];this.element.radialReference=a;y&&y.radAttr&&y.animate(this.renderer.getRadialAttr(a,y.radAttr));return this},translate:function(a,
g){return this.attr({translateX:a,translateY:g})},invert:function(a){this.inverted=a;this.updateTransform();return this},updateTransform:function(){var a=this.translateX||0,g=this.translateY||0,b=this.scaleX,v=this.scaleY,c=this.inverted,k=this.rotation,d=this.matrix,q=this.element;c&&(a+=this.width,g+=this.height);a=["translate("+a+","+g+")"];u(d)&&a.push("matrix("+d.join(",")+")");c?a.push("rotate(90) scale(-1,1)"):k&&a.push("rotate("+k+" "+G(this.rotationOriginX,q.getAttribute("x"),0)+" "+G(this.rotationOriginY,
q.getAttribute("y")||0)+")");(u(b)||u(v))&&a.push("scale("+G(b,1)+" "+G(v,1)+")");a.length&&q.setAttribute("transform",a.join(" "))},toFront:function(){var a=this.element;a.parentNode.appendChild(a);return this},align:function(a,g,v){var y,c,k,d,q={};c=this.renderer;k=c.alignedObjects;var e,O;if(a){if(this.alignOptions=a,this.alignByTranslate=g,!v||C(v))this.alignTo=y=v||"renderer",b(k,this),k.push(this),v=null}else a=this.alignOptions,g=this.alignByTranslate,y=this.alignTo;v=G(v,c[y],c);y=a.align;
c=a.verticalAlign;k=(v.x||0)+(a.x||0);d=(v.y||0)+(a.y||0);"right"===y?e=1:"center"===y&&(e=2);e&&(k+=(v.width-(a.width||0))/e);q[g?"translateX":"x"]=Math.round(k);"bottom"===c?O=1:"middle"===c&&(O=2);O&&(d+=(v.height-(a.height||0))/O);q[g?"translateY":"y"]=Math.round(d);this[this.placed?"animate":"attr"](q);this.placed=!0;this.alignAttr=q;return this},getBBox:function(a,g){var y,b=this.renderer,v,k=this.element,q=this.styles,O,z=this.textStr,h,m=b.cache,L=b.cacheKeys,A;g=G(g,this.rotation);v=g*e;
O=q&&q.fontSize;u(z)&&(A=z.toString(),-1===A.indexOf("\x3c")&&(A=A.replace(/[0-9]/g,"0")),A+=["",g||0,O,q&&q.width,q&&q.textOverflow].join());A&&!a&&(y=m[A]);if(!y){if(k.namespaceURI===this.SVG_NS||b.forExport){try{(h=this.fakeTS&&function(a){d(k.querySelectorAll(".highcharts-text-outline"),function(y){y.style.display=a})})&&h("none"),y=k.getBBox?c({},k.getBBox()):{width:k.offsetWidth,height:k.offsetHeight},h&&h("")}catch(W){}if(!y||0>y.width)y={width:0,height:0}}else y=this.htmlGetBBox();b.isSVG&&
(a=y.width,b=y.height,q&&"11px"===q.fontSize&&17===Math.round(b)&&(y.height=b=14),g&&(y.width=Math.abs(b*Math.sin(v))+Math.abs(a*Math.cos(v)),y.height=Math.abs(b*Math.cos(v))+Math.abs(a*Math.sin(v))));if(A&&0<y.height){for(;250<L.length;)delete m[L.shift()];m[A]||L.push(A);m[A]=y}}return y},show:function(a){return this.attr({visibility:a?"inherit":"visible"})},hide:function(){return this.attr({visibility:"hidden"})},fadeOut:function(a){var y=this;y.animate({opacity:0},{duration:a||150,complete:function(){y.attr({y:-9999})}})},
add:function(a){var y=this.renderer,g=this.element,b;a&&(this.parentGroup=a);this.parentInverted=a&&a.inverted;void 0!==this.textStr&&y.buildText(this);this.added=!0;if(!a||a.handleZ||this.zIndex)b=this.zIndexSetter();b||(a?a.element:y.box).appendChild(g);if(this.onAdd)this.onAdd();return this},safeRemoveChild:function(a){var y=a.parentNode;y&&y.removeChild(a)},destroy:function(){var a=this,g=a.element||{},v=a.renderer.isSVG&&"SPAN"===g.nodeName&&a.parentGroup,c=g.ownerSVGElement;g.onclick=g.onmouseout=
g.onmouseover=g.onmousemove=g.point=null;Q(a);a.clipPath&&c&&(d(c.querySelectorAll("[clip-path],[CLIP-PATH]"),function(g){g.getAttribute("clip-path").match(RegExp('[("]#'+a.clipPath.element.id+'[)"]'))&&g.removeAttribute("clip-path")}),a.clipPath=a.clipPath.destroy());if(a.stops){for(c=0;c<a.stops.length;c++)a.stops[c]=a.stops[c].destroy();a.stops=null}a.safeRemoveChild(g);for(a.destroyShadows();v&&v.div&&0===v.div.childNodes.length;)g=v.parentGroup,a.safeRemoveChild(v.div),delete v.div,v=g;a.alignTo&&
b(a.renderer.alignedObjects,a);F(a,function(g,y){delete a[y]});return null},shadow:function(a,g,b){var y=[],v,c,k=this.element,d,q,e,z;if(!a)this.destroyShadows();else if(!this.shadows){q=G(a.width,3);e=(a.opacity||.15)/q;z=this.parentInverted?"(-1,-1)":"("+G(a.offsetX,1)+", "+G(a.offsetY,1)+")";for(v=1;v<=q;v++)c=k.cloneNode(0),d=2*q+1-2*v,f(c,{isShadow:"true",stroke:a.color||"#000000","stroke-opacity":e*v,"stroke-width":d,transform:"translate"+z,fill:"none"}),b&&(f(c,"height",Math.max(f(c,"height")-
d,0)),c.cutHeight=d),g?g.element.appendChild(c):k.parentNode&&k.parentNode.insertBefore(c,k),y.push(c);this.shadows=y}return this},destroyShadows:function(){d(this.shadows||[],function(a){this.safeRemoveChild(a)},this);this.shadows=void 0},xGetter:function(a){"circle"===this.element.nodeName&&("x"===a?a="cx":"y"===a&&(a="cy"));return this._defaultGetter(a)},_defaultGetter:function(a){a=G(this[a+"Value"],this[a],this.element?this.element.getAttribute(a):null,0);/^[\-0-9\.]+$/.test(a)&&(a=parseFloat(a));
return a},dSetter:function(a,g,b){a&&a.join&&(a=a.join(" "));/(NaN| {2}|^$)/.test(a)&&(a="M 0 0");this[g]!==a&&(b.setAttribute(g,a),this[g]=a)},dashstyleSetter:function(a){var b,v=this["stroke-width"];"inherit"===v&&(v=1);if(a=a&&a.toLowerCase()){a=a.replace("shortdashdotdot","3,1,1,1,1,1,").replace("shortdashdot","3,1,1,1").replace("shortdot","1,1,").replace("shortdash","3,1,").replace("longdash","8,3,").replace(/dot/g,"1,3,").replace("dash","4,3,").replace(/,$/,"").split(",");for(b=a.length;b--;)a[b]=
g(a[b])*v;a=a.join(",").replace(/NaN/g,"none");this.element.setAttribute("stroke-dasharray",a)}},alignSetter:function(a){this.alignValue=a;this.element.setAttribute("text-anchor",{left:"start",center:"middle",right:"end"}[a])},opacitySetter:function(a,g,b){this[g]=a;b.setAttribute(g,a)},titleSetter:function(a){var g=this.element.getElementsByTagName("title")[0];g||(g=m.createElementNS(this.SVG_NS,"title"),this.element.appendChild(g));g.firstChild&&g.removeChild(g.firstChild);g.appendChild(m.createTextNode(String(G(a),
"").replace(/<[^>]*>/g,"")))},textSetter:function(a){a!==this.textStr&&(delete this.bBox,this.textStr=a,this.added&&this.renderer.buildText(this))},fillSetter:function(a,g,b){"string"===typeof a?b.setAttribute(g,a):a&&this.colorGradient(a,g,b)},visibilitySetter:function(a,g,b){"inherit"===a?b.removeAttribute(g):this[g]!==a&&b.setAttribute(g,a);this[g]=a},zIndexSetter:function(a,b){var v=this.renderer,y=this.parentGroup,c=(y||v).element||v.box,k,d=this.element,q,e,v=c===v.box;k=this.added;var z;u(a)&&
(d.zIndex=a,a=+a,this[b]===a&&(k=!1),this[b]=a);if(k){(a=this.zIndex)&&y&&(y.handleZ=!0);b=c.childNodes;for(z=b.length-1;0<=z&&!q;z--)if(y=b[z],k=y.zIndex,e=!u(k),y!==d)if(0>a&&e&&!v&&!z)c.insertBefore(d,b[z]),q=!0;else if(g(k)<=a||e&&(!u(a)||0<=a))c.insertBefore(d,b[z+1]||null),q=!0;q||(c.insertBefore(d,b[v?3:0]||null),q=!0)}return q},_defaultSetter:function(a,g,b){b.setAttribute(g,a)}});E.prototype.yGetter=E.prototype.xGetter;E.prototype.translateXSetter=E.prototype.translateYSetter=E.prototype.rotationSetter=
E.prototype.verticalAlignSetter=E.prototype.rotationOriginXSetter=E.prototype.rotationOriginYSetter=E.prototype.scaleXSetter=E.prototype.scaleYSetter=E.prototype.matrixSetter=function(a,g){this[g]=a;this.doTransform=!0};E.prototype["stroke-widthSetter"]=E.prototype.strokeSetter=function(a,g,b){this[g]=a;this.stroke&&this["stroke-width"]?(E.prototype.fillSetter.call(this,this.stroke,"stroke",b),b.setAttribute("stroke-width",this["stroke-width"]),this.hasStroke=!0):"stroke-width"===g&&0===a&&this.hasStroke&&
(b.removeAttribute("stroke"),this.hasStroke=!1)};D=a.SVGRenderer=function(){this.init.apply(this,arguments)};c(D.prototype,{Element:E,SVG_NS:P,init:function(a,g,b,v,c,k){var y;v=this.createElement("svg").attr({version:"1.1","class":"highcharts-root"}).css(this.getStyle(v));y=v.element;a.appendChild(y);f(a,"dir","ltr");-1===a.innerHTML.indexOf("xmlns")&&f(y,"xmlns",this.SVG_NS);this.isSVG=!0;this.box=y;this.boxWrapper=v;this.alignedObjects=[];this.url=(x||N)&&m.getElementsByTagName("base").length?
R.location.href.replace(/#.*?$/,"").replace(/<[^>]*>/g,"").replace(/([\('\)])/g,"\\$1").replace(/ /g,"%20"):"";this.createElement("desc").add().element.appendChild(m.createTextNode("Created with Highcharts 6.0.4"));this.defs=this.createElement("defs").add();this.allowHTML=k;this.forExport=c;this.gradients={};this.cache={};this.cacheKeys=[];this.imgCount=0;this.setSize(g,b,!1);var d;x&&a.getBoundingClientRect&&(g=function(){n(a,{left:0,top:0});d=a.getBoundingClientRect();n(a,{left:Math.ceil(d.left)-
d.left+"px",top:Math.ceil(d.top)-d.top+"px"})},g(),this.unSubPixelFix=H(R,"resize",g))},getStyle:function(a){return this.style=c({fontFamily:'"Lucida Grande", "Lucida Sans Unicode", Arial, Helvetica, sans-serif',fontSize:"12px"},a)},setStyle:function(a){this.boxWrapper.css(this.getStyle(a))},isHidden:function(){return!this.boxWrapper.getBBox().width},destroy:function(){var a=this.defs;this.box=null;this.boxWrapper=this.boxWrapper.destroy();h(this.gradients||{});this.gradients=null;a&&(this.defs=a.destroy());
this.unSubPixelFix&&this.unSubPixelFix();return this.alignedObjects=null},createElement:function(a){var g=new this.Element;g.init(this,a);return g},draw:A,getRadialAttr:function(a,g){return{cx:a[0]-a[2]/2+g.cx*a[2],cy:a[1]-a[2]/2+g.cy*a[2],r:g.r*a[2]}},getSpanWidth:function(a,g){var b=a.getBBox(!0).width;!L&&this.forExport&&(b=this.measureSpanWidth(g.firstChild.data,a.styles));return b},applyEllipsis:function(a,g,b,v){var c=a.rotation,y=b,k,d=0,q=b.length,e=function(a){g.removeChild(g.firstChild);
a&&g.appendChild(m.createTextNode(a))},z;a.rotation=0;y=this.getSpanWidth(a,g);if(z=y>v){for(;d<=q;)k=Math.ceil((d+q)/2),y=b.substring(0,k)+"\u2026",e(y),y=this.getSpanWidth(a,g),d===q?d=q+1:y>v?q=k-1:d=k;0===q&&e("")}a.rotation=c;return z},escapes:{"\x26":"\x26amp;","\x3c":"\x26lt;","\x3e":"\x26gt;","'":"\x26#39;",'"':"\x26quot;"},buildText:function(a){var b=a.element,v=this,c=v.forExport,y=G(a.textStr,"").toString(),q=-1!==y.indexOf("\x3c"),e=b.childNodes,z,h,A,J,t=f(b,"x"),x=a.styles,B=a.textWidth,
l=x&&x.lineHeight,C=x&&x.textOutline,u=x&&"ellipsis"===x.textOverflow,Q=x&&"nowrap"===x.whiteSpace,w=x&&x.fontSize,R,I,r=e.length,x=B&&!a.added&&this.box,p=function(a){var c;c=/(px|em)$/.test(a&&a.style.fontSize)?a.style.fontSize:w||v.style.fontSize||12;return l?g(l):v.fontMetrics(c,a.getAttribute("style")?a:b).h},K=function(a){F(v.escapes,function(g,b){a=a.replace(new RegExp(g,"g"),b)});return a};R=[y,u,Q,l,C,w,B].join();if(R!==a.textCache){for(a.textCache=R;r--;)b.removeChild(e[r]);q||C||u||B||
-1!==y.indexOf(" ")?(z=/<.*class="([^"]+)".*>/,h=/<.*style="([^"]+)".*>/,A=/<.*href="([^"]+)".*>/,x&&x.appendChild(b),y=q?y.replace(/<(b|strong)>/g,'\x3cspan style\x3d"font-weight:bold"\x3e').replace(/<(i|em)>/g,'\x3cspan style\x3d"font-style:italic"\x3e').replace(/<a/g,"\x3cspan").replace(/<\/(b|strong|i|em|a)>/g,"\x3c/span\x3e").split(/<br.*?>/g):[y],y=k(y,function(a){return""!==a}),d(y,function(g,y){var k,q=0;g=g.replace(/^\s+|\s+$/g,"").replace(/<span/g,"|||\x3cspan").replace(/<\/span>/g,"\x3c/span\x3e|||");
k=g.split("|||");d(k,function(g){if(""!==g||1===k.length){var d={},e=m.createElementNS(v.SVG_NS,"tspan"),x,F;z.test(g)&&(x=g.match(z)[1],f(e,"class",x));h.test(g)&&(F=g.match(h)[1].replace(/(;| |^)color([ :])/,"$1fill$2"),f(e,"style",F));A.test(g)&&!c&&(f(e,"onclick",'location.href\x3d"'+g.match(A)[1]+'"'),f(e,"class","highcharts-anchor"),n(e,{cursor:"pointer"}));g=K(g.replace(/<[a-zA-Z\/](.|\n)*?>/g,"")||" ");if(" "!==g){e.appendChild(m.createTextNode(g));q?d.dx=0:y&&null!==t&&(d.x=t);f(e,d);b.appendChild(e);
!q&&I&&(!L&&c&&n(e,{display:"block"}),f(e,"dy",p(e)));if(B){d=g.replace(/([^\^])-/g,"$1- ").split(" ");x=1<k.length||y||1<d.length&&!Q;var O=[],l,C=p(e),G=a.rotation;for(u&&(J=v.applyEllipsis(a,e,g,B));!u&&x&&(d.length||O.length);)a.rotation=0,l=v.getSpanWidth(a,e),g=l>B,void 0===J&&(J=g),g&&1!==d.length?(e.removeChild(e.firstChild),O.unshift(d.pop())):(d=O,O=[],d.length&&!Q&&(e=m.createElementNS(P,"tspan"),f(e,{dy:C,x:t}),F&&f(e,"style",F),b.appendChild(e)),l>B&&(B=l)),d.length&&e.appendChild(m.createTextNode(d.join(" ").replace(/- /g,
"-")));a.rotation=G}q++}}});I=I||b.childNodes.length}),J&&a.attr("title",a.textStr),x&&x.removeChild(b),C&&a.applyTextOutline&&a.applyTextOutline(C)):b.appendChild(m.createTextNode(K(y)))}},getContrast:function(a){a=r(a).rgba;return 510<a[0]+a[1]+a[2]?"#000000":"#FFFFFF"},button:function(a,g,b,v,d,k,e,z,h){var y=this.label(a,g,b,h,null,null,null,null,"button"),m=0;y.attr(q({padding:8,r:2},d));var A,L,J,t;d=q({fill:"#f7f7f7",stroke:"#cccccc","stroke-width":1,style:{color:"#333333",cursor:"pointer",
fontWeight:"normal"}},d);A=d.style;delete d.style;k=q(d,{fill:"#e6e6e6"},k);L=k.style;delete k.style;e=q(d,{fill:"#e6ebf5",style:{color:"#000000",fontWeight:"bold"}},e);J=e.style;delete e.style;z=q(d,{style:{color:"#cccccc"}},z);t=z.style;delete z.style;H(y.element,K?"mouseover":"mouseenter",function(){3!==m&&y.setState(1)});H(y.element,K?"mouseout":"mouseleave",function(){3!==m&&y.setState(m)});y.setState=function(a){1!==a&&(y.state=m=a);y.removeClass(/highcharts-button-(normal|hover|pressed|disabled)/).addClass("highcharts-button-"+
["normal","hover","pressed","disabled"][a||0]);y.attr([d,k,e,z][a||0]).css([A,L,J,t][a||0])};y.attr(d).css(c({cursor:"default"},A));return y.on("click",function(a){3!==m&&v.call(y,a)})},crispLine:function(a,g){a[1]===a[4]&&(a[1]=a[4]=Math.round(a[1])-g%2/2);a[2]===a[5]&&(a[2]=a[5]=Math.round(a[2])+g%2/2);return a},path:function(a){var g={fill:"none"};I(a)?g.d=a:t(a)&&c(g,a);return this.createElement("path").attr(g)},circle:function(a,g,b){a=t(a)?a:{x:a,y:g,r:b};g=this.createElement("circle");g.xSetter=
g.ySetter=function(a,g,b){b.setAttribute("c"+g,a)};return g.attr(a)},arc:function(a,g,b,v,c,d){t(a)?(v=a,g=v.y,b=v.r,a=v.x):v={innerR:v,start:c,end:d};a=this.symbol("arc",a,g,b,b,v);a.r=b;return a},rect:function(a,g,b,v,c,d){c=t(a)?a.r:c;var k=this.createElement("rect");a=t(a)?a:void 0===a?{}:{x:a,y:g,width:Math.max(b,0),height:Math.max(v,0)};void 0!==d&&(a.strokeWidth=d,a=k.crisp(a));a.fill="none";c&&(a.r=c);k.rSetter=function(a,g,b){f(b,{rx:a,ry:a})};return k.attr(a)},setSize:function(a,g,b){var v=
this.alignedObjects,c=v.length;this.width=a;this.height=g;for(this.boxWrapper.animate({width:a,height:g},{step:function(){this.attr({viewBox:"0 0 "+this.attr("width")+" "+this.attr("height")})},duration:G(b,!0)?void 0:0});c--;)v[c].align()},g:function(a){var g=this.createElement("g");return a?g.attr({"class":"highcharts-"+a}):g},image:function(a,g,b,v,d){var k={preserveAspectRatio:"none"};1<arguments.length&&c(k,{x:g,y:b,width:v,height:d});k=this.createElement("image").attr(k);k.element.setAttributeNS?
k.element.setAttributeNS("http://www.w3.org/1999/xlink","href",a):k.element.setAttribute("hc-svg-href",a);return k},symbol:function(a,g,b,v,k,q){var e=this,y,z=/^url\((.*?)\)$/,h=z.test(a),A=!h&&(this.symbols[a]?a:"circle"),L=A&&this.symbols[A],t=u(g)&&L&&L.call(this.symbols,Math.round(g),Math.round(b),v,k,q),x,F;L?(y=this.path(t),y.attr("fill","none"),c(y,{symbolName:A,x:g,y:b,width:v,height:k}),q&&c(y,q)):h&&(x=a.match(z)[1],y=this.image(x),y.imgwidth=G(J[x]&&J[x].width,q&&q.width),y.imgheight=
G(J[x]&&J[x].height,q&&q.height),F=function(){y.attr({width:y.width,height:y.height})},d(["width","height"],function(a){y[a+"Setter"]=function(a,g){var b={},v=this["img"+g],c="width"===g?"translateX":"translateY";this[g]=a;u(v)&&(this.element&&this.element.setAttribute(g,v),this.alignByTranslate||(b[c]=((this[g]||0)-v)/2,this.attr(b)))}}),u(g)&&y.attr({x:g,y:b}),y.isImg=!0,u(y.imgwidth)&&u(y.imgheight)?F():(y.attr({width:0,height:0}),w("img",{onload:function(){var a=l[e.chartIndex];0===this.width&&
(n(this,{position:"absolute",top:"-999em"}),m.body.appendChild(this));J[x]={width:this.width,height:this.height};y.imgwidth=this.width;y.imgheight=this.height;y.element&&F();this.parentNode&&this.parentNode.removeChild(this);e.imgCount--;if(!e.imgCount&&a&&a.onload)a.onload()},src:x}),this.imgCount++));return y},symbols:{circle:function(a,g,b,v){return this.arc(a+b/2,g+v/2,b/2,v/2,{start:0,end:2*Math.PI,open:!1})},square:function(a,g,b,v){return["M",a,g,"L",a+b,g,a+b,g+v,a,g+v,"Z"]},triangle:function(a,
g,b,v){return["M",a+b/2,g,"L",a+b,g+v,a,g+v,"Z"]},"triangle-down":function(a,g,b,v){return["M",a,g,"L",a+b,g,a+b/2,g+v,"Z"]},diamond:function(a,g,b,v){return["M",a+b/2,g,"L",a+b,g+v/2,a+b/2,g+v,a,g+v/2,"Z"]},arc:function(a,g,b,v,c){var k=c.start,d=c.r||b,q=c.r||v||b,e=c.end-.001;b=c.innerR;v=G(c.open,.001>Math.abs(c.end-c.start-2*Math.PI));var y=Math.cos(k),z=Math.sin(k),h=Math.cos(e),e=Math.sin(e);c=.001>c.end-k-Math.PI?0:1;d=["M",a+d*y,g+q*z,"A",d,q,0,c,1,a+d*h,g+q*e];u(b)&&d.push(v?"M":"L",a+b*
h,g+b*e,"A",b,b,0,c,0,a+b*y,g+b*z);d.push(v?"":"Z");return d},callout:function(a,g,b,v,c){var d=Math.min(c&&c.r||0,b,v),k=d+6,q=c&&c.anchorX;c=c&&c.anchorY;var e;e=["M",a+d,g,"L",a+b-d,g,"C",a+b,g,a+b,g,a+b,g+d,"L",a+b,g+v-d,"C",a+b,g+v,a+b,g+v,a+b-d,g+v,"L",a+d,g+v,"C",a,g+v,a,g+v,a,g+v-d,"L",a,g+d,"C",a,g,a,g,a+d,g];q&&q>b?c>g+k&&c<g+v-k?e.splice(13,3,"L",a+b,c-6,a+b+6,c,a+b,c+6,a+b,g+v-d):e.splice(13,3,"L",a+b,v/2,q,c,a+b,v/2,a+b,g+v-d):q&&0>q?c>g+k&&c<g+v-k?e.splice(33,3,"L",a,c+6,a-6,c,a,c-6,
a,g+d):e.splice(33,3,"L",a,v/2,q,c,a,v/2,a,g+d):c&&c>v&&q>a+k&&q<a+b-k?e.splice(23,3,"L",q+6,g+v,q,g+v+6,q-6,g+v,a+d,g+v):c&&0>c&&q>a+k&&q<a+b-k&&e.splice(3,3,"L",q-6,g,q,g-6,q+6,g,b-d,g);return e}},clipRect:function(g,b,v,c){var d=a.uniqueKey(),k=this.createElement("clipPath").attr({id:d}).add(this.defs);g=this.rect(g,b,v,c,0).add(k);g.id=d;g.clipPath=k;g.count=0;return g},text:function(a,g,b,v){var c={};if(v&&(this.allowHTML||!this.forExport))return this.html(a,g,b);c.x=Math.round(g||0);b&&(c.y=
Math.round(b));if(a||0===a)c.text=a;a=this.createElement("text").attr(c);v||(a.xSetter=function(a,g,b){var v=b.getElementsByTagName("tspan"),c,d=b.getAttribute(g),k;for(k=0;k<v.length;k++)c=v[k],c.getAttribute(g)===d&&c.setAttribute(g,a);b.setAttribute(g,a)});return a},fontMetrics:function(a,b){a=a||b&&b.style&&b.style.fontSize||this.style&&this.style.fontSize;a=/px/.test(a)?g(a):/em/.test(a)?parseFloat(a)*(b?this.fontMetrics(null,b.parentNode).f:16):12;b=24>a?a+3:Math.round(1.2*a);return{h:b,b:Math.round(.8*
b),f:a}},rotCorr:function(a,g,b){var v=a;g&&b&&(v=Math.max(v*Math.cos(g*e),4));return{x:-a/3*Math.sin(g*e),y:v}},label:function(g,b,k,e,z,h,m,A,L){var y=this,J=y.g("button"!==L&&"label"),t=J.text=y.text("",0,0,m).attr({zIndex:1}),x,F,n=0,B=3,l=0,C,f,Q,G,w,R={},I,P,r=/^url\((.*?)\)$/.test(e),p=r,K,O,N,T;L&&J.addClass("highcharts-"+L);p=r;K=function(){return(I||0)%2/2};O=function(){var a=t.element.style,g={};F=(void 0===C||void 0===f||w)&&u(t.textStr)&&t.getBBox();J.width=(C||F.width||0)+2*B+l;J.height=
(f||F.height||0)+2*B;P=B+y.fontMetrics(a&&a.fontSize,t).b;p&&(x||(J.box=x=y.symbols[e]||r?y.symbol(e):y.rect(),x.addClass(("button"===L?"":"highcharts-label-box")+(L?" highcharts-"+L+"-box":"")),x.add(J),a=K(),g.x=a,g.y=(A?-P:0)+a),g.width=Math.round(J.width),g.height=Math.round(J.height),x.attr(c(g,R)),R={})};N=function(){var a=l+B,g;g=A?0:P;u(C)&&F&&("center"===w||"right"===w)&&(a+={center:.5,right:1}[w]*(C-F.width));if(a!==t.x||g!==t.y)t.attr("x",a),void 0!==g&&t.attr("y",g);t.x=a;t.y=g};T=function(a,
g){x?x.attr(a,g):R[a]=g};J.onAdd=function(){t.add(J);J.attr({text:g||0===g?g:"",x:b,y:k});x&&u(z)&&J.attr({anchorX:z,anchorY:h})};J.widthSetter=function(g){C=a.isNumber(g)?g:null};J.heightSetter=function(a){f=a};J["text-alignSetter"]=function(a){w=a};J.paddingSetter=function(a){u(a)&&a!==B&&(B=J.padding=a,N())};J.paddingLeftSetter=function(a){u(a)&&a!==l&&(l=a,N())};J.alignSetter=function(a){a={left:0,center:.5,right:1}[a];a!==n&&(n=a,F&&J.attr({x:Q}))};J.textSetter=function(a){void 0!==a&&t.textSetter(a);
O();N()};J["stroke-widthSetter"]=function(a,g){a&&(p=!0);I=this["stroke-width"]=a;T(g,a)};J.strokeSetter=J.fillSetter=J.rSetter=function(a,g){"r"!==g&&("fill"===g&&a&&(p=!0),J[g]=a);T(g,a)};J.anchorXSetter=function(a,g){z=J.anchorX=a;T(g,Math.round(a)-K()-Q)};J.anchorYSetter=function(a,g){h=J.anchorY=a;T(g,a-G)};J.xSetter=function(a){J.x=a;n&&(a-=n*((C||F.width)+2*B));Q=Math.round(a);J.attr("translateX",Q)};J.ySetter=function(a){G=J.y=Math.round(a);J.attr("translateY",G)};var U=J.css;return c(J,{css:function(a){if(a){var g=
{};a=q(a);d(J.textProps,function(b){void 0!==a[b]&&(g[b]=a[b],delete a[b])});t.css(g)}return U.call(J,a)},getBBox:function(){return{width:F.width+2*B,height:F.height+2*B,x:F.x-B,y:F.y-B}},shadow:function(a){a&&(O(),x&&x.shadow(a));return J},destroy:function(){v(J.element,"mouseenter");v(J.element,"mouseleave");t&&(t=t.destroy());x&&(x=x.destroy());E.prototype.destroy.call(J);J=y=O=N=T=null}})}});a.Renderer=D})(M);(function(a){var E=a.attr,D=a.createElement,H=a.css,p=a.defined,f=a.each,l=a.extend,
r=a.isFirefox,n=a.isMS,w=a.isWebKit,u=a.pick,e=a.pInt,h=a.SVGRenderer,m=a.win,d=a.wrap;l(a.SVGElement.prototype,{htmlCss:function(a){var b=this.element;if(b=a&&"SPAN"===b.tagName&&a.width)delete a.width,this.textWidth=b,this.updateTransform();a&&"ellipsis"===a.textOverflow&&(a.whiteSpace="nowrap",a.overflow="hidden");this.styles=l(this.styles,a);H(this.element,a);return this},htmlGetBBox:function(){var a=this.element;return{x:a.offsetLeft,y:a.offsetTop,width:a.offsetWidth,height:a.offsetHeight}},
htmlUpdateTransform:function(){if(this.added){var a=this.renderer,b=this.element,d=this.translateX||0,z=this.translateY||0,h=this.x||0,m=this.y||0,x=this.textAlign||"left",n={left:0,center:.5,right:1}[x],t=this.styles;H(b,{marginLeft:d,marginTop:z});this.shadows&&f(this.shadows,function(a){H(a,{marginLeft:d+1,marginTop:z+1})});this.inverted&&f(b.childNodes,function(c){a.invertChild(c,b)});if("SPAN"===b.tagName){var l=this.rotation,u=e(this.textWidth),q=t&&t.whiteSpace,A=[l,x,b.innerHTML,this.textWidth,
this.textAlign].join();A!==this.cTT&&(t=a.fontMetrics(b.style.fontSize).b,p(l)&&this.setSpanRotation(l,n,t),H(b,{width:"",whiteSpace:q||"nowrap"}),b.offsetWidth>u&&/[ \-]/.test(b.textContent||b.innerText)&&H(b,{width:u+"px",display:"block",whiteSpace:q||"normal"}),this.getSpanCorrection(b.offsetWidth,t,n,l,x));H(b,{left:h+(this.xCorr||0)+"px",top:m+(this.yCorr||0)+"px"});w&&(t=b.offsetHeight);this.cTT=A}}else this.alignOnAdd=!0},setSpanRotation:function(a,b,d){var c={},k=this.renderer.getTransformKey();
c[k]=c.transform="rotate("+a+"deg)";c[k+(r?"Origin":"-origin")]=c.transformOrigin=100*b+"% "+d+"px";H(this.element,c)},getSpanCorrection:function(a,b,d){this.xCorr=-a*d;this.yCorr=-b}});l(h.prototype,{getTransformKey:function(){return n&&!/Edge/.test(m.navigator.userAgent)?"-ms-transform":w?"-webkit-transform":r?"MozTransform":m.opera?"-o-transform":""},html:function(a,b,k){var c=this.createElement("span"),e=c.element,h=c.renderer,m=h.isSVG,w=function(a,b){f(["opacity","visibility"],function(c){d(a,
c+"Setter",function(a,c,d,k){a.call(this,c,d,k);b[d]=c})})};c.textSetter=function(a){a!==e.innerHTML&&delete this.bBox;this.textStr=a;e.innerHTML=u(a,"");c.htmlUpdateTransform()};m&&w(c,c.element.style);c.xSetter=c.ySetter=c.alignSetter=c.rotationSetter=function(a,b){"align"===b&&(b="textAlign");c[b]=a;c.htmlUpdateTransform()};c.attr({text:a,x:Math.round(b),y:Math.round(k)}).css({fontFamily:this.style.fontFamily,fontSize:this.style.fontSize,position:"absolute"});e.style.whiteSpace="nowrap";c.css=
c.htmlCss;m&&(c.add=function(a){var b,d=h.box.parentNode,k=[];if(this.parentGroup=a){if(b=a.div,!b){for(;a;)k.push(a),a=a.parentGroup;f(k.reverse(),function(a){function e(g,b){a[b]=g;n?q[h.getTransformKey()]="translate("+(a.x||a.translateX)+"px,"+(a.y||a.translateY)+"px)":"translateX"===b?q.left=g+"px":q.top=g+"px";a.doTransform=!0}var q,g=E(a.element,"class");g&&(g={className:g});b=a.div=a.div||D("div",g,{position:"absolute",left:(a.translateX||0)+"px",top:(a.translateY||0)+"px",display:a.display,
opacity:a.opacity,pointerEvents:a.styles&&a.styles.pointerEvents},b||d);q=b.style;l(a,{classSetter:function(a){return function(g){this.element.setAttribute("class",g);a.className=g}}(b),on:function(){k[0].div&&c.on.apply({element:k[0].div},arguments);return a},translateXSetter:e,translateYSetter:e});w(a,q)})}}else b=d;b.appendChild(e);c.added=!0;c.alignOnAdd&&c.htmlUpdateTransform();return c});return c}})})(M);(function(a){function E(){var n=a.defaultOptions.global,l=r.moment;if(n.timezone){if(l)return function(a){return-l.tz(a,
n.timezone).utcOffset()};a.error(25)}return n.useUTC&&n.getTimezoneOffset}function D(){var n=a.defaultOptions.global,f,u=n.useUTC,e=u?"getUTC":"get",h=u?"setUTC":"set",m="Minutes Hours Day Date Month FullYear".split(" "),d=m.concat(["Milliseconds","Seconds"]);a.Date=f=n.Date||r.Date;f.hcTimezoneOffset=u&&n.timezoneOffset;f.hcGetTimezoneOffset=E();f.hcHasTimeZone=!(!f.hcTimezoneOffset&&!f.hcGetTimezoneOffset);f.hcMakeTime=function(a,b,d,e,h,m){var c;u?(c=f.UTC.apply(0,arguments),c+=p(c)):c=(new f(a,
b,l(d,1),l(e,0),l(h,0),l(m,0))).getTime();return c};for(n=0;n<m.length;n++)f["hcGet"+m[n]]=e+m[n];for(n=0;n<d.length;n++)f["hcSet"+d[n]]=h+d[n]}var H=a.color,p=a.getTZOffset,f=a.merge,l=a.pick,r=a.win;a.defaultOptions={colors:"#7cb5ec #434348 #90ed7d #f7a35c #8085e9 #f15c80 #e4d354 #2b908f #f45b5b #91e8e1".split(" "),symbols:["circle","diamond","square","triangle","triangle-down"],lang:{loading:"Loading...",months:"January February March April May June July August September October November December".split(" "),
shortMonths:"Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec".split(" "),weekdays:"Sunday Monday Tuesday Wednesday Thursday Friday Saturday".split(" "),decimalPoint:".",numericSymbols:"kMGTPE".split(""),resetZoom:"Reset zoom",resetZoomTitle:"Reset zoom level 1:1",thousandsSep:" "},global:{useUTC:!0},chart:{borderRadius:0,defaultSeriesType:"line",ignoreHiddenSeries:!0,spacing:[10,10,15,10],resetZoomButton:{theme:{zIndex:6},position:{align:"right",x:-10,y:10}},width:null,height:null,borderColor:"#335cad",
backgroundColor:"#ffffff",plotBorderColor:"#cccccc"},title:{text:"Chart title",align:"center",margin:15,widthAdjust:-44},subtitle:{text:"",align:"center",widthAdjust:-44},plotOptions:{},labels:{style:{position:"absolute",color:"#333333"}},legend:{enabled:!0,align:"center",layout:"horizontal",labelFormatter:function(){return this.name},borderColor:"#999999",borderRadius:0,navigation:{activeColor:"#003399",inactiveColor:"#cccccc"},itemStyle:{color:"#333333",fontSize:"12px",fontWeight:"bold",textOverflow:"ellipsis"},
itemHoverStyle:{color:"#000000"},itemHiddenStyle:{color:"#cccccc"},shadow:!1,itemCheckboxStyle:{position:"absolute",width:"13px",height:"13px"},squareSymbol:!0,symbolPadding:5,verticalAlign:"bottom",x:0,y:0,title:{style:{fontWeight:"bold"}}},loading:{labelStyle:{fontWeight:"bold",position:"relative",top:"45%"},style:{position:"absolute",backgroundColor:"#ffffff",opacity:.5,textAlign:"center"}},tooltip:{enabled:!0,animation:a.svg,borderRadius:3,dateTimeLabelFormats:{millisecond:"%A, %b %e, %H:%M:%S.%L",
second:"%A, %b %e, %H:%M:%S",minute:"%A, %b %e, %H:%M",hour:"%A, %b %e, %H:%M",day:"%A, %b %e, %Y",week:"Week from %A, %b %e, %Y",month:"%B %Y",year:"%Y"},footerFormat:"",padding:8,snap:a.isTouchDevice?25:10,backgroundColor:H("#f7f7f7").setOpacity(.85).get(),borderWidth:1,headerFormat:'\x3cspan style\x3d"font-size: 10px"\x3e{point.key}\x3c/span\x3e\x3cbr/\x3e',pointFormat:'\x3cspan style\x3d"color:{point.color}"\x3e\u25cf\x3c/span\x3e {series.name}: \x3cb\x3e{point.y}\x3c/b\x3e\x3cbr/\x3e',shadow:!0,
style:{color:"#333333",cursor:"default",fontSize:"12px",pointerEvents:"none",whiteSpace:"nowrap"}},credits:{enabled:!0,href:"http://www.highcharts.com",position:{align:"right",x:-10,verticalAlign:"bottom",y:-5},style:{cursor:"pointer",color:"#999999",fontSize:"9px"},text:"Highcharts.com"}};a.setOptions=function(n){a.defaultOptions=f(!0,a.defaultOptions,n);D();return a.defaultOptions};a.getOptions=function(){return a.defaultOptions};a.defaultPlotOptions=a.defaultOptions.plotOptions;D()})(M);(function(a){var E=
a.correctFloat,D=a.defined,H=a.destroyObjectProperties,p=a.isNumber,f=a.merge,l=a.pick,r=a.deg2rad;a.Tick=function(a,l,f,e){this.axis=a;this.pos=l;this.type=f||"";this.isNewLabel=this.isNew=!0;f||e||this.addLabel()};a.Tick.prototype={addLabel:function(){var a=this.axis,w=a.options,u=a.chart,e=a.categories,h=a.names,m=this.pos,d=w.labels,c=a.tickPositions,b=m===c[0],k=m===c[c.length-1],h=e?l(e[m],h[m],m):m,e=this.label,c=c.info,z;a.isDatetimeAxis&&c&&(z=w.dateTimeLabelFormats[c.higherRanks[m]||c.unitName]);
this.isFirst=b;this.isLast=k;w=a.labelFormatter.call({axis:a,chart:u,isFirst:b,isLast:k,dateTimeLabelFormat:z,value:a.isLog?E(a.lin2log(h)):h,pos:m});D(e)?e&&e.attr({text:w}):(this.labelLength=(this.label=e=D(w)&&d.enabled?u.renderer.text(w,0,0,d.useHTML).css(f(d.style)).add(a.labelGroup):null)&&e.getBBox().width,this.rotation=0)},getLabelSize:function(){return this.label?this.label.getBBox()[this.axis.horiz?"height":"width"]:0},handleOverflow:function(a){var f=this.axis,n=f.options.labels,e=a.x,
h=f.chart.chartWidth,m=f.chart.spacing,d=l(f.labelLeft,Math.min(f.pos,m[3])),m=l(f.labelRight,Math.max(f.isRadial?0:f.pos+f.len,h-m[1])),c=this.label,b=this.rotation,k={left:0,center:.5,right:1}[f.labelAlign||c.attr("align")],z=c.getBBox().width,B=f.getSlotWidth(),I=B,x=1,p,t={};if(b||!1===n.overflow)0>b&&e-k*z<d?p=Math.round(e/Math.cos(b*r)-d):0<b&&e+k*z>m&&(p=Math.round((h-e)/Math.cos(b*r)));else if(h=e+(1-k)*z,e-k*z<d?I=a.x+I*(1-k)-d:h>m&&(I=m-a.x+I*k,x=-1),I=Math.min(B,I),I<B&&"center"===f.labelAlign&&
(a.x+=x*(B-I-k*(B-Math.min(z,I)))),z>I||f.autoRotation&&(c.styles||{}).width)p=I;p&&(t.width=p,(n.style||{}).textOverflow||(t.textOverflow="ellipsis"),c.css(t))},getPosition:function(a,f,l,e){var h=this.axis,m=h.chart,d=e&&m.oldChartHeight||m.chartHeight;return{x:a?h.translate(f+l,null,null,e)+h.transB:h.left+h.offset+(h.opposite?(e&&m.oldChartWidth||m.chartWidth)-h.right-h.left:0),y:a?d-h.bottom+h.offset-(h.opposite?h.height:0):d-h.translate(f+l,null,null,e)-h.transB}},getLabelPosition:function(a,
f,l,e,h,m,d,c){var b=this.axis,k=b.transA,z=b.reversed,B=b.staggerLines,n=b.tickRotCorr||{x:0,y:0},x=h.y,u=e||b.reserveSpaceDefault?0:-b.labelOffset*("center"===b.labelAlign?.5:1);D(x)||(x=0===b.side?l.rotation?-8:-l.getBBox().height:2===b.side?n.y+8:Math.cos(l.rotation*r)*(n.y-l.getBBox(!1,0).height/2));a=a+h.x+u+n.x-(m&&e?m*k*(z?-1:1):0);f=f+x-(m&&!e?m*k*(z?1:-1):0);B&&(l=d/(c||1)%B,b.opposite&&(l=B-l-1),f+=b.labelOffset/B*l);return{x:a,y:Math.round(f)}},getMarkPath:function(a,f,l,e,h,m){return m.crispLine(["M",
a,f,"L",a+(h?0:-l),f+(h?l:0)],e)},renderGridLine:function(a,f,l){var e=this.axis,h=e.options,m=this.gridLine,d={},c=this.pos,b=this.type,k=e.tickmarkOffset,z=e.chart.renderer,B=b?b+"Grid":"grid",n=h[B+"LineWidth"],x=h[B+"LineColor"],h=h[B+"LineDashStyle"];m||(d.stroke=x,d["stroke-width"]=n,h&&(d.dashstyle=h),b||(d.zIndex=1),a&&(d.opacity=0),this.gridLine=m=z.path().attr(d).addClass("highcharts-"+(b?b+"-":"")+"grid-line").add(e.gridGroup));if(!a&&m&&(a=e.getPlotLinePath(c+k,m.strokeWidth()*l,a,!0)))m[this.isNew?
"attr":"animate"]({d:a,opacity:f})},renderMark:function(a,f,u){var e=this.axis,h=e.options,m=e.chart.renderer,d=this.type,c=d?d+"Tick":"tick",b=e.tickSize(c),k=this.mark,z=!k,B=a.x;a=a.y;var n=l(h[c+"Width"],!d&&e.isXAxis?1:0),h=h[c+"Color"];b&&(e.opposite&&(b[0]=-b[0]),z&&(this.mark=k=m.path().addClass("highcharts-"+(d?d+"-":"")+"tick").add(e.axisGroup),k.attr({stroke:h,"stroke-width":n})),k[z?"attr":"animate"]({d:this.getMarkPath(B,a,b[0],k.strokeWidth()*u,e.horiz,m),opacity:f}))},renderLabel:function(a,
f,u,e){var h=this.axis,m=h.horiz,d=h.options,c=this.label,b=d.labels,k=b.step,h=h.tickmarkOffset,z=!0,B=a.x;a=a.y;c&&p(B)&&(c.xy=a=this.getLabelPosition(B,a,c,m,b,h,e,k),this.isFirst&&!this.isLast&&!l(d.showFirstLabel,1)||this.isLast&&!this.isFirst&&!l(d.showLastLabel,1)?z=!1:!m||b.step||b.rotation||f||0===u||this.handleOverflow(a),k&&e%k&&(z=!1),z&&p(a.y)?(a.opacity=u,c[this.isNewLabel?"attr":"animate"](a),this.isNewLabel=!1):(c.attr("y",-9999),this.isNewLabel=!0))},render:function(a,f,u){var e=
this.axis,h=e.horiz,m=this.getPosition(h,this.pos,e.tickmarkOffset,f),d=m.x,c=m.y,e=h&&d===e.pos+e.len||!h&&c===e.pos?-1:1;u=l(u,1);this.isActive=!0;this.renderGridLine(f,u,e);this.renderMark(m,u,e);this.renderLabel(m,f,u,a);this.isNew=!1},destroy:function(){H(this,this.axis)}}})(M);var V=function(a){var E=a.addEvent,D=a.animObject,H=a.arrayMax,p=a.arrayMin,f=a.color,l=a.correctFloat,r=a.defaultOptions,n=a.defined,w=a.deg2rad,u=a.destroyObjectProperties,e=a.each,h=a.extend,m=a.fireEvent,d=a.format,
c=a.getMagnitude,b=a.grep,k=a.inArray,z=a.isArray,B=a.isNumber,I=a.isString,x=a.merge,K=a.normalizeTickInterval,t=a.objectEach,C=a.pick,N=a.removeEvent,q=a.splat,A=a.syncTimeout,F=a.Tick,G=function(){this.init.apply(this,arguments)};a.extend(G.prototype,{defaultOptions:{dateTimeLabelFormats:{millisecond:"%H:%M:%S.%L",second:"%H:%M:%S",minute:"%H:%M",hour:"%H:%M",day:"%e. %b",week:"%e. %b",month:"%b '%y",year:"%Y"},endOnTick:!1,labels:{enabled:!0,style:{color:"#666666",cursor:"default",fontSize:"11px"},
x:0},maxPadding:.01,minorTickLength:2,minorTickPosition:"outside",minPadding:.01,startOfWeek:1,startOnTick:!1,tickLength:10,tickmarkPlacement:"between",tickPixelInterval:100,tickPosition:"outside",title:{align:"middle",style:{color:"#666666"}},type:"linear",minorGridLineColor:"#f2f2f2",minorGridLineWidth:1,minorTickColor:"#999999",lineColor:"#ccd6eb",lineWidth:1,gridLineColor:"#e6e6e6",tickColor:"#ccd6eb"},defaultYAxisOptions:{endOnTick:!0,tickPixelInterval:72,showLastLabel:!0,labels:{x:-8},maxPadding:.05,
minPadding:.05,startOnTick:!0,title:{rotation:270,text:"Values"},stackLabels:{allowOverlap:!1,enabled:!1,formatter:function(){return a.numberFormat(this.total,-1)},style:{fontSize:"11px",fontWeight:"bold",color:"#000000",textOutline:"1px contrast"}},gridLineWidth:1,lineWidth:0},defaultLeftAxisOptions:{labels:{x:-15},title:{rotation:270}},defaultRightAxisOptions:{labels:{x:15},title:{rotation:90}},defaultBottomAxisOptions:{labels:{autoRotation:[-45],x:0},title:{rotation:0}},defaultTopAxisOptions:{labels:{autoRotation:[-45],
x:0},title:{rotation:0}},init:function(a,b){var g=b.isX,v=this;v.chart=a;v.horiz=a.inverted&&!v.isZAxis?!g:g;v.isXAxis=g;v.coll=v.coll||(g?"xAxis":"yAxis");v.opposite=b.opposite;v.side=b.side||(v.horiz?v.opposite?0:2:v.opposite?1:3);v.setOptions(b);var c=this.options,d=c.type;v.labelFormatter=c.labels.formatter||v.defaultLabelFormatter;v.userOptions=b;v.minPixelPadding=0;v.reversed=c.reversed;v.visible=!1!==c.visible;v.zoomEnabled=!1!==c.zoomEnabled;v.hasNames="category"===d||!0===c.categories;v.categories=
c.categories||v.hasNames;v.names=v.names||[];v.plotLinesAndBandsGroups={};v.isLog="logarithmic"===d;v.isDatetimeAxis="datetime"===d;v.positiveValuesOnly=v.isLog&&!v.allowNegativeLog;v.isLinked=n(c.linkedTo);v.ticks={};v.labelEdge=[];v.minorTicks={};v.plotLinesAndBands=[];v.alternateBands={};v.len=0;v.minRange=v.userMinRange=c.minRange||c.maxZoom;v.range=c.range;v.offset=c.offset||0;v.stacks={};v.oldStacks={};v.stacksTouched=0;v.max=null;v.min=null;v.crosshair=C(c.crosshair,q(a.options.tooltip.crosshairs)[g?
0:1],!1);b=v.options.events;-1===k(v,a.axes)&&(g?a.axes.splice(a.xAxis.length,0,v):a.axes.push(v),a[v.coll].push(v));v.series=v.series||[];a.inverted&&!v.isZAxis&&g&&void 0===v.reversed&&(v.reversed=!0);t(b,function(a,g){E(v,g,a)});v.lin2log=c.linearToLogConverter||v.lin2log;v.isLog&&(v.val2lin=v.log2lin,v.lin2val=v.lin2log)},setOptions:function(a){this.options=x(this.defaultOptions,"yAxis"===this.coll&&this.defaultYAxisOptions,[this.defaultTopAxisOptions,this.defaultRightAxisOptions,this.defaultBottomAxisOptions,
this.defaultLeftAxisOptions][this.side],x(r[this.coll],a))},defaultLabelFormatter:function(){var g=this.axis,b=this.value,c=g.categories,k=this.dateTimeLabelFormat,e=r.lang,q=e.numericSymbols,e=e.numericSymbolMagnitude||1E3,h=q&&q.length,m,z=g.options.labels.format,g=g.isLog?Math.abs(b):g.tickInterval;if(z)m=d(z,this);else if(c)m=b;else if(k)m=a.dateFormat(k,b);else if(h&&1E3<=g)for(;h--&&void 0===m;)c=Math.pow(e,h+1),g>=c&&0===10*b%c&&null!==q[h]&&0!==b&&(m=a.numberFormat(b/c,-1)+q[h]);void 0===
m&&(m=1E4<=Math.abs(b)?a.numberFormat(b,-1):a.numberFormat(b,-1,void 0,""));return m},getSeriesExtremes:function(){var a=this,v=a.chart;a.hasVisibleSeries=!1;a.dataMin=a.dataMax=a.threshold=null;a.softThreshold=!a.isXAxis;a.buildStacks&&a.buildStacks();e(a.series,function(g){if(g.visible||!v.options.chart.ignoreHiddenSeries){var c=g.options,d=c.threshold,k;a.hasVisibleSeries=!0;a.positiveValuesOnly&&0>=d&&(d=null);if(a.isXAxis)c=g.xData,c.length&&(g=p(c),k=H(c),B(g)||g instanceof Date||(c=b(c,B),
g=p(c)),a.dataMin=Math.min(C(a.dataMin,c[0],g),g),a.dataMax=Math.max(C(a.dataMax,c[0],k),k));else if(g.getExtremes(),k=g.dataMax,g=g.dataMin,n(g)&&n(k)&&(a.dataMin=Math.min(C(a.dataMin,g),g),a.dataMax=Math.max(C(a.dataMax,k),k)),n(d)&&(a.threshold=d),!c.softThreshold||a.positiveValuesOnly)a.softThreshold=!1}})},translate:function(a,b,c,d,k,e){var g=this.linkedParent||this,v=1,q=0,h=d?g.oldTransA:g.transA;d=d?g.oldMin:g.min;var m=g.minPixelPadding;k=(g.isOrdinal||g.isBroken||g.isLog&&k)&&g.lin2val;
h||(h=g.transA);c&&(v*=-1,q=g.len);g.reversed&&(v*=-1,q-=v*(g.sector||g.len));b?(a=(a*v+q-m)/h+d,k&&(a=g.lin2val(a))):(k&&(a=g.val2lin(a)),a=B(d)?v*(a-d)*h+q+v*m+(B(e)?h*e:0):void 0);return a},toPixels:function(a,b){return this.translate(a,!1,!this.horiz,null,!0)+(b?0:this.pos)},toValue:function(a,b){return this.translate(a-(b?0:this.pos),!0,!this.horiz,null,!0)},getPlotLinePath:function(a,b,c,d,k){var g=this.chart,v=this.left,q=this.top,e,h,m=c&&g.oldChartHeight||g.chartHeight,z=c&&g.oldChartWidth||
g.chartWidth,A;e=this.transB;var t=function(a,g,b){if(a<g||a>b)d?a=Math.min(Math.max(g,a),b):A=!0;return a};k=C(k,this.translate(a,null,null,c));a=c=Math.round(k+e);e=h=Math.round(m-k-e);B(k)?this.horiz?(e=q,h=m-this.bottom,a=c=t(a,v,v+this.width)):(a=v,c=z-this.right,e=h=t(e,q,q+this.height)):(A=!0,d=!1);return A&&!d?null:g.renderer.crispLine(["M",a,e,"L",c,h],b||1)},getLinearTickPositions:function(a,b,c){var g,v=l(Math.floor(b/a)*a);c=l(Math.ceil(c/a)*a);var d=[],k;l(v+a)===v&&(k=20);if(this.single)return[b];
for(b=v;b<=c;){d.push(b);b=l(b+a,k);if(b===g)break;g=b}return d},getMinorTickInterval:function(){var a=this.options;return!0===a.minorTicks?C(a.minorTickInterval,"auto"):!1===a.minorTicks?null:a.minorTickInterval},getMinorTickPositions:function(){var a=this,b=a.options,c=a.tickPositions,d=a.minorTickInterval,k=[],q=a.pointRangePadding||0,h=a.min-q,q=a.max+q,m=q-h;if(m&&m/d<a.len/3)if(a.isLog)e(this.paddedTicks,function(g,b,v){b&&k.push.apply(k,a.getLogTickPositions(d,v[b-1],v[b],!0))});else if(a.isDatetimeAxis&&
"auto"===this.getMinorTickInterval())k=k.concat(a.getTimeTicks(a.normalizeTimeTickInterval(d),h,q,b.startOfWeek));else for(b=h+(c[0]-h)%d;b<=q&&b!==k[0];b+=d)k.push(b);0!==k.length&&a.trimTicks(k);return k},adjustForMinRange:function(){var a=this.options,b=this.min,c=this.max,d,k,q,h,m,z,A,t;this.isXAxis&&void 0===this.minRange&&!this.isLog&&(n(a.min)||n(a.max)?this.minRange=null:(e(this.series,function(a){z=a.xData;for(h=A=a.xIncrement?1:z.length-1;0<h;h--)if(m=z[h]-z[h-1],void 0===q||m<q)q=m}),
this.minRange=Math.min(5*q,this.dataMax-this.dataMin)));c-b<this.minRange&&(k=this.dataMax-this.dataMin>=this.minRange,t=this.minRange,d=(t-c+b)/2,d=[b-d,C(a.min,b-d)],k&&(d[2]=this.isLog?this.log2lin(this.dataMin):this.dataMin),b=H(d),c=[b+t,C(a.max,b+t)],k&&(c[2]=this.isLog?this.log2lin(this.dataMax):this.dataMax),c=p(c),c-b<t&&(d[0]=c-t,d[1]=C(a.min,c-t),b=H(d)));this.min=b;this.max=c},getClosest:function(){var a;this.categories?a=1:e(this.series,function(g){var b=g.closestPointRange,v=g.visible||
!g.chart.options.chart.ignoreHiddenSeries;!g.noSharedTooltip&&n(b)&&v&&(a=n(a)?Math.min(a,b):b)});return a},nameToX:function(a){var g=z(this.categories),b=g?this.categories:this.names,c=a.options.x,d;a.series.requireSorting=!1;n(c)||(c=!1===this.options.uniqueNames?a.series.autoIncrement():k(a.name,b));-1===c?g||(d=b.length):d=c;void 0!==d&&(this.names[d]=a.name);return d},updateNames:function(){var a=this;0<this.names.length&&(this.names.length=0,this.minRange=this.userMinRange,e(this.series||[],
function(g){g.xIncrement=null;if(!g.points||g.isDirtyData)g.processData(),g.generatePoints();e(g.points,function(b,v){var c;b.options&&(c=a.nameToX(b),void 0!==c&&c!==b.x&&(b.x=c,g.xData[v]=c))})}))},setAxisTranslation:function(a){var g=this,b=g.max-g.min,c=g.axisPointRange||0,d,k=0,q=0,h=g.linkedParent,m=!!g.categories,z=g.transA,A=g.isXAxis;if(A||m||c)d=g.getClosest(),h?(k=h.minPointOffset,q=h.pointRangePadding):e(g.series,function(a){var b=m?1:A?C(a.options.pointRange,d,0):g.axisPointRange||0;
a=a.options.pointPlacement;c=Math.max(c,b);g.single||(k=Math.max(k,I(a)?0:b/2),q=Math.max(q,"on"===a?0:b))}),h=g.ordinalSlope&&d?g.ordinalSlope/d:1,g.minPointOffset=k*=h,g.pointRangePadding=q*=h,g.pointRange=Math.min(c,b),A&&(g.closestPointRange=d);a&&(g.oldTransA=z);g.translationSlope=g.transA=z=g.options.staticScale||g.len/(b+q||1);g.transB=g.horiz?g.left:g.bottom;g.minPixelPadding=z*k},minFromRange:function(){return this.max-this.range},setTickInterval:function(g){var b=this,d=b.chart,k=b.options,
q=b.isLog,h=b.log2lin,z=b.isDatetimeAxis,A=b.isXAxis,t=b.isLinked,x=k.maxPadding,f=k.minPadding,F=k.tickInterval,u=k.tickPixelInterval,G=b.categories,p=b.threshold,I=b.softThreshold,r,w,N,D;z||G||t||this.getTickAmount();N=C(b.userMin,k.min);D=C(b.userMax,k.max);t?(b.linkedParent=d[b.coll][k.linkedTo],d=b.linkedParent.getExtremes(),b.min=C(d.min,d.dataMin),b.max=C(d.max,d.dataMax),k.type!==b.linkedParent.options.type&&a.error(11,1)):(!I&&n(p)&&(b.dataMin>=p?(r=p,f=0):b.dataMax<=p&&(w=p,x=0)),b.min=
C(N,r,b.dataMin),b.max=C(D,w,b.dataMax));q&&(b.positiveValuesOnly&&!g&&0>=Math.min(b.min,C(b.dataMin,b.min))&&a.error(10,1),b.min=l(h(b.min),15),b.max=l(h(b.max),15));b.range&&n(b.max)&&(b.userMin=b.min=N=Math.max(b.dataMin,b.minFromRange()),b.userMax=D=b.max,b.range=null);m(b,"foundExtremes");b.beforePadding&&b.beforePadding();b.adjustForMinRange();!(G||b.axisPointRange||b.usePercentage||t)&&n(b.min)&&n(b.max)&&(h=b.max-b.min)&&(!n(N)&&f&&(b.min-=h*f),!n(D)&&x&&(b.max+=h*x));B(k.softMin)&&!B(b.userMin)&&
(b.min=Math.min(b.min,k.softMin));B(k.softMax)&&!B(b.userMax)&&(b.max=Math.max(b.max,k.softMax));B(k.floor)&&(b.min=Math.max(b.min,k.floor));B(k.ceiling)&&(b.max=Math.min(b.max,k.ceiling));I&&n(b.dataMin)&&(p=p||0,!n(N)&&b.min<p&&b.dataMin>=p?b.min=p:!n(D)&&b.max>p&&b.dataMax<=p&&(b.max=p));b.tickInterval=b.min===b.max||void 0===b.min||void 0===b.max?1:t&&!F&&u===b.linkedParent.options.tickPixelInterval?F=b.linkedParent.tickInterval:C(F,this.tickAmount?(b.max-b.min)/Math.max(this.tickAmount-1,1):
void 0,G?1:(b.max-b.min)*u/Math.max(b.len,u));A&&!g&&e(b.series,function(a){a.processData(b.min!==b.oldMin||b.max!==b.oldMax)});b.setAxisTranslation(!0);b.beforeSetTickPositions&&b.beforeSetTickPositions();b.postProcessTickInterval&&(b.tickInterval=b.postProcessTickInterval(b.tickInterval));b.pointRange&&!F&&(b.tickInterval=Math.max(b.pointRange,b.tickInterval));g=C(k.minTickInterval,b.isDatetimeAxis&&b.closestPointRange);!F&&b.tickInterval<g&&(b.tickInterval=g);z||q||F||(b.tickInterval=K(b.tickInterval,
null,c(b.tickInterval),C(k.allowDecimals,!(.5<b.tickInterval&&5>b.tickInterval&&1E3<b.max&&9999>b.max)),!!this.tickAmount));this.tickAmount||(b.tickInterval=b.unsquish());this.setTickPositions()},setTickPositions:function(){var a=this.options,b,c=a.tickPositions;b=this.getMinorTickInterval();var d=a.tickPositioner,k=a.startOnTick,q=a.endOnTick;this.tickmarkOffset=this.categories&&"between"===a.tickmarkPlacement&&1===this.tickInterval?.5:0;this.minorTickInterval="auto"===b&&this.tickInterval?this.tickInterval/
5:b;this.single=this.min===this.max&&n(this.min)&&!this.tickAmount&&(parseInt(this.min,10)===this.min||!1!==a.allowDecimals);this.tickPositions=b=c&&c.slice();!b&&(b=this.isDatetimeAxis?this.getTimeTicks(this.normalizeTimeTickInterval(this.tickInterval,a.units),this.min,this.max,a.startOfWeek,this.ordinalPositions,this.closestPointRange,!0):this.isLog?this.getLogTickPositions(this.tickInterval,this.min,this.max):this.getLinearTickPositions(this.tickInterval,this.min,this.max),b.length>this.len&&(b=
[b[0],b.pop()],b[0]===b[1]&&(b.length=1)),this.tickPositions=b,d&&(d=d.apply(this,[this.min,this.max])))&&(this.tickPositions=b=d);this.paddedTicks=b.slice(0);this.trimTicks(b,k,q);this.isLinked||(this.single&&2>b.length&&(this.min-=.5,this.max+=.5),c||d||this.adjustTickAmount())},trimTicks:function(a,b,c){var g=a[0],d=a[a.length-1],k=this.minPointOffset||0;if(!this.isLinked){if(b&&-Infinity!==g)this.min=g;else for(;this.min-k>a[0];)a.shift();if(c)this.max=d;else for(;this.max+k<a[a.length-1];)a.pop();
0===a.length&&n(g)&&!this.options.tickPositions&&a.push((d+g)/2)}},alignToOthers:function(){var a={},b,c=this.options;!1===this.chart.options.chart.alignTicks||!1===c.alignTicks||this.isLog||e(this.chart[this.coll],function(g){var c=g.options,c=[g.horiz?c.left:c.top,c.width,c.height,c.pane].join();g.series.length&&(a[c]?b=!0:a[c]=1)});return b},getTickAmount:function(){var a=this.options,b=a.tickAmount,c=a.tickPixelInterval;!n(a.tickInterval)&&this.len<c&&!this.isRadial&&!this.isLog&&a.startOnTick&&
a.endOnTick&&(b=2);!b&&this.alignToOthers()&&(b=Math.ceil(this.len/c)+1);4>b&&(this.finalTickAmt=b,b=5);this.tickAmount=b},adjustTickAmount:function(){var a=this.tickInterval,b=this.tickPositions,c=this.tickAmount,d=this.finalTickAmt,k=b&&b.length,q=C(this.threshold,this.softThreshold?0:null);if(this.hasData()){if(k<c){for(;b.length<c;)b.length%2||this.min===q?b.push(l(b[b.length-1]+a)):b.unshift(l(b[0]-a));this.transA*=(k-1)/(c-1);this.min=b[0];this.max=b[b.length-1]}else k>c&&(this.tickInterval*=
2,this.setTickPositions());if(n(d)){for(a=c=b.length;a--;)(3===d&&1===a%2||2>=d&&0<a&&a<c-1)&&b.splice(a,1);this.finalTickAmt=void 0}}},setScale:function(){var a,b;this.oldMin=this.min;this.oldMax=this.max;this.oldAxisLength=this.len;this.setAxisSize();b=this.len!==this.oldAxisLength;e(this.series,function(b){if(b.isDirtyData||b.isDirty||b.xAxis.isDirty)a=!0});b||a||this.isLinked||this.forceRedraw||this.userMin!==this.oldUserMin||this.userMax!==this.oldUserMax||this.alignToOthers()?(this.resetStacks&&
this.resetStacks(),this.forceRedraw=!1,this.getSeriesExtremes(),this.setTickInterval(),this.oldUserMin=this.userMin,this.oldUserMax=this.userMax,this.isDirty||(this.isDirty=b||this.min!==this.oldMin||this.max!==this.oldMax)):this.cleanStacks&&this.cleanStacks()},setExtremes:function(a,b,c,d,k){var g=this,q=g.chart;c=C(c,!0);e(g.series,function(a){delete a.kdTree});k=h(k,{min:a,max:b});m(g,"setExtremes",k,function(){g.userMin=a;g.userMax=b;g.eventArgs=k;c&&q.redraw(d)})},zoom:function(a,b){var g=this.dataMin,
c=this.dataMax,d=this.options,k=Math.min(g,C(d.min,g)),d=Math.max(c,C(d.max,c));if(a!==this.min||b!==this.max)this.allowZoomOutside||(n(g)&&(a<k&&(a=k),a>d&&(a=d)),n(c)&&(b<k&&(b=k),b>d&&(b=d))),this.displayBtn=void 0!==a||void 0!==b,this.setExtremes(a,b,!1,void 0,{trigger:"zoom"});return!0},setAxisSize:function(){var b=this.chart,c=this.options,d=c.offsets||[0,0,0,0],k=this.horiz,q=this.width=Math.round(a.relativeLength(C(c.width,b.plotWidth-d[3]+d[1]),b.plotWidth)),e=this.height=Math.round(a.relativeLength(C(c.height,
b.plotHeight-d[0]+d[2]),b.plotHeight)),h=this.top=Math.round(a.relativeLength(C(c.top,b.plotTop+d[0]),b.plotHeight,b.plotTop)),c=this.left=Math.round(a.relativeLength(C(c.left,b.plotLeft+d[3]),b.plotWidth,b.plotLeft));this.bottom=b.chartHeight-e-h;this.right=b.chartWidth-q-c;this.len=Math.max(k?q:e,0);this.pos=k?c:h},getExtremes:function(){var a=this.isLog,b=this.lin2log;return{min:a?l(b(this.min)):this.min,max:a?l(b(this.max)):this.max,dataMin:this.dataMin,dataMax:this.dataMax,userMin:this.userMin,
userMax:this.userMax}},getThreshold:function(a){var b=this.isLog,g=this.lin2log,c=b?g(this.min):this.min,b=b?g(this.max):this.max;null===a?a=c:c>a?a=c:b<a&&(a=b);return this.translate(a,0,1,0,1)},autoLabelAlign:function(a){a=(C(a,0)-90*this.side+720)%360;return 15<a&&165>a?"right":195<a&&345>a?"left":"center"},tickSize:function(a){var b=this.options,g=b[a+"Length"],c=C(b[a+"Width"],"tick"===a&&this.isXAxis?1:0);if(c&&g)return"inside"===b[a+"Position"]&&(g=-g),[g,c]},labelMetrics:function(){var a=
this.tickPositions&&this.tickPositions[0]||0;return this.chart.renderer.fontMetrics(this.options.labels.style&&this.options.labels.style.fontSize,this.ticks[a]&&this.ticks[a].label)},unsquish:function(){var a=this.options.labels,b=this.horiz,c=this.tickInterval,d=c,k=this.len/(((this.categories?1:0)+this.max-this.min)/c),q,h=a.rotation,m=this.labelMetrics(),z,A=Number.MAX_VALUE,t,x=function(a){a/=k||1;a=1<a?Math.ceil(a):1;return a*c};b?(t=!a.staggerLines&&!a.step&&(n(h)?[h]:k<C(a.autoRotationLimit,
80)&&a.autoRotation))&&e(t,function(a){var b;if(a===h||a&&-90<=a&&90>=a)z=x(Math.abs(m.h/Math.sin(w*a))),b=z+Math.abs(a/360),b<A&&(A=b,q=a,d=z)}):a.step||(d=x(m.h));this.autoRotation=t;this.labelRotation=C(q,h);return d},getSlotWidth:function(){var a=this.chart,b=this.horiz,c=this.options.labels,d=Math.max(this.tickPositions.length-(this.categories?0:1),1),k=a.margin[3];return b&&2>(c.step||0)&&!c.rotation&&(this.staggerLines||1)*this.len/d||!b&&(c.style&&parseInt(c.style.width,10)||k&&k-a.spacing[3]||
.33*a.chartWidth)},renderUnsquish:function(){var a=this.chart,b=a.renderer,c=this.tickPositions,d=this.ticks,k=this.options.labels,q=this.horiz,h=this.getSlotWidth(),m=Math.max(1,Math.round(h-2*(k.padding||5))),z={},A=this.labelMetrics(),t=k.style&&k.style.textOverflow,f,F=0,l,B;I(k.rotation)||(z.rotation=k.rotation||0);e(c,function(a){(a=d[a])&&a.labelLength>F&&(F=a.labelLength)});this.maxLabelLength=F;if(this.autoRotation)F>m&&F>A.h?z.rotation=this.labelRotation:this.labelRotation=0;else if(h&&
(f={width:m+"px"},!t))for(f.textOverflow="clip",l=c.length;!q&&l--;)if(B=c[l],m=d[B].label)m.styles&&"ellipsis"===m.styles.textOverflow?m.css({textOverflow:"clip"}):d[B].labelLength>h&&m.css({width:h+"px"}),m.getBBox().height>this.len/c.length-(A.h-A.f)&&(m.specCss={textOverflow:"ellipsis"});z.rotation&&(f={width:(F>.5*a.chartHeight?.33*a.chartHeight:a.chartHeight)+"px"},t||(f.textOverflow="ellipsis"));if(this.labelAlign=k.align||this.autoLabelAlign(this.labelRotation))z.align=this.labelAlign;e(c,
function(a){var b=(a=d[a])&&a.label;b&&(b.attr(z),f&&b.css(x(f,b.specCss)),delete b.specCss,a.rotation=z.rotation)});this.tickRotCorr=b.rotCorr(A.b,this.labelRotation||0,0!==this.side)},hasData:function(){return this.hasVisibleSeries||n(this.min)&&n(this.max)&&this.tickPositions&&0<this.tickPositions.length},addTitle:function(a){var b=this.chart.renderer,g=this.horiz,c=this.opposite,d=this.options.title,k;this.axisTitle||((k=d.textAlign)||(k=(g?{low:"left",middle:"center",high:"right"}:{low:c?"right":
"left",middle:"center",high:c?"left":"right"})[d.align]),this.axisTitle=b.text(d.text,0,0,d.useHTML).attr({zIndex:7,rotation:d.rotation||0,align:k}).addClass("highcharts-axis-title").css(d.style).add(this.axisGroup),this.axisTitle.isNew=!0);d.style.width||this.isRadial||this.axisTitle.css({width:this.len});this.axisTitle[a?"show":"hide"](!0)},generateTick:function(a){var b=this.ticks;b[a]?b[a].addLabel():b[a]=new F(this,a)},getOffset:function(){var a=this,b=a.chart,c=b.renderer,d=a.options,k=a.tickPositions,
q=a.ticks,h=a.horiz,m=a.side,z=b.inverted&&!a.isZAxis?[1,0,3,2][m]:m,A,x,f=0,F,l=0,B=d.title,u=d.labels,G=0,p=b.axisOffset,b=b.clipOffset,I=[-1,1,1,-1][m],r=d.className,w=a.axisParent,K=this.tickSize("tick");A=a.hasData();a.showAxis=x=A||C(d.showEmpty,!0);a.staggerLines=a.horiz&&u.staggerLines;a.axisGroup||(a.gridGroup=c.g("grid").attr({zIndex:d.gridZIndex||1}).addClass("highcharts-"+this.coll.toLowerCase()+"-grid "+(r||"")).add(w),a.axisGroup=c.g("axis").attr({zIndex:d.zIndex||2}).addClass("highcharts-"+
this.coll.toLowerCase()+" "+(r||"")).add(w),a.labelGroup=c.g("axis-labels").attr({zIndex:u.zIndex||7}).addClass("highcharts-"+a.coll.toLowerCase()+"-labels "+(r||"")).add(w));A||a.isLinked?(e(k,function(b,c){a.generateTick(b,c)}),a.renderUnsquish(),a.reserveSpaceDefault=0===m||2===m||{1:"left",3:"right"}[m]===a.labelAlign,C(u.reserveSpace,"center"===a.labelAlign?!0:null,a.reserveSpaceDefault)&&e(k,function(a){G=Math.max(q[a].getLabelSize(),G)}),a.staggerLines&&(G*=a.staggerLines),a.labelOffset=G*
(a.opposite?-1:1)):t(q,function(a,b){a.destroy();delete q[b]});B&&B.text&&!1!==B.enabled&&(a.addTitle(x),x&&!1!==B.reserveSpace&&(a.titleOffset=f=a.axisTitle.getBBox()[h?"height":"width"],F=B.offset,l=n(F)?0:C(B.margin,h?5:10)));a.renderLine();a.offset=I*C(d.offset,p[m]);a.tickRotCorr=a.tickRotCorr||{x:0,y:0};c=0===m?-a.labelMetrics().h:2===m?a.tickRotCorr.y:0;l=Math.abs(G)+l;G&&(l=l-c+I*(h?C(u.y,a.tickRotCorr.y+8*I):u.x));a.axisTitleMargin=C(F,l);p[m]=Math.max(p[m],a.axisTitleMargin+f+I*a.offset,
l,A&&k.length&&K?K[0]+I*a.offset:0);d=d.offset?0:2*Math.floor(a.axisLine.strokeWidth()/2);b[z]=Math.max(b[z],d)},getLinePath:function(a){var b=this.chart,c=this.opposite,g=this.offset,d=this.horiz,k=this.left+(c?this.width:0)+g,g=b.chartHeight-this.bottom-(c?this.height:0)+g;c&&(a*=-1);return b.renderer.crispLine(["M",d?this.left:k,d?g:this.top,"L",d?b.chartWidth-this.right:k,d?g:b.chartHeight-this.bottom],a)},renderLine:function(){this.axisLine||(this.axisLine=this.chart.renderer.path().addClass("highcharts-axis-line").add(this.axisGroup),
this.axisLine.attr({stroke:this.options.lineColor,"stroke-width":this.options.lineWidth,zIndex:7}))},getTitlePosition:function(){var a=this.horiz,b=this.left,c=this.top,d=this.len,k=this.options.title,q=a?b:c,e=this.opposite,h=this.offset,m=k.x||0,z=k.y||0,A=this.axisTitle,t=this.chart.renderer.fontMetrics(k.style&&k.style.fontSize,A),A=Math.max(A.getBBox(null,0).height-t.h-1,0),d={low:q+(a?0:d),middle:q+d/2,high:q+(a?d:0)}[k.align],b=(a?c+this.height:b)+(a?1:-1)*(e?-1:1)*this.axisTitleMargin+[-A,
A,t.f,-A][this.side];return{x:a?d+m:b+(e?this.width:0)+h+m,y:a?b+z-(e?this.height:0)+h:d+z}},renderMinorTick:function(a){var b=this.chart.hasRendered&&B(this.oldMin),c=this.minorTicks;c[a]||(c[a]=new F(this,a,"minor"));b&&c[a].isNew&&c[a].render(null,!0);c[a].render(null,!1,1)},renderTick:function(a,b){var c=this.isLinked,g=this.ticks,d=this.chart.hasRendered&&B(this.oldMin);if(!c||a>=this.min&&a<=this.max)g[a]||(g[a]=new F(this,a)),d&&g[a].isNew&&g[a].render(b,!0,.1),g[a].render(b)},render:function(){var b=
this,c=b.chart,d=b.options,k=b.isLog,q=b.lin2log,h=b.isLinked,m=b.tickPositions,z=b.axisTitle,x=b.ticks,f=b.minorTicks,l=b.alternateBands,C=d.stackLabels,n=d.alternateGridColor,u=b.tickmarkOffset,G=b.axisLine,p=b.showAxis,I=D(c.renderer.globalAnimation),r,w;b.labelEdge.length=0;b.overlap=!1;e([x,f,l],function(a){t(a,function(a){a.isActive=!1})});if(b.hasData()||h)b.minorTickInterval&&!b.categories&&e(b.getMinorTickPositions(),function(a){b.renderMinorTick(a)}),m.length&&(e(m,function(a,c){b.renderTick(a,
c)}),u&&(0===b.min||b.single)&&(x[-1]||(x[-1]=new F(b,-1,null,!0)),x[-1].render(-1))),n&&e(m,function(d,g){w=void 0!==m[g+1]?m[g+1]+u:b.max-u;0===g%2&&d<b.max&&w<=b.max+(c.polar?-u:u)&&(l[d]||(l[d]=new a.PlotLineOrBand(b)),r=d+u,l[d].options={from:k?q(r):r,to:k?q(w):w,color:n},l[d].render(),l[d].isActive=!0)}),b._addedPlotLB||(e((d.plotLines||[]).concat(d.plotBands||[]),function(a){b.addPlotBandOrLine(a)}),b._addedPlotLB=!0);e([x,f,l],function(a){var b,d=[],g=I.duration;t(a,function(a,b){a.isActive||
(a.render(b,!1,0),a.isActive=!1,d.push(b))});A(function(){for(b=d.length;b--;)a[d[b]]&&!a[d[b]].isActive&&(a[d[b]].destroy(),delete a[d[b]])},a!==l&&c.hasRendered&&g?g:0)});G&&(G[G.isPlaced?"animate":"attr"]({d:this.getLinePath(G.strokeWidth())}),G.isPlaced=!0,G[p?"show":"hide"](!0));z&&p&&(d=b.getTitlePosition(),B(d.y)?(z[z.isNew?"attr":"animate"](d),z.isNew=!1):(z.attr("y",-9999),z.isNew=!0));C&&C.enabled&&b.renderStackTotals();b.isDirty=!1},redraw:function(){this.visible&&(this.render(),e(this.plotLinesAndBands,
function(a){a.render()}));e(this.series,function(a){a.isDirty=!0})},keepProps:"extKey hcEvents names series userMax userMin".split(" "),destroy:function(a){var b=this,c=b.stacks,d=b.plotLinesAndBands,g;a||N(b);t(c,function(a,b){u(a);c[b]=null});e([b.ticks,b.minorTicks,b.alternateBands],function(a){u(a)});if(d)for(a=d.length;a--;)d[a].destroy();e("stackTotalGroup axisLine axisTitle axisGroup gridGroup labelGroup cross".split(" "),function(a){b[a]&&(b[a]=b[a].destroy())});for(g in b.plotLinesAndBandsGroups)b.plotLinesAndBandsGroups[g]=
b.plotLinesAndBandsGroups[g].destroy();t(b,function(a,c){-1===k(c,b.keepProps)&&delete b[c]})},drawCrosshair:function(a,b){var c,d=this.crosshair,g=C(d.snap,!0),k,q=this.cross;a||(a=this.cross&&this.cross.e);this.crosshair&&!1!==(n(b)||!g)?(g?n(b)&&(k=this.isXAxis?b.plotX:this.len-b.plotY):k=a&&(this.horiz?a.chartX-this.pos:this.len-a.chartY+this.pos),n(k)&&(c=this.getPlotLinePath(b&&(this.isXAxis?b.x:C(b.stackY,b.y)),null,null,null,k)||null),n(c)?(b=this.categories&&!this.isRadial,q||(this.cross=
q=this.chart.renderer.path().addClass("highcharts-crosshair highcharts-crosshair-"+(b?"category ":"thin ")+d.className).attr({zIndex:C(d.zIndex,2)}).add(),q.attr({stroke:d.color||(b?f("#ccd6eb").setOpacity(.25).get():"#cccccc"),"stroke-width":C(d.width,1)}).css({"pointer-events":"none"}),d.dashStyle&&q.attr({dashstyle:d.dashStyle})),q.show().attr({d:c}),b&&!d.width&&q.attr({"stroke-width":this.transA}),this.cross.e=a):this.hideCrosshair()):this.hideCrosshair()},hideCrosshair:function(){this.cross&&
this.cross.hide()}});return a.Axis=G}(M);(function(a){var E=a.Axis,D=a.Date,H=a.dateFormat,p=a.defaultOptions,f=a.defined,l=a.each,r=a.extend,n=a.getMagnitude,w=a.getTZOffset,u=a.normalizeTickInterval,e=a.pick,h=a.timeUnits;E.prototype.getTimeTicks=function(a,d,c,b){var k=[],m={},B=p.global.useUTC,n,x=new D(d-Math.max(w(d),w(c))),u=D.hcMakeTime,t=a.unitRange,C=a.count,N,q;if(f(d)){x[D.hcSetMilliseconds](t>=h.second?0:C*Math.floor(x.getMilliseconds()/C));if(t>=h.second)x[D.hcSetSeconds](t>=h.minute?
0:C*Math.floor(x.getSeconds()/C));if(t>=h.minute)x[D.hcSetMinutes](t>=h.hour?0:C*Math.floor(x[D.hcGetMinutes]()/C));if(t>=h.hour)x[D.hcSetHours](t>=h.day?0:C*Math.floor(x[D.hcGetHours]()/C));if(t>=h.day)x[D.hcSetDate](t>=h.month?1:C*Math.floor(x[D.hcGetDate]()/C));t>=h.month&&(x[D.hcSetMonth](t>=h.year?0:C*Math.floor(x[D.hcGetMonth]()/C)),n=x[D.hcGetFullYear]());if(t>=h.year)x[D.hcSetFullYear](n-n%C);if(t===h.week)x[D.hcSetDate](x[D.hcGetDate]()-x[D.hcGetDay]()+e(b,1));n=x[D.hcGetFullYear]();b=x[D.hcGetMonth]();
var A=x[D.hcGetDate](),F=x[D.hcGetHours]();d=x.getTime();D.hcHasTimeZone&&(q=(!B||!!D.hcGetTimezoneOffset)&&(c-d>4*h.month||w(d)!==w(c)),N=w(x),x=new D(d+N));B=x.getTime();for(d=1;B<c;)k.push(B),B=t===h.year?u(n+d*C,0):t===h.month?u(n,b+d*C):!q||t!==h.day&&t!==h.week?q&&t===h.hour?u(n,b,A,F+d*C,0,0,N)-N:B+t*C:u(n,b,A+d*C*(t===h.day?1:7)),d++;k.push(B);t<=h.hour&&1E4>k.length&&l(k,function(a){0===a%18E5&&"000000000"===H("%H%M%S%L",a)&&(m[a]="day")})}k.info=r(a,{higherRanks:m,totalRange:t*C});return k};
E.prototype.normalizeTimeTickInterval=function(a,d){var c=d||[["millisecond",[1,2,5,10,20,25,50,100,200,500]],["second",[1,2,5,10,15,30]],["minute",[1,2,5,10,15,30]],["hour",[1,2,3,4,6,8,12]],["day",[1,2]],["week",[1,2]],["month",[1,2,3,4,6]],["year",null]];d=c[c.length-1];var b=h[d[0]],k=d[1],e;for(e=0;e<c.length&&!(d=c[e],b=h[d[0]],k=d[1],c[e+1]&&a<=(b*k[k.length-1]+h[c[e+1][0]])/2);e++);b===h.year&&a<5*b&&(k=[1,2,5]);a=u(a/b,k,"year"===d[0]?Math.max(n(a/b),1):1);return{unitRange:b,count:a,unitName:d[0]}}})(M);
(function(a){var E=a.Axis,D=a.getMagnitude,H=a.map,p=a.normalizeTickInterval,f=a.pick;E.prototype.getLogTickPositions=function(a,r,n,w){var l=this.options,e=this.len,h=this.lin2log,m=this.log2lin,d=[];w||(this._minorAutoInterval=null);if(.5<=a)a=Math.round(a),d=this.getLinearTickPositions(a,r,n);else if(.08<=a)for(var e=Math.floor(r),c,b,k,z,B,l=.3<a?[1,2,4]:.15<a?[1,2,4,6,8]:[1,2,3,4,5,6,7,8,9];e<n+1&&!B;e++)for(b=l.length,c=0;c<b&&!B;c++)k=m(h(e)*l[c]),k>r&&(!w||z<=n)&&void 0!==z&&d.push(z),z>n&&
(B=!0),z=k;else r=h(r),n=h(n),a=w?this.getMinorTickInterval():l.tickInterval,a=f("auto"===a?null:a,this._minorAutoInterval,l.tickPixelInterval/(w?5:1)*(n-r)/((w?e/this.tickPositions.length:e)||1)),a=p(a,null,D(a)),d=H(this.getLinearTickPositions(a,r,n),m),w||(this._minorAutoInterval=a/5);w||(this.tickInterval=a);return d};E.prototype.log2lin=function(a){return Math.log(a)/Math.LN10};E.prototype.lin2log=function(a){return Math.pow(10,a)}})(M);(function(a,E){var D=a.arrayMax,H=a.arrayMin,p=a.defined,
f=a.destroyObjectProperties,l=a.each,r=a.erase,n=a.merge,w=a.pick;a.PlotLineOrBand=function(a,e){this.axis=a;e&&(this.options=e,this.id=e.id)};a.PlotLineOrBand.prototype={render:function(){var f=this,e=f.axis,h=e.horiz,m=f.options,d=m.label,c=f.label,b=m.to,k=m.from,z=m.value,l=p(k)&&p(b),r=p(z),x=f.svgElem,K=!x,t=[],C=m.color,N=w(m.zIndex,0),q=m.events,t={"class":"highcharts-plot-"+(l?"band ":"line ")+(m.className||"")},A={},F=e.chart.renderer,G=l?"bands":"lines",g=e.log2lin;e.isLog&&(k=g(k),b=g(b),
z=g(z));r?(t={stroke:C,"stroke-width":m.width},m.dashStyle&&(t.dashstyle=m.dashStyle)):l&&(C&&(t.fill=C),m.borderWidth&&(t.stroke=m.borderColor,t["stroke-width"]=m.borderWidth));A.zIndex=N;G+="-"+N;(C=e.plotLinesAndBandsGroups[G])||(e.plotLinesAndBandsGroups[G]=C=F.g("plot-"+G).attr(A).add());K&&(f.svgElem=x=F.path().attr(t).add(C));if(r)t=e.getPlotLinePath(z,x.strokeWidth());else if(l)t=e.getPlotBandPath(k,b,m);else return;K&&t&&t.length?(x.attr({d:t}),q&&a.objectEach(q,function(a,b){x.on(b,function(a){q[b].apply(f,
[a])})})):x&&(t?(x.show(),x.animate({d:t})):(x.hide(),c&&(f.label=c=c.destroy())));d&&p(d.text)&&t&&t.length&&0<e.width&&0<e.height&&!t.flat?(d=n({align:h&&l&&"center",x:h?!l&&4:10,verticalAlign:!h&&l&&"middle",y:h?l?16:10:l?6:-4,rotation:h&&!l&&90},d),this.renderLabel(d,t,l,N)):c&&c.hide();return f},renderLabel:function(a,e,h,m){var d=this.label,c=this.axis.chart.renderer;d||(d={align:a.textAlign||a.align,rotation:a.rotation,"class":"highcharts-plot-"+(h?"band":"line")+"-label "+(a.className||"")},
d.zIndex=m,this.label=d=c.text(a.text,0,0,a.useHTML).attr(d).add(),d.css(a.style));m=e.xBounds||[e[1],e[4],h?e[6]:e[1]];e=e.yBounds||[e[2],e[5],h?e[7]:e[2]];h=H(m);c=H(e);d.align(a,!1,{x:h,y:c,width:D(m)-h,height:D(e)-c});d.show()},destroy:function(){r(this.axis.plotLinesAndBands,this);delete this.axis;f(this)}};a.extend(E.prototype,{getPlotBandPath:function(a,e){var h=this.getPlotLinePath(e,null,null,!0),m=this.getPlotLinePath(a,null,null,!0),d=[],c=this.horiz,b=1,k;a=a<this.min&&e<this.min||a>this.max&&
e>this.max;if(m&&h)for(a&&(k=m.toString()===h.toString(),b=0),a=0;a<m.length;a+=6)c&&h[a+1]===m[a+1]?(h[a+1]+=b,h[a+4]+=b):c||h[a+2]!==m[a+2]||(h[a+2]+=b,h[a+5]+=b),d.push("M",m[a+1],m[a+2],"L",m[a+4],m[a+5],h[a+4],h[a+5],h[a+1],h[a+2],"z"),d.flat=k;return d},addPlotBand:function(a){return this.addPlotBandOrLine(a,"plotBands")},addPlotLine:function(a){return this.addPlotBandOrLine(a,"plotLines")},addPlotBandOrLine:function(f,e){var h=(new a.PlotLineOrBand(this,f)).render(),m=this.userOptions;h&&(e&&
(m[e]=m[e]||[],m[e].push(f)),this.plotLinesAndBands.push(h));return h},removePlotBandOrLine:function(a){for(var e=this.plotLinesAndBands,h=this.options,m=this.userOptions,d=e.length;d--;)e[d].id===a&&e[d].destroy();l([h.plotLines||[],m.plotLines||[],h.plotBands||[],m.plotBands||[]],function(c){for(d=c.length;d--;)c[d].id===a&&r(c,c[d])})},removePlotBand:function(a){this.removePlotBandOrLine(a)},removePlotLine:function(a){this.removePlotBandOrLine(a)}})})(M,V);(function(a){var E=a.dateFormat,D=a.each,
H=a.extend,p=a.format,f=a.isNumber,l=a.map,r=a.merge,n=a.pick,w=a.splat,u=a.syncTimeout,e=a.timeUnits;a.Tooltip=function(){this.init.apply(this,arguments)};a.Tooltip.prototype={init:function(a,e){this.chart=a;this.options=e;this.crosshairs=[];this.now={x:0,y:0};this.isHidden=!0;this.split=e.split&&!a.inverted;this.shared=e.shared||this.split},cleanSplit:function(a){D(this.chart.series,function(e){var d=e&&e.tt;d&&(!d.isActive||a?e.tt=d.destroy():d.isActive=!1)})},getLabel:function(){var a=this.chart.renderer,
e=this.options;this.label||(this.split?this.label=a.g("tooltip"):(this.label=a.label("",0,0,e.shape||"callout",null,null,e.useHTML,null,"tooltip").attr({padding:e.padding,r:e.borderRadius}),this.label.attr({fill:e.backgroundColor,"stroke-width":e.borderWidth}).css(e.style).shadow(e.shadow)),this.label.attr({zIndex:8}).add());return this.label},update:function(a){this.destroy();r(!0,this.chart.options.tooltip.userOptions,a);this.init(this.chart,r(!0,this.options,a))},destroy:function(){this.label&&
(this.label=this.label.destroy());this.split&&this.tt&&(this.cleanSplit(this.chart,!0),this.tt=this.tt.destroy());clearTimeout(this.hideTimer);clearTimeout(this.tooltipTimeout)},move:function(a,e,d,c){var b=this,k=b.now,h=!1!==b.options.animation&&!b.isHidden&&(1<Math.abs(a-k.x)||1<Math.abs(e-k.y)),m=b.followPointer||1<b.len;H(k,{x:h?(2*k.x+a)/3:a,y:h?(k.y+e)/2:e,anchorX:m?void 0:h?(2*k.anchorX+d)/3:d,anchorY:m?void 0:h?(k.anchorY+c)/2:c});b.getLabel().attr(k);h&&(clearTimeout(this.tooltipTimeout),
this.tooltipTimeout=setTimeout(function(){b&&b.move(a,e,d,c)},32))},hide:function(a){var e=this;clearTimeout(this.hideTimer);a=n(a,this.options.hideDelay,500);this.isHidden||(this.hideTimer=u(function(){e.getLabel()[a?"fadeOut":"hide"]();e.isHidden=!0},a))},getAnchor:function(a,e){var d,c=this.chart,b=c.inverted,k=c.plotTop,h=c.plotLeft,m=0,f=0,x,n;a=w(a);d=a[0].tooltipPos;this.followPointer&&e&&(void 0===e.chartX&&(e=c.pointer.normalize(e)),d=[e.chartX-c.plotLeft,e.chartY-k]);d||(D(a,function(a){x=
a.series.yAxis;n=a.series.xAxis;m+=a.plotX+(!b&&n?n.left-h:0);f+=(a.plotLow?(a.plotLow+a.plotHigh)/2:a.plotY)+(!b&&x?x.top-k:0)}),m/=a.length,f/=a.length,d=[b?c.plotWidth-f:m,this.shared&&!b&&1<a.length&&e?e.chartY-k:b?c.plotHeight-m:f]);return l(d,Math.round)},getPosition:function(a,e,d){var c=this.chart,b=this.distance,k={},h=c.inverted&&d.h||0,m,f=["y",c.chartHeight,e,d.plotY+c.plotTop,c.plotTop,c.plotTop+c.plotHeight],x=["x",c.chartWidth,a,d.plotX+c.plotLeft,c.plotLeft,c.plotLeft+c.plotWidth],
l=!this.followPointer&&n(d.ttBelow,!c.inverted===!!d.negative),t=function(a,c,d,g,e,q){var m=d<g-b,z=g+b+d<c,A=g-b-d;g+=b;if(l&&z)k[a]=g;else if(!l&&m)k[a]=A;else if(m)k[a]=Math.min(q-d,0>A-h?A:A-h);else if(z)k[a]=Math.max(e,g+h+d>c?g:g+h);else return!1},C=function(a,c,d,g){var e;g<b||g>c-b?e=!1:k[a]=g<d/2?1:g>c-d/2?c-d-2:g-d/2;return e},p=function(a){var b=f;f=x;x=b;m=a},q=function(){!1!==t.apply(0,f)?!1!==C.apply(0,x)||m||(p(!0),q()):m?k.x=k.y=0:(p(!0),q())};(c.inverted||1<this.len)&&p();q();return k},
defaultFormatter:function(a){var e=this.points||w(this),d;d=[a.tooltipFooterHeaderFormatter(e[0])];d=d.concat(a.bodyFormatter(e));d.push(a.tooltipFooterHeaderFormatter(e[0],!0));return d},refresh:function(a,e){var d,c=this.options,b,k=a,h,m={},f=[];d=c.formatter||this.defaultFormatter;var m=this.shared,x;c.enabled&&(clearTimeout(this.hideTimer),this.followPointer=w(k)[0].series.tooltipOptions.followPointer,h=this.getAnchor(k,e),e=h[0],b=h[1],!m||k.series&&k.series.noSharedTooltip?m=k.getLabelConfig():
(D(k,function(a){a.setState("hover");f.push(a.getLabelConfig())}),m={x:k[0].category,y:k[0].y},m.points=f,k=k[0]),this.len=f.length,m=d.call(m,this),x=k.series,this.distance=n(x.tooltipOptions.distance,16),!1===m?this.hide():(d=this.getLabel(),this.isHidden&&d.attr({opacity:1}).show(),this.split?this.renderSplit(m,w(a)):(c.style.width||d.css({width:this.chart.spacingBox.width}),d.attr({text:m&&m.join?m.join(""):m}),d.removeClass(/highcharts-color-[\d]+/g).addClass("highcharts-color-"+n(k.colorIndex,
x.colorIndex)),d.attr({stroke:c.borderColor||k.color||x.color||"#666666"}),this.updatePosition({plotX:e,plotY:b,negative:k.negative,ttBelow:k.ttBelow,h:h[2]||0})),this.isHidden=!1))},renderSplit:function(e,m){var d=this,c=[],b=this.chart,k=b.renderer,h=!0,f=this.options,l=0,x=this.getLabel();a.isString(e)&&(e=[!1,e]);D(e.slice(0,m.length+1),function(a,e){if(!1!==a){e=m[e-1]||{isHeader:!0,plotX:m[0].plotX};var z=e.series||d,t=z.tt,q=e.series||{},A="highcharts-color-"+n(e.colorIndex,q.colorIndex,"none");
t||(z.tt=t=k.label(null,null,null,"callout",null,null,f.useHTML).addClass("highcharts-tooltip-box "+A).attr({padding:f.padding,r:f.borderRadius,fill:f.backgroundColor,stroke:f.borderColor||e.color||q.color||"#333333","stroke-width":f.borderWidth}).add(x));t.isActive=!0;t.attr({text:a});t.css(f.style).shadow(f.shadow);a=t.getBBox();q=a.width+t.strokeWidth();e.isHeader?(l=a.height,q=Math.max(0,Math.min(e.plotX+b.plotLeft-q/2,b.chartWidth-q))):q=e.plotX+b.plotLeft-n(f.distance,16)-q;0>q&&(h=!1);a=(e.series&&
e.series.yAxis&&e.series.yAxis.pos)+(e.plotY||0);a-=b.plotTop;c.push({target:e.isHeader?b.plotHeight+l:a,rank:e.isHeader?1:0,size:z.tt.getBBox().height+1,point:e,x:q,tt:t})}});this.cleanSplit();a.distribute(c,b.plotHeight+l);D(c,function(a){var c=a.point,d=c.series;a.tt.attr({visibility:void 0===a.pos?"hidden":"inherit",x:h||c.isHeader?a.x:c.plotX+b.plotLeft+n(f.distance,16),y:a.pos+b.plotTop,anchorX:c.isHeader?c.plotX+b.plotLeft:c.plotX+d.xAxis.pos,anchorY:c.isHeader?a.pos+b.plotTop-15:c.plotY+d.yAxis.pos})})},
updatePosition:function(a){var e=this.chart,d=this.getLabel(),d=(this.options.positioner||this.getPosition).call(this,d.width,d.height,a);this.move(Math.round(d.x),Math.round(d.y||0),a.plotX+e.plotLeft,a.plotY+e.plotTop)},getDateFormat:function(a,m,d,c){var b=E("%m-%d %H:%M:%S.%L",m),k,h,f={millisecond:15,second:12,minute:9,hour:6,day:3},l="millisecond";for(h in e){if(a===e.week&&+E("%w",m)===d&&"00:00:00.000"===b.substr(6)){h="week";break}if(e[h]>a){h=l;break}if(f[h]&&b.substr(f[h])!=="01-01 00:00:00.000".substr(f[h]))break;
"week"!==h&&(l=h)}h&&(k=c[h]);return k},getXDateFormat:function(a,e,d){e=e.dateTimeLabelFormats;var c=d&&d.closestPointRange;return(c?this.getDateFormat(c,a.x,d.options.startOfWeek,e):e.day)||e.year},tooltipFooterHeaderFormatter:function(a,e){e=e?"footer":"header";var d=a.series,c=d.tooltipOptions,b=c.xDateFormat,k=d.xAxis,h=k&&"datetime"===k.options.type&&f(a.key),m=c[e+"Format"];h&&!b&&(b=this.getXDateFormat(a,c,k));h&&b&&D(a.point&&a.point.tooltipDateKeys||["key"],function(a){m=m.replace("{point."+
a+"}","{point."+a+":"+b+"}")});return p(m,{point:a,series:d})},bodyFormatter:function(a){return l(a,function(a){var d=a.series.tooltipOptions;return(d[(a.point.formatPrefix||"point")+"Formatter"]||a.point.tooltipFormatter).call(a.point,d[(a.point.formatPrefix||"point")+"Format"])})}}})(M);(function(a){var E=a.addEvent,D=a.attr,H=a.charts,p=a.color,f=a.css,l=a.defined,r=a.each,n=a.extend,w=a.find,u=a.fireEvent,e=a.isObject,h=a.offset,m=a.pick,d=a.splat,c=a.Tooltip;a.Pointer=function(a,c){this.init(a,
c)};a.Pointer.prototype={init:function(a,d){this.options=d;this.chart=a;this.runChartClick=d.chart.events&&!!d.chart.events.click;this.pinchDown=[];this.lastValidTouch={};c&&(a.tooltip=new c(a,d.tooltip),this.followTouchMove=m(d.tooltip.followTouchMove,!0));this.setDOMEvents()},zoomOption:function(a){var b=this.chart,c=b.options.chart,d=c.zoomType||"",b=b.inverted;/touch/.test(a.type)&&(d=m(c.pinchType,d));this.zoomX=a=/x/.test(d);this.zoomY=d=/y/.test(d);this.zoomHor=a&&!b||d&&b;this.zoomVert=d&&
!b||a&&b;this.hasZoom=a||d},normalize:function(a,c){var b;b=a.touches?a.touches.length?a.touches.item(0):a.changedTouches[0]:a;c||(this.chartPosition=c=h(this.chart.container));return n(a,{chartX:Math.round(b.pageX-c.left),chartY:Math.round(b.pageY-c.top)})},getCoordinates:function(a){var b={xAxis:[],yAxis:[]};r(this.chart.axes,function(c){b[c.isXAxis?"xAxis":"yAxis"].push({axis:c,value:c.toValue(a[c.horiz?"chartX":"chartY"])})});return b},findNearestKDPoint:function(a,c,d){var b;r(a,function(a){var k=
!(a.noSharedTooltip&&c)&&0>a.options.findNearestPointBy.indexOf("y");a=a.searchPoint(d,k);if((k=e(a,!0))&&!(k=!e(b,!0)))var k=b.distX-a.distX,h=b.dist-a.dist,m=(a.series.group&&a.series.group.zIndex)-(b.series.group&&b.series.group.zIndex),k=0<(0!==k&&c?k:0!==h?h:0!==m?m:b.series.index>a.series.index?-1:1);k&&(b=a)});return b},getPointFromEvent:function(a){a=a.target;for(var b;a&&!b;)b=a.point,a=a.parentNode;return b},getChartCoordinatesFromPoint:function(a,c){var b=a.series,d=b.xAxis,b=b.yAxis,k=
m(a.clientX,a.plotX);if(d&&b)return c?{chartX:d.len+d.pos-k,chartY:b.len+b.pos-a.plotY}:{chartX:k+d.pos,chartY:a.plotY+b.pos}},getHoverData:function(b,c,d,h,f,l,n){var k,z=[],x=n&&n.isBoosting;h=!(!h||!b);n=c&&!c.stickyTracking?[c]:a.grep(d,function(a){return a.visible&&!(!f&&a.directTouch)&&m(a.options.enableMouseTracking,!0)&&a.stickyTracking});c=(k=h?b:this.findNearestKDPoint(n,f,l))&&k.series;k&&(f&&!c.noSharedTooltip?(n=a.grep(d,function(a){return a.visible&&!(!f&&a.directTouch)&&m(a.options.enableMouseTracking,
!0)&&!a.noSharedTooltip}),r(n,function(a){var b=w(a.points,function(a){return a.x===k.x&&!a.isNull});e(b)&&(x&&(b=a.getPoint(b)),z.push(b))})):z.push(k));return{hoverPoint:k,hoverSeries:c,hoverPoints:z}},runPointActions:function(b,c){var d=this.chart,k=d.tooltip&&d.tooltip.options.enabled?d.tooltip:void 0,e=k?k.shared:!1,h=c||d.hoverPoint,f=h&&h.series||d.hoverSeries,f=this.getHoverData(h,f,d.series,!!c||f&&f.directTouch&&this.isDirectTouch,e,b,{isBoosting:d.isBoosting}),l,h=f.hoverPoint;l=f.hoverPoints;
c=(f=f.hoverSeries)&&f.tooltipOptions.followPointer;e=e&&f&&!f.noSharedTooltip;if(h&&(h!==d.hoverPoint||k&&k.isHidden)){r(d.hoverPoints||[],function(b){-1===a.inArray(b,l)&&b.setState()});r(l||[],function(a){a.setState("hover")});if(d.hoverSeries!==f)f.onMouseOver();d.hoverPoint&&d.hoverPoint.firePointEvent("mouseOut");if(!h.series)return;h.firePointEvent("mouseOver");d.hoverPoints=l;d.hoverPoint=h;k&&k.refresh(e?l:h,b)}else c&&k&&!k.isHidden&&(h=k.getAnchor([{}],b),k.updatePosition({plotX:h[0],plotY:h[1]}));
this.unDocMouseMove||(this.unDocMouseMove=E(d.container.ownerDocument,"mousemove",function(b){var c=H[a.hoverChartIndex];if(c)c.pointer.onDocumentMouseMove(b)}));r(d.axes,function(c){var d=m(c.crosshair.snap,!0),k=d?a.find(l,function(a){return a.series[c.coll]===c}):void 0;k||!d?c.drawCrosshair(b,k):c.hideCrosshair()})},reset:function(a,c){var b=this.chart,k=b.hoverSeries,e=b.hoverPoint,h=b.hoverPoints,m=b.tooltip,f=m&&m.shared?h:e;a&&f&&r(d(f),function(b){b.series.isCartesian&&void 0===b.plotX&&
(a=!1)});if(a)m&&f&&(m.refresh(f),e&&(e.setState(e.state,!0),r(b.axes,function(a){a.crosshair&&a.drawCrosshair(null,e)})));else{if(e)e.onMouseOut();h&&r(h,function(a){a.setState()});if(k)k.onMouseOut();m&&m.hide(c);this.unDocMouseMove&&(this.unDocMouseMove=this.unDocMouseMove());r(b.axes,function(a){a.hideCrosshair()});this.hoverX=b.hoverPoints=b.hoverPoint=null}},scaleGroups:function(a,c){var b=this.chart,d;r(b.series,function(k){d=a||k.getPlotBox();k.xAxis&&k.xAxis.zoomEnabled&&k.group&&(k.group.attr(d),
k.markerGroup&&(k.markerGroup.attr(d),k.markerGroup.clip(c?b.clipRect:null)),k.dataLabelsGroup&&k.dataLabelsGroup.attr(d))});b.clipRect.attr(c||b.clipBox)},dragStart:function(a){var b=this.chart;b.mouseIsDown=a.type;b.cancelClick=!1;b.mouseDownX=this.mouseDownX=a.chartX;b.mouseDownY=this.mouseDownY=a.chartY},drag:function(a){var b=this.chart,c=b.options.chart,d=a.chartX,e=a.chartY,h=this.zoomHor,m=this.zoomVert,f=b.plotLeft,l=b.plotTop,n=b.plotWidth,q=b.plotHeight,A,F=this.selectionMarker,G=this.mouseDownX,
g=this.mouseDownY,v=c.panKey&&a[c.panKey+"Key"];F&&F.touch||(d<f?d=f:d>f+n&&(d=f+n),e<l?e=l:e>l+q&&(e=l+q),this.hasDragged=Math.sqrt(Math.pow(G-d,2)+Math.pow(g-e,2)),10<this.hasDragged&&(A=b.isInsidePlot(G-f,g-l),b.hasCartesianSeries&&(this.zoomX||this.zoomY)&&A&&!v&&!F&&(this.selectionMarker=F=b.renderer.rect(f,l,h?1:n,m?1:q,0).attr({fill:c.selectionMarkerFill||p("#335cad").setOpacity(.25).get(),"class":"highcharts-selection-marker",zIndex:7}).add()),F&&h&&(d-=G,F.attr({width:Math.abs(d),x:(0<d?
0:d)+G})),F&&m&&(d=e-g,F.attr({height:Math.abs(d),y:(0<d?0:d)+g})),A&&!F&&c.panning&&b.pan(a,c.panning)))},drop:function(a){var b=this,c=this.chart,d=this.hasPinched;if(this.selectionMarker){var e={originalEvent:a,xAxis:[],yAxis:[]},h=this.selectionMarker,m=h.attr?h.attr("x"):h.x,t=h.attr?h.attr("y"):h.y,p=h.attr?h.attr("width"):h.width,w=h.attr?h.attr("height"):h.height,q;if(this.hasDragged||d)r(c.axes,function(c){if(c.zoomEnabled&&l(c.min)&&(d||b[{xAxis:"zoomX",yAxis:"zoomY"}[c.coll]])){var k=c.horiz,
h="touchend"===a.type?c.minPixelPadding:0,g=c.toValue((k?m:t)+h),k=c.toValue((k?m+p:t+w)-h);e[c.coll].push({axis:c,min:Math.min(g,k),max:Math.max(g,k)});q=!0}}),q&&u(c,"selection",e,function(a){c.zoom(n(a,d?{animation:!1}:null))});this.selectionMarker=this.selectionMarker.destroy();d&&this.scaleGroups()}c&&(f(c.container,{cursor:c._cursor}),c.cancelClick=10<this.hasDragged,c.mouseIsDown=this.hasDragged=this.hasPinched=!1,this.pinchDown=[])},onContainerMouseDown:function(a){2!==a.button&&(a=this.normalize(a),
this.zoomOption(a),a.preventDefault&&a.preventDefault(),this.dragStart(a))},onDocumentMouseUp:function(b){H[a.hoverChartIndex]&&H[a.hoverChartIndex].pointer.drop(b)},onDocumentMouseMove:function(a){var b=this.chart,c=this.chartPosition;a=this.normalize(a,c);!c||this.inClass(a.target,"highcharts-tracker")||b.isInsidePlot(a.chartX-b.plotLeft,a.chartY-b.plotTop)||this.reset()},onContainerMouseLeave:function(b){var c=H[a.hoverChartIndex];c&&(b.relatedTarget||b.toElement)&&(c.pointer.reset(),c.pointer.chartPosition=
null)},onContainerMouseMove:function(b){var c=this.chart;l(a.hoverChartIndex)&&H[a.hoverChartIndex]&&H[a.hoverChartIndex].mouseIsDown||(a.hoverChartIndex=c.index);b=this.normalize(b);b.returnValue=!1;"mousedown"===c.mouseIsDown&&this.drag(b);!this.inClass(b.target,"highcharts-tracker")&&!c.isInsidePlot(b.chartX-c.plotLeft,b.chartY-c.plotTop)||c.openMenu||this.runPointActions(b)},inClass:function(a,c){for(var b;a;){if(b=D(a,"class")){if(-1!==b.indexOf(c))return!0;if(-1!==b.indexOf("highcharts-container"))return!1}a=
a.parentNode}},onTrackerMouseOut:function(a){var b=this.chart.hoverSeries;a=a.relatedTarget||a.toElement;this.isDirectTouch=!1;if(!(!b||!a||b.stickyTracking||this.inClass(a,"highcharts-tooltip")||this.inClass(a,"highcharts-series-"+b.index)&&this.inClass(a,"highcharts-tracker")))b.onMouseOut()},onContainerClick:function(a){var b=this.chart,c=b.hoverPoint,d=b.plotLeft,e=b.plotTop;a=this.normalize(a);b.cancelClick||(c&&this.inClass(a.target,"highcharts-tracker")?(u(c.series,"click",n(a,{point:c})),
b.hoverPoint&&c.firePointEvent("click",a)):(n(a,this.getCoordinates(a)),b.isInsidePlot(a.chartX-d,a.chartY-e)&&u(b,"click",a)))},setDOMEvents:function(){var b=this,c=b.chart.container,d=c.ownerDocument;c.onmousedown=function(a){b.onContainerMouseDown(a)};c.onmousemove=function(a){b.onContainerMouseMove(a)};c.onclick=function(a){b.onContainerClick(a)};this.unbindContainerMouseLeave=E(c,"mouseleave",b.onContainerMouseLeave);a.unbindDocumentMouseUp||(a.unbindDocumentMouseUp=E(d,"mouseup",b.onDocumentMouseUp));
a.hasTouch&&(c.ontouchstart=function(a){b.onContainerTouchStart(a)},c.ontouchmove=function(a){b.onContainerTouchMove(a)},a.unbindDocumentTouchEnd||(a.unbindDocumentTouchEnd=E(d,"touchend",b.onDocumentTouchEnd)))},destroy:function(){var b=this;b.unDocMouseMove&&b.unDocMouseMove();this.unbindContainerMouseLeave();a.chartCount||(a.unbindDocumentMouseUp&&(a.unbindDocumentMouseUp=a.unbindDocumentMouseUp()),a.unbindDocumentTouchEnd&&(a.unbindDocumentTouchEnd=a.unbindDocumentTouchEnd()));clearInterval(b.tooltipTimeout);
a.objectEach(b,function(a,c){b[c]=null})}}})(M);(function(a){var E=a.charts,D=a.each,H=a.extend,p=a.map,f=a.noop,l=a.pick;H(a.Pointer.prototype,{pinchTranslate:function(a,f,l,p,e,h){this.zoomHor&&this.pinchTranslateDirection(!0,a,f,l,p,e,h);this.zoomVert&&this.pinchTranslateDirection(!1,a,f,l,p,e,h)},pinchTranslateDirection:function(a,f,l,p,e,h,m,d){var c=this.chart,b=a?"x":"y",k=a?"X":"Y",z="chart"+k,n=a?"width":"height",r=c["plot"+(a?"Left":"Top")],x,w,t=d||1,C=c.inverted,u=c.bounds[a?"h":"v"],
q=1===f.length,A=f[0][z],F=l[0][z],G=!q&&f[1][z],g=!q&&l[1][z],v;l=function(){!q&&20<Math.abs(A-G)&&(t=d||Math.abs(F-g)/Math.abs(A-G));w=(r-F)/t+A;x=c["plot"+(a?"Width":"Height")]/t};l();f=w;f<u.min?(f=u.min,v=!0):f+x>u.max&&(f=u.max-x,v=!0);v?(F-=.8*(F-m[b][0]),q||(g-=.8*(g-m[b][1])),l()):m[b]=[F,g];C||(h[b]=w-r,h[n]=x);h=C?1/t:t;e[n]=x;e[b]=f;p[C?a?"scaleY":"scaleX":"scale"+k]=t;p["translate"+k]=h*r+(F-h*A)},pinch:function(a){var n=this,r=n.chart,u=n.pinchDown,e=a.touches,h=e.length,m=n.lastValidTouch,
d=n.hasZoom,c=n.selectionMarker,b={},k=1===h&&(n.inClass(a.target,"highcharts-tracker")&&r.runTrackerClick||n.runChartClick),z={};1<h&&(n.initiated=!0);d&&n.initiated&&!k&&a.preventDefault();p(e,function(a){return n.normalize(a)});"touchstart"===a.type?(D(e,function(a,b){u[b]={chartX:a.chartX,chartY:a.chartY}}),m.x=[u[0].chartX,u[1]&&u[1].chartX],m.y=[u[0].chartY,u[1]&&u[1].chartY],D(r.axes,function(a){if(a.zoomEnabled){var b=r.bounds[a.horiz?"h":"v"],c=a.minPixelPadding,d=a.toPixels(l(a.options.min,
a.dataMin)),e=a.toPixels(l(a.options.max,a.dataMax)),k=Math.max(d,e);b.min=Math.min(a.pos,Math.min(d,e)-c);b.max=Math.max(a.pos+a.len,k+c)}}),n.res=!0):n.followTouchMove&&1===h?this.runPointActions(n.normalize(a)):u.length&&(c||(n.selectionMarker=c=H({destroy:f,touch:!0},r.plotBox)),n.pinchTranslate(u,e,b,c,z,m),n.hasPinched=d,n.scaleGroups(b,z),n.res&&(n.res=!1,this.reset(!1,0)))},touch:function(f,n){var p=this.chart,r,e;if(p.index!==a.hoverChartIndex)this.onContainerMouseLeave({relatedTarget:!0});
a.hoverChartIndex=p.index;1===f.touches.length?(f=this.normalize(f),(e=p.isInsidePlot(f.chartX-p.plotLeft,f.chartY-p.plotTop))&&!p.openMenu?(n&&this.runPointActions(f),"touchmove"===f.type&&(n=this.pinchDown,r=n[0]?4<=Math.sqrt(Math.pow(n[0].chartX-f.chartX,2)+Math.pow(n[0].chartY-f.chartY,2)):!1),l(r,!0)&&this.pinch(f)):n&&this.reset()):2===f.touches.length&&this.pinch(f)},onContainerTouchStart:function(a){this.zoomOption(a);this.touch(a,!0)},onContainerTouchMove:function(a){this.touch(a)},onDocumentTouchEnd:function(f){E[a.hoverChartIndex]&&
E[a.hoverChartIndex].pointer.drop(f)}})})(M);(function(a){var E=a.addEvent,D=a.charts,H=a.css,p=a.doc,f=a.extend,l=a.noop,r=a.Pointer,n=a.removeEvent,w=a.win,u=a.wrap;if(!a.hasTouch&&(w.PointerEvent||w.MSPointerEvent)){var e={},h=!!w.PointerEvent,m=function(){var c=[];c.item=function(a){return this[a]};a.objectEach(e,function(a){c.push({pageX:a.pageX,pageY:a.pageY,target:a.target})});return c},d=function(c,b,d,e){"touch"!==c.pointerType&&c.pointerType!==c.MSPOINTER_TYPE_TOUCH||!D[a.hoverChartIndex]||
(e(c),e=D[a.hoverChartIndex].pointer,e[b]({type:d,target:c.currentTarget,preventDefault:l,touches:m()}))};f(r.prototype,{onContainerPointerDown:function(a){d(a,"onContainerTouchStart","touchstart",function(a){e[a.pointerId]={pageX:a.pageX,pageY:a.pageY,target:a.currentTarget}})},onContainerPointerMove:function(a){d(a,"onContainerTouchMove","touchmove",function(a){e[a.pointerId]={pageX:a.pageX,pageY:a.pageY};e[a.pointerId].target||(e[a.pointerId].target=a.currentTarget)})},onDocumentPointerUp:function(a){d(a,
"onDocumentTouchEnd","touchend",function(a){delete e[a.pointerId]})},batchMSEvents:function(a){a(this.chart.container,h?"pointerdown":"MSPointerDown",this.onContainerPointerDown);a(this.chart.container,h?"pointermove":"MSPointerMove",this.onContainerPointerMove);a(p,h?"pointerup":"MSPointerUp",this.onDocumentPointerUp)}});u(r.prototype,"init",function(a,b,d){a.call(this,b,d);this.hasZoom&&H(b.container,{"-ms-touch-action":"none","touch-action":"none"})});u(r.prototype,"setDOMEvents",function(a){a.apply(this);
(this.hasZoom||this.followTouchMove)&&this.batchMSEvents(E)});u(r.prototype,"destroy",function(a){this.batchMSEvents(n);a.call(this)})}})(M);(function(a){var E=a.addEvent,D=a.css,H=a.discardElement,p=a.defined,f=a.each,l=a.isFirefox,r=a.marginNames,n=a.merge,w=a.pick,u=a.setAnimation,e=a.stableSort,h=a.win,m=a.wrap;a.Legend=function(a,c){this.init(a,c)};a.Legend.prototype={init:function(a,c){this.chart=a;this.setOptions(c);c.enabled&&(this.render(),E(this.chart,"endResize",function(){this.legend.positionCheckboxes()}))},
setOptions:function(a){var c=w(a.padding,8);this.options=a;this.itemStyle=a.itemStyle;this.itemHiddenStyle=n(this.itemStyle,a.itemHiddenStyle);this.itemMarginTop=a.itemMarginTop||0;this.padding=c;this.initialItemY=c-5;this.itemHeight=this.maxItemWidth=0;this.symbolWidth=w(a.symbolWidth,16);this.pages=[]},update:function(a,c){var b=this.chart;this.setOptions(n(!0,this.options,a));this.destroy();b.isDirtyLegend=b.isDirtyBox=!0;w(c,!0)&&b.redraw()},colorizeItem:function(a,c){a.legendGroup[c?"removeClass":
"addClass"]("highcharts-legend-item-hidden");var b=this.options,d=a.legendItem,e=a.legendLine,h=a.legendSymbol,f=this.itemHiddenStyle.color,b=c?b.itemStyle.color:f,m=c?a.color||f:f,l=a.options&&a.options.marker,t={fill:m};d&&d.css({fill:b,color:b});e&&e.attr({stroke:m});h&&(l&&h.isMarker&&(t=a.pointAttribs(),c||(t.stroke=t.fill=f)),h.attr(t))},positionItem:function(a){var c=this.options,b=c.symbolPadding,c=!c.rtl,d=a._legendItemPos,e=d[0],d=d[1],h=a.checkbox;(a=a.legendGroup)&&a.element&&a.translate(c?
e:this.legendWidth-e-2*b-4,d);h&&(h.x=e,h.y=d)},destroyItem:function(a){var c=a.checkbox;f(["legendItem","legendLine","legendSymbol","legendGroup"],function(b){a[b]&&(a[b]=a[b].destroy())});c&&H(a.checkbox)},destroy:function(){function a(a){this[a]&&(this[a]=this[a].destroy())}f(this.getAllItems(),function(c){f(["legendItem","legendGroup"],a,c)});f("clipRect up down pager nav box title group".split(" "),a,this);this.display=null},positionCheckboxes:function(){var a=this.group&&this.group.alignAttr,
c,b=this.clipHeight||this.legendHeight,e=this.titleHeight;a&&(c=a.translateY,f(this.allItems,function(d){var k=d.checkbox,h;k&&(h=c+e+k.y+(this.scrollOffset||0)+3,D(k,{left:a.translateX+d.checkboxOffset+k.x-20+"px",top:h+"px",display:h>c-6&&h<c+b-6?"":"none"}))},this))},renderTitle:function(){var a=this.options,c=this.padding,b=a.title,e=0;b.text&&(this.title||(this.title=this.chart.renderer.label(b.text,c-3,c-4,null,null,null,a.useHTML,null,"legend-title").attr({zIndex:1}).css(b.style).add(this.group)),
a=this.title.getBBox(),e=a.height,this.offsetWidth=a.width,this.contentGroup.attr({translateY:e}));this.titleHeight=e},setText:function(d){var c=this.options;d.legendItem.attr({text:c.labelFormat?a.format(c.labelFormat,d):c.labelFormatter.call(d)})},renderItem:function(a){var c=this.chart,b=c.renderer,d=this.options,e="horizontal"===d.layout,h=this.symbolWidth,f=d.symbolPadding,m=this.itemStyle,l=this.itemHiddenStyle,t=this.padding,p=e?w(d.itemDistance,20):0,r=!d.rtl,q=d.width,A=d.itemMarginBottom||
0,F=this.itemMarginTop,G=a.legendItem,g=!a.series,v=!g&&a.series.drawLegendSymbol?a.series:a,u=v.options,L=this.createCheckboxForItem&&u&&u.showCheckbox,u=h+f+p+(L?20:0),P=d.useHTML,J=a.options.className;G||(a.legendGroup=b.g("legend-item").addClass("highcharts-"+v.type+"-series highcharts-color-"+a.colorIndex+(J?" "+J:"")+(g?" highcharts-series-"+a.index:"")).attr({zIndex:1}).add(this.scrollGroup),a.legendItem=G=b.text("",r?h+f:-f,this.baseline||0,P).css(n(a.visible?m:l)).attr({align:r?"left":"right",
zIndex:2}).add(a.legendGroup),this.baseline||(h=m.fontSize,this.fontMetrics=b.fontMetrics(h,G),this.baseline=this.fontMetrics.f+3+F,G.attr("y",this.baseline)),this.symbolHeight=d.symbolHeight||this.fontMetrics.f,v.drawLegendSymbol(this,a),this.setItemEvents&&this.setItemEvents(a,G,P),L&&this.createCheckboxForItem(a));this.colorizeItem(a,a.visible);m.width||G.css({width:(d.itemWidth||d.width||c.spacingBox.width)-u});this.setText(a);b=G.getBBox();m=a.checkboxOffset=d.itemWidth||a.legendItemWidth||b.width+
u;this.itemHeight=b=Math.round(a.legendItemHeight||b.height||this.symbolHeight);e&&this.itemX-t+m>(q||c.spacingBox.width-2*t-d.x)&&(this.itemX=t,this.itemY+=F+this.lastLineHeight+A,this.lastLineHeight=0);this.maxItemWidth=Math.max(this.maxItemWidth,m);this.lastItemY=F+this.itemY+A;this.lastLineHeight=Math.max(b,this.lastLineHeight);a._legendItemPos=[this.itemX,this.itemY];e?this.itemX+=m:(this.itemY+=F+b+A,this.lastLineHeight=b);this.offsetWidth=q||Math.max((e?this.itemX-t-(a.checkbox?0:p):m)+t,this.offsetWidth)},
getAllItems:function(){var a=[];f(this.chart.series,function(c){var b=c&&c.options;c&&w(b.showInLegend,p(b.linkedTo)?!1:void 0,!0)&&(a=a.concat(c.legendItems||("point"===b.legendType?c.data:c)))});return a},getAlignment:function(){var a=this.options;return a.floating?"":a.align.charAt(0)+a.verticalAlign.charAt(0)+a.layout.charAt(0)},adjustMargins:function(a,c){var b=this.chart,d=this.options,e=this.getAlignment();e&&f([/(lth|ct|rth)/,/(rtv|rm|rbv)/,/(rbh|cb|lbh)/,/(lbv|lm|ltv)/],function(k,h){k.test(e)&&
!p(a[h])&&(b[r[h]]=Math.max(b[r[h]],b.legend[(h+1)%2?"legendHeight":"legendWidth"]+[1,-1,-1,1][h]*d[h%2?"x":"y"]+w(d.margin,12)+c[h]+(0===h?b.titleOffset+b.options.title.margin:0)))})},render:function(){var a=this,c=a.chart,b=c.renderer,k=a.group,h,m,l,x,p=a.box,t=a.options,C=a.padding;a.itemX=C;a.itemY=a.initialItemY;a.offsetWidth=0;a.lastItemY=0;k||(a.group=k=b.g("legend").attr({zIndex:7}).add(),a.contentGroup=b.g().attr({zIndex:1}).add(k),a.scrollGroup=b.g().add(a.contentGroup));a.renderTitle();
h=a.getAllItems();e(h,function(a,b){return(a.options&&a.options.legendIndex||0)-(b.options&&b.options.legendIndex||0)});t.reversed&&h.reverse();a.allItems=h;a.display=m=!!h.length;a.lastLineHeight=0;f(h,function(b){a.renderItem(b)});l=(t.width||a.offsetWidth)+C;x=a.lastItemY+a.lastLineHeight+a.titleHeight;x=a.handleOverflow(x);x+=C;p||(a.box=p=b.rect().addClass("highcharts-legend-box").attr({r:t.borderRadius}).add(k),p.isNew=!0);p.attr({stroke:t.borderColor,"stroke-width":t.borderWidth||0,fill:t.backgroundColor||
"none"}).shadow(t.shadow);0<l&&0<x&&(p[p.isNew?"attr":"animate"](p.crisp.call({},{x:0,y:0,width:l,height:x},p.strokeWidth())),p.isNew=!1);p[m?"show":"hide"]();a.legendWidth=l;a.legendHeight=x;f(h,function(b){a.positionItem(b)});m&&(b=c.spacingBox,/(lth|ct|rth)/.test(a.getAlignment())&&(b=n(b,{y:b.y+c.titleOffset+c.options.title.margin})),k.align(n(t,{width:l,height:x}),!0,b));c.isResizing||this.positionCheckboxes()},handleOverflow:function(a){var c=this,b=this.chart,d=b.renderer,e=this.options,h=
e.y,m=this.padding,b=b.spacingBox.height+("top"===e.verticalAlign?-h:h)-m,h=e.maxHeight,l,n=this.clipRect,t=e.navigation,p=w(t.animation,!0),r=t.arrowSize||12,q=this.nav,A=this.pages,F,G=this.allItems,g=function(a){"number"===typeof a?n.attr({height:a}):n&&(c.clipRect=n.destroy(),c.contentGroup.clip());c.contentGroup.div&&(c.contentGroup.div.style.clip=a?"rect("+m+"px,9999px,"+(m+a)+"px,0)":"auto")};"horizontal"!==e.layout||"middle"===e.verticalAlign||e.floating||(b/=2);h&&(b=Math.min(b,h));A.length=
0;a>b&&!1!==t.enabled?(this.clipHeight=l=Math.max(b-20-this.titleHeight-m,0),this.currentPage=w(this.currentPage,1),this.fullHeight=a,f(G,function(a,b){var c=a._legendItemPos[1],d=Math.round(a.legendItem.getBBox().height),g=A.length;if(!g||c-A[g-1]>l&&(F||c)!==A[g-1])A.push(F||c),g++;a.pageIx=g-1;F&&(G[b-1].pageIx=g-1);b===G.length-1&&c+d-A[g-1]>l&&(A.push(c),a.pageIx=g);c!==F&&(F=c)}),n||(n=c.clipRect=d.clipRect(0,m,9999,0),c.contentGroup.clip(n)),g(l),q||(this.nav=q=d.g().attr({zIndex:1}).add(this.group),
this.up=d.symbol("triangle",0,0,r,r).on("click",function(){c.scroll(-1,p)}).add(q),this.pager=d.text("",15,10).addClass("highcharts-legend-navigation").css(t.style).add(q),this.down=d.symbol("triangle-down",0,0,r,r).on("click",function(){c.scroll(1,p)}).add(q)),c.scroll(0),a=b):q&&(g(),this.nav=q.destroy(),this.scrollGroup.attr({translateY:1}),this.clipHeight=0);return a},scroll:function(a,c){var b=this.pages,d=b.length;a=this.currentPage+a;var e=this.clipHeight,h=this.options.navigation,f=this.pager,
m=this.padding;a>d&&(a=d);0<a&&(void 0!==c&&u(c,this.chart),this.nav.attr({translateX:m,translateY:e+this.padding+7+this.titleHeight,visibility:"visible"}),this.up.attr({"class":1===a?"highcharts-legend-nav-inactive":"highcharts-legend-nav-active"}),f.attr({text:a+"/"+d}),this.down.attr({x:18+this.pager.getBBox().width,"class":a===d?"highcharts-legend-nav-inactive":"highcharts-legend-nav-active"}),this.up.attr({fill:1===a?h.inactiveColor:h.activeColor}).css({cursor:1===a?"default":"pointer"}),this.down.attr({fill:a===
d?h.inactiveColor:h.activeColor}).css({cursor:a===d?"default":"pointer"}),this.scrollOffset=-b[a-1]+this.initialItemY,this.scrollGroup.animate({translateY:this.scrollOffset}),this.currentPage=a,this.positionCheckboxes())}};a.LegendSymbolMixin={drawRectangle:function(a,c){var b=a.symbolHeight,d=a.options.squareSymbol;c.legendSymbol=this.chart.renderer.rect(d?(a.symbolWidth-b)/2:0,a.baseline-b+1,d?b:a.symbolWidth,b,w(a.options.symbolRadius,b/2)).addClass("highcharts-point").attr({zIndex:3}).add(c.legendGroup)},
drawLineMarker:function(a){var c=this.options,b=c.marker,d=a.symbolWidth,e=a.symbolHeight,h=e/2,f=this.chart.renderer,m=this.legendGroup;a=a.baseline-Math.round(.3*a.fontMetrics.b);var l;l={"stroke-width":c.lineWidth||0};c.dashStyle&&(l.dashstyle=c.dashStyle);this.legendLine=f.path(["M",0,a,"L",d,a]).addClass("highcharts-graph").attr(l).add(m);b&&!1!==b.enabled&&(c=Math.min(w(b.radius,h),h),0===this.symbol.indexOf("url")&&(b=n(b,{width:e,height:e}),c=0),this.legendSymbol=b=f.symbol(this.symbol,d/
2-c,a-c,2*c,2*c,b).addClass("highcharts-point").add(m),b.isMarker=!0)}};(/Trident\/7\.0/.test(h.navigator.userAgent)||l)&&m(a.Legend.prototype,"positionItem",function(a,c){var b=this,d=function(){c._legendItemPos&&a.call(b,c)};d();setTimeout(d)})})(M);(function(a){var E=a.addEvent,D=a.animate,H=a.animObject,p=a.attr,f=a.doc,l=a.Axis,r=a.createElement,n=a.defaultOptions,w=a.discardElement,u=a.charts,e=a.css,h=a.defined,m=a.each,d=a.extend,c=a.find,b=a.fireEvent,k=a.grep,z=a.isNumber,B=a.isObject,I=
a.isString,x=a.Legend,K=a.marginNames,t=a.merge,C=a.objectEach,N=a.Pointer,q=a.pick,A=a.pInt,F=a.removeEvent,G=a.seriesTypes,g=a.splat,v=a.svg,Q=a.syncTimeout,L=a.win,P=a.Chart=function(){this.getArgs.apply(this,arguments)};a.chart=function(a,b,c){return new P(a,b,c)};d(P.prototype,{callbacks:[],getArgs:function(){var a=[].slice.call(arguments);if(I(a[0])||a[0].nodeName)this.renderTo=a.shift();this.init(a[0],a[1])},init:function(b,c){var d,g,e=b.series,k=b.plotOptions||{};b.series=null;d=t(n,b);for(g in d.plotOptions)d.plotOptions[g].tooltip=
k[g]&&t(k[g].tooltip)||void 0;d.tooltip.userOptions=b.chart&&b.chart.forExport&&b.tooltip.userOptions||b.tooltip;d.series=b.series=e;this.userOptions=b;b=d.chart;g=b.events;this.margin=[];this.spacing=[];this.bounds={h:{},v:{}};this.labelCollectors=[];this.callback=c;this.isResizing=0;this.options=d;this.axes=[];this.series=[];this.hasCartesianSeries=b.showAxes;var q=this;q.index=u.length;u.push(q);a.chartCount++;g&&C(g,function(a,b){E(q,b,a)});q.xAxis=[];q.yAxis=[];q.pointCount=q.colorCounter=q.symbolCounter=
0;q.firstRender()},initSeries:function(b){var c=this.options.chart;(c=G[b.type||c.type||c.defaultSeriesType])||a.error(17,!0);c=new c;c.init(this,b);return c},orderSeries:function(a){var b=this.series;for(a=a||0;a<b.length;a++)b[a]&&(b[a].index=a,b[a].name=b[a].name||"Series "+(b[a].index+1))},isInsidePlot:function(a,b,c){var d=c?b:a;a=c?a:b;return 0<=d&&d<=this.plotWidth&&0<=a&&a<=this.plotHeight},redraw:function(c){var g=this.axes,e=this.series,k=this.pointer,q=this.legend,h=this.isDirtyLegend,
f,l,v=this.hasCartesianSeries,A=this.isDirtyBox,F,t=this.renderer,x=t.isHidden(),n=[];this.setResponsive&&this.setResponsive(!1);a.setAnimation(c,this);x&&this.temporaryDisplay();this.layOutTitles();for(c=e.length;c--;)if(F=e[c],F.options.stacking&&(f=!0,F.isDirty)){l=!0;break}if(l)for(c=e.length;c--;)F=e[c],F.options.stacking&&(F.isDirty=!0);m(e,function(a){a.isDirty&&"point"===a.options.legendType&&(a.updateTotals&&a.updateTotals(),h=!0);a.isDirtyData&&b(a,"updatedData")});h&&q.options.enabled&&
(q.render(),this.isDirtyLegend=!1);f&&this.getStacks();v&&m(g,function(a){a.updateNames();a.setScale()});this.getMargins();v&&(m(g,function(a){a.isDirty&&(A=!0)}),m(g,function(a){var c=a.min+","+a.max;a.extKey!==c&&(a.extKey=c,n.push(function(){b(a,"afterSetExtremes",d(a.eventArgs,a.getExtremes()));delete a.eventArgs}));(A||f)&&a.redraw()}));A&&this.drawChartBox();b(this,"predraw");m(e,function(a){(A||a.isDirty)&&a.visible&&a.redraw();a.isDirtyData=!1});k&&k.reset(!0);t.draw();b(this,"redraw");b(this,
"render");x&&this.temporaryDisplay(!0);m(n,function(a){a.call()})},get:function(a){function b(b){return b.id===a||b.options&&b.options.id===a}var d,g=this.series,e;d=c(this.axes,b)||c(this.series,b);for(e=0;!d&&e<g.length;e++)d=c(g[e].points||[],b);return d},getAxes:function(){var a=this,b=this.options,c=b.xAxis=g(b.xAxis||{}),b=b.yAxis=g(b.yAxis||{});m(c,function(a,b){a.index=b;a.isX=!0});m(b,function(a,b){a.index=b});c=c.concat(b);m(c,function(b){new l(a,b)})},getSelectedPoints:function(){var a=
[];m(this.series,function(b){a=a.concat(k(b.data||[],function(a){return a.selected}))});return a},getSelectedSeries:function(){return k(this.series,function(a){return a.selected})},setTitle:function(a,b,c){var d=this,g=d.options,e;e=g.title=t({style:{color:"#333333",fontSize:g.isStock?"16px":"18px"}},g.title,a);g=g.subtitle=t({style:{color:"#666666"}},g.subtitle,b);m([["title",a,e],["subtitle",b,g]],function(a,b){var c=a[0],g=d[c],e=a[1];a=a[2];g&&e&&(d[c]=g=g.destroy());a&&!g&&(d[c]=d.renderer.text(a.text,
0,0,a.useHTML).attr({align:a.align,"class":"highcharts-"+c,zIndex:a.zIndex||4}).add(),d[c].update=function(a){d.setTitle(!b&&a,b&&a)},d[c].css(a.style))});d.layOutTitles(c)},layOutTitles:function(a){var b=0,c,g=this.renderer,e=this.spacingBox;m(["title","subtitle"],function(a){var c=this[a],k=this.options[a];a="title"===a?-3:k.verticalAlign?0:b+2;var q;c&&(q=k.style.fontSize,q=g.fontMetrics(q,c).b,c.css({width:(k.width||e.width+k.widthAdjust)+"px"}).align(d({y:a+q},k),!1,"spacingBox"),k.floating||
k.verticalAlign||(b=Math.ceil(b+c.getBBox(k.useHTML).height)))},this);c=this.titleOffset!==b;this.titleOffset=b;!this.isDirtyBox&&c&&(this.isDirtyBox=c,this.hasRendered&&q(a,!0)&&this.isDirtyBox&&this.redraw())},getChartSize:function(){var b=this.options.chart,c=b.width,b=b.height,d=this.renderTo;h(c)||(this.containerWidth=a.getStyle(d,"width"));h(b)||(this.containerHeight=a.getStyle(d,"height"));this.chartWidth=Math.max(0,c||this.containerWidth||600);this.chartHeight=Math.max(0,a.relativeLength(b,
this.chartWidth)||(1<this.containerHeight?this.containerHeight:400))},temporaryDisplay:function(b){var c=this.renderTo;if(b)for(;c&&c.style;)c.hcOrigStyle&&(a.css(c,c.hcOrigStyle),delete c.hcOrigStyle),c.hcOrigDetached&&(f.body.removeChild(c),c.hcOrigDetached=!1),c=c.parentNode;else for(;c&&c.style;){f.body.contains(c)||c.parentNode||(c.hcOrigDetached=!0,f.body.appendChild(c));if("none"===a.getStyle(c,"display",!1)||c.hcOricDetached)c.hcOrigStyle={display:c.style.display,height:c.style.height,overflow:c.style.overflow},
b={display:"block",overflow:"hidden"},c!==this.renderTo&&(b.height=0),a.css(c,b),c.offsetWidth||c.style.setProperty("display","block","important");c=c.parentNode;if(c===f.body)break}},setClassName:function(a){this.container.className="highcharts-container "+(a||"")},getContainer:function(){var b,c=this.options,g=c.chart,e,k;b=this.renderTo;var q=a.uniqueKey(),h;b||(this.renderTo=b=g.renderTo);I(b)&&(this.renderTo=b=f.getElementById(b));b||a.error(13,!0);e=A(p(b,"data-highcharts-chart"));z(e)&&u[e]&&
u[e].hasRendered&&u[e].destroy();p(b,"data-highcharts-chart",this.index);b.innerHTML="";g.skipClone||b.offsetWidth||this.temporaryDisplay();this.getChartSize();e=this.chartWidth;k=this.chartHeight;h=d({position:"relative",overflow:"hidden",width:e+"px",height:k+"px",textAlign:"left",lineHeight:"normal",zIndex:0,"-webkit-tap-highlight-color":"rgba(0,0,0,0)"},g.style);this.container=b=r("div",{id:q},h,b);this._cursor=b.style.cursor;this.renderer=new (a[g.renderer]||a.Renderer)(b,e,k,null,g.forExport,
c.exporting&&c.exporting.allowHTML);this.setClassName(g.className);this.renderer.setStyle(g.style);this.renderer.chartIndex=this.index},getMargins:function(a){var b=this.spacing,c=this.margin,d=this.titleOffset;this.resetMargins();d&&!h(c[0])&&(this.plotTop=Math.max(this.plotTop,d+this.options.title.margin+b[0]));this.legend&&this.legend.display&&this.legend.adjustMargins(c,b);this.extraMargin&&(this[this.extraMargin.type]=(this[this.extraMargin.type]||0)+this.extraMargin.value);this.adjustPlotArea&&
this.adjustPlotArea();a||this.getAxisMargins()},getAxisMargins:function(){var a=this,b=a.axisOffset=[0,0,0,0],c=a.margin;a.hasCartesianSeries&&m(a.axes,function(a){a.visible&&a.getOffset()});m(K,function(d,g){h(c[g])||(a[d]+=b[g])});a.setChartSize()},reflow:function(b){var c=this,d=c.options.chart,g=c.renderTo,e=h(d.width)&&h(d.height),k=d.width||a.getStyle(g,"width"),d=d.height||a.getStyle(g,"height"),g=b?b.target:L;if(!e&&!c.isPrinting&&k&&d&&(g===L||g===f)){if(k!==c.containerWidth||d!==c.containerHeight)clearTimeout(c.reflowTimeout),
c.reflowTimeout=Q(function(){c.container&&c.setSize(void 0,void 0,!1)},b?100:0);c.containerWidth=k;c.containerHeight=d}},initReflow:function(){var a=this,b;b=E(L,"resize",function(b){a.reflow(b)});E(a,"destroy",b)},setSize:function(c,d,g){var k=this,q=k.renderer;k.isResizing+=1;a.setAnimation(g,k);k.oldChartHeight=k.chartHeight;k.oldChartWidth=k.chartWidth;void 0!==c&&(k.options.chart.width=c);void 0!==d&&(k.options.chart.height=d);k.getChartSize();c=q.globalAnimation;(c?D:e)(k.container,{width:k.chartWidth+
"px",height:k.chartHeight+"px"},c);k.setChartSize(!0);q.setSize(k.chartWidth,k.chartHeight,g);m(k.axes,function(a){a.isDirty=!0;a.setScale()});k.isDirtyLegend=!0;k.isDirtyBox=!0;k.layOutTitles();k.getMargins();k.redraw(g);k.oldChartHeight=null;b(k,"resize");Q(function(){k&&b(k,"endResize",null,function(){--k.isResizing})},H(c).duration)},setChartSize:function(a){var b=this.inverted,c=this.renderer,d=this.chartWidth,g=this.chartHeight,e=this.options.chart,k=this.spacing,q=this.clipOffset,h,f,l,v;this.plotLeft=
h=Math.round(this.plotLeft);this.plotTop=f=Math.round(this.plotTop);this.plotWidth=l=Math.max(0,Math.round(d-h-this.marginRight));this.plotHeight=v=Math.max(0,Math.round(g-f-this.marginBottom));this.plotSizeX=b?v:l;this.plotSizeY=b?l:v;this.plotBorderWidth=e.plotBorderWidth||0;this.spacingBox=c.spacingBox={x:k[3],y:k[0],width:d-k[3]-k[1],height:g-k[0]-k[2]};this.plotBox=c.plotBox={x:h,y:f,width:l,height:v};d=2*Math.floor(this.plotBorderWidth/2);b=Math.ceil(Math.max(d,q[3])/2);c=Math.ceil(Math.max(d,
q[0])/2);this.clipBox={x:b,y:c,width:Math.floor(this.plotSizeX-Math.max(d,q[1])/2-b),height:Math.max(0,Math.floor(this.plotSizeY-Math.max(d,q[2])/2-c))};a||m(this.axes,function(a){a.setAxisSize();a.setAxisTranslation()})},resetMargins:function(){var a=this,b=a.options.chart;m(["margin","spacing"],function(c){var d=b[c],g=B(d)?d:[d,d,d,d];m(["Top","Right","Bottom","Left"],function(d,e){a[c][e]=q(b[c+d],g[e])})});m(K,function(b,c){a[b]=q(a.margin[c],a.spacing[c])});a.axisOffset=[0,0,0,0];a.clipOffset=
[0,0,0,0]},drawChartBox:function(){var a=this.options.chart,b=this.renderer,c=this.chartWidth,d=this.chartHeight,g=this.chartBackground,e=this.plotBackground,k=this.plotBorder,q,h=this.plotBGImage,f=a.backgroundColor,m=a.plotBackgroundColor,l=a.plotBackgroundImage,v,A=this.plotLeft,F=this.plotTop,t=this.plotWidth,x=this.plotHeight,n=this.plotBox,p=this.clipRect,z=this.clipBox,G="animate";g||(this.chartBackground=g=b.rect().addClass("highcharts-background").add(),G="attr");q=a.borderWidth||0;v=q+(a.shadow?
8:0);f={fill:f||"none"};if(q||g["stroke-width"])f.stroke=a.borderColor,f["stroke-width"]=q;g.attr(f).shadow(a.shadow);g[G]({x:v/2,y:v/2,width:c-v-q%2,height:d-v-q%2,r:a.borderRadius});G="animate";e||(G="attr",this.plotBackground=e=b.rect().addClass("highcharts-plot-background").add());e[G](n);e.attr({fill:m||"none"}).shadow(a.plotShadow);l&&(h?h.animate(n):this.plotBGImage=b.image(l,A,F,t,x).add());p?p.animate({width:z.width,height:z.height}):this.clipRect=b.clipRect(z);G="animate";k||(G="attr",this.plotBorder=
k=b.rect().addClass("highcharts-plot-border").attr({zIndex:1}).add());k.attr({stroke:a.plotBorderColor,"stroke-width":a.plotBorderWidth||0,fill:"none"});k[G](k.crisp({x:A,y:F,width:t,height:x},-k.strokeWidth()));this.isDirtyBox=!1},propFromSeries:function(){var a=this,b=a.options.chart,c,d=a.options.series,g,e;m(["inverted","angular","polar"],function(k){c=G[b.type||b.defaultSeriesType];e=b[k]||c&&c.prototype[k];for(g=d&&d.length;!e&&g--;)(c=G[d[g].type])&&c.prototype[k]&&(e=!0);a[k]=e})},linkSeries:function(){var a=
this,b=a.series;m(b,function(a){a.linkedSeries.length=0});m(b,function(b){var c=b.options.linkedTo;I(c)&&(c=":previous"===c?a.series[b.index-1]:a.get(c))&&c.linkedParent!==b&&(c.linkedSeries.push(b),b.linkedParent=c,b.visible=q(b.options.visible,c.options.visible,b.visible))})},renderSeries:function(){m(this.series,function(a){a.translate();a.render()})},renderLabels:function(){var a=this,b=a.options.labels;b.items&&m(b.items,function(c){var g=d(b.style,c.style),e=A(g.left)+a.plotLeft,k=A(g.top)+
a.plotTop+12;delete g.left;delete g.top;a.renderer.text(c.html,e,k).attr({zIndex:2}).css(g).add()})},render:function(){var a=this.axes,b=this.renderer,c=this.options,d,g,e;this.setTitle();this.legend=new x(this,c.legend);this.getStacks&&this.getStacks();this.getMargins(!0);this.setChartSize();c=this.plotWidth;d=this.plotHeight=Math.max(this.plotHeight-21,0);m(a,function(a){a.setScale()});this.getAxisMargins();g=1.1<c/this.plotWidth;e=1.05<d/this.plotHeight;if(g||e)m(a,function(a){(a.horiz&&g||!a.horiz&&
e)&&a.setTickInterval(!0)}),this.getMargins();this.drawChartBox();this.hasCartesianSeries&&m(a,function(a){a.visible&&a.render()});this.seriesGroup||(this.seriesGroup=b.g("series-group").attr({zIndex:3}).add());this.renderSeries();this.renderLabels();this.addCredits();this.setResponsive&&this.setResponsive();this.hasRendered=!0},addCredits:function(a){var b=this;a=t(!0,this.options.credits,a);a.enabled&&!this.credits&&(this.credits=this.renderer.text(a.text+(this.mapCredits||""),0,0).addClass("highcharts-credits").on("click",
function(){a.href&&(L.location.href=a.href)}).attr({align:a.position.align,zIndex:8}).css(a.style).add().align(a.position),this.credits.update=function(a){b.credits=b.credits.destroy();b.addCredits(a)})},destroy:function(){var c=this,d=c.axes,g=c.series,e=c.container,k,q=e&&e.parentNode;b(c,"destroy");c.renderer.forExport?a.erase(u,c):u[c.index]=void 0;a.chartCount--;c.renderTo.removeAttribute("data-highcharts-chart");F(c);for(k=d.length;k--;)d[k]=d[k].destroy();this.scroller&&this.scroller.destroy&&
this.scroller.destroy();for(k=g.length;k--;)g[k]=g[k].destroy();m("title subtitle chartBackground plotBackground plotBGImage plotBorder seriesGroup clipRect credits pointer rangeSelector legend resetZoomButton tooltip renderer".split(" "),function(a){var b=c[a];b&&b.destroy&&(c[a]=b.destroy())});e&&(e.innerHTML="",F(e),q&&w(e));C(c,function(a,b){delete c[b]})},isReadyToRender:function(){var a=this;return v||L!=L.top||"complete"===f.readyState?!0:(f.attachEvent("onreadystatechange",function(){f.detachEvent("onreadystatechange",
a.firstRender);"complete"===f.readyState&&a.firstRender()}),!1)},firstRender:function(){var a=this,c=a.options;if(a.isReadyToRender()){a.getContainer();b(a,"init");a.resetMargins();a.setChartSize();a.propFromSeries();a.getAxes();m(c.series||[],function(b){a.initSeries(b)});a.linkSeries();b(a,"beforeRender");N&&(a.pointer=new N(a,c));a.render();if(!a.renderer.imgCount&&a.onload)a.onload();a.temporaryDisplay(!0)}},onload:function(){m([this.callback].concat(this.callbacks),function(a){a&&void 0!==this.index&&
a.apply(this,[this])},this);b(this,"load");b(this,"render");h(this.index)&&!1!==this.options.chart.reflow&&this.initReflow();this.onload=null}})})(M);(function(a){var E,D=a.each,H=a.extend,p=a.erase,f=a.fireEvent,l=a.format,r=a.isArray,n=a.isNumber,w=a.pick,u=a.removeEvent;a.Point=E=function(){};a.Point.prototype={init:function(a,h,f){this.series=a;this.color=a.color;this.applyOptions(h,f);a.options.colorByPoint?(h=a.options.colors||a.chart.options.colors,this.color=this.color||h[a.colorCounter],
h=h.length,f=a.colorCounter,a.colorCounter++,a.colorCounter===h&&(a.colorCounter=0)):f=a.colorIndex;this.colorIndex=w(this.colorIndex,f);a.chart.pointCount++;return this},applyOptions:function(a,h){var e=this.series,d=e.options.pointValKey||e.pointValKey;a=E.prototype.optionsToObject.call(this,a);H(this,a);this.options=this.options?H(this.options,a):a;a.group&&delete this.group;d&&(this.y=this[d]);this.isNull=w(this.isValid&&!this.isValid(),null===this.x||!n(this.y,!0));this.selected&&(this.state=
"select");"name"in this&&void 0===h&&e.xAxis&&e.xAxis.hasNames&&(this.x=e.xAxis.nameToX(this));void 0===this.x&&e&&(this.x=void 0===h?e.autoIncrement(this):h);return this},optionsToObject:function(a){var e={},f=this.series,d=f.options.keys,c=d||f.pointArrayMap||["y"],b=c.length,k=0,l=0;if(n(a)||null===a)e[c[0]]=a;else if(r(a))for(!d&&a.length>b&&(f=typeof a[0],"string"===f?e.name=a[0]:"number"===f&&(e.x=a[0]),k++);l<b;)d&&void 0===a[k]||(e[c[l]]=a[k]),k++,l++;else"object"===typeof a&&(e=a,a.dataLabels&&
(f._hasPointLabels=!0),a.marker&&(f._hasPointMarkers=!0));return e},getClassName:function(){return"highcharts-point"+(this.selected?" highcharts-point-select":"")+(this.negative?" highcharts-negative":"")+(this.isNull?" highcharts-null-point":"")+(void 0!==this.colorIndex?" highcharts-color-"+this.colorIndex:"")+(this.options.className?" "+this.options.className:"")+(this.zone&&this.zone.className?" "+this.zone.className.replace("highcharts-negative",""):"")},getZone:function(){var a=this.series,
h=a.zones,a=a.zoneAxis||"y",f=0,d;for(d=h[f];this[a]>=d.value;)d=h[++f];d&&d.color&&!this.options.color&&(this.color=d.color);return d},destroy:function(){var a=this.series.chart,h=a.hoverPoints,f;a.pointCount--;h&&(this.setState(),p(h,this),h.length||(a.hoverPoints=null));if(this===a.hoverPoint)this.onMouseOut();if(this.graphic||this.dataLabel)u(this),this.destroyElements();this.legendItem&&a.legend.destroyItem(this);for(f in this)this[f]=null},destroyElements:function(){for(var a=["graphic","dataLabel",
"dataLabelUpper","connector","shadowGroup"],h,f=6;f--;)h=a[f],this[h]&&(this[h]=this[h].destroy())},getLabelConfig:function(){return{x:this.category,y:this.y,color:this.color,colorIndex:this.colorIndex,key:this.name||this.category,series:this.series,point:this,percentage:this.percentage,total:this.total||this.stackTotal}},tooltipFormatter:function(a){var e=this.series,f=e.tooltipOptions,d=w(f.valueDecimals,""),c=f.valuePrefix||"",b=f.valueSuffix||"";D(e.pointArrayMap||["y"],function(e){e="{point."+
e;if(c||b)a=a.replace(e+"}",c+e+"}"+b);a=a.replace(e+"}",e+":,."+d+"f}")});return l(a,{point:this,series:this.series})},firePointEvent:function(a,h,m){var d=this,c=this.series.options;(c.point.events[a]||d.options&&d.options.events&&d.options.events[a])&&this.importEvents();"click"===a&&c.allowPointSelect&&(m=function(a){d.select&&d.select(null,a.ctrlKey||a.metaKey||a.shiftKey)});f(this,a,h,m)},visible:!0}})(M);(function(a){var E=a.addEvent,D=a.animObject,H=a.arrayMax,p=a.arrayMin,f=a.correctFloat,
l=a.Date,r=a.defaultOptions,n=a.defaultPlotOptions,w=a.defined,u=a.each,e=a.erase,h=a.extend,m=a.fireEvent,d=a.grep,c=a.isArray,b=a.isNumber,k=a.isString,z=a.merge,B=a.objectEach,I=a.pick,x=a.removeEvent,K=a.splat,t=a.SVGElement,C=a.syncTimeout,N=a.win;a.Series=a.seriesType("line",null,{lineWidth:2,allowPointSelect:!1,showCheckbox:!1,animation:{duration:1E3},events:{},marker:{lineWidth:0,lineColor:"#ffffff",radius:4,states:{hover:{animation:{duration:50},enabled:!0,radiusPlus:2,lineWidthPlus:1},select:{fillColor:"#cccccc",
lineColor:"#000000",lineWidth:2}}},point:{events:{}},dataLabels:{align:"center",formatter:function(){return null===this.y?"":a.numberFormat(this.y,-1)},style:{fontSize:"11px",fontWeight:"bold",color:"contrast",textOutline:"1px contrast"},verticalAlign:"bottom",x:0,y:0,padding:5},cropThreshold:300,pointRange:0,softThreshold:!0,states:{hover:{animation:{duration:50},lineWidthPlus:1,marker:{},halo:{size:10,opacity:.25}},select:{marker:{}}},stickyTracking:!0,turboThreshold:1E3,findNearestPointBy:"x"},
{isCartesian:!0,pointClass:a.Point,sorted:!0,requireSorting:!0,directTouch:!1,axisTypes:["xAxis","yAxis"],colorCounter:0,parallelArrays:["x","y"],coll:"series",init:function(a,b){var c=this,d,g=a.series,e;c.chart=a;c.options=b=c.setOptions(b);c.linkedSeries=[];c.bindAxes();h(c,{name:b.name,state:"",visible:!1!==b.visible,selected:!0===b.selected});d=b.events;B(d,function(a,b){E(c,b,a)});if(d&&d.click||b.point&&b.point.events&&b.point.events.click||b.allowPointSelect)a.runTrackerClick=!0;c.getColor();
c.getSymbol();u(c.parallelArrays,function(a){c[a+"Data"]=[]});c.setData(b.data,!1);c.isCartesian&&(a.hasCartesianSeries=!0);g.length&&(e=g[g.length-1]);c._i=I(e&&e._i,-1)+1;a.orderSeries(this.insert(g))},insert:function(a){var c=this.options.index,d;if(b(c)){for(d=a.length;d--;)if(c>=I(a[d].options.index,a[d]._i)){a.splice(d+1,0,this);break}-1===d&&a.unshift(this);d+=1}else a.push(this);return I(d,a.length-1)},bindAxes:function(){var b=this,c=b.options,d=b.chart,e;u(b.axisTypes||[],function(g){u(d[g],
function(a){e=a.options;if(c[g]===e.index||void 0!==c[g]&&c[g]===e.id||void 0===c[g]&&0===e.index)b.insert(a.series),b[g]=a,a.isDirty=!0});b[g]||b.optionalAxis===g||a.error(18,!0)})},updateParallelArrays:function(a,c){var d=a.series,e=arguments,g=b(c)?function(b){var g="y"===b&&d.toYData?d.toYData(a):a[b];d[b+"Data"][c]=g}:function(a){Array.prototype[c].apply(d[a+"Data"],Array.prototype.slice.call(e,2))};u(d.parallelArrays,g)},autoIncrement:function(){var b=this.options,c=this.xIncrement,d,e=b.pointIntervalUnit,
g=0,c=I(c,b.pointStart,0);this.pointInterval=d=I(this.pointInterval,b.pointInterval,1);e&&(b=new l(c),"day"===e?b=+b[l.hcSetDate](b[l.hcGetDate]()+d):"month"===e?b=+b[l.hcSetMonth](b[l.hcGetMonth]()+d):"year"===e&&(b=+b[l.hcSetFullYear](b[l.hcGetFullYear]()+d)),l.hcHasTimeZone&&(g=a.getTZOffset(b)-a.getTZOffset(c)),d=b-c+g);this.xIncrement=c+d;return c},setOptions:function(a){var b=this.chart,c=b.options,d=c.plotOptions,g=(b.userOptions||{}).plotOptions||{},e=d[this.type];this.userOptions=a;b=z(e,
d.series,a);this.tooltipOptions=z(r.tooltip,r.plotOptions.series&&r.plotOptions.series.tooltip,r.plotOptions[this.type].tooltip,c.tooltip.userOptions,d.series&&d.series.tooltip,d[this.type].tooltip,a.tooltip);this.stickyTracking=I(a.stickyTracking,g[this.type]&&g[this.type].stickyTracking,g.series&&g.series.stickyTracking,this.tooltipOptions.shared&&!this.noSharedTooltip?!0:b.stickyTracking);null===e.marker&&delete b.marker;this.zoneAxis=b.zoneAxis;a=this.zones=(b.zones||[]).slice();!b.negativeColor&&
!b.negativeFillColor||b.zones||a.push({value:b[this.zoneAxis+"Threshold"]||b.threshold||0,className:"highcharts-negative",color:b.negativeColor,fillColor:b.negativeFillColor});a.length&&w(a[a.length-1].value)&&a.push({color:this.color,fillColor:this.fillColor});return b},getCyclic:function(a,b,c){var d,g=this.chart,e=this.userOptions,k=a+"Index",h=a+"Counter",q=c?c.length:I(g.options.chart[a+"Count"],g[a+"Count"]);b||(d=I(e[k],e["_"+k]),w(d)||(g.series.length||(g[h]=0),e["_"+k]=d=g[h]%q,g[h]+=1),
c&&(b=c[d]));void 0!==d&&(this[k]=d);this[a]=b},getColor:function(){this.options.colorByPoint?this.options.color=null:this.getCyclic("color",this.options.color||n[this.type].color,this.chart.options.colors)},getSymbol:function(){this.getCyclic("symbol",this.options.marker.symbol,this.chart.options.symbols)},drawLegendSymbol:a.LegendSymbolMixin.drawLineMarker,setData:function(d,e,h,f){var g=this,q=g.points,m=q&&q.length||0,l,A=g.options,t=g.chart,x=null,n=g.xAxis,p=A.turboThreshold,z=this.xData,F=
this.yData,C=(l=g.pointArrayMap)&&l.length;d=d||[];l=d.length;e=I(e,!0);if(!1!==f&&l&&m===l&&!g.cropped&&!g.hasGroupedData&&g.visible)u(d,function(a,b){q[b].update&&a!==A.data[b]&&q[b].update(a,!1,null,!1)});else{g.xIncrement=null;g.colorCounter=0;u(this.parallelArrays,function(a){g[a+"Data"].length=0});if(p&&l>p){for(h=0;null===x&&h<l;)x=d[h],h++;if(b(x))for(h=0;h<l;h++)z[h]=this.autoIncrement(),F[h]=d[h];else if(c(x))if(C)for(h=0;h<l;h++)x=d[h],z[h]=x[0],F[h]=x.slice(1,C+1);else for(h=0;h<l;h++)x=
d[h],z[h]=x[0],F[h]=x[1];else a.error(12)}else for(h=0;h<l;h++)void 0!==d[h]&&(x={series:g},g.pointClass.prototype.applyOptions.apply(x,[d[h]]),g.updateParallelArrays(x,h));F&&k(F[0])&&a.error(14,!0);g.data=[];g.options.data=g.userOptions.data=d;for(h=m;h--;)q[h]&&q[h].destroy&&q[h].destroy();n&&(n.minRange=n.userMinRange);g.isDirty=t.isDirtyBox=!0;g.isDirtyData=!!q;h=!1}"point"===A.legendType&&(this.processData(),this.generatePoints());e&&t.redraw(h)},processData:function(b){var c=this.xData,d=this.yData,
e=c.length,g;g=0;var k,h,q=this.xAxis,f,m=this.options;f=m.cropThreshold;var l=this.getExtremesFromAll||m.getExtremesFromAll,t=this.isCartesian,m=q&&q.val2lin,x=q&&q.isLog,n=this.requireSorting,p,z;if(t&&!this.isDirty&&!q.isDirty&&!this.yAxis.isDirty&&!b)return!1;q&&(b=q.getExtremes(),p=b.min,z=b.max);if(t&&this.sorted&&!l&&(!f||e>f||this.forceCrop))if(c[e-1]<p||c[0]>z)c=[],d=[];else if(c[0]<p||c[e-1]>z)g=this.cropData(this.xData,this.yData,p,z),c=g.xData,d=g.yData,g=g.start,k=!0;for(f=c.length||
1;--f;)e=x?m(c[f])-m(c[f-1]):c[f]-c[f-1],0<e&&(void 0===h||e<h)?h=e:0>e&&n&&(a.error(15),n=!1);this.cropped=k;this.cropStart=g;this.processedXData=c;this.processedYData=d;this.closestPointRange=h},cropData:function(a,b,c,d){var g=a.length,e=0,k=g,h=I(this.cropShoulder,1),f;for(f=0;f<g;f++)if(a[f]>=c){e=Math.max(0,f-h);break}for(c=f;c<g;c++)if(a[c]>d){k=c+h;break}return{xData:a.slice(e,k),yData:b.slice(e,k),start:e,end:k}},generatePoints:function(){var a=this.options,b=a.data,c=this.data,d,g=this.processedXData,
e=this.processedYData,k=this.pointClass,h=g.length,f=this.cropStart||0,m,l=this.hasGroupedData,a=a.keys,t,x=[],n;c||l||(c=[],c.length=b.length,c=this.data=c);a&&l&&(this.options.keys=!1);for(n=0;n<h;n++)m=f+n,l?(t=(new k).init(this,[g[n]].concat(K(e[n]))),t.dataGroup=this.groupMap[n]):(t=c[m])||void 0===b[m]||(c[m]=t=(new k).init(this,b[m],g[n])),t&&(t.index=m,x[n]=t);this.options.keys=a;if(c&&(h!==(d=c.length)||l))for(n=0;n<d;n++)n!==f||l||(n+=h),c[n]&&(c[n].destroyElements(),c[n].plotX=void 0);
this.data=c;this.points=x},getExtremes:function(a){var d=this.yAxis,e=this.processedXData,k,g=[],h=0;k=this.xAxis.getExtremes();var f=k.min,q=k.max,m,l,t,n;a=a||this.stackedYData||this.processedYData||[];k=a.length;for(n=0;n<k;n++)if(l=e[n],t=a[n],m=(b(t,!0)||c(t))&&(!d.positiveValuesOnly||t.length||0<t),l=this.getExtremesFromAll||this.options.getExtremesFromAll||this.cropped||(e[n+1]||l)>=f&&(e[n-1]||l)<=q,m&&l)if(m=t.length)for(;m--;)"number"===typeof t[m]&&(g[h++]=t[m]);else g[h++]=t;this.dataMin=
p(g);this.dataMax=H(g)},translate:function(){this.processedXData||this.processData();this.generatePoints();var a=this.options,c=a.stacking,d=this.xAxis,e=d.categories,g=this.yAxis,k=this.points,h=k.length,m=!!this.modifyValue,l=a.pointPlacement,t="between"===l||b(l),n=a.threshold,x=a.startFromThreshold?n:0,p,z,C,r,u=Number.MAX_VALUE;"between"===l&&(l=.5);b(l)&&(l*=I(a.pointRange||d.pointRange));for(a=0;a<h;a++){var B=k[a],N=B.x,K=B.y;z=B.low;var D=c&&g.stacks[(this.negStacks&&K<(x?0:n)?"-":"")+this.stackKey],
E;g.positiveValuesOnly&&null!==K&&0>=K&&(B.isNull=!0);B.plotX=p=f(Math.min(Math.max(-1E5,d.translate(N,0,0,0,1,l,"flags"===this.type)),1E5));c&&this.visible&&!B.isNull&&D&&D[N]&&(r=this.getStackIndicator(r,N,this.index),E=D[N],K=E.points[r.key],z=K[0],K=K[1],z===x&&r.key===D[N].base&&(z=I(n,g.min)),g.positiveValuesOnly&&0>=z&&(z=null),B.total=B.stackTotal=E.total,B.percentage=E.total&&B.y/E.total*100,B.stackY=K,E.setOffset(this.pointXOffset||0,this.barW||0));B.yBottom=w(z)?g.translate(z,0,1,0,1):
null;m&&(K=this.modifyValue(K,B));B.plotY=z="number"===typeof K&&Infinity!==K?Math.min(Math.max(-1E5,g.translate(K,0,1,0,1)),1E5):void 0;B.isInside=void 0!==z&&0<=z&&z<=g.len&&0<=p&&p<=d.len;B.clientX=t?f(d.translate(N,0,0,0,1,l)):p;B.negative=B.y<(n||0);B.category=e&&void 0!==e[B.x]?e[B.x]:B.x;B.isNull||(void 0!==C&&(u=Math.min(u,Math.abs(p-C))),C=p);B.zone=this.zones.length&&B.getZone()}this.closestPointRangePx=u},getValidPoints:function(a,b){var c=this.chart;return d(a||this.points||[],function(a){return b&&
!c.isInsidePlot(a.plotX,a.plotY,c.inverted)?!1:!a.isNull})},setClip:function(a){var b=this.chart,c=this.options,d=b.renderer,g=b.inverted,e=this.clipBox,k=e||b.clipBox,h=this.sharedClipKey||["_sharedClip",a&&a.duration,a&&a.easing,k.height,c.xAxis,c.yAxis].join(),f=b[h],q=b[h+"m"];f||(a&&(k.width=0,g&&(k.x=b.plotSizeX),b[h+"m"]=q=d.clipRect(g?b.plotSizeX+99:-99,g?-b.plotLeft:-b.plotTop,99,g?b.chartWidth:b.chartHeight)),b[h]=f=d.clipRect(k),f.count={length:0});a&&!f.count[this.index]&&(f.count[this.index]=
!0,f.count.length+=1);!1!==c.clip&&(this.group.clip(a||e?f:b.clipRect),this.markerGroup.clip(q),this.sharedClipKey=h);a||(f.count[this.index]&&(delete f.count[this.index],--f.count.length),0===f.count.length&&h&&b[h]&&(e||(b[h]=b[h].destroy()),b[h+"m"]&&(b[h+"m"]=b[h+"m"].destroy())))},animate:function(a){var b=this.chart,c=D(this.options.animation),d;a?this.setClip(c):(d=this.sharedClipKey,(a=b[d])&&a.animate({width:b.plotSizeX,x:0},c),b[d+"m"]&&b[d+"m"].animate({width:b.plotSizeX+99,x:0},c),this.animate=
null)},afterAnimate:function(){this.setClip();m(this,"afterAnimate");this.finishedAnimating=!0},drawPoints:function(){var a=this.points,b=this.chart,c,d,g,e,k=this.options.marker,h,f,l,m=this[this.specialGroup]||this.markerGroup,t,n=I(k.enabled,this.xAxis.isRadial?!0:null,this.closestPointRangePx>=2*k.radius);if(!1!==k.enabled||this._hasPointMarkers)for(c=0;c<a.length;c++)d=a[c],e=d.graphic,h=d.marker||{},f=!!d.marker,g=n&&void 0===h.enabled||h.enabled,l=d.isInside,g&&!d.isNull?(g=I(h.symbol,this.symbol),
d.hasImage=0===g.indexOf("url"),t=this.markerAttribs(d,d.selected&&"select"),e?e[l?"show":"hide"](!0).animate(t):l&&(0<t.width||d.hasImage)&&(d.graphic=e=b.renderer.symbol(g,t.x,t.y,t.width,t.height,f?h:k).add(m)),e&&e.attr(this.pointAttribs(d,d.selected&&"select")),e&&e.addClass(d.getClassName(),!0)):e&&(d.graphic=e.destroy())},markerAttribs:function(a,b){var c=this.options.marker,d=a.marker||{},g=I(d.radius,c.radius);b&&(c=c.states[b],b=d.states&&d.states[b],g=I(b&&b.radius,c&&c.radius,g+(c&&c.radiusPlus||
0)));a.hasImage&&(g=0);a={x:Math.floor(a.plotX)-g,y:a.plotY-g};g&&(a.width=a.height=2*g);return a},pointAttribs:function(a,b){var c=this.options.marker,d=a&&a.options,g=d&&d.marker||{},e=this.color,k=d&&d.color,h=a&&a.color,d=I(g.lineWidth,c.lineWidth);a=a&&a.zone&&a.zone.color;e=k||a||h||e;a=g.fillColor||c.fillColor||e;e=g.lineColor||c.lineColor||e;b&&(c=c.states[b],b=g.states&&g.states[b]||{},d=I(b.lineWidth,c.lineWidth,d+I(b.lineWidthPlus,c.lineWidthPlus,0)),a=b.fillColor||c.fillColor||a,e=b.lineColor||
c.lineColor||e);return{stroke:e,"stroke-width":d,fill:a}},destroy:function(){var a=this,b=a.chart,c=/AppleWebKit\/533/.test(N.navigator.userAgent),d,g,k=a.data||[],h,f;m(a,"destroy");x(a);u(a.axisTypes||[],function(b){(f=a[b])&&f.series&&(e(f.series,a),f.isDirty=f.forceRedraw=!0)});a.legendItem&&a.chart.legend.destroyItem(a);for(g=k.length;g--;)(h=k[g])&&h.destroy&&h.destroy();a.points=null;clearTimeout(a.animationTimeout);B(a,function(a,b){a instanceof t&&!a.survive&&(d=c&&"group"===b?"hide":"destroy",
a[d]())});b.hoverSeries===a&&(b.hoverSeries=null);e(b.series,a);b.orderSeries();B(a,function(b,c){delete a[c]})},getGraphPath:function(a,b,c){var d=this,g=d.options,e=g.step,k,h=[],f=[],l;a=a||d.points;(k=a.reversed)&&a.reverse();(e={right:1,center:2}[e]||e&&3)&&k&&(e=4-e);!g.connectNulls||b||c||(a=this.getValidPoints(a));u(a,function(k,m){var q=k.plotX,t=k.plotY,n=a[m-1];(k.leftCliff||n&&n.rightCliff)&&!c&&(l=!0);k.isNull&&!w(b)&&0<m?l=!g.connectNulls:k.isNull&&!b?l=!0:(0===m||l?m=["M",k.plotX,k.plotY]:
d.getPointSpline?m=d.getPointSpline(a,k,m):e?(m=1===e?["L",n.plotX,t]:2===e?["L",(n.plotX+q)/2,n.plotY,"L",(n.plotX+q)/2,t]:["L",q,n.plotY],m.push("L",q,t)):m=["L",q,t],f.push(k.x),e&&f.push(k.x),h.push.apply(h,m),l=!1)});h.xMap=f;return d.graphPath=h},drawGraph:function(){var a=this,b=this.options,c=(this.gappedPath||this.getGraphPath).call(this),d=[["graph","highcharts-graph",b.lineColor||this.color,b.dashStyle]];u(this.zones,function(c,e){d.push(["zone-graph-"+e,"highcharts-graph highcharts-zone-graph-"+
e+" "+(c.className||""),c.color||a.color,c.dashStyle||b.dashStyle])});u(d,function(d,e){var g=d[0],k=a[g];k?(k.endX=a.preventGraphAnimation?null:c.xMap,k.animate({d:c})):c.length&&(a[g]=a.chart.renderer.path(c).addClass(d[1]).attr({zIndex:1}).add(a.group),k={stroke:d[2],"stroke-width":b.lineWidth,fill:a.fillGraph&&a.color||"none"},d[3]?k.dashstyle=d[3]:"square"!==b.linecap&&(k["stroke-linecap"]=k["stroke-linejoin"]="round"),k=a[g].attr(k).shadow(2>e&&b.shadow));k&&(k.startX=c.xMap,k.isArea=c.isArea)})},
applyZones:function(){var a=this,b=this.chart,c=b.renderer,d=this.zones,e,k,h=this.clips||[],f,m=this.graph,l=this.area,t=Math.max(b.chartWidth,b.chartHeight),n=this[(this.zoneAxis||"y")+"Axis"],x,p,z=b.inverted,C,r,w,B,K=!1;d.length&&(m||l)&&n&&void 0!==n.min&&(p=n.reversed,C=n.horiz,m&&m.hide(),l&&l.hide(),x=n.getExtremes(),u(d,function(d,g){e=p?C?b.plotWidth:0:C?0:n.toPixels(x.min);e=Math.min(Math.max(I(k,e),0),t);k=Math.min(Math.max(Math.round(n.toPixels(I(d.value,x.max),!0)),0),t);K&&(e=k=n.toPixels(x.max));
r=Math.abs(e-k);w=Math.min(e,k);B=Math.max(e,k);n.isXAxis?(f={x:z?B:w,y:0,width:r,height:t},C||(f.x=b.plotHeight-f.x)):(f={x:0,y:z?B:w,width:t,height:r},C&&(f.y=b.plotWidth-f.y));z&&c.isVML&&(f=n.isXAxis?{x:0,y:p?w:B,height:f.width,width:b.chartWidth}:{x:f.y-b.plotLeft-b.spacingBox.x,y:0,width:f.height,height:b.chartHeight});h[g]?h[g].animate(f):(h[g]=c.clipRect(f),m&&a["zone-graph-"+g].clip(h[g]),l&&a["zone-area-"+g].clip(h[g]));K=d.value>x.max}),this.clips=h)},invertGroups:function(a){function b(){u(["group",
"markerGroup"],function(b){c[b]&&(d.renderer.isVML&&c[b].attr({width:c.yAxis.len,height:c.xAxis.len}),c[b].width=c.yAxis.len,c[b].height=c.xAxis.len,c[b].invert(a))})}var c=this,d=c.chart,e;c.xAxis&&(e=E(d,"resize",b),E(c,"destroy",e),b(a),c.invertGroups=b)},plotGroup:function(a,b,c,d,e){var g=this[a],k=!g;k&&(this[a]=g=this.chart.renderer.g().attr({zIndex:d||.1}).add(e));g.addClass("highcharts-"+b+" highcharts-series-"+this.index+" highcharts-"+this.type+"-series "+(w(this.colorIndex)?"highcharts-color-"+
this.colorIndex+" ":"")+(this.options.className||"")+(g.hasClass("highcharts-tracker")?" highcharts-tracker":""),!0);g.attr({visibility:c})[k?"attr":"animate"](this.getPlotBox());return g},getPlotBox:function(){var a=this.chart,b=this.xAxis,c=this.yAxis;a.inverted&&(b=c,c=this.xAxis);return{translateX:b?b.left:a.plotLeft,translateY:c?c.top:a.plotTop,scaleX:1,scaleY:1}},render:function(){var a=this,b=a.chart,c,d=a.options,e=!!a.animate&&b.renderer.isSVG&&D(d.animation).duration,k=a.visible?"inherit":
"hidden",h=d.zIndex,f=a.hasRendered,m=b.seriesGroup,l=b.inverted;c=a.plotGroup("group","series",k,h,m);a.markerGroup=a.plotGroup("markerGroup","markers",k,h,m);e&&a.animate(!0);c.inverted=a.isCartesian?l:!1;a.drawGraph&&(a.drawGraph(),a.applyZones());a.drawDataLabels&&a.drawDataLabels();a.visible&&a.drawPoints();a.drawTracker&&!1!==a.options.enableMouseTracking&&a.drawTracker();a.invertGroups(l);!1===d.clip||a.sharedClipKey||f||c.clip(b.clipRect);e&&a.animate();f||(a.animationTimeout=C(function(){a.afterAnimate()},
e));a.isDirty=!1;a.hasRendered=!0},redraw:function(){var a=this.chart,b=this.isDirty||this.isDirtyData,c=this.group,d=this.xAxis,e=this.yAxis;c&&(a.inverted&&c.attr({width:a.plotWidth,height:a.plotHeight}),c.animate({translateX:I(d&&d.left,a.plotLeft),translateY:I(e&&e.top,a.plotTop)}));this.translate();this.render();b&&delete this.kdTree},kdAxisArray:["clientX","plotY"],searchPoint:function(a,b){var c=this.xAxis,d=this.yAxis,e=this.chart.inverted;return this.searchKDTree({clientX:e?c.len-a.chartY+
c.pos:a.chartX-c.pos,plotY:e?d.len-a.chartX+d.pos:a.chartY-d.pos},b)},buildKDTree:function(){function a(c,d,e){var g,k;if(k=c&&c.length)return g=b.kdAxisArray[d%e],c.sort(function(a,b){return a[g]-b[g]}),k=Math.floor(k/2),{point:c[k],left:a(c.slice(0,k),d+1,e),right:a(c.slice(k+1),d+1,e)}}this.buildingKdTree=!0;var b=this,c=-1<b.options.findNearestPointBy.indexOf("y")?2:1;delete b.kdTree;C(function(){b.kdTree=a(b.getValidPoints(null,!b.directTouch),c,c);b.buildingKdTree=!1},b.options.kdNow?0:1)},
searchKDTree:function(a,b){function c(a,b,g,f){var m=b.point,l=d.kdAxisArray[g%f],q,t,n=m;t=w(a[e])&&w(m[e])?Math.pow(a[e]-m[e],2):null;q=w(a[k])&&w(m[k])?Math.pow(a[k]-m[k],2):null;q=(t||0)+(q||0);m.dist=w(q)?Math.sqrt(q):Number.MAX_VALUE;m.distX=w(t)?Math.sqrt(t):Number.MAX_VALUE;l=a[l]-m[l];q=0>l?"left":"right";t=0>l?"right":"left";b[q]&&(q=c(a,b[q],g+1,f),n=q[h]<n[h]?q:m);b[t]&&Math.sqrt(l*l)<n[h]&&(a=c(a,b[t],g+1,f),n=a[h]<n[h]?a:n);return n}var d=this,e=this.kdAxisArray[0],k=this.kdAxisArray[1],
h=b?"distX":"dist";b=-1<d.options.findNearestPointBy.indexOf("y")?2:1;this.kdTree||this.buildingKdTree||this.buildKDTree();if(this.kdTree)return c(a,this.kdTree,b,b)}})})(M);(function(a){var E=a.Axis,D=a.Chart,H=a.correctFloat,p=a.defined,f=a.destroyObjectProperties,l=a.each,r=a.format,n=a.objectEach,w=a.pick,u=a.Series;a.StackItem=function(a,h,f,d,c){var b=a.chart.inverted;this.axis=a;this.isNegative=f;this.options=h;this.x=d;this.total=null;this.points={};this.stack=c;this.rightCliff=this.leftCliff=
0;this.alignOptions={align:h.align||(b?f?"left":"right":"center"),verticalAlign:h.verticalAlign||(b?"middle":f?"bottom":"top"),y:w(h.y,b?4:f?14:-6),x:w(h.x,b?f?-6:6:0)};this.textAlign=h.textAlign||(b?f?"right":"left":"center")};a.StackItem.prototype={destroy:function(){f(this,this.axis)},render:function(a){var e=this.options,f=e.format,f=f?r(f,this):e.formatter.call(this);this.label?this.label.attr({text:f,visibility:"hidden"}):this.label=this.axis.chart.renderer.text(f,null,null,e.useHTML).css(e.style).attr({align:this.textAlign,
rotation:e.rotation,visibility:"hidden"}).add(a)},setOffset:function(a,h){var e=this.axis,d=e.chart,c=e.translate(e.usePercentage?100:this.total,0,0,0,1),e=e.translate(0),e=Math.abs(c-e);a=d.xAxis[0].translate(this.x)+a;c=this.getStackBox(d,this,a,c,h,e);if(h=this.label)h.align(this.alignOptions,null,c),c=h.alignAttr,h[!1===this.options.crop||d.isInsidePlot(c.x,c.y)?"show":"hide"](!0)},getStackBox:function(a,h,f,d,c,b){var e=h.axis.reversed,l=a.inverted;a=a.plotHeight;h=h.isNegative&&!e||!h.isNegative&&
e;return{x:l?h?d:d-b:f,y:l?a-f-c:h?a-d-b:a-d,width:l?b:c,height:l?c:b}}};D.prototype.getStacks=function(){var a=this;l(a.yAxis,function(a){a.stacks&&a.hasVisibleSeries&&(a.oldStacks=a.stacks)});l(a.series,function(e){!e.options.stacking||!0!==e.visible&&!1!==a.options.chart.ignoreHiddenSeries||(e.stackKey=e.type+w(e.options.stack,""))})};E.prototype.buildStacks=function(){var a=this.series,h=w(this.options.reversedStacks,!0),f=a.length,d;if(!this.isXAxis){this.usePercentage=!1;for(d=f;d--;)a[h?d:
f-d-1].setStackedPoints();for(d=0;d<f;d++)a[d].modifyStacks()}};E.prototype.renderStackTotals=function(){var a=this.chart,h=a.renderer,f=this.stacks,d=this.stackTotalGroup;d||(this.stackTotalGroup=d=h.g("stack-labels").attr({visibility:"visible",zIndex:6}).add());d.translate(a.plotLeft,a.plotTop);n(f,function(a){n(a,function(a){a.render(d)})})};E.prototype.resetStacks=function(){var a=this,h=a.stacks;a.isXAxis||n(h,function(e){n(e,function(d,c){d.touched<a.stacksTouched?(d.destroy(),delete e[c]):
(d.total=null,d.cumulative=null)})})};E.prototype.cleanStacks=function(){var a;this.isXAxis||(this.oldStacks&&(a=this.stacks=this.oldStacks),n(a,function(a){n(a,function(a){a.cumulative=a.total})}))};u.prototype.setStackedPoints=function(){if(this.options.stacking&&(!0===this.visible||!1===this.chart.options.chart.ignoreHiddenSeries)){var e=this.processedXData,h=this.processedYData,f=[],d=h.length,c=this.options,b=c.threshold,k=w(c.startFromThreshold&&b,0),l=c.stack,c=c.stacking,n=this.stackKey,r=
"-"+n,x=this.negStacks,u=this.yAxis,t=u.stacks,C=u.oldStacks,N,q,A,F,G,g,v;u.stacksTouched+=1;for(G=0;G<d;G++)g=e[G],v=h[G],N=this.getStackIndicator(N,g,this.index),F=N.key,A=(q=x&&v<(k?0:b))?r:n,t[A]||(t[A]={}),t[A][g]||(C[A]&&C[A][g]?(t[A][g]=C[A][g],t[A][g].total=null):t[A][g]=new a.StackItem(u,u.options.stackLabels,q,g,l)),A=t[A][g],null!==v?(A.points[F]=A.points[this.index]=[w(A.cumulative,k)],p(A.cumulative)||(A.base=F),A.touched=u.stacksTouched,0<N.index&&!1===this.singleStacks&&(A.points[F][0]=
A.points[this.index+","+g+",0"][0])):A.points[F]=A.points[this.index]=null,"percent"===c?(q=q?n:r,x&&t[q]&&t[q][g]?(q=t[q][g],A.total=q.total=Math.max(q.total,A.total)+Math.abs(v)||0):A.total=H(A.total+(Math.abs(v)||0))):A.total=H(A.total+(v||0)),A.cumulative=w(A.cumulative,k)+(v||0),null!==v&&(A.points[F].push(A.cumulative),f[G]=A.cumulative);"percent"===c&&(u.usePercentage=!0);this.stackedYData=f;u.oldStacks={}}};u.prototype.modifyStacks=function(){var a=this,h=a.stackKey,f=a.yAxis.stacks,d=a.processedXData,
c,b=a.options.stacking;a[b+"Stacker"]&&l([h,"-"+h],function(e){for(var k=d.length,h,l;k--;)if(h=d[k],c=a.getStackIndicator(c,h,a.index,e),l=(h=f[e]&&f[e][h])&&h.points[c.key])a[b+"Stacker"](l,h,k)})};u.prototype.percentStacker=function(a,h,f){h=h.total?100/h.total:0;a[0]=H(a[0]*h);a[1]=H(a[1]*h);this.stackedYData[f]=a[1]};u.prototype.getStackIndicator=function(a,h,f,d){!p(a)||a.x!==h||d&&a.key!==d?a={x:h,index:0,key:d}:a.index++;a.key=[f,h,a.index].join();return a}})(M);(function(a){var E=a.addEvent,
D=a.animate,H=a.Axis,p=a.createElement,f=a.css,l=a.defined,r=a.each,n=a.erase,w=a.extend,u=a.fireEvent,e=a.inArray,h=a.isNumber,m=a.isObject,d=a.isArray,c=a.merge,b=a.objectEach,k=a.pick,z=a.Point,B=a.Series,I=a.seriesTypes,x=a.setAnimation,K=a.splat;w(a.Chart.prototype,{addSeries:function(a,b,c){var d,e=this;a&&(b=k(b,!0),u(e,"addSeries",{options:a},function(){d=e.initSeries(a);e.isDirtyLegend=!0;e.linkSeries();b&&e.redraw(c)}));return d},addAxis:function(a,b,d,e){var h=b?"xAxis":"yAxis",f=this.options;
a=c(a,{index:this[h].length,isX:b});b=new H(this,a);f[h]=K(f[h]||{});f[h].push(a);k(d,!0)&&this.redraw(e);return b},showLoading:function(a){var b=this,c=b.options,d=b.loadingDiv,e=c.loading,k=function(){d&&f(d,{left:b.plotLeft+"px",top:b.plotTop+"px",width:b.plotWidth+"px",height:b.plotHeight+"px"})};d||(b.loadingDiv=d=p("div",{className:"highcharts-loading highcharts-loading-hidden"},null,b.container),b.loadingSpan=p("span",{className:"highcharts-loading-inner"},null,d),E(b,"redraw",k));d.className=
"highcharts-loading";b.loadingSpan.innerHTML=a||c.lang.loading;f(d,w(e.style,{zIndex:10}));f(b.loadingSpan,e.labelStyle);b.loadingShown||(f(d,{opacity:0,display:""}),D(d,{opacity:e.style.opacity||.5},{duration:e.showDuration||0}));b.loadingShown=!0;k()},hideLoading:function(){var a=this.options,b=this.loadingDiv;b&&(b.className="highcharts-loading highcharts-loading-hidden",D(b,{opacity:0},{duration:a.loading.hideDuration||100,complete:function(){f(b,{display:"none"})}}));this.loadingShown=!1},propsRequireDirtyBox:"backgroundColor borderColor borderWidth margin marginTop marginRight marginBottom marginLeft spacing spacingTop spacingRight spacingBottom spacingLeft borderRadius plotBackgroundColor plotBackgroundImage plotBorderColor plotBorderWidth plotShadow shadow".split(" "),
propsRequireUpdateSeries:"chart.inverted chart.polar chart.ignoreHiddenSeries chart.type colors plotOptions tooltip".split(" "),update:function(a,d,f){var m=this,n={credits:"addCredits",title:"setTitle",subtitle:"setSubtitle"},t=a.chart,x,g,p=[];if(t){c(!0,m.options.chart,t);"className"in t&&m.setClassName(t.className);if("inverted"in t||"polar"in t)m.propFromSeries(),x=!0;"alignTicks"in t&&(x=!0);b(t,function(a,b){-1!==e("chart."+b,m.propsRequireUpdateSeries)&&(g=!0);-1!==e(b,m.propsRequireDirtyBox)&&
(m.isDirtyBox=!0)});"style"in t&&m.renderer.setStyle(t.style)}a.colors&&(this.options.colors=a.colors);a.plotOptions&&c(!0,this.options.plotOptions,a.plotOptions);b(a,function(a,b){if(m[b]&&"function"===typeof m[b].update)m[b].update(a,!1);else if("function"===typeof m[n[b]])m[n[b]](a);"chart"!==b&&-1!==e(b,m.propsRequireUpdateSeries)&&(g=!0)});r("xAxis yAxis zAxis series colorAxis pane".split(" "),function(b){a[b]&&(r(K(a[b]),function(a,c){(c=l(a.id)&&m.get(a.id)||m[b][c])&&c.coll===b&&(c.update(a,
!1),f&&(c.touched=!0));if(!c&&f)if("series"===b)m.addSeries(a,!1).touched=!0;else if("xAxis"===b||"yAxis"===b)m.addAxis(a,"xAxis"===b,!1).touched=!0}),f&&r(m[b],function(a){a.touched?delete a.touched:p.push(a)}))});r(p,function(a){a.remove(!1)});x&&r(m.axes,function(a){a.update({},!1)});g&&r(m.series,function(a){a.update({},!1)});a.loading&&c(!0,m.options.loading,a.loading);x=t&&t.width;t=t&&t.height;h(x)&&x!==m.chartWidth||h(t)&&t!==m.chartHeight?m.setSize(x,t):k(d,!0)&&m.redraw()},setSubtitle:function(a){this.setTitle(void 0,
a)}});w(z.prototype,{update:function(a,b,c,d){function e(){h.applyOptions(a);null===h.y&&g&&(h.graphic=g.destroy());m(a,!0)&&(g&&g.element&&a&&a.marker&&void 0!==a.marker.symbol&&(h.graphic=g.destroy()),a&&a.dataLabels&&h.dataLabel&&(h.dataLabel=h.dataLabel.destroy()),h.connector&&(h.connector=h.connector.destroy()));l=h.index;f.updateParallelArrays(h,l);t.data[l]=m(t.data[l],!0)||m(a,!0)?h.options:a;f.isDirty=f.isDirtyData=!0;!f.fixedBox&&f.hasCartesianSeries&&(n.isDirtyBox=!0);"point"===t.legendType&&
(n.isDirtyLegend=!0);b&&n.redraw(c)}var h=this,f=h.series,g=h.graphic,l,n=f.chart,t=f.options;b=k(b,!0);!1===d?e():h.firePointEvent("update",{options:a},e)},remove:function(a,b){this.series.removePoint(e(this,this.series.data),a,b)}});w(B.prototype,{addPoint:function(a,b,c,d){var e=this.options,h=this.data,f=this.chart,g=this.xAxis,g=g&&g.hasNames&&g.names,l=e.data,m,n,t=this.xData,q,x;b=k(b,!0);m={series:this};this.pointClass.prototype.applyOptions.apply(m,[a]);x=m.x;q=t.length;if(this.requireSorting&&
x<t[q-1])for(n=!0;q&&t[q-1]>x;)q--;this.updateParallelArrays(m,"splice",q,0,0);this.updateParallelArrays(m,q);g&&m.name&&(g[x]=m.name);l.splice(q,0,a);n&&(this.data.splice(q,0,null),this.processData());"point"===e.legendType&&this.generatePoints();c&&(h[0]&&h[0].remove?h[0].remove(!1):(h.shift(),this.updateParallelArrays(m,"shift"),l.shift()));this.isDirtyData=this.isDirty=!0;b&&f.redraw(d)},removePoint:function(a,b,c){var d=this,e=d.data,h=e[a],f=d.points,g=d.chart,l=function(){f&&f.length===e.length&&
f.splice(a,1);e.splice(a,1);d.options.data.splice(a,1);d.updateParallelArrays(h||{series:d},"splice",a,1);h&&h.destroy();d.isDirty=!0;d.isDirtyData=!0;b&&g.redraw()};x(c,g);b=k(b,!0);h?h.firePointEvent("remove",null,l):l()},remove:function(a,b,c){function d(){e.destroy();h.isDirtyLegend=h.isDirtyBox=!0;h.linkSeries();k(a,!0)&&h.redraw(b)}var e=this,h=e.chart;!1!==c?u(e,"remove",null,d):d()},update:function(a,b){var d=this,e=d.chart,h=d.userOptions,f=d.oldType||d.type,l=a.type||h.type||e.options.chart.type,
g=I[f].prototype,m,n=["group","markerGroup","dataLabelsGroup"],t=["navigatorSeries","baseSeries"],x=d.finishedAnimating&&{animation:!1};if(Object.keys&&"data"===Object.keys(a).toString())return this.setData(a.data,b);t=n.concat(t);r(t,function(a){t[a]=d[a];delete d[a]});a=c(h,x,{index:d.index,pointStart:d.xData[0]},{data:d.options.data},a);d.remove(!1,null,!1);for(m in g)d[m]=void 0;w(d,I[l||f].prototype);r(t,function(a){d[a]=t[a]});d.init(e,a);a.zIndex!==h.zIndex&&r(n,function(b){d[b]&&d[b].attr({zIndex:a.zIndex})});
d.oldType=f;e.linkSeries();k(b,!0)&&e.redraw(!1)}});w(H.prototype,{update:function(a,b){var d=this.chart;a=d.options[this.coll][this.options.index]=c(this.userOptions,a);this.destroy(!0);this.init(d,w(a,{events:void 0}));d.isDirtyBox=!0;k(b,!0)&&d.redraw()},remove:function(a){for(var b=this.chart,c=this.coll,e=this.series,h=e.length;h--;)e[h]&&e[h].remove(!1);n(b.axes,this);n(b[c],this);d(b.options[c])?b.options[c].splice(this.options.index,1):delete b.options[c];r(b[c],function(a,b){a.options.index=
b});this.destroy();b.isDirtyBox=!0;k(a,!0)&&b.redraw()},setTitle:function(a,b){this.update({title:a},b)},setCategories:function(a,b){this.update({categories:a},b)}})})(M);(function(a){var E=a.color,D=a.each,H=a.map,p=a.pick,f=a.Series,l=a.seriesType;l("area","line",{softThreshold:!1,threshold:0},{singleStacks:!1,getStackPoints:function(f){var l=[],r=[],u=this.xAxis,e=this.yAxis,h=e.stacks[this.stackKey],m={},d=this.index,c=e.series,b=c.length,k,z=p(e.options.reversedStacks,!0)?1:-1,B;f=f||this.points;
if(this.options.stacking){for(B=0;B<f.length;B++)f[B].leftNull=f[B].rightNull=null,m[f[B].x]=f[B];a.objectEach(h,function(a,b){null!==a.total&&r.push(b)});r.sort(function(a,b){return a-b});k=H(c,function(){return this.visible});D(r,function(a,c){var f=0,n,x;if(m[a]&&!m[a].isNull)l.push(m[a]),D([-1,1],function(e){var f=1===e?"rightNull":"leftNull",l=0,t=h[r[c+e]];if(t)for(B=d;0<=B&&B<b;)n=t.points[B],n||(B===d?m[a][f]=!0:k[B]&&(x=h[a].points[B])&&(l-=x[1]-x[0])),B+=z;m[a][1===e?"rightCliff":"leftCliff"]=
l});else{for(B=d;0<=B&&B<b;){if(n=h[a].points[B]){f=n[1];break}B+=z}f=e.translate(f,0,1,0,1);l.push({isNull:!0,plotX:u.translate(a,0,0,0,1),x:a,plotY:f,yBottom:f})}})}return l},getGraphPath:function(a){var l=f.prototype.getGraphPath,r=this.options,u=r.stacking,e=this.yAxis,h,m,d=[],c=[],b=this.index,k,z=e.stacks[this.stackKey],B=r.threshold,I=e.getThreshold(r.threshold),x,r=r.connectNulls||"percent"===u,K=function(h,f,l){var m=a[h];h=u&&z[m.x].points[b];var n=m[l+"Null"]||0;l=m[l+"Cliff"]||0;var x,
t,m=!0;l||n?(x=(n?h[0]:h[1])+l,t=h[0]+l,m=!!n):!u&&a[f]&&a[f].isNull&&(x=t=B);void 0!==x&&(c.push({plotX:k,plotY:null===x?I:e.getThreshold(x),isNull:m,isCliff:!0}),d.push({plotX:k,plotY:null===t?I:e.getThreshold(t),doCurve:!1}))};a=a||this.points;u&&(a=this.getStackPoints(a));for(h=0;h<a.length;h++)if(m=a[h].isNull,k=p(a[h].rectPlotX,a[h].plotX),x=p(a[h].yBottom,I),!m||r)r||K(h,h-1,"left"),m&&!u&&r||(c.push(a[h]),d.push({x:h,plotX:k,plotY:x})),r||K(h,h+1,"right");h=l.call(this,c,!0,!0);d.reversed=
!0;m=l.call(this,d,!0,!0);m.length&&(m[0]="L");m=h.concat(m);l=l.call(this,c,!1,r);m.xMap=h.xMap;this.areaPath=m;return l},drawGraph:function(){this.areaPath=[];f.prototype.drawGraph.apply(this);var a=this,l=this.areaPath,w=this.options,u=[["area","highcharts-area",this.color,w.fillColor]];D(this.zones,function(e,h){u.push(["zone-area-"+h,"highcharts-area highcharts-zone-area-"+h+" "+e.className,e.color||a.color,e.fillColor||w.fillColor])});D(u,function(e){var h=e[0],f=a[h];f?(f.endX=a.preventGraphAnimation?
null:l.xMap,f.animate({d:l})):(f=a[h]=a.chart.renderer.path(l).addClass(e[1]).attr({fill:p(e[3],E(e[2]).setOpacity(p(w.fillOpacity,.75)).get()),zIndex:0}).add(a.group),f.isArea=!0);f.startX=l.xMap;f.shiftUnit=w.step?2:1})},drawLegendSymbol:a.LegendSymbolMixin.drawRectangle})})(M);(function(a){var E=a.pick;a=a.seriesType;a("spline","line",{},{getPointSpline:function(a,H,p){var f=H.plotX,l=H.plotY,r=a[p-1];p=a[p+1];var n,w,u,e;if(r&&!r.isNull&&!1!==r.doCurve&&!H.isCliff&&p&&!p.isNull&&!1!==p.doCurve&&
!H.isCliff){a=r.plotY;u=p.plotX;p=p.plotY;var h=0;n=(1.5*f+r.plotX)/2.5;w=(1.5*l+a)/2.5;u=(1.5*f+u)/2.5;e=(1.5*l+p)/2.5;u!==n&&(h=(e-w)*(u-f)/(u-n)+l-e);w+=h;e+=h;w>a&&w>l?(w=Math.max(a,l),e=2*l-w):w<a&&w<l&&(w=Math.min(a,l),e=2*l-w);e>p&&e>l?(e=Math.max(p,l),w=2*l-e):e<p&&e<l&&(e=Math.min(p,l),w=2*l-e);H.rightContX=u;H.rightContY=e}H=["C",E(r.rightContX,r.plotX),E(r.rightContY,r.plotY),E(n,f),E(w,l),f,l];r.rightContX=r.rightContY=null;return H}})})(M);(function(a){var E=a.seriesTypes.area.prototype,
D=a.seriesType;D("areaspline","spline",a.defaultPlotOptions.area,{getStackPoints:E.getStackPoints,getGraphPath:E.getGraphPath,drawGraph:E.drawGraph,drawLegendSymbol:a.LegendSymbolMixin.drawRectangle})})(M);(function(a){var E=a.animObject,D=a.color,H=a.each,p=a.extend,f=a.isNumber,l=a.merge,r=a.pick,n=a.Series,w=a.seriesType,u=a.svg;w("column","line",{borderRadius:0,crisp:!0,groupPadding:.2,marker:null,pointPadding:.1,minPointLength:0,cropThreshold:50,pointRange:null,states:{hover:{halo:!1,brightness:.1},
select:{color:"#cccccc",borderColor:"#000000"}},dataLabels:{align:null,verticalAlign:null,y:null},softThreshold:!1,startFromThreshold:!0,stickyTracking:!1,tooltip:{distance:6},threshold:0,borderColor:"#ffffff"},{cropShoulder:0,directTouch:!0,trackerGroups:["group","dataLabelsGroup"],negStacks:!0,init:function(){n.prototype.init.apply(this,arguments);var a=this,h=a.chart;h.hasRendered&&H(h.series,function(e){e.type===a.type&&(e.isDirty=!0)})},getColumnMetrics:function(){var a=this,h=a.options,f=a.xAxis,
d=a.yAxis,c=f.reversed,b,k={},l=0;!1===h.grouping?l=1:H(a.chart.series,function(c){var e=c.options,h=c.yAxis,f;c.type!==a.type||!c.visible&&a.chart.options.chart.ignoreHiddenSeries||d.len!==h.len||d.pos!==h.pos||(e.stacking?(b=c.stackKey,void 0===k[b]&&(k[b]=l++),f=k[b]):!1!==e.grouping&&(f=l++),c.columnIndex=f)});var n=Math.min(Math.abs(f.transA)*(f.ordinalSlope||h.pointRange||f.closestPointRange||f.tickInterval||1),f.len),p=n*h.groupPadding,x=(n-2*p)/(l||1),h=Math.min(h.maxPointWidth||f.len,r(h.pointWidth,
x*(1-2*h.pointPadding)));a.columnMetrics={width:h,offset:(x-h)/2+(p+((a.columnIndex||0)+(c?1:0))*x-n/2)*(c?-1:1)};return a.columnMetrics},crispCol:function(a,h,f,d){var c=this.chart,b=this.borderWidth,e=-(b%2?.5:0),b=b%2?.5:1;c.inverted&&c.renderer.isVML&&(b+=1);this.options.crisp&&(f=Math.round(a+f)+e,a=Math.round(a)+e,f-=a);d=Math.round(h+d)+b;e=.5>=Math.abs(h)&&.5<d;h=Math.round(h)+b;d-=h;e&&d&&(--h,d+=1);return{x:a,y:h,width:f,height:d}},translate:function(){var a=this,h=a.chart,f=a.options,d=
a.dense=2>a.closestPointRange*a.xAxis.transA,d=a.borderWidth=r(f.borderWidth,d?0:1),c=a.yAxis,b=f.threshold,k=a.translatedThreshold=c.getThreshold(b),l=r(f.minPointLength,5),p=a.getColumnMetrics(),u=p.width,x=a.barW=Math.max(u,1+2*d),w=a.pointXOffset=p.offset;h.inverted&&(k-=.5);f.pointPadding&&(x=Math.ceil(x));n.prototype.translate.apply(a);H(a.points,function(d){var e=r(d.yBottom,k),f=999+Math.abs(e),f=Math.min(Math.max(-f,d.plotY),c.len+f),m=d.plotX+w,n=x,t=Math.min(f,e),p,g=Math.max(f,e)-t;l&&
Math.abs(g)<l&&(g=l,p=!c.reversed&&!d.negative||c.reversed&&d.negative,d.y===b&&a.dataMax<=b&&c.min<b&&(p=!p),t=Math.abs(t-k)>l?e-l:k-(p?l:0));d.barX=m;d.pointWidth=u;d.tooltipPos=h.inverted?[c.len+c.pos-h.plotLeft-f,a.xAxis.len-m-n/2,g]:[m+n/2,f+c.pos-h.plotTop,g];d.shapeType="rect";d.shapeArgs=a.crispCol.apply(a,d.isNull?[m,k,n,0]:[m,t,n,g])})},getSymbol:a.noop,drawLegendSymbol:a.LegendSymbolMixin.drawRectangle,drawGraph:function(){this.group[this.dense?"addClass":"removeClass"]("highcharts-dense-data")},
pointAttribs:function(a,f){var e=this.options,d,c=this.pointAttrToOptions||{};d=c.stroke||"borderColor";var b=c["stroke-width"]||"borderWidth",k=a&&a.color||this.color,h=a&&a[d]||e[d]||this.color||k,n=a&&a[b]||e[b]||this[b]||0,c=e.dashStyle;a&&this.zones.length&&(k=a.getZone(),k=a.options.color||k&&k.color||this.color);f&&(a=l(e.states[f],a.options.states&&a.options.states[f]||{}),f=a.brightness,k=a.color||void 0!==f&&D(k).brighten(a.brightness).get()||k,h=a[d]||h,n=a[b]||n,c=a.dashStyle||c);d={fill:k,
stroke:h,"stroke-width":n};c&&(d.dashstyle=c);return d},drawPoints:function(){var a=this,h=this.chart,m=a.options,d=h.renderer,c=m.animationLimit||250,b;H(a.points,function(e){var k=e.graphic;if(f(e.plotY)&&null!==e.y){b=e.shapeArgs;if(k)k[h.pointCount<c?"animate":"attr"](l(b));else e.graphic=k=d[e.shapeType](b).add(e.group||a.group);m.borderRadius&&k.attr({r:m.borderRadius});k.attr(a.pointAttribs(e,e.selected&&"select")).shadow(m.shadow,null,m.stacking&&!m.borderRadius);k.addClass(e.getClassName(),
!0)}else k&&(e.graphic=k.destroy())})},animate:function(a){var e=this,f=this.yAxis,d=e.options,c=this.chart.inverted,b={},k=c?"translateX":"translateY",l;u&&(a?(b.scaleY=.001,a=Math.min(f.pos+f.len,Math.max(f.pos,f.toPixels(d.threshold))),c?b.translateX=a-f.len:b.translateY=a,e.group.attr(b)):(l=e.group.attr(k),e.group.animate({scaleY:1},p(E(e.options.animation),{step:function(a,c){b[k]=l+c.pos*(f.pos-l);e.group.attr(b)}})),e.animate=null))},remove:function(){var a=this,f=a.chart;f.hasRendered&&H(f.series,
function(e){e.type===a.type&&(e.isDirty=!0)});n.prototype.remove.apply(a,arguments)}})})(M);(function(a){a=a.seriesType;a("bar","column",null,{inverted:!0})})(M);(function(a){var E=a.Series;a=a.seriesType;a("scatter","line",{lineWidth:0,findNearestPointBy:"xy",marker:{enabled:!0},tooltip:{headerFormat:'\x3cspan style\x3d"color:{point.color}"\x3e\u25cf\x3c/span\x3e \x3cspan style\x3d"font-size: 0.85em"\x3e {series.name}\x3c/span\x3e\x3cbr/\x3e',pointFormat:"x: \x3cb\x3e{point.x}\x3c/b\x3e\x3cbr/\x3ey: \x3cb\x3e{point.y}\x3c/b\x3e\x3cbr/\x3e"}},
{sorted:!1,requireSorting:!1,noSharedTooltip:!0,trackerGroups:["group","markerGroup","dataLabelsGroup"],takeOrdinalPosition:!1,drawGraph:function(){this.options.lineWidth&&E.prototype.drawGraph.call(this)}})})(M);(function(a){var E=a.deg2rad,D=a.isNumber,H=a.pick,p=a.relativeLength;a.CenteredSeriesMixin={getCenter:function(){var a=this.options,l=this.chart,r=2*(a.slicedOffset||0),n=l.plotWidth-2*r,l=l.plotHeight-2*r,w=a.center,w=[H(w[0],"50%"),H(w[1],"50%"),a.size||"100%",a.innerSize||0],u=Math.min(n,
l),e,h;for(e=0;4>e;++e)h=w[e],a=2>e||2===e&&/%$/.test(h),w[e]=p(h,[n,l,u,w[2]][e])+(a?r:0);w[3]>w[2]&&(w[3]=w[2]);return w},getStartAndEndRadians:function(a,l){a=D(a)?a:0;l=D(l)&&l>a&&360>l-a?l:a+360;return{start:E*(a+-90),end:E*(l+-90)}}}})(M);(function(a){var E=a.addEvent,D=a.CenteredSeriesMixin,H=a.defined,p=a.each,f=a.extend,l=D.getStartAndEndRadians,r=a.inArray,n=a.noop,w=a.pick,u=a.Point,e=a.Series,h=a.seriesType,m=a.setAnimation;h("pie","line",{center:[null,null],clip:!1,colorByPoint:!0,dataLabels:{distance:30,
enabled:!0,formatter:function(){return this.point.isNull?void 0:this.point.name},x:0},ignoreHiddenPoint:!0,legendType:"point",marker:null,size:null,showInLegend:!1,slicedOffset:10,stickyTracking:!1,tooltip:{followPointer:!0},borderColor:"#ffffff",borderWidth:1,states:{hover:{brightness:.1,shadow:!1}}},{isCartesian:!1,requireSorting:!1,directTouch:!0,noSharedTooltip:!0,trackerGroups:["group","dataLabelsGroup"],axisTypes:[],pointAttribs:a.seriesTypes.column.prototype.pointAttribs,animate:function(a){var c=
this,b=c.points,d=c.startAngleRad;a||(p(b,function(a){var b=a.graphic,e=a.shapeArgs;b&&(b.attr({r:a.startR||c.center[3]/2,start:d,end:d}),b.animate({r:e.r,start:e.start,end:e.end},c.options.animation))}),c.animate=null)},updateTotals:function(){var a,c=0,b=this.points,e=b.length,f,h=this.options.ignoreHiddenPoint;for(a=0;a<e;a++)f=b[a],c+=h&&!f.visible?0:f.isNull?0:f.y;this.total=c;for(a=0;a<e;a++)f=b[a],f.percentage=0<c&&(f.visible||!h)?f.y/c*100:0,f.total=c},generatePoints:function(){e.prototype.generatePoints.call(this);
this.updateTotals()},translate:function(a){this.generatePoints();var c=0,b=this.options,d=b.slicedOffset,e=d+(b.borderWidth||0),f,h,m,n=l(b.startAngle,b.endAngle),t=this.startAngleRad=n.start,n=(this.endAngleRad=n.end)-t,p=this.points,u,q=b.dataLabels.distance,b=b.ignoreHiddenPoint,r,F=p.length,G;a||(this.center=a=this.getCenter());this.getX=function(b,c,d){m=Math.asin(Math.min((b-a[1])/(a[2]/2+d.labelDistance),1));return a[0]+(c?-1:1)*Math.cos(m)*(a[2]/2+d.labelDistance)};for(r=0;r<F;r++){G=p[r];
G.labelDistance=w(G.options.dataLabels&&G.options.dataLabels.distance,q);this.maxLabelDistance=Math.max(this.maxLabelDistance||0,G.labelDistance);f=t+c*n;if(!b||G.visible)c+=G.percentage/100;h=t+c*n;G.shapeType="arc";G.shapeArgs={x:a[0],y:a[1],r:a[2]/2,innerR:a[3]/2,start:Math.round(1E3*f)/1E3,end:Math.round(1E3*h)/1E3};m=(h+f)/2;m>1.5*Math.PI?m-=2*Math.PI:m<-Math.PI/2&&(m+=2*Math.PI);G.slicedTranslation={translateX:Math.round(Math.cos(m)*d),translateY:Math.round(Math.sin(m)*d)};h=Math.cos(m)*a[2]/
2;u=Math.sin(m)*a[2]/2;G.tooltipPos=[a[0]+.7*h,a[1]+.7*u];G.half=m<-Math.PI/2||m>Math.PI/2?1:0;G.angle=m;f=Math.min(e,G.labelDistance/5);G.labelPos=[a[0]+h+Math.cos(m)*G.labelDistance,a[1]+u+Math.sin(m)*G.labelDistance,a[0]+h+Math.cos(m)*f,a[1]+u+Math.sin(m)*f,a[0]+h,a[1]+u,0>G.labelDistance?"center":G.half?"right":"left",m]}},drawGraph:null,drawPoints:function(){var a=this,c=a.chart.renderer,b,e,h,l,m=a.options.shadow;m&&!a.shadowGroup&&(a.shadowGroup=c.g("shadow").add(a.group));p(a.points,function(d){e=
d.graphic;if(d.isNull)e&&(d.graphic=e.destroy());else{l=d.shapeArgs;b=d.getTranslate();var k=d.shadowGroup;m&&!k&&(k=d.shadowGroup=c.g("shadow").add(a.shadowGroup));k&&k.attr(b);h=a.pointAttribs(d,d.selected&&"select");e?e.setRadialReference(a.center).attr(h).animate(f(l,b)):(d.graphic=e=c[d.shapeType](l).setRadialReference(a.center).attr(b).add(a.group),d.visible||e.attr({visibility:"hidden"}),e.attr(h).attr({"stroke-linejoin":"round"}).shadow(m,k));e.addClass(d.getClassName())}})},searchPoint:n,
sortByAngle:function(a,c){a.sort(function(a,d){return void 0!==a.angle&&(d.angle-a.angle)*c})},drawLegendSymbol:a.LegendSymbolMixin.drawRectangle,getCenter:D.getCenter,getSymbol:n},{init:function(){u.prototype.init.apply(this,arguments);var a=this,c;a.name=w(a.name,"Slice");c=function(b){a.slice("select"===b.type)};E(a,"select",c);E(a,"unselect",c);return a},isValid:function(){return a.isNumber(this.y,!0)&&0<=this.y},setVisible:function(a,c){var b=this,d=b.series,e=d.chart,f=d.options.ignoreHiddenPoint;
c=w(c,f);a!==b.visible&&(b.visible=b.options.visible=a=void 0===a?!b.visible:a,d.options.data[r(b,d.data)]=b.options,p(["graphic","dataLabel","connector","shadowGroup"],function(c){if(b[c])b[c][a?"show":"hide"](!0)}),b.legendItem&&e.legend.colorizeItem(b,a),a||"hover"!==b.state||b.setState(""),f&&(d.isDirty=!0),c&&e.redraw())},slice:function(a,c,b){var d=this.series;m(b,d.chart);w(c,!0);this.sliced=this.options.sliced=H(a)?a:!this.sliced;d.options.data[r(this,d.data)]=this.options;this.graphic.animate(this.getTranslate());
this.shadowGroup&&this.shadowGroup.animate(this.getTranslate())},getTranslate:function(){return this.sliced?this.slicedTranslation:{translateX:0,translateY:0}},haloPath:function(a){var c=this.shapeArgs;return this.sliced||!this.visible?[]:this.series.chart.renderer.symbols.arc(c.x,c.y,c.r+a,c.r+a,{innerR:this.shapeArgs.r-1,start:c.start,end:c.end})}})})(M);(function(a){var E=a.addEvent,D=a.arrayMax,H=a.defined,p=a.each,f=a.extend,l=a.format,r=a.map,n=a.merge,w=a.noop,u=a.pick,e=a.relativeLength,h=
a.Series,m=a.seriesTypes,d=a.stableSort;a.distribute=function(a,b){function c(a,b){return a.target-b.target}var e,f=!0,h=a,l=[],m;m=0;for(e=a.length;e--;)m+=a[e].size;if(m>b){d(a,function(a,b){return(b.rank||0)-(a.rank||0)});for(m=e=0;m<=b;)m+=a[e].size,e++;l=a.splice(e-1,a.length)}d(a,c);for(a=r(a,function(a){return{size:a.size,targets:[a.target],align:u(a.align,.5)}});f;){for(e=a.length;e--;)f=a[e],m=(Math.min.apply(0,f.targets)+Math.max.apply(0,f.targets))/2,f.pos=Math.min(Math.max(0,m-f.size*
f.align),b-f.size);e=a.length;for(f=!1;e--;)0<e&&a[e-1].pos+a[e-1].size>a[e].pos&&(a[e-1].size+=a[e].size,a[e-1].targets=a[e-1].targets.concat(a[e].targets),a[e-1].align=.5,a[e-1].pos+a[e-1].size>b&&(a[e-1].pos=b-a[e-1].size),a.splice(e,1),f=!0)}e=0;p(a,function(a){var b=0;p(a.targets,function(){h[e].pos=a.pos+b;b+=h[e].size;e++})});h.push.apply(h,l);d(h,c)};h.prototype.drawDataLabels=function(){function c(a,b){var c=b.filter;return c?(b=c.operator,a=a[c.property],c=c.value,"\x3e"===b&&a>c||"\x3c"===
b&&a<c||"\x3e\x3d"===b&&a>=c||"\x3c\x3d"===b&&a<=c||"\x3d\x3d"===b&&a==c||"\x3d\x3d\x3d"===b&&a===c?!0:!1):!0}var b=this,d=b.options,e=d.dataLabels,f=b.points,h,m,r=b.hasRendered||0,t,w,D=u(e.defer,!!d.animation),q=b.chart.renderer;if(e.enabled||b._hasPointLabels)b.dlProcessOptions&&b.dlProcessOptions(e),w=b.plotGroup("dataLabelsGroup","data-labels",D&&!r?"hidden":"visible",e.zIndex||6),D&&(w.attr({opacity:+r}),r||E(b,"afterAnimate",function(){b.visible&&w.show(!0);w[d.animation?"animate":"attr"]({opacity:1},
{duration:200})})),m=e,p(f,function(f){var k,p=f.dataLabel,g,x,r=f.connector,z=!p,C;h=f.dlOptions||f.options&&f.options.dataLabels;(k=u(h&&h.enabled,m.enabled)&&!f.isNull)&&(k=!0===c(f,h||e));k&&(e=n(m,h),g=f.getLabelConfig(),C=e[f.formatPrefix+"Format"]||e.format,t=H(C)?l(C,g):(e[f.formatPrefix+"Formatter"]||e.formatter).call(g,e),C=e.style,g=e.rotation,C.color=u(e.color,C.color,b.color,"#000000"),"contrast"===C.color&&(f.contrastColor=q.getContrast(f.color||b.color),C.color=e.inside||0>u(f.labelDistance,
e.distance)||d.stacking?f.contrastColor:"#000000"),d.cursor&&(C.cursor=d.cursor),x={fill:e.backgroundColor,stroke:e.borderColor,"stroke-width":e.borderWidth,r:e.borderRadius||0,rotation:g,padding:e.padding,zIndex:1},a.objectEach(x,function(a,b){void 0===a&&delete x[b]}));!p||k&&H(t)?k&&H(t)&&(p?x.text=t:(p=f.dataLabel=g?q.text(t,0,-9999).addClass("highcharts-data-label"):q.label(t,0,-9999,e.shape,null,null,e.useHTML,null,"data-label"),p.addClass(" highcharts-data-label-color-"+f.colorIndex+" "+(e.className||
"")+(e.useHTML?"highcharts-tracker":""))),p.attr(x),p.css(C).shadow(e.shadow),p.added||p.add(w),b.alignDataLabel(f,p,e,null,z)):(f.dataLabel=p=p.destroy(),r&&(f.connector=r.destroy()))})};h.prototype.alignDataLabel=function(a,b,d,e,h){var c=this.chart,k=c.inverted,l=u(a.dlBox&&a.dlBox.centerX,a.plotX,-9999),m=u(a.plotY,-9999),n=b.getBBox(),p,q=d.rotation,r=d.align,w=this.visible&&(a.series.forceDL||c.isInsidePlot(l,Math.round(m),k)||e&&c.isInsidePlot(l,k?e.x+1:e.y+e.height-1,k)),z="justify"===u(d.overflow,
"justify");if(w&&(p=d.style.fontSize,p=c.renderer.fontMetrics(p,b).b,e=f({x:k?this.yAxis.len-m:l,y:Math.round(k?this.xAxis.len-l:m),width:0,height:0},e),f(d,{width:n.width,height:n.height}),q?(z=!1,l=c.renderer.rotCorr(p,q),l={x:e.x+d.x+e.width/2+l.x,y:e.y+d.y+{top:0,middle:.5,bottom:1}[d.verticalAlign]*e.height},b[h?"attr":"animate"](l).attr({align:r}),m=(q+720)%360,m=180<m&&360>m,"left"===r?l.y-=m?n.height:0:"center"===r?(l.x-=n.width/2,l.y-=n.height/2):"right"===r&&(l.x-=n.width,l.y-=m?0:n.height)):
(b.align(d,null,e),l=b.alignAttr),z?a.isLabelJustified=this.justifyDataLabel(b,d,l,n,e,h):u(d.crop,!0)&&(w=c.isInsidePlot(l.x,l.y)&&c.isInsidePlot(l.x+n.width,l.y+n.height)),d.shape&&!q))b[h?"attr":"animate"]({anchorX:k?c.plotWidth-a.plotY:a.plotX,anchorY:k?c.plotHeight-a.plotX:a.plotY});w||(b.attr({y:-9999}),b.placed=!1)};h.prototype.justifyDataLabel=function(a,b,d,e,f,h){var c=this.chart,k=b.align,l=b.verticalAlign,m,n,p=a.box?0:a.padding||0;m=d.x+p;0>m&&("right"===k?b.align="left":b.x=-m,n=!0);
m=d.x+e.width-p;m>c.plotWidth&&("left"===k?b.align="right":b.x=c.plotWidth-m,n=!0);m=d.y+p;0>m&&("bottom"===l?b.verticalAlign="top":b.y=-m,n=!0);m=d.y+e.height-p;m>c.plotHeight&&("top"===l?b.verticalAlign="bottom":b.y=c.plotHeight-m,n=!0);n&&(a.placed=!h,a.align(b,null,f));return n};m.pie&&(m.pie.prototype.drawDataLabels=function(){var c=this,b=c.data,d,e=c.chart,f=c.options.dataLabels,l=u(f.connectorPadding,10),m=u(f.connectorWidth,1),n=e.plotWidth,t=e.plotHeight,r,w=c.center,q=w[2]/2,A=w[1],F,G,
g,v,E=[[],[]],L,P,J,M,y=[0,0,0,0];c.visible&&(f.enabled||c._hasPointLabels)&&(p(b,function(a){a.dataLabel&&a.visible&&a.dataLabel.shortened&&(a.dataLabel.attr({width:"auto"}).css({width:"auto",textOverflow:"clip"}),a.dataLabel.shortened=!1)}),h.prototype.drawDataLabels.apply(c),p(b,function(a){a.dataLabel&&a.visible&&(E[a.half].push(a),a.dataLabel._pos=null)}),p(E,function(b,h){var k,m,x=b.length,r=[],z;if(x)for(c.sortByAngle(b,h-.5),0<c.maxLabelDistance&&(k=Math.max(0,A-q-c.maxLabelDistance),m=Math.min(A+
q+c.maxLabelDistance,e.plotHeight),p(b,function(a){0<a.labelDistance&&a.dataLabel&&(a.top=Math.max(0,A-q-a.labelDistance),a.bottom=Math.min(A+q+a.labelDistance,e.plotHeight),z=a.dataLabel.getBBox().height||21,a.positionsIndex=r.push({target:a.labelPos[1]-a.top+z/2,size:z,rank:a.y})-1)}),a.distribute(r,m+z-k)),M=0;M<x;M++)d=b[M],m=d.positionsIndex,g=d.labelPos,F=d.dataLabel,J=!1===d.visible?"hidden":"inherit",P=k=g[1],r&&H(r[m])&&(void 0===r[m].pos?J="hidden":(v=r[m].size,P=d.top+r[m].pos)),delete d.positionIndex,
L=f.justify?w[0]+(h?-1:1)*(q+d.labelDistance):c.getX(P<d.top+2||P>d.bottom-2?k:P,h,d),F._attr={visibility:J,align:g[6]},F._pos={x:L+f.x+({left:l,right:-l}[g[6]]||0),y:P+f.y-10},g.x=L,g.y=P,u(f.crop,!0)&&(G=F.getBBox().width,k=null,L-G<l?(k=Math.round(G-L+l),y[3]=Math.max(k,y[3])):L+G>n-l&&(k=Math.round(L+G-n+l),y[1]=Math.max(k,y[1])),0>P-v/2?y[0]=Math.max(Math.round(-P+v/2),y[0]):P+v/2>t&&(y[2]=Math.max(Math.round(P+v/2-t),y[2])),F.sideOverflow=k)}),0===D(y)||this.verifyDataLabelOverflow(y))&&(this.placeDataLabels(),
m&&p(this.points,function(a){var b;r=a.connector;if((F=a.dataLabel)&&F._pos&&a.visible&&0<a.labelDistance){J=F._attr.visibility;if(b=!r)a.connector=r=e.renderer.path().addClass("highcharts-data-label-connector  highcharts-color-"+a.colorIndex).add(c.dataLabelsGroup),r.attr({"stroke-width":m,stroke:f.connectorColor||a.color||"#666666"});r[b?"attr":"animate"]({d:c.connectorPath(a.labelPos)});r.attr("visibility",J)}else r&&(a.connector=r.destroy())}))},m.pie.prototype.connectorPath=function(a){var b=
a.x,c=a.y;return u(this.options.dataLabels.softConnector,!0)?["M",b+("left"===a[6]?5:-5),c,"C",b,c,2*a[2]-a[4],2*a[3]-a[5],a[2],a[3],"L",a[4],a[5]]:["M",b+("left"===a[6]?5:-5),c,"L",a[2],a[3],"L",a[4],a[5]]},m.pie.prototype.placeDataLabels=function(){p(this.points,function(a){var b=a.dataLabel;b&&a.visible&&((a=b._pos)?(b.sideOverflow&&(b._attr.width=b.getBBox().width-b.sideOverflow,b.css({width:b._attr.width+"px",textOverflow:"ellipsis"}),b.shortened=!0),b.attr(b._attr),b[b.moved?"animate":"attr"](a),
b.moved=!0):b&&b.attr({y:-9999}))},this)},m.pie.prototype.alignDataLabel=w,m.pie.prototype.verifyDataLabelOverflow=function(a){var b=this.center,c=this.options,d=c.center,f=c.minSize||80,h,l=null!==c.size;l||(null!==d[0]?h=Math.max(b[2]-Math.max(a[1],a[3]),f):(h=Math.max(b[2]-a[1]-a[3],f),b[0]+=(a[3]-a[1])/2),null!==d[1]?h=Math.max(Math.min(h,b[2]-Math.max(a[0],a[2])),f):(h=Math.max(Math.min(h,b[2]-a[0]-a[2]),f),b[1]+=(a[0]-a[2])/2),h<b[2]?(b[2]=h,b[3]=Math.min(e(c.innerSize||0,h),h),this.translate(b),
this.drawDataLabels&&this.drawDataLabels()):l=!0);return l});m.column&&(m.column.prototype.alignDataLabel=function(a,b,d,e,f){var c=this.chart.inverted,k=a.series,l=a.dlBox||a.shapeArgs,m=u(a.below,a.plotY>u(this.translatedThreshold,k.yAxis.len)),p=u(d.inside,!!this.options.stacking);l&&(e=n(l),0>e.y&&(e.height+=e.y,e.y=0),l=e.y+e.height-k.yAxis.len,0<l&&(e.height-=l),c&&(e={x:k.yAxis.len-e.y-e.height,y:k.xAxis.len-e.x-e.width,width:e.height,height:e.width}),p||(c?(e.x+=m?0:e.width,e.width=0):(e.y+=
m?e.height:0,e.height=0)));d.align=u(d.align,!c||p?"center":m?"right":"left");d.verticalAlign=u(d.verticalAlign,c||p?"middle":m?"top":"bottom");h.prototype.alignDataLabel.call(this,a,b,d,e,f);a.isLabelJustified&&a.contrastColor&&a.dataLabel.css({color:a.contrastColor})})})(M);(function(a){var E=a.Chart,D=a.each,H=a.objectEach,p=a.pick;a=a.addEvent;a(E.prototype,"render",function(){var a=[];D(this.labelCollectors||[],function(f){a=a.concat(f())});D(this.yAxis||[],function(f){f.options.stackLabels&&
!f.options.stackLabels.allowOverlap&&H(f.stacks,function(f){H(f,function(f){a.push(f.label)})})});D(this.series||[],function(f){var l=f.options.dataLabels,n=f.dataLabelCollections||["dataLabel"];(l.enabled||f._hasPointLabels)&&!l.allowOverlap&&f.visible&&D(n,function(l){D(f.points,function(f){f[l]&&(f[l].labelrank=p(f.labelrank,f.shapeArgs&&f.shapeArgs.height),a.push(f[l]))})})});this.hideOverlappingLabels(a)});E.prototype.hideOverlappingLabels=function(a){var f=a.length,p,n,w,u,e,h,m,d,c,b=function(a,
b,c,d,e,f,h,l){return!(e>a+c||e+h<a||f>b+d||f+l<b)};for(n=0;n<f;n++)if(p=a[n])p.oldOpacity=p.opacity,p.newOpacity=1,p.width||(w=p.getBBox(),p.width=w.width,p.height=w.height);a.sort(function(a,b){return(b.labelrank||0)-(a.labelrank||0)});for(n=0;n<f;n++)for(w=a[n],p=n+1;p<f;++p)if(u=a[p],w&&u&&w!==u&&w.placed&&u.placed&&0!==w.newOpacity&&0!==u.newOpacity&&(e=w.alignAttr,h=u.alignAttr,m=w.parentGroup,d=u.parentGroup,c=2*(w.box?0:w.padding||0),e=b(e.x+m.translateX,e.y+m.translateY,w.width-c,w.height-
c,h.x+d.translateX,h.y+d.translateY,u.width-c,u.height-c)))(w.labelrank<u.labelrank?w:u).newOpacity=0;D(a,function(a){var b,c;a&&(c=a.newOpacity,a.oldOpacity!==c&&a.placed&&(c?a.show(!0):b=function(){a.hide()},a.alignAttr.opacity=c,a[a.isOld?"animate":"attr"](a.alignAttr,null,b)),a.isOld=!0)})}})(M);(function(a){var E=a.addEvent,D=a.Chart,H=a.createElement,p=a.css,f=a.defaultOptions,l=a.defaultPlotOptions,r=a.each,n=a.extend,w=a.fireEvent,u=a.hasTouch,e=a.inArray,h=a.isObject,m=a.Legend,d=a.merge,
c=a.pick,b=a.Point,k=a.Series,z=a.seriesTypes,B=a.svg,I;I=a.TrackerMixin={drawTrackerPoint:function(){var a=this,b=a.chart.pointer,c=function(a){var c=b.getPointFromEvent(a);void 0!==c&&(b.isDirectTouch=!0,c.onMouseOver(a))};r(a.points,function(a){a.graphic&&(a.graphic.element.point=a);a.dataLabel&&(a.dataLabel.div?a.dataLabel.div.point=a:a.dataLabel.element.point=a)});a._hasTracking||(r(a.trackerGroups,function(d){if(a[d]){a[d].addClass("highcharts-tracker").on("mouseover",c).on("mouseout",function(a){b.onTrackerMouseOut(a)});
if(u)a[d].on("touchstart",c);a.options.cursor&&a[d].css(p).css({cursor:a.options.cursor})}}),a._hasTracking=!0)},drawTrackerGraph:function(){var a=this,b=a.options,c=b.trackByArea,d=[].concat(c?a.areaPath:a.graphPath),e=d.length,f=a.chart,h=f.pointer,k=f.renderer,l=f.options.tooltip.snap,g=a.tracker,m,n=function(){if(f.hoverSeries!==a)a.onMouseOver()},p="rgba(192,192,192,"+(B?.0001:.002)+")";if(e&&!c)for(m=e+1;m--;)"M"===d[m]&&d.splice(m+1,0,d[m+1]-l,d[m+2],"L"),(m&&"M"===d[m]||m===e)&&d.splice(m,
0,"L",d[m-2]+l,d[m-1]);g?g.attr({d:d}):a.graph&&(a.tracker=k.path(d).attr({"stroke-linejoin":"round",visibility:a.visible?"visible":"hidden",stroke:p,fill:c?p:"none","stroke-width":a.graph.strokeWidth()+(c?0:2*l),zIndex:2}).add(a.group),r([a.tracker,a.markerGroup],function(a){a.addClass("highcharts-tracker").on("mouseover",n).on("mouseout",function(a){h.onTrackerMouseOut(a)});b.cursor&&a.css({cursor:b.cursor});if(u)a.on("touchstart",n)}))}};z.column&&(z.column.prototype.drawTracker=I.drawTrackerPoint);
z.pie&&(z.pie.prototype.drawTracker=I.drawTrackerPoint);z.scatter&&(z.scatter.prototype.drawTracker=I.drawTrackerPoint);n(m.prototype,{setItemEvents:function(a,c,e){var f=this,h=f.chart.renderer.boxWrapper,k="highcharts-legend-"+(a instanceof b?"point":"series")+"-active";(e?c:a.legendGroup).on("mouseover",function(){a.setState("hover");h.addClass(k);c.css(f.options.itemHoverStyle)}).on("mouseout",function(){c.css(d(a.visible?f.itemStyle:f.itemHiddenStyle));h.removeClass(k);a.setState()}).on("click",
function(b){var c=function(){a.setVisible&&a.setVisible()};h.removeClass(k);b={browserEvent:b};a.firePointEvent?a.firePointEvent("legendItemClick",b,c):w(a,"legendItemClick",b,c)})},createCheckboxForItem:function(a){a.checkbox=H("input",{type:"checkbox",checked:a.selected,defaultChecked:a.selected},this.options.itemCheckboxStyle,this.chart.container);E(a.checkbox,"click",function(b){w(a.series||a,"checkboxClick",{checked:b.target.checked,item:a},function(){a.select()})})}});f.legend.itemStyle.cursor=
"pointer";n(D.prototype,{showResetZoom:function(){var a=this,b=f.lang,c=a.options.chart.resetZoomButton,d=c.theme,e=d.states,h="chart"===c.relativeTo?null:"plotBox";this.resetZoomButton=a.renderer.button(b.resetZoom,null,null,function(){a.zoomOut()},d,e&&e.hover).attr({align:c.position.align,title:b.resetZoomTitle}).addClass("highcharts-reset-zoom").add().align(c.position,!1,h)},zoomOut:function(){var a=this;w(a,"selection",{resetSelection:!0},function(){a.zoom()})},zoom:function(a){var b,d=this.pointer,
e=!1,f;!a||a.resetSelection?(r(this.axes,function(a){b=a.zoom()}),d.initiated=!1):r(a.xAxis.concat(a.yAxis),function(a){var c=a.axis;d[c.isXAxis?"zoomX":"zoomY"]&&(b=c.zoom(a.min,a.max),c.displayBtn&&(e=!0))});f=this.resetZoomButton;e&&!f?this.showResetZoom():!e&&h(f)&&(this.resetZoomButton=f.destroy());b&&this.redraw(c(this.options.chart.animation,a&&a.animation,100>this.pointCount))},pan:function(a,b){var c=this,d=c.hoverPoints,e;d&&r(d,function(a){a.setState()});r("xy"===b?[1,0]:[1],function(b){b=
c[b?"xAxis":"yAxis"][0];var d=b.horiz,f=a[d?"chartX":"chartY"],d=d?"mouseDownX":"mouseDownY",h=c[d],g=(b.pointRange||0)/2,k=b.getExtremes(),l=b.toValue(h-f,!0)+g,m=b.toValue(h+b.len-f,!0)-g,n=m<l,h=n?m:l,l=n?l:m,m=Math.min(k.dataMin,g?k.min:b.toValue(b.toPixels(k.min)-b.minPixelPadding)),g=Math.max(k.dataMax,g?k.max:b.toValue(b.toPixels(k.max)+b.minPixelPadding)),n=m-h;0<n&&(l+=n,h=m);n=l-g;0<n&&(l=g,h-=n);b.series.length&&h!==k.min&&l!==k.max&&(b.setExtremes(h,l,!1,!1,{trigger:"pan"}),e=!0);c[d]=
f});e&&c.redraw(!1);p(c.container,{cursor:"move"})}});n(b.prototype,{select:function(a,b){var d=this,f=d.series,h=f.chart;a=c(a,!d.selected);d.firePointEvent(a?"select":"unselect",{accumulate:b},function(){d.selected=d.options.selected=a;f.options.data[e(d,f.data)]=d.options;d.setState(a&&"select");b||r(h.getSelectedPoints(),function(a){a.selected&&a!==d&&(a.selected=a.options.selected=!1,f.options.data[e(a,f.data)]=a.options,a.setState(""),a.firePointEvent("unselect"))})})},onMouseOver:function(a){var b=
this.series.chart,c=b.pointer;a=a?c.normalize(a):c.getChartCoordinatesFromPoint(this,b.inverted);c.runPointActions(a,this)},onMouseOut:function(){var a=this.series.chart;this.firePointEvent("mouseOut");r(a.hoverPoints||[],function(a){a.setState()});a.hoverPoints=a.hoverPoint=null},importEvents:function(){if(!this.hasImportedEvents){var b=this,c=d(b.series.options.point,b.options).events;b.events=c;a.objectEach(c,function(a,c){E(b,c,a)});this.hasImportedEvents=!0}},setState:function(a,b){var d=Math.floor(this.plotX),
e=this.plotY,f=this.series,h=f.options.states[a]||{},k=l[f.type].marker&&f.options.marker,m=k&&!1===k.enabled,p=k&&k.states&&k.states[a]||{},g=!1===p.enabled,r=f.stateMarkerGraphic,u=this.marker||{},w=f.chart,x=f.halo,z,B=k&&f.markerAttribs;a=a||"";if(!(a===this.state&&!b||this.selected&&"select"!==a||!1===h.enabled||a&&(g||m&&!1===p.enabled)||a&&u.states&&u.states[a]&&!1===u.states[a].enabled)){B&&(z=f.markerAttribs(this,a));if(this.graphic)this.state&&this.graphic.removeClass("highcharts-point-"+
this.state),a&&this.graphic.addClass("highcharts-point-"+a),this.graphic.animate(f.pointAttribs(this,a),c(w.options.chart.animation,h.animation)),z&&this.graphic.animate(z,c(w.options.chart.animation,p.animation,k.animation)),r&&r.hide();else{if(a&&p){k=u.symbol||f.symbol;r&&r.currentSymbol!==k&&(r=r.destroy());if(r)r[b?"animate":"attr"]({x:z.x,y:z.y});else k&&(f.stateMarkerGraphic=r=w.renderer.symbol(k,z.x,z.y,z.width,z.height).add(f.markerGroup),r.currentSymbol=k);r&&r.attr(f.pointAttribs(this,
a))}r&&(r[a&&w.isInsidePlot(d,e,w.inverted)?"show":"hide"](),r.element.point=this)}(d=h.halo)&&d.size?(x||(f.halo=x=w.renderer.path().add((this.graphic||r).parentGroup)),x[b?"animate":"attr"]({d:this.haloPath(d.size)}),x.attr({"class":"highcharts-halo highcharts-color-"+c(this.colorIndex,f.colorIndex)}),x.point=this,x.attr(n({fill:this.color||f.color,"fill-opacity":d.opacity,zIndex:-1},d.attributes))):x&&x.point&&x.point.haloPath&&x.animate({d:x.point.haloPath(0)});this.state=a}},haloPath:function(a){return this.series.chart.renderer.symbols.circle(Math.floor(this.plotX)-
a,this.plotY-a,2*a,2*a)}});n(k.prototype,{onMouseOver:function(){var a=this.chart,b=a.hoverSeries;if(b&&b!==this)b.onMouseOut();this.options.events.mouseOver&&w(this,"mouseOver");this.setState("hover");a.hoverSeries=this},onMouseOut:function(){var a=this.options,b=this.chart,c=b.tooltip,d=b.hoverPoint;b.hoverSeries=null;if(d)d.onMouseOut();this&&a.events.mouseOut&&w(this,"mouseOut");!c||this.stickyTracking||c.shared&&!this.noSharedTooltip||c.hide();this.setState()},setState:function(a){var b=this,
d=b.options,e=b.graph,f=d.states,h=d.lineWidth,d=0;a=a||"";if(b.state!==a&&(r([b.group,b.markerGroup,b.dataLabelsGroup],function(c){c&&(b.state&&c.removeClass("highcharts-series-"+b.state),a&&c.addClass("highcharts-series-"+a))}),b.state=a,!f[a]||!1!==f[a].enabled)&&(a&&(h=f[a].lineWidth||h+(f[a].lineWidthPlus||0)),e&&!e.dashstyle))for(h={"stroke-width":h},e.animate(h,c(b.chart.options.chart.animation,f[a]&&f[a].animation));b["zone-graph-"+d];)b["zone-graph-"+d].attr(h),d+=1},setVisible:function(a,
b){var c=this,d=c.chart,e=c.legendItem,f,h=d.options.chart.ignoreHiddenSeries,k=c.visible;f=(c.visible=a=c.options.visible=c.userOptions.visible=void 0===a?!k:a)?"show":"hide";r(["group","dataLabelsGroup","markerGroup","tracker","tt"],function(a){if(c[a])c[a][f]()});if(d.hoverSeries===c||(d.hoverPoint&&d.hoverPoint.series)===c)c.onMouseOut();e&&d.legend.colorizeItem(c,a);c.isDirty=!0;c.options.stacking&&r(d.series,function(a){a.options.stacking&&a.visible&&(a.isDirty=!0)});r(c.linkedSeries,function(b){b.setVisible(a,
!1)});h&&(d.isDirtyBox=!0);!1!==b&&d.redraw();w(c,f)},show:function(){this.setVisible(!0)},hide:function(){this.setVisible(!1)},select:function(a){this.selected=a=void 0===a?!this.selected:a;this.checkbox&&(this.checkbox.checked=a);w(this,a?"select":"unselect")},drawTracker:I.drawTrackerGraph})})(M);(function(a){var E=a.Chart,D=a.each,H=a.inArray,p=a.isArray,f=a.isObject,l=a.pick,r=a.splat;E.prototype.setResponsive=function(f){var l=this.options.responsive,n=[],e=this.currentResponsive;l&&l.rules&&
D(l.rules,function(e){void 0===e._id&&(e._id=a.uniqueKey());this.matchResponsiveRule(e,n,f)},this);var h=a.merge.apply(0,a.map(n,function(e){return a.find(l.rules,function(a){return a._id===e}).chartOptions})),n=n.toString()||void 0;n!==(e&&e.ruleIds)&&(e&&this.update(e.undoOptions,f),n?(this.currentResponsive={ruleIds:n,mergedOptions:h,undoOptions:this.currentOptions(h)},this.update(h,f)):this.currentResponsive=void 0)};E.prototype.matchResponsiveRule=function(a,f){var n=a.condition;(n.callback||
function(){return this.chartWidth<=l(n.maxWidth,Number.MAX_VALUE)&&this.chartHeight<=l(n.maxHeight,Number.MAX_VALUE)&&this.chartWidth>=l(n.minWidth,0)&&this.chartHeight>=l(n.minHeight,0)}).call(this)&&f.push(a._id)};E.prototype.currentOptions=function(l){function n(e,h,l,d){var c;a.objectEach(e,function(a,e){if(!d&&-1<H(e,["series","xAxis","yAxis"]))for(a=r(a),l[e]=[],c=0;c<a.length;c++)h[e][c]&&(l[e][c]={},n(a[c],h[e][c],l[e][c],d+1));else f(a)?(l[e]=p(a)?[]:{},n(a,h[e]||{},l[e],d+1)):l[e]=h[e]||
null})}var u={};n(l,this.options,u,0);return u}})(M);return M});


/***/ }),

/***/ "../../../../highcharts/modules/exporting.src.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/**
 * @license Highcharts JS v6.0.4 (2017-12-15)
 * Exporting module
 *
 * (c) 2010-2017 Torstein Honsi
 *
 * License: www.highcharts.com/license
 */

(function(factory) {
    if (typeof module === 'object' && module.exports) {
        module.exports = factory;
    } else {
        factory(Highcharts);
    }
}(function(Highcharts) {
    (function(H) {
        /**
         * Exporting module
         *
         * (c) 2010-2017 Torstein Honsi
         *
         * License: www.highcharts.com/license
         */

        /* eslint indent:0 */

        // create shortcuts
        var defaultOptions = H.defaultOptions,
            doc = H.doc,
            Chart = H.Chart,
            addEvent = H.addEvent,
            removeEvent = H.removeEvent,
            fireEvent = H.fireEvent,
            createElement = H.createElement,
            discardElement = H.discardElement,
            css = H.css,
            merge = H.merge,
            pick = H.pick,
            each = H.each,
            objectEach = H.objectEach,
            extend = H.extend,
            isTouchDevice = H.isTouchDevice,
            win = H.win,
            userAgent = win.navigator.userAgent,
            SVGRenderer = H.SVGRenderer,
            symbols = H.Renderer.prototype.symbols,
            isMSBrowser = /Edge\/|Trident\/|MSIE /.test(userAgent),
            isFirefoxBrowser = /firefox/i.test(userAgent);

        // Add language
        extend(defaultOptions.lang, {
            /**
             * Exporting module only. The text for the menu item to print the chart.
             * 
             * @type {String}
             * @default Print chart
             * @since 3.0.1
             * @apioption lang.printChart
             */
            printChart: 'Print chart',
            /**
             * Exporting module only. The text for the PNG download menu item.
             * 
             * @type {String}
             * @default Download PNG image
             * @since 2.0
             * @apioption lang.downloadPNG
             */
            downloadPNG: 'Download PNG image',
            /**
             * Exporting module only. The text for the JPEG download menu item.
             * 
             * @type {String}
             * @default Download JPEG image
             * @since 2.0
             * @apioption lang.downloadJPEG
             */
            downloadJPEG: 'Download JPEG image',
            /**
             * Exporting module only. The text for the PDF download menu item.
             * 
             * @type {String}
             * @default Download PDF document
             * @since 2.0
             * @apioption lang.downloadPDF
             */
            downloadPDF: 'Download PDF document',
            /**
             * Exporting module only. The text for the SVG download menu item.
             * 
             * @type {String}
             * @default Download SVG vector image
             * @since 2.0
             * @apioption lang.downloadSVG
             */
            downloadSVG: 'Download SVG vector image',
            /**
             * Exporting module menu. The tooltip title for the context menu holding
             * print and export menu items.
             * 
             * @type {String}
             * @default Chart context menu
             * @since 3.0
             * @apioption lang.contextButtonTitle
             */
            contextButtonTitle: 'Chart context menu'
        });

        // Buttons and menus are collected in a separate config option set called 'navigation'.
        // This can be extended later to add control buttons like zoom and pan right click menus.
        defaultOptions.navigation = {
            buttonOptions: {
                theme: {},

                /**
                 * Whether to enable buttons.
                 * 
                 * @type {Boolean}
                 * @sample {highcharts} highcharts/navigation/buttonoptions-enabled/ Exporting module loaded but buttons disabled
                 * @sample {highstock} highcharts/navigation/buttonoptions-enabled/ Exporting module loaded but buttons disabled
                 * @sample {highmaps} highcharts/navigation/buttonoptions-enabled/ Exporting module loaded but buttons disabled
                 * @default true
                 * @since 2.0
                 * @apioption navigation.buttonOptions.enabled
                 */

                /**
                 * The pixel size of the symbol on the button.
                 * 
                 * @type {Number}
                 * @sample {highcharts} highcharts/navigation/buttonoptions-height/ Bigger buttons
                 * @sample {highstock} highcharts/navigation/buttonoptions-height/ Bigger buttons
                 * @sample {highmaps} highcharts/navigation/buttonoptions-height/ Bigger buttons
                 * @default 14
                 * @since 2.0
                 * @apioption navigation.buttonOptions.symbolSize
                 */
                symbolSize: 14,

                /**
                 * The x position of the center of the symbol inside the button.
                 * 
                 * @type {Number}
                 * @sample {highcharts} highcharts/navigation/buttonoptions-height/ Bigger buttons
                 * @sample {highstock} highcharts/navigation/buttonoptions-height/ Bigger buttons
                 * @sample {highmaps} highcharts/navigation/buttonoptions-height/ Bigger buttons
                 * @default 12.5
                 * @since 2.0
                 * @apioption navigation.buttonOptions.symbolX
                 */
                symbolX: 12.5,

                /**
                 * The y position of the center of the symbol inside the button.
                 * 
                 * @type {Number}
                 * @sample {highcharts} highcharts/navigation/buttonoptions-height/ Bigger buttons
                 * @sample {highstock} highcharts/navigation/buttonoptions-height/ Bigger buttons
                 * @sample {highmaps} highcharts/navigation/buttonoptions-height/ Bigger buttons
                 * @default 10.5
                 * @since 2.0
                 * @apioption navigation.buttonOptions.symbolY
                 */
                symbolY: 10.5,

                /**
                 * Alignment for the buttons.
                 * 
                 * @validvalue ["left", "center", "right"]
                 * @type {String}
                 * @sample {highcharts} highcharts/navigation/buttonoptions-align/ Center aligned
                 * @sample {highstock} highcharts/navigation/buttonoptions-align/ Center aligned
                 * @sample {highmaps} highcharts/navigation/buttonoptions-align/ Center aligned
                 * @default right
                 * @since 2.0
                 * @apioption navigation.buttonOptions.align
                 */
                align: 'right',

                /**
                 * The pixel spacing between buttons.
                 * 
                 * @type {Number}
                 * @default 3
                 * @since 2.0
                 * @apioption navigation.buttonOptions.buttonSpacing
                 */
                buttonSpacing: 3,

                /**
                 * Pixel height of the buttons.
                 * 
                 * @type {Number}
                 * @sample {highcharts} highcharts/navigation/buttonoptions-height/ Bigger buttons
                 * @sample {highstock} highcharts/navigation/buttonoptions-height/ Bigger buttons
                 * @sample {highmaps} highcharts/navigation/buttonoptions-height/ Bigger buttons
                 * @default 22
                 * @since 2.0
                 * @apioption navigation.buttonOptions.height
                 */
                height: 22,

                /**
                 * A text string to add to the individual button.
                 * 
                 * @type {String}
                 * @sample {highcharts} highcharts/exporting/buttons-text/ Full text button
                 * @sample {highcharts} highcharts/exporting/buttons-text-symbol/ Combined symbol and text
                 * @sample {highstock} highcharts/exporting/buttons-text/ Full text button
                 * @sample {highstock} highcharts/exporting/buttons-text-symbol/ Combined symbol and text
                 * @sample {highmaps} highcharts/exporting/buttons-text/ Full text button
                 * @sample {highmaps} highcharts/exporting/buttons-text-symbol/ Combined symbol and text
                 * @default null
                 * @since 3.0
                 * @apioption navigation.buttonOptions.text
                 */

                /**
                 * The vertical alignment of the buttons. Can be one of "top", "middle"
                 * or "bottom".
                 * 
                 * @validvalue ["top", "middle", "bottom"]
                 * @type {String}
                 * @sample {highcharts} highcharts/navigation/buttonoptions-verticalalign/ Buttons at lower right
                 * @sample {highstock} highcharts/navigation/buttonoptions-verticalalign/ Buttons at lower right
                 * @sample {highmaps} highcharts/navigation/buttonoptions-verticalalign/ Buttons at lower right
                 * @default top
                 * @since 2.0
                 * @apioption navigation.buttonOptions.verticalAlign
                 */
                verticalAlign: 'top',

                /**
                 * The pixel width of the button.
                 * 
                 * @type {Number}
                 * @sample {highcharts} highcharts/navigation/buttonoptions-height/ Bigger buttons
                 * @sample {highstock} highcharts/navigation/buttonoptions-height/ Bigger buttons
                 * @sample {highmaps} highcharts/navigation/buttonoptions-height/ Bigger buttons
                 * @default 24
                 * @since 2.0
                 * @apioption navigation.buttonOptions.width
                 */
                width: 24

                /**
                 * The vertical offset of the button's position relative to its
                 * `verticalAlign`.
                 * 
                 * @type {Number}
                 * @sample {highcharts} highcharts/navigation/buttonoptions-verticalalign/ Buttons at lower right
                 * @sample {highstock} highcharts/navigation/buttonoptions-verticalalign/ Buttons at lower right
                 * @sample {highmaps} highcharts/navigation/buttonoptions-verticalalign/ Buttons at lower right
                 * @default 0
                 * @since 2.0
                 * @apioption navigation.buttonOptions.y
                 */
            }
        };


        // Presentational attributes

        merge(true, defaultOptions.navigation,
            /**
             * A collection of options for buttons and menus appearing in the exporting module.
             * @type {Object}
             * @optionparent navigation
             */
            {

                /**
                 * CSS styles for the popup menu appearing by default when the export
                 * icon is clicked. This menu is rendered in HTML.
                 * 
                 * @type {CSSObject}
                 * @see In styled mode, the menu is styled with the `.highcharts-menu`
                 * class.
                 * @sample {highcharts} highcharts/navigation/menustyle/ Light gray menu background
                 * @sample {highstock} highcharts/navigation/menustyle/ Light gray menu background
                 * @sample {highmaps} highcharts/navigation/menustyle/ Light gray menu background
                 * @default { "border": "1px solid #999999", "background": "#ffffff", "padding": "5px 0" }
                 * @since 2.0
                 */
                menuStyle: {
                    border: '1px solid #999999',
                    background: '#ffffff',
                    padding: '5px 0'
                },

                /**
                 * CSS styles for the individual items within the popup menu appearing
                 * by default when the export icon is clicked. The menu items are rendered
                 * in HTML.
                 * 
                 * @type {CSSObject}
                 * @see In styled mode, the menu items are styled with the `.highcharts-menu-item` class.
                 * @sample {highcharts} highcharts/navigation/menuitemstyle/ Add a grey stripe to the left
                 * @sample {highstock} highcharts/navigation/menuitemstyle/ Add a grey stripe to the left
                 * @sample {highmaps} highcharts/navigation/menuitemstyle/ Add a grey stripe to the left
                 * @default { "padding": "0.5em 1em", "color": "#333333", "background": "none" }
                 * @since 2.0
                 */
                menuItemStyle: {
                    padding: '0.5em 1em',
                    background: 'none',
                    color: '#333333',
                    /**
                     * Defaults to `14px` on touch devices and `11px` on desktop.
                     * @type {String}
                     */
                    fontSize: isTouchDevice ? '14px' : '11px',
                    transition: 'background 250ms, color 250ms'
                },

                /**
                 * CSS styles for the hover state of the individual items within the
                 * popup menu appearing by default when the export icon is clicked.
                 *  The menu items are rendered in HTML.
                 * 
                 * @type {CSSObject}
                 * @see In styled mode, the menu items are styled with the `.highcharts-menu-item` class.
                 * @sample {highcharts} highcharts/navigation/menuitemhoverstyle/ Bold text on hover
                 * @sample {highstock} highcharts/navigation/menuitemhoverstyle/ Bold text on hover
                 * @sample {highmaps} highcharts/navigation/menuitemhoverstyle/ Bold text on hover
                 * @default { "background": "#335cad", "color": "#ffffff" }
                 * @since 2.0
                 */
                menuItemHoverStyle: {
                    background: '#335cad',
                    color: '#ffffff'
                },

                /**
                 * A collection of options for buttons appearing in the exporting module.
                 * 
                 * 
                 * In styled mode, the buttons are styled with the `.highcharts-contextbutton` and `.highcharts-button-symbol` class.
                 *
                 */
                buttonOptions: {

                    /**
                     * Fill color for the symbol within the button.
                     * 
                     * @type {Color}
                     * @sample {highcharts} highcharts/navigation/buttonoptions-symbolfill/ Blue symbol stroke for one of the buttons
                     * @sample {highstock} highcharts/navigation/buttonoptions-symbolfill/ Blue symbol stroke for one of the buttons
                     * @sample {highmaps} highcharts/navigation/buttonoptions-symbolfill/ Blue symbol stroke for one of the buttons
                     * @default #666666
                     * @since 2.0
                     */
                    symbolFill: '#666666',

                    /**
                     * The color of the symbol's stroke or line.
                     * 
                     * @type {Color}
                     * @sample {highcharts} highcharts/navigation/buttonoptions-symbolstroke/ Blue symbol stroke
                     * @sample {highstock} highcharts/navigation/buttonoptions-symbolstroke/ Blue symbol stroke
                     * @sample {highmaps} highcharts/navigation/buttonoptions-symbolstroke/ Blue symbol stroke
                     * @default #666666
                     * @since 2.0
                     */
                    symbolStroke: '#666666',

                    /**
                     * The pixel stroke width of the symbol on the button.
                     * 
                     * @type {Number}
                     * @sample {highcharts} highcharts/navigation/buttonoptions-height/ Bigger buttons
                     * @sample {highstock} highcharts/navigation/buttonoptions-height/ Bigger buttons
                     * @sample {highmaps} highcharts/navigation/buttonoptions-height/ Bigger buttons
                     * @default 1
                     * @since 2.0
                     */
                    symbolStrokeWidth: 3,

                    /**
                     * A configuration object for the button theme. The object accepts
                     * SVG properties like `stroke-width`, `stroke` and `fill`. Tri-state
                     * button styles are supported by the `states.hover` and `states.select`
                     * objects.
                     * 
                     * @type {Object}
                     * @sample {highcharts} highcharts/navigation/buttonoptions-theme/ Theming the buttons
                     * @sample {highstock} highcharts/navigation/buttonoptions-theme/ Theming the buttons
                     * @sample {highmaps} highcharts/navigation/buttonoptions-theme/ Theming the buttons
                     * @since 3.0
                     */
                    theme: {
                        /**
                         * The default fill exists only to capture hover events.
                         * @type {String}
                         */
                        fill: '#ffffff',
                        /**
                         * @type {String}
                         */
                        stroke: 'none',
                        /**
                         * @type {Number}
                         * @default 5
                         */
                        padding: 5
                    }
                }
            });



        // Add the export related options

        /**
         * Options for the exporting module. For an overview on the matter, see [the docs](http://www.highcharts.com/docs/export-module/export-module-overview).
         * @type {Object}
         * @optionparent exporting
         */
        defaultOptions.exporting = {

            /**
             * Experimental setting to allow HTML inside the chart (added through
             * the `useHTML` options), directly in the exported image. This allows
             * you to preserve complicated HTML structures like tables or bi-directional
             * text in exported charts.
             * 
             * Disclaimer: The HTML is rendered in a `foreignObject` tag in the
             * generated SVG. The official export server is based on PhantomJS,
             * which supports this, but other SVG clients, like Batik, does not
             * support it. This also applies to downloaded SVG that you want to
             * open in a desktop client.
             * 
             * @type {Boolean}
             * @default false
             * @since 4.1.8
             * @apioption exporting.allowHTML
             */

            /**
             * Additional chart options to be merged into an exported chart. For
             * example, a common use case is to add data labels to improve readability
             * of the exported chart, or to add a printer-friendly color scheme.
             * 
             * @type {Object}
             * @sample {highcharts} highcharts/exporting/chartoptions-data-labels/
             *         Added data labels
             * @sample {highstock} highcharts/exporting/chartoptions-data-labels/
             *         Added data labels
             * @default null
             * @apioption exporting.chartOptions
             */

            /**
             * Whether to enable the exporting module. Disabling the module will
             * hide the context button, but API methods will still be available.
             * 
             * @type {Boolean}
             * @sample {highcharts} highcharts/exporting/enabled-false/
             *         Exporting module is loaded but disabled
             * @sample {highstock} highcharts/exporting/enabled-false/
             *         Exporting module is loaded but disabled
             * @default true
             * @since 2.0
             * @apioption exporting.enabled
             */

            /**
             * Function to call if the offline-exporting module fails to export
             * a chart on the client side, and [fallbackToExportServer](#exporting.
             * fallbackToExportServer) is disabled. If left undefined, an exception
             * is thrown instead.
             * 
             * @type {Function}
             * @see [fallbackToExportServer](#exporting.fallbackToExportServer)
             * @default undefined
             * @since 5.0.0
             * @apioption exporting.error
             */

            /**
             * Whether or not to fall back to the export server if the offline-exporting
             * module is unable to export the chart on the client side.
             * 
             * @type {Boolean}
             * @default true
             * @since 4.1.8
             * @apioption exporting.fallbackToExportServer
             */

            /**
             * The filename, without extension, to use for the exported chart.
             * 
             * @type {String}
             * @sample {highcharts} highcharts/exporting/filename/ Custom file name
             * @sample {highstock} highcharts/exporting/filename/ Custom file name
             * @default chart
             * @since 2.0
             * @apioption exporting.filename
             */

            /**
             * An object containing additional attributes for the POST form that
             * sends the SVG to the export server. For example, a `target` can be
             * set to make sure the generated image is received in another frame,
             *  or a custom `enctype` or `encoding` can be set.
             * 
             * @type {Object}
             * @since 3.0.8
             * @apioption exporting.formAttributes
             */

            /**
             * Path where Highcharts will look for export module dependencies to
             * load on demand if they don't already exist on `window`. Should currently
             * point to location of [CanVG](https://github.com/canvg/canvg) library,
             * [RGBColor.js](https://github.com/canvg/canvg), [jsPDF](https://github.
             * com/yWorks/jsPDF) and [svg2pdf.js](https://github.com/yWorks/svg2pdf.
             * js), required for client side export in certain browsers.
             * 
             * @type {String}
             * @default https://code.highcharts.com/{version}/lib
             * @since 5.0.0
             * @apioption exporting.libURL
             */

            /**
             * Analogous to [sourceWidth](#exporting.sourceWidth).
             * 
             * @type {Number}
             * @since 3.0
             * @apioption exporting.sourceHeight
             */

            /**
             * The width of the original chart when exported, unless an explicit
             * [chart.width](#chart.width) is set. The width exported raster image
             * is then multiplied by [scale](#exporting.scale).
             * 
             * @type {Number}
             * @sample {highcharts} highcharts/exporting/sourcewidth/ Source size demo
             * @sample {highstock} highcharts/exporting/sourcewidth/ Source size demo
             * @sample {highmaps} maps/exporting/sourcewidth/ Source size demo
             * @since 3.0
             * @apioption exporting.sourceWidth
             */

            /**
             * The pixel width of charts exported to PNG or JPG. As of Highcharts
             * 3.0, the default pixel width is a function of the [chart.width](#chart.
             * width) or [exporting.sourceWidth](#exporting.sourceWidth) and the
             * [exporting.scale](#exporting.scale).
             * 
             * @type {Number}
             * @sample {highcharts} highcharts/exporting/width/
             *         Export to 200px wide images
             * @sample {highstock} highcharts/exporting/width/
             *         Export to 200px wide images
             * @default undefined
             * @since 2.0
             * @apioption exporting.width
             */

            /**
             * Default MIME type for exporting if `chart.exportChart()` is called
             * without specifying a `type` option. Possible values are `image/png`,
             *  `image/jpeg`, `application/pdf` and `image/svg+xml`.
             * 
             * @validvalue ["image/png", "image/jpeg", "application/pdf", "image/svg+xml"]
             * @since 2.0
             */
            type: 'image/png',

            /**
             * The URL for the server module converting the SVG string to an image
             * format. By default this points to Highchart's free web service.
             * 
             * @type {String}
             * @default https://export.highcharts.com
             * @since 2.0
             */
            url: 'https://export.highcharts.com/',
            /**
             * When printing the chart from the menu item in the burger menu, if
             * the on-screen chart exceeds this width, it is resized. After printing
             * or cancelled, it is restored. The default width makes the chart
             * fit into typical paper format. Note that this does not affect the
             * chart when printing the web page as a whole.
             * 
             * @type {Number}
             * @default 780
             * @since 4.2.5
             */
            printMaxWidth: 780,

            /**
             * Defines the scale or zoom factor for the exported image compared
             * to the on-screen display. While for instance a 600px wide chart
             * may look good on a website, it will look bad in print. The default
             * scale of 2 makes this chart export to a 1200px PNG or JPG.
             * 
             * @see [chart.width](#chart.width), [exporting.sourceWidth](#exporting.
             * sourceWidth)
             * @sample {highcharts} highcharts/exporting/scale/ Scale demonstrated
             * @sample {highstock} highcharts/exporting/scale/ Scale demonstrated
             * @sample {highmaps} maps/exporting/scale/ Scale demonstrated
             * @since 3.0
             */
            scale: 2,

            /**
             * Options for the export related buttons, print and export. In addition
             * to the default buttons listed here, custom buttons can be added.
             * See [navigation.buttonOptions](#navigation.buttonOptions) for general
             * options.
             *
             */
            buttons: {

                /**
                 * Options for the export button.
                 * 
                 * In styled mode, export button styles can be applied with the
                 * `.highcharts-contextbutton` class.
                 * 
                 * @extends navigation.buttonOptions
                 */
                contextButton: {

                    /**
                     * A click handler callback to use on the button directly instead of
                     * the popup menu.
                     * 
                     * @type {Function}
                     * @sample {highcharts} highcharts/exporting/buttons-contextbutton-onclick/ Skip the menu and export the chart directly
                     * @sample {highstock} highcharts/exporting/buttons-contextbutton-onclick/ Skip the menu and export the chart directly
                     * @sample {highmaps} highcharts/exporting/buttons-contextbutton-onclick/ Skip the menu and export the chart directly
                     * @since 2.0
                     * @apioption exporting.buttons.contextButton.onclick
                     */

                    /**
                     * See [navigation.buttonOptions.symbolFill](#navigation.buttonOptions.symbolFill).
                     * 
                     * @type {Color}
                     * @default #666666
                     * @since 2.0
                     * @apioption exporting.buttons.contextButton.symbolFill
                     */

                    /**
                     * The horizontal position of the button relative to the `align`
                     * option.
                     * 
                     * @type {Number}
                     * @default -10
                     * @since 2.0
                     * @apioption exporting.buttons.contextButton.x
                     */

                    /**
                     * The class name of the context button.
                     * @type {String}
                     */
                    className: 'highcharts-contextbutton',

                    /**
                     * The class name of the menu appearing from the button.
                     * @type {String}
                     */
                    menuClassName: 'highcharts-contextmenu',

                    /**
                     * The symbol for the button. Points to a definition function in
                     * the `Highcharts.Renderer.symbols` collection. The default `exportIcon`
                     * function is part of the exporting module.
                     * 
                     * @validvalue ["circle", "square", "diamond", "triangle", "triangle-down", "menu"]
                     * @type {String}
                     * @sample {highcharts} highcharts/exporting/buttons-contextbutton-symbol/ Use a circle for symbol
                     * @sample {highstock} highcharts/exporting/buttons-contextbutton-symbol/ Use a circle for symbol
                     * @sample {highmaps} highcharts/exporting/buttons-contextbutton-symbol/ Use a circle for symbol
                     * @default menu
                     * @since 2.0
                     */
                    symbol: 'menu',

                    /**
                     * The key to a [lang](#lang) option setting that is used for the
                     * button`s title tooltip. When the key is `contextButtonTitle`, it
                     * refers to [lang.contextButtonTitle](#lang.contextButtonTitle)
                     * that defaults to "Chart context menu".
                     * @type {String}
                     */
                    _titleKey: 'contextButtonTitle',

                    /**
                     * A collection of strings pointing to config options for the menu
                     * items. The config options are defined in the
                     * `menuItemDefinitions` option.
                     * 
                     * By default, there is the "Print" menu item plus one menu item
                     * for each of the available export types. 
                     *
                     * Defaults to 
                     * <pre>
                     * [
                     *	'printChart',
                     *	'separator',
                     *	'downloadPNG',
                     *	'downloadJPEG',
                     *	'downloadPDF',
                     *	'downloadSVG'
                     * ]
                     * </pre>
                     * 
                     * @type {Array<String>|Array<Object>}
                     * @sample {highcharts} highcharts/exporting/menuitemdefinitions/
                     *         Menu item definitions
                     * @sample {highstock} highcharts/exporting/menuitemdefinitions/
                     *         Menu item definitions
                     * @sample {highmaps} highcharts/exporting/menuitemdefinitions/
                     *         Menu item definitions
                     * @since 2.0
                     */
                    menuItems: [
                        'printChart',
                        'separator',
                        'downloadPNG',
                        'downloadJPEG',
                        'downloadPDF',
                        'downloadSVG'
                    ]
                }
            },
            /**
             * An object consisting of definitions for the menu items in the context
             * menu. Each key value pair has a `key` that is referenced in the
             * [menuItems](#exporting.buttons.contextButton.menuItems) setting,
             * and a `value`, which is an object with the following properties:
             * 
             * <dl>
             * 
             * <dt>onclick</dt>
             * 
             * <dd>The click handler for the menu item</dd>
             * 
             * <dt>text</dt>
             * 
             * <dd>The text for the menu item</dd>
             * 
             * <dt>textKey</dt>
             * 
             * <dd>If internationalization is required, the key to a language string</dd>
             * 
             * </dl>
             * 
             * @type {Object}
             * @sample {highcharts} highcharts/exporting/menuitemdefinitions/
             *         Menu item definitions
             * @sample {highstock} highcharts/exporting/menuitemdefinitions/
             *         Menu item definitions
             * @sample {highmaps} highcharts/exporting/menuitemdefinitions/
             *         Menu item definitions
             * @since 5.0.13
             */
            menuItemDefinitions: {

                /**
                 * @ignore
                 */
                printChart: {
                    textKey: 'printChart',
                    onclick: function() {
                        this.print();
                    }
                },

                /**
                 * @ignore
                 */
                separator: {
                    separator: true
                },

                /**
                 * @ignore
                 */
                downloadPNG: {
                    textKey: 'downloadPNG',
                    onclick: function() {
                        this.exportChart();
                    }
                },

                /**
                 * @ignore
                 */
                downloadJPEG: {
                    textKey: 'downloadJPEG',
                    onclick: function() {
                        this.exportChart({
                            type: 'image/jpeg'
                        });
                    }
                },

                /**
                 * @ignore
                 */
                downloadPDF: {
                    textKey: 'downloadPDF',
                    onclick: function() {
                        this.exportChart({
                            type: 'application/pdf'
                        });
                    }
                },

                /**
                 * @ignore
                 */
                downloadSVG: {
                    textKey: 'downloadSVG',
                    onclick: function() {
                        this.exportChart({
                            type: 'image/svg+xml'
                        });
                    }
                }
            }
        };

        /**
         * Fires after a chart is printed through the context menu item or the
         * `Chart.print` method. Requires the exporting module.
         * 
         * @type {Function}
         * @context Chart
         * @sample {highcharts} highcharts/chart/events-beforeprint-afterprint/
         *         Rescale the chart to print
         * @sample {highstock} highcharts/chart/events-beforeprint-afterprint/
         *         Rescale the chart to print
         * @sample {highmaps} highcharts/chart/events-beforeprint-afterprint/
         *         Rescale the chart to print
         * @since 4.1.0
         * @apioption chart.events.afterPrint
         */

        /**
         * Fires before a chart is printed through the context menu item or
         * the `Chart.print` method. Requires the exporting module.
         * 
         * @type {Function}
         * @context Chart
         * @sample {highcharts} highcharts/chart/events-beforeprint-afterprint/
         *         Rescale the chart to print
         * @sample {highstock} highcharts/chart/events-beforeprint-afterprint/
         *         Rescale the chart to print
         * @sample {highmaps} highcharts/chart/events-beforeprint-afterprint/
         *         Rescale the chart to print
         * @since 4.1.0
         * @apioption chart.events.beforePrint
         */


        // Add the H.post utility
        H.post = function(url, data, formAttributes) {
            // create the form
            var form = createElement('form', merge({
                method: 'post',
                action: url,
                enctype: 'multipart/form-data'
            }, formAttributes), {
                display: 'none'
            }, doc.body);

            // add the data
            objectEach(data, function(val, name) {
                createElement('input', {
                    type: 'hidden',
                    name: name,
                    value: val
                }, null, form);
            });

            // submit
            form.submit();

            // clean up
            discardElement(form);
        };

        extend(Chart.prototype, /** @lends Highcharts.Chart.prototype */ {

            /**
             * Exporting module only. A collection of fixes on the produced SVG to
             * account for expando properties, browser bugs, VML problems and other.
             * Returns a cleaned SVG.
             *
             * @private
             */
            sanitizeSVG: function(svg, options) {
                // Move HTML into a foreignObject
                if (options && options.exporting && options.exporting.allowHTML) {
                    var html = svg.match(/<\/svg>(.*?$)/);
                    if (html && html[1]) {
                        html = '<foreignObject x="0" y="0" ' +
                            'width="' + options.chart.width + '" ' +
                            'height="' + options.chart.height + '">' +
                            '<body xmlns="http://www.w3.org/1999/xhtml">' +
                            html[1] +
                            '</body>' +
                            '</foreignObject>';
                        svg = svg.replace('</svg>', html + '</svg>');
                    }
                }

                svg = svg
                    .replace(/zIndex="[^"]+"/g, '')
                    .replace(/isShadow="[^"]+"/g, '')
                    .replace(/symbolName="[^"]+"/g, '')
                    .replace(/jQuery[0-9]+="[^"]+"/g, '')
                    .replace(/url\(("|&quot;)(\S+)("|&quot;)\)/g, 'url($2)')
                    .replace(/url\([^#]+#/g, 'url(#')
                    .replace(/<svg /, '<svg xmlns:xlink="http://www.w3.org/1999/xlink" ')
                    .replace(/ (NS[0-9]+\:)?href=/g, ' xlink:href=') // #3567
                    .replace(/\n/, ' ')
                    // Any HTML added to the container after the SVG (#894)
                    .replace(/<\/svg>.*?$/, '</svg>')
                    // Batik doesn't support rgba fills and strokes (#3095)
                    .replace(/(fill|stroke)="rgba\(([ 0-9]+,[ 0-9]+,[ 0-9]+),([ 0-9\.]+)\)"/g, '$1="rgb($2)" $1-opacity="$3"')

                    // Replace HTML entities, issue #347
                    .replace(/&nbsp;/g, '\u00A0') // no-break space
                    .replace(/&shy;/g, '\u00AD'); // soft hyphen


                // Further sanitize for oldIE
                if (this.ieSanitizeSVG) {
                    svg = this.ieSanitizeSVG(svg);
                }


                return svg;
            },

            /**
             * Return the unfiltered innerHTML of the chart container. Used as hook for
             * plugins. In styled mode, it also takes care of inlining CSS style rules.
             *
             * @see  Chart#getSVG
             *
             * @returns {String}
             *          The unfiltered SVG of the chart.
             */
            getChartHTML: function() {

                return this.container.innerHTML;
            },

            /**
             * Return an SVG representation of the chart.
             *
             * @param  chartOptions {Options}
             *         Additional chart options for the generated SVG representation.
             *         For collections like `xAxis`, `yAxis` or `series`, the additional
             *         options is either merged in to the orininal item of the same
             *         `id`, or to the first item if a common id is not found.
             * @return {String}
             *         The SVG representation of the rendered chart.
             * @sample highcharts/members/chart-getsvg/
             *         View the SVG from a button
             */
            getSVG: function(chartOptions) {
                var chart = this,
                    chartCopy,
                    sandbox,
                    svg,
                    seriesOptions,
                    sourceWidth,
                    sourceHeight,
                    cssWidth,
                    cssHeight,
                    options = merge(chart.options, chartOptions); // copy the options and add extra options


                // create a sandbox where a new chart will be generated
                sandbox = createElement('div', null, {
                    position: 'absolute',
                    top: '-9999em',
                    width: chart.chartWidth + 'px',
                    height: chart.chartHeight + 'px'
                }, doc.body);

                // get the source size
                cssWidth = chart.renderTo.style.width;
                cssHeight = chart.renderTo.style.height;
                sourceWidth = options.exporting.sourceWidth ||
                    options.chart.width ||
                    (/px$/.test(cssWidth) && parseInt(cssWidth, 10)) ||
                    600;
                sourceHeight = options.exporting.sourceHeight ||
                    options.chart.height ||
                    (/px$/.test(cssHeight) && parseInt(cssHeight, 10)) ||
                    400;

                // override some options
                extend(options.chart, {
                    animation: false,
                    renderTo: sandbox,
                    forExport: true,
                    renderer: 'SVGRenderer',
                    width: sourceWidth,
                    height: sourceHeight
                });
                options.exporting.enabled = false; // hide buttons in print
                delete options.data; // #3004

                // prepare for replicating the chart
                options.series = [];
                each(chart.series, function(serie) {
                    seriesOptions = merge(serie.userOptions, { // #4912
                        animation: false, // turn off animation
                        enableMouseTracking: false,
                        showCheckbox: false,
                        visible: serie.visible
                    });

                    if (!seriesOptions.isInternal) { // used for the navigator series that has its own option set
                        options.series.push(seriesOptions);
                    }
                });

                // Assign an internal key to ensure a one-to-one mapping (#5924)
                each(chart.axes, function(axis) {
                    if (!axis.userOptions.internalKey) { // #6444
                        axis.userOptions.internalKey = H.uniqueKey();
                    }
                });

                // generate the chart copy
                chartCopy = new H.Chart(options, chart.callback);

                // Axis options and series options  (#2022, #3900, #5982)
                if (chartOptions) {
                    each(['xAxis', 'yAxis', 'series'], function(coll) {
                        var collOptions = {};
                        if (chartOptions[coll]) {
                            collOptions[coll] = chartOptions[coll];
                            chartCopy.update(collOptions);
                        }
                    });
                }

                // Reflect axis extremes in the export (#5924)
                each(chart.axes, function(axis) {
                    var axisCopy = H.find(chartCopy.axes, function(copy) {
                            return copy.options.internalKey ===
                                axis.userOptions.internalKey;
                        }),
                        extremes = axis.getExtremes(),
                        userMin = extremes.userMin,
                        userMax = extremes.userMax;

                    if (axisCopy && (userMin !== undefined || userMax !== undefined)) {
                        axisCopy.setExtremes(userMin, userMax, true, false);
                    }
                });

                // Get the SVG from the container's innerHTML
                svg = chartCopy.getChartHTML();

                svg = chart.sanitizeSVG(svg, options);

                // free up memory
                options = null;
                chartCopy.destroy();
                discardElement(sandbox);

                return svg;
            },

            getSVGForExport: function(options, chartOptions) {
                var chartExportingOptions = this.options.exporting;

                return this.getSVG(merge({
                        chart: {
                            borderRadius: 0
                        }
                    },
                    chartExportingOptions.chartOptions,
                    chartOptions, {
                        exporting: {
                            sourceWidth: (options && options.sourceWidth) || chartExportingOptions.sourceWidth,
                            sourceHeight: (options && options.sourceHeight) || chartExportingOptions.sourceHeight
                        }
                    }
                ));
            },

            /**
             * Exporting module required. Submit an SVG version of the chart to a server
             * along with some parameters for conversion.
             * @param  {Object} exportingOptions
             *         Exporting options in addition to those defined in {@link
             *         https://api.highcharts.com/highcharts/exporting|exporting}.
             * @param  {String} exportingOptions.filename
             *         The file name for the export without extension.
             * @param  {String} exportingOptions.url
             *         The URL for the server module to do the conversion.
             * @param  {Number} exportingOptions.width
             *         The width of the PNG or JPG image generated on the server.
             * @param  {String} exportingOptions.type
             *         The MIME type of the converted image.
             * @param  {Number} exportingOptions.sourceWidth
             *         The pixel width of the source (in-page) chart.
             * @param  {Number} exportingOptions.sourceHeight
             *         The pixel height of the source (in-page) chart.
             * @param  {Options} chartOptions
             *         Additional chart options for the exported chart. For example a
             *         different background color can be added here, or `dataLabels`
             *         for export only.
             *
             * @sample highcharts/members/chart-exportchart/
             *         Export with no options
             * @sample highcharts/members/chart-exportchart-filename/
             *         PDF type and custom filename
             * @sample highcharts/members/chart-exportchart-custom-background/
             *         Different chart background in export
             * @sample stock/members/chart-exportchart/
             *         Export with Highstock
             */
            exportChart: function(exportingOptions, chartOptions) {

                var svg = this.getSVGForExport(exportingOptions, chartOptions);

                // merge the options
                exportingOptions = merge(this.options.exporting, exportingOptions);

                // do the post
                H.post(exportingOptions.url, {
                    filename: exportingOptions.filename || 'chart',
                    type: exportingOptions.type,
                    width: exportingOptions.width || 0, // IE8 fails to post undefined correctly, so use 0
                    scale: exportingOptions.scale,
                    svg: svg
                }, exportingOptions.formAttributes);

            },

            /**
             * Exporting module required. Clears away other elements in the page and
             * prints the chart as it is displayed. By default, when the exporting
             * module is enabled, a context button with a drop down menu in the upper
             * right corner accesses this function.
             *
             * @sample highcharts/members/chart-print/
             *         Print from a HTML button
             */
            print: function() {

                var chart = this,
                    container = chart.container,
                    origDisplay = [],
                    origParent = container.parentNode,
                    body = doc.body,
                    childNodes = body.childNodes,
                    printMaxWidth = chart.options.exporting.printMaxWidth,
                    resetParams,
                    handleMaxWidth;

                if (chart.isPrinting) { // block the button while in printing mode
                    return;
                }

                chart.isPrinting = true;
                chart.pointer.reset(null, 0);

                fireEvent(chart, 'beforePrint');

                // Handle printMaxWidth
                handleMaxWidth = printMaxWidth && chart.chartWidth > printMaxWidth;
                if (handleMaxWidth) {
                    resetParams = [chart.options.chart.width, undefined, false];
                    chart.setSize(printMaxWidth, undefined, false);
                }

                // hide all body content
                each(childNodes, function(node, i) {
                    if (node.nodeType === 1) {
                        origDisplay[i] = node.style.display;
                        node.style.display = 'none';
                    }
                });

                // pull out the chart
                body.appendChild(container);

                // print
                win.focus(); // #1510
                win.print();

                // allow the browser to prepare before reverting
                setTimeout(function() {

                    // put the chart back in
                    origParent.appendChild(container);

                    // restore all body content
                    each(childNodes, function(node, i) {
                        if (node.nodeType === 1) {
                            node.style.display = origDisplay[i];
                        }
                    });

                    chart.isPrinting = false;

                    // Reset printMaxWidth
                    if (handleMaxWidth) {
                        chart.setSize.apply(chart, resetParams);
                    }

                    fireEvent(chart, 'afterPrint');

                }, 1000);

            },

            /**
             * Display a popup menu for choosing the export type.
             *
             * @private
             *
             * @param {String} className An identifier for the menu
             * @param {Array} items A collection with text and onclicks for the items
             * @param {Number} x The x position of the opener button
             * @param {Number} y The y position of the opener button
             * @param {Number} width The width of the opener button
             * @param {Number} height The height of the opener button
             */
            contextMenu: function(className, items, x, y, width, height, button) {
                var chart = this,
                    navOptions = chart.options.navigation,
                    chartWidth = chart.chartWidth,
                    chartHeight = chart.chartHeight,
                    cacheName = 'cache-' + className,
                    menu = chart[cacheName],
                    menuPadding = Math.max(width, height), // for mouse leave detection
                    innerMenu,
                    hide,
                    menuStyle;

                // create the menu only the first time
                if (!menu) {

                    // create a HTML element above the SVG
                    chart[cacheName] = menu = createElement('div', {
                        className: className
                    }, {
                        position: 'absolute',
                        zIndex: 1000,
                        padding: menuPadding + 'px'
                    }, chart.container);

                    innerMenu = createElement('div', {
                        className: 'highcharts-menu'
                    }, null, menu);


                    // Presentational CSS
                    css(innerMenu, extend({
                        MozBoxShadow: '3px 3px 10px #888',
                        WebkitBoxShadow: '3px 3px 10px #888',
                        boxShadow: '3px 3px 10px #888'
                    }, navOptions.menuStyle));


                    // hide on mouse out
                    hide = function() {
                        css(menu, {
                            display: 'none'
                        });
                        if (button) {
                            button.setState(0);
                        }
                        chart.openMenu = false;
                    };

                    // Hide the menu some time after mouse leave (#1357)
                    chart.exportEvents.push(
                        addEvent(menu, 'mouseleave', function() {
                            menu.hideTimer = setTimeout(hide, 500);
                        }),
                        addEvent(menu, 'mouseenter', function() {
                            clearTimeout(menu.hideTimer);
                        }),

                        // Hide it on clicking or touching outside the menu (#2258, #2335,
                        // #2407)
                        addEvent(doc, 'mouseup', function(e) {
                            if (!chart.pointer.inClass(e.target, className)) {
                                hide();
                            }
                        })
                    );

                    // create the items
                    each(items, function(item) {

                        if (typeof item === 'string') {
                            item = chart.options.exporting.menuItemDefinitions[item];
                        }

                        if (H.isObject(item, true)) {
                            var element;

                            if (item.separator) {
                                element = createElement('hr', null, null, innerMenu);

                            } else {
                                element = createElement('div', {
                                    className: 'highcharts-menu-item',
                                    onclick: function(e) {
                                        if (e) { // IE7
                                            e.stopPropagation();
                                        }
                                        hide();
                                        if (item.onclick) {
                                            item.onclick.apply(chart, arguments);
                                        }
                                    },
                                    innerHTML: item.text || chart.options.lang[item.textKey]
                                }, null, innerMenu);


                                element.onmouseover = function() {
                                    css(this, navOptions.menuItemHoverStyle);
                                };
                                element.onmouseout = function() {
                                    css(this, navOptions.menuItemStyle);
                                };
                                css(element, extend({
                                    cursor: 'pointer'
                                }, navOptions.menuItemStyle));

                            }

                            // Keep references to menu divs to be able to destroy them
                            chart.exportDivElements.push(element);
                        }
                    });

                    // Keep references to menu and innerMenu div to be able to destroy them
                    chart.exportDivElements.push(innerMenu, menu);

                    chart.exportMenuWidth = menu.offsetWidth;
                    chart.exportMenuHeight = menu.offsetHeight;
                }

                menuStyle = {
                    display: 'block'
                };

                // if outside right, right align it
                if (x + chart.exportMenuWidth > chartWidth) {
                    menuStyle.right = (chartWidth - x - width - menuPadding) + 'px';
                } else {
                    menuStyle.left = (x - menuPadding) + 'px';
                }
                // if outside bottom, bottom align it
                if (y + height + chart.exportMenuHeight > chartHeight && button.alignOptions.verticalAlign !== 'top') {
                    menuStyle.bottom = (chartHeight - y - menuPadding) + 'px';
                } else {
                    menuStyle.top = (y + height - menuPadding) + 'px';
                }

                css(menu, menuStyle);
                chart.openMenu = true;
            },

            /**
             * Add the export button to the chart, with options.
             *
             * @private
             */
            addButton: function(options) {
                var chart = this,
                    renderer = chart.renderer,
                    btnOptions = merge(chart.options.navigation.buttonOptions, options),
                    onclick = btnOptions.onclick,
                    menuItems = btnOptions.menuItems,
                    symbol,
                    button,
                    symbolSize = btnOptions.symbolSize || 12;
                if (!chart.btnCount) {
                    chart.btnCount = 0;
                }

                // Keeps references to the button elements
                if (!chart.exportDivElements) {
                    chart.exportDivElements = [];
                    chart.exportSVGElements = [];
                }

                if (btnOptions.enabled === false) {
                    return;
                }


                var attr = btnOptions.theme,
                    states = attr.states,
                    hover = states && states.hover,
                    select = states && states.select,
                    callback;

                delete attr.states;

                if (onclick) {
                    callback = function(e) {
                        e.stopPropagation();
                        onclick.call(chart, e);
                    };

                } else if (menuItems) {
                    callback = function() {
                        chart.contextMenu(
                            button.menuClassName,
                            menuItems,
                            button.translateX,
                            button.translateY,
                            button.width,
                            button.height,
                            button
                        );
                        button.setState(2);
                    };
                }


                if (btnOptions.text && btnOptions.symbol) {
                    attr.paddingLeft = pick(attr.paddingLeft, 25);

                } else if (!btnOptions.text) {
                    extend(attr, {
                        width: btnOptions.width,
                        height: btnOptions.height,
                        padding: 0
                    });
                }

                button = renderer.button(btnOptions.text, 0, 0, callback, attr, hover, select)
                    .addClass(options.className)
                    .attr({

                        'stroke-linecap': 'round',

                        title: chart.options.lang[btnOptions._titleKey],
                        zIndex: 3 // #4955
                    });
                button.menuClassName = options.menuClassName || 'highcharts-menu-' + chart.btnCount++;

                if (btnOptions.symbol) {
                    symbol = renderer.symbol(
                            btnOptions.symbol,
                            btnOptions.symbolX - (symbolSize / 2),
                            btnOptions.symbolY - (symbolSize / 2),
                            symbolSize,
                            symbolSize
                        )
                        .addClass('highcharts-button-symbol')
                        .attr({
                            zIndex: 1
                        }).add(button);


                    symbol.attr({
                        stroke: btnOptions.symbolStroke,
                        fill: btnOptions.symbolFill,
                        'stroke-width': btnOptions.symbolStrokeWidth || 1
                    });

                }

                button.add()
                    .align(extend(btnOptions, {
                        width: button.width,
                        x: pick(btnOptions.x, chart.buttonOffset) // #1654
                    }), true, 'spacingBox');

                chart.buttonOffset += (button.width + btnOptions.buttonSpacing) * (btnOptions.align === 'right' ? -1 : 1);

                chart.exportSVGElements.push(button, symbol);

            },

            /**
             * Destroy the export buttons.
             *
             * @private
             */
            destroyExport: function(e) {
                var chart = e ? e.target : this,
                    exportSVGElements = chart.exportSVGElements,
                    exportDivElements = chart.exportDivElements,
                    exportEvents = chart.exportEvents,
                    cacheName;

                // Destroy the extra buttons added
                if (exportSVGElements) {
                    each(exportSVGElements, function(elem, i) {

                        // Destroy and null the svg elements
                        if (elem) { // #1822
                            elem.onclick = elem.ontouchstart = null;
                            cacheName = 'cache-' + elem.menuClassName;

                            if (chart[cacheName]) {
                                delete chart[cacheName];
                            }

                            chart.exportSVGElements[i] = elem.destroy();
                        }
                    });
                    exportSVGElements.length = 0;
                }

                // Destroy the divs for the menu
                if (exportDivElements) {
                    each(exportDivElements, function(elem, i) {

                        // Remove the event handler
                        clearTimeout(elem.hideTimer); // #5427
                        removeEvent(elem, 'mouseleave');

                        // Remove inline events
                        chart.exportDivElements[i] = elem.onmouseout = elem.onmouseover = elem.ontouchstart = elem.onclick = null;

                        // Destroy the div by moving to garbage bin
                        discardElement(elem);
                    });
                    exportDivElements.length = 0;
                }

                if (exportEvents) {
                    each(exportEvents, function(unbind) {
                        unbind();
                    });
                    exportEvents.length = 0;
                }
            }
        });




        symbols.menu = function(x, y, width, height) {
            var arr = [
                'M', x, y + 2.5,
                'L', x + width, y + 2.5,
                'M', x, y + height / 2 + 0.5,
                'L', x + width, y + height / 2 + 0.5,
                'M', x, y + height - 1.5,
                'L', x + width, y + height - 1.5
            ];
            return arr;
        };

        // Add the buttons on chart load
        Chart.prototype.renderExporting = function() {
            var chart = this,
                exportingOptions = chart.options.exporting,
                buttons = exportingOptions.buttons,
                isDirty = chart.isDirtyExporting || !chart.exportSVGElements;

            chart.buttonOffset = 0;
            if (chart.isDirtyExporting) {
                chart.destroyExport();
            }

            if (isDirty && exportingOptions.enabled !== false) {
                chart.exportEvents = [];

                objectEach(buttons, function(button) {
                    chart.addButton(button);
                });

                chart.isDirtyExporting = false;
            }

            // Destroy the export elements at chart destroy
            addEvent(chart, 'destroy', chart.destroyExport);
        };

        Chart.prototype.callbacks.push(function(chart) {

            function update(prop, options, redraw) {
                chart.isDirtyExporting = true;
                merge(true, chart.options[prop], options);
                if (pick(redraw, true)) {
                    chart.redraw();
                }

            }

            chart.renderExporting();

            addEvent(chart, 'redraw', chart.renderExporting);

            // Add update methods to handle chart.update and chart.exporting.update
            // and chart.navigation.update.
            each(['exporting', 'navigation'], function(prop) {
                chart[prop] = {
                    update: function(options, redraw) {
                        update(prop, options, redraw);
                    }
                };
            });

            // Uncomment this to see a button directly below the chart, for quick
            // testing of export
            /*
            if (!chart.renderer.forExport) {
            	var button;

            	// View SVG Image
            	button = doc.createElement('button');
            	button.innerHTML = 'View SVG Image';
            	chart.renderTo.parentNode.appendChild(button);
            	button.onclick = function () {
            		var div = doc.createElement('div');
            		div.innerHTML = chart.getSVGForExport();
            		chart.renderTo.parentNode.appendChild(div);
            	};

            	// View SVG Source
            	button = doc.createElement('button');
            	button.innerHTML = 'View SVG Source';
            	chart.renderTo.parentNode.appendChild(button);
            	button.onclick = function () {
            		var pre = doc.createElement('pre');
            		pre.innerHTML = chart.getSVGForExport()
            			.replace(/</g, '\n&lt;')
            			.replace(/>/g, '&gt;');
            		chart.renderTo.parentNode.appendChild(pre);
            	};
            }
            // */
        });

    }(Highcharts));
}));


/***/ }),

/***/ "../../../../highcharts/modules/stock.src.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/**
 * @license Highcharts JS v6.0.4 (2017-12-15)
 * Highstock as a plugin for Highcharts
 *
 * (c) 2017 Torstein Honsi
 *
 * License: www.highcharts.com/license
 */

(function(factory) {
    if (typeof module === 'object' && module.exports) {
        module.exports = factory;
    } else {
        factory(Highcharts);
    }
}(function(Highcharts) {
    (function(H) {
        /**
         * (c) 2010-2017 Torstein Honsi
         *
         * License: www.highcharts.com/license
         */
        var addEvent = H.addEvent,
            Axis = H.Axis,
            Chart = H.Chart,
            css = H.css,
            dateFormat = H.dateFormat,
            defined = H.defined,
            each = H.each,
            extend = H.extend,
            noop = H.noop,
            pick = H.pick,
            Series = H.Series,
            timeUnits = H.timeUnits,
            wrap = H.wrap;

        /* ****************************************************************************
         * Start ordinal axis logic                                                   *
         *****************************************************************************/


        wrap(Series.prototype, 'init', function(proceed) {
            var series = this,
                xAxis;

            // call the original function
            proceed.apply(this, Array.prototype.slice.call(arguments, 1));

            xAxis = series.xAxis;

            // Destroy the extended ordinal index on updated data
            if (xAxis && xAxis.options.ordinal) {
                addEvent(series, 'updatedData', function() {
                    delete xAxis.ordinalIndex;
                });
            }
        });

        /**
         * In an ordinal axis, there might be areas with dense consentrations of points, then large
         * gaps between some. Creating equally distributed ticks over this entire range
         * may lead to a huge number of ticks that will later be removed. So instead, break the
         * positions up in segments, find the tick positions for each segment then concatenize them.
         * This method is used from both data grouping logic and X axis tick position logic.
         */
        wrap(Axis.prototype, 'getTimeTicks', function(proceed, normalizedInterval, min, max, startOfWeek, positions, closestDistance, findHigherRanks) {

            var start = 0,
                end,
                segmentPositions,
                higherRanks = {},
                hasCrossedHigherRank,
                info,
                posLength,
                outsideMax,
                groupPositions = [],
                lastGroupPosition = -Number.MAX_VALUE,
                tickPixelIntervalOption = this.options.tickPixelInterval;

            // The positions are not always defined, for example for ordinal positions when data
            // has regular interval (#1557, #2090)
            if ((!this.options.ordinal && !this.options.breaks) || !positions || positions.length < 3 || min === undefined) {
                return proceed.call(this, normalizedInterval, min, max, startOfWeek);
            }

            // Analyze the positions array to split it into segments on gaps larger than 5 times
            // the closest distance. The closest distance is already found at this point, so
            // we reuse that instead of computing it again.
            posLength = positions.length;

            for (end = 0; end < posLength; end++) {

                outsideMax = end && positions[end - 1] > max;

                if (positions[end] < min) { // Set the last position before min
                    start = end;
                }

                if (end === posLength - 1 || positions[end + 1] - positions[end] > closestDistance * 5 || outsideMax) {

                    // For each segment, calculate the tick positions from the getTimeTicks utility
                    // function. The interval will be the same regardless of how long the segment is.
                    if (positions[end] > lastGroupPosition) { // #1475

                        segmentPositions = proceed.call(this, normalizedInterval, positions[start], positions[end], startOfWeek);

                        // Prevent duplicate groups, for example for multiple segments within one larger time frame (#1475)
                        while (segmentPositions.length && segmentPositions[0] <= lastGroupPosition) {
                            segmentPositions.shift();
                        }
                        if (segmentPositions.length) {
                            lastGroupPosition = segmentPositions[segmentPositions.length - 1];
                        }

                        groupPositions = groupPositions.concat(segmentPositions);
                    }
                    // Set start of next segment
                    start = end + 1;
                }

                if (outsideMax) {
                    break;
                }
            }

            // Get the grouping info from the last of the segments. The info is the same for
            // all segments.
            info = segmentPositions.info;

            // Optionally identify ticks with higher rank, for example when the ticks
            // have crossed midnight.
            if (findHigherRanks && info.unitRange <= timeUnits.hour) {
                end = groupPositions.length - 1;

                // Compare points two by two
                for (start = 1; start < end; start++) {
                    if (dateFormat('%d', groupPositions[start]) !== dateFormat('%d', groupPositions[start - 1])) {
                        higherRanks[groupPositions[start]] = 'day';
                        hasCrossedHigherRank = true;
                    }
                }

                // If the complete array has crossed midnight, we want to mark the first
                // positions also as higher rank
                if (hasCrossedHigherRank) {
                    higherRanks[groupPositions[0]] = 'day';
                }
                info.higherRanks = higherRanks;
            }

            // Save the info
            groupPositions.info = info;



            // Don't show ticks within a gap in the ordinal axis, where the space between
            // two points is greater than a portion of the tick pixel interval
            if (findHigherRanks && defined(tickPixelIntervalOption)) { // check for squashed ticks

                var length = groupPositions.length,
                    i = length,
                    itemToRemove,
                    translated,
                    translatedArr = [],
                    lastTranslated,
                    medianDistance,
                    distance,
                    distances = [];

                // Find median pixel distance in order to keep a reasonably even distance between
                // ticks (#748)
                while (i--) {
                    translated = this.translate(groupPositions[i]);
                    if (lastTranslated) {
                        distances[i] = lastTranslated - translated;
                    }
                    translatedArr[i] = lastTranslated = translated;
                }
                distances.sort();
                medianDistance = distances[Math.floor(distances.length / 2)];
                if (medianDistance < tickPixelIntervalOption * 0.6) {
                    medianDistance = null;
                }

                // Now loop over again and remove ticks where needed
                i = groupPositions[length - 1] > max ? length - 1 : length; // #817
                lastTranslated = undefined;
                while (i--) {
                    translated = translatedArr[i];
                    distance = Math.abs(lastTranslated - translated);
                    // #4175 - when axis is reversed, the distance, is negative but 
                    // tickPixelIntervalOption positive, so we need to compare the same values

                    // Remove ticks that are closer than 0.6 times the pixel interval from the one to the right,
                    // but not if it is close to the median distance (#748).
                    if (lastTranslated && distance < tickPixelIntervalOption * 0.8 &&
                        (medianDistance === null || distance < medianDistance * 0.8)) {

                        // Is this a higher ranked position with a normal position to the right?
                        if (higherRanks[groupPositions[i]] && !higherRanks[groupPositions[i + 1]]) {

                            // Yes: remove the lower ranked neighbour to the right
                            itemToRemove = i + 1;
                            lastTranslated = translated; // #709

                        } else {

                            // No: remove this one
                            itemToRemove = i;
                        }

                        groupPositions.splice(itemToRemove, 1);

                    } else {
                        lastTranslated = translated;
                    }
                }
            }
            return groupPositions;
        });

        // Extend the Axis prototype
        extend(Axis.prototype, /** @lends Axis.prototype */ {

            /**
             * Calculate the ordinal positions before tick positions are calculated.
             */
            beforeSetTickPositions: function() {
                var axis = this,
                    len,
                    ordinalPositions = [],
                    useOrdinal = false,
                    dist,
                    extremes = axis.getExtremes(),
                    min = extremes.min,
                    max = extremes.max,
                    minIndex,
                    maxIndex,
                    slope,
                    hasBreaks = axis.isXAxis && !!axis.options.breaks,
                    isOrdinal = axis.options.ordinal,
                    overscrollPointsRange = Number.MAX_VALUE,
                    ignoreHiddenSeries = axis.chart.options.chart.ignoreHiddenSeries,
                    isNavigatorAxis = axis.options.className === 'highcharts-navigator-xaxis',
                    i;

                if (
                    axis.options.overscroll &&
                    axis.max === axis.dataMax &&
                    (
                        // Panning is an execption,
                        // We don't want to apply overscroll when panning over the dataMax
                        !axis.chart.mouseIsDown ||
                        isNavigatorAxis
                    ) && (
                        // Scrollbar buttons are the other execption:
                        !axis.eventArgs ||
                        axis.eventArgs && axis.eventArgs.trigger !== 'navigator'
                    )
                ) {
                    axis.max += axis.options.overscroll;

                    // Live data and buttons require translation for the min:
                    if (!isNavigatorAxis && defined(axis.userMin)) {
                        axis.min += axis.options.overscroll;
                    }
                }

                // Apply the ordinal logic
                if (isOrdinal || hasBreaks) { // #4167 YAxis is never ordinal ?

                    each(axis.series, function(series, i) {

                        if (
                            (!ignoreHiddenSeries || series.visible !== false) &&
                            (series.takeOrdinalPosition !== false || hasBreaks)
                        ) {

                            // concatenate the processed X data into the existing positions, or the empty array
                            ordinalPositions = ordinalPositions.concat(series.processedXData);
                            len = ordinalPositions.length;

                            // remove duplicates (#1588)
                            ordinalPositions.sort(function(a, b) {
                                return a - b; // without a custom function it is sorted as strings
                            });

                            overscrollPointsRange = Math.min(
                                overscrollPointsRange,
                                pick(
                                    // Check for a single-point series:
                                    series.closestPointRange,
                                    overscrollPointsRange
                                )
                            );

                            if (len) {
                                i = len - 1;
                                while (i--) {
                                    if (ordinalPositions[i] === ordinalPositions[i + 1]) {
                                        ordinalPositions.splice(i, 1);
                                    }
                                }
                            }
                        }

                    });

                    // cache the length
                    len = ordinalPositions.length;

                    // Check if we really need the overhead of mapping axis data against the ordinal positions.
                    // If the series consist of evenly spaced data any way, we don't need any ordinal logic.
                    if (len > 2) { // two points have equal distance by default
                        dist = ordinalPositions[1] - ordinalPositions[0];
                        i = len - 1;
                        while (i-- && !useOrdinal) {
                            if (ordinalPositions[i + 1] - ordinalPositions[i] !== dist) {
                                useOrdinal = true;
                            }
                        }

                        // When zooming in on a week, prevent axis padding for weekends even though the data within
                        // the week is evenly spaced.
                        if (!axis.options.keepOrdinalPadding &&
                            (
                                ordinalPositions[0] - min > dist ||
                                max - ordinalPositions[ordinalPositions.length - 1] > dist
                            )
                        ) {
                            useOrdinal = true;
                        }
                    } else if (axis.options.overscroll) {
                        if (len === 2) {
                            // Exactly two points, distance for overscroll is fixed:
                            overscrollPointsRange = ordinalPositions[1] - ordinalPositions[0];
                        } else if (len === 1) {
                            // We have just one point, closest distance is unknown.
                            // Assume then it is last point and overscrolled range:
                            overscrollPointsRange = axis.options.overscroll;
                            ordinalPositions = [ordinalPositions[0], ordinalPositions[0] + overscrollPointsRange];
                        } else {
                            // In case of zooming in on overscrolled range, stick to the old range:
                            overscrollPointsRange = axis.overscrollPointsRange;
                        }
                    }

                    // Record the slope and offset to compute the linear values from the array index.
                    // Since the ordinal positions may exceed the current range, get the start and
                    // end positions within it (#719, #665b)
                    if (useOrdinal) {

                        if (axis.options.overscroll) {
                            axis.overscrollPointsRange = overscrollPointsRange;
                            ordinalPositions = ordinalPositions.concat(axis.getOverscrollPositions());
                        }

                        // Register
                        axis.ordinalPositions = ordinalPositions;

                        // This relies on the ordinalPositions being set. Use Math.max
                        // and Math.min to prevent padding on either sides of the data.
                        minIndex = axis.ordinal2lin( // #5979
                            Math.max(
                                min,
                                ordinalPositions[0]
                            ),
                            true
                        );
                        maxIndex = Math.max(axis.ordinal2lin(
                            Math.min(
                                max,
                                ordinalPositions[ordinalPositions.length - 1]
                            ),
                            true
                        ), 1); // #3339

                        // Set the slope and offset of the values compared to the indices in the ordinal positions
                        axis.ordinalSlope = slope = (max - min) / (maxIndex - minIndex);
                        axis.ordinalOffset = min - (minIndex * slope);

                    } else {
                        axis.overscrollPointsRange = pick(axis.closestPointRange, axis.overscrollPointsRange);
                        axis.ordinalPositions = axis.ordinalSlope = axis.ordinalOffset = undefined;
                    }
                }

                axis.isOrdinal = isOrdinal && useOrdinal; // #3818, #4196, #4926
                axis.groupIntervalFactor = null; // reset for next run
            },
            /**
             * Translate from a linear axis value to the corresponding ordinal axis position. If there
             * are no gaps in the ordinal axis this will be the same. The translated value is the value
             * that the point would have if the axis were linear, using the same min and max.
             *
             * @param Number val The axis value
             * @param Boolean toIndex Whether to return the index in the ordinalPositions or the new value
             */
            val2lin: function(val, toIndex) {
                var axis = this,
                    ordinalPositions = axis.ordinalPositions,
                    ret;

                if (!ordinalPositions) {
                    ret = val;

                } else {

                    var ordinalLength = ordinalPositions.length,
                        i,
                        distance,
                        ordinalIndex;

                    // first look for an exact match in the ordinalpositions array
                    i = ordinalLength;
                    while (i--) {
                        if (ordinalPositions[i] === val) {
                            ordinalIndex = i;
                            break;
                        }
                    }

                    // if that failed, find the intermediate position between the two nearest values
                    i = ordinalLength - 1;
                    while (i--) {
                        if (val > ordinalPositions[i] || i === 0) { // interpolate
                            distance = (val - ordinalPositions[i]) / (ordinalPositions[i + 1] - ordinalPositions[i]); // something between 0 and 1
                            ordinalIndex = i + distance;
                            break;
                        }
                    }
                    ret = toIndex ?
                        ordinalIndex :
                        axis.ordinalSlope * (ordinalIndex || 0) + axis.ordinalOffset;
                }
                return ret;
            },
            /**
             * Translate from linear (internal) to axis value
             *
             * @param Number val The linear abstracted value
             * @param Boolean fromIndex Translate from an index in the ordinal positions rather than a value
             */
            lin2val: function(val, fromIndex) {
                var axis = this,
                    ordinalPositions = axis.ordinalPositions,
                    ret;

                if (!ordinalPositions) { // the visible range contains only equally spaced values
                    ret = val;

                } else {

                    var ordinalSlope = axis.ordinalSlope,
                        ordinalOffset = axis.ordinalOffset,
                        i = ordinalPositions.length - 1,
                        linearEquivalentLeft,
                        linearEquivalentRight,
                        distance;


                    // Handle the case where we translate from the index directly, used only
                    // when panning an ordinal axis
                    if (fromIndex) {

                        if (val < 0) { // out of range, in effect panning to the left
                            val = ordinalPositions[0];
                        } else if (val > i) { // out of range, panning to the right
                            val = ordinalPositions[i];
                        } else { // split it up
                            i = Math.floor(val);
                            distance = val - i; // the decimal
                        }

                        // Loop down along the ordinal positions. When the linear equivalent of i matches
                        // an ordinal position, interpolate between the left and right values.
                    } else {
                        while (i--) {
                            linearEquivalentLeft = (ordinalSlope * i) + ordinalOffset;
                            if (val >= linearEquivalentLeft) {
                                linearEquivalentRight = (ordinalSlope * (i + 1)) + ordinalOffset;
                                distance = (val - linearEquivalentLeft) / (linearEquivalentRight - linearEquivalentLeft); // something between 0 and 1
                                break;
                            }
                        }
                    }

                    // If the index is within the range of the ordinal positions, return the associated
                    // or interpolated value. If not, just return the value
                    return distance !== undefined && ordinalPositions[i] !== undefined ?
                        ordinalPositions[i] + (distance ? distance * (ordinalPositions[i + 1] - ordinalPositions[i]) : 0) :
                        val;
                }
                return ret;
            },
            /**
             * Get the ordinal positions for the entire data set. This is necessary in chart panning
             * because we need to find out what points or data groups are available outside the
             * visible range. When a panning operation starts, if an index for the given grouping
             * does not exists, it is created and cached. This index is deleted on updated data, so
             * it will be regenerated the next time a panning operation starts.
             */
            getExtendedPositions: function() {
                var axis = this,
                    chart = axis.chart,
                    grouping = axis.series[0].currentDataGrouping,
                    ordinalIndex = axis.ordinalIndex,
                    key = grouping ? grouping.count + grouping.unitName : 'raw',
                    overscroll = axis.options.overscroll,
                    extremes = axis.getExtremes(),
                    fakeAxis,
                    fakeSeries;

                // If this is the first time, or the ordinal index is deleted by updatedData,
                // create it.
                if (!ordinalIndex) {
                    ordinalIndex = axis.ordinalIndex = {};
                }


                if (!ordinalIndex[key]) {

                    // Create a fake axis object where the extended ordinal positions are emulated
                    fakeAxis = {
                        series: [],
                        chart: chart,
                        getExtremes: function() {
                            return {
                                min: extremes.dataMin,
                                max: extremes.dataMax + overscroll
                            };
                        },
                        options: {
                            ordinal: true
                        },
                        val2lin: Axis.prototype.val2lin, // #2590
                        ordinal2lin: Axis.prototype.ordinal2lin // #6276
                    };

                    // Add the fake series to hold the full data, then apply processData to it
                    each(axis.series, function(series) {
                        fakeSeries = {
                            xAxis: fakeAxis,
                            xData: series.xData.slice(),
                            chart: chart,
                            destroyGroupedData: noop
                        };

                        fakeSeries.xData = fakeSeries.xData.concat(axis.getOverscrollPositions());

                        fakeSeries.options = {
                            dataGrouping: grouping ? {
                                enabled: true,
                                forced: true,
                                approximation: 'open', // doesn't matter which, use the fastest
                                units: [
                                    [grouping.unitName, [grouping.count]]
                                ]
                            } : {
                                enabled: false
                            }
                        };
                        series.processData.apply(fakeSeries);


                        fakeAxis.series.push(fakeSeries);
                    });

                    // Run beforeSetTickPositions to compute the ordinalPositions
                    axis.beforeSetTickPositions.apply(fakeAxis);

                    // Cache it
                    ordinalIndex[key] = fakeAxis.ordinalPositions;
                }
                return ordinalIndex[key];
            },

            /**
             * Get ticks for an ordinal axis within a range where points don't exist.
             * It is required when overscroll is enabled. We can't base on points,
             * because we may not have any, so we use approximated pointRange and
             * generate these ticks between <Axis.dataMax, Axis.dataMax + Axis.overscroll>
             * evenly spaced. Used in panning and navigator scrolling.
             *
             * @returns positions {Array} Generated ticks
             * @private
             */
            getOverscrollPositions: function() {
                var axis = this,
                    extraRange = axis.options.overscroll,
                    distance = axis.overscrollPointsRange,
                    positions = [],
                    max = axis.dataMax;

                if (H.defined(distance)) {
                    // Max + pointRange because we need to scroll to the last

                    positions.push(max);

                    while (max <= axis.dataMax + extraRange) {
                        max += distance;
                        positions.push(max);
                    }

                }

                return positions;
            },

            /**
             * Find the factor to estimate how wide the plot area would have been if ordinal
             * gaps were included. This value is used to compute an imagined plot width in order
             * to establish the data grouping interval.
             *
             * A real world case is the intraday-candlestick
             * example. Without this logic, it would show the correct data grouping when viewing
             * a range within each day, but once moving the range to include the gap between two
             * days, the interval would include the cut-away night hours and the data grouping
             * would be wrong. So the below method tries to compensate by identifying the most
             * common point interval, in this case days.
             *
             * An opposite case is presented in issue #718. We have a long array of daily data,
             * then one point is appended one hour after the last point. We expect the data grouping
             * not to change.
             *
             * In the future, if we find cases where this estimation doesn't work optimally, we
             * might need to add a second pass to the data grouping logic, where we do another run
             * with a greater interval if the number of data groups is more than a certain fraction
             * of the desired group count.
             */
            getGroupIntervalFactor: function(xMin, xMax, series) {
                var i,
                    processedXData = series.processedXData,
                    len = processedXData.length,
                    distances = [],
                    median,
                    groupIntervalFactor = this.groupIntervalFactor;

                // Only do this computation for the first series, let the other inherit it (#2416)
                if (!groupIntervalFactor) {

                    // Register all the distances in an array
                    for (i = 0; i < len - 1; i++) {
                        distances[i] = processedXData[i + 1] - processedXData[i];
                    }

                    // Sort them and find the median
                    distances.sort(function(a, b) {
                        return a - b;
                    });
                    median = distances[Math.floor(len / 2)];

                    // Compensate for series that don't extend through the entire axis extent. #1675.
                    xMin = Math.max(xMin, processedXData[0]);
                    xMax = Math.min(xMax, processedXData[len - 1]);

                    this.groupIntervalFactor = groupIntervalFactor = (len * median) / (xMax - xMin);
                }

                // Return the factor needed for data grouping
                return groupIntervalFactor;
            },

            /**
             * Make the tick intervals closer because the ordinal gaps make the ticks spread out or cluster
             */
            postProcessTickInterval: function(tickInterval) {
                // Problem: http://jsfiddle.net/highcharts/FQm4E/1/
                // This is a case where this algorithm doesn't work optimally. In this case, the
                // tick labels are spread out per week, but all the gaps reside within weeks. So
                // we have a situation where the labels are courser than the ordinal gaps, and
                // thus the tick interval should not be altered
                var ordinalSlope = this.ordinalSlope,
                    ret;


                if (ordinalSlope) {
                    if (!this.options.breaks) {
                        ret = tickInterval / (ordinalSlope / this.closestPointRange);
                    } else {
                        ret = this.closestPointRange || tickInterval; // #7275
                    }
                } else {
                    ret = tickInterval;
                }
                return ret;
            }
        });

        // Record this to prevent overwriting by broken-axis module (#5979)
        Axis.prototype.ordinal2lin = Axis.prototype.val2lin;

        // Extending the Chart.pan method for ordinal axes
        wrap(Chart.prototype, 'pan', function(proceed, e) {
            var chart = this,
                xAxis = chart.xAxis[0],
                overscroll = xAxis.options.overscroll,
                chartX = e.chartX,
                runBase = false;

            if (xAxis.options.ordinal && xAxis.series.length) {

                var mouseDownX = chart.mouseDownX,
                    extremes = xAxis.getExtremes(),
                    dataMax = extremes.dataMax,
                    min = extremes.min,
                    max = extremes.max,
                    trimmedRange,
                    hoverPoints = chart.hoverPoints,
                    closestPointRange = xAxis.closestPointRange || xAxis.overscrollPointsRange,
                    pointPixelWidth = xAxis.translationSlope * (xAxis.ordinalSlope || closestPointRange),
                    movedUnits = (mouseDownX - chartX) / pointPixelWidth, // how many ordinal units did we move?
                    extendedAxis = {
                        ordinalPositions: xAxis.getExtendedPositions()
                    }, // get index of all the chart's points
                    ordinalPositions,
                    searchAxisLeft,
                    lin2val = xAxis.lin2val,
                    val2lin = xAxis.val2lin,
                    searchAxisRight;

                if (!extendedAxis.ordinalPositions) { // we have an ordinal axis, but the data is equally spaced
                    runBase = true;

                } else if (Math.abs(movedUnits) > 1) {

                    // Remove active points for shared tooltip
                    if (hoverPoints) {
                        each(hoverPoints, function(point) {
                            point.setState();
                        });
                    }

                    if (movedUnits < 0) {
                        searchAxisLeft = extendedAxis;
                        searchAxisRight = xAxis.ordinalPositions ? xAxis : extendedAxis;
                    } else {
                        searchAxisLeft = xAxis.ordinalPositions ? xAxis : extendedAxis;
                        searchAxisRight = extendedAxis;
                    }

                    // In grouped data series, the last ordinal position represents the grouped data, which is
                    // to the left of the real data max. If we don't compensate for this, we will be allowed
                    // to pan grouped data series passed the right of the plot area.
                    ordinalPositions = searchAxisRight.ordinalPositions;
                    if (dataMax > ordinalPositions[ordinalPositions.length - 1]) {
                        ordinalPositions.push(dataMax);
                    }

                    // Get the new min and max values by getting the ordinal index for the current extreme,
                    // then add the moved units and translate back to values. This happens on the
                    // extended ordinal positions if the new position is out of range, else it happens
                    // on the current x axis which is smaller and faster.
                    chart.fixedRange = max - min;
                    trimmedRange = xAxis.toFixedRange(null, null,
                        lin2val.apply(searchAxisLeft, [
                            val2lin.apply(searchAxisLeft, [min, true]) + movedUnits, // the new index
                            true // translate from index
                        ]),
                        lin2val.apply(searchAxisRight, [
                            val2lin.apply(searchAxisRight, [max, true]) + movedUnits, // the new index
                            true // translate from index
                        ])
                    );

                    // Apply it if it is within the available data range
                    if (
                        trimmedRange.min >= Math.min(extremes.dataMin, min) &&
                        trimmedRange.max <= Math.max(dataMax, max) + overscroll
                    ) {
                        xAxis.setExtremes(trimmedRange.min, trimmedRange.max, true, false, {
                            trigger: 'pan'
                        });
                    }

                    chart.mouseDownX = chartX; // set new reference for next run
                    css(chart.container, {
                        cursor: 'move'
                    });
                }

            } else {
                runBase = true;
            }

            // revert to the linear chart.pan version
            if (runBase) {
                if (overscroll) {
                    xAxis.max = xAxis.dataMax + overscroll;
                }
                // call the original function
                proceed.apply(this, Array.prototype.slice.call(arguments, 1));
            }
        });

        /* ****************************************************************************
         * End ordinal axis logic                                                   *
         *****************************************************************************/

    }(Highcharts));
    (function(H) {
        /**
         * (c) 2009-2017 Torstein Honsi
         *
         * License: www.highcharts.com/license
         */

        var pick = H.pick,
            wrap = H.wrap,
            each = H.each,
            extend = H.extend,
            isArray = H.isArray,
            fireEvent = H.fireEvent,
            Axis = H.Axis,
            Series = H.Series;

        function stripArguments() {
            return Array.prototype.slice.call(arguments, 1);
        }

        extend(Axis.prototype, {
            isInBreak: function(brk, val) {
                var ret,
                    repeat = brk.repeat || Infinity,
                    from = brk.from,
                    length = brk.to - brk.from,
                    test = (val >= from ? (val - from) % repeat : repeat - ((from - val) % repeat));

                if (!brk.inclusive) {
                    ret = test < length && test !== 0;
                } else {
                    ret = test <= length;
                }
                return ret;
            },

            isInAnyBreak: function(val, testKeep) {

                var breaks = this.options.breaks,
                    i = breaks && breaks.length,
                    inbrk,
                    keep,
                    ret;


                if (i) {

                    while (i--) {
                        if (this.isInBreak(breaks[i], val)) {
                            inbrk = true;
                            if (!keep) {
                                keep = pick(breaks[i].showPoints, this.isXAxis ? false : true);
                            }
                        }
                    }

                    if (inbrk && testKeep) {
                        ret = inbrk && !keep;
                    } else {
                        ret = inbrk;
                    }
                }
                return ret;
            }
        });

        wrap(Axis.prototype, 'setTickPositions', function(proceed) {
            proceed.apply(this, Array.prototype.slice.call(arguments, 1));

            if (this.options.breaks) {
                var axis = this,
                    tickPositions = this.tickPositions,
                    info = this.tickPositions.info,
                    newPositions = [],
                    i;

                for (i = 0; i < tickPositions.length; i++) {
                    if (!axis.isInAnyBreak(tickPositions[i])) {
                        newPositions.push(tickPositions[i]);
                    }
                }

                this.tickPositions = newPositions;
                this.tickPositions.info = info;
            }
        });

        wrap(Axis.prototype, 'init', function(proceed, chart, userOptions) {
            var axis = this,
                breaks;
            // Force Axis to be not-ordinal when breaks are defined
            if (userOptions.breaks && userOptions.breaks.length) {
                userOptions.ordinal = false;
            }
            proceed.call(this, chart, userOptions);
            breaks = this.options.breaks;
            axis.isBroken = (isArray(breaks) && !!breaks.length);
            if (axis.isBroken) {
                axis.val2lin = function(val) {
                    var nval = val,
                        brk,
                        i;

                    for (i = 0; i < axis.breakArray.length; i++) {
                        brk = axis.breakArray[i];
                        if (brk.to <= val) {
                            nval -= brk.len;
                        } else if (brk.from >= val) {
                            break;
                        } else if (axis.isInBreak(brk, val)) {
                            nval -= (val - brk.from);
                            break;
                        }
                    }

                    return nval;
                };

                axis.lin2val = function(val) {
                    var nval = val,
                        brk,
                        i;

                    for (i = 0; i < axis.breakArray.length; i++) {
                        brk = axis.breakArray[i];
                        if (brk.from >= nval) {
                            break;
                        } else if (brk.to < nval) {
                            nval += brk.len;
                        } else if (axis.isInBreak(brk, nval)) {
                            nval += brk.len;
                        }
                    }
                    return nval;
                };

                axis.setExtremes = function(newMin, newMax, redraw, animation, eventArguments) {
                    // If trying to set extremes inside a break, extend it to before and after the break ( #3857 )
                    while (this.isInAnyBreak(newMin)) {
                        newMin -= this.closestPointRange;
                    }
                    while (this.isInAnyBreak(newMax)) {
                        newMax -= this.closestPointRange;
                    }
                    Axis.prototype.setExtremes.call(this, newMin, newMax, redraw, animation, eventArguments);
                };

                axis.setAxisTranslation = function(saveOld) {
                    Axis.prototype.setAxisTranslation.call(this, saveOld);

                    var breaks = axis.options.breaks,
                        breakArrayT = [], // Temporary one
                        breakArray = [],
                        length = 0,
                        inBrk,
                        repeat,
                        min = axis.userMin || axis.min,
                        max = axis.userMax || axis.max,
                        pointRangePadding = pick(axis.pointRangePadding, 0),
                        start,
                        i;

                    // Min & max check (#4247)
                    each(breaks, function(brk) {
                        repeat = brk.repeat || Infinity;
                        if (axis.isInBreak(brk, min)) {
                            min += (brk.to % repeat) - (min % repeat);
                        }
                        if (axis.isInBreak(brk, max)) {
                            max -= (max % repeat) - (brk.from % repeat);
                        }
                    });

                    // Construct an array holding all breaks in the axis
                    each(breaks, function(brk) {
                        start = brk.from;
                        repeat = brk.repeat || Infinity;

                        while (start - repeat > min) {
                            start -= repeat;
                        }
                        while (start < min) {
                            start += repeat;
                        }

                        for (i = start; i < max; i += repeat) {
                            breakArrayT.push({
                                value: i,
                                move: 'in'
                            });
                            breakArrayT.push({
                                value: i + (brk.to - brk.from),
                                move: 'out',
                                size: brk.breakSize
                            });
                        }
                    });

                    breakArrayT.sort(function(a, b) {
                        var ret;
                        if (a.value === b.value) {
                            ret = (a.move === 'in' ? 0 : 1) - (b.move === 'in' ? 0 : 1);
                        } else {
                            ret = a.value - b.value;
                        }
                        return ret;
                    });

                    // Simplify the breaks
                    inBrk = 0;
                    start = min;

                    each(breakArrayT, function(brk) {
                        inBrk += (brk.move === 'in' ? 1 : -1);

                        if (inBrk === 1 && brk.move === 'in') {
                            start = brk.value;
                        }
                        if (inBrk === 0) {
                            breakArray.push({
                                from: start,
                                to: brk.value,
                                len: brk.value - start - (brk.size || 0)
                            });
                            length += brk.value - start - (brk.size || 0);
                        }
                    });

                    axis.breakArray = breakArray;

                    // Used with staticScale, and below, the actual axis length when
                    // breaks are substracted.
                    axis.unitLength = max - min - length + pointRangePadding;

                    fireEvent(axis, 'afterBreaks');

                    if (axis.options.staticScale) {
                        axis.transA = axis.options.staticScale;
                    } else if (axis.unitLength) {
                        axis.transA *= (max - axis.min + pointRangePadding) /
                            axis.unitLength;
                    }

                    if (pointRangePadding) {
                        axis.minPixelPadding = axis.transA * axis.minPointOffset;
                    }

                    axis.min = min;
                    axis.max = max;
                };
            }
        });

        wrap(Series.prototype, 'generatePoints', function(proceed) {

            proceed.apply(this, stripArguments(arguments));

            var series = this,
                xAxis = series.xAxis,
                yAxis = series.yAxis,
                points = series.points,
                point,
                i = points.length,
                connectNulls = series.options.connectNulls,
                nullGap;


            if (xAxis && yAxis && (xAxis.options.breaks || yAxis.options.breaks)) {
                while (i--) {
                    point = points[i];

                    nullGap = point.y === null && connectNulls === false; // respect nulls inside the break (#4275)
                    if (!nullGap && (xAxis.isInAnyBreak(point.x, true) || yAxis.isInAnyBreak(point.y, true))) {
                        points.splice(i, 1);
                        if (this.data[i]) {
                            this.data[i].destroyElements(); // removes the graphics for this point if they exist
                        }
                    }
                }
            }

        });

        function drawPointsWrapped(proceed) {
            proceed.apply(this);
            this.drawBreaks(this.xAxis, ['x']);
            this.drawBreaks(this.yAxis, pick(this.pointArrayMap, ['y']));
        }

        H.Series.prototype.drawBreaks = function(axis, keys) {
            var series = this,
                points = series.points,
                breaks,
                threshold,
                eventName,
                y;

            if (!axis) {
                return; // #5950
            }

            each(keys, function(key) {
                breaks = axis.breakArray || [];
                threshold = axis.isXAxis ? axis.min : pick(series.options.threshold, axis.min);
                each(points, function(point) {
                    y = pick(point['stack' + key.toUpperCase()], point[key]);
                    each(breaks, function(brk) {
                        eventName = false;

                        if ((threshold < brk.from && y > brk.to) || (threshold > brk.from && y < brk.from)) {
                            eventName = 'pointBreak';
                        } else if ((threshold < brk.from && y > brk.from && y < brk.to) || (threshold > brk.from && y > brk.to && y < brk.from)) { // point falls inside the break
                            eventName = 'pointInBreak';
                        }
                        if (eventName) {
                            fireEvent(axis, eventName, {
                                point: point,
                                brk: brk
                            });
                        }
                    });
                });
            });
        };


        /**
         * Extend getGraphPath by identifying gaps in the data so that we can draw a gap
         * in the line or area. This was moved from ordinal axis module to broken axis
         * module as of #5045.
         */
        H.Series.prototype.gappedPath = function() {
            var gapSize = this.options.gapSize,
                points = this.points.slice(),
                i = points.length - 1,
                yAxis = this.yAxis,
                xRange,
                stack;

            /**
             * Defines when to display a gap in the graph, together with the `gapUnit`
             * option.
             * 
             * When the `gapUnit` is `relative` (default), a gap size of 5 means
             * that if the distance between two points is greater than five times
             * that of the two closest points, the graph will be broken.
             *
             * When the `gapUnit` is `value`, the gap is based on absolute axis values,
             * which on a datetime axis is milliseconds.
             * 
             * In practice, this option is most often used to visualize gaps in
             * time series. In a stock chart, intraday data is available for daytime
             * hours, while gaps will appear in nights and weekends.
             * 
             * @type {Number}
             * @see [xAxis.breaks](#xAxis.breaks)
             * @sample {highstock} stock/plotoptions/series-gapsize/
             *         Setting the gap size to 2 introduces gaps for weekends in daily
             *         datasets.
             * @default 0
             * @product highstock
             * @apioption plotOptions.series.gapSize
             */

            /**
             * Together with `gapSize`, this option defines where to draw gaps in the 
             * graph.
             *
             * @type {String}
             * @see [gapSize](plotOptions.series.gapSize)
             * @default relative
             * @validvalue ["relative", "value"]
             * @since 5.0.13
             * @product highstock
             * @apioption plotOptions.series.gapUnit
             */

            if (gapSize && i > 0) { // #5008

                // Gap unit is relative
                if (this.options.gapUnit !== 'value') {
                    gapSize *= this.closestPointRange;
                }

                // extension for ordinal breaks
                while (i--) {
                    if (points[i + 1].x - points[i].x > gapSize) {
                        xRange = (points[i].x + points[i + 1].x) / 2;

                        points.splice( // insert after this one
                            i + 1,
                            0, {
                                isNull: true,
                                x: xRange
                            }
                        );

                        // For stacked chart generate empty stack items, #6546
                        if (this.options.stacking) {
                            stack = yAxis.stacks[this.stackKey][xRange] = new H.StackItem(
                                yAxis,
                                yAxis.options.stackLabels,
                                false,
                                xRange,
                                this.stack
                            );
                            stack.total = 0;
                        }
                    }
                }
            }

            // Call base method
            return this.getGraphPath(points);
        };

        wrap(H.seriesTypes.column.prototype, 'drawPoints', drawPointsWrapped);
        wrap(H.Series.prototype, 'drawPoints', drawPointsWrapped);

    }(Highcharts));
    (function() {


    }());
    (function(H) {
        /**
         * (c) 2010-2017 Torstein Honsi
         *
         * License: www.highcharts.com/license
         */
        var arrayMax = H.arrayMax,
            arrayMin = H.arrayMin,
            Axis = H.Axis,
            defaultPlotOptions = H.defaultPlotOptions,
            defined = H.defined,
            each = H.each,
            extend = H.extend,
            format = H.format,
            isNumber = H.isNumber,
            merge = H.merge,
            pick = H.pick,
            Point = H.Point,
            Series = H.Series,
            Tooltip = H.Tooltip,
            wrap = H.wrap;

        /* ****************************************************************************
         * Start data grouping module												 *
         ******************************************************************************/

        /**
         * Data grouping is the concept of sampling the data values into larger
         * blocks in order to ease readability and increase performance of the
         * JavaScript charts. Highstock by default applies data grouping when
         * the points become closer than a certain pixel value, determined by
         * the `groupPixelWidth` option.
         * 
         * If data grouping is applied, the grouping information of grouped
         * points can be read from the [Point.dataGroup](#Point.dataGroup).
         * 
         * @product highstock
         * @apioption plotOptions.series.dataGrouping
         */

        /**
         * The method of approximation inside a group. When for example 30 days
         * are grouped into one month, this determines what value should represent
         * the group. Possible values are "average", "averages", "open", "high",
         * "low", "close" and "sum". For OHLC and candlestick series the approximation
         * is "ohlc" by default, which finds the open, high, low and close values
         * within all the grouped data. For ranges, the approximation is "range",
         * which finds the low and high values. For multi-dimensional data,
         * like ranges and OHLC, "averages" will compute the average for each
         * dimension.
         * 
         * Custom aggregate methods can be added by assigning a callback function
         * as the approximation. This function takes a numeric array as the
         * argument and should return a single numeric value or `null`. Note
         * that the numeric array will never contain null values, only true
         * numbers. Instead, if null values are present in the raw data, the
         * numeric array will have an `.hasNulls` property set to `true`. For
         * single-value data sets the data is available in the first argument
         * of the callback function. For OHLC data sets, all the open values
         * are in the first argument, all high values in the second etc.
         * 
         * Since v4.2.7, grouping meta data is available in the approximation
         * callback from `this.dataGroupInfo`. It can be used to extract information
         * from the raw data.
         * 
         * Defaults to `average` for line-type series, `sum` for columns, `range`
         * for range series and `ohlc` for OHLC and candlestick.
         * 
         * @validvalue ["average", "averages", "open", "high", "low", "close", "sum"]
         * @type {String|Function}
         * @sample {highstock} stock/plotoptions/series-datagrouping-approximation Approximation callback with custom data
         * @product highstock
         * @apioption plotOptions.series.dataGrouping.approximation
         */

        /**
         * Datetime formats for the header of the tooltip in a stock chart.
         * The format can vary within a chart depending on the currently selected
         * time range and the current data grouping.
         * 
         * The default formats are:
         * 
         * <pre>{
         *     millisecond: ['%A, %b %e, %H:%M:%S.%L', '%A, %b %e, %H:%M:%S.%L', '-%H:%M:%S.%L'],
         *     second: ['%A, %b %e, %H:%M:%S', '%A, %b %e, %H:%M:%S', '-%H:%M:%S'],
         *     minute: ['%A, %b %e, %H:%M', '%A, %b %e, %H:%M', '-%H:%M'],
         *     hour: ['%A, %b %e, %H:%M', '%A, %b %e, %H:%M', '-%H:%M'],
         *     day: ['%A, %b %e, %Y', '%A, %b %e', '-%A, %b %e, %Y'],
         *     week: ['Week from %A, %b %e, %Y', '%A, %b %e', '-%A, %b %e, %Y'],
         *     month: ['%B %Y', '%B', '-%B %Y'],
         *     year: ['%Y', '%Y', '-%Y']
         * }</pre>
         * 
         * For each of these array definitions, the first item is the format
         * used when the active time span is one unit. For instance, if the
         * current data applies to one week, the first item of the week array
         * is used. The second and third items are used when the active time
         * span is more than two units. For instance, if the current data applies
         * to two weeks, the second and third item of the week array are used,
         *  and applied to the start and end date of the time span.
         * 
         * @type {Object}
         * @product highstock
         * @apioption plotOptions.series.dataGrouping.dateTimeLabelFormats
         */

        /**
         * Enable or disable data grouping.
         * 
         * @type {Boolean}
         * @default true
         * @product highstock
         * @apioption plotOptions.series.dataGrouping.enabled
         */

        /**
         * When data grouping is forced, it runs no matter how small the intervals
         * are. This can be handy for example when the sum should be calculated
         * for values appearing at random times within each hour.
         * 
         * @type {Boolean}
         * @default false
         * @product highstock
         * @apioption plotOptions.series.dataGrouping.forced
         */

        /**
         * The approximate pixel width of each group. If for example a series
         * with 30 points is displayed over a 600 pixel wide plot area, no grouping
         * is performed. If however the series contains so many points that
         * the spacing is less than the groupPixelWidth, Highcharts will try
         * to group it into appropriate groups so that each is more or less
         * two pixels wide. If multiple series with different group pixel widths
         * are drawn on the same x axis, all series will take the greatest width.
         * For example, line series have 2px default group width, while column
         * series have 10px. If combined, both the line and the column will
         * have 10px by default.
         * 
         * @type {Number}
         * @default 2
         * @product highstock
         * @apioption plotOptions.series.dataGrouping.groupPixelWidth
         */

        /**
         * Normally, a group is indexed by the start of that group, so for example
         * when 30 daily values are grouped into one month, that month's x value
         * will be the 1st of the month. This apparently shifts the data to
         * the left. When the smoothed option is true, this is compensated for.
         * The data is shifted to the middle of the group, and min and max
         * values are preserved. Internally, this is used in the Navigator series.
         * 
         * @type {Boolean}
         * @default false
         * @product highstock
         * @apioption plotOptions.series.dataGrouping.smoothed
         */

        /**
         * An array determining what time intervals the data is allowed to be
         * grouped to. Each array item is an array where the first value is
         * the time unit and the second value another array of allowed multiples.
         * Defaults to:
         * 
         * <pre>units: [[
         *     'millisecond', // unit name
         *     [1, 2, 5, 10, 20, 25, 50, 100, 200, 500] // allowed multiples
         * ], [
         *     'second',
         *     [1, 2, 5, 10, 15, 30]
         * ], [
         *     'minute',
         *     [1, 2, 5, 10, 15, 30]
         * ], [
         *     'hour',
         *     [1, 2, 3, 4, 6, 8, 12]
         * ], [
         *     'day',
         *     [1]
         * ], [
         *     'week',
         *     [1]
         * ], [
         *     'month',
         *     [1, 3, 6]
         * ], [
         *     'year',
         *     null
         * ]]</pre>
         * 
         * @type {Array}
         * @product highstock
         * @apioption plotOptions.series.dataGrouping.units
         */

        /**
         * The approximate pixel width of each group. If for example a series
         * with 30 points is displayed over a 600 pixel wide plot area, no grouping
         * is performed. If however the series contains so many points that
         * the spacing is less than the groupPixelWidth, Highcharts will try
         * to group it into appropriate groups so that each is more or less
         * two pixels wide. Defaults to `10`.
         * 
         * @type {Number}
         * @sample {highstock} stock/plotoptions/series-datagrouping-grouppixelwidth/
         *         Two series with the same data density but different groupPixelWidth
         * @default 10
         * @product highstock
         * @apioption plotOptions.column.dataGrouping.groupPixelWidth
         */

        var seriesProto = Series.prototype,
            baseProcessData = seriesProto.processData,
            baseGeneratePoints = seriesProto.generatePoints,

            /** 
             * 
             */
            commonOptions = {
                approximation: 'average', // average, open, high, low, close, sum
                // enabled: null, // (true for stock charts, false for basic),
                // forced: undefined,
                groupPixelWidth: 2,
                // the first one is the point or start value, the second is the start value if we're dealing with range,
                // the third one is the end value if dealing with a range
                dateTimeLabelFormats: {
                    millisecond: ['%A, %b %e, %H:%M:%S.%L', '%A, %b %e, %H:%M:%S.%L', '-%H:%M:%S.%L'],
                    second: ['%A, %b %e, %H:%M:%S', '%A, %b %e, %H:%M:%S', '-%H:%M:%S'],
                    minute: ['%A, %b %e, %H:%M', '%A, %b %e, %H:%M', '-%H:%M'],
                    hour: ['%A, %b %e, %H:%M', '%A, %b %e, %H:%M', '-%H:%M'],
                    day: ['%A, %b %e, %Y', '%A, %b %e', '-%A, %b %e, %Y'],
                    week: ['Week from %A, %b %e, %Y', '%A, %b %e', '-%A, %b %e, %Y'],
                    month: ['%B %Y', '%B', '-%B %Y'],
                    year: ['%Y', '%Y', '-%Y']
                }
                // smoothed = false, // enable this for navigator series only
            },

            specificOptions = { // extends common options
                line: {},
                spline: {},
                area: {},
                areaspline: {},
                column: {
                    approximation: 'sum',
                    groupPixelWidth: 10
                },
                arearange: {
                    approximation: 'range'
                },
                areasplinerange: {
                    approximation: 'range'
                },
                columnrange: {
                    approximation: 'range',
                    groupPixelWidth: 10
                },
                candlestick: {
                    approximation: 'ohlc',
                    groupPixelWidth: 10
                },
                ohlc: {
                    approximation: 'ohlc',
                    groupPixelWidth: 5
                }
            },

            // units are defined in a separate array to allow complete overriding in case of a user option
            defaultDataGroupingUnits = H.defaultDataGroupingUnits = [
                [
                    'millisecond', // unit name
                    [1, 2, 5, 10, 20, 25, 50, 100, 200, 500] // allowed multiples
                ],
                [
                    'second', [1, 2, 5, 10, 15, 30]
                ],
                [
                    'minute', [1, 2, 5, 10, 15, 30]
                ],
                [
                    'hour', [1, 2, 3, 4, 6, 8, 12]
                ],
                [
                    'day', [1]
                ],
                [
                    'week', [1]
                ],
                [
                    'month', [1, 3, 6]
                ],
                [
                    'year',
                    null
                ]
            ],


            /**
             * Define the available approximation types. The data grouping
             * approximations takes an array or numbers as the first parameter. In case
             * of ohlc, four arrays are sent in as four parameters. Each array consists
             * only of numbers. In case null values belong to the group, the property
             * .hasNulls will be set to true on the array.
             */
            approximations = H.approximations = {
                sum: function(arr) {
                    var len = arr.length,
                        ret;

                    // 1. it consists of nulls exclusively
                    if (!len && arr.hasNulls) {
                        ret = null;
                        // 2. it has a length and real values
                    } else if (len) {
                        ret = 0;
                        while (len--) {
                            ret += arr[len];
                        }
                    }
                    // 3. it has zero length, so just return undefined
                    // => doNothing()

                    return ret;
                },
                average: function(arr) {
                    var len = arr.length,
                        ret = approximations.sum(arr);

                    // If we have a number, return it divided by the length. If not,
                    // return null or undefined based on what the sum method finds.
                    if (isNumber(ret) && len) {
                        ret = ret / len;
                    }

                    return ret;
                },
                // The same as average, but for series with multiple values, like area
                // ranges.
                averages: function() { // #5479
                    var ret = [];

                    each(arguments, function(arr) {
                        ret.push(approximations.average(arr));
                    });

                    // Return undefined when first elem. is undefined and let
                    // sum method handle null (#7377)
                    return ret[0] === undefined ? undefined : ret;
                },
                open: function(arr) {
                    return arr.length ? arr[0] : (arr.hasNulls ? null : undefined);
                },
                high: function(arr) {
                    return arr.length ? arrayMax(arr) : (arr.hasNulls ? null : undefined);
                },
                low: function(arr) {
                    return arr.length ? arrayMin(arr) : (arr.hasNulls ? null : undefined);
                },
                close: function(arr) {
                    return arr.length ? arr[arr.length - 1] : (arr.hasNulls ? null : undefined);
                },
                // ohlc and range are special cases where a multidimensional array is input and an array is output
                ohlc: function(open, high, low, close) {
                    open = approximations.open(open);
                    high = approximations.high(high);
                    low = approximations.low(low);
                    close = approximations.close(close);

                    if (isNumber(open) || isNumber(high) || isNumber(low) || isNumber(close)) {
                        return [open, high, low, close];
                    }
                    // else, return is undefined
                },
                range: function(low, high) {
                    low = approximations.low(low);
                    high = approximations.high(high);

                    if (isNumber(low) || isNumber(high)) {
                        return [low, high];
                    } else if (low === null && high === null) {
                        return null;
                    }
                    // else, return is undefined
                }
            };

        /**
         * Takes parallel arrays of x and y data and groups the data into intervals 
         * defined by groupPositions, a collection of starting x values for each group.
         */
        seriesProto.groupData = function(xData, yData, groupPositions, approximation) {
            var series = this,
                data = series.data,
                dataOptions = series.options.data,
                groupedXData = [],
                groupedYData = [],
                groupMap = [],
                dataLength = xData.length,
                pointX,
                pointY,
                groupedY,
                // when grouping the fake extended axis for panning,
                // we don't need to consider y
                handleYData = !!yData,
                values = [],
                approximationFn = typeof approximation === 'function' ?
                approximation :
                approximations[approximation] ||
                // if the approximation is not found use default series type
                // approximation (#2914)
                (
                    specificOptions[series.type] &&
                    approximations[specificOptions[series.type].approximation]
                ) || approximations[commonOptions.approximation],
                pointArrayMap = series.pointArrayMap,
                pointArrayMapLength = pointArrayMap && pointArrayMap.length,
                pos = 0,
                start = 0,
                valuesLen,
                i, j;

            // Calculate values array size from pointArrayMap length
            if (pointArrayMapLength) {
                each(pointArrayMap, function() {
                    values.push([]);
                });
            } else {
                values.push([]);
            }
            valuesLen = pointArrayMapLength || 1;

            // Start with the first point within the X axis range (#2696)
            for (i = 0; i <= dataLength; i++) {
                if (xData[i] >= groupPositions[0]) {
                    break;
                }
            }

            for (i; i <= dataLength; i++) {

                // when a new group is entered, summarize and initiate 
                // the previous group
                while ((
                        groupPositions[pos + 1] !== undefined &&
                        xData[i] >= groupPositions[pos + 1]
                    ) || i === dataLength) { // get the last group

                    // get group x and y
                    pointX = groupPositions[pos];
                    series.dataGroupInfo = {
                        start: start,
                        length: values[0].length
                    };
                    groupedY = approximationFn.apply(series, values);

                    // push the grouped data
                    if (groupedY !== undefined) {
                        groupedXData.push(pointX);
                        groupedYData.push(groupedY);
                        groupMap.push(series.dataGroupInfo);
                    }

                    // reset the aggregate arrays
                    start = i;
                    for (j = 0; j < valuesLen; j++) {
                        values[j].length = 0; // faster than values[j] = []
                        values[j].hasNulls = false;
                    }

                    // Advance on the group positions
                    pos += 1;

                    // don't loop beyond the last group
                    if (i === dataLength) {
                        break;
                    }
                }

                // break out
                if (i === dataLength) {
                    break;
                }

                // for each raw data point, push it to an array that contains all values
                // for this specific group
                if (pointArrayMap) {

                    var index = series.cropStart + i,
                        point = (data && data[index]) ||
                        series.pointClass.prototype.applyOptions.apply({
                            series: series
                        }, [dataOptions[index]]),
                        val;

                    for (j = 0; j < pointArrayMapLength; j++) {
                        val = point[pointArrayMap[j]];
                        if (isNumber(val)) {
                            values[j].push(val);
                        } else if (val === null) {
                            values[j].hasNulls = true;
                        }
                    }

                } else {
                    pointY = handleYData ? yData[i] : null;

                    if (isNumber(pointY)) {
                        values[0].push(pointY);
                    } else if (pointY === null) {
                        values[0].hasNulls = true;
                    }
                }
            }

            return [groupedXData, groupedYData, groupMap];
        };

        /**
         * Extend the basic processData method, that crops the data to the current zoom
         * range, with data grouping logic.
         */
        seriesProto.processData = function() {
            var series = this,
                chart = series.chart,
                options = series.options,
                dataGroupingOptions = options.dataGrouping,
                groupingEnabled = series.allowDG !== false && dataGroupingOptions &&
                pick(dataGroupingOptions.enabled, chart.options.isStock),
                visible = series.visible || !chart.options.chart.ignoreHiddenSeries,
                hasGroupedData,
                skip,
                lastDataGrouping = this.currentDataGrouping,
                currentDataGrouping;

            // run base method
            series.forceCrop = groupingEnabled; // #334
            series.groupPixelWidth = null; // #2110
            series.hasProcessed = true; // #2692

            // skip if processData returns false or if grouping is disabled (in that order)
            skip = baseProcessData.apply(series, arguments) === false || !groupingEnabled;
            if (!skip) {
                series.destroyGroupedData();

                var i,
                    processedXData = series.processedXData,
                    processedYData = series.processedYData,
                    plotSizeX = chart.plotSizeX,
                    xAxis = series.xAxis,
                    ordinal = xAxis.options.ordinal,
                    groupPixelWidth = series.groupPixelWidth = xAxis.getGroupPixelWidth && xAxis.getGroupPixelWidth();

                // Execute grouping if the amount of points is greater than the limit defined in groupPixelWidth
                if (groupPixelWidth) {
                    hasGroupedData = true;

                    series.isDirty = true; // force recreation of point instances in series.translate, #5699
                    series.points = null; // #6709

                    var extremes = xAxis.getExtremes(),
                        xMin = extremes.min,
                        xMax = extremes.max,
                        groupIntervalFactor = (ordinal && xAxis.getGroupIntervalFactor(xMin, xMax, series)) || 1,
                        interval = (groupPixelWidth * (xMax - xMin) / plotSizeX) * groupIntervalFactor,
                        groupPositions = xAxis.getTimeTicks(
                            xAxis.normalizeTimeTickInterval(interval, dataGroupingOptions.units || defaultDataGroupingUnits),
                            Math.min(xMin, processedXData[0]), // Processed data may extend beyond axis (#4907)
                            Math.max(xMax, processedXData[processedXData.length - 1]),
                            xAxis.options.startOfWeek,
                            processedXData,
                            series.closestPointRange
                        ),
                        groupedData = seriesProto.groupData.apply(series, [processedXData, processedYData, groupPositions, dataGroupingOptions.approximation]),
                        groupedXData = groupedData[0],
                        groupedYData = groupedData[1];

                    // prevent the smoothed data to spill out left and right, and make
                    // sure data is not shifted to the left
                    if (dataGroupingOptions.smoothed && groupedXData.length) {
                        i = groupedXData.length - 1;
                        groupedXData[i] = Math.min(groupedXData[i], xMax);
                        while (i-- && i > 0) {
                            groupedXData[i] += interval / 2;
                        }
                        groupedXData[0] = Math.max(groupedXData[0], xMin);
                    }

                    // record what data grouping values were used
                    currentDataGrouping = groupPositions.info;
                    series.closestPointRange = groupPositions.info.totalRange;
                    series.groupMap = groupedData[2];

                    // Make sure the X axis extends to show the first group (#2533)
                    // But only for visible series (#5493, #6393)
                    if (defined(groupedXData[0]) && groupedXData[0] < xAxis.dataMin && visible) {
                        if (xAxis.min === xAxis.dataMin) {
                            xAxis.min = groupedXData[0];
                        }
                        xAxis.dataMin = groupedXData[0];
                    }

                    // set series props
                    series.processedXData = groupedXData;
                    series.processedYData = groupedYData;
                } else {
                    series.groupMap = null;
                }
                series.hasGroupedData = hasGroupedData;
                series.currentDataGrouping = currentDataGrouping;

                series.preventGraphAnimation =
                    (lastDataGrouping && lastDataGrouping.totalRange) !==
                    (currentDataGrouping && currentDataGrouping.totalRange);
            }
        };

        /**
         * Destroy the grouped data points. #622, #740
         */
        seriesProto.destroyGroupedData = function() {

            var groupedData = this.groupedData;

            // clear previous groups
            each(groupedData || [], function(point, i) {
                if (point) {
                    groupedData[i] = point.destroy ? point.destroy() : null;
                }
            });
            this.groupedData = null;
        };

        /**
         * Override the generatePoints method by adding a reference to grouped data
         */
        seriesProto.generatePoints = function() {

            baseGeneratePoints.apply(this);

            // record grouped data in order to let it be destroyed the next time processData runs
            this.destroyGroupedData(); // #622
            this.groupedData = this.hasGroupedData ? this.points : null;
        };

        /**
         * Override point prototype to throw a warning when trying to update grouped points
         */
        wrap(Point.prototype, 'update', function(proceed) {
            if (this.dataGroup) {
                H.error(24);
            } else {
                proceed.apply(this, [].slice.call(arguments, 1));
            }
        });

        /**
         * Extend the original method, make the tooltip's header reflect the grouped range
         */
        wrap(Tooltip.prototype, 'tooltipFooterHeaderFormatter', function(proceed, labelConfig, isFooter) {
            var tooltip = this,
                series = labelConfig.series,
                options = series.options,
                tooltipOptions = series.tooltipOptions,
                dataGroupingOptions = options.dataGrouping,
                xDateFormat = tooltipOptions.xDateFormat,
                xDateFormatEnd,
                xAxis = series.xAxis,
                dateFormat = H.dateFormat,
                currentDataGrouping,
                dateTimeLabelFormats,
                labelFormats,
                formattedKey;

            // apply only to grouped series
            if (xAxis && xAxis.options.type === 'datetime' && dataGroupingOptions && isNumber(labelConfig.key)) {

                // set variables
                currentDataGrouping = series.currentDataGrouping;
                dateTimeLabelFormats = dataGroupingOptions.dateTimeLabelFormats;

                // if we have grouped data, use the grouping information to get the right format
                if (currentDataGrouping) {
                    labelFormats = dateTimeLabelFormats[currentDataGrouping.unitName];
                    if (currentDataGrouping.count === 1) {
                        xDateFormat = labelFormats[0];
                    } else {
                        xDateFormat = labelFormats[1];
                        xDateFormatEnd = labelFormats[2];
                    }
                    // if not grouped, and we don't have set the xDateFormat option, get the best fit,
                    // so if the least distance between points is one minute, show it, but if the
                    // least distance is one day, skip hours and minutes etc.
                } else if (!xDateFormat && dateTimeLabelFormats) {
                    xDateFormat = tooltip.getXDateFormat(labelConfig, tooltipOptions, xAxis);
                }

                // now format the key
                formattedKey = dateFormat(xDateFormat, labelConfig.key);
                if (xDateFormatEnd) {
                    formattedKey += dateFormat(xDateFormatEnd, labelConfig.key + currentDataGrouping.totalRange - 1);
                }

                // return the replaced format
                return format(tooltipOptions[(isFooter ? 'footer' : 'header') + 'Format'], {
                    point: extend(labelConfig.point, {
                        key: formattedKey
                    }),
                    series: series
                });

            }

            // else, fall back to the regular formatter
            return proceed.call(tooltip, labelConfig, isFooter);
        });

        /**
         * Destroy grouped data on series destroy
         */
        wrap(seriesProto, 'destroy', function(proceed) {
            proceed.call(this);
            this.destroyGroupedData();
        });


        // Handle default options for data grouping. This must be set at runtime because some series types are
        // defined after this.
        wrap(seriesProto, 'setOptions', function(proceed, itemOptions) {

            var options = proceed.call(this, itemOptions),
                type = this.type,
                plotOptions = this.chart.options.plotOptions,
                defaultOptions = defaultPlotOptions[type].dataGrouping;

            if (specificOptions[type]) { // #1284
                if (!defaultOptions) {
                    defaultOptions = merge(commonOptions, specificOptions[type]);
                }

                options.dataGrouping = merge(
                    defaultOptions,
                    plotOptions.series && plotOptions.series.dataGrouping, // #1228
                    plotOptions[type].dataGrouping, // Set by the StockChart constructor
                    itemOptions.dataGrouping
                );
            }

            if (this.chart.options.isStock) {
                this.requireSorting = true;
            }

            return options;
        });


        /**
         * When resetting the scale reset the hasProccessed flag to avoid taking previous data grouping
         * of neighbour series into accound when determining group pixel width (#2692).
         */
        wrap(Axis.prototype, 'setScale', function(proceed) {
            proceed.call(this);
            each(this.series, function(series) {
                series.hasProcessed = false;
            });
        });

        /**
         * Get the data grouping pixel width based on the greatest defined individual width
         * of the axis' series, and if whether one of the axes need grouping.
         */
        Axis.prototype.getGroupPixelWidth = function() {

            var series = this.series,
                len = series.length,
                i,
                groupPixelWidth = 0,
                doGrouping = false,
                dataLength,
                dgOptions;

            // If multiple series are compared on the same x axis, give them the same
            // group pixel width (#334)
            i = len;
            while (i--) {
                dgOptions = series[i].options.dataGrouping;
                if (dgOptions) {
                    groupPixelWidth = Math.max(groupPixelWidth, dgOptions.groupPixelWidth);

                }
            }

            // If one of the series needs grouping, apply it to all (#1634)
            i = len;
            while (i--) {
                dgOptions = series[i].options.dataGrouping;

                if (dgOptions && series[i].hasProcessed) { // #2692

                    dataLength = (series[i].processedXData || series[i].data).length;

                    // Execute grouping if the amount of points is greater than the limit defined in groupPixelWidth
                    if (series[i].groupPixelWidth || dataLength > (this.chart.plotSizeX / groupPixelWidth) || (dataLength && dgOptions.forced)) {
                        doGrouping = true;
                    }
                }
            }

            return doGrouping ? groupPixelWidth : 0;
        };

        /**
         * Highstock only. Force data grouping on all the axis' series.
         *
         * @param  {SeriesDatagroupingOptions} [dataGrouping]
         *         A `dataGrouping` configuration. Use `false` to disable data grouping
         *         dynamically.
         * @param  {Boolean} [redraw=true]
         *         Whether to redraw the chart or wait for a later call to {@link
         *         Chart#redraw}.
         *
         * @function setDataGrouping
         * @memberOf Axis.prototype
         */
        Axis.prototype.setDataGrouping = function(dataGrouping, redraw) {
            var i;

            redraw = pick(redraw, true);

            if (!dataGrouping) {
                dataGrouping = {
                    forced: false,
                    units: null
                };
            }

            // Axis is instantiated, update all series
            if (this instanceof Axis) {
                i = this.series.length;
                while (i--) {
                    this.series[i].update({
                        dataGrouping: dataGrouping
                    }, false);
                }

                // Axis not yet instanciated, alter series options
            } else {
                each(this.chart.options.series, function(seriesOptions) {
                    seriesOptions.dataGrouping = dataGrouping;
                }, false);
            }

            if (redraw) {
                this.chart.redraw();
            }
        };



        /* ****************************************************************************
         * End data grouping module												   *
         ******************************************************************************/

    }(Highcharts));
    (function(H) {
        /**
         * (c) 2010-2017 Torstein Honsi
         *
         * License: www.highcharts.com/license
         */
        var each = H.each,
            Point = H.Point,
            seriesType = H.seriesType,
            seriesTypes = H.seriesTypes;

        /**
         * The ohlc series type.
         *
         * @constructor seriesTypes.ohlc
         * @augments seriesTypes.column
         */
        /**
         * An OHLC chart is a style of financial chart used to describe price
         * movements over time. It displays open, high, low and close values per data
         * point.
         *
         * @sample stock/demo/ohlc/ OHLC chart
         * @extends {plotOptions.column}
         * @excluding borderColor,borderRadius,borderWidth,crisp
         * @product highstock
         * @optionparent plotOptions.ohlc
         */
        seriesType('ohlc', 'column', {

            /**
             * The approximate pixel width of each group. If for example a series
             * with 30 points is displayed over a 600 pixel wide plot area, no grouping
             * is performed. If however the series contains so many points that
             * the spacing is less than the groupPixelWidth, Highcharts will try
             * to group it into appropriate groups so that each is more or less
             * two pixels wide. Defaults to `5`.
             * 
             * @type {Number}
             * @default 5
             * @product highstock
             * @apioption plotOptions.ohlc.dataGrouping.groupPixelWidth
             */

            /**
             * The pixel width of the line/border. Defaults to `1`.
             * 
             * @type {Number}
             * @sample {highstock} stock/plotoptions/ohlc-linewidth/
             *         A greater line width
             * @default 1
             * @product highstock
             */
            lineWidth: 1,

            tooltip: {


                pointFormat: '<span style="color:{point.color}">\u25CF</span> <b> {series.name}</b><br/>' + // eslint-disable-line max-len
                    'Open: {point.open}<br/>' +
                    'High: {point.high}<br/>' +
                    'Low: {point.low}<br/>' +
                    'Close: {point.close}<br/>'

            },

            threshold: null,


            states: {

                /**
                 * @extends plotOptions.column.states.hover
                 * @product highstock
                 */
                hover: {

                    /**
                     * The pixel width of the line representing the OHLC point.
                     * 
                     * @type {Number}
                     * @default 3
                     * @product highstock
                     */
                    lineWidth: 3
                }
            },


            /**
             * Line color for up points.
             * 
             * @type {Color}
             * @product highstock
             * @apioption plotOptions.ohlc.upColor
             */



            stickyTracking: true

        }, /** @lends seriesTypes.ohlc */ {
            directTouch: false,
            pointArrayMap: ['open', 'high', 'low', 'close'],
            toYData: function(point) { // return a plain array for speedy calculation
                return [point.open, point.high, point.low, point.close];
            },
            pointValKey: 'close',


            pointAttrToOptions: {
                'stroke': 'color',
                'stroke-width': 'lineWidth'
            },

            /**
             * Postprocess mapping between options and SVG attributes
             */
            pointAttribs: function(point, state) {
                var attribs = seriesTypes.column.prototype.pointAttribs.call(
                        this,
                        point,
                        state
                    ),
                    options = this.options;

                delete attribs.fill;

                if (!point.options.color &&
                    options.upColor &&
                    point.open < point.close
                ) {
                    attribs.stroke = options.upColor;
                }

                return attribs;
            },


            /**
             * Translate data points from raw values x and y to plotX and plotY
             */
            translate: function() {
                var series = this,
                    yAxis = series.yAxis,
                    hasModifyValue = !!series.modifyValue,
                    translated = [
                        'plotOpen',
                        'plotHigh',
                        'plotLow',
                        'plotClose',
                        'yBottom'
                    ]; // translate OHLC for

                seriesTypes.column.prototype.translate.apply(series);

                // Do the translation
                each(series.points, function(point) {
                    each(
                        [point.open, point.high, point.low, point.close, point.low],
                        function(value, i) {
                            if (value !== null) {
                                if (hasModifyValue) {
                                    value = series.modifyValue(value);
                                }
                                point[translated[i]] = yAxis.toPixels(value, true);
                            }
                        }
                    );

                    // Align the tooltip to the high value to avoid covering the point
                    point.tooltipPos[1] =
                        point.plotHigh + yAxis.pos - series.chart.plotTop;
                });
            },

            /**
             * Draw the data points
             */
            drawPoints: function() {
                var series = this,
                    points = series.points,
                    chart = series.chart;


                each(points, function(point) {
                    var plotOpen,
                        plotClose,
                        crispCorr,
                        halfWidth,
                        path,
                        graphic = point.graphic,
                        crispX,
                        isNew = !graphic;

                    if (point.plotY !== undefined) {

                        // Create and/or update the graphic
                        if (!graphic) {
                            point.graphic = graphic = chart.renderer.path()
                                .add(series.group);
                        }


                        graphic.attr(
                            series.pointAttribs(point, point.selected && 'select')
                        ); // #3897


                        // crisp vector coordinates
                        crispCorr = (graphic.strokeWidth() % 2) / 2;
                        crispX = Math.round(point.plotX) - crispCorr; // #2596
                        halfWidth = Math.round(point.shapeArgs.width / 2);

                        // the vertical stem
                        path = [
                            'M',
                            crispX, Math.round(point.yBottom),
                            'L',
                            crispX, Math.round(point.plotHigh)
                        ];

                        // open
                        if (point.open !== null) {
                            plotOpen = Math.round(point.plotOpen) + crispCorr;
                            path.push(
                                'M',
                                crispX,
                                plotOpen,
                                'L',
                                crispX - halfWidth,
                                plotOpen
                            );
                        }

                        // close
                        if (point.close !== null) {
                            plotClose = Math.round(point.plotClose) + crispCorr;
                            path.push(
                                'M',
                                crispX,
                                plotClose,
                                'L',
                                crispX + halfWidth,
                                plotClose
                            );
                        }

                        graphic[isNew ? 'attr' : 'animate']({
                                d: path
                            })
                            .addClass(point.getClassName(), true);

                    }


                });

            },

            animate: null // Disable animation

            /**
             * @constructor seriesTypes.ohlc.prototype.pointClass
             * @extends {Point}
             */
        }, /** @lends seriesTypes.ohlc.prototype.pointClass.prototype */ {
            /**
             * Extend the parent method by adding up or down to the class name.
             */
            getClassName: function() {
                return Point.prototype.getClassName.call(this) +
                    (
                        this.open < this.close ?
                        ' highcharts-point-up' :
                        ' highcharts-point-down'
                    );
            }
        });

        /**
         * A `ohlc` series. If the [type](#series.ohlc.type) option is not
         * specified, it is inherited from [chart.type](#chart.type).
         * 
         * For options that apply to multiple series, it is recommended to add
         * them to the [plotOptions.series](#plotOptions.series) options structure.
         * To apply to all series of this specific type, apply it to [plotOptions.
         * ohlc](#plotOptions.ohlc).
         * 
         * @type {Object}
         * @extends series,plotOptions.ohlc
         * @excluding dataParser,dataURL
         * @product highstock
         * @apioption series.ohlc
         */

        /**
         * An array of data points for the series. For the `ohlc` series type,
         * points can be given in the following ways:
         * 
         * 1.  An array of arrays with 5 or 4 values. In this case, the values
         * correspond to `x,open,high,low,close`. If the first value is a string,
         * it is applied as the name of the point, and the `x` value is inferred.
         * The `x` value can also be omitted, in which case the inner arrays
         * should be of length 4\. Then the `x` value is automatically calculated,
         * either starting at 0 and incremented by 1, or from `pointStart`
         * and `pointInterval` given in the series options.
         * 
         *  ```js
         *     data: [
         *         [0, 6, 5, 6, 7],
         *         [1, 9, 4, 8, 2],
         *         [2, 6, 3, 4, 10]
         *     ]
         *  ```
         * 
         * 2.  An array of objects with named values. The objects are point
         * configuration objects as seen below. If the total number of data
         * points exceeds the series' [turboThreshold](#series.ohlc.turboThreshold),
         * this option is not available.
         * 
         *  ```js
         *     data: [{
         *         x: 1,
         *         open: 3,
         *         high: 4,
         *         low: 5,
         *         close: 2,
         *         name: "Point2",
         *         color: "#00FF00"
         *     }, {
         *         x: 1,
         *         open: 4,
         *         high: 3,
         *         low: 6,
         *         close: 7,
         *         name: "Point1",
         *         color: "#FF00FF"
         *     }]
         *  ```
         * 
         * @type {Array<Object|Array>}
         * @extends series.arearange.data
         * @excluding y,marker
         * @product highstock
         * @apioption series.ohlc.data
         */

        /**
         * The closing value of each data point.
         * 
         * @type {Number}
         * @product highstock
         * @apioption series.ohlc.data.close
         */

        /**
         * The opening value of each data point.
         * 
         * @type {Number}
         * @product highstock
         * @apioption series.ohlc.data.open
         */

    }(Highcharts));
    (function(H) {
        /**
         * (c) 2010-2017 Torstein Honsi
         *
         * License: www.highcharts.com/license
         */
        var defaultPlotOptions = H.defaultPlotOptions,
            each = H.each,
            merge = H.merge,
            seriesType = H.seriesType,
            seriesTypes = H.seriesTypes;

        /**
         * A candlestick chart is a style of financial chart used to describe price
         * movements over time.
         *
         * @sample stock/demo/candlestick/ Candlestick chart
         * 
         * @extends {plotOptions.ohlc}
         * @excluding borderColor,borderRadius,borderWidth
         * @product highstock
         * @optionparent plotOptions.candlestick
         */
        var candlestickOptions = {

            states: {

                /**
                 * @extends plotOptions.column.states.hover
                 * @product highstock
                 */
                hover: {

                    /**
                     * The pixel width of the line/border around the candlestick.
                     * 
                     * @type {Number}
                     * @default 2
                     * @product highstock
                     */
                    lineWidth: 2
                }
            },

            /**
             * @extends {plotOptions.ohlc.tooltip}
             */
            tooltip: defaultPlotOptions.ohlc.tooltip,

            threshold: null,


            /**
             * The color of the line/border of the candlestick.
             * 
             * In styled mode, the line stroke can be set with the `.highcharts-
             * candlestick-series .highcahrts-point` rule.
             * 
             * @type {Color}
             * @see [upLineColor](#plotOptions.candlestick.upLineColor)
             * @sample {highstock} stock/plotoptions/candlestick-linecolor/
             *         Candlestick line colors
             * @default #000000
             * @product highstock
             */
            lineColor: '#000000',

            /**
             * The pixel width of the candlestick line/border. Defaults to `1`.
             * 
             * 
             * In styled mode, the line stroke width can be set with the `.
             * highcharts-candlestick-series .highcahrts-point` rule.
             * 
             * @type {Number}
             * @default 1
             * @product highstock
             */
            lineWidth: 1,

            /**
             * The fill color of the candlestick when values are rising.
             * 
             * In styled mode, the up color can be set with the `.highcharts-
             * candlestick-series .highcharts-point-up` rule.
             * 
             * @type {Color}
             * @sample {highstock} stock/plotoptions/candlestick-color/ Custom colors
             * @sample {highstock} highcharts/css/candlestick/ Colors in styled mode
             * @default #ffffff
             * @product highstock
             */
            upColor: '#ffffff',

            stickyTracking: true

            /**
             * The specific line color for up candle sticks. The default is to inherit
             * the general `lineColor` setting.
             * 
             * @type {Color}
             * @sample {highstock} stock/plotoptions/candlestick-linecolor/ Candlestick line colors
             * @default null
             * @since 1.3.6
             * @product highstock
             * @apioption plotOptions.candlestick.upLineColor
             */


            /**
             * @default ohlc
             * @apioption plotOptions.candlestick.dataGrouping.approximation
             */

        };

        /**
         * The candlestick series type.
         *
         * @constructor seriesTypes.candlestick
         * @augments seriesTypes.ohlc
         */
        seriesType('candlestick', 'ohlc', merge(
            defaultPlotOptions.column,
            candlestickOptions
        ), /** @lends seriesTypes.candlestick */ {

            /**
             * Postprocess mapping between options and SVG attributes
             */
            pointAttribs: function(point, state) {
                var attribs = seriesTypes.column.prototype.pointAttribs.call(this, point, state),
                    options = this.options,
                    isUp = point.open < point.close,
                    stroke = options.lineColor || this.color,
                    stateOptions;

                attribs['stroke-width'] = options.lineWidth;

                attribs.fill = point.options.color || (isUp ? (options.upColor || this.color) : this.color);
                attribs.stroke = point.lineColor || (isUp ? (options.upLineColor || stroke) : stroke);

                // Select or hover states
                if (state) {
                    stateOptions = options.states[state];
                    attribs.fill = stateOptions.color || attribs.fill;
                    attribs.stroke = stateOptions.lineColor || attribs.stroke;
                    attribs['stroke-width'] =
                        stateOptions.lineWidth || attribs['stroke-width'];
                }


                return attribs;
            },

            /**
             * Draw the data points
             */
            drawPoints: function() {
                var series = this,
                    points = series.points,
                    chart = series.chart;


                each(points, function(point) {

                    var graphic = point.graphic,
                        plotOpen,
                        plotClose,
                        topBox,
                        bottomBox,
                        hasTopWhisker,
                        hasBottomWhisker,
                        crispCorr,
                        crispX,
                        path,
                        halfWidth,
                        isNew = !graphic;

                    if (point.plotY !== undefined) {

                        if (!graphic) {
                            point.graphic = graphic = chart.renderer.path()
                                .add(series.group);
                        }


                        graphic
                            .attr(series.pointAttribs(point, point.selected && 'select')) // #3897
                            .shadow(series.options.shadow);


                        // Crisp vector coordinates
                        crispCorr = (graphic.strokeWidth() % 2) / 2;
                        crispX = Math.round(point.plotX) - crispCorr; // #2596
                        plotOpen = point.plotOpen;
                        plotClose = point.plotClose;
                        topBox = Math.min(plotOpen, plotClose);
                        bottomBox = Math.max(plotOpen, plotClose);
                        halfWidth = Math.round(point.shapeArgs.width / 2);
                        hasTopWhisker = Math.round(topBox) !== Math.round(point.plotHigh);
                        hasBottomWhisker = bottomBox !== point.yBottom;
                        topBox = Math.round(topBox) + crispCorr;
                        bottomBox = Math.round(bottomBox) + crispCorr;

                        // Create the path. Due to a bug in Chrome 49, the path is first instanciated
                        // with no values, then the values pushed. For unknown reasons, instanciated
                        // the path array with all the values would lead to a crash when updating
                        // frequently (#5193).
                        path = [];
                        path.push(
                            'M',
                            crispX - halfWidth, bottomBox,
                            'L',
                            crispX - halfWidth, topBox,
                            'L',
                            crispX + halfWidth, topBox,
                            'L',
                            crispX + halfWidth, bottomBox,
                            'Z', // Use a close statement to ensure a nice rectangle #2602
                            'M',
                            crispX, topBox,
                            'L',
                            crispX, hasTopWhisker ? Math.round(point.plotHigh) : topBox, // #460, #2094
                            'M',
                            crispX, bottomBox,
                            'L',
                            crispX, hasBottomWhisker ? Math.round(point.yBottom) : bottomBox // #460, #2094
                        );

                        graphic[isNew ? 'attr' : 'animate']({
                                d: path
                            })
                            .addClass(point.getClassName(), true);

                    }
                });

            }


        });

        /**
         * A `candlestick` series. If the [type](#series.candlestick.type)
         * option is not specified, it is inherited from [chart.type](#chart.
         * type).
         * 
         * For options that apply to multiple series, it is recommended to add
         * them to the [plotOptions.series](#plotOptions.series) options structure.
         * To apply to all series of this specific type, apply it to [plotOptions.
         * candlestick](#plotOptions.candlestick).
         * 
         * @type {Object}
         * @extends series,plotOptions.candlestick
         * @excluding dataParser,dataURL
         * @product highstock
         * @apioption series.candlestick
         */

        /**
         * An array of data points for the series. For the `candlestick` series
         * type, points can be given in the following ways:
         * 
         * 1.  An array of arrays with 5 or 4 values. In this case, the values
         * correspond to `x,open,high,low,close`. If the first value is a string,
         * it is applied as the name of the point, and the `x` value is inferred.
         * The `x` value can also be omitted, in which case the inner arrays
         * should be of length 4\. Then the `x` value is automatically calculated,
         * either starting at 0 and incremented by 1, or from `pointStart`
         * and `pointInterval` given in the series options.
         * 
         *  ```js
         *     data: [
         *         [0, 7, 2, 0, 4],
         *         [1, 1, 4, 2, 8],
         *         [2, 3, 3, 9, 3]
         *     ]
         *  ```
         * 
         * 2.  An array of objects with named values. The objects are point
         * configuration objects as seen below. If the total number of data
         * points exceeds the series' [turboThreshold](#series.candlestick.
         * turboThreshold), this option is not available.
         * 
         *  ```js
         *     data: [{
         *         x: 1,
         *         open: 9,
         *         high: 2,
         *         low: 4,
         *         close: 6,
         *         name: "Point2",
         *         color: "#00FF00"
         *     }, {
         *         x: 1,
         *         open: 1,
         *         high: 4,
         *         low: 7,
         *         close: 7,
         *         name: "Point1",
         *         color: "#FF00FF"
         *     }]
         *  ```
         * 
         * @type {Array<Object|Array>}
         * @extends series.ohlc.data
         * @excluding y
         * @product highstock
         * @apioption series.candlestick.data
         */

    }(Highcharts));
    var onSeriesMixin = (function(H) {
        /**
         * (c) 2010-2017 Torstein Honsi
         *
         * License: www.highcharts.com/license
         */

        var each = H.each,
            seriesTypes = H.seriesTypes,
            stableSort = H.stableSort;

        var onSeriesMixin = {
            /**
             * Extend the translate method by placing the point on the related series
             */
            translate: function() {

                seriesTypes.column.prototype.translate.apply(this);

                var series = this,
                    options = series.options,
                    chart = series.chart,
                    points = series.points,
                    cursor = points.length - 1,
                    point,
                    lastPoint,
                    optionsOnSeries = options.onSeries,
                    onSeries = optionsOnSeries && chart.get(optionsOnSeries),
                    onKey = options.onKey || 'y',
                    step = onSeries && onSeries.options.step,
                    onData = onSeries && onSeries.points,
                    i = onData && onData.length,
                    xAxis = series.xAxis,
                    yAxis = series.yAxis,
                    xAxisExt = xAxis.getExtremes(),
                    xOffset = 0,
                    leftPoint,
                    lastX,
                    rightPoint,
                    currentDataGrouping,
                    distanceRatio;

                // relate to a master series
                if (onSeries && onSeries.visible && i) {
                    xOffset = (onSeries.pointXOffset || 0) + (onSeries.barW || 0) / 2;
                    currentDataGrouping = onSeries.currentDataGrouping;
                    lastX = (
                        onData[i - 1].x +
                        (currentDataGrouping ? currentDataGrouping.totalRange : 0)
                    ); // #2374

                    // sort the data points
                    stableSort(points, function(a, b) {
                        return (a.x - b.x);
                    });

                    onKey = 'plot' + onKey[0].toUpperCase() + onKey.substr(1);
                    while (i-- && points[cursor]) {
                        leftPoint = onData[i];
                        point = points[cursor];
                        point.y = leftPoint.y;

                        if (leftPoint.x <= point.x && leftPoint[onKey] !== undefined) {
                            if (point.x <= lastX) { // #803

                                point.plotY = leftPoint[onKey];

                                // interpolate between points, #666
                                if (leftPoint.x < point.x && !step) {
                                    rightPoint = onData[i + 1];
                                    if (rightPoint && rightPoint[onKey] !== undefined) {
                                        // the distance ratio, between 0 and 1
                                        distanceRatio = (point.x - leftPoint.x) /
                                            (rightPoint.x - leftPoint.x);
                                        point.plotY +=
                                            distanceRatio *
                                            // the plotY distance
                                            (rightPoint[onKey] - leftPoint[onKey]);
                                        point.y +=
                                            distanceRatio *
                                            (rightPoint.y - leftPoint.y);
                                    }
                                }
                            }
                            cursor--;
                            i++; // check again for points in the same x position
                            if (cursor < 0) {
                                break;
                            }
                        }
                    }
                }

                // Add plotY position and handle stacking
                each(points, function(point, i) {

                    var stackIndex;

                    // Undefined plotY means the point is either on axis, outside series
                    // range or hidden series. If the series is outside the range of the
                    // x axis it should fall through with an undefined plotY, but then
                    // we must remove the shapeArgs (#847).
                    if (point.plotY === undefined) {
                        if (point.x >= xAxisExt.min && point.x <= xAxisExt.max) {
                            // we're inside xAxis range
                            point.plotY = chart.chartHeight - xAxis.bottom -
                                (xAxis.opposite ? xAxis.height : 0) +
                                xAxis.offset - yAxis.top; // #3517
                        } else {
                            point.shapeArgs = {}; // 847
                        }
                    }
                    point.plotX += xOffset; // #2049
                    // if multiple flags appear at the same x, order them into a stack
                    lastPoint = points[i - 1];
                    if (lastPoint && lastPoint.plotX === point.plotX) {
                        if (lastPoint.stackIndex === undefined) {
                            lastPoint.stackIndex = 0;
                        }
                        stackIndex = lastPoint.stackIndex + 1;
                    }
                    point.stackIndex = stackIndex; // #3639
                });


            }
        };
        return onSeriesMixin;
    }(Highcharts));
    (function(H, onSeriesMixin) {
        /**
         * (c) 2010-2017 Torstein Honsi
         *
         * License: www.highcharts.com/license
         */
        var addEvent = H.addEvent,
            each = H.each,
            merge = H.merge,
            noop = H.noop,
            Renderer = H.Renderer,
            Series = H.Series,
            seriesType = H.seriesType,
            SVGRenderer = H.SVGRenderer,
            TrackerMixin = H.TrackerMixin,
            VMLRenderer = H.VMLRenderer,
            symbols = SVGRenderer.prototype.symbols;

        /**
         * The Flags series.
         * @constructor seriesTypes.flags
         * @augments seriesTypes.column
         */
        /**
         * Flags are used to mark events in stock charts. They can be added on the
         * timeline, or attached to a specific series.
         *
         * @sample stock/demo/flags-general/ Flags on a line series
         * @extends {plotOptions.column}
         * @excluding animation,borderColor,borderRadius,borderWidth,colorByPoint,dataGrouping,pointPadding,pointWidth,turboThreshold
         * @product highstock
         * @optionparent plotOptions.flags
         */
        seriesType('flags', 'column', {

            /**
             * In case the flag is placed on a series, on what point key to place
             * it. Line and columns have one key, `y`. In range or OHLC-type series,
             * however, the flag can optionally be placed on the `open`, `high`,
             *  `low` or `close` key.
             * 
             * @validvalue ["y", "open", "high", "low", "close"]
             * @type {String}
             * @sample {highstock} stock/plotoptions/flags-onkey/ Range series, flag on high
             * @default y
             * @since 4.2.2
             * @product highstock
             * @apioption plotOptions.flags.onKey
             */

            /**
             * The id of the series that the flags should be drawn on. If no id
             * is given, the flags are drawn on the x axis.
             * 
             * @type {String}
             * @sample {highstock} stock/plotoptions/flags/ Flags on series and on x axis
             * @default undefined
             * @product highstock
             * @apioption plotOptions.flags.onSeries
             */

            pointRange: 0, // #673

            /**
             * Whether the flags are allowed to overlap sideways. If `false`, the flags
             * are moved sideways using an algorithm that seeks to place every flag as
             * close as possible to its original position.
             *
             * @sample {highstock} stock/plotoptions/flags-allowoverlapx
             *         Allow sideways overlap
             *
             * @since 6.0.4
             */
            allowOverlapX: false,

            /**
             * The shape of the marker. Can be one of "flag", "circlepin", "squarepin",
             * or an image on the format `url(/path-to-image.jpg)`. Individual
             * shapes can also be set for each point.
             * 
             * @validvalue ["flag", "circlepin", "squarepin"]
             * @type {String}
             * @sample {highstock} stock/plotoptions/flags/ Different shapes
             * @default flag
             * @product highstock
             */
            shape: 'flag',

            /**
             * When multiple flags in the same series fall on the same value, this
             * number determines the vertical offset between them.
             * 
             * @type {Number}
             * @sample {highstock} stock/plotoptions/flags-stackdistance/ A greater stack distance
             * @default 12
             * @product highstock
             */
            stackDistance: 12,

            /**
             * Text alignment for the text inside the flag.
             * 
             * @validvalue ["left", "center", "right"]
             * @type {String}
             * @default center
             * @since 5.0.0
             * @product highstock
             */
            textAlign: 'center',

            /**
             * Specific tooltip options for flag series. Flag series tooltips are
             * different from most other types in that a flag doesn't have a data
             * value, so the tooltip rather displays the `text` option for each
             * point.
             * 
             * @type {Object}
             * @extends plotOptions.series.tooltip
             * @excluding changeDecimals,valueDecimals,valuePrefix,valueSuffix
             * @product highstock
             */
            tooltip: {
                pointFormat: '{point.text}<br/>'
            },

            threshold: null,

            /**
             * The text to display on each flag. This can be defined on series level,
             *  or individually for each point. Defaults to `"A"`.
             * 
             * @type {String}
             * @default A
             * @product highstock
             * @apioption plotOptions.flags.title
             */

            /**
             * The y position of the top left corner of the flag relative to either
             * the series (if onSeries is defined), or the x axis. Defaults to
             * `-30`.
             * 
             * @type {Number}
             * @default -30
             * @product highstock
             */
            y: -30,

            /**
             * Whether to use HTML to render the flag texts. Using HTML allows for
             * advanced formatting, images and reliable bi-directional text rendering.
             * Note that exported images won't respect the HTML, and that HTML
             * won't respect Z-index settings.
             * 
             * @type {Boolean}
             * @default false
             * @since 1.3
             * @product highstock
             * @apioption plotOptions.flags.useHTML
             */



            /**
             * The fill color for the flags.
             */
            fillColor: '#ffffff',

            /**
             * The color of the line/border of the flag.
             * 
             * In styled mode, the stroke is set in the `.highcharts-flag-series
             * .highcharts-point` rule.
             * 
             * @type {Color}
             * @default #000000
             * @product highstock
             * @apioption plotOptions.flags.lineColor
             */

            /**
             * The pixel width of the flag's line/border.
             * 
             * @type {Number}
             * @default 1
             * @product highstock
             */
            lineWidth: 1,

            states: {

                /**
                 * @extends plotOptions.column.states.hover
                 * @product highstock
                 */
                hover: {

                    /**
                     * The color of the line/border of the flag.
                     * 
                     * @product highstock
                     */
                    lineColor: '#000000',

                    /**
                     * The fill or background color of the flag.
                     * 
                     * @product highstock
                     */
                    fillColor: '#ccd6eb'
                }
            },

            /**
             * The text styles of the flag.
             * 
             * In styled mode, the styles are set in the `.highcharts-flag-
             * series .highcharts-point` rule.
             * 
             * @type {CSSObject}
             * @default { "fontSize": "11px", "fontWeight": "bold" }
             * @product highstock
             */
            style: {
                fontSize: '11px',
                fontWeight: 'bold'
            }


        }, /** @lends seriesTypes.flags.prototype */ {
            sorted: false,
            noSharedTooltip: true,
            allowDG: false,
            takeOrdinalPosition: false, // #1074
            trackerGroups: ['markerGroup'],
            forceCrop: true,
            /**
             * Inherit the initialization from base Series.
             */
            init: Series.prototype.init,


            /**
             * Get presentational attributes
             */
            pointAttribs: function(point, state) {
                var options = this.options,
                    color = (point && point.color) || this.color,
                    lineColor = options.lineColor,
                    lineWidth = (point && point.lineWidth),
                    fill = (point && point.fillColor) || options.fillColor;

                if (state) {
                    fill = options.states[state].fillColor;
                    lineColor = options.states[state].lineColor;
                    lineWidth = options.states[state].lineWidth;
                }

                return {
                    'fill': fill || color,
                    'stroke': lineColor || color,
                    'stroke-width': lineWidth || options.lineWidth || 0
                };
            },


            translate: onSeriesMixin.translate,

            /**
             * Draw the markers
             */
            drawPoints: function() {
                var series = this,
                    points = series.points,
                    chart = series.chart,
                    renderer = chart.renderer,
                    plotX,
                    plotY,
                    options = series.options,
                    optionsY = options.y,
                    shape,
                    i,
                    point,
                    graphic,
                    stackIndex,
                    anchorY,
                    attribs,
                    outsideRight,
                    yAxis = series.yAxis,
                    boxesMap = {},
                    boxes = [];

                i = points.length;
                while (i--) {
                    point = points[i];
                    outsideRight = point.plotX > series.xAxis.len;
                    plotX = point.plotX;
                    stackIndex = point.stackIndex;
                    shape = point.options.shape || options.shape;
                    plotY = point.plotY;

                    if (plotY !== undefined) {
                        plotY = point.plotY + optionsY - (stackIndex !== undefined && stackIndex * options.stackDistance);
                    }
                    point.anchorX = stackIndex ? undefined : point.plotX; // skip connectors for higher level stacked points
                    anchorY = stackIndex ? undefined : point.plotY;

                    graphic = point.graphic;

                    // Only draw the point if y is defined and the flag is within the visible area
                    if (plotY !== undefined && plotX >= 0 && !outsideRight) {

                        // Create the flag
                        if (!graphic) {
                            graphic = point.graphic = renderer.label(
                                    '',
                                    null,
                                    null,
                                    shape,
                                    null,
                                    null,
                                    options.useHTML
                                )

                                .attr(series.pointAttribs(point))
                                .css(merge(options.style, point.style))

                                .attr({
                                    align: shape === 'flag' ? 'left' : 'center',
                                    width: options.width,
                                    height: options.height,
                                    'text-align': options.textAlign
                                })
                                .addClass('highcharts-point')
                                .add(series.markerGroup);

                            // Add reference to the point for tracker (#6303)
                            if (point.graphic.div) {
                                point.graphic.div.point = point;
                            }


                            graphic.shadow(options.shadow);

                            graphic.isNew = true;
                        }

                        if (plotX > 0) { // #3119
                            plotX -= graphic.strokeWidth() % 2; // #4285
                        }

                        // Plant the flag
                        attribs = {
                            y: plotY,
                            anchorY: anchorY
                        };
                        if (options.allowOverlapX) {
                            attribs.x = plotX;
                            attribs.anchorX = point.anchorX;
                        }
                        graphic.attr({
                            text: point.options.title || options.title || 'A'
                        })[graphic.isNew ? 'attr' : 'animate'](attribs);

                        // Rig for the distribute function
                        if (!options.allowOverlapX) {
                            if (!boxesMap[point.plotX]) {
                                boxesMap[point.plotX] = {
                                    align: 0,
                                    size: graphic.width,
                                    target: plotX,
                                    anchorX: plotX
                                };
                            } else {
                                boxesMap[point.plotX].size = Math.max(
                                    boxesMap[point.plotX].size,
                                    graphic.width
                                );
                            }
                        }

                        // Set the tooltip anchor position
                        point.tooltipPos = chart.inverted ? [yAxis.len + yAxis.pos - chart.plotLeft - plotY, series.xAxis.len - plotX] : [plotX, plotY + yAxis.pos - chart.plotTop]; // #6327

                    } else if (graphic) {
                        point.graphic = graphic.destroy();
                    }

                }

                // Handle X-dimension overlapping
                if (!options.allowOverlapX) {
                    H.objectEach(boxesMap, function(box) {
                        box.plotX = box.anchorX;
                        boxes.push(box);
                    });

                    H.distribute(boxes, this.xAxis.len);

                    each(points, function(point) {
                        var box = point.graphic && boxesMap[point.plotX];
                        if (box) {
                            point.graphic[point.graphic.isNew ? 'attr' : 'animate']({
                                x: box.pos,
                                anchorX: point.anchorX
                            });
                            point.graphic.isNew = false;
                        }
                    });
                }

                // Might be a mix of SVG and HTML and we need events for both (#6303)
                if (options.useHTML) {
                    H.wrap(series.markerGroup, 'on', function(proceed) {
                        return H.SVGElement.prototype.on.apply(
                            proceed.apply(this, [].slice.call(arguments, 1)), // for HTML
                            [].slice.call(arguments, 1)); // and for SVG
                    });
                }

            },

            /**
             * Extend the column trackers with listeners to expand and contract stacks
             */
            drawTracker: function() {
                var series = this,
                    points = series.points;

                TrackerMixin.drawTrackerPoint.apply(this);

                // Bring each stacked flag up on mouse over, this allows readability of vertically
                // stacked elements as well as tight points on the x axis. #1924.
                each(points, function(point) {
                    var graphic = point.graphic;
                    if (graphic) {
                        addEvent(graphic.element, 'mouseover', function() {

                            // Raise this point
                            if (point.stackIndex > 0 && !point.raised) {
                                point._y = graphic.y;
                                graphic.attr({
                                    y: point._y - 8
                                });
                                point.raised = true;
                            }

                            // Revert other raised points
                            each(points, function(otherPoint) {
                                if (otherPoint !== point && otherPoint.raised && otherPoint.graphic) {
                                    otherPoint.graphic.attr({
                                        y: otherPoint._y
                                    });
                                    otherPoint.raised = false;
                                }
                            });
                        });
                    }
                });
            },

            animate: noop, // Disable animation
            buildKDTree: noop,
            setClip: noop

        });

        // create the flag icon with anchor
        symbols.flag = function(x, y, w, h, options) {
            var anchorX = (options && options.anchorX) || x,
                anchorY = (options && options.anchorY) || y;

            return symbols.circle(anchorX - 1, anchorY - 1, 2, 2).concat(
                [
                    'M', anchorX, anchorY,
                    'L', x, y + h,
                    x, y,
                    x + w, y,
                    x + w, y + h,
                    x, y + h,
                    'Z'
                ]
            );
        };

        /*
         * Create the circlepin and squarepin icons with anchor
         */
        function createPinSymbol(shape) {
            symbols[shape + 'pin'] = function(x, y, w, h, options) {

                var anchorX = options && options.anchorX,
                    anchorY = options && options.anchorY,
                    path,
                    labelTopOrBottomY;

                // For single-letter flags, make sure circular flags are not taller than their width
                if (shape === 'circle' && h > w) {
                    x -= Math.round((h - w) / 2);
                    w = h;
                }

                path = symbols[shape](x, y, w, h);

                if (anchorX && anchorY) {
                    // if the label is below the anchor, draw the connecting line from the top edge of the label
                    // otherwise start drawing from the bottom edge
                    labelTopOrBottomY = (y > anchorY) ? y : y + h;
                    path.push(
                        'M',
                        shape === 'circle' ? path[1] - path[4] : path[1] + path[4] / 2,
                        labelTopOrBottomY,
                        'L',
                        anchorX,
                        anchorY
                    );
                    path = path.concat(
                        symbols.circle(anchorX - 1, anchorY - 1, 2, 2)
                    );
                }

                return path;
            };
        }
        createPinSymbol('circle');
        createPinSymbol('square');


        // The symbol callbacks are generated on the SVGRenderer object in all browsers. Even
        // VML browsers need this in order to generate shapes in export. Now share
        // them with the VMLRenderer.
        if (Renderer === VMLRenderer) {
            each(['flag', 'circlepin', 'squarepin'], function(shape) {
                VMLRenderer.prototype.symbols[shape] = symbols[shape];
            });
        }


        /**
         * A `flags` series. If the [type](#series.flags.type) option is not
         * specified, it is inherited from [chart.type](#chart.type).
         * 
         * For options that apply to multiple series, it is recommended to add
         * them to the [plotOptions.series](#plotOptions.series) options structure.
         * To apply to all series of this specific type, apply it to [plotOptions.
         * flags](#plotOptions.flags).
         * 
         * @type {Object}
         * @extends series,plotOptions.flags
         * @excluding dataParser,dataURL
         * @product highstock
         * @apioption series.flags
         */

        /**
         * An array of data points for the series. For the `flags` series type,
         * points can be given in the following ways:
         * 
         * 1.  An array of objects with named values. The objects are point
         * configuration objects as seen below. If the total number of data
         * points exceeds the series' [turboThreshold](#series.flags.turboThreshold),
         * this option is not available.
         * 
         *  ```js
         *     data: [{
         *     x: 1,
         *     title: "A",
         *     text: "First event"
         * }, {
         *     x: 1,
         *     title: "B",
         *     text: "Second event"
         * }]</pre>
         * 
         * @type {Array<Object>}
         * @extends series.line.data
         * @excluding y,dataLabels,marker,name
         * @product highstock
         * @apioption series.flags.data
         */

        /**
         * The fill color of an individual flag. By default it inherits from
         * the series color.
         * 
         * @type {Color}
         * @product highstock
         * @apioption series.flags.data.fillColor
         */

        /**
         * The longer text to be shown in the flag's tooltip.
         * 
         * @type {String}
         * @product highstock
         * @apioption series.flags.data.text
         */

        /**
         * The short text to be shown on the flag.
         * 
         * @type {String}
         * @product highstock
         * @apioption series.flags.data.title
         */


    }(Highcharts, onSeriesMixin));
    (function(H) {
        /**
         * (c) 2010-2017 Torstein Honsi
         *
         * License: www.highcharts.com/license
         */
        var addEvent = H.addEvent,
            Axis = H.Axis,
            correctFloat = H.correctFloat,
            defaultOptions = H.defaultOptions,
            defined = H.defined,
            destroyObjectProperties = H.destroyObjectProperties,
            each = H.each,
            fireEvent = H.fireEvent,
            hasTouch = H.hasTouch,
            isTouchDevice = H.isTouchDevice,
            merge = H.merge,
            pick = H.pick,
            removeEvent = H.removeEvent,
            svg = H.svg,
            wrap = H.wrap,
            swapXY;

        /**
         * 
         * The scrollbar is a means of panning over the X axis of a stock chart.
         * 
         * In styled mode, all the presentational options for the
         * scrollbar are replaced by the classes `.highcharts-scrollbar-thumb`,
         * `.highcharts-scrollbar-arrow`, `.highcharts-scrollbar-button`,
         * `.highcharts-scrollbar-rifles` and `.highcharts-scrollbar-track`.
         * 
         * @product highstock
         * @optionparent scrollbar
         */
        var defaultScrollbarOptions = {

            /**
             * The height of the scrollbar. The height also applies to the width
             * of the scroll arrows so that they are always squares. Defaults to
             * 20 for touch devices and 14 for mouse devices.
             * 
             * @type {Number}
             * @sample {highstock} stock/scrollbar/height/ A 30px scrollbar
             * @product highstock
             */
            height: isTouchDevice ? 20 : 14,

            /**
             * The border rounding radius of the bar.
             * 
             * @type {Number}
             * @sample {highstock} stock/scrollbar/style/ Scrollbar styling
             * @default 0
             * @product highstock
             */
            barBorderRadius: 0,

            /**
             * The corner radius of the scrollbar buttons.
             * 
             * @type {Number}
             * @sample {highstock} stock/scrollbar/style/ Scrollbar styling
             * @default 0
             * @product highstock
             */
            buttonBorderRadius: 0,

            /**
             * Whether to redraw the main chart as the scrollbar or the navigator
             * zoomed window is moved. Defaults to `true` for modern browsers and
             * `false` for legacy IE browsers as well as mobile devices.
             * 
             * @type {Boolean}
             * @since 1.3
             * @product highstock
             */
            liveRedraw: svg && !isTouchDevice,

            /**
             * The margin between the scrollbar and its axis when the scrollbar is
             * applied directly to an axis.
             */
            margin: 10,

            /**
             * The minimum width of the scrollbar.
             * 
             * @type {Number}
             * @default 6
             * @since 1.2.5
             * @product highstock
             */
            minWidth: 6,

            step: 0.2,

            /**
             * The z index of the scrollbar group.
             */
            zIndex: 3,


            /**
             * The background color of the scrollbar itself.
             * 
             * @type {Color}
             * @sample {highstock} stock/scrollbar/style/ Scrollbar styling
             * @default #cccccc
             * @product highstock
             */
            barBackgroundColor: '#cccccc',

            /**
             * The width of the bar's border.
             * 
             * @type {Number}
             * @sample {highstock} stock/scrollbar/style/ Scrollbar styling
             * @default 1
             * @product highstock
             */
            barBorderWidth: 1,

            /**
             * The color of the scrollbar's border.
             * 
             * @type {Color}
             * @default #cccccc
             * @product highstock
             */
            barBorderColor: '#cccccc',

            /**
             * The color of the small arrow inside the scrollbar buttons.
             * 
             * @type {Color}
             * @sample {highstock} stock/scrollbar/style/ Scrollbar styling
             * @default #333333
             * @product highstock
             */
            buttonArrowColor: '#333333',

            /**
             * The color of scrollbar buttons.
             * 
             * @type {Color}
             * @sample {highstock} stock/scrollbar/style/ Scrollbar styling
             * @default #e6e6e6
             * @product highstock
             */
            buttonBackgroundColor: '#e6e6e6',

            /**
             * The color of the border of the scrollbar buttons.
             * 
             * @type {Color}
             * @sample {highstock} stock/scrollbar/style/ Scrollbar styling
             * @default #cccccc
             * @product highstock
             */
            buttonBorderColor: '#cccccc',

            /**
             * The border width of the scrollbar buttons.
             * 
             * @type {Number}
             * @sample {highstock} stock/scrollbar/style/ Scrollbar styling
             * @default 1
             * @product highstock
             */
            buttonBorderWidth: 1,

            /**
             * The color of the small rifles in the middle of the scrollbar.
             * 
             * @type {Color}
             * @default #333333
             * @product highstock
             */
            rifleColor: '#333333',

            /**
             * The color of the track background.
             * 
             * @type {Color}
             * @sample {highstock} stock/scrollbar/style/ Scrollbar styling
             * @default #f2f2f2
             * @product highstock
             */
            trackBackgroundColor: '#f2f2f2',

            /**
             * The color of the border of the scrollbar track.
             * 
             * @type {Color}
             * @sample {highstock} stock/scrollbar/style/ Scrollbar styling
             * @default #f2f2f2
             * @product highstock
             */
            trackBorderColor: '#f2f2f2',

            /**
             * The width of the border of the scrollbar track.
             * 
             * @type {Number}
             * @sample {highstock} stock/scrollbar/style/ Scrollbar styling
             * @default 1
             * @product highstock
             */
            trackBorderWidth: 1

        };

        defaultOptions.scrollbar = merge(true, defaultScrollbarOptions, defaultOptions.scrollbar);

        /**
         * When we have vertical scrollbar, rifles and arrow in buttons should be rotated.
         * The same method is used in Navigator's handles, to rotate them.
         * @param {Array} path - path to be rotated
         * @param {Boolean} vertical - if vertical scrollbar, swap x-y values
         */
        H.swapXY = swapXY = function(path, vertical) {
            var i,
                len = path.length,
                temp;

            if (vertical) {
                for (i = 0; i < len; i += 3) {
                    temp = path[i + 1];
                    path[i + 1] = path[i + 2];
                    path[i + 2] = temp;
                }
            }

            return path;
        };

        /**
         * A reusable scrollbar, internally used in Highstock's navigator and optionally
         * on individual axes.
         *
         * @class
         * @param {Object} renderer
         * @param {Object} options
         * @param {Object} chart
         */
        function Scrollbar(renderer, options, chart) { // docs
            this.init(renderer, options, chart);
        }

        Scrollbar.prototype = {

            init: function(renderer, options, chart) {

                this.scrollbarButtons = [];

                this.renderer = renderer;

                this.userOptions = options;
                this.options = merge(defaultScrollbarOptions, options);

                this.chart = chart;

                this.size = pick(this.options.size, this.options.height); // backward compatibility

                // Init
                if (options.enabled) {
                    this.render();
                    this.initEvents();
                    this.addEvents();
                }
            },

            /**
             * Render scrollbar with all required items.
             */
            render: function() {
                var scroller = this,
                    renderer = scroller.renderer,
                    options = scroller.options,
                    size = scroller.size,
                    group;

                // Draw the scrollbar group
                scroller.group = group = renderer.g('scrollbar').attr({
                    zIndex: options.zIndex,
                    translateY: -99999
                }).add();

                // Draw the scrollbar track:
                scroller.track = renderer.rect()
                    .addClass('highcharts-scrollbar-track')
                    .attr({
                        x: 0,
                        r: options.trackBorderRadius || 0,
                        height: size,
                        width: size
                    }).add(group);


                scroller.track.attr({
                    fill: options.trackBackgroundColor,
                    stroke: options.trackBorderColor,
                    'stroke-width': options.trackBorderWidth
                });

                this.trackBorderWidth = scroller.track.strokeWidth();
                scroller.track.attr({
                    y: -this.trackBorderWidth % 2 / 2
                });


                // Draw the scrollbar itself
                scroller.scrollbarGroup = renderer.g().add(group);

                scroller.scrollbar = renderer.rect()
                    .addClass('highcharts-scrollbar-thumb')
                    .attr({
                        height: size,
                        width: size,
                        r: options.barBorderRadius || 0
                    }).add(scroller.scrollbarGroup);

                scroller.scrollbarRifles = renderer.path(
                        swapXY([
                            'M', -3, size / 4,
                            'L', -3, 2 * size / 3,
                            'M',
                            0, size / 4,
                            'L',
                            0, 2 * size / 3,
                            'M',
                            3, size / 4,
                            'L',
                            3, 2 * size / 3
                        ], options.vertical))
                    .addClass('highcharts-scrollbar-rifles')
                    .add(scroller.scrollbarGroup);


                scroller.scrollbar.attr({
                    fill: options.barBackgroundColor,
                    stroke: options.barBorderColor,
                    'stroke-width': options.barBorderWidth
                });
                scroller.scrollbarRifles.attr({
                    stroke: options.rifleColor,
                    'stroke-width': 1
                });

                scroller.scrollbarStrokeWidth = scroller.scrollbar.strokeWidth();
                scroller.scrollbarGroup.translate(-scroller.scrollbarStrokeWidth % 2 / 2, -scroller.scrollbarStrokeWidth % 2 / 2);

                // Draw the buttons:
                scroller.drawScrollbarButton(0);
                scroller.drawScrollbarButton(1);
            },

            /**
             * Position the scrollbar, method called from a parent with defined dimensions
             * @param {Number} x - x-position on the chart
             * @param {Number} y - y-position on the chart
             * @param {Number} width - width of the scrollbar
             * @param {Number} height - height of the scorllbar
             */
            position: function(x, y, width, height) {
                var scroller = this,
                    options = scroller.options,
                    vertical = options.vertical,
                    xOffset = height,
                    yOffset = 0,
                    method = scroller.rendered ? 'animate' : 'attr';

                scroller.x = x;
                scroller.y = y + this.trackBorderWidth;
                scroller.width = width; // width with buttons
                scroller.height = height;
                scroller.xOffset = xOffset;
                scroller.yOffset = yOffset;

                // If Scrollbar is a vertical type, swap options:
                if (vertical) {
                    scroller.width = scroller.yOffset = width = yOffset = scroller.size;
                    scroller.xOffset = xOffset = 0;
                    scroller.barWidth = height - width * 2; // width without buttons
                    scroller.x = x = x + scroller.options.margin;
                } else {
                    scroller.height = scroller.xOffset = height = xOffset = scroller.size;
                    scroller.barWidth = width - height * 2; // width without buttons
                    scroller.y = scroller.y + scroller.options.margin;
                }

                // Set general position for a group:
                scroller.group[method]({
                    translateX: x,
                    translateY: scroller.y
                });

                // Resize background/track:
                scroller.track[method]({
                    width: width,
                    height: height
                });

                // Move right/bottom button ot it's place:
                scroller.scrollbarButtons[1][method]({
                    translateX: vertical ? 0 : width - xOffset,
                    translateY: vertical ? height - yOffset : 0
                });
            },

            /**
             * Draw the scrollbar buttons with arrows
             * @param {Number} index 0 is left, 1 is right
             */
            drawScrollbarButton: function(index) {
                var scroller = this,
                    renderer = scroller.renderer,
                    scrollbarButtons = scroller.scrollbarButtons,
                    options = scroller.options,
                    size = scroller.size,
                    group,
                    tempElem;

                group = renderer.g().add(scroller.group);
                scrollbarButtons.push(group);

                // Create a rectangle for the scrollbar button
                tempElem = renderer.rect()
                    .addClass('highcharts-scrollbar-button')
                    .add(group);


                // Presentational attributes
                tempElem.attr({
                    stroke: options.buttonBorderColor,
                    'stroke-width': options.buttonBorderWidth,
                    fill: options.buttonBackgroundColor
                });


                // Place the rectangle based on the rendered stroke width
                tempElem.attr(tempElem.crisp({
                    x: -0.5,
                    y: -0.5,
                    width: size + 1, // +1 to compensate for crispifying in rect method
                    height: size + 1,
                    r: options.buttonBorderRadius
                }, tempElem.strokeWidth()));

                // Button arrow
                tempElem = renderer
                    .path(swapXY([
                        'M',
                        size / 2 + (index ? -1 : 1),
                        size / 2 - 3,
                        'L',
                        size / 2 + (index ? -1 : 1),
                        size / 2 + 3,
                        'L',
                        size / 2 + (index ? 2 : -2),
                        size / 2
                    ], options.vertical))
                    .addClass('highcharts-scrollbar-arrow')
                    .add(scrollbarButtons[index]);


                tempElem.attr({
                    fill: options.buttonArrowColor
                });

            },

            /**
             * Set scrollbar size, with a given scale.
             * @param {Number} from - scale (0-1) where bar should start
             * @param {Number} to - scale (0-1) where bar should end
             */
            setRange: function(from, to) {
                var scroller = this,
                    options = scroller.options,
                    vertical = options.vertical,
                    minWidth = options.minWidth,
                    fullWidth = scroller.barWidth,
                    fromPX,
                    toPX,
                    newPos,
                    newSize,
                    newRiflesPos,
                    method = this.rendered && !this.hasDragged ? 'animate' : 'attr';

                if (!defined(fullWidth)) {
                    return;
                }

                from = Math.max(from, 0);
                fromPX = Math.ceil(fullWidth * from);
                toPX = fullWidth * Math.min(to, 1);
                scroller.calculatedWidth = newSize = correctFloat(toPX - fromPX);

                // We need to recalculate position, if minWidth is used
                if (newSize < minWidth) {
                    fromPX = (fullWidth - minWidth + newSize) * from;
                    newSize = minWidth;
                }
                newPos = Math.floor(fromPX + scroller.xOffset + scroller.yOffset);
                newRiflesPos = newSize / 2 - 0.5; // -0.5 -> rifle line width / 2

                // Store current position:
                scroller.from = from;
                scroller.to = to;

                if (!vertical) {
                    scroller.scrollbarGroup[method]({
                        translateX: newPos
                    });
                    scroller.scrollbar[method]({
                        width: newSize
                    });
                    scroller.scrollbarRifles[method]({
                        translateX: newRiflesPos
                    });
                    scroller.scrollbarLeft = newPos;
                    scroller.scrollbarTop = 0;
                } else {
                    scroller.scrollbarGroup[method]({
                        translateY: newPos
                    });
                    scroller.scrollbar[method]({
                        height: newSize
                    });
                    scroller.scrollbarRifles[method]({
                        translateY: newRiflesPos
                    });
                    scroller.scrollbarTop = newPos;
                    scroller.scrollbarLeft = 0;
                }

                if (newSize <= 12) {
                    scroller.scrollbarRifles.hide();
                } else {
                    scroller.scrollbarRifles.show(true);
                }

                // Show or hide the scrollbar based on the showFull setting
                if (options.showFull === false) {
                    if (from <= 0 && to >= 1) {
                        scroller.group.hide();
                    } else {
                        scroller.group.show();
                    }
                }

                scroller.rendered = true;
            },

            /**
             * Init events methods, so we have an access to the Scrollbar itself
             */
            initEvents: function() {
                var scroller = this;
                /**
                 * Event handler for the mouse move event.
                 */
                scroller.mouseMoveHandler = function(e) {
                    var normalizedEvent = scroller.chart.pointer.normalize(e),
                        options = scroller.options,
                        direction = options.vertical ? 'chartY' : 'chartX',
                        initPositions = scroller.initPositions,
                        scrollPosition,
                        chartPosition,
                        change;

                    // In iOS, a mousemove event with e.pageX === 0 is fired when holding the finger
                    // down in the center of the scrollbar. This should be ignored.
                    if (scroller.grabbedCenter && (!e.touches || e.touches[0][direction] !== 0)) { // #4696, scrollbar failed on Android
                        chartPosition = scroller.cursorToScrollbarPosition(normalizedEvent)[direction];
                        scrollPosition = scroller[direction];

                        change = chartPosition - scrollPosition;

                        scroller.hasDragged = true;
                        scroller.updatePosition(initPositions[0] + change, initPositions[1] + change);

                        if (scroller.hasDragged) {
                            fireEvent(scroller, 'changed', {
                                from: scroller.from,
                                to: scroller.to,
                                trigger: 'scrollbar',
                                DOMType: e.type,
                                DOMEvent: e
                            });
                        }
                    }
                };

                /**
                 * Event handler for the mouse up event.
                 */
                scroller.mouseUpHandler = function(e) {
                    if (scroller.hasDragged) {
                        fireEvent(scroller, 'changed', {
                            from: scroller.from,
                            to: scroller.to,
                            trigger: 'scrollbar',
                            DOMType: e.type,
                            DOMEvent: e
                        });
                    }
                    scroller.grabbedCenter = scroller.hasDragged = scroller.chartX = scroller.chartY = null;
                };

                scroller.mouseDownHandler = function(e) {
                    var normalizedEvent = scroller.chart.pointer.normalize(e),
                        mousePosition = scroller.cursorToScrollbarPosition(normalizedEvent);

                    scroller.chartX = mousePosition.chartX;
                    scroller.chartY = mousePosition.chartY;
                    scroller.initPositions = [scroller.from, scroller.to];

                    scroller.grabbedCenter = true;
                };

                scroller.buttonToMinClick = function(e) {
                    var range = correctFloat(scroller.to - scroller.from) * scroller.options.step;
                    scroller.updatePosition(correctFloat(scroller.from - range), correctFloat(scroller.to - range));
                    fireEvent(scroller, 'changed', {
                        from: scroller.from,
                        to: scroller.to,
                        trigger: 'scrollbar',
                        DOMEvent: e
                    });
                };

                scroller.buttonToMaxClick = function(e) {
                    var range = (scroller.to - scroller.from) * scroller.options.step;
                    scroller.updatePosition(scroller.from + range, scroller.to + range);
                    fireEvent(scroller, 'changed', {
                        from: scroller.from,
                        to: scroller.to,
                        trigger: 'scrollbar',
                        DOMEvent: e
                    });
                };

                scroller.trackClick = function(e) {
                    var normalizedEvent = scroller.chart.pointer.normalize(e),
                        range = scroller.to - scroller.from,
                        top = scroller.y + scroller.scrollbarTop,
                        left = scroller.x + scroller.scrollbarLeft;

                    if ((scroller.options.vertical && normalizedEvent.chartY > top) ||
                        (!scroller.options.vertical && normalizedEvent.chartX > left)) {
                        // On the top or on the left side of the track:
                        scroller.updatePosition(scroller.from + range, scroller.to + range);
                    } else {
                        // On the bottom or the right side of the track:
                        scroller.updatePosition(scroller.from - range, scroller.to - range);
                    }

                    fireEvent(scroller, 'changed', {
                        from: scroller.from,
                        to: scroller.to,
                        trigger: 'scrollbar',
                        DOMEvent: e
                    });
                };
            },

            /**
             * Get normalized (0-1) cursor position over the scrollbar
             * @param {Event} normalizedEvent - normalized event, with chartX and chartY values
             * @return {Object} Local position {chartX, chartY}
             */
            cursorToScrollbarPosition: function(normalizedEvent) {
                var scroller = this,
                    options = scroller.options,
                    minWidthDifference = options.minWidth > scroller.calculatedWidth ? options.minWidth : 0; // minWidth distorts translation

                return {
                    chartX: (normalizedEvent.chartX - scroller.x - scroller.xOffset) / (scroller.barWidth - minWidthDifference),
                    chartY: (normalizedEvent.chartY - scroller.y - scroller.yOffset) / (scroller.barWidth - minWidthDifference)
                };
            },

            /**
             * Update position option in the Scrollbar, with normalized 0-1 scale
             */
            updatePosition: function(from, to) {
                if (to > 1) {
                    from = correctFloat(1 - correctFloat(to - from));
                    to = 1;
                }

                if (from < 0) {
                    to = correctFloat(to - from);
                    from = 0;
                }

                this.from = from;
                this.to = to;
            },

            /**
             * Update the scrollbar with new options
             */
            update: function(options) {
                this.destroy();
                this.init(this.chart.renderer, merge(true, this.options, options), this.chart);
            },

            /**
             * Set up the mouse and touch events for the Scrollbar
             */
            addEvents: function() {
                var buttonsOrder = this.options.inverted ? [1, 0] : [0, 1],
                    buttons = this.scrollbarButtons,
                    bar = this.scrollbarGroup.element,
                    track = this.track.element,
                    mouseDownHandler = this.mouseDownHandler,
                    mouseMoveHandler = this.mouseMoveHandler,
                    mouseUpHandler = this.mouseUpHandler,
                    _events;

                // Mouse events
                _events = [
                    [buttons[buttonsOrder[0]].element, 'click', this.buttonToMinClick],
                    [buttons[buttonsOrder[1]].element, 'click', this.buttonToMaxClick],
                    [track, 'click', this.trackClick],
                    [bar, 'mousedown', mouseDownHandler],
                    [bar.ownerDocument, 'mousemove', mouseMoveHandler],
                    [bar.ownerDocument, 'mouseup', mouseUpHandler]
                ];

                // Touch events
                if (hasTouch) {
                    _events.push(
                        [bar, 'touchstart', mouseDownHandler], [bar.ownerDocument, 'touchmove', mouseMoveHandler], [bar.ownerDocument, 'touchend', mouseUpHandler]
                    );
                }

                // Add them all
                each(_events, function(args) {
                    addEvent.apply(null, args);
                });
                this._events = _events;
            },

            /**
             * Removes the event handlers attached previously with addEvents.
             */
            removeEvents: function() {
                each(this._events, function(args) {
                    removeEvent.apply(null, args);
                });
                this._events.length = 0;
            },

            /**
             * Destroys allocated elements.
             */
            destroy: function() {

                var scroller = this.chart.scroller;

                // Disconnect events added in addEvents
                this.removeEvents();

                // Destroy properties
                each(['track', 'scrollbarRifles', 'scrollbar', 'scrollbarGroup', 'group'], function(prop) {
                    if (this[prop] && this[prop].destroy) {
                        this[prop] = this[prop].destroy();
                    }
                }, this);

                if (scroller && this === scroller.scrollbar) { // #6421, chart may have more scrollbars
                    scroller.scrollbar = null;

                    // Destroy elements in collection
                    destroyObjectProperties(scroller.scrollbarButtons);
                }
            }
        };

        /**
         * Wrap axis initialization and create scrollbar if enabled:
         */
        wrap(Axis.prototype, 'init', function(proceed) {
            var axis = this;
            proceed.apply(axis, Array.prototype.slice.call(arguments, 1));

            if (axis.options.scrollbar && axis.options.scrollbar.enabled) {
                // Predefined options:
                axis.options.scrollbar.vertical = !axis.horiz;
                axis.options.startOnTick = axis.options.endOnTick = false;

                axis.scrollbar = new Scrollbar(axis.chart.renderer, axis.options.scrollbar, axis.chart);

                addEvent(axis.scrollbar, 'changed', function(e) {
                    var unitedMin = Math.min(pick(axis.options.min, axis.min), axis.min, axis.dataMin),
                        unitedMax = Math.max(pick(axis.options.max, axis.max), axis.max, axis.dataMax),
                        range = unitedMax - unitedMin,
                        to,
                        from;

                    if ((axis.horiz && !axis.reversed) || (!axis.horiz && axis.reversed)) {
                        to = unitedMin + range * this.to;
                        from = unitedMin + range * this.from;
                    } else {
                        // y-values in browser are reversed, but this also applies for reversed horizontal axis:
                        to = unitedMin + range * (1 - this.from);
                        from = unitedMin + range * (1 - this.to);
                    }

                    axis.setExtremes(from, to, true, false, e);
                });
            }
        });

        /**
         * Wrap rendering axis, and update scrollbar if one is created:
         */
        wrap(Axis.prototype, 'render', function(proceed) {
            var axis = this,
                scrollMin = Math.min(
                    pick(axis.options.min, axis.min),
                    axis.min,
                    pick(axis.dataMin, axis.min) // #6930
                ),
                scrollMax = Math.max(
                    pick(axis.options.max, axis.max),
                    axis.max,
                    pick(axis.dataMax, axis.max) // #6930
                ),
                scrollbar = axis.scrollbar,
                titleOffset = axis.titleOffset || 0,
                offsetsIndex,
                from,
                to;

            proceed.apply(axis, Array.prototype.slice.call(arguments, 1));

            if (scrollbar) {

                if (axis.horiz) {
                    scrollbar.position(
                        axis.left,
                        axis.top + axis.height + 2 + axis.chart.scrollbarsOffsets[1] +
                        (axis.opposite ?
                            0 :
                            titleOffset + axis.axisTitleMargin + axis.offset
                        ),
                        axis.width,
                        axis.height
                    );
                    offsetsIndex = 1;
                } else {
                    scrollbar.position(
                        axis.left + axis.width + 2 + axis.chart.scrollbarsOffsets[0] +
                        (axis.opposite ?
                            titleOffset + axis.axisTitleMargin + axis.offset :
                            0
                        ),
                        axis.top,
                        axis.width,
                        axis.height
                    );
                    offsetsIndex = 0;
                }

                if ((!axis.opposite && !axis.horiz) || (axis.opposite && axis.horiz)) {
                    axis.chart.scrollbarsOffsets[offsetsIndex] +=
                        axis.scrollbar.size + axis.scrollbar.options.margin;
                }

                if (isNaN(scrollMin) || isNaN(scrollMax) || !defined(axis.min) || !defined(axis.max)) {
                    scrollbar.setRange(0, 0); // default action: when there is not extremes on the axis, but scrollbar exists, make it full size
                } else {
                    from = (axis.min - scrollMin) / (scrollMax - scrollMin);
                    to = (axis.max - scrollMin) / (scrollMax - scrollMin);

                    if ((axis.horiz && !axis.reversed) || (!axis.horiz && axis.reversed)) {
                        scrollbar.setRange(from, to);
                    } else {
                        scrollbar.setRange(1 - to, 1 - from); // inverse vertical axis
                    }
                }
            }
        });

        /**
         * Make space for a scrollbar
         */
        wrap(Axis.prototype, 'getOffset', function(proceed) {
            var axis = this,
                index = axis.horiz ? 2 : 1,
                scrollbar = axis.scrollbar;

            proceed.apply(axis, Array.prototype.slice.call(arguments, 1));

            if (scrollbar) {
                axis.chart.scrollbarsOffsets = [0, 0]; // reset scrollbars offsets
                axis.chart.axisOffset[index] += scrollbar.size + scrollbar.options.margin;
            }
        });

        /**
         * Destroy scrollbar when connected to the specific axis
         */
        wrap(Axis.prototype, 'destroy', function(proceed) {
            if (this.scrollbar) {
                this.scrollbar = this.scrollbar.destroy();
            }

            proceed.apply(this, Array.prototype.slice.call(arguments, 1));
        });

        H.Scrollbar = Scrollbar;

    }(Highcharts));
    (function(H) {
        /**
         * (c) 2010-2017 Torstein Honsi
         *
         * License: www.highcharts.com/license
         */
        /* eslint max-len: ["warn", 80, 4] */

        /**
         * Options for the corresponding navigator series if `showInNavigator`
         * is `true` for this series. Available options are the same as any
         * series, documented at [plotOptions](#plotOptions.series) and
         * [series](#series).
         *
         *
         * These options are merged with options in [navigator.series](#navigator.
         * series), and will take precedence if the same option is defined both
         * places.
         *
         * @type {Object}
         * @see [navigator.series](#navigator.series)
         * @default undefined
         * @since 5.0.0
         * @product highstock
         * @apioption plotOptions.series.navigatorOptions
         */

        /**
         * Whether or not to show the series in the navigator. Takes precedence
         * over [navigator.baseSeries](#navigator.baseSeries) if defined.
         *
         * @type {Boolean}
         * @default undefined
         * @since 5.0.0
         * @product highstock
         * @apioption plotOptions.series.showInNavigator
         */

        var addEvent = H.addEvent,
            Axis = H.Axis,
            Chart = H.Chart,
            color = H.color,
            defaultDataGroupingUnits = H.defaultDataGroupingUnits,
            defaultOptions = H.defaultOptions,
            defined = H.defined,
            destroyObjectProperties = H.destroyObjectProperties,
            each = H.each,
            erase = H.erase,
            error = H.error,
            extend = H.extend,
            grep = H.grep,
            hasTouch = H.hasTouch,
            isArray = H.isArray,
            isNumber = H.isNumber,
            isObject = H.isObject,
            merge = H.merge,
            pick = H.pick,
            removeEvent = H.removeEvent,
            Scrollbar = H.Scrollbar,
            Series = H.Series,
            seriesTypes = H.seriesTypes,
            wrap = H.wrap,

            units = [].concat(defaultDataGroupingUnits), // copy
            defaultSeriesType,

            // Finding the min or max of a set of variables where we don't know if they
            // are defined, is a pattern that is repeated several places in Highcharts.
            // Consider making this a global utility method.
            numExt = function(extreme) {
                var numbers = grep(arguments, isNumber);
                if (numbers.length) {
                    return Math[extreme].apply(0, numbers);
                }
            };

        // add more resolution to units
        units[4] = ['day', [1, 2, 3, 4]]; // allow more days
        units[5] = ['week', [1, 2, 3]]; // allow more weeks

        defaultSeriesType = seriesTypes.areaspline === undefined ?
            'line' :
            'areaspline';

        extend(defaultOptions, {

            /**
             * The navigator is a small series below the main series, displaying
             * a view of the entire data set. It provides tools to zoom in and
             * out on parts of the data as well as panning across the dataset.
             *
             * @product highstock
             * @optionparent navigator
             */
            navigator: {
                /**
                 * The height of the navigator.
                 *
                 * @type {Number}
                 * @sample {highstock} stock/navigator/height/ A higher navigator
                 * @default 40
                 * @product highstock
                 */
                height: 40,

                /**
                 * The distance from the nearest element, the X axis or X axis labels.
                 *
                 * @type {Number}
                 * @sample {highstock} stock/navigator/margin/
                 *         A margin of 2 draws the navigator closer to the X axis labels
                 * @default 25
                 * @product highstock
                 */
                margin: 25,

                /**
                 * Whether the mask should be inside the range marking the zoomed
                 * range, or outside. In Highstock 1.x it was always `false`.
                 *
                 * @type {Boolean}
                 * @sample {highstock} stock/navigator/maskinside-false/
                 *         False, mask outside
                 * @default true
                 * @since 2.0
                 * @product highstock
                 */
                maskInside: true,

                /**
                 * Options for the handles for dragging the zoomed area.
                 *
                 * @type {Object}
                 * @sample {highstock} stock/navigator/handles/ Colored handles
                 * @product highstock
                 */
                handles: {
                    /**
                     * Width for handles.
                     *
                     * @type {Number}
                     * @default 7
                     * @product highstock
                     * @sample {highstock} stock/navigator/styled-handles/
                     *         Styled handles
                     * @since 6.0.0
                     */
                    width: 7,

                    /**
                     * Height for handles.
                     *
                     * @type {Number}
                     * @default 15
                     * @product highstock
                     * @sample {highstock} stock/navigator/styled-handles/
                     *         Styled handles
                     * @since 6.0.0
                     */
                    height: 15,

                    /**
                     * Array to define shapes of handles. 0-index for left, 1-index for
                     * right.
                     *
                     * Additionally, the URL to a graphic can be given on this form:
                     * `url(graphic.png)`. Note that for the image to be applied to
                     * exported charts, its URL needs to be accessible by the export
                     * server.
                     *
                     * Custom callbacks for symbol path generation can also be added to
                     * `Highcharts.SVGRenderer.prototype.symbols`. The callback is then
                     * used by its method name, as shown in the demo.
                     *
                     * @type {Array}
                     * @default ['navigator-handle', 'navigator-handle']
                     * @product highstock
                     * @sample {highstock} stock/navigator/styled-handles/
                     *         Styled handles
                     * @since 6.0.0
                     */
                    symbols: ['navigator-handle', 'navigator-handle'],

                    /**
                     * Allows to enable/disable handles.
                     *
                     * @type {Boolean}
                     * @default true
                     * @product highstock
                     * @since 6.0.0
                     */
                    enabled: true,


                    /**
                     * The width for the handle border and the stripes inside.
                     *
                     * @type {Number}
                     * @default 7
                     * @product highstock
                     * @sample {highstock} stock/navigator/styled-handles/
                     *         Styled handles
                     * @since 6.0.0
                     */
                    lineWidth: 1,

                    /**
                     * The fill for the handle.
                     *
                     * @type {Color}
                     * @product highstock
                     */
                    backgroundColor: '#f2f2f2',

                    /**
                     * The stroke for the handle border and the stripes inside.
                     *
                     * @type {Color}
                     * @product highstock
                     */
                    borderColor: '#999999'


                },



                /**
                 * The color of the mask covering the areas of the navigator series
                 * that are currently not visible in the main series. The default
                 * color is bluish with an opacity of 0.3 to see the series below.
                 *
                 * @type {Color}
                 * @see     In styled mode, the mask is styled with the
                 *          `.highcharts-navigator-mask` and
                 *          `.highcharts-navigator-mask-inside` classes.
                 * @sample  {highstock} stock/navigator/maskfill/
                 *          Blue, semi transparent mask
                 * @default rgba(102,133,194,0.3)
                 * @product highstock
                 */
                maskFill: color('#6685c2').setOpacity(0.3).get(),

                /**
                 * The color of the line marking the currently zoomed area in the
                 * navigator.
                 *
                 * @type {Color}
                 * @sample {highstock} stock/navigator/outline/ 2px blue outline
                 * @default #cccccc
                 * @product highstock
                 */
                outlineColor: '#cccccc',

                /**
                 * The width of the line marking the currently zoomed area in the
                 * navigator.
                 *
                 * @type {Number}
                 * @see In styled mode, the outline stroke width is set with the `.
                 * highcharts-navigator-outline` class.
                 * @sample {highstock} stock/navigator/outline/ 2px blue outline
                 * @default 2
                 * @product highstock
                 */
                outlineWidth: 1,


                /**
                 * Options for the navigator series. Available options are the same
                 * as any series, documented at [plotOptions](#plotOptions.series)
                 * and [series](#series).
                 *
                 * Unless data is explicitly defined on navigator.series, the data
                 * is borrowed from the first series in the chart.
                 *
                 * Default series options for the navigator series are:
                 *
                 * <pre>series: {
                 *     type: 'areaspline',
                 *     fillOpacity: 0.05,
                 *     dataGrouping: {
                 *         smoothed: true
                 *     },
                 *     lineWidth: 1,
                 *     marker: {
                 *         enabled: false
                 *     }
                 * }</pre>
                 *
                 * @type {Object}
                 * @see In styled mode, the navigator series is styled with the `.
                 * highcharts-navigator-series` class.
                 * @sample {highstock} stock/navigator/series-data/
                 *         Using a separate data set for the navigator
                 * @sample {highstock} stock/navigator/series/
                 *         A green navigator series
                 * @product highstock
                 */
                series: {

                    /**
                     * The type of the navigator series. Defaults to `areaspline` if
                     * defined, otherwise `line`.
                     *
                     * @type {String}
                     */
                    type: defaultSeriesType,



                    /**
                     * The fill opacity of the navigator series.
                     */
                    fillOpacity: 0.05,

                    /**
                     * The pixel line width of the navigator series.
                     */
                    lineWidth: 1,


                    /**
                     * @ignore
                     */
                    compare: null,

                    /**
                     * Data grouping options for the navigator series.
                     *
                     * @extends {plotOptions.series.dataGrouping}
                     */
                    dataGrouping: {
                        approximation: 'average',
                        enabled: true,
                        groupPixelWidth: 2,
                        smoothed: true,
                        units: units
                    },

                    /**
                     * Data label options for the navigator series. Data labels are
                     * disabled by default on the navigator series.
                     *
                     * @extends {plotOptions.series.dataLabels}
                     */
                    dataLabels: {
                        enabled: false,
                        zIndex: 2 // #1839
                    },

                    id: 'highcharts-navigator-series',
                    className: 'highcharts-navigator-series',

                    /**
                     * Line color for the navigator series. Allows setting the color
                     * while disallowing the default candlestick setting.
                     *
                     * @type {Color}
                     */
                    lineColor: null, // #4602

                    marker: {
                        enabled: false
                    },

                    pointRange: 0,
                    /**
                     * The threshold option. Setting it to 0 will make the default
                     * navigator area series draw its area from the 0 value and up.
                     * @type {Number}
                     */
                    threshold: null
                },

                /**
                 * Options for the navigator X axis. Default series options
                 * for the navigator xAxis are:
                 *
                 * <pre>xAxis: {
                 *     tickWidth: 0,
                 *     lineWidth: 0,
                 *     gridLineWidth: 1,
                 *     tickPixelInterval: 200,
                 *     labels: {
                 *     	   align: 'left',
                 *         style: {
                 *             color: '#888'
                 *         },
                 *         x: 3,
                 *         y: -4
                 *     }
                 * }</pre>
                 *
                 * @type {Object}
                 * @extends {xAxis}
                 * @excluding linkedTo,maxZoom,minRange,opposite,range,scrollbar,
                 *          showEmpty,maxRange
                 * @product highstock
                 */
                xAxis: {
                    /**
                     * Additional range on the right side of the xAxis. Works similar to
                     * xAxis.maxPadding, but value is set in milliseconds.
                     * Can be set for both, main xAxis and navigator's xAxis.
                     *
                     * @type {Number}
                     * @default 0
                     * @since 6.0.0
                     * @product highstock
                     * @apioption xAxis.overscroll
                     */
                    overscroll: 0,

                    className: 'highcharts-navigator-xaxis',
                    tickLength: 0,


                    lineWidth: 0,
                    gridLineColor: '#e6e6e6',
                    gridLineWidth: 1,


                    tickPixelInterval: 200,

                    labels: {
                        align: 'left',


                        style: {
                            color: '#999999'
                        },


                        x: 3,
                        y: -4
                    },

                    crosshair: false
                },

                /**
                 * Options for the navigator Y axis. Default series options
                 * for the navigator yAxis are:
                 *
                 * <pre>yAxis: {
                 *     gridLineWidth: 0,
                 *     startOnTick: false,
                 *     endOnTick: false,
                 *     minPadding: 0.1,
                 *     maxPadding: 0.1,
                 *     labels: {
                 *         enabled: false
                 *     },
                 *     title: {
                 *         text: null
                 *     },
                 *     tickWidth: 0
                 * }</pre>
                 *
                 * @type {Object}
                 * @extends {yAxis}
                 * @excluding height,linkedTo,maxZoom,minRange,ordinal,range,showEmpty,
                 *          scrollbar,top,units,maxRange
                 * @product highstock
                 */
                yAxis: {

                    className: 'highcharts-navigator-yaxis',


                    gridLineWidth: 0,


                    startOnTick: false,
                    endOnTick: false,
                    minPadding: 0.1,
                    maxPadding: 0.1,
                    labels: {
                        enabled: false
                    },
                    crosshair: false,
                    title: {
                        text: null
                    },
                    tickLength: 0,
                    tickWidth: 0
                }
            }
        });

        /**
         * Draw one of the handles on the side of the zoomed range in the navigator
         * @param {Boolean} inverted flag for chart.inverted
         * @returns {Array} Path to be used in a handle
         */
        H.Renderer.prototype.symbols['navigator-handle'] = function(
            x,
            y,
            w,
            h,
            options
        ) {
            var halfWidth = options.width / 2,
                markerPosition = Math.round(halfWidth / 3) + 0.5,
                height = options.height;

            return [
                'M', -halfWidth - 1, 0.5,
                'L',
                halfWidth, 0.5,
                'L',
                halfWidth, height + 0.5,
                'L', -halfWidth - 1, height + 0.5,
                'L', -halfWidth - 1, 0.5,
                'M', -markerPosition, 4,
                'L', -markerPosition, height - 3,
                'M',
                markerPosition - 1, 4,
                'L',
                markerPosition - 1, height - 3
            ];
        };

        /**
         * The Navigator class
         * @param {Object} chart - Chart object
         * @class
         */
        function Navigator(chart) {
            this.init(chart);
        }

        Navigator.prototype = {
            /**
             * Draw one of the handles on the side of the zoomed range in the navigator
             * @param {Number} x The x center for the handle
             * @param {Number} index 0 for left and 1 for right
             * @param {Boolean} inverted flag for chart.inverted
             * @param {String} verb use 'animate' or 'attr'
             */
            drawHandle: function(x, index, inverted, verb) {
                var navigator = this,
                    height = navigator.navigatorOptions.handles.height;

                // Place it
                navigator.handles[index][verb](inverted ? {
                    translateX: Math.round(navigator.left + navigator.height / 2),
                    translateY: Math.round(
                        navigator.top + parseInt(x, 10) + 0.5 - height
                    )
                } : {
                    translateX: Math.round(navigator.left + parseInt(x, 10)),
                    translateY: Math.round(
                        navigator.top + navigator.height / 2 - height / 2 - 1
                    )
                });
            },

            /**
             * Render outline around the zoomed range
             * @param {Number} zoomedMin in pixels position where zoomed range starts
             * @param {Number} zoomedMax in pixels position where zoomed range ends
             * @param {Boolean} inverted flag if chart is inverted
             * @param {String} verb use 'animate' or 'attr'
             */
            drawOutline: function(zoomedMin, zoomedMax, inverted, verb) {
                var navigator = this,
                    maskInside = navigator.navigatorOptions.maskInside,
                    outlineWidth = navigator.outline.strokeWidth(),
                    halfOutline = outlineWidth / 2,
                    outlineCorrection = (outlineWidth % 2) / 2, // #5800
                    outlineHeight = navigator.outlineHeight,
                    scrollbarHeight = navigator.scrollbarHeight,
                    navigatorSize = navigator.size,
                    left = navigator.left - scrollbarHeight,
                    navigatorTop = navigator.top,
                    verticalMin,
                    path;

                if (inverted) {
                    left -= halfOutline;
                    verticalMin = navigatorTop + zoomedMax + outlineCorrection;
                    zoomedMax = navigatorTop + zoomedMin + outlineCorrection;

                    path = [
                        'M',
                        left + outlineHeight,
                        navigatorTop - scrollbarHeight - outlineCorrection, // top edge
                        'L',
                        left + outlineHeight,
                        verticalMin, // top right of zoomed range
                        'L',
                        left,
                        verticalMin, // top left of z.r.
                        'L',
                        left,
                        zoomedMax, // bottom left of z.r.
                        'L',
                        left + outlineHeight,
                        zoomedMax, // bottom right of z.r.
                        'L',
                        left + outlineHeight,
                        navigatorTop + navigatorSize + scrollbarHeight // bottom edge
                    ].concat(maskInside ? [
                        'M',
                        left + outlineHeight,
                        verticalMin - halfOutline, // upper left of zoomed range
                        'L',
                        left + outlineHeight,
                        zoomedMax + halfOutline // upper right of z.r.
                    ] : []);
                } else {
                    zoomedMin += left + scrollbarHeight - outlineCorrection;
                    zoomedMax += left + scrollbarHeight - outlineCorrection;
                    navigatorTop += halfOutline;

                    path = [
                        'M',
                        left,
                        navigatorTop, // left
                        'L',
                        zoomedMin,
                        navigatorTop, // upper left of zoomed range
                        'L',
                        zoomedMin,
                        navigatorTop + outlineHeight, // lower left of z.r.
                        'L',
                        zoomedMax,
                        navigatorTop + outlineHeight, // lower right of z.r.
                        'L',
                        zoomedMax,
                        navigatorTop, // upper right of z.r.
                        'L',
                        left + navigatorSize + scrollbarHeight * 2,
                        navigatorTop // right
                    ].concat(maskInside ? [
                        'M',
                        zoomedMin - halfOutline,
                        navigatorTop, // upper left of zoomed range
                        'L',
                        zoomedMax + halfOutline,
                        navigatorTop // upper right of z.r.
                    ] : []);
                }
                navigator.outline[verb]({
                    d: path
                });
            },

            /**
             * Render outline around the zoomed range
             * @param {Number} zoomedMin in pixels position where zoomed range starts
             * @param {Number} zoomedMax in pixels position where zoomed range ends
             * @param {Boolean} inverted flag if chart is inverted
             * @param {String} verb use 'animate' or 'attr'
             */
            drawMasks: function(zoomedMin, zoomedMax, inverted, verb) {
                var navigator = this,
                    left = navigator.left,
                    top = navigator.top,
                    navigatorHeight = navigator.height,
                    height,
                    width,
                    x,
                    y;

                // Determine rectangle position & size
                // According to (non)inverted position:
                if (inverted) {
                    x = [left, left, left];
                    y = [top, top + zoomedMin, top + zoomedMax];
                    width = [navigatorHeight, navigatorHeight, navigatorHeight];
                    height = [
                        zoomedMin,
                        zoomedMax - zoomedMin,
                        navigator.size - zoomedMax
                    ];
                } else {
                    x = [left, left + zoomedMin, left + zoomedMax];
                    y = [top, top, top];
                    width = [
                        zoomedMin,
                        zoomedMax - zoomedMin,
                        navigator.size - zoomedMax
                    ];
                    height = [navigatorHeight, navigatorHeight, navigatorHeight];
                }
                each(navigator.shades, function(shade, i) {
                    shade[verb]({
                        x: x[i],
                        y: y[i],
                        width: width[i],
                        height: height[i]
                    });
                });
            },

            /**
             * Generate DOM elements for a navigator:
             * - main navigator group
             * - all shades
             * - outline
             * - handles
             */
            renderElements: function() {
                var navigator = this,
                    navigatorOptions = navigator.navigatorOptions,
                    maskInside = navigatorOptions.maskInside,
                    chart = navigator.chart,
                    inverted = chart.inverted,
                    renderer = chart.renderer,
                    navigatorGroup;

                // Create the main navigator group
                navigator.navigatorGroup = navigatorGroup = renderer.g('navigator')
                    .attr({
                        zIndex: 8,
                        visibility: 'hidden'
                    })
                    .add();



                var mouseCursor = {
                    cursor: inverted ? 'ns-resize' : 'ew-resize'
                };


                // Create masks, each mask will get events and fill:
                each([!maskInside, maskInside, !maskInside], function(hasMask, index) {
                    navigator.shades[index] = renderer.rect()
                        .addClass('highcharts-navigator-mask' +
                            (index === 1 ? '-inside' : '-outside'))

                        .attr({
                            fill: hasMask ? navigatorOptions.maskFill : 'rgba(0,0,0,0)'
                        })
                        .css(index === 1 && mouseCursor)

                        .add(navigatorGroup);
                });

                // Create the outline:
                navigator.outline = renderer.path()
                    .addClass('highcharts-navigator-outline')

                    .attr({
                        'stroke-width': navigatorOptions.outlineWidth,
                        stroke: navigatorOptions.outlineColor
                    })

                    .add(navigatorGroup);

                // Create the handlers:
                if (navigatorOptions.handles.enabled) {
                    each([0, 1], function(index) {
                        navigatorOptions.handles.inverted = chart.inverted;
                        navigator.handles[index] = renderer.symbol(
                            navigatorOptions.handles.symbols[index], -navigatorOptions.handles.width / 2 - 1,
                            0,
                            navigatorOptions.handles.width,
                            navigatorOptions.handles.height,
                            navigatorOptions.handles
                        );
                        // zIndex = 6 for right handle, 7 for left.
                        // Can't be 10, because of the tooltip in inverted chart #2908
                        navigator.handles[index].attr({
                                zIndex: 7 - index
                            })
                            .addClass(
                                'highcharts-navigator-handle ' +
                                'highcharts-navigator-handle-' + ['left', 'right'][index]
                            ).add(navigatorGroup);


                        var handlesOptions = navigatorOptions.handles;
                        navigator.handles[index]
                            .attr({
                                fill: handlesOptions.backgroundColor,
                                stroke: handlesOptions.borderColor,
                                'stroke-width': handlesOptions.lineWidth
                            })
                            .css(mouseCursor);

                    });
                }
            },

            /**
             * Update navigator
             * @param {Object} options Options to merge in when updating navigator
             */
            update: function(options) {
                // Remove references to old navigator series in base series
                each(this.series || [], function(series) {
                    if (series.baseSeries) {
                        delete series.baseSeries.navigatorSeries;
                    }
                });
                // Destroy and rebuild navigator
                this.destroy();
                var chartOptions = this.chart.options;
                merge(true, chartOptions.navigator, this.options, options);
                this.init(this.chart);
            },

            /**
             * Render the navigator
             * @param {Number} min X axis value minimum
             * @param {Number} max X axis value maximum
             * @param {Number} pxMin Pixel value minimum
             * @param {Number} pxMax Pixel value maximum
             */
            render: function(min, max, pxMin, pxMax) {

                var navigator = this,
                    chart = navigator.chart,
                    navigatorWidth,
                    scrollbarLeft,
                    scrollbarTop,
                    scrollbarHeight = navigator.scrollbarHeight,
                    navigatorSize,
                    xAxis = navigator.xAxis,
                    scrollbarXAxis = xAxis.fake ? chart.xAxis[0] : xAxis,
                    navigatorEnabled = navigator.navigatorEnabled,
                    zoomedMin,
                    zoomedMax,
                    rendered = navigator.rendered,
                    inverted = chart.inverted,
                    verb,
                    newMin,
                    newMax,
                    currentRange,
                    minRange = chart.xAxis[0].minRange,
                    maxRange = chart.xAxis[0].options.maxRange;

                // Don't redraw while moving the handles (#4703).
                if (this.hasDragged && !defined(pxMin)) {
                    return;
                }

                // Don't render the navigator until we have data (#486, #4202, #5172).
                if (!isNumber(min) || !isNumber(max)) {
                    // However, if navigator was already rendered, we may need to resize
                    // it. For example hidden series, but visible navigator (#6022).
                    if (rendered) {
                        pxMin = 0;
                        pxMax = pick(xAxis.width, scrollbarXAxis.width);
                    } else {
                        return;
                    }
                }

                navigator.left = pick(
                    xAxis.left,
                    // in case of scrollbar only, without navigator
                    chart.plotLeft + scrollbarHeight + (inverted ? chart.plotWidth : 0)
                );

                navigator.size = zoomedMax = navigatorSize = pick(
                    xAxis.len,
                    (inverted ? chart.plotHeight : chart.plotWidth) -
                    2 * scrollbarHeight
                );

                if (inverted) {
                    navigatorWidth = scrollbarHeight;
                } else {
                    navigatorWidth = navigatorSize + 2 * scrollbarHeight;
                }

                // Get the pixel position of the handles
                pxMin = pick(pxMin, xAxis.toPixels(min, true));
                pxMax = pick(pxMax, xAxis.toPixels(max, true));

                // Verify (#1851, #2238)
                if (!isNumber(pxMin) || Math.abs(pxMin) === Infinity) {
                    pxMin = 0;
                    pxMax = navigatorWidth;
                }

                // Are we below the minRange? (#2618, #6191)
                newMin = xAxis.toValue(pxMin, true);
                newMax = xAxis.toValue(pxMax, true);
                currentRange = Math.abs(H.correctFloat(newMax - newMin));
                if (currentRange < minRange) {
                    if (this.grabbedLeft) {
                        pxMin = xAxis.toPixels(newMax - minRange, true);
                    } else if (this.grabbedRight) {
                        pxMax = xAxis.toPixels(newMin + minRange, true);
                    }
                } else if (defined(maxRange) && currentRange > maxRange) {
                    /**
                     * Maximum range which can be set using the navigator's handles.
                     * Opposite of [xAxis.minRange](#xAxis.minRange).
                     *
                     * @type {Number}
                     * @default undefined
                     * @product highstock
                     * @sample {highstock} stock/navigator/maxrange/
                     *         Defined max and min range
                     * @since 6.0.0
                     * @apioption xAxis.maxRange
                     */
                    if (this.grabbedLeft) {
                        pxMin = xAxis.toPixels(newMax - maxRange, true);
                    } else if (this.grabbedRight) {
                        pxMax = xAxis.toPixels(newMin + maxRange, true);
                    }
                }

                // Handles are allowed to cross, but never exceed the plot area
                navigator.zoomedMax = Math.min(Math.max(pxMin, pxMax, 0), zoomedMax);
                navigator.zoomedMin = Math.min(
                    Math.max(
                        navigator.fixedWidth ?
                        navigator.zoomedMax - navigator.fixedWidth :
                        Math.min(pxMin, pxMax),
                        0
                    ),
                    zoomedMax
                );

                navigator.range = navigator.zoomedMax - navigator.zoomedMin;

                zoomedMax = Math.round(navigator.zoomedMax);
                zoomedMin = Math.round(navigator.zoomedMin);

                if (navigatorEnabled) {
                    navigator.navigatorGroup.attr({
                        visibility: 'visible'
                    });
                    // Place elements
                    verb = rendered && !navigator.hasDragged ? 'animate' : 'attr';

                    navigator.drawMasks(zoomedMin, zoomedMax, inverted, verb);
                    navigator.drawOutline(zoomedMin, zoomedMax, inverted, verb);

                    if (navigator.navigatorOptions.handles.enabled) {
                        navigator.drawHandle(zoomedMin, 0, inverted, verb);
                        navigator.drawHandle(zoomedMax, 1, inverted, verb);
                    }
                }

                if (navigator.scrollbar) {
                    if (inverted) {
                        scrollbarTop = navigator.top - scrollbarHeight;
                        scrollbarLeft = navigator.left - scrollbarHeight +
                            (navigatorEnabled || !scrollbarXAxis.opposite ? 0 :
                                // Multiple axes has offsets:
                                (scrollbarXAxis.titleOffset || 0) +
                                // Self margin from the axis.title
                                scrollbarXAxis.axisTitleMargin
                            );
                        scrollbarHeight = navigatorSize + 2 * scrollbarHeight;
                    } else {
                        scrollbarTop = navigator.top +
                            (navigatorEnabled ? navigator.height : -scrollbarHeight);
                        scrollbarLeft = navigator.left - scrollbarHeight;
                    }
                    // Reposition scrollbar
                    navigator.scrollbar.position(
                        scrollbarLeft,
                        scrollbarTop,
                        navigatorWidth,
                        scrollbarHeight
                    );
                    // Keep scale 0-1
                    navigator.scrollbar.setRange(
                        // Use real value, not rounded because range can be very small
                        // (#1716)
                        navigator.zoomedMin / navigatorSize,
                        navigator.zoomedMax / navigatorSize
                    );
                }
                navigator.rendered = true;
            },

            /**
             * Set up the mouse and touch events for the navigator
             */
            addMouseEvents: function() {
                var navigator = this,
                    chart = navigator.chart,
                    container = chart.container,
                    eventsToUnbind = [],
                    mouseMoveHandler,
                    mouseUpHandler;

                /**
                 * Create mouse events' handlers.
                 * Make them as separate functions to enable wrapping them:
                 */
                navigator.mouseMoveHandler = mouseMoveHandler = function(e) {
                    navigator.onMouseMove(e);
                };
                navigator.mouseUpHandler = mouseUpHandler = function(e) {
                    navigator.onMouseUp(e);
                };

                // Add shades and handles mousedown events
                eventsToUnbind = navigator.getPartsEvents('mousedown');
                // Add mouse move and mouseup events. These are bind to doc/container,
                // because Navigator.grabbedSomething flags are stored in mousedown
                // events
                eventsToUnbind.push(
                    addEvent(container, 'mousemove', mouseMoveHandler),
                    addEvent(container.ownerDocument, 'mouseup', mouseUpHandler)
                );

                // Touch events
                if (hasTouch) {
                    eventsToUnbind.push(
                        addEvent(container, 'touchmove', mouseMoveHandler),
                        addEvent(container.ownerDocument, 'touchend', mouseUpHandler)
                    );
                    eventsToUnbind.concat(navigator.getPartsEvents('touchstart'));
                }

                navigator.eventsToUnbind = eventsToUnbind;

                // Data events
                if (navigator.series && navigator.series[0]) {
                    eventsToUnbind.push(
                        addEvent(
                            navigator.series[0].xAxis,
                            'foundExtremes',
                            function() {
                                chart.navigator.modifyNavigatorAxisExtremes();
                            }
                        )
                    );
                }
            },

            /**
             * Generate events for handles and masks
             * @param {String} eventName Event name handler, 'mousedown' or 'touchstart'
             * @returns {Array} An array of arrays: [DOMElement, eventName, callback].
             */
            getPartsEvents: function(eventName) {
                var navigator = this,
                    events = [];
                each(['shades', 'handles'], function(name) {
                    each(navigator[name], function(navigatorItem, index) {
                        events.push(
                            addEvent(
                                navigatorItem.element,
                                eventName,
                                function(e) {
                                    navigator[name + 'Mousedown'](e, index);
                                }
                            )
                        );
                    });
                });
                return events;
            },

            /**
             * Mousedown on a shaded mask, either:
             * - will be stored for future drag&drop
             * - will directly shift to a new range
             *
             * @param {Object} e Mouse event
             * @param {Number} index Index of a mask in Navigator.shades array
             */
            shadesMousedown: function(e, index) {
                e = this.chart.pointer.normalize(e);

                var navigator = this,
                    chart = navigator.chart,
                    xAxis = navigator.xAxis,
                    zoomedMin = navigator.zoomedMin,
                    navigatorPosition = navigator.left,
                    navigatorSize = navigator.size,
                    range = navigator.range,
                    chartX = e.chartX,
                    fixedMax,
                    ext,
                    left;

                // For inverted chart, swap some options:
                if (chart.inverted) {
                    chartX = e.chartY;
                    navigatorPosition = navigator.top;
                }

                if (index === 1) {
                    // Store information for drag&drop
                    navigator.grabbedCenter = chartX;
                    navigator.fixedWidth = range;
                    navigator.dragOffset = chartX - zoomedMin;
                } else {
                    // Shift the range by clicking on shaded areas
                    left = chartX - navigatorPosition - range / 2;
                    if (index === 0) {
                        left = Math.max(0, left);
                    } else if (index === 2 && left + range >= navigatorSize) {
                        left = navigatorSize - range;
                        fixedMax = navigator.getUnionExtremes().dataMax; // #2293, #3543
                    }
                    if (left !== zoomedMin) { // it has actually moved
                        navigator.fixedWidth = range; // #1370

                        ext = xAxis.toFixedRange(left, left + range, null, fixedMax);
                        if (defined(ext.min)) { // #7411
                            chart.xAxis[0].setExtremes(
                                Math.min(ext.min, ext.max),
                                Math.max(ext.min, ext.max),
                                true,
                                null, // auto animation
                                {
                                    trigger: 'navigator'
                                }
                            );
                        }
                    }
                }
            },

            /**
             * Mousedown on a handle mask.
             * Will store necessary information for drag&drop.
             *
             * @param {Object} e Mouse event
             * @param {Number} index Index of a handle in Navigator.handles array
             */
            handlesMousedown: function(e, index) {
                e = this.chart.pointer.normalize(e);

                var navigator = this,
                    chart = navigator.chart,
                    baseXAxis = chart.xAxis[0],
                    // For reversed axes, min and max are chagned,
                    // so the other extreme should be stored
                    reverse = (chart.inverted && !baseXAxis.reversed) ||
                    (!chart.inverted && baseXAxis.reversed);

                if (index === 0) {
                    // Grab the left handle
                    navigator.grabbedLeft = true;
                    navigator.otherHandlePos = navigator.zoomedMax;
                    navigator.fixedExtreme = reverse ? baseXAxis.min : baseXAxis.max;
                } else {
                    // Grab the right handle
                    navigator.grabbedRight = true;
                    navigator.otherHandlePos = navigator.zoomedMin;
                    navigator.fixedExtreme = reverse ? baseXAxis.max : baseXAxis.min;
                }

                chart.fixedRange = null;
            },
            /**
             * Mouse move event based on x/y mouse position.
             * @param {Object} e Mouse event
             */
            onMouseMove: function(e) {
                var navigator = this,
                    chart = navigator.chart,
                    left = navigator.left,
                    navigatorSize = navigator.navigatorSize,
                    range = navigator.range,
                    dragOffset = navigator.dragOffset,
                    inverted = chart.inverted,
                    chartX;


                // In iOS, a mousemove event with e.pageX === 0 is fired when holding
                // the finger down in the center of the scrollbar. This should be
                // ignored.
                if (!e.touches || e.touches[0].pageX !== 0) { // #4696

                    e = chart.pointer.normalize(e);
                    chartX = e.chartX;

                    // Swap some options for inverted chart
                    if (inverted) {
                        left = navigator.top;
                        chartX = e.chartY;
                    }

                    // Drag left handle or top handle
                    if (navigator.grabbedLeft) {
                        navigator.hasDragged = true;
                        navigator.render(
                            0,
                            0,
                            chartX - left,
                            navigator.otherHandlePos
                        );
                        // Drag right handle or bottom handle
                    } else if (navigator.grabbedRight) {
                        navigator.hasDragged = true;
                        navigator.render(
                            0,
                            0,
                            navigator.otherHandlePos,
                            chartX - left
                        );
                        // Drag scrollbar or open area in navigator
                    } else if (navigator.grabbedCenter) {
                        navigator.hasDragged = true;
                        if (chartX < dragOffset) { // outside left
                            chartX = dragOffset;
                            // outside right
                        } else if (chartX > navigatorSize + dragOffset - range) {
                            chartX = navigatorSize + dragOffset - range;
                        }

                        navigator.render(
                            0,
                            0,
                            chartX - dragOffset,
                            chartX - dragOffset + range
                        );
                    }
                    if (
                        navigator.hasDragged &&
                        navigator.scrollbar &&
                        navigator.scrollbar.options.liveRedraw
                    ) {
                        e.DOMType = e.type; // DOMType is for IE8
                        setTimeout(function() {
                            navigator.onMouseUp(e);
                        }, 0);
                    }
                }
            },

            /**
             * Mouse up event based on x/y mouse position.
             * @param {Object} e Mouse event
             */
            onMouseUp: function(e) {
                var navigator = this,
                    chart = navigator.chart,
                    xAxis = navigator.xAxis,
                    scrollbar = navigator.scrollbar,
                    fixedMin,
                    fixedMax,
                    ext,
                    DOMEvent = e.DOMEvent || e;

                if (
                    // MouseUp is called for both, navigator and scrollbar (that order),
                    // which causes calling afterSetExtremes twice. Prevent first call
                    // by checking if scrollbar is going to set new extremes (#6334)
                    (navigator.hasDragged && (!scrollbar || !scrollbar.hasDragged)) ||
                    e.trigger === 'scrollbar'
                ) {
                    // When dragging one handle, make sure the other one doesn't change
                    if (navigator.zoomedMin === navigator.otherHandlePos) {
                        fixedMin = navigator.fixedExtreme;
                    } else if (navigator.zoomedMax === navigator.otherHandlePos) {
                        fixedMax = navigator.fixedExtreme;
                    }
                    // Snap to right edge (#4076)
                    if (navigator.zoomedMax === navigator.size) {
                        fixedMax = navigator.getUnionExtremes().dataMax;
                    }
                    ext = xAxis.toFixedRange(
                        navigator.zoomedMin,
                        navigator.zoomedMax,
                        fixedMin,
                        fixedMax
                    );

                    if (defined(ext.min)) {
                        chart.xAxis[0].setExtremes(
                            Math.min(ext.min, ext.max),
                            Math.max(ext.min, ext.max),
                            true,
                            // Run animation when clicking buttons, scrollbar track etc,
                            // but not when dragging handles or scrollbar
                            navigator.hasDragged ? false : null, {
                                trigger: 'navigator',
                                triggerOp: 'navigator-drag',
                                DOMEvent: DOMEvent // #1838
                            }
                        );
                    }
                }

                if (e.DOMType !== 'mousemove') {
                    navigator.grabbedLeft = navigator.grabbedRight =
                        navigator.grabbedCenter = navigator.fixedWidth =
                        navigator.fixedExtreme = navigator.otherHandlePos =
                        navigator.hasDragged = navigator.dragOffset = null;
                }
            },

            /**
             * Removes the event handlers attached previously with addEvents.
             */
            removeEvents: function() {
                if (this.eventsToUnbind) {
                    each(this.eventsToUnbind, function(unbind) {
                        unbind();
                    });
                    this.eventsToUnbind = undefined;
                }
                this.removeBaseSeriesEvents();
            },

            /**
             * Remove data events.
             */
            removeBaseSeriesEvents: function() {
                var baseSeries = this.baseSeries || [];
                if (this.navigatorEnabled && baseSeries[0]) {
                    if (this.navigatorOptions.adaptToUpdatedData !== false) {
                        each(baseSeries, function(series) {
                            removeEvent(series, 'updatedData', this.updatedDataHandler);
                        }, this);
                    }

                    // We only listen for extremes-events on the first baseSeries
                    if (baseSeries[0].xAxis) {
                        removeEvent(
                            baseSeries[0].xAxis,
                            'foundExtremes',
                            this.modifyBaseAxisExtremes
                        );
                    }
                }
            },

            /**
             * Initiate the Navigator object
             */
            init: function(chart) {
                var chartOptions = chart.options,
                    navigatorOptions = chartOptions.navigator,
                    navigatorEnabled = navigatorOptions.enabled,
                    scrollbarOptions = chartOptions.scrollbar,
                    scrollbarEnabled = scrollbarOptions.enabled,
                    height = navigatorEnabled ? navigatorOptions.height : 0,
                    scrollbarHeight = scrollbarEnabled ? scrollbarOptions.height : 0;

                this.handles = [];
                this.shades = [];

                this.chart = chart;
                this.setBaseSeries();

                this.height = height;
                this.scrollbarHeight = scrollbarHeight;
                this.scrollbarEnabled = scrollbarEnabled;
                this.navigatorEnabled = navigatorEnabled;
                this.navigatorOptions = navigatorOptions;
                this.scrollbarOptions = scrollbarOptions;
                this.outlineHeight = height + scrollbarHeight;

                this.opposite = pick(
                    navigatorOptions.opposite, !navigatorEnabled && chart.inverted
                ); // #6262

                var navigator = this,
                    baseSeries = navigator.baseSeries,
                    xAxisIndex = chart.xAxis.length,
                    yAxisIndex = chart.yAxis.length,
                    baseXaxis = baseSeries && baseSeries[0] && baseSeries[0].xAxis ||
                    chart.xAxis[0];

                // Make room for the navigator, can be placed around the chart:
                chart.extraMargin = {
                    type: navigator.opposite ? 'plotTop' : 'marginBottom',
                    value: (
                        navigatorEnabled || !chart.inverted ?
                        navigator.outlineHeight :
                        0
                    ) + navigatorOptions.margin
                };
                if (chart.inverted) {
                    chart.extraMargin.type = navigator.opposite ?
                        'marginRight' :
                        'plotLeft';
                }
                chart.isDirtyBox = true;

                if (navigator.navigatorEnabled) {
                    // an x axis is required for scrollbar also
                    navigator.xAxis = new Axis(chart, merge({
                        // inherit base xAxis' break and ordinal options
                        breaks: baseXaxis.options.breaks,
                        ordinal: baseXaxis.options.ordinal
                    }, navigatorOptions.xAxis, {
                        id: 'navigator-x-axis',
                        yAxis: 'navigator-y-axis',
                        isX: true,
                        type: 'datetime',
                        index: xAxisIndex,
                        offset: 0,
                        keepOrdinalPadding: true, // #2436
                        startOnTick: false,
                        endOnTick: false,
                        minPadding: 0,
                        maxPadding: 0,
                        zoomEnabled: false
                    }, chart.inverted ? {
                        offsets: [scrollbarHeight, 0, -scrollbarHeight, 0],
                        width: height
                    } : {
                        offsets: [0, -scrollbarHeight, 0, scrollbarHeight],
                        height: height
                    }));

                    navigator.yAxis = new Axis(chart, merge(navigatorOptions.yAxis, {
                        id: 'navigator-y-axis',
                        alignTicks: false,
                        offset: 0,
                        index: yAxisIndex,
                        zoomEnabled: false
                    }, chart.inverted ? {
                        width: height
                    } : {
                        height: height
                    }));

                    // If we have a base series, initialize the navigator series
                    if (baseSeries || navigatorOptions.series.data) {
                        navigator.updateNavigatorSeries();

                        // If not, set up an event to listen for added series
                    } else if (chart.series.length === 0) {

                        wrap(chart, 'redraw', function(proceed, animation) {
                            // We've got one, now add it as base and reset chart.redraw
                            if (chart.series.length > 0 && !navigator.series) {
                                navigator.setBaseSeries();
                                chart.redraw = proceed; // reset
                            }
                            proceed.call(chart, animation);
                        });
                    }

                    // Render items, so we can bind events to them:
                    navigator.renderElements();
                    // Add mouse events
                    navigator.addMouseEvents();

                    // in case of scrollbar only, fake an x axis to get translation
                } else {
                    navigator.xAxis = {
                        translate: function(value, reverse) {
                            var axis = chart.xAxis[0],
                                ext = axis.getExtremes(),
                                scrollTrackWidth = axis.len - 2 * scrollbarHeight,
                                min = numExt('min', axis.options.min, ext.dataMin),
                                valueRange = numExt(
                                    'max',
                                    axis.options.max,
                                    ext.dataMax
                                ) - min;

                            return reverse ?
                                // from pixel to value
                                (value * valueRange / scrollTrackWidth) + min :
                                // from value to pixel
                                scrollTrackWidth * (value - min) / valueRange;
                        },
                        toPixels: function(value) {
                            return this.translate(value);
                        },
                        toValue: function(value) {
                            return this.translate(value, true);
                        },
                        toFixedRange: Axis.prototype.toFixedRange,
                        fake: true
                    };
                }


                // Initialize the scrollbar
                if (chart.options.scrollbar.enabled) {
                    chart.scrollbar = navigator.scrollbar = new Scrollbar(
                        chart.renderer,
                        merge(chart.options.scrollbar, {
                            margin: navigator.navigatorEnabled ? 0 : 10,
                            vertical: chart.inverted
                        }),
                        chart
                    );
                    addEvent(navigator.scrollbar, 'changed', function(e) {
                        var range = navigator.size,
                            to = range * this.to,
                            from = range * this.from;

                        navigator.hasDragged = navigator.scrollbar.hasDragged;
                        navigator.render(0, 0, from, to);

                        if (
                            chart.options.scrollbar.liveRedraw ||
                            e.DOMType !== 'mousemove'
                        ) {
                            setTimeout(function() {
                                navigator.onMouseUp(e);
                            });
                        }
                    });
                }

                // Add data events
                navigator.addBaseSeriesEvents();
                // Add redraw events
                navigator.addChartEvents();
            },

            /**
             * Get the union data extremes of the chart - the outer data extremes of the
             * base X axis and the navigator axis.
             * @param {boolean} returnFalseOnNoBaseSeries - as the param says.
             */
            getUnionExtremes: function(returnFalseOnNoBaseSeries) {
                var baseAxis = this.chart.xAxis[0],
                    navAxis = this.xAxis,
                    navAxisOptions = navAxis.options,
                    baseAxisOptions = baseAxis.options,
                    ret;

                if (!returnFalseOnNoBaseSeries || baseAxis.dataMin !== null) {
                    ret = {
                        dataMin: pick( // #4053
                            navAxisOptions && navAxisOptions.min,
                            numExt(
                                'min',
                                baseAxisOptions.min,
                                baseAxis.dataMin,
                                navAxis.dataMin,
                                navAxis.min
                            )
                        ),
                        dataMax: pick(
                            navAxisOptions && navAxisOptions.max,
                            numExt(
                                'max',
                                baseAxisOptions.max,
                                baseAxis.dataMax,
                                navAxis.dataMax,
                                navAxis.max
                            )
                        )
                    };
                }
                return ret;
            },

            /**
             * Set the base series and update the navigator series from this. With a bit
             * of modification we should be able to make this an API method to be called
             * from the outside
             * @param  {Object} baseSeriesOptions
             *         Additional series options for a navigator
             * @param  {Boolean} [redraw]
             *         Whether to redraw after update.
             */
            setBaseSeries: function(baseSeriesOptions, redraw) {
                var chart = this.chart,
                    baseSeries = this.baseSeries = [];

                baseSeriesOptions = (
                    baseSeriesOptions ||
                    chart.options && chart.options.navigator.baseSeries ||
                    0
                );

                // Iterate through series and add the ones that should be shown in
                // navigator.
                each(chart.series || [], function(series, i) {
                    if (
                        // Don't include existing nav series
                        !series.options.isInternal &&
                        (
                            series.options.showInNavigator ||
                            (
                                i === baseSeriesOptions ||
                                series.options.id === baseSeriesOptions
                            ) &&
                            series.options.showInNavigator !== false
                        )
                    ) {
                        baseSeries.push(series);
                    }
                });

                // When run after render, this.xAxis already exists
                if (this.xAxis && !this.xAxis.fake) {
                    this.updateNavigatorSeries(redraw);
                }
            },

            /*
             * Update series in the navigator from baseSeries, adding new if does not
             * exist.
             */
            updateNavigatorSeries: function(redraw) {
                var navigator = this,
                    chart = navigator.chart,
                    baseSeries = navigator.baseSeries,
                    baseOptions,
                    mergedNavSeriesOptions,
                    chartNavigatorSeriesOptions = navigator.navigatorOptions.series,
                    baseNavigatorOptions,
                    navSeriesMixin = {
                        enableMouseTracking: false,
                        index: null, // #6162
                        linkedTo: null, // #6734
                        group: 'nav', // for columns
                        padXAxis: false,
                        xAxis: 'navigator-x-axis',
                        yAxis: 'navigator-y-axis',
                        showInLegend: false,
                        stacking: false, // #4823
                        isInternal: true,
                        visible: true
                    },
                    // Remove navigator series that are no longer in the baseSeries
                    navigatorSeries = navigator.series = H.grep(
                        navigator.series || [],
                        function(navSeries) {
                            var base = navSeries.baseSeries;
                            if (H.inArray(base, baseSeries) < 0) { // Not in array
                                // If there is still a base series connected to this
                                // series, remove event handler and reference.
                                if (base) {
                                    removeEvent(
                                        base,
                                        'updatedData',
                                        navigator.updatedDataHandler
                                    );
                                    delete base.navigatorSeries;
                                }
                                // Kill the nav series
                                navSeries.destroy();
                                return false;
                            }
                            return true;
                        }
                    );

                // Go through each base series and merge the options to create new
                // series
                if (baseSeries && baseSeries.length) {
                    each(baseSeries, function eachBaseSeries(base) {
                        var linkedNavSeries = base.navigatorSeries,
                            userNavOptions = extend(
                                // Grab color from base as default
                                {
                                    color: base.color
                                }, !isArray(chartNavigatorSeriesOptions) ?
                                chartNavigatorSeriesOptions :
                                defaultOptions.navigator.series
                            );

                        // Don't update if the series exists in nav and we have disabled
                        // adaptToUpdatedData.
                        if (
                            linkedNavSeries &&
                            navigator.navigatorOptions.adaptToUpdatedData === false
                        ) {
                            return;
                        }

                        navSeriesMixin.name = 'Navigator ' + baseSeries.length;

                        baseOptions = base.options || {};
                        baseNavigatorOptions = baseOptions.navigatorOptions || {};
                        mergedNavSeriesOptions = merge(
                            baseOptions,
                            navSeriesMixin,
                            userNavOptions,
                            baseNavigatorOptions
                        );

                        // Merge data separately. Do a slice to avoid mutating the
                        // navigator options from base series (#4923).
                        var navigatorSeriesData =
                            baseNavigatorOptions.data || userNavOptions.data;
                        navigator.hasNavigatorData =
                            navigator.hasNavigatorData || !!navigatorSeriesData;
                        mergedNavSeriesOptions.data =
                            navigatorSeriesData ||
                            baseOptions.data && baseOptions.data.slice(0);

                        // Update or add the series
                        if (linkedNavSeries && linkedNavSeries.options) {
                            linkedNavSeries.update(mergedNavSeriesOptions, redraw);
                        } else {
                            base.navigatorSeries = chart.initSeries(
                                mergedNavSeriesOptions
                            );
                            base.navigatorSeries.baseSeries = base; // Store ref
                            navigatorSeries.push(base.navigatorSeries);
                        }
                    });
                }

                // If user has defined data (and no base series) or explicitly defined
                // navigator.series as an array, we create these series on top of any
                // base series.
                if (
                    chartNavigatorSeriesOptions.data &&
                    !(baseSeries && baseSeries.length) ||
                    isArray(chartNavigatorSeriesOptions)
                ) {
                    navigator.hasNavigatorData = false;
                    // Allow navigator.series to be an array
                    chartNavigatorSeriesOptions = H.splat(chartNavigatorSeriesOptions);
                    each(chartNavigatorSeriesOptions, function(userSeriesOptions, i) {
                        navSeriesMixin.name =
                            'Navigator ' + (navigatorSeries.length + 1);
                        mergedNavSeriesOptions = merge(
                            defaultOptions.navigator.series, {
                                // Since we don't have a base series to pull color from,
                                // try to fake it by using color from series with same
                                // index. Otherwise pull from the colors array. We need
                                // an explicit color as otherwise updates will increment
                                // color counter and we'll get a new color for each
                                // update of the nav series.
                                color: chart.series[i] &&
                                    !chart.series[i].options.isInternal &&
                                    chart.series[i].color ||
                                    chart.options.colors[i] ||
                                    chart.options.colors[0]
                            },
                            navSeriesMixin,
                            userSeriesOptions
                        );
                        mergedNavSeriesOptions.data = userSeriesOptions.data;
                        if (mergedNavSeriesOptions.data) {
                            navigator.hasNavigatorData = true;
                            navigatorSeries.push(
                                chart.initSeries(mergedNavSeriesOptions)
                            );
                        }
                    });
                }

                this.addBaseSeriesEvents();
            },

            /**
             * Add data events.
             * For example when main series is updated we need to recalculate extremes
             */
            addBaseSeriesEvents: function() {
                var navigator = this,
                    baseSeries = navigator.baseSeries || [];

                // Bind modified extremes event to first base's xAxis only.
                // In event of > 1 base-xAxes, the navigator will ignore those.
                // Adding this multiple times to the same axis is no problem, as
                // duplicates should be discarded by the browser.
                if (baseSeries[0] && baseSeries[0].xAxis) {
                    addEvent(
                        baseSeries[0].xAxis,
                        'foundExtremes',
                        this.modifyBaseAxisExtremes
                    );
                }

                each(baseSeries, function(base) {
                    // Link base series show/hide to navigator series visibility
                    addEvent(base, 'show', function() {
                        if (this.navigatorSeries) {
                            this.navigatorSeries.setVisible(true, false);
                        }
                    });
                    addEvent(base, 'hide', function() {
                        if (this.navigatorSeries) {
                            this.navigatorSeries.setVisible(false, false);
                        }
                    });

                    // Respond to updated data in the base series, unless explicitily
                    // not adapting to data changes.
                    if (this.navigatorOptions.adaptToUpdatedData !== false) {
                        if (base.xAxis) {
                            addEvent(base, 'updatedData', this.updatedDataHandler);
                        }
                    }

                    // Handle series removal
                    addEvent(base, 'remove', function() {
                        if (this.navigatorSeries) {
                            erase(navigator.series, this.navigatorSeries);
                            this.navigatorSeries.remove(false);
                            delete this.navigatorSeries;
                        }
                    });
                }, this);
            },

            /**
             * Set the navigator x axis extremes to reflect the total. The navigator
             * extremes should always be the extremes of the union of all series in the
             * chart as well as the navigator series.
             */
            modifyNavigatorAxisExtremes: function() {
                var xAxis = this.xAxis,
                    unionExtremes;

                if (xAxis.getExtremes) {
                    unionExtremes = this.getUnionExtremes(true);
                    if (
                        unionExtremes &&
                        (
                            unionExtremes.dataMin !== xAxis.min ||
                            unionExtremes.dataMax !== xAxis.max
                        )
                    ) {
                        xAxis.min = unionExtremes.dataMin;
                        xAxis.max = unionExtremes.dataMax;
                    }
                }
            },

            /**
             * Hook to modify the base axis extremes with information from the Navigator
             */
            modifyBaseAxisExtremes: function() {
                var baseXAxis = this,
                    navigator = baseXAxis.chart.navigator,
                    baseExtremes = baseXAxis.getExtremes(),
                    baseMin = baseExtremes.min,
                    baseMax = baseExtremes.max,
                    baseDataMin = baseExtremes.dataMin,
                    baseDataMax = baseExtremes.dataMax,
                    range = baseMax - baseMin,
                    stickToMin = navigator.stickToMin,
                    stickToMax = navigator.stickToMax,
                    overscroll = baseXAxis.options.overscroll,
                    newMax,
                    newMin,
                    navigatorSeries = navigator.series && navigator.series[0],
                    hasSetExtremes = !!baseXAxis.setExtremes,

                    // When the extremes have been set by range selector button, don't
                    // stick to min or max. The range selector buttons will handle the
                    // extremes. (#5489)
                    unmutable = baseXAxis.eventArgs &&
                    baseXAxis.eventArgs.trigger === 'rangeSelectorButton';

                if (!unmutable) {

                    // If the zoomed range is already at the min, move it to the right
                    // as new data comes in
                    if (stickToMin) {
                        newMin = baseDataMin;
                        newMax = newMin + range;
                    }

                    // If the zoomed range is already at the max, move it to the right
                    // as new data comes in
                    if (stickToMax) {
                        newMax = baseDataMax + overscroll;

                        // if stickToMin is true, the new min value is set above
                        if (!stickToMin) {
                            newMin = Math.max(
                                newMax - range,
                                navigatorSeries && navigatorSeries.xData ?
                                navigatorSeries.xData[0] : -Number.MAX_VALUE
                            );
                        }
                    }

                    // Update the extremes
                    if (hasSetExtremes && (stickToMin || stickToMax)) {
                        if (isNumber(newMin)) {
                            baseXAxis.min = baseXAxis.userMin = newMin;
                            baseXAxis.max = baseXAxis.userMax = newMax;
                        }
                    }
                }

                // Reset
                navigator.stickToMin = navigator.stickToMax = null;
            },

            /**
             * Handler for updated data on the base series. When data is modified, the
             * navigator series must reflect it. This is called from the Chart.redraw
             * function before axis and series extremes are computed.
             */
            updatedDataHandler: function() {
                var navigator = this.chart.navigator,
                    baseSeries = this,
                    navigatorSeries = this.navigatorSeries;

                // If the scrollbar is scrolled all the way to the right, keep right as
                // new data  comes in.
                navigator.stickToMax =
                    Math.round(navigator.zoomedMax) >= Math.round(navigator.size);

                // Detect whether the zoomed area should stick to the minimum or
                // maximum. If the current axis minimum falls outside the new updated
                // dataset, we must adjust.
                navigator.stickToMin = isNumber(baseSeries.xAxis.min) &&
                    (baseSeries.xAxis.min <= baseSeries.xData[0]) &&
                    (!this.chart.fixedRange || !navigator.stickToMax);

                // Set the navigator series data to the new data of the base series
                if (navigatorSeries && !navigator.hasNavigatorData) {
                    navigatorSeries.options.pointStart = baseSeries.xData[0];
                    navigatorSeries.setData(
                        baseSeries.options.data,
                        false,
                        null,
                        false
                    ); // #5414
                }
            },

            /**
             * Add chart events, like redrawing navigator, when chart requires that.
             */
            addChartEvents: function() {
                addEvent(this.chart, 'redraw', function() {
                    // Move the scrollbar after redraw, like after data updata even if
                    // axes don't redraw
                    var navigator = this.navigator,
                        xAxis = navigator && (
                            navigator.baseSeries &&
                            navigator.baseSeries[0] &&
                            navigator.baseSeries[0].xAxis ||
                            navigator.scrollbar && this.xAxis[0]
                        ); // #5709

                    if (xAxis) {
                        navigator.render(xAxis.min, xAxis.max);
                    }
                });
            },

            /**
             * Destroys allocated elements.
             */
            destroy: function() {

                // Disconnect events added in addEvents
                this.removeEvents();

                if (this.xAxis) {
                    erase(this.chart.xAxis, this.xAxis);
                    erase(this.chart.axes, this.xAxis);
                }
                if (this.yAxis) {
                    erase(this.chart.yAxis, this.yAxis);
                    erase(this.chart.axes, this.yAxis);
                }
                // Destroy series
                each(this.series || [], function(s) {
                    if (s.destroy) {
                        s.destroy();
                    }
                });

                // Destroy properties
                each([
                    'series', 'xAxis', 'yAxis', 'shades', 'outline', 'scrollbarTrack',
                    'scrollbarRifles', 'scrollbarGroup', 'scrollbar', 'navigatorGroup',
                    'rendered'
                ], function(prop) {
                    if (this[prop] && this[prop].destroy) {
                        this[prop].destroy();
                    }
                    this[prop] = null;
                }, this);

                // Destroy elements in collection
                each([this.handles], function(coll) {
                    destroyObjectProperties(coll);
                }, this);
            }
        };

        H.Navigator = Navigator;

        /**
         * For Stock charts, override selection zooming with some special features
         * because X axis zooming is already allowed by the Navigator and Range
         * selector.
         */
        wrap(Axis.prototype, 'zoom', function(proceed, newMin, newMax) {
            var chart = this.chart,
                chartOptions = chart.options,
                zoomType = chartOptions.chart.zoomType,
                previousZoom,
                navigator = chartOptions.navigator,
                rangeSelector = chartOptions.rangeSelector,
                ret;

            if (this.isXAxis && ((navigator && navigator.enabled) ||
                    (rangeSelector && rangeSelector.enabled))) {

                // For x only zooming, fool the chart.zoom method not to create the zoom
                // button because the property already exists
                if (zoomType === 'x') {
                    chart.resetZoomButton = 'blocked';

                    // For y only zooming, ignore the X axis completely
                } else if (zoomType === 'y') {
                    ret = false;

                    // For xy zooming, record the state of the zoom before zoom selection,
                    // then when the reset button is pressed, revert to this state. This
                    // should apply only if the chart is initialized with a range (#6612),
                    // otherwise zoom all the way out.
                } else if (zoomType === 'xy' && this.options.range) {

                    previousZoom = this.previousZoom;
                    if (defined(newMin)) {
                        this.previousZoom = [this.min, this.max];
                    } else if (previousZoom) {
                        newMin = previousZoom[0];
                        newMax = previousZoom[1];
                        delete this.previousZoom;
                    }
                }

            }
            return ret !== undefined ? ret : proceed.call(this, newMin, newMax);
        });

        // Initialize navigator for stock charts
        wrap(Chart.prototype, 'init', function(proceed, options, callback) {

            addEvent(this, 'beforeRender', function() {
                var options = this.options;
                if (options.navigator.enabled || options.scrollbar.enabled) {
                    this.scroller = this.navigator = new Navigator(this);
                }
            });

            proceed.call(this, options, callback);

        });

        /**
         * For stock charts, extend the Chart.setChartSize method so that we can set the
         * final top position of the navigator once the height of the chart, including
         * the legend, is determined. #367. We can't use Chart.getMargins, because
         * labels offsets are not calculated yet.
         */
        wrap(Chart.prototype, 'setChartSize', function(proceed) {

            var legend = this.legend,
                navigator = this.navigator,
                scrollbarHeight,
                legendOptions,
                xAxis,
                yAxis;

            proceed.apply(this, [].slice.call(arguments, 1));

            if (navigator) {
                legendOptions = legend && legend.options;
                xAxis = navigator.xAxis;
                yAxis = navigator.yAxis;
                scrollbarHeight = navigator.scrollbarHeight;

                // Compute the top position
                if (this.inverted) {
                    navigator.left = navigator.opposite ?
                        this.chartWidth - scrollbarHeight - navigator.height :
                        this.spacing[3] + scrollbarHeight;
                    navigator.top = this.plotTop + scrollbarHeight;
                } else {
                    navigator.left = this.plotLeft + scrollbarHeight;
                    navigator.top = navigator.navigatorOptions.top ||
                        this.chartHeight -
                        navigator.height -
                        scrollbarHeight -
                        this.spacing[2] -
                        (
                            this.rangeSelector && this.extraBottomMargin ?
                            this.rangeSelector.getHeight() :
                            0
                        ) -
                        (
                            (
                                legendOptions &&
                                legendOptions.verticalAlign === 'bottom' &&
                                legendOptions.enabled &&
                                !legendOptions.floating
                            ) ?
                            legend.legendHeight + pick(legendOptions.margin, 10) :
                            0
                        );
                }

                if (xAxis && yAxis) { // false if navigator is disabled (#904)

                    if (this.inverted) {
                        xAxis.options.left = yAxis.options.left = navigator.left;
                    } else {
                        xAxis.options.top = yAxis.options.top = navigator.top;
                    }

                    xAxis.setAxisSize();
                    yAxis.setAxisSize();
                }
            }
        });

        // Pick up badly formatted point options to addPoint
        wrap(Series.prototype, 'addPoint', function(
            proceed,
            options,
            redraw,
            shift,
            animation
        ) {
            var turboThreshold = this.options.turboThreshold;
            if (
                turboThreshold &&
                this.xData.length > turboThreshold &&
                isObject(options, true) &&
                this.chart.navigator
            ) {
                error(20, true);
            }
            proceed.call(this, options, redraw, shift, animation);
        });

        // Handle adding new series
        wrap(Chart.prototype, 'addSeries', function(
            proceed,
            options,
            redraw,
            animation
        ) {
            var series = proceed.call(this, options, false, animation);
            if (this.navigator) {
                // Recompute which series should be shown in navigator, and add them
                this.navigator.setBaseSeries(null, false);
            }
            if (pick(redraw, true)) {
                this.redraw();
            }
            return series;
        });

        // Handle updating series
        wrap(Series.prototype, 'update', function(proceed, newOptions, redraw) {
            proceed.call(this, newOptions, false);
            if (this.chart.navigator && !this.options.isInternal) {
                this.chart.navigator.setBaseSeries(null, false);
            }
            if (pick(redraw, true)) {
                this.chart.redraw();
            }
        });

        Chart.prototype.callbacks.push(function(chart) {
            var extremes,
                navigator = chart.navigator;

            // Initiate the navigator
            if (navigator) {
                extremes = chart.xAxis[0].getExtremes();
                navigator.render(extremes.min, extremes.max);
            }
        });


    }(Highcharts));
    (function(H) {
        /**
         * (c) 2010-2017 Torstein Honsi
         *
         * License: www.highcharts.com/license
         */
        var addEvent = H.addEvent,
            Axis = H.Axis,
            Chart = H.Chart,
            css = H.css,
            createElement = H.createElement,
            dateFormat = H.dateFormat,
            defaultOptions = H.defaultOptions,
            useUTC = defaultOptions.global.useUTC,
            defined = H.defined,
            destroyObjectProperties = H.destroyObjectProperties,
            discardElement = H.discardElement,
            each = H.each,
            extend = H.extend,
            fireEvent = H.fireEvent,
            HCDate = H.Date,
            isNumber = H.isNumber,
            merge = H.merge,
            pick = H.pick,
            pInt = H.pInt,
            splat = H.splat,
            wrap = H.wrap;

        /* ****************************************************************************
         * Start Range Selector code												  *
         *****************************************************************************/
        extend(defaultOptions, {

            /**
             * The range selector is a tool for selecting ranges to display within
             * the chart. It provides buttons to select preconfigured ranges in
             * the chart, like 1 day, 1 week, 1 month etc. It also provides input
             * boxes where min and max dates can be manually input.
             *
             * @product highstock
             * @optionparent rangeSelector
             */
            rangeSelector: {
                // allButtonsEnabled: false,
                // enabled: true,
                // buttons: {Object}
                // buttonSpacing: 0,

                /**
                 * The vertical alignment of the rangeselector box. Allowed properties are `top`,
                 * `middle`, `bottom`.
                 *
                 * @since 6.0.0
                 *
                 * @sample {highstock} stock/rangeselector/vertical-align-middle/ Middle
                 *
                 * @sample {highstock} stock/rangeselector/vertical-align-bottom/ Bottom
                 */
                verticalAlign: 'top',

                /**
                 * A collection of attributes for the buttons. The object takes SVG
                 * attributes like `fill`, `stroke`, `stroke-width`, as well as `style`,
                 * a collection of CSS properties for the text.
                 * 
                 * The object can also be extended with states, so you can set presentational
                 * options for `hover`, `select` or `disabled` button states.
                 * 
                 * CSS styles for the text label.
                 * 
                 * In styled mode, the buttons are styled by the `.highcharts-
                 * range-selector-buttons .highcharts-button` rule with its different
                 * states.
                 * 
                 * @type {Object}
                 * @sample {highstock} stock/rangeselector/styling/ Styling the buttons and inputs
                 * @product highstock
                 */
                buttonTheme: {
                    'stroke-width': 0,
                    width: 28,
                    height: 18,
                    padding: 2,
                    zIndex: 7 // #484, #852
                },

                /**
                 * When the rangeselector is floating, the plot area does not reserve 
                 * space for it. This opens for positioning anywhere on the chart.
                 * 
                 * @sample {highstock} stock/rangeselector/floating/
                 *         Placing the range selector between the plot area and the
                 *         navigator
                 * @since 6.0.0
                 * @product highstock
                 */
                floating: false,

                /**
                 * The x offset of the range selector relative to its horizontal
                 * alignment within `chart.spacingLeft` and `chart.spacingRight`.
                 * 
                 * @since 6.0.0
                 * @product highstock
                 */
                x: 0,

                /**
                 * The y offset of the range selector relative to its horizontal
                 * alignment within `chart.spacingLeft` and `chart.spacingRight`.
                 * 
                 * @since 6.0.0
                 * @product highstock
                 */
                y: 0,

                /**
                 * Deprecated. The height of the range selector. Currently it is
                 * calculated dynamically.
                 * 
                 * @type {Number}
                 * @default undefined
                 * @since 2.1.9
                 * @product highstock
                 * @deprecated true
                 */
                height: undefined, // reserved space for buttons and input

                /**
                 * Positioning for the input boxes. Allowed properties are `align`,
                 *  `x` and `y`.
                 * 
                 * @type {Object}
                 * @default { align: "right" }
                 * @since 1.2.4
                 * @product highstock
                 */
                inputPosition: {
                    /**
                     * The alignment of the input box. Allowed properties are `left`,
                     * `center`, `right`.
                     * @validvalue ["left", "center", "right"]
                     * @sample {highstock} stock/rangeselector/input-button-position/ 
                     *         Alignment
                     * @since 6.0.0
                     */
                    align: 'right',
                    x: 0,
                    y: 0
                },

                /**
                 * Positioning for the button row.
                 * 
                 * @since 1.2.4
                 * @product highstock
                 */
                buttonPosition: {
                    /**
                     * The alignment of the input box. Allowed properties are `left`,
                     * `center`, `right`.
                     *
                     * @validvalue ["left", "center", "right"]
                     * @sample {highstock} stock/rangeselector/input-button-position/ 
                     *         Alignment
                     * @since 6.0.0
                     */
                    align: 'left',
                    /**
                     * X offset of the button row.
                     */
                    x: 0,
                    /**
                     * Y offset of the button row.
                     */
                    y: 0
                },
                // inputDateFormat: '%b %e, %Y',
                // inputEditDateFormat: '%Y-%m-%d',
                // inputEnabled: true,
                // selected: undefined,

                // inputStyle: {},

                /**
                 * CSS styles for the labels - the Zoom, From and To texts.
                 * 
                 * In styled mode, the labels are styled by the `.highcharts-range-label` class.
                 * 
                 * @type {CSSObject}
                 * @sample {highstock} stock/rangeselector/styling/ Styling the buttons and inputs
                 * @product highstock
                 */
                labelStyle: {
                    color: '#666666'
                }

            }
        });

        defaultOptions.lang = merge(
            defaultOptions.lang,
            /**
             * Language object. The language object is global and it can't be set
             * on each chart initiation. Instead, use `Highcharts.setOptions` to
             * set it before any chart is initialized.
             * 
             * <pre>Highcharts.setOptions({
             *     lang: {
             *         months: [
             *             'Janvier', 'Février', 'Mars', 'Avril',
             *             'Mai', 'Juin', 'Juillet', 'Août',
             *             'Septembre', 'Octobre', 'Novembre', 'Décembre'
             *         ],
             *         weekdays: [
             *             'Dimanche', 'Lundi', 'Mardi', 'Mercredi',
             *             'Jeudi', 'Vendredi', 'Samedi'
             *         ]
             *     }
             * });</pre>
             *
             * @optionparent lang
             * @product highstock
             */
            {

                /**
                 * The text for the label for the range selector buttons.
                 * 
                 * @type {String}
                 * @default Zoom
                 * @product highstock
                 */
                rangeSelectorZoom: 'Zoom',

                /**
                 * The text for the label for the "from" input box in the range
                 * selector.
                 * 
                 * @type {String}
                 * @default From
                 * @product highstock
                 */
                rangeSelectorFrom: 'From',

                /**
                 * The text for the label for the "to" input box in the range selector.
                 * 
                 * @type {String}
                 * @default To
                 * @product highstock
                 */
                rangeSelectorTo: 'To'
            }
        );

        /**
         * The range selector.
         * @class
         * @param {Object} chart
         */
        function RangeSelector(chart) {

            // Run RangeSelector
            this.init(chart);
        }

        RangeSelector.prototype = {
            /**
             * The method to run when one of the buttons in the range selectors is clicked
             * @param {Number} i The index of the button
             * @param {Object} rangeOptions
             * @param {Boolean} redraw
             */
            clickButton: function(i, redraw) {
                var rangeSelector = this,
                    chart = rangeSelector.chart,
                    rangeOptions = rangeSelector.buttonOptions[i],
                    baseAxis = chart.xAxis[0],
                    unionExtremes = (chart.scroller && chart.scroller.getUnionExtremes()) || baseAxis || {},
                    dataMin = unionExtremes.dataMin,
                    dataMax = unionExtremes.dataMax,
                    newMin,
                    newMax = baseAxis && Math.round(Math.min(baseAxis.max, pick(dataMax, baseAxis.max))), // #1568
                    type = rangeOptions.type,
                    baseXAxisOptions,
                    range = rangeOptions._range,
                    rangeMin,
                    minSetting,
                    rangeSetting,
                    ctx,
                    ytdExtremes,
                    dataGrouping = rangeOptions.dataGrouping;

                if (dataMin === null || dataMax === null) { // chart has no data, base series is removed
                    return;
                }

                // Set the fixed range before range is altered
                chart.fixedRange = range;

                // Apply dataGrouping associated to button
                if (dataGrouping) {
                    this.forcedDataGrouping = true;
                    Axis.prototype.setDataGrouping.call(baseAxis || {
                        chart: this.chart
                    }, dataGrouping, false);
                }

                // Apply range
                if (type === 'month' || type === 'year') {
                    if (!baseAxis) {
                        // This is set to the user options and picked up later when the axis is instantiated
                        // so that we know the min and max.
                        range = rangeOptions;
                    } else {
                        ctx = {
                            range: rangeOptions,
                            max: newMax,
                            dataMin: dataMin,
                            dataMax: dataMax
                        };
                        newMin = baseAxis.minFromRange.call(ctx);
                        if (isNumber(ctx.newMax)) {
                            newMax = ctx.newMax;
                        }
                    }

                    // Fixed times like minutes, hours, days
                } else if (range) {
                    newMin = Math.max(newMax - range, dataMin);
                    newMax = Math.min(newMin + range, dataMax);

                } else if (type === 'ytd') {

                    // On user clicks on the buttons, or a delayed action running from the beforeRender
                    // event (below), the baseAxis is defined.
                    if (baseAxis) {
                        // When "ytd" is the pre-selected button for the initial view, its calculation
                        // is delayed and rerun in the beforeRender event (below). When the series
                        // are initialized, but before the chart is rendered, we have access to the xData
                        // array (#942).
                        if (dataMax === undefined) {
                            dataMin = Number.MAX_VALUE;
                            dataMax = Number.MIN_VALUE;
                            each(chart.series, function(series) {
                                var xData = series.xData; // reassign it to the last item
                                dataMin = Math.min(xData[0], dataMin);
                                dataMax = Math.max(xData[xData.length - 1], dataMax);
                            });
                            redraw = false;
                        }
                        ytdExtremes = rangeSelector.getYTDExtremes(dataMax, dataMin, useUTC);
                        newMin = rangeMin = ytdExtremes.min;
                        newMax = ytdExtremes.max;

                        // "ytd" is pre-selected. We don't yet have access to processed point and extremes data
                        // (things like pointStart and pointInterval are missing), so we delay the process (#942)
                    } else {
                        addEvent(chart, 'beforeRender', function() {
                            rangeSelector.clickButton(i);
                        });
                        return;
                    }
                } else if (type === 'all' && baseAxis) {
                    newMin = dataMin;
                    newMax = dataMax;
                }

                newMin += rangeOptions._offsetMin;
                newMax += rangeOptions._offsetMax;

                rangeSelector.setSelected(i);

                // Update the chart
                if (!baseAxis) {
                    // Axis not yet instanciated. Temporarily set min and range
                    // options and remove them on chart load (#4317).
                    baseXAxisOptions = splat(chart.options.xAxis)[0];
                    rangeSetting = baseXAxisOptions.range;
                    baseXAxisOptions.range = range;
                    minSetting = baseXAxisOptions.min;
                    baseXAxisOptions.min = rangeMin;
                    addEvent(chart, 'load', function resetMinAndRange() {
                        baseXAxisOptions.range = rangeSetting;
                        baseXAxisOptions.min = minSetting;
                    });
                } else {
                    // Existing axis object. Set extremes after render time.
                    baseAxis.setExtremes(
                        newMin,
                        newMax,
                        pick(redraw, 1),
                        null, // auto animation
                        {
                            trigger: 'rangeSelectorButton',
                            rangeSelectorButton: rangeOptions
                        }
                    );
                }
            },

            /**
             * Set the selected option. This method only sets the internal flag, it
             * doesn't update the buttons or the actual zoomed range.
             */
            setSelected: function(selected) {
                this.selected = this.options.selected = selected;
            },

            /**
             * The default buttons for pre-selecting time frames
             */
            defaultButtons: [{
                type: 'month',
                count: 1,
                text: '1m'
            }, {
                type: 'month',
                count: 3,
                text: '3m'
            }, {
                type: 'month',
                count: 6,
                text: '6m'
            }, {
                type: 'ytd',
                text: 'YTD'
            }, {
                type: 'year',
                count: 1,
                text: '1y'
            }, {
                type: 'all',
                text: 'All'
            }],

            /**
             * Initialize the range selector
             */
            init: function(chart) {
                var rangeSelector = this,
                    options = chart.options.rangeSelector,
                    buttonOptions = options.buttons || [].concat(rangeSelector.defaultButtons),
                    selectedOption = options.selected,
                    blurInputs = function() {
                        var minInput = rangeSelector.minInput,
                            maxInput = rangeSelector.maxInput;

                        // #3274 in some case blur is not defined
                        if (minInput && minInput.blur) {
                            fireEvent(minInput, 'blur');
                        }
                        if (maxInput && maxInput.blur) {
                            fireEvent(maxInput, 'blur');
                        }
                    };

                rangeSelector.chart = chart;
                rangeSelector.options = options;
                rangeSelector.buttons = [];

                chart.extraTopMargin = options.height;
                rangeSelector.buttonOptions = buttonOptions;

                this.unMouseDown = addEvent(chart.container, 'mousedown', blurInputs);
                this.unResize = addEvent(chart, 'resize', blurInputs);

                // Extend the buttonOptions with actual range
                each(buttonOptions, rangeSelector.computeButtonRange);

                // zoomed range based on a pre-selected button index
                if (selectedOption !== undefined && buttonOptions[selectedOption]) {
                    this.clickButton(selectedOption, false);
                }


                addEvent(chart, 'load', function() {
                    // If a data grouping is applied to the current button, release it
                    // when extremes change
                    if (chart.xAxis && chart.xAxis[0]) {
                        addEvent(chart.xAxis[0], 'setExtremes', function(e) {
                            if (
                                this.max - this.min !== chart.fixedRange &&
                                e.trigger !== 'rangeSelectorButton' &&
                                e.trigger !== 'updatedData' &&
                                rangeSelector.forcedDataGrouping
                            ) {
                                this.setDataGrouping(false, false);
                            }
                        });
                    }
                });
            },

            /**
             * Dynamically update the range selector buttons after a new range has been
             * set
             */
            updateButtonStates: function() {
                var rangeSelector = this,
                    chart = this.chart,
                    baseAxis = chart.xAxis[0],
                    actualRange = Math.round(baseAxis.max - baseAxis.min),
                    hasNoData = !baseAxis.hasVisibleSeries,
                    day = 24 * 36e5, // A single day in milliseconds
                    unionExtremes = (
                        chart.scroller &&
                        chart.scroller.getUnionExtremes()
                    ) || baseAxis,
                    dataMin = unionExtremes.dataMin,
                    dataMax = unionExtremes.dataMax,
                    ytdExtremes = rangeSelector.getYTDExtremes(
                        dataMax,
                        dataMin,
                        useUTC
                    ),
                    ytdMin = ytdExtremes.min,
                    ytdMax = ytdExtremes.max,
                    selected = rangeSelector.selected,
                    selectedExists = isNumber(selected),
                    allButtonsEnabled = rangeSelector.options.allButtonsEnabled,
                    buttons = rangeSelector.buttons;

                each(rangeSelector.buttonOptions, function(rangeOptions, i) {
                    var range = rangeOptions._range,
                        type = rangeOptions.type,
                        count = rangeOptions.count || 1,
                        button = buttons[i],
                        state = 0,
                        disable,
                        select,
                        offsetRange = rangeOptions._offsetMax - rangeOptions._offsetMin,
                        isSelected = i === selected,
                        // Disable buttons where the range exceeds what is allowed in
                        // the current view
                        isTooGreatRange = range > dataMax - dataMin,
                        // Disable buttons where the range is smaller than the minimum
                        // range
                        isTooSmallRange = range < baseAxis.minRange,
                        // Do not select the YTD button if not explicitly told so
                        isYTDButNotSelected = false,
                        // Disable the All button if we're already showing all
                        isAllButAlreadyShowingAll = false,
                        isSameRange = range === actualRange;
                    // Months and years have a variable range so we check the extremes
                    if (
                        (type === 'month' || type === 'year') &&
                        (
                            actualRange + 36e5 >= {
                                month: 28,
                                year: 365
                            }[type] * day * count + offsetRange
                        ) &&
                        (
                            actualRange - 36e5 <= {
                                month: 31,
                                year: 366
                            }[type] * day * count + offsetRange
                        )
                    ) {
                        isSameRange = true;
                    } else if (type === 'ytd') {
                        isSameRange = (ytdMax - ytdMin + offsetRange) === actualRange;
                        isYTDButNotSelected = !isSelected;
                    } else if (type === 'all') {
                        isSameRange = baseAxis.max - baseAxis.min >= dataMax - dataMin;
                        isAllButAlreadyShowingAll = (!isSelected &&
                            selectedExists &&
                            isSameRange
                        );
                    }

                    // The new zoom area happens to match the range for a button - mark
                    // it selected. This happens when scrolling across an ordinal gap.
                    // It can be seen in the intraday demos when selecting 1h and scroll
                    // across the night gap.
                    disable = (!allButtonsEnabled &&
                        (
                            isTooGreatRange ||
                            isTooSmallRange ||
                            isAllButAlreadyShowingAll ||
                            hasNoData
                        )
                    );
                    select = (
                        (isSelected && isSameRange) ||
                        (isSameRange && !selectedExists && !isYTDButNotSelected)
                    );

                    if (disable) {
                        state = 3;
                    } else if (select) {
                        selectedExists = true; // Only one button can be selected
                        state = 2;
                    }

                    // If state has changed, update the button
                    if (button.state !== state) {
                        button.setState(state);
                    }
                });
            },

            /**
             * Compute and cache the range for an individual button
             */
            computeButtonRange: function(rangeOptions) {
                var type = rangeOptions.type,
                    count = rangeOptions.count || 1,

                    // these time intervals have a fixed number of milliseconds, as
                    // opposed to month, ytd and year
                    fixedTimes = {
                        millisecond: 1,
                        second: 1000,
                        minute: 60 * 1000,
                        hour: 3600 * 1000,
                        day: 24 * 3600 * 1000,
                        week: 7 * 24 * 3600 * 1000
                    };

                // Store the range on the button object
                if (fixedTimes[type]) {
                    rangeOptions._range = fixedTimes[type] * count;
                } else if (type === 'month' || type === 'year') {
                    rangeOptions._range = {
                        month: 30,
                        year: 365
                    }[type] * 24 * 36e5 * count;
                }

                rangeOptions._offsetMin = pick(rangeOptions.offsetMin, 0);
                rangeOptions._offsetMax = pick(rangeOptions.offsetMax, 0);
                rangeOptions._range +=
                    rangeOptions._offsetMax - rangeOptions._offsetMin;
            },

            /**
             * Set the internal and displayed value of a HTML input for the dates
             * @param {String} name
             * @param {Number} time
             */
            setInputValue: function(name, time) {
                var options = this.chart.options.rangeSelector,
                    input = this[name + 'Input'];

                if (defined(time)) {
                    input.previousValue = input.HCTime;
                    input.HCTime = time;
                }

                input.value = dateFormat(
                    options.inputEditDateFormat || '%Y-%m-%d',
                    input.HCTime
                );
                this[name + 'DateBox'].attr({
                    text: dateFormat(
                        options.inputDateFormat || '%b %e, %Y',
                        input.HCTime
                    )
                });
            },

            showInput: function(name) {
                var inputGroup = this.inputGroup,
                    dateBox = this[name + 'DateBox'];

                css(this[name + 'Input'], {
                    left: (inputGroup.translateX + dateBox.x) + 'px',
                    top: inputGroup.translateY + 'px',
                    width: (dateBox.width - 2) + 'px',
                    height: (dateBox.height - 2) + 'px',
                    border: '2px solid silver'
                });
            },

            hideInput: function(name) {
                css(this[name + 'Input'], {
                    border: 0,
                    width: '1px',
                    height: '1px'
                });
                this.setInputValue(name);
            },

            /**
             * Draw either the 'from' or the 'to' HTML input box of the range selector
             * @param {Object} name
             */
            drawInput: function(name) {
                var rangeSelector = this,
                    chart = rangeSelector.chart,
                    chartStyle = chart.renderer.style || {},
                    renderer = chart.renderer,
                    options = chart.options.rangeSelector,
                    lang = defaultOptions.lang,
                    div = rangeSelector.div,
                    isMin = name === 'min',
                    input,
                    label,
                    dateBox,
                    inputGroup = this.inputGroup;

                function updateExtremes() {
                    var inputValue = input.value,
                        value = (options.inputDateParser || Date.parse)(inputValue),
                        chartAxis = chart.xAxis[0],
                        dataAxis = chart.scroller && chart.scroller.xAxis ? chart.scroller.xAxis : chartAxis,
                        dataMin = dataAxis.dataMin,
                        dataMax = dataAxis.dataMax;
                    if (value !== input.previousValue) {
                        input.previousValue = value;
                        // If the value isn't parsed directly to a value by the browser's Date.parse method,
                        // like YYYY-MM-DD in IE, try parsing it a different way
                        if (!isNumber(value)) {
                            value = inputValue.split('-');
                            value = Date.UTC(pInt(value[0]), pInt(value[1]) - 1, pInt(value[2]));
                        }

                        if (isNumber(value)) {

                            // Correct for timezone offset (#433)
                            if (!useUTC) {
                                value = value + new Date().getTimezoneOffset() * 60 * 1000;
                            }

                            // Validate the extremes. If it goes beyound the data min or max, use the
                            // actual data extreme (#2438).
                            if (isMin) {
                                if (value > rangeSelector.maxInput.HCTime) {
                                    value = undefined;
                                } else if (value < dataMin) {
                                    value = dataMin;
                                }
                            } else {
                                if (value < rangeSelector.minInput.HCTime) {
                                    value = undefined;
                                } else if (value > dataMax) {
                                    value = dataMax;
                                }
                            }

                            // Set the extremes
                            if (value !== undefined) {
                                chartAxis.setExtremes(
                                    isMin ? value : chartAxis.min,
                                    isMin ? chartAxis.max : value,
                                    undefined,
                                    undefined, {
                                        trigger: 'rangeSelectorInput'
                                    }
                                );
                            }
                        }
                    }
                }

                // Create the text label
                this[name + 'Label'] = label = renderer.label(lang[isMin ? 'rangeSelectorFrom' : 'rangeSelectorTo'], this.inputGroup.offset)
                    .addClass('highcharts-range-label')
                    .attr({
                        padding: 2
                    })
                    .add(inputGroup);
                inputGroup.offset += label.width + 5;

                // Create an SVG label that shows updated date ranges and and records click events that
                // bring in the HTML input.
                this[name + 'DateBox'] = dateBox = renderer.label('', inputGroup.offset)
                    .addClass('highcharts-range-input')
                    .attr({
                        padding: 2,
                        width: options.inputBoxWidth || 90,
                        height: options.inputBoxHeight || 17,
                        stroke: options.inputBoxBorderColor || '#cccccc',
                        'stroke-width': 1,
                        'text-align': 'center'
                    })
                    .on('click', function() {
                        rangeSelector.showInput(name); // If it is already focused, the onfocus event doesn't fire (#3713)
                        rangeSelector[name + 'Input'].focus();
                    })
                    .add(inputGroup);
                inputGroup.offset += dateBox.width + (isMin ? 10 : 0);


                // Create the HTML input element. This is rendered as 1x1 pixel then set to the right size
                // when focused.
                this[name + 'Input'] = input = createElement('input', {
                    name: name,
                    className: 'highcharts-range-selector',
                    type: 'text'
                }, {
                    top: chart.plotTop + 'px' // prevent jump on focus in Firefox
                }, div);


                // Styles
                label.css(merge(chartStyle, options.labelStyle));

                dateBox.css(merge({
                    color: '#333333'
                }, chartStyle, options.inputStyle));

                css(input, extend({
                    position: 'absolute',
                    border: 0,
                    width: '1px', // Chrome needs a pixel to see it
                    height: '1px',
                    padding: 0,
                    textAlign: 'center',
                    fontSize: chartStyle.fontSize,
                    fontFamily: chartStyle.fontFamily,
                    top: '-9999em' // #4798
                }, options.inputStyle));


                // Blow up the input box
                input.onfocus = function() {
                    rangeSelector.showInput(name);
                };
                // Hide away the input box
                input.onblur = function() {
                    rangeSelector.hideInput(name);
                };

                // handle changes in the input boxes
                input.onchange = updateExtremes;

                input.onkeypress = function(event) {
                    // IE does not fire onchange on enter
                    if (event.keyCode === 13) {
                        updateExtremes();
                    }
                };
            },

            /**
             * Get the position of the range selector buttons and inputs. This can be overridden from outside for custom positioning.
             */
            getPosition: function() {
                var chart = this.chart,
                    options = chart.options.rangeSelector,
                    top = (options.verticalAlign) === 'top' ? chart.plotTop - chart.axisOffset[0] : 0; // set offset only for varticalAlign top

                return {
                    buttonTop: top + options.buttonPosition.y,
                    inputTop: top + options.inputPosition.y - 10
                };
            },
            /**
             * Get the extremes of YTD. 
             * Will choose dataMax if its value is lower than the current timestamp.
             * Will choose dataMin if its value is higher than the timestamp for
             * 	the start of current year.
             * @param  {number} dataMax
             * @param  {number} dataMin
             * @return {object} Returns min and max for the YTD
             */
            getYTDExtremes: function(dataMax, dataMin, useUTC) {
                var min,
                    now = new HCDate(dataMax),
                    year = now[HCDate.hcGetFullYear](),
                    startOfYear = useUTC ? HCDate.UTC(year, 0, 1) : +new HCDate(year, 0, 1); // eslint-disable-line new-cap
                min = Math.max(dataMin || 0, startOfYear);
                now = now.getTime();
                return {
                    max: Math.min(dataMax || now, now),
                    min: min
                };
            },

            /**
             * Render the range selector including the buttons and the inputs. The first time render
             * is called, the elements are created and positioned. On subsequent calls, they are
             * moved and updated.
             * @param {Number} min X axis minimum
             * @param {Number} max X axis maximum
             */
            render: function(min, max) {

                var rangeSelector = this,
                    chart = rangeSelector.chart,
                    renderer = chart.renderer,
                    container = chart.container,
                    chartOptions = chart.options,
                    navButtonOptions = chartOptions.exporting && chartOptions.exporting.enabled !== false &&
                    chartOptions.navigation && chartOptions.navigation.buttonOptions,
                    lang = defaultOptions.lang,
                    div = rangeSelector.div,
                    options = chartOptions.rangeSelector,
                    floating = options.floating,
                    buttons = rangeSelector.buttons,
                    inputGroup = rangeSelector.inputGroup,
                    buttonTheme = options.buttonTheme,
                    buttonPosition = options.buttonPosition,
                    inputPosition = options.inputPosition,
                    inputEnabled = options.inputEnabled,
                    states = buttonTheme && buttonTheme.states,
                    plotLeft = chart.plotLeft,
                    buttonLeft,
                    buttonGroup = rangeSelector.buttonGroup,
                    group,
                    groupHeight,
                    rendered = rangeSelector.rendered,
                    verticalAlign = rangeSelector.options.verticalAlign,
                    legend = chart.legend,
                    legendOptions = legend && legend.options,
                    buttonPositionY = buttonPosition.y,
                    inputPositionY = inputPosition.y,
                    animate = rendered || false,
                    exportingX = 0,
                    alignTranslateY,
                    legendHeight,
                    minPosition,
                    translateY = 0,
                    translateX;

                if (options.enabled === false) {
                    return;
                }

                // create the elements
                if (!rendered) {

                    rangeSelector.group = group = renderer.g('range-selector-group')
                        .attr({
                            zIndex: 7
                        })
                        .add();

                    rangeSelector.buttonGroup = buttonGroup = renderer.g('range-selector-buttons').add(group);

                    rangeSelector.zoomText = renderer.text(lang.rangeSelectorZoom, pick(plotLeft + buttonPosition.x, plotLeft), 15)
                        .css(options.labelStyle)
                        .add(buttonGroup);

                    // button start position
                    buttonLeft = pick(plotLeft + buttonPosition.x, plotLeft) + rangeSelector.zoomText.getBBox().width + 5;

                    each(rangeSelector.buttonOptions, function(rangeOptions, i) {

                        buttons[i] = renderer.button(
                                rangeOptions.text,
                                buttonLeft,
                                0,
                                function() {

                                    // extract events from button object and call
                                    var buttonEvents = rangeOptions.events && rangeOptions.events.click,
                                        callDefaultEvent;

                                    if (buttonEvents) {
                                        callDefaultEvent = buttonEvents.call(rangeOptions);
                                    }

                                    if (callDefaultEvent !== false) {
                                        rangeSelector.clickButton(i);
                                    }

                                    rangeSelector.isActive = true;
                                },
                                buttonTheme,
                                states && states.hover,
                                states && states.select,
                                states && states.disabled
                            )
                            .attr({
                                'text-align': 'center'
                            })
                            .add(buttonGroup);

                        // increase button position for the next button
                        buttonLeft += buttons[i].width + pick(options.buttonSpacing, 5);
                    });

                    // first create a wrapper outside the container in order to make
                    // the inputs work and make export correct
                    if (inputEnabled !== false) {
                        rangeSelector.div = div = createElement('div', null, {
                            position: 'relative',
                            height: 0,
                            zIndex: 1 // above container
                        });

                        container.parentNode.insertBefore(div, container);

                        // Create the group to keep the inputs
                        rangeSelector.inputGroup = inputGroup = renderer.g('input-group')
                            .add(group);
                        inputGroup.offset = 0;

                        rangeSelector.drawInput('min');
                        rangeSelector.drawInput('max');
                    }
                }

                plotLeft = chart.plotLeft - chart.spacing[3];
                rangeSelector.updateButtonStates();

                // detect collisiton with exporting
                if (
                    navButtonOptions &&
                    this.titleCollision(chart) &&
                    verticalAlign === 'top' &&
                    buttonPosition.align === 'right' &&
                    (
                        (buttonPosition.y + buttonGroup.getBBox().height - 12) <
                        ((navButtonOptions.y || 0) + navButtonOptions.height)
                    )
                ) {
                    exportingX = -40;
                }

                if (buttonPosition.align === 'left') {
                    translateX = buttonPosition.x - chart.spacing[3];
                } else if (buttonPosition.align === 'right') {
                    translateX = buttonPosition.x + exportingX - chart.spacing[1];
                }

                // align button group
                buttonGroup.align({
                    y: buttonPosition.y,
                    width: buttonGroup.getBBox().width,
                    align: buttonPosition.align,
                    x: translateX
                }, true, chart.spacingBox);

                // skip animation
                rangeSelector.group.placed = animate;
                rangeSelector.buttonGroup.placed = animate;

                if (inputEnabled !== false) {

                    var inputGroupX,
                        inputGroupWidth,
                        buttonGroupX,
                        buttonGroupWidth;

                    // detect collision with exporting
                    if (
                        navButtonOptions &&
                        this.titleCollision(chart) &&
                        verticalAlign === 'top' &&
                        inputPosition.align === 'right' &&
                        (
                            (inputPosition.y - inputGroup.getBBox().height - 12) <
                            ((navButtonOptions.y || 0) + navButtonOptions.height + chart.spacing[0])
                        )
                    ) {
                        exportingX = -40;
                    } else {
                        exportingX = 0;
                    }

                    if (inputPosition.align === 'left') {
                        translateX = plotLeft;
                    } else if (inputPosition.align === 'right') {
                        translateX = -Math.max(chart.axisOffset[1], -exportingX); // yAxis offset
                    }

                    // Update the alignment to the updated spacing box
                    inputGroup.align({
                        y: inputPosition.y,
                        width: inputGroup.getBBox().width,
                        align: inputPosition.align,
                        x: inputPosition.x + translateX - 2 // fix wrong getBBox() value on right align 
                    }, true, chart.spacingBox);

                    // detect collision
                    inputGroupX = inputGroup.alignAttr.translateX + inputGroup.alignOptions.x -
                        exportingX + inputGroup.getBBox().x + 2; // getBBox for detecing left margin, 2px padding to not overlap input and label

                    inputGroupWidth = inputGroup.alignOptions.width;

                    buttonGroupX = buttonGroup.alignAttr.translateX + buttonGroup.getBBox().x;
                    buttonGroupWidth = buttonGroup.getBBox().width + 20; // 20 is minimal spacing between elements

                    if (
                        (inputPosition.align === buttonPosition.align) ||
                        (
                            (buttonGroupX + buttonGroupWidth > inputGroupX) &&
                            (inputGroupX + inputGroupWidth > buttonGroupX) &&
                            (buttonPositionY < (inputPositionY + inputGroup.getBBox().height))
                        )
                    ) {

                        inputGroup.attr({
                            translateX: inputGroup.alignAttr.translateX + (chart.axisOffset[1] >= -exportingX ? 0 : -exportingX),
                            translateY: inputGroup.alignAttr.translateY + buttonGroup.getBBox().height + 10
                        });

                    }

                    // Set or reset the input values
                    rangeSelector.setInputValue('min', min);
                    rangeSelector.setInputValue('max', max);

                    // skip animation
                    rangeSelector.inputGroup.placed = animate;
                }

                // vertical align
                rangeSelector.group.align({
                    verticalAlign: verticalAlign
                }, true, chart.spacingBox);

                // set position 
                groupHeight = rangeSelector.group.getBBox().height + 20; // # 20 padding
                alignTranslateY = rangeSelector.group.alignAttr.translateY;

                // calculate bottom position 
                if (verticalAlign === 'bottom') {
                    legendHeight = legendOptions && legendOptions.verticalAlign === 'bottom' && legendOptions.enabled &&
                        !legendOptions.floating ? legend.legendHeight + pick(legendOptions.margin, 10) : 0;

                    groupHeight = groupHeight + legendHeight - 20;
                    translateY = alignTranslateY - groupHeight - (floating ? 0 : options.y) - 10; // 10 spacing

                }

                if (verticalAlign === 'top') {
                    if (floating) {
                        translateY = 0;
                    }

                    if (chart.titleOffset) {
                        translateY = chart.titleOffset + chart.options.title.margin;
                    }

                    translateY += ((chart.margin[0] - chart.spacing[0]) || 0);

                } else if (verticalAlign === 'middle') {
                    if (inputPositionY === buttonPositionY) {
                        if (inputPositionY < 0) {
                            translateY = alignTranslateY + minPosition;
                        } else {
                            translateY = alignTranslateY;
                        }
                    } else if (inputPositionY || buttonPositionY) {
                        if (inputPositionY < 0 || buttonPositionY < 0) {
                            translateY -= Math.min(inputPositionY, buttonPositionY);
                        } else {
                            translateY = alignTranslateY - groupHeight + minPosition;
                        }
                    }
                }

                rangeSelector.group.translate(
                    options.x,
                    options.y + Math.floor(translateY)
                );

                // translate HTML inputs
                if (inputEnabled !== false) {
                    rangeSelector.minInput.style.marginTop = rangeSelector.group.translateY + 'px';
                    rangeSelector.maxInput.style.marginTop = rangeSelector.group.translateY + 'px';
                }

                rangeSelector.rendered = true;
            },

            /** 
             * Extracts height of range selector 
             * @return {Number} Returns rangeSelector height
             */
            getHeight: function() {
                var rangeSelector = this,
                    options = rangeSelector.options,
                    rangeSelectorGroup = rangeSelector.group,
                    inputPosition = options.inputPosition,
                    buttonPosition = options.buttonPosition,
                    yPosition = options.y,
                    buttonPositionY = buttonPosition.y,
                    inputPositionY = inputPosition.y,
                    rangeSelectorHeight = 0,
                    minPosition;

                rangeSelectorHeight = rangeSelectorGroup ? (rangeSelectorGroup.getBBox(true).height) + 13 + yPosition : 0; // 13px to keep back compatibility

                minPosition = Math.min(inputPositionY, buttonPositionY);

                if (
                    (inputPositionY < 0 && buttonPositionY < 0) ||
                    (inputPositionY > 0 && buttonPositionY > 0)
                ) {
                    rangeSelectorHeight += Math.abs(minPosition);
                }

                return rangeSelectorHeight;
            },

            /**
             * Detect collision with title or subtitle
             * @param {object} chart
             * @return {Boolean} Returns collision status
             */
            titleCollision: function(chart) {
                return !(chart.options.title.text || chart.options.subtitle.text);
            },

            /**
             * Update the range selector with new options
             * @param {object} options
             */
            update: function(options) {
                var chart = this.chart;

                merge(true, chart.options.rangeSelector, options);
                this.destroy();
                this.init(chart);
                chart.rangeSelector.render();
            },

            /**
             * Destroys allocated elements.
             */
            destroy: function() {
                var rSelector = this,
                    minInput = rSelector.minInput,
                    maxInput = rSelector.maxInput;

                rSelector.unMouseDown();
                rSelector.unResize();

                // Destroy elements in collections
                destroyObjectProperties(rSelector.buttons);

                // Clear input element events
                if (minInput) {
                    minInput.onfocus = minInput.onblur = minInput.onchange = null;
                }
                if (maxInput) {
                    maxInput.onfocus = maxInput.onblur = maxInput.onchange = null;
                }

                // Destroy HTML and SVG elements
                H.objectEach(rSelector, function(val, key) {
                    if (val && key !== 'chart') {
                        if (val.destroy) { // SVGElement
                            val.destroy();
                        } else if (val.nodeType) { // HTML element
                            discardElement(this[key]);
                        }
                    }
                    if (val !== RangeSelector.prototype[key]) {
                        rSelector[key] = null;
                    }
                }, this);
            }
        };

        /**
         * Add logic to normalize the zoomed range in order to preserve the pressed state of range selector buttons
         */
        Axis.prototype.toFixedRange = function(pxMin, pxMax, fixedMin, fixedMax) {
            var fixedRange = this.chart && this.chart.fixedRange,
                newMin = pick(fixedMin, this.translate(pxMin, true, !this.horiz)),
                newMax = pick(fixedMax, this.translate(pxMax, true, !this.horiz)),
                changeRatio = fixedRange && (newMax - newMin) / fixedRange;

            // If the difference between the fixed range and the actual requested range is
            // too great, the user is dragging across an ordinal gap, and we need to release
            // the range selector button.
            if (changeRatio > 0.7 && changeRatio < 1.3) {
                if (fixedMax) {
                    newMin = newMax - fixedRange;
                } else {
                    newMax = newMin + fixedRange;
                }
            }
            if (!isNumber(newMin) || !isNumber(newMax)) { // #1195, #7411
                newMin = newMax = undefined;
            }

            return {
                min: newMin,
                max: newMax
            };
        };

        /**
         * Get the axis min value based on the range option and the current max. For
         * stock charts this is extended via the {@link RangeSelector} so that if the
         * selected range is a multiple of months or years, it is compensated for
         * various month lengths.
         * 
         * @return {number} The new minimum value.
         */
        Axis.prototype.minFromRange = function() {
            var rangeOptions = this.range,
                type = rangeOptions.type,
                timeName = {
                    month: 'Month',
                    year: 'FullYear'
                }[type],
                min,
                max = this.max,
                dataMin,
                range,
                // Get the true range from a start date
                getTrueRange = function(base, count) {
                    var date = new Date(base),
                        basePeriod = date['get' + timeName]();

                    date['set' + timeName](basePeriod + count);

                    if (basePeriod === date['get' + timeName]()) {
                        date.setDate(0); // #6537
                    }

                    return date.getTime() - base;
                };

            if (isNumber(rangeOptions)) {
                min = max - rangeOptions;
                range = rangeOptions;
            } else {
                min = max + getTrueRange(max, -rangeOptions.count);

                // Let the fixedRange reflect initial settings (#5930)
                if (this.chart) {
                    this.chart.fixedRange = max - min;
                }
            }

            dataMin = pick(this.dataMin, Number.MIN_VALUE);
            if (!isNumber(min)) {
                min = dataMin;
            }
            if (min <= dataMin) {
                min = dataMin;
                if (range === undefined) { // #4501
                    range = getTrueRange(min, rangeOptions.count);
                }
                this.newMax = Math.min(min + range, this.dataMax);
            }
            if (!isNumber(max)) {
                min = undefined;
            }
            return min;

        };

        // Initialize rangeselector for stock charts
        wrap(Chart.prototype, 'init', function(proceed, options, callback) {

            addEvent(this, 'init', function() {
                if (this.options.rangeSelector.enabled) {
                    this.rangeSelector = new RangeSelector(this);
                }
            });

            proceed.call(this, options, callback);

        });

        wrap(Chart.prototype, 'render', function(proceed, options, callback) {

            var chart = this,
                axes = chart.axes,
                rangeSelector = chart.rangeSelector,
                verticalAlign;

            if (rangeSelector) {

                each(axes, function(axis) {
                    axis.updateNames();
                    axis.setScale();
                });

                chart.getAxisMargins();

                rangeSelector.render();
                verticalAlign = rangeSelector.options.verticalAlign;

                if (!rangeSelector.options.floating) {
                    if (verticalAlign === 'bottom') {
                        this.extraBottomMargin = true;
                    } else if (verticalAlign !== 'middle') {
                        this.extraTopMargin = true;
                    }
                }
            }

            proceed.call(this, options, callback);

        });

        wrap(Chart.prototype, 'update', function(proceed, options, redraw, oneToOne) {

            var chart = this,
                rangeSelector = chart.rangeSelector,
                verticalAlign;

            this.extraBottomMargin = false;
            this.extraTopMargin = false;

            if (rangeSelector) {

                rangeSelector.render();

                verticalAlign = (options.rangeSelector && options.rangeSelector.verticalAlign) ||
                    (rangeSelector.options && rangeSelector.options.verticalAlign);

                if (!rangeSelector.options.floating) {
                    if (verticalAlign === 'bottom') {
                        this.extraBottomMargin = true;
                    } else if (verticalAlign !== 'middle') {
                        this.extraTopMargin = true;
                    }
                }
            }

            proceed.call(this, H.merge(true, options, {
                chart: {
                    marginBottom: pick(options.chart && options.chart.marginBottom, chart.margin.bottom),
                    spacingBottom: pick(options.chart && options.chart.spacingBottom, chart.spacing.bottom)
                }
            }), redraw, oneToOne);

        });

        wrap(Chart.prototype, 'redraw', function(proceed, options, callback) {
            var chart = this,
                rangeSelector = chart.rangeSelector,
                verticalAlign;

            if (rangeSelector && !rangeSelector.options.floating) {

                rangeSelector.render();
                verticalAlign = rangeSelector.options.verticalAlign;

                if (verticalAlign === 'bottom') {
                    this.extraBottomMargin = true;
                } else if (verticalAlign !== 'middle') {
                    this.extraTopMargin = true;
                }
            }

            proceed.call(this, options, callback);
        });

        Chart.prototype.adjustPlotArea = function() {
            var chart = this,
                rangeSelector = chart.rangeSelector,
                rangeSelectorHeight;

            if (this.rangeSelector) {

                rangeSelectorHeight = rangeSelector.getHeight();

                if (this.extraTopMargin) {
                    this.plotTop += rangeSelectorHeight;
                }

                if (this.extraBottomMargin) {
                    this.marginBottom += rangeSelectorHeight;
                }
            }
        };

        Chart.prototype.callbacks.push(function(chart) {
            var extremes,
                rangeSelector = chart.rangeSelector,
                unbindRender,
                unbindSetExtremes;

            function renderRangeSelector() {
                extremes = chart.xAxis[0].getExtremes();
                if (isNumber(extremes.min)) {
                    rangeSelector.render(extremes.min, extremes.max);
                }
            }

            if (rangeSelector) {
                // redraw the scroller on setExtremes
                unbindSetExtremes = addEvent(
                    chart.xAxis[0],
                    'afterSetExtremes',
                    function(e) {
                        rangeSelector.render(e.min, e.max);
                    }
                );

                // redraw the scroller chart resize
                unbindRender = addEvent(chart, 'redraw', renderRangeSelector);

                // do it now
                renderRangeSelector();
            }

            // Remove resize/afterSetExtremes at chart destroy
            addEvent(chart, 'destroy', function destroyEvents() {
                if (rangeSelector) {
                    unbindRender();
                    unbindSetExtremes();
                }
            });
        });


        H.RangeSelector = RangeSelector;

        /* ****************************************************************************
         * End Range Selector code													 *
         *****************************************************************************/

    }(Highcharts));
    (function(H) {
        /**
         * (c) 2010-2017 Torstein Honsi
         *
         * License: www.highcharts.com/license
         */
        var arrayMax = H.arrayMax,
            arrayMin = H.arrayMin,
            Axis = H.Axis,
            Chart = H.Chart,
            defined = H.defined,
            each = H.each,
            extend = H.extend,
            format = H.format,
            grep = H.grep,
            inArray = H.inArray,
            isNumber = H.isNumber,
            isString = H.isString,
            map = H.map,
            merge = H.merge,
            pick = H.pick,
            Point = H.Point,
            Renderer = H.Renderer,
            Series = H.Series,
            splat = H.splat,
            SVGRenderer = H.SVGRenderer,
            VMLRenderer = H.VMLRenderer,
            wrap = H.wrap,


            seriesProto = Series.prototype,
            seriesInit = seriesProto.init,
            seriesProcessData = seriesProto.processData,
            pointTooltipFormatter = Point.prototype.tooltipFormatter;


        /**
         * Compare the values of the series against the first non-null, non-
         * zero value in the visible range. The y axis will show percentage
         * or absolute change depending on whether `compare` is set to `"percent"`
         * or `"value"`. When this is applied to multiple series, it allows
         * comparing the development of the series against each other.
         * 
         * @type {String}
         * @see [compareBase](#plotOptions.series.compareBase), [Axis.setCompare()](#Axis.
         * setCompare())
         * @sample {highstock} stock/plotoptions/series-compare-percent/ Percent
         * @sample {highstock} stock/plotoptions/series-compare-value/ Value
         * @default undefined
         * @since 1.0.1
         * @product highstock
         * @apioption plotOptions.series.compare
         */

        /**
         * Defines if comparisson should start from the first point within the visible
         * range or should start from the first point <b>before</b> the range.
         * In other words, this flag determines if first point within the visible range
         * will have 0% (`compareStart=true`) or should have been already calculated
         * according to the previous point (`compareStart=false`).
         *
         * @type {Boolean}
         * @sample {highstock} stock/plotoptions/series-comparestart/ Calculate compare within visible range
         * @default false
         * @since 6.0.0
         * @product highstock
         * @apioption plotOptions.series.compareStart
         */

        /**
         * When [compare](#plotOptions.series.compare) is `percent`, this option
         * dictates whether to use 0 or 100 as the base of comparison.
         * 
         * @validvalue [0, 100]
         * @type {Number}
         * @sample {highstock} / Compare base is 100
         * @default 0
         * @since 5.0.6
         * @product highstock
         * @apioption plotOptions.series.compareBase
         */

        /**
         * Factory function for creating new stock charts. Creates a new {@link Chart|
         * Chart} object with different default options than the basic Chart.
         * 
         * @function #stockChart
         * @memberOf Highcharts
         *
         * @param  {String|HTMLDOMElement} renderTo
         *         The DOM element to render to, or its id.
         * @param  {Options} options
         *         The chart options structure as described in the {@link
         *         https://api.highcharts.com/highstock|options reference}.
         * @param  {Function} callback
         *         A function to execute when the chart object is finished loading and
         *         rendering. In most cases the chart is built in one thread, but in
         *         Internet Explorer version 8 or less the chart is sometimes initialized
         *         before the document is ready, and in these cases the chart object
         *         will not be finished synchronously. As a consequence, code that
         *         relies on the newly built Chart object should always run in the
         *         callback. Defining a {@link https://api.highcharts.com/highstock/chart.events.load|
         *         chart.event.load} handler is equivalent.
         *
         * @return {Chart}
         *         The chart object.
         *
         * @example
         * var chart = Highcharts.stockChart('container', {
         *     series: [{
         *         data: [1, 2, 3, 4, 5, 6, 7, 8, 9],
         *         pointInterval: 24 * 60 * 60 * 1000
         *     }]
         * });
         */
        H.StockChart = H.stockChart = function(a, b, c) {
            var hasRenderToArg = isString(a) || a.nodeName,
                options = arguments[hasRenderToArg ? 1 : 0],
                seriesOptions = options.series, // to increase performance, don't merge the data
                defaultOptions = H.getOptions(),
                opposite,

                // Always disable startOnTick:true on the main axis when the navigator
                // is enabled (#1090)
                navigatorEnabled = pick(
                    options.navigator && options.navigator.enabled,
                    defaultOptions.navigator.enabled,
                    true
                ),
                disableStartOnTick = navigatorEnabled ? {
                    startOnTick: false,
                    endOnTick: false
                } : null,

                lineOptions = {

                    marker: {
                        enabled: false,
                        radius: 2
                    }
                    // gapSize: 0
                },
                columnOptions = {
                    shadow: false,
                    borderWidth: 0
                };

            // apply X axis options to both single and multi y axes
            options.xAxis = map(splat(options.xAxis || {}), function(xAxisOptions) {
                return merge({ // defaults
                        minPadding: 0,
                        maxPadding: 0,
                        overscroll: 0,
                        ordinal: true,
                        title: {
                            text: null
                        },
                        labels: {
                            overflow: 'justify'
                        },
                        showLastLabel: true
                    },
                    defaultOptions.xAxis, // #3802
                    xAxisOptions, // user options
                    { // forced options
                        type: 'datetime',
                        categories: null
                    },
                    disableStartOnTick
                );
            });

            // apply Y axis options to both single and multi y axes
            options.yAxis = map(splat(options.yAxis || {}), function(yAxisOptions) {
                opposite = pick(yAxisOptions.opposite, true);
                return merge({ // defaults
                        labels: {
                            y: -2
                        },
                        opposite: opposite,

                        /**
                         * @default {highcharts} true
                         * @default {highstock} false
                         * @apioption yAxis.showLastLabel
                         */
                        showLastLabel: false,

                        title: {
                            text: null
                        }
                    },
                    defaultOptions.yAxis, // #3802
                    yAxisOptions // user options
                );
            });

            options.series = null;

            options = merge({
                    chart: {
                        panning: true,
                        pinchType: 'x'
                    },
                    navigator: {
                        enabled: navigatorEnabled
                    },
                    scrollbar: {
                        // #4988 - check if setOptions was called
                        enabled: pick(defaultOptions.scrollbar.enabled, true)
                    },
                    rangeSelector: {
                        // #4988 - check if setOptions was called
                        enabled: pick(defaultOptions.rangeSelector.enabled, true)
                    },
                    title: {
                        text: null
                    },
                    tooltip: {
                        split: pick(defaultOptions.tooltip.split, true),
                        crosshairs: true
                    },
                    legend: {
                        enabled: false
                    },

                    plotOptions: {
                        line: lineOptions,
                        spline: lineOptions,
                        area: lineOptions,
                        areaspline: lineOptions,
                        arearange: lineOptions,
                        areasplinerange: lineOptions,
                        column: columnOptions,
                        columnrange: columnOptions,
                        candlestick: columnOptions,
                        ohlc: columnOptions
                    }

                },

                options, // user's options

                { // forced options
                    isStock: true // internal flag
                }
            );

            options.series = seriesOptions;

            return hasRenderToArg ?
                new Chart(a, options, c) :
                new Chart(options, b);
        };

        // Override the automatic label alignment so that the first Y axis' labels
        // are drawn on top of the grid line, and subsequent axes are drawn outside
        wrap(Axis.prototype, 'autoLabelAlign', function(proceed) {
            var chart = this.chart,
                options = this.options,
                panes = chart._labelPanes = chart._labelPanes || {},
                key,
                labelOptions = this.options.labels;
            if (this.chart.options.isStock && this.coll === 'yAxis') {
                key = options.top + ',' + options.height;
                if (!panes[key] && labelOptions.enabled) { // do it only for the first Y axis of each pane
                    if (labelOptions.x === 15) { // default
                        labelOptions.x = 0;
                    }
                    if (labelOptions.align === undefined) {
                        labelOptions.align = 'right';
                    }
                    panes[key] = this;
                    return 'right';
                }
            }
            return proceed.apply(this, [].slice.call(arguments, 1));
        });

        // Clear axis from label panes (#6071)
        wrap(Axis.prototype, 'destroy', function(proceed) {
            var chart = this.chart,
                key = this.options && (this.options.top + ',' + this.options.height);

            if (key && chart._labelPanes && chart._labelPanes[key] === this) {
                delete chart._labelPanes[key];
            }

            return proceed.apply(this, Array.prototype.slice.call(arguments, 1));
        });

        // Override getPlotLinePath to allow for multipane charts
        wrap(Axis.prototype, 'getPlotLinePath', function(proceed, value, lineWidth, old, force, translatedValue) {
            var axis = this,
                series = (this.isLinked && !this.series ? this.linkedParent.series : this.series),
                chart = axis.chart,
                renderer = chart.renderer,
                axisLeft = axis.left,
                axisTop = axis.top,
                x1,
                y1,
                x2,
                y2,
                result = [],
                axes = [], // #3416 need a default array
                axes2,
                uniqueAxes,
                transVal;

            /**
             * Return the other axis based on either the axis option or on related series.
             */
            function getAxis(coll) {
                var otherColl = coll === 'xAxis' ? 'yAxis' : 'xAxis',
                    opt = axis.options[otherColl];

                // Other axis indexed by number
                if (isNumber(opt)) {
                    return [chart[otherColl][opt]];
                }

                // Other axis indexed by id (like navigator)
                if (isString(opt)) {
                    return [chart.get(opt)];
                }

                // Auto detect based on existing series
                return map(series, function(s) {
                    return s[otherColl];
                });
            }

            // Ignore in case of colorAxis or zAxis. #3360, #3524, #6720
            if (axis.coll !== 'xAxis' && axis.coll !== 'yAxis') {
                return proceed.apply(this, [].slice.call(arguments, 1));
            }

            // Get the related axes based on series
            axes = getAxis(axis.coll);

            // Get the related axes based options.*Axis setting #2810
            axes2 = (axis.isXAxis ? chart.yAxis : chart.xAxis);
            each(axes2, function(A) {
                if (defined(A.options.id) ? A.options.id.indexOf('navigator') === -1 : true) {
                    var a = (A.isXAxis ? 'yAxis' : 'xAxis'),
                        rax = (defined(A.options[a]) ? chart[a][A.options[a]] : chart[a][0]);

                    if (axis === rax) {
                        axes.push(A);
                    }
                }
            });


            // Remove duplicates in the axes array. If there are no axes in the axes array,
            // we are adding an axis without data, so we need to populate this with grid
            // lines (#2796).
            uniqueAxes = axes.length ? [] : [axis.isXAxis ? chart.yAxis[0] : chart.xAxis[0]]; // #3742
            each(axes, function(axis2) {
                if (
                    inArray(axis2, uniqueAxes) === -1 &&
                    // Do not draw on axis which overlap completely. #5424
                    !H.find(uniqueAxes, function(unique) {
                        return unique.pos === axis2.pos && unique.len && axis2.len;
                    })
                ) {
                    uniqueAxes.push(axis2);
                }
            });

            transVal = pick(translatedValue, axis.translate(value, null, null, old));
            if (isNumber(transVal)) {
                if (axis.horiz) {
                    each(uniqueAxes, function(axis2) {
                        var skip;

                        y1 = axis2.pos;
                        y2 = y1 + axis2.len;
                        x1 = x2 = Math.round(transVal + axis.transB);

                        if (x1 < axisLeft || x1 > axisLeft + axis.width) { // outside plot area
                            if (force) {
                                x1 = x2 = Math.min(Math.max(axisLeft, x1), axisLeft + axis.width);
                            } else {
                                skip = true;
                            }
                        }
                        if (!skip) {
                            result.push('M', x1, y1, 'L', x2, y2);
                        }
                    });
                } else {
                    each(uniqueAxes, function(axis2) {
                        var skip;

                        x1 = axis2.pos;
                        x2 = x1 + axis2.len;
                        y1 = y2 = Math.round(axisTop + axis.height - transVal);

                        if (y1 < axisTop || y1 > axisTop + axis.height) { // outside plot area
                            if (force) {
                                y1 = y2 = Math.min(Math.max(axisTop, y1), axis.top + axis.height);
                            } else {
                                skip = true;
                            }
                        }
                        if (!skip) {
                            result.push('M', x1, y1, 'L', x2, y2);
                        }
                    });
                }
            }
            return result.length > 0 ?
                renderer.crispPolyLine(result, lineWidth || 1) :
                null; // #3557 getPlotLinePath in regular Highcharts also returns null
        });

        // Function to crisp a line with multiple segments
        SVGRenderer.prototype.crispPolyLine = function(points, width) {
            // points format: ['M', 0, 0, 'L', 100, 0]		
            // normalize to a crisp line
            var i;
            for (i = 0; i < points.length; i = i + 6) {
                if (points[i + 1] === points[i + 4]) {
                    // Substract due to #1129. Now bottom and left axis gridlines behave the same.
                    points[i + 1] = points[i + 4] = Math.round(points[i + 1]) - (width % 2 / 2);
                }
                if (points[i + 2] === points[i + 5]) {
                    points[i + 2] = points[i + 5] = Math.round(points[i + 2]) + (width % 2 / 2);
                }
            }
            return points;
        };

        if (Renderer === VMLRenderer) {
            VMLRenderer.prototype.crispPolyLine = SVGRenderer.prototype.crispPolyLine;
        }


        // Wrapper to hide the label
        wrap(Axis.prototype, 'hideCrosshair', function(proceed, i) {

            proceed.call(this, i);

            if (this.crossLabel) {
                this.crossLabel = this.crossLabel.hide();
            }
        });

        // Wrapper to draw the label
        wrap(Axis.prototype, 'drawCrosshair', function(proceed, e, point) {

            // Draw the crosshair
            proceed.call(this, e, point);

            // Check if the label has to be drawn
            if (!defined(this.crosshair.label) || !this.crosshair.label.enabled || !this.cross) {
                return;
            }

            var chart = this.chart,
                options = this.options.crosshair.label, // the label's options
                horiz = this.horiz, // axis orientation
                opposite = this.opposite, // axis position
                left = this.left, // left position
                top = this.top, // top position
                crossLabel = this.crossLabel, // reference to the svgElement
                posx,
                posy,
                crossBox,
                formatOption = options.format,
                formatFormat = '',
                limit,
                align,
                tickInside = this.options.tickPosition === 'inside',
                snap = this.crosshair.snap !== false,
                value,
                offset = 0;

            // Use last available event (#5287)
            if (!e) {
                e = this.cross && this.cross.e;
            }

            align = (horiz ? 'center' : opposite ?
                (this.labelAlign === 'right' ? 'right' : 'left') :
                (this.labelAlign === 'left' ? 'left' : 'center'));

            // If the label does not exist yet, create it.
            if (!crossLabel) {
                crossLabel = this.crossLabel = chart.renderer.label(null, null, null, options.shape || 'callout')
                    .addClass('highcharts-crosshair-label' +
                        (this.series[0] && ' highcharts-color-' + this.series[0].colorIndex))
                    .attr({
                        align: options.align || align,
                        padding: pick(options.padding, 8),
                        r: pick(options.borderRadius, 3),
                        zIndex: 2
                    })
                    .add(this.labelGroup);


                // Presentational
                crossLabel
                    .attr({
                        fill: options.backgroundColor ||
                            (this.series[0] && this.series[0].color) || '#666666',
                        stroke: options.borderColor || '',
                        'stroke-width': options.borderWidth || 0
                    })
                    .css(extend({
                        color: '#ffffff',
                        fontWeight: 'normal',
                        fontSize: '11px',
                        textAlign: 'center'
                    }, options.style));

            }

            if (horiz) {
                posx = snap ? point.plotX + left : e.chartX;
                posy = top + (opposite ? 0 : this.height);
            } else {
                posx = opposite ? this.width + left : 0;
                posy = snap ? point.plotY + top : e.chartY;
            }

            if (!formatOption && !options.formatter) {
                if (this.isDatetimeAxis) {
                    formatFormat = '%b %d, %Y';
                }
                formatOption = '{value' + (formatFormat ? ':' + formatFormat : '') + '}';
            }

            // Show the label
            value = snap ? point[this.isXAxis ? 'x' : 'y'] : this.toValue(horiz ? e.chartX : e.chartY);
            crossLabel.attr({
                text: formatOption ? format(formatOption, {
                    value: value
                }) : options.formatter.call(this, value),
                x: posx,
                y: posy,
                visibility: 'visible'
            });

            crossBox = crossLabel.getBBox();

            // now it is placed we can correct its position
            if (horiz) {
                if ((tickInside && !opposite) || (!tickInside && opposite)) {
                    posy = crossLabel.y - crossBox.height;
                }
            } else {
                posy = crossLabel.y - (crossBox.height / 2);
            }

            // check the edges
            if (horiz) {
                limit = {
                    left: left - crossBox.x,
                    right: left + this.width - crossBox.x
                };
            } else {
                limit = {
                    left: this.labelAlign === 'left' ? left : 0,
                    right: this.labelAlign === 'right' ? left + this.width : chart.chartWidth
                };
            }

            // left edge
            if (crossLabel.translateX < limit.left) {
                offset = limit.left - crossLabel.translateX;
            }
            // right edge
            if (crossLabel.translateX + crossBox.width >= limit.right) {
                offset = -(crossLabel.translateX + crossBox.width - limit.right);
            }

            // show the crosslabel
            crossLabel.attr({
                x: posx + offset,
                y: posy,
                // First set x and y, then anchorX and anchorY, when box is actually calculated, #5702
                anchorX: horiz ? posx : (this.opposite ? 0 : chart.chartWidth),
                anchorY: horiz ? (this.opposite ? chart.chartHeight : 0) : posy + crossBox.height / 2
            });
        });

        /* ****************************************************************************
         * Start value compare logic												  *
         *****************************************************************************/

        /**
         * Extend series.init by adding a method to modify the y value used for plotting
         * on the y axis. This method is called both from the axis when finding dataMin
         * and dataMax, and from the series.translate method.
         */
        seriesProto.init = function() {

            // Call base method
            seriesInit.apply(this, arguments);

            // Set comparison mode
            this.setCompare(this.options.compare);
        };

        /**
         * Highstock only. Set the {@link
         * http://api.highcharts.com/highstock/plotOptions.series.compare|
         * compare} mode of the series after render time. In most cases it is more
         * useful running {@link Axis#setCompare} on the X axis to update all its
         * series.
         *
         * @function setCompare
         * @memberOf Series.prototype
         *
         * @param  {String} compare
         *         Can be one of `null`, `"percent"` or `"value"`.
         */
        seriesProto.setCompare = function(compare) {

            // Set or unset the modifyValue method
            this.modifyValue = (compare === 'value' || compare === 'percent') ? function(value, point) {
                var compareValue = this.compareValue;

                if (value !== undefined && compareValue !== undefined) { // #2601, #5814

                    // Get the modified value
                    if (compare === 'value') {
                        value -= compareValue;

                        // Compare percent
                    } else {
                        value = 100 * (value / compareValue) -
                            (this.options.compareBase === 100 ? 0 : 100);
                    }

                    // record for tooltip etc.
                    if (point) {
                        point.change = value;
                    }

                    return value;
                }
            } : null;

            // Survive to export, #5485
            this.userOptions.compare = compare;

            // Mark dirty
            if (this.chart.hasRendered) {
                this.isDirty = true;
            }

        };

        /**
         * Extend series.processData by finding the first y value in the plot area,
         * used for comparing the following values
         */
        seriesProto.processData = function() {
            var series = this,
                i,
                keyIndex = -1,
                processedXData,
                processedYData,
                compareStart = series.options.compareStart === true ? 0 : 1,
                length,
                compareValue;

            // call base method
            seriesProcessData.apply(this, arguments);

            if (series.xAxis && series.processedYData) { // not pies

                // local variables
                processedXData = series.processedXData;
                processedYData = series.processedYData;
                length = processedYData.length;

                // For series with more than one value (range, OHLC etc), compare against
                // close or the pointValKey (#4922, #3112)
                if (series.pointArrayMap) {
                    // Use close if present (#3112)
                    keyIndex = inArray('close', series.pointArrayMap);
                    if (keyIndex === -1) {
                        keyIndex = inArray(series.pointValKey || 'y', series.pointArrayMap);
                    }
                }

                // find the first value for comparison
                for (i = 0; i < length - compareStart; i++) {
                    compareValue = processedYData[i] && keyIndex > -1 ?
                        processedYData[i][keyIndex] :
                        processedYData[i];
                    if (
                        isNumber(compareValue) &&
                        processedXData[i + compareStart] >= series.xAxis.min &&
                        compareValue !== 0
                    ) {
                        series.compareValue = compareValue;
                        break;
                    }
                }
            }
        };

        /**
         * Modify series extremes
         */
        wrap(seriesProto, 'getExtremes', function(proceed) {
            var extremes;

            proceed.apply(this, [].slice.call(arguments, 1));

            if (this.modifyValue) {
                extremes = [this.modifyValue(this.dataMin), this.modifyValue(this.dataMax)];
                this.dataMin = arrayMin(extremes);
                this.dataMax = arrayMax(extremes);
            }
        });

        /**
         * Highstock only. Set the compare mode on all series belonging to an Y axis
         * after render time.
         *
         * @param  {String} compare
         *         The compare mode. Can be one of `null`, `"value"` or `"percent"`.
         * @param  {Boolean} [redraw=true]
         *         Whether to redraw the chart or to wait for a later call to {@link
         *         Chart#redraw},
         *
         * @function setCompare
         * @memberOf Axis.prototype
         *
         * @see    {@link https://api.highcharts.com/highstock/series.plotOptions.compare|
         *         series.plotOptions.compare}
         *
         * @sample stock/members/axis-setcompare/
         *         Set compoare
         */
        Axis.prototype.setCompare = function(compare, redraw) {
            if (!this.isXAxis) {
                each(this.series, function(series) {
                    series.setCompare(compare);
                });
                if (pick(redraw, true)) {
                    this.chart.redraw();
                }
            }
        };

        /**
         * Extend the tooltip formatter by adding support for the point.change variable
         * as well as the changeDecimals option
         */
        Point.prototype.tooltipFormatter = function(pointFormat) {
            var point = this;

            pointFormat = pointFormat.replace(
                '{point.change}',
                (point.change > 0 ? '+' : '') +
                H.numberFormat(point.change, pick(point.series.tooltipOptions.changeDecimals, 2))
            );

            return pointTooltipFormatter.apply(this, [pointFormat]);
        };

        /* ****************************************************************************
         * End value compare logic													*
         *****************************************************************************/


        /**
         * Extend the Series prototype to create a separate series clip box. This is
         * related to using multiple panes, and a future pane logic should incorporate
         * this feature (#2754).
         */
        wrap(Series.prototype, 'render', function(proceed) {
            // Only do this on not 3d (#2939, #5904) nor polar (#6057) charts, and only
            // if the series type handles clipping in the animate method (#2975).
            if (!(this.chart.is3d && this.chart.is3d()) &&
                !this.chart.polar &&
                this.xAxis &&
                !this.xAxis.isRadial // Gauge, #6192
            ) {

                // First render, initial clip box
                if (!this.clipBox && this.animate) {
                    this.clipBox = merge(this.chart.clipBox);
                    this.clipBox.width = this.xAxis.len;
                    this.clipBox.height = this.yAxis.len;

                    // On redrawing, resizing etc, update the clip rectangle
                } else if (this.chart[this.sharedClipKey]) {
                    this.chart[this.sharedClipKey].attr({
                        width: this.xAxis.len,
                        height: this.yAxis.len
                    });
                    // #3111
                } else if (this.clipBox) {
                    this.clipBox.width = this.xAxis.len;
                    this.clipBox.height = this.yAxis.len;
                }
            }
            proceed.call(this);
        });

        wrap(Chart.prototype, 'getSelectedPoints', function(proceed) {
            var points = proceed.call(this);

            each(this.series, function(serie) {
                // series.points - for grouped points (#6445)
                if (serie.hasGroupedData) {
                    points = points.concat(grep(serie.points || [], function(point) {
                        return point.selected;
                    }));
                }
            });
            return points;
        });

        wrap(Chart.prototype, 'update', function(proceed, options) {
            // Use case: enabling scrollbar from a disabled state.
            // Scrollbar needs to be initialized from a controller, Navigator in this
            // case (#6615)
            if ('scrollbar' in options && this.navigator) {
                merge(true, this.options.scrollbar, options.scrollbar);
                this.navigator.update({}, false);
                delete options.scrollbar;
            }

            return proceed.apply(this, Array.prototype.slice.call(arguments, 1));
        });

    }(Highcharts));
}));


/***/ })

});
//# sourceMappingURL=exchange.module.chunk.js.map