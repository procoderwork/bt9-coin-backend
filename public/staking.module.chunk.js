webpackJsonp(["staking.module"],{

/***/ "../../../../../src/app/pages/staking/staking.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\" *ngIf=\"settings.getSysSetting('enable_staking') == 0\">\n    <div class=\"container-fluid\">\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n                <div class=\"alert alert-info\">\n                    Staking is disabled.\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n\n<div class=\"main-content\" *ngIf=\"settings.getSysSetting('enable_staking') == 1\">\n    <div class=\"container-fluid\">\n        <div class=\"row\">\n            <div class=\"col-md-3\">\n                <div class=\"card card-stats\">\n                    <div class=\"card-content\">\n                        <p class=\"category text-left\">Total Staking BT9</p>\n                        <h3 class=\"card-title\">{{stakingBalance.total_amount | number:'1.0-8'}}</h3>\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-md-3\">\n                <div class=\"card card-stats\">\n                    <div class=\"card-content\">\n                        <p class=\"category text-left\">Active BT9</p>\n                        <h3 class=\"card-title\">{{stakingBalance.locked_amount | number:'1.0-8'}}</h3>\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-md-3\">\n                <div class=\"card card-stats\">\n                    <div class=\"card-content\">\n                        <p class=\"category text-left\">Release BT9</p>\n                        <h3 class=\"card-title\">{{stakingBalance.unlocked_amount | number:'1.0-8'}}</h3>\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-md-3\">\n                <div class=\"card card-stats\">\n                    <div class=\"card-content\">\n                        <p class=\"category text-left\">Earned BT9</p>\n                        <h3 class=\"card-title\">{{stakingBalance.earned_amount | number:'1.0-8'}}</h3>\n                    </div>\n                </div>\n            </div>\n        </div>\n        <div class=\"row\">\n            <div class=\"col-md-6\">\n                <div class=\"card\">\n                    <div class=\"card-header\">\n                        <h4 class=\"card-title\">Staking</h4>\n                    </div>\n                    <div class=\"card-content\">\n                        <div class=\"row\">\n                            <div class=\"col-md-12\">\n                                <div class=\"card-content\" style=\"margin: 5%;\">\n                                    <div class=\"text-center\">\n                                        <h4>Enter Amount BT9</h4>\n                                        <form action=\"#\" method=\"#\" novalidate=\"\"\n                                              class=\"ng-untouched ng-pristine ng-valid\">\n                                            <div class=\"form-group label-floating\">\n                                                <label class=\"control-label label-amount\">Amount BT9</label>\n                                                <input class=\"form-control input-amount ng-untouched ng-pristine ng-valid\"\n                                                       [(ngModel)]=\"stakingAmount\" name=\"amount\" required=\"\" type=\"text\">\n                                                <div class=\"text-left\">\n                                                <small class=\"text-black\">Minimum Amount: {{settings.getSysSetting('staking_minimum_amount')}}</small>\n                                                </div>\n                                                <div class=\"all-btn\">\n                                                    <button class=\"btn btn-info btn-xs\" (click)=\"allBT9()\">ALL BT9\n                                                    </button>\n                                                </div>\n                                            </div>\n                                            <div style=\"margin-top: 7px;\">\n                                                <p>\n                                                    <small>Your Staking will be locked for {{settings.getSysSetting('staking_locked_days')}} days.</small>\n                                                </p>\n                                                <p>\n                                                    <small>You will receive {{settings.getSysSetting('staking_interest_month') / 30 | number:'1.0-2'}} every day</small>\n                                                </p>\n                                            </div>\n                                            <button class=\"btn btn-fill btn-info\" type=\"button\"\n                                                    (click)=\"buyStaking()\">Buy with BT9 Wallet\n                                            </button>\n                                        </form>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-md-6\">\n                <div class=\"card\">\n                    <div class=\"card-content\">\n                        <div class=\"row\">\n                            <div class=\"col-md-12\">\n                                <div class=\"bs-component\">\n                                    <div class=\"panel-group accordion\">\n                                        <div class=\"panel mbn\" style=\"border: 1px solid #c2c8d8;\">\n                                            <div class=\"panel-heading\" style=\"height: auto\">\n                                                <p style=\"border-radius: 0px;background-color:transparent;color:black\">\n                                                    Your Staking\n                                                </p>\n                                            </div>\n                                            <div class=\"panel-collapse collapse in \"\n                                                 style=\"background-color: white\">\n                                                <div class=\"table-responsive\">\n                                                    <table cellspacing=\"0\"\n                                                           class=\"table table-striped table-hover table-condensed text-black \"\n                                                           width=\"100%\">\n                                                        <thead>\n                                                        <tr>\n                                                            <th>#</th>\n                                                            <th>Amount</th>\n                                                            <th>Date</th>\n                                                            <th>Finish Date</th>\n                                                        </tr>\n                                                        </thead>\n                                                        <tbody>\n                                                        <tr *ngFor=\"let staking of stakings; let i=index\">\n                                                            <td>{{i+1}}</td>\n                                                            <td>{{staking.src_amount | number:'1.0-8'}}</td>\n                                                            <td>{{staking.date | date:'yyyy-MM-dd'}}</td>\n                                                            <td>{{staking.finish_date | date:'yyyy-MM-dd'}}</td>\n                                                        </tr>\n                                                        <!---->\n                                                        <tr *ngIf=\"stakings?.length == 0\">\n                                                            <td class=\"text-center\" colspan=\"4\">\n                                                                No Staking found.\n                                                            </td>\n                                                        </tr>\n                                                        <!---->\n                                                        </tbody>\n                                                    </table>\n                                                </div>\n                                            </div>\n                                        </div>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/pages/staking/staking.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".all-btn {\n  position: absolute;\n  right: 0;\n  top: 0px; }\n\n.panel-group .panel-heading {\n  position: relative;\n  display: block;\n  width: 100%; }\n\n.panel-heading {\n  height: 52px;\n  line-height: 49px;\n  letter-spacing: .2px;\n  color: #a9acbd;\n  font-size: 15px;\n  font-weight: 400;\n  padding: 0 13px;\n  background: #fafafa;\n  border: 1px solid #e5eaee;\n  border-bottom-left-radius: 4px;\n  border-top-left-radius: 4px; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/pages/staking/staking.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StakingComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_notifications_service__ = __webpack_require__("../../../../../src/app/services/notifications.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_settings_settings_service__ = __webpack_require__("../../../../../src/app/services/settings/settings.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_api_service__ = __webpack_require__("../../../../../src/app/services/api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_balance_service__ = __webpack_require__("../../../../../src/app/services/balance.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var StakingComponent = (function () {
    function StakingComponent(settings, api, balance, notify) {
        this.settings = settings;
        this.api = api;
        this.balance = balance;
        this.notify = notify;
        this.stakingAmount = 0;
        this.stakings = [];
        this.stakingBalance = {};
        this.dailyBalance = {};
    }
    StakingComponent.prototype.ngOnInit = function () {
        this.loadStakings();
        this.loadStakingBalance();
    };
    StakingComponent.prototype.loadStakings = function () {
        var _this = this;
        this.api.getStaking({}).subscribe(function (res) {
            if (res.success) {
                _this.stakings = res.data;
            }
        }, function (err) {
        });
    };
    StakingComponent.prototype.loadStakingBalance = function () {
        var _this = this;
        this.api.getStakingBalance({}).subscribe(function (res) {
            if (res.success) {
                _this.stakingBalance.total_amount = res.total_amount;
                _this.stakingBalance.locked_amount = res.locked_amount;
                _this.stakingBalance.unlocked_amount = res.total_amount - res.locked_amount;
                _this.stakingBalance.earned_amount = res.earned_amount;
            }
        }, function (err) {
        });
    };
    StakingComponent.prototype.buyStaking = function () {
        var _this = this;
        if (Number(this.stakingAmount) < this.settings.getSysSetting('staking_minimum_amount')) {
            swal({
                type: 'warning',
                title: 'Warning',
                text: 'Minimum Staking Amount is ' + this.settings.getSysSetting('staking_minimum_amount'),
                buttonsStyling: false,
                confirmButtonClass: 'btn btn-success'
            });
            return;
        }
        this.settings.loading = true;
        this.api.saveStaking({
            amount: this.stakingAmount,
            after: this.settings.getSysSetting('staking_locked_days'),
            daily: this.settings.getSysSetting('staking_interest_month') / 30
        }).subscribe(function (res) {
            _this.settings.loading = false;
            if (res.success) {
                swal({
                    type: 'success',
                    title: 'Success',
                    text: 'Successfully',
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-success'
                });
                _this.balance.getTokenBalance();
                _this.loadStakings();
                _this.loadStakingBalance();
            }
            else {
                swal({
                    type: 'warning',
                    title: 'Failed',
                    text: res.error,
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-success'
                });
            }
        }, function (err) {
            _this.settings.loading = false;
            swal({
                type: 'warning',
                title: 'Failed',
                text: err._body,
                buttonsStyling: false,
                confirmButtonClass: 'btn btn-success'
            });
        });
    };
    StakingComponent.prototype.allBT9 = function () {
        this.stakingAmount = this.settings.getUserSetting('token_balance');
    };
    StakingComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-staking',
            template: __webpack_require__("../../../../../src/app/pages/staking/staking.component.html"),
            styles: [__webpack_require__("../../../../../src/app/pages/staking/staking.component.scss")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__services_settings_settings_service__["a" /* SettingsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_settings_settings_service__["a" /* SettingsService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__services_api_service__["a" /* Api */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services_api_service__["a" /* Api */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_4__services_balance_service__["a" /* BalanceService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__services_balance_service__["a" /* BalanceService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_1__services_notifications_service__["a" /* Notifications */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_notifications_service__["a" /* Notifications */]) === "function" && _d || Object])
    ], StakingComponent);
    return StakingComponent;
    var _a, _b, _c, _d;
}());

//# sourceMappingURL=staking.component.js.map

/***/ }),

/***/ "../../../../../src/app/pages/staking/staking.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StakingModule", function() { return StakingModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__md_md_module__ = __webpack_require__("../../../../../src/app/md/md.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__staking_component__ = __webpack_require__("../../../../../src/app/pages/staking/staking.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__staking_routing__ = __webpack_require__("../../../../../src/app/pages/staking/staking.routing.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








var StakingModule = (function () {
    function StakingModule() {
    }
    StakingModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_2__angular_common__["b" /* CommonModule */],
                __WEBPACK_IMPORTED_MODULE_1__angular_router__["d" /* RouterModule */].forChild(__WEBPACK_IMPORTED_MODULE_7__staking_routing__["a" /* StakingRoutes */]),
                __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormsModule"],
                __WEBPACK_IMPORTED_MODULE_4__md_md_module__["a" /* MdModule */],
                __WEBPACK_IMPORTED_MODULE_5__app_module__["b" /* MaterialModule */]
            ],
            declarations: [__WEBPACK_IMPORTED_MODULE_6__staking_component__["a" /* StakingComponent */]]
        })
    ], StakingModule);
    return StakingModule;
}());

//# sourceMappingURL=staking.module.js.map

/***/ }),

/***/ "../../../../../src/app/pages/staking/staking.routing.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StakingRoutes; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__staking_component__ = __webpack_require__("../../../../../src/app/pages/staking/staking.component.ts");

var StakingRoutes = [
    {
        path: '',
        component: __WEBPACK_IMPORTED_MODULE_0__staking_component__["a" /* StakingComponent */]
    }
];
//# sourceMappingURL=staking.routing.js.map

/***/ })

});
//# sourceMappingURL=staking.module.chunk.js.map