<?php

namespace App\Entities;

class UserType {
    const USER = 0;
    const ADMIN = 1;
}
