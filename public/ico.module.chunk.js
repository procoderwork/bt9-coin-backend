webpackJsonp(["ico.module"],{

/***/ "../../../../../src/app/pages/ico/ico.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\" *ngIf=\"settings.getSysSetting('enable_ico') == 0\">\n    <div class=\"container-fluid\">\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n                <div class=\"alert alert-info\">\n                    ICO is disabled.\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n\n<div class=\"main-content\" *ngIf=\"settings.getSysSetting('enable_ico') == 1\">\n    <div class=\"container-fluid\">\n        <div class=\"row\">\n            <div *ngFor=\"let agenda of agendas\" class=\"col-md-2\">\n                <div class=\"card\">\n                    <div class=\"card-content ico\">\n                        <div class=\"complete-label\" *ngIf=\"agenda.status == 1\">\n                            <label class=\"label-static\"><small>COMPLETED</small></label>\n                        </div>\n                        <div class=\"complete-image\" *ngIf=\"agenda.status == 1\"></div>\n                        <div class=\"date\">\n                            <b>{{agenda.from | date:'dd'}}</b><small>th</small> - <b>{{agenda.to | date:'dd'}}</b><small>th</small><br>\n                            {{agenda.from | date:'MMMM, yyyy'}}\n                        </div>\n                        <hr>\n                        <div>\n                            <b>{{agenda.price | number:'1.2-2'}}</b><br>\n                            USD per BT9\n                        </div>\n                        <hr>\n                        <div>\n                            <b>{{agenda.total | number:'1.0-2'}}</b><br>\n                            BT9 per day\n                        </div>\n                        <hr>\n                        <div>\n                            <small>MAX</small><b> {{agenda.per_account | number:'1.0-2'}}</b><br>\n                            BT9 per acc per day\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n                <div class=\"card\">\n                    <div class=\"card-header\">\n                        <h4 class=\"card-title\">Buy BT9Coin</h4>\n                    </div>\n                    <div class=\"card-content\">\n                        <div class=\"row\">\n                            <div class=\"col-md-7\">\n                                <div class=\"card-content\" style=\"margin: 5%;\">\n                                    <div class=\"text-center\">\n                                        <h4>Enter Amount BT9Coin</h4>\n                                        <form action=\"#\" method=\"#\" novalidate=\"\"\n                                              class=\"ng-untouched ng-pristine ng-valid\">\n\n                                            <div class=\"form-group label-floating\">\n                                                <label class=\"control-label label-amount\">Amount BT9Coin</label>\n                                                <input class=\"form-control input-amount ng-untouched ng-pristine ng-valid\"\n                                                       [(ngModel)]=\"tokenAmount\" name=\"amount\" required=\"\" type=\"text\"\n                                                       value=\"0\">\n                                                <div class=\"text-left\">\n                                                    <small class=\"text-black\">Minimum Amount BT9Coin: 5</small>\n                                                </div>\n\n                                                <div class=\"all-btn\">\n                                                    <button class=\"btn btn-info btn-xs\" (click)=\"allBTC()\">ALL BTC\n                                                    </button>\n                                                    <button class=\"btn btn-primary btn-xs\" (click)=\"allETH()\">ALL ETH\n                                                    </button>\n                                                </div>\n                                                <!---->\n                                                <div style=\"padding-top: 10px;\">\n                                                    <p><b> = {{getBTCAmount() | number:'1.8-8'}} BTC</b></p>\n                                                </div>\n                                                <div style=\"padding-top: 10px;\">\n                                                    <p><b> = {{getETHAmount() | number:'1.8-8'}} ETH</b></p>\n                                                </div>\n                                                <hr>\n                                                <!--<div style=\"margin-bottom: 7px;\">-->\n                                                    <!--<p>-->\n                                                        <!--<small>bonus<b> {{tokenAmount * curBonus / 100 | number:'1.0-8'}} BT9Coin</b></small>-->\n                                                    <!--</p>-->\n                                                    <!--<p>-->\n                                                        <!--<small>total receive<b> {{tokenAmount * (100 + curBonus) / 100 | number:'1.0-8'}} BT9Coin</b></small>-->\n                                                    <!--</p>-->\n                                                <!--</div>-->\n                                                <!--<p><small >total receive <b >0 ECH</b></small></p>-->\n                                            </div>\n                                            <button class=\"btn btn-fill btn-info\" type=\"button\"\n                                                    (click)=\"buyTokenWithBTC()\">Buy with BTC Wallet\n                                            </button>\n                                            <button class=\"btn btn-fill btn-primary\" type=\"button\"\n                                                    (click)=\"buyTokenWithETH()\">Buy with ETH Wallet\n                                            </button>\n                                        </form>\n                                    </div>\n                                </div>\n                            </div>\n                            <div class=\"col-md-5\">\n                                <div class=\"card-content\">\n                                    <h3>BT9Coin Rate Now</h3>\n                                    <hr>\n                                    <div class=\"investrates__item\"><p class=\"investrates__item__currency\">\n                                        <img src=\"/assets/img/usd.svg\"/><span> USD</span></p>\n                                        <p class=\"investrates__item__price\">{{token_usd | number:'1.0-8'}}</p></div>\n                                    <hr class=\"hr\">\n                                    <div class=\"investrates__item\"><p class=\"investrates__item__currency\">\n                                        <img src=\"/assets/img/bitcoin-icon.png\"/><span> BTC</span></p>\n                                        <p class=\"investrates__item__price\">{{token_btc | number:'1.8-8'}}</p></div>\n                                    <hr class=\"hr\">\n                                    <div class=\"investrates__item\"><p class=\"investrates__item__currency\">\n                                        <img src=\"/assets/img/eth_29x29.png\" style=\"height: 17px;\"/><span> ETH</span>\n                                    </p>\n                                        <p class=\"investrates__item__price\">{{token_eth | number:'1.8-8'}}</p></div>\n                                    <hr class=\"hr\">\n                                    <!--<div class=\"investrates__item\"><p class=\"investrates__item__currency\">-->\n                                        <!--<span>Bonus Now: </span></p>-->\n                                        <!--<p class=\"investrates__item__price\">{{curBonus}}%</p></div>-->\n                                    <!--<hr class=\"hr\">-->\n                                    <!--<div class=\"investrates__item__price\">-->\n                                    <!--<p style=\"font-weight: bold\">1 USD = {{usd_token | number: '1.0-8'}}-->\n                                    <!--BT9Coin</p>-->\n                                    <!--</div>-->\n                                    <div class=\"investrates__item__price\">\n                                        <p style=\"font-weight: bold\">1 BTC = {{btc_usd | number: '1.0-8'}} USD\n                                            <a class=\"btn btn-round btn-warning btn-sm\">\n                                                {{counttime_min}}:{{countTime % 60}}</a></p>\n                                    </div>\n                                    <div class=\"investrates__item__price\">\n                                        <p style=\"font-weight: bold\">1 ETH = {{eth_usd | number: '1.0-8'}} USD</p>\n                                    </div>\n                                    <hr>\n                                    <div class=\"investrates__item__price note\">\n                                        <small>Note: <p>1M = 1,000,000</p>\n                                            <p>BTC AND ETH price will be refresh every 2 mins</p></small>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n                <div class=\"card\">\n                    <div class=\"card-content\">\n                        <!--<h4 class=\"card-title\">Rates and Bonus</h4>-->\n                        <!--<div class=\"content table-responsive\">-->\n                            <!--<table class=\"table\">-->\n                                <!--<thead class=\"text-primary\">-->\n                                <!--<tr>-->\n                                    <!--<th >MyAltCoin Sold</th>-->\n                                    <!--<th >USD Price</th>-->\n                                    <!--<th >BTC Price</th>-->\n                                    <!--<th >ETH Price</th>-->\n                                    <!--<th >Bonus</th>-->\n                                <!--</tr>-->\n                                <!--</thead>-->\n                                <!--<tbody>-->\n                                <!--<tr *ngFor=\"let rate of tokenRates\">-->\n                                    <!--<td>{{(rate.id-1) + (rate.id > 1 ? 'M' : '') + '-' + rate.id + 'M'}}</td>-->\n                                    <!--<td>{{rate.rate | number:'1.0-2'}}</td>-->\n                                    <!--<td>{{rate.rate * usd_btc | number:'1.8-8'}}</td>-->\n                                    <!--<td>{{rate.rate * usd_eth | number:'1.8-8'}}</td>-->\n                                    <!--<td>{{rate.bonus | number:'1.0-2'}}</td>-->\n                                <!--</tr>-->\n                                <!--</tbody>-->\n                            <!--</table>-->\n                        <!--</div>-->\n                        <h4 class=\"card-title\">ICO Agenda</h4>\n                        <div class=\"table-responsive\">\n                            <table class=\"table\">\n                                <thead>\n                                <tr>\n                                    <th class=\"text-center\">Date</th>\n                                    <th class=\"text-center\">Total BT9Coin</th>\n                                    <th class=\"text-center\">Price(USD)</th>\n                                    <th class=\"text-center\">Limit per acc</th>\n                                    <th class=\"text-center\">Status</th>\n                                </tr>\n                                </thead>\n                                <tbody>\n                                <tr *ngFor=\"let agenda of agendas\">\n                                    <td class=\"text-center\">{{agenda.from | date:'yyyy-MM-dd'}} ~ {{agenda.to | date:'yyyy-MM-dd'}}</td>\n                                    <td class=\"text-center\">{{agenda.total | number:'1.0-2'}}</td>\n                                    <td class=\"text-center\">{{agenda.price | number:'1.0-2'}}</td>\n                                    <td class=\"text-center\">{{agenda.per_account | number:'1.0-2'}}</td>\n                                    <td class=\"text-center\">{{agenda.status == 1 ? 'Completed' : 'Inactive'}}</td>\n                                </tr>\n                                <tr *ngIf=\"agendas?.length == 0\">\n                                    <td class=\"text-center\" colspan=\"4\">No Agenda</td>\n                                </tr>\n                                </tbody>\n                            </table>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/pages/ico/ico.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".input-amount {\n  text-align: center; }\n\n.investrates__item {\n  display: block;\n  font-size: 0;\n  height: 12px;\n  line-height: 12px;\n  padding: 0; }\n\n.investrates__item p {\n  display: inline-block;\n  vertical-align: baseline;\n  width: 30%;\n  text-align: left;\n  font-size: 15px;\n  font-weight: bold; }\n\n.investrates__item p img {\n  display: inline-block;\n  vertical-align: baseline;\n  height: 16px;\n  width: auto;\n  margin-right: 14px; }\n\n.investrates__item p span {\n  text-align: left;\n  font-weight: 600;\n  font-size: 13px; }\n\n.investrates__item + .investrates__item {\n  border-top: 1px solid #d5dae1; }\n\n.investrates__item__currency {\n  position: relative;\n  box-sizing: border-box;\n  padding-left: 33px; }\n\n.investrates__item__currency img {\n  position: absolute;\n  left: 0;\n  top: 0;\n  bottom: 0;\n  margin: auto; }\n\n.ico {\n  text-align: center;\n  position: relative; }\n  .ico .date {\n    margin-top: 10px; }\n  .ico b {\n    font-size: 18px; }\n  .ico .complete-label {\n    position: absolute;\n    left: 0;\n    right: 0;\n    top: 0; }\n  .ico .complete-image {\n    position: absolute;\n    background-image: url(/assets/img/completed-stamp-sm.png);\n    background-repeat: no-repeat;\n    background-position: center;\n    background-size: contain;\n    right: 0;\n    left: 0;\n    top: 0;\n    bottom: 0;\n    width: 128px;\n    height: 96px;\n    margin: auto; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/pages/ico/ico.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IcoComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_api_service__ = __webpack_require__("../../../../../src/app/services/api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_settings_settings_service__ = __webpack_require__("../../../../../src/app/services/settings/settings.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_balance_service__ = __webpack_require__("../../../../../src/app/services/balance.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var IcoComponent = (function () {
    function IcoComponent(api, settings, balance, router) {
        this.api = api;
        this.settings = settings;
        this.balance = balance;
        this.router = router;
        // constructor(private navbarTitleService: NavbarTitleService, private notificationService: NotificationService) { }
        this.receivedTxList = [];
        this.btc_usd = 0;
        this.usd_btc = 0;
        this.eth_usd = 0;
        this.usd_eth = 0;
        this.usd_token = 0;
        this.token_usd = 0;
        this.token_btc = 0;
        this.token_eth = 0;
        this.tokenAmount = 0;
        this.countTime = 0;
        this.counttime_min = 0;
        this.sysSettings = {};
        this.remain = {};
        this.totalSold = 0;
        this.curStep = 0;
        this.curBonus = 0;
        this.tokenRates = [];
        this.agendas = [];
    }
    IcoComponent.prototype.startAnimationForLineChart = function (chart) {
    };
    IcoComponent.prototype.startAnimationForBarChart = function (chart) {
    };
    IcoComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (this.settings.getUserSetting('role') == 'admin') {
            this.router.navigate(['/admin/dashboard']);
        }
        this.api.getICOAgenda({}).subscribe(function (res) {
            if (res.success) {
                _this.agendas = res.data;
                var now = new Date();
                for (var i = 0; i < res.data.length; i++) {
                    var from = new Date(res.data[i].from);
                    var to = new Date(res.data[i].to);
                    to.setDate(to.getDate() + 1);
                    if (from <= now && to > now) {
                        // this.curBonus = Number(this.tokenRates[this.curStep]['bonus']);
                        _this.token_usd = Number(res.data[i].price);
                        _this.usd_token = 1 / _this.token_usd;
                    }
                }
                if (_this.token_usd == 0) {
                    _this.token_usd = Number(res.data[res.data.length - 1].price);
                    _this.usd_token = 1 / _this.token_usd;
                }
            }
        }, function (err) {
        });
        this.api.getSettings().subscribe(function (res) {
            if (res.success) {
                _this.sysSettings = res.data;
                // this.api.getTotalSold({}).subscribe( res => {
                //     this.totalSold = res.balance;
                //     this.curStep = Math.floor(this.totalSold / 1000000);
                //
                //     // this.api.getTokenRate({}).subscribe( res => {
                //     //     if (res.success) {
                //     //
                //     //         this.tokenRates = res.data;
                //     //         this.curBonus = Number(this.tokenRates[this.curStep]['bonus']);
                //     //         this.token_usd = Number(this.tokenRates[this.curStep]['rate']);
                //     //         this.usd_token = 1 / this.token_usd;
                //     //     }
                //     // }, err => {
                //     //
                //     // });
                //     this.api.getICOAgenda({}).subscribe( res => {
                //         if (res.success) {
                //             this.agendas = res.data;
                //             var now = new Date();
                //             for (var i = 0; i < res.data.length; i ++) {
                //                 if (new Date(res.data[i].from) <= now && new Date(res.data[i].to) >= now) {
                //                     // this.curBonus = Number(this.tokenRates[this.curStep]['bonus']);
                //                     this.token_usd = Number(res.data[i].price);
                //                     this.usd_token = 1 / this.token_usd;
                //                 }
                //             }
                //             if (this.token_usd == 0) {
                //                 this.token_usd = Number(res.data[res.data.length - 1].price);
                //                 this.usd_token = 1 / this.token_usd;
                //             }
                //         }
                //     }, err => {
                //
                //     });
                // });
                //
                // var _parent = this;
                // setInterval(function() {
                //     var now:any = new Date();
                //     var ico_end:any = new Date(_parent.sysSettings.ico_end_datetime);
                //     var secs = Math.round((ico_end - now) / 1000);
                //
                //     _parent.remain.day = Math.floor(secs / (60 * 60 * 24));
                //     _parent.remain.hr = Math.floor((secs % (60 * 60 * 24)) / (60 * 60));
                //     _parent.remain.min = Math.floor(((secs % (60 * 60 * 24)) % (60 * 60)) / 60);
                //     _parent.remain.sec = secs % 60;
                // }, 1000);
            }
        });
        this.loadCoinRate();
        // this.loadTokenRate();
        // this.loadICOAgenda();
        var _parent = this;
        this.timerID = setInterval(function () {
            _parent.countTime--;
            if (_parent.countTime <= 0) {
                _parent.countTime = 119;
                _parent.loadCoinRate();
            }
            _parent.counttime_min = Math.floor(_parent.countTime / 60);
        }, 1000);
    };
    IcoComponent.prototype.ngAfterViewInit = function () {
    };
    IcoComponent.prototype.ngOnDestroy = function () {
        if (this.timerID) {
            clearInterval(this.timerID);
            console.log('timer clear');
        }
    };
    IcoComponent.prototype.loadICOAgenda = function () {
        var _this = this;
        this.api.getICOAgenda({}).subscribe(function (res) {
            if (res.success) {
                _this.agendas = res.data;
            }
        });
    };
    IcoComponent.prototype.loadCoinRate = function () {
        var _this = this;
        this.api.getCoinRate({}).subscribe(function (res) {
            if (res.success) {
                _this.btc_usd = res.btc_rate;
                _this.usd_btc = 1 / Number(res.btc_rate);
                _this.eth_usd = res.eth_rate;
                _this.usd_eth = 1 / Number(res.eth_rate);
                _this.token_btc = _this.token_usd * _this.usd_btc;
                _this.token_eth = _this.token_usd * _this.usd_eth;
            }
        }, function (err) {
        });
    };
    IcoComponent.prototype.loadTokenRate = function () {
        var _this = this;
        this.api.getTokenRate({}).subscribe(function (res) {
            if (res.success) {
                _this.token_usd = res.rate;
                _this.usd_token = 1 / Number(res.rate);
                _this.token_btc = _this.token_usd * _this.usd_btc;
                _this.token_eth = _this.token_usd * _this.usd_eth;
            }
        }, function (err) {
        });
    };
    IcoComponent.prototype.getBTCAmount = function () {
        return this.tokenAmount * this.token_btc;
    };
    IcoComponent.prototype.getETHAmount = function () {
        return this.tokenAmount * this.token_eth;
    };
    IcoComponent.prototype.allBTC = function () {
        var btcBalance = this.settings.getUserSetting('btc_balance');
        this.tokenAmount = btcBalance * (1 / this.token_btc);
    };
    IcoComponent.prototype.allETH = function () {
        var ethBalance = this.settings.getUserSetting('eth_balance');
        this.tokenAmount = ethBalance * (1 / this.token_eth);
    };
    IcoComponent.prototype.buyTokenWithBTC = function () {
        var _this = this;
        if (Number(this.tokenAmount) == 0) {
            return;
        }
        if (Number(this.tokenAmount) < 5) {
            swal({
                type: 'warning',
                title: 'Warning',
                text: 'Minimum Token Amount is 5',
                buttonsStyling: false,
                confirmButtonClass: 'btn btn-success'
            });
            return;
        }
        if (Number(this.tokenAmount * this.token_btc) > Number(this.settings.getUserSetting('btc_balance'))) {
            swal({
                type: 'warning',
                title: 'Warning',
                text: 'Your Bitcoin balance is not able',
                buttonsStyling: false,
                confirmButtonClass: 'btn btn-success'
            });
            return;
        }
        this.settings.loading = true;
        this.api.buyTokenWithBTC({
            amount: this.tokenAmount,
            token_btc: this.token_btc,
            // bonus: this.tokenAmount * this.curBonus / 100,
            token_usd: this.token_usd
        }).subscribe(function (res) {
            _this.settings.loading = false;
            if (res.success) {
                swal({
                    type: 'success',
                    title: 'Success',
                    text: 'You have bought BT9Coin with BTC successfully',
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-success'
                });
                _this.balance.getTokenBalance();
                _this.balance.getBtcBalance();
            }
            else {
                swal({
                    type: 'warning',
                    title: 'Failed',
                    text: res.error,
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-success'
                });
            }
        }, function (err) {
            _this.settings.loading = false;
            swal({
                type: 'warning',
                title: 'Failed',
                text: err._body,
                buttonsStyling: false,
                confirmButtonClass: 'btn btn-success'
            });
        });
    };
    IcoComponent.prototype.buyTokenWithETH = function () {
        var _this = this;
        if (Number(this.tokenAmount) == 0) {
            return;
        }
        if (Number(this.tokenAmount) < 5) {
            swal({
                type: 'warning',
                title: 'Warning',
                text: 'Minimum MyAltCoin Amount is 5',
                buttonsStyling: false,
                confirmButtonClass: 'btn btn-success'
            });
            return;
        }
        if (Number(this.tokenAmount * this.token_eth) > Number(this.settings.getUserSetting('eth_balance'))) {
            swal({
                type: 'warning',
                title: 'Warning',
                text: 'Your Ethereum balance is not able',
                buttonsStyling: false,
                confirmButtonClass: 'btn btn-success'
            });
            return;
        }
        this.settings.loading = true;
        this.api.buyTokenWithETH({
            amount: this.tokenAmount,
            token_eth: this.token_eth,
            bonus: this.tokenAmount * this.curBonus / 100,
            token_usd: this.token_usd
        }).subscribe(function (res) {
            _this.settings.loading = false;
            if (res.success) {
                swal({
                    type: 'success',
                    title: 'Success',
                    text: 'You have bought BT9Coin with ETH successfully',
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-success'
                });
                _this.balance.getTokenBalance();
                _this.balance.getEthBalance();
            }
            else {
                swal({
                    type: 'warning',
                    title: 'Failed',
                    text: res.error,
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-success'
                });
            }
        }, function (err) {
            _this.settings.loading = false;
            swal({
                type: 'warning',
                title: 'Failed',
                text: err._body,
                buttonsStyling: false,
                confirmButtonClass: 'btn btn-success'
            });
        });
    };
    IcoComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-ico',
            template: __webpack_require__("../../../../../src/app/pages/ico/ico.component.html"),
            styles: [__webpack_require__("../../../../../src/app/pages/ico/ico.component.scss")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_api_service__["a" /* Api */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_api_service__["a" /* Api */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__services_settings_settings_service__["a" /* SettingsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_settings_settings_service__["a" /* SettingsService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__services_balance_service__["a" /* BalanceService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services_balance_service__["a" /* BalanceService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_4__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__angular_router__["c" /* Router */]) === "function" && _d || Object])
    ], IcoComponent);
    return IcoComponent;
    var _a, _b, _c, _d;
}());

//# sourceMappingURL=ico.component.js.map

/***/ }),

/***/ "../../../../../src/app/pages/ico/ico.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IcoModule", function() { return IcoModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__md_md_module__ = __webpack_require__("../../../../../src/app/md/md.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ico_component__ = __webpack_require__("../../../../../src/app/pages/ico/ico.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ico_routing__ = __webpack_require__("../../../../../src/app/pages/ico/ico.routing.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








var IcoModule = (function () {
    function IcoModule() {
    }
    IcoModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_2__angular_common__["b" /* CommonModule */],
                __WEBPACK_IMPORTED_MODULE_1__angular_router__["d" /* RouterModule */].forChild(__WEBPACK_IMPORTED_MODULE_7__ico_routing__["a" /* IcoRoutes */]),
                __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormsModule"],
                __WEBPACK_IMPORTED_MODULE_4__md_md_module__["a" /* MdModule */],
                __WEBPACK_IMPORTED_MODULE_5__app_module__["b" /* MaterialModule */]
            ],
            declarations: [__WEBPACK_IMPORTED_MODULE_6__ico_component__["a" /* IcoComponent */]]
        })
    ], IcoModule);
    return IcoModule;
}());

//# sourceMappingURL=ico.module.js.map

/***/ }),

/***/ "../../../../../src/app/pages/ico/ico.routing.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IcoRoutes; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ico_component__ = __webpack_require__("../../../../../src/app/pages/ico/ico.component.ts");

var IcoRoutes = [
    {
        path: '',
        component: __WEBPACK_IMPORTED_MODULE_0__ico_component__["a" /* IcoComponent */]
    }
];
//# sourceMappingURL=ico.routing.js.map

/***/ })

});
//# sourceMappingURL=ico.module.chunk.js.map