webpackJsonp(["transaction.module"],{

/***/ "../../../../../src/app/pages/transaction/transaction.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\n    <div class=\"container-fluid\">\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n                <h3 class=\"title text-center\">Transaction Detail</h3>\n                <br/>\n                <div class=\"nav-center\">\n                    <ul class=\"nav nav-pills nav-pills-warning nav-pills-icons\" role=\"tablist\">\n                        <li class=\"active\" (click)=\"btcLoadTxList()\">\n                            <a href=\"#btc\" role=\"tab\" data-toggle=\"tab\">\n                                <i class=\"fa fa-bitcoin\"></i> BTC\n                            </a>\n                        </li>\n                        <li (click)=\"ethLoadTxList()\">\n                            <a href=\"#eth\" role=\"tab\" data-toggle=\"tab\">\n                                <i><img src=\"assets/img/eth_29x29.png\" /></i> ETH\n                            </a>\n                        </li>\n                        <li (click)=\"altcoinLoadTxList()\">\n                            <a href=\"#altcoin\" role=\"tab\" data-toggle=\"tab\">\n                                <i><img src=\"assets/img/B9_black.png\" style=\"width: 30px;\"/></i> BT9\n                            </a>\n                        </li>\n                        <li (click)=\"lendingLoad()\">\n                            <a href=\"#lending\" role=\"tab\" data-toggle=\"tab\">\n                                <i class=\"material-icons\">device_hub</i> Lending\n                            </a>\n                        </li>\n                    </ul>\n                </div>\n                <div class=\"tab-content\">\n                    <div class=\"tab-pane\" id=\"altcoin\">\n                        <div class=\"row\">\n                            <div class=\"col-md-12\">\n                                <div class=\"card\">\n                                    <div class=\"card-content\">\n                                        <h4 class=\"card-title\">BT9Coin Transaction</h4>\n                                        <div class=\"toolbar\">\n                                            <!--        Here you can write extra buttons/actions for the toolbar              -->\n                                        </div>\n                                        <div class=\"material-datatables table-responsive\">\n                                            <table id=\"altcoindatatables\"\n                                                   class=\"table table-striped table-no-bordered table-hover\"\n                                                   cellspacing=\"0\" width=\"100%\" style=\"width:100%\">\n                                                <thead>\n                                                <tr>\n                                                    <th style=\"width: 200px;\">DATETIME</th>\n                                                    <th>AMOUNT</th>\n                                                    <th>TYPE</th>\n                                                    <th>STATUS</th>\n                                                    <th>DESCRIPTION</th>\n                                                </tr>\n                                                </thead>\n                                                <tbody>\n                                                <tr *ngFor=\"let tx of altcoinTxList; let last = last\">\n                                                    <td style=\"width: 200px;\">{{tx['date'] | date: 'yyyy-MM-dd HH:mm:ss'}}</td>\n                                                    <td>{{(tx['category'] == 'send' ? (-1) * tx['src_amount'] : tx['dest_amount']) | number:'1.0-8'}}</td>\n                                                    <td [ngSwitch]=\"tx['type']\">\n                                                        <span *ngSwitchCase=\"1\">PURCHASE TOKEN</span>\n                                                        <span *ngSwitchCase=\"2\">\n                                                            {{tx['exchange_type'] == 0 ? 'EXCHANGE/BUY' : 'EXCHANGE/SELL'}}\n                                                        </span>\n                                                        <span *ngSwitchCase=\"3\">BONUS</span>\n                                                        <span *ngSwitchCase=\"4\">LENDING</span>\n                                                        <span *ngSwitchCase=\"5\">WITHDRAW</span>\n                                                        <span *ngSwitchCase=\"6\">LENDING/TRANSFER</span>\n                                                        <span *ngSwitchCase=\"7\">STAKING</span>\n                                                        <span *ngSwitchCase=\"8\">STAKING/UNLOCK</span>\n                                                        <span *ngSwitchCase=\"9\">DEPOSIT</span>\n                                                        <span *ngSwitchCase=\"10\">STAKING/EARN</span>\n                                                    </td>\n                                                    <td [ngSwitch]=\"tx['status']\">\n                                                        <label class=\"label label-primary\" *ngSwitchCase=\"0\">PENDING</label>\n                                                        <label class=\"label label-success\" *ngSwitchCase=\"1\">SUCCESS</label>\n                                                        <label class=\"label label-warning\" *ngSwitchCase=\"4\">OPENING</label>\n                                                    </td>\n                                                    <td>{{tx['txid']}}</td>\n                                                </tr>\n                                                </tbody>\n                                            </table>\n                                        </div>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"tab-pane active\" id=\"btc\">\n                        <div class=\"row\">\n                            <div class=\"col-md-12\">\n                                <div class=\"card\">\n                                    <div class=\"card-content\">\n                                        <h4 class=\"card-title\">BitCoin Transaction</h4>\n                                        <div class=\"toolbar\">\n                                            <!--        Here you can write extra buttons/actions for the toolbar              -->\n                                        </div>\n                                        <div class=\"material-datatables table-responsive\">\n                                            <table id=\"btcdatatables\"\n                                                   class=\"table table-striped table-no-bordered table-hover\"\n                                                   cellspacing=\"0\" width=\"100%\" style=\"width:100%\">\n                                                <thead>\n                                                <tr>\n                                                    <th style=\"width: 200px;\">DATETIME</th>\n                                                    <th>AMOUNT</th>\n                                                    <th>TYPE</th>\n                                                    <th>STATUS</th>\n                                                    <th>DESCRIPTION</th>\n                                                </tr>\n                                                </thead>\n                                                <tbody>\n                                                <tr *ngFor=\"let tx of btcTxList; let last = last\">\n                                                    <td style=\"width: 200px;\">{{tx['date'] | date: 'yyyy-MM-dd HH:mm:ss'}}</td>\n                                                    <td>{{(tx['category'] == 'send' ? (-1) * tx['src_amount'] : tx['dest_amount']) | number:'1.0-8'}}</td>\n                                                    <td [ngSwitch]=\"tx['type']\">\n                                                        <span *ngSwitchCase=\"1\">PURCHASE TOKEN</span>\n                                                        <span *ngSwitchCase=\"2\">\n                                                            {{tx['exchange_type'] == 0 ? 'EXCHANGE/BUY' : 'EXCHANGE/SELL'}}\n                                                        </span>\n                                                        <span *ngSwitchCase=\"3\">BONUS</span>\n                                                        <span *ngSwitchCase=\"4\">LENDING</span>\n                                                        <span *ngSwitchCase=\"5\">WITHDRAW</span>\n                                                        <span *ngSwitchCase=\"6\">LENDING/TRANSFER</span>\n                                                        <span *ngSwitchCase=\"7\">STAKING</span>\n                                                        <span *ngSwitchCase=\"8\">STAKING/UNLOCK</span>\n                                                        <span *ngSwitchCase=\"9\">DEPOSIT</span>\n                                                        <span *ngSwitchCase=\"10\">STAKING/EARN</span>\n                                                    </td>\n                                                    <td [ngSwitch]=\"tx['status']\">\n                                                        <label class=\"label label-primary\" *ngSwitchCase=\"0\">PENDING</label>\n                                                        <label class=\"label label-success\" *ngSwitchCase=\"1\">SUCCESS</label>\n                                                        <label class=\"label label-warning\" *ngSwitchCase=\"4\">OPENING</label>\n                                                    </td>\n                                                    <td>{{tx['txid']}}</td>\n                                                </tr>\n                                                </tbody>\n                                            </table>\n                                        </div>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"tab-pane\" id=\"eth\">\n                        <div class=\"row\">\n                            <div class=\"col-md-12\">\n                                <div class=\"card\">\n                                    <div class=\"card-content\">\n                                        <h4 class=\"card-title\">Ethereum Transaction</h4>\n                                        <div class=\"toolbar\">\n                                            <!--        Here you can write extra buttons/actions for the toolbar              -->\n                                        </div>\n                                        <div class=\"material-datatables table-responsive\">\n                                            <table id=\"ethdatatables\"\n                                                   class=\"table table-striped table-no-bordered table-hover\"\n                                                   cellspacing=\"0\" width=\"100%\" style=\"width:100%\">\n                                                <thead>\n                                                <tr>\n                                                    <th style=\"width: 200px;\">DATETIME</th>\n                                                    <th>AMOUNT</th>\n                                                    <th>TYPE</th>\n                                                    <th>STATUS</th>\n                                                    <th>DESCRIPTION</th>\n                                                </tr>\n                                                </thead>\n                                                <tbody>\n                                                <tr *ngFor=\"let tx of ethTxList; let last = last\">\n                                                    <td style=\"width: 200px;\">{{tx['date'] | date: 'yyyy-MM-dd HH:mm:ss'}}</td>\n                                                    <td>{{(tx['category'] == 'send' ? (-1) * tx['src_amount'] : tx['dest_amount']) | number:'1.0-8'}}</td>\n                                                    <td [ngSwitch]=\"tx['type']\">\n                                                        <span *ngSwitchCase=\"1\">PURCHASE TOKEN</span>\n                                                        <span *ngSwitchCase=\"2\">\n                                                            {{tx['exchange_type'] == 0 ? 'EXCHANGE/BUY' : 'EXCHANGE/SELL'}}\n                                                        </span>\n                                                        <span *ngSwitchCase=\"3\">BONUS</span>\n                                                        <span *ngSwitchCase=\"4\">LENDING</span>\n                                                        <span *ngSwitchCase=\"5\">WITHDRAW</span>\n                                                        <span *ngSwitchCase=\"6\">LENDING/TRANSFER</span>\n                                                        <span *ngSwitchCase=\"7\">STAKING</span>\n                                                        <span *ngSwitchCase=\"8\">STAKING/UNLOCK</span>\n                                                        <span *ngSwitchCase=\"9\">DEPOSIT</span>\n                                                        <span *ngSwitchCase=\"10\">STAKING/EARN</span>\n                                                    </td>\n                                                    <td [ngSwitch]=\"tx['status']\">\n                                                        <label class=\"label label-primary\" *ngSwitchCase=\"0\">PENDING</label>\n                                                        <label class=\"label label-success\" *ngSwitchCase=\"1\">SUCCESS</label>\n                                                        <label class=\"label label-warning\" *ngSwitchCase=\"4\">OPENING</label>\n                                                    </td>\n                                                    <td>{{tx['txid']}}</td>\n                                                </tr>\n                                                </tbody>\n                                            </table>\n                                        </div>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"tab-pane\" id=\"lending\">\n                        <div class=\"row\">\n                            <div class=\"col-md-12\">\n                                <div class=\"card\">\n                                    <div class=\"card-content\">\n                                        <h4 class=\"card-title\">Lending Transaction</h4>\n                                        <div class=\"toolbar\">\n                                            <!--        Here you can write extra buttons/actions for the toolbar              -->\n                                        </div>\n                                        <div class=\"material-datatables table-responsive\">\n                                            <table id=\"lendingdatatables\"\n                                                   class=\"table table-striped table-no-bordered table-hover\"\n                                                   cellspacing=\"0\" width=\"100%\" style=\"width:100%\">\n                                                <thead>\n                                                <tr>\n                                                    <th style=\"width: 200px;\">DATETIME</th>\n                                                    <th>AMOUNT</th>\n                                                    <th>TYPE</th>\n                                                    <th>STATUS</th>\n                                                    <th>DESCRIPTION</th>\n                                                </tr>\n                                                </thead>\n                                                <tbody>\n                                                <tr *ngFor=\"let tx of lendingList; let last = last\">\n                                                    <td style=\"width: 200px;\">{{tx['date'] | date: 'yyyy-MM-dd HH:mm:ss'}}</td>\n                                                    <td>{{(tx['category'] == 'lending' ? tx['dest_amount'] : tx['src_amount']) | number:'1.0-2'}}</td>\n                                                    <td>\n                                                        {{tx['category'] == 'lending' ? 'LENDING' : 'TRANSFER'}}\n                                                    </td>\n                                                    <td [ngSwitch]=\"tx['status']\">\n                                                        <label class=\"label label-primary\" *ngSwitchCase=\"0\">PENDING</label>\n                                                        <label class=\"label label-success\" *ngSwitchCase=\"1\">SUCCESS</label>\n                                                    </td>\n                                                    <td>\n                                                        {{tx['category'] == 'lending' ? ('Finish Day:  ' +  (tx['finish_date'] | date: 'yyyy-MM-dd HH:mm:ss')) : ''}}\n                                                    </td>\n                                                </tr>\n                                                </tbody>\n                                            </table>\n                                        </div>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/pages/transaction/transaction.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TransactionComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_api_service__ = __webpack_require__("../../../../../src/app/services/api.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var TransactionComponent = (function () {
    function TransactionComponent(api) {
        this.api = api;
        this.btcTxList = [];
        this.ethTxList = [];
        this.altcoinTxList = [];
        this.lendingList = [];
        this.altcoindatatable = null;
        this.btcdatatable = null;
        this.ethdatatable = null;
        this.lendingdatatable = null;
    }
    TransactionComponent.prototype.ngOnInit = function () {
        this.btcLoadTxList();
        // this.ethLoadTxList();
        // this.altcoinLoadTxList();
        // this.lendingLoad();
    };
    TransactionComponent.prototype.buildBTCDataTable = function () {
        this.btcdatatable = $('#btcdatatables').DataTable({
            'pagingType': 'full_numbers',
            'lengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
            responsive: true,
            language: {
                search: '_INPUT_',
                searchPlaceholder: 'Search records',
            }
        });
    };
    TransactionComponent.prototype.buildETHDataTable = function () {
        this.ethdatatable = $('#ethdatatables').DataTable({
            'pagingType': 'full_numbers',
            'lengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
            responsive: true,
            language: {
                search: '_INPUT_',
                searchPlaceholder: 'Search records',
            }
        });
    };
    TransactionComponent.prototype.buildAltCoinDataTable = function () {
        this.altcoindatatable = $('#altcoindatatables').DataTable({
            'pagingType': 'full_numbers',
            'lengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
            responsive: true,
            language: {
                search: '_INPUT_',
                searchPlaceholder: 'Search records',
            }
        });
    };
    TransactionComponent.prototype.buildLendingDataTable = function () {
        this.lendingdatatable = $('#lendingdatatables').DataTable({
            'pagingType': 'full_numbers',
            'lengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
            responsive: true,
            language: {
                search: '_INPUT_',
                searchPlaceholder: 'Search records',
            }
        });
    };
    TransactionComponent.prototype.btcLoadTxList = function () {
        var _this = this;
        if (this.btcdatatable) {
            this.btcdatatable.destroy();
        }
        this.api.getTxList('BTC', {}).subscribe(function (res) {
            if (res.success) {
                _this.btcTxList = res.data;
                var _parent_1 = _this;
                var timerID = setInterval(function () {
                    if ($('#btcdatatables tbody tr').length == res.data.length) {
                        clearInterval(timerID);
                        _parent_1.buildBTCDataTable();
                    }
                }, 200);
            }
        }, function (err) {
        });
    };
    TransactionComponent.prototype.ethLoadTxList = function () {
        var _this = this;
        if (this.ethdatatable) {
            this.ethdatatable.destroy();
        }
        this.api.getTxList('ETH', {}).subscribe(function (res) {
            if (res.success) {
                _this.ethTxList = res.data;
                var _parent_2 = _this;
                var timerID1 = setInterval(function () {
                    if ($('#ethdatatables tbody tr').length == res.data.length) {
                        clearInterval(timerID1);
                        _parent_2.buildETHDataTable();
                    }
                }, 200);
            }
        }, function (err) {
        });
    };
    TransactionComponent.prototype.altcoinLoadTxList = function () {
        var _this = this;
        if (this.altcoindatatable) {
            this.altcoindatatable.destroy();
        }
        this.api.getTokenHistory().subscribe(function (res) {
            if (res.success) {
                _this.altcoinTxList = res.data;
                var _parent_3 = _this;
                var timerID1 = setInterval(function () {
                    if ($('#altcoindatatables tbody tr').length == res.data.length) {
                        clearInterval(timerID1);
                        _parent_3.buildAltCoinDataTable();
                    }
                }, 200);
            }
        }, function (err) {
        });
    };
    TransactionComponent.prototype.lendingLoad = function () {
        var _this = this;
        if (this.lendingdatatable) {
            this.lendingdatatable.destroy();
        }
        this.api.getLendingTx({}).subscribe(function (res) {
            if (res.success) {
                _this.lendingList = res.data;
                var _parent_4 = _this;
                var timerID2 = setInterval(function () {
                    if ($('#lendingdatatables tbody tr').length == res.data.length) {
                        clearInterval(timerID2);
                        _parent_4.buildLendingDataTable();
                    }
                }, 200);
            }
        }, function (err) {
        });
    };
    TransactionComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-transaction',
            template: __webpack_require__("../../../../../src/app/pages/transaction/transaction.component.html")
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_api_service__["a" /* Api */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_api_service__["a" /* Api */]) === "function" && _a || Object])
    ], TransactionComponent);
    return TransactionComponent;
    var _a;
}());

//# sourceMappingURL=transaction.component.js.map

/***/ }),

/***/ "../../../../../src/app/pages/transaction/transaction.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TransactionModule", function() { return TransactionModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__md_md_module__ = __webpack_require__("../../../../../src/app/md/md.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__transaction_component__ = __webpack_require__("../../../../../src/app/pages/transaction/transaction.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__transaction_routing__ = __webpack_require__("../../../../../src/app/pages/transaction/transaction.routing.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








var TransactionModule = (function () {
    function TransactionModule() {
    }
    TransactionModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_2__angular_common__["b" /* CommonModule */],
                __WEBPACK_IMPORTED_MODULE_1__angular_router__["d" /* RouterModule */].forChild(__WEBPACK_IMPORTED_MODULE_7__transaction_routing__["a" /* TransactionRoutes */]),
                __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormsModule"],
                __WEBPACK_IMPORTED_MODULE_4__md_md_module__["a" /* MdModule */],
                __WEBPACK_IMPORTED_MODULE_5__app_module__["b" /* MaterialModule */]
            ],
            declarations: [__WEBPACK_IMPORTED_MODULE_6__transaction_component__["a" /* TransactionComponent */]]
        })
    ], TransactionModule);
    return TransactionModule;
}());

//# sourceMappingURL=transaction.module.js.map

/***/ }),

/***/ "../../../../../src/app/pages/transaction/transaction.routing.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TransactionRoutes; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__transaction_component__ = __webpack_require__("../../../../../src/app/pages/transaction/transaction.component.ts");

var TransactionRoutes = [
    {
        path: '',
        component: __WEBPACK_IMPORTED_MODULE_0__transaction_component__["a" /* TransactionComponent */]
    }
];
//# sourceMappingURL=transaction.routing.js.map

/***/ })

});
//# sourceMappingURL=transaction.module.chunk.js.map