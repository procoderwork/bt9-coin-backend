<!DOCTYPE html>
<html>
<head>
    <title>Verify your account</title>
</head>
<style>
    body {
        font-size: 16px;
    }
    .button {
        background-color: #4caf50;
        color: #ffffff;
        border-radius: 5px;
        cursor: pointer;
        display: inline-block;
        font-size: 14px;
        font-weight: bold;
        margin: 0;
        padding: 12px 25px;
        text-decoration: none;
        text-transform: capitalize;
        height: 15px;
    }
</style>
<body>
    Hi <b>{{$user->fullname}}</b>
    <br>
    <br>
    Thank you for registering on BT9Coin!
    <br>
    <br>
    In order to activate your account, please verify your e-mail address.
    <br>
    <br>
    You can active account by clicking the button or going to <a
            href="{{$user->confirmation_url}}">{{$user->confirmation_url}}</a>
    <br>
    <br>
    <a href="{{$user->confirmation_url}}" class="button">
        Verify Now
    </a>
</body>
</html>