<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class LoginNotificationMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $user;
    protected $loginInfo;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $loginInfo)
    {
        //
        $this->user= $user;
        $this->loginInfo = $loginInfo;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->to($this->user->email, $this->user->fullname)
            ->subject('Login Notification')
            ->view('emails.login')
            ->with([
                'user' => $this->user,
                'loginInfo' => $this->loginInfo
            ]);
    }
}
