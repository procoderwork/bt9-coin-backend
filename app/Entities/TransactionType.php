<?php

namespace App\Entities;


interface TransactionType {
    const RECORD = 0;
    const BUYTOKEN = 1;
    const EXCHANGE = 2;
    const TOKEN_BONUS = 3;
    const LENDING = 4;
    const WITHDRAW = 5;
    const LENDING_TRANSFER = 6;
    const STAKING = 7;
    const STAKING_TRANSFER = 8;
    const DEPOSIT = 9;
    const STAKING_EARN = 10;
    const LENDING_BONUS = 11;
}