<?php

namespace App\Http\Controllers\Api;

use App\Entities\ExchangeList;
use App\Entities\Setting;
use App\Entities\TransactionStatus;
use App\Entities\TransactionType;
use App\Libs\BitcoinAddressValidator;
use App\Libs\JsonRPCClient;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class CoinController extends Controller
{
    public function withdraw($coin, Request $request) {
        $input = $request->input();

        $user = Auth::user();

        if (!$user) {
            return response('Not Authorized', 403);
        }

        $coin = strtolower($coin);

        $settings = Setting::all()->pluck('value', 'name');

        if ($coin == 'btc' || $coin == 'bt9') {
            if ($coin == 'btc') {
                $btcController = new BTCController();
                $rpcUrl = $btcController->getRPCUrl($settings);
            } else {
                $bt9Controller = new TokenController();
                $rpcUrl = $bt9Controller->getRPCUrl($settings);
            }

            $jsonRpc = new JsonRPCClient($rpcUrl);
            try {
                $validate = $jsonRpc->validateaddress($input['address']);
            } catch (\Exception $e) {
                return response()->json([
                    'success' => false,
                    'error' => 'Withdraw address is invalid'
                ]);
            }

            if (!$validate['isvalid']) {
                return response()->json([
                    'success' => false,
                    'error' => 'Withdraw address is invalid'
                ]);
            }
        }

        if ($coin == 'bt9') {
            $coin = 'token';
        }

        $amount = $input['amount'];
        $fee = $input['fee'];
        $address = $input['address'];

        if ($coin == 'btc') {
            $btcController = new BTCController();
            $balance = $btcController->balance($user);
        } else if ($coin == 'eth') {
            $ethController = new ETHController();
            $balance = $ethController->balance($user);
        } else {
            $tokenController = new TokenController();
            $balance = $tokenController->balance($user);
        }

        if ($amount > $balance) {
            return response()->json([
                'success' => false,
                'error' => 'Your balance is not enough.'
            ]);
        }

        $fee_amount = round($amount * $fee / 100, 8);

        $exList = new ExchangeList();
        $exList->user_id = $user->id;
        $exList->date = Carbon::now();
        $exList->src_currency = strtoupper($coin);
        $exList->src_amount = $amount;
        $exList->type = TransactionType::WITHDRAW;
        $exList->status = TransactionStatus::PENDING;
        $exList->fee = $fee_amount;
        $exList->fee_rate = $fee;
        $exList->address = $address;
        $exList->save();

        return response()->json([
            'success' => true
        ]);
    }

    public function cancelWithdraw($coin, Request $request) {
        $input = $request->input();

        $id = $input['id'];

        $list = ExchangeList::find($id);

        if (!$list) {
            return response()->json([
                'success' => false,
                'error' => 'Transaction Not Found'
            ]);
        }

        $list->delete();

        return response()->json([
            'success' => true
        ]);
    }

    public function getUnconfirmedSentTxList($coin, Request $request) {
        $user = Auth::user();

        if (!$user) {
            return response('Not Authorized', 403);
        }

        if ($coin == 'BT9') {
            $coin = 'TOKEN';
        }

        $lists = ExchangeList::where([['user_id', $user->id], ['src_currency', $coin], ['type', TransactionType::WITHDRAW], ['status', TransactionStatus::PENDING]])->get();

        return response()->json([
            'success' => true,
            'data' => $lists
        ]);
    }

    public function getWithdrawPending($coin, Request $request) {
        if ($coin == 'BT9') {
            $coin = 'TOKEN';
        }
        $data = ExchangeList::where([['type', TransactionType::WITHDRAW], ['status', TransactionStatus::PENDING], ['src_currency', $coin]])->get();

        return response()->json([
            'success' => true,
            'data' => $data
        ]);
    }
}
