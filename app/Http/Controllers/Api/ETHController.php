<?php

namespace App\Http\Controllers\Api;

use App\Entities\EthAddress;
use App\Entities\ETHOrder;
use App\Entities\ExchangeList;
use App\Entities\MarketOrderType;
use App\Entities\Order;
use App\Entities\Setting;
use App\Entities\TransactionStatus;
use App\Entities\TransactionType;
use App\Libs\BitcoinAddressValidator;
use App\User;
use BitWasp\Bitcoin\Transaction\Transaction;
use BlockCypher\Api\TX;
use BlockCypher\Api\Wallet;
use BlockCypher\Auth\SimpleTokenCredential;
use BlockCypher\Client\AddressClient;
use BlockCypher\Client\BlockchainClient;
use BlockCypher\Client\WalletClient;
use BlockCypher\Rest\ApiContext;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ETHController extends Controller
{
    //
    public $BIT_SATOSHI = 100000000;

    public function getBlockCypherToken() {
        $token = Setting::where('name', 'blockcypher_token')->first();
        
        if ($token) {
            return $token->value;
        } else {
            return env('BLOCKCYPHER_TOKEN');
        }
        
    }

    public function getCoinAddress($user) {
        return EthAddress::where('user_id', $user->id)->first();
    }

    //
    public function getAddress(Request $request) {
        $user = Auth::user();

        if (!$user) {
            return response('Not Authorized', 403);
        }

        $settings = Setting::all()->pluck('value', 'name');

        $apiContext = ApiContext::create(
            'main', 'eth', 'v1',
            new SimpleTokenCredential($this->getBlockCypherToken()),
            array('log.LogEnabled' => true, 'log.FileName' => 'BlockCypher.log', 'log.LogLevel' => 'DEBUG')
        );

        $coin_address = $this->getCoinAddress($user);
        if ($coin_address && $coin_address->address) {
            return response()->json([
                'success' => true,
                'address' => $coin_address->address
            ]);
        }

        if (!$coin_address) {
            $coin_address = new EthAddress();
            $coin_address->user_id = $user->id;
        }

        $label = $user->email;

        try {
            $addressClient = new AddressClient($apiContext);
            $addressKeyChain = $addressClient->generateAddress();

            $newAddress = $addressKeyChain->getAddress();

            $coin_address->address = $newAddress;
            $coin_address->privatekey = $addressKeyChain->getPrivate();
            $coin_address->publickey = $addressKeyChain->getPublic();
            $coin_address->wif = $addressKeyChain->getWif();
            $coin_address->save();

            return response()->json([
                'success' => true,
                'address' => $newAddress
            ]);

//            if ($coin == 'eth') {
//            } else {
//                $wallet = new Wallet();
//                $wallet->setName($label);
//                $wallet->setAddresses(array(
//                    $newAddress
//                ));
//
//                $walletClient = new WalletClient($apiContext);
//                $createdWallet = $walletClient->create($wallet);
//            }
        } catch (\Exception $e) {
            return response($e->getMessage(), 403);
        }
    }

    public function balance($user) {
        $settings = Setting::all()->pluck('value', 'name');


        $sent = ExchangeList::where([['src_currency', 'ETH'], ['user_id', $user->id]])->sum('src_amount');
        $receive = ExchangeList::where([['dest_currency', 'ETH'], ['user_id', $user->id]])->sum('dest_amount');

        $sell = ETHOrder::where([['user_id', $user->id], ['type', MarketOrderType::BUY]])->sum(\DB::raw('rate * amount'));

        $balance = 0 - $sent + $receive - $sell;

        if ($settings['enable_eth_wallet'] == 1) {
            $apiContext = ApiContext::create(
                'main', 'eth', 'v1',
                new SimpleTokenCredential($this->getBlockCypherToken()),
                array('log.LogEnabled' => true, 'log.FileName' => 'BlockCypher.log', 'log.LogLevel' => 'DEBUG')
            );

            $coin_address = $this->getCoinAddress($user);

            if ($coin_address) {
                $addressClient = new AddressClient($apiContext);

                try {
                    $addressBalance = $addressClient->getBalance($coin_address->address);
                    $wallet_balance = $addressBalance->final_balance / $this->BIT_SATOSHI;
                } catch (\Exception $e) {
                    $wallet_balance = 0;
                }
                $balance += $wallet_balance;
            }
        }

        return $balance;
    }

    public function getBalance(Request $request) {
        $user = Auth::user();

        if (!$user) {
            return response('Not Authorized', 403);
        }

        return response()->json([
            'success' => true,
            'available_balance' => $this->balance($user)
        ]);
    }

    public function getUserHistory($user) {
        $settings = Setting::all()->pluck('value', 'name');

        if ($settings['enable_eth_wallet'] == 1) {
            $apiContext = ApiContext::create(
                'main', 'eth', 'v1',
                new SimpleTokenCredential($this->getBlockCypherToken()),
                array('log.LogEnabled' => true, 'log.FileName' => 'BlockCypher.log', 'log.LogLevel' => 'DEBUG')
            );

            $coin_address = $this->getCoinAddress($user);
            if ($coin_address) {
                $addressClient = new \BlockCypher\Client\AddressClient($apiContext);

                try {
                    $fullAddress = $addressClient->getFullAddress($coin_address->address);
                    $txs = $fullAddress->getTxs();
                } catch (\Exception $ex) {
                    $txs = array();
                }
            }
        }

        $data = array();

        $send_lists = ExchangeList::where([['src_currency', 'ETH'], ['user_id', $user->id], ['date', '<=', Carbon::now()]])->get();
        $receive_lists = ExchangeList::where([['dest_currency', 'ETH'], ['user_id', $user->id], ['date', '<=', Carbon::now()]])->get();

        $open_lists = ETHOrder::where([['user_id', $user->id], ['type', MarketOrderType::BUY]])->get();

        if (count($send_lists) > 0) {
            foreach ($send_lists as $list) {
                $list->category = 'send';
                array_push($data, $list);
            }
        }

        if (count($receive_lists) > 0) {
            foreach ($receive_lists as $list) {
                $list->category = 'receive';
                array_push($data, $list);
            }
        }

        if (count($open_lists) > 0) {
            foreach ($open_lists as $list) {
                array_push($data, array(
                    'date' => $list->created_at->toDateTimeString(),
                    'src_amount' => $list->amount * $list->rate,
                    'is_open' => 1,
                    'type' => TransactionType::EXCHANGE,
                    'exchange_type' => MarketOrderType::BUY,
                    'status' => TransactionStatus::OPEN,
                    'category' => 'send'
                ));
            }
        }

        if (count($txs) > 0) {
            foreach ($txs as $tx) {
                $txOutputs = $tx->getOutputs();

                foreach ($txOutputs as $txOutput) {
                    $addresses = $txOutput->getAddresses();

                    foreach ($addresses as $address) {
                        if ($coin_address->address == $address) {
                            array_push($data, array(
                                'date' => $tx->getReceived(),
                                'dest_amount' => $txOutput->getValue() / $this->BIT_SATOSHI,
                                'type' => TransactionType::DEPOSIT,
                                'category' => 'receive',
                                'txid' => $tx->getHash(),
                            ));
                        }
                    }
                }
            }
        }

        return $data;
    }

    public function getTxList(Request $request) {
        $user = Auth::user();

        if (!$user) {
            return response('Not Authorized', 403);
        }

        $data = $this->getUserHistory($user);

        return response()->json([
            'success' => true,
            'data' => $data
        ]);
    }


    public function getUnconfirmedReceivedTxList(Request $request) {
        $user = Auth::user();

        if (!$user) {
            return response('Not Authorized', 403);
        }

        $settings = Setting::all()->pluck('value', 'name');

        $coin_address = $this->getCoinAddress($user);
        if (!$coin_address) {
            return response()->json('Failed', 403);
        }

        $apiContext = ApiContext::create(
            'main', 'eth', 'v1',
            new SimpleTokenCredential($this->getBlockCypherToken()),
            array('log.LogEnabled' => true, 'log.FileName' => 'BlockCypher.log', 'log.LogLevel' => 'DEBUG')
        );

        $addressClient = new \BlockCypher\Client\AddressClient($apiContext);

        try {
            $fullAddress = $addressClient->getFullAddress($coin_address->address);
        } catch (\Exception $ex) {
            return response()->json('Failed', 403);
        }

        $txs = $fullAddress->getTxs();

        $result = array();
        if (is_null($txs)) {
            return response()->json([
                'success' => true,
                'data' => $result
            ]);
        }

        foreach($txs as $tx) {
            if ($tx->getConfirmations() > 0) continue;

            $txOutputs = $tx->getOutputs();

            foreach ($txOutputs as $txOutput) {
                $addresses = $txOutput->getAddresses();

                foreach ($addresses as $address) {
                    if ($coin_address->address == $address) {
                        array_push($result, array(
                            'txid' => $tx->getHash(),
                            'amount' => $txOutput->getValue() / $this->BIT_SATOSHI,
                            'confirmations' => $tx->getConfirmations()
                        ));
                    }
                }
            }
        }

        return response()->json([
            'success' => true,
            'data' => $result
        ]);
    }


    public function getTotalBalance(Request $request) {
        $settings = Setting::all()->pluck('value', 'name');

        $balance = 0;
//        try {
//            $balance = $jsonRpc->getbalance();
//        } catch (\Exception $e) {
//            $balance = 0;
//        }

        return response()->json([
            'success' => true,
            'balance' => $balance
        ]);
    }



    public function getAllTx(Request $request) {

        $data = array();

        return response()->json([
            'success' => true,
            'data' => $data
        ]);
    }

    public function getStatistics(Request $request) {
        $now = Carbon::now();

        $last = ETHOrder::where('type', MarketOrderType::BUY)->orderBy('created_at', 'desc')->first();
        $lastBid = 0;
        if ($last) {
            $lastBid = $last->rate;
        }

        $last = ETHOrder::where('type', MarketOrderType::SELL)->orderBy('created_at', 'desc')->first();
        $lastAsk = 0;
        if ($last) {
            $lastAsk = $last->rate;
        }

        $last = ExchangeList::where([['type', TransactionType::EXCHANGE], ['src_currency', 'ETH']])->orderBy('date', 'desc')->first();
        $lastPrice = 0;
        if ($last) {
            $lastPrice = $last->rate;
        }

        $before = Carbon::now()->addDay(-1);
        $high = ETHOrder::where('created_at', '>=', $before)->max('rate');
        $low = ETHOrder::where('created_at', '>=', $before)->max('rate');

        $volume = ExchangeList::where([['type', TransactionType::EXCHANGE], ['src_currency', 'ETH'], ['date', '>=', $before]])->sum('src_amount');

        return response()->json([
            'success' => true,
            'data' => array(
                'lastBid' => $lastBid,
                'lastAsk' => $lastAsk,
                'lastPrice' => $lastPrice,
                'high' => $high ? $high: 0,
                'low' => $low ? $low : 0,
                'volume' => $volume ? $volume : 0
            )
        ]);
    }

    public function getHistory(Request $request) {

        $query =    "SELECT date, ".
                        "MAX(rate) open, MIN(rate) close, MAX(rate) high, MIN(rate) low, SUM(IF(src_currency = 'ETH', src_amount, 0)) volume ".
                    "FROM exchange_lists WHERE src_currency = 'ETH' and type = ".TransactionType::EXCHANGE." ".
                    "GROUP BY date ".
                    " UNION ALL ".
                    "SELECT created_at AS date, ".
                        "MAX(rate) open, MIN(rate) close, MAX(rate) high, MIN(rate) low, 0 volume ".
                    "FROM eth_orders ".
                    "GROUP BY  created_at ";

        $query =    "SELECT date, MAX(open) open, MAX(high) high, MIN(low) low, MIN(close) close, SUM(volume) volume ".
                    "FROM (".$query.") AS t1 ".
                    "GROUP BY date ORDER BY date";

        $data = \DB::select($query);

        foreach ($data as $rec) {
            $rec->date = strtotime($rec->date) * 1000;
        }

        return response()->json([
            'success' => true,
            'data' => $data
        ]);
    }
}
