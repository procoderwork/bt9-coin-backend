<?php

namespace App\Http\Controllers\Api;

use App\Entities\BonusList;
use App\Entities\BtcAddress;
use App\Entities\EthAddress;
use App\Entities\ExchangeList;
use App\Entities\ICOAgenda;
use App\Entities\LoginHistory;
use App\Entities\ResetPassword;
use App\Entities\Setting;
use App\Entities\TokenRate;
use App\Events\SendEmailEvent;
use App\Libs\GoogleAuthenticator;
use App\Mail\ForgotPasswordMail;
use App\Mail\LoginNotificationMail;
use App\Mail\VerifyAccountMail;
use App\User;
use BlockCypher\Auth\SimpleTokenCredential;
use BlockCypher\Client\AddressClient;
use BlockCypher\Rest\ApiContext;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Symfony\Component\HttpFoundation\Exception\RequestExceptionInterface;
use tests\Simple;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;
use Jenssegers\Agent\Agent;

class ICOAgendaController extends Controller
{
    //
    public function getICOAgenda(Request $request) {
        $data = ICOAgenda::orderBy('from', 'asc')->get();

        return response()->json([
            'success' => true,
            'data' => $data
        ]);
    }

    public function saveICOAgenda(Request $request) {
        $input = $request->input();

        if (isset($input['id']) && $input['id'] > 0) {
            $agenda = ICOAgenda::find($input['id']);

            if (!$agenda) {
                return response()->json([
                    'success' => false,
                    'data' => 'No data'
                ]);
            }
        }  else {
            $agenda = new ICOAgenda();
        }

        $agenda->from = $input['from'];
        $agenda->to = $input['to'];
        $agenda->total = $input['total'];
        $agenda->price = $input['price'];
        $agenda->per_account = $input['per_account'];
        $agenda->status = $input['status'];

        $agenda->save();

        return response()->json([
            'success' => true,
        ]);
    }

    public function deleteICOAgenda(Request $request) {
        $input = $request->input();

        if (isset($input['id']) && $input['id'] > 0) {
            $agenda = ICOAgenda::find($input['id']);

            if (!$agenda) {
                return response()->json([
                    'success' => false,
                    'data' => 'No data'
                ]);
            }

            $agenda->delete();

            return response()->json([
                'success' => true
            ]);

        }  else {
            return response()->json([
                'success' => false,
                'data' => 'No data'
            ]);
        }
    }

    public function getICOAgendaChartData(Request $request) {
        $data = ICOAgenda::orderBy('from', 'asc')->get();

        return response()->json([
            'success' => true,
            'data' => $data
        ]);
    }
}
