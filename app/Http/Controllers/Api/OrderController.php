<?php

namespace App\Http\Controllers\Api;

use App\Entities\MarketOrderType;
use App\Entities\ExchangeList;
use App\Entities\Order;
use App\Entities\Setting;
use App\Entities\TransactionType;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    public function buyOrder(Request $request) {
        $input = $request->input();

        if (!isset($input['amount'])) {
            return response()->json([
                'success' => false,
                'error' => 'Please enter amount to buy'
            ]);
        }

        if (!isset($input['rate'])) {
            return response()->json([
                'success' => false,
                'error' => 'Please enter exhcange rate to buy'
            ]);
        }

        $user = Auth::user();

        $amount = $input['amount'];
        $rate = $input['rate'];

        $btcController = new BlockCypherController();
        $balance = $btcController->balance('BTC', $user);

        if ($rate * $amount > $balance) {
            return response()->json([
                'success' => false,
                'error' => 'Your BTC Balance is not enough to buy.'
            ]);
        }

        $rate = round($rate, 8);
        $amount = round($amount, 8);
        $buyAmount = 0;
        
        $settings = Setting::all()->pluck('value', 'name');

        $sellOrders = Order::where([['type', MarketOrderType::SELL], ['rate', $rate]])->orderBy('created_at')->get();

        foreach ($sellOrders as $order) {
            if ($order->amount > $amount) {
                $buyAmount += $amount;

                $feeAmount = round($order->rate * $amount * $settings['exchange_sell_fee'] / 100, 8);

                $btcTransaction = new ExchangeList();
                $btcTransaction->user_id = $order->user_id;
                $btcTransaction->date = Carbon::now();
                $btcTransaction->src_currency = 'TOKEN';
                $btcTransaction->src_amount = $amount;
                $btcTransaction->dest_currency = 'BTC';
                $btcTransaction->dest_amount = round($order->rate * $amount - $feeAmount, 8);
                $btcTransaction->rate = $order->rate;
                $btcTransaction->fee = $feeAmount;
                $btcTransaction->fee_rate = $settings['exchange_sell_fee'];
                $btcTransaction->type = TransactionType::EXCHANGE;
                $btcTransaction->exchange_type = MarketOrderType::SELL;
                $btcTransaction->status = TransactionStatus::SUCCESS;
                $btcTransaction->save();

                $order->amount = $order->amount - $amount;
                $order->save();
                $amount = 0;

                break;
            } else {
                $buyAmount += $order->amount;

                $feeAmount = round($order->rate * $order->amount * $settings['exchange_sell_fee'] / 100, 8);
                $btcTransaction = new ExchangeList();
                $btcTransaction->user_id = $order->user_id;
                $btcTransaction->datetime = Carbon::now();
                $btcTransaction->src_currency = 'TOKEN';
                $btcTransaction->src_amount = $order->amount;
                $btcTransaction->dest_currency = 'BTC';
                $btcTransaction->dest_amount = round($order->rate * $order->amount - $feeAmount, 8);
                $btcTransaction->rate = $order->rate;
                $btcTransaction->fee = $feeAmount;
                $btcTransaction->fee_rate = $settings['exchange_sell_fee'];
                $btcTransaction->type = TransactionType::EXCHANGE;
                $btcTransaction->exchange_type = MarketOrderType::SELL;
                $btcTransaction->status = TransactionStatus::SUCCESS;
                $btcTransaction->save();
                
                $order->delete();
                $amount = $amount - $order->amount;
            }
            if ($amount <= 0) break;
        }

        if ($buyAmount > 0) {
            $feeAmount = round($buyAmount * $settings['exchange_buy_fee'] / 100, 8);

            $btcTransaction = new ExchangeList();
            $btcTransaction->user_id = $user->id;
            $btcTransaction->datetime = Carbon::now();
            $btcTransaction->src_currency = 'BTC';
            $btcTransaction->src_amount = round($rate * $buyAmount, 8);
            $btcTransaction->dest_currency = 'TOKEN';
            $btcTransaction->dest_amount = round($buyAmount - $feeAmount, 8);
            $btcTransaction->rate = $rate;
            $btcTransaction->fee = $feeAmount;
            $btcTransaction->fee_rate = $settings['exchange_buy_fee'];
            $btcTransaction->type = TransactionType::EXCHANGE;
            $btcTransaction->exchange_type = MarketOrderType::BUY;
            $btcTransaction->status = TransactionStatus::SUCCESS;
            $btcTransaction->save();

        }

        if ($amount > 0) {
            $buyOrder = new Order();

            $buyOrder->type = MarketOrderType::BUY;
            $buyOrder->user_id = $user->id;
            $buyOrder->rate = $rate;
            $buyOrder->amount = $amount;

            $buyOrder->save();
        }

        return response()->json([
            'success' => true,
        ]);
    }

    public function sellOrder(Request $request) {
        $input = $request->input();

        if (!isset($input['amount'])) {
            return response()->json([
                'success' => false,
                'error' => 'Please enter amount to buy'
            ]);
        }

        if (!isset($input['rate'])) {
            return response()->json([
                'success' => false,
                'error' => 'Please enter exhcange rate to sell'
            ]);
        }

        $user = Auth::user();

        $amount = $input['amount'];
        $rate = $input['rate'];

        $tokenController = new TokenController();
        $balance = $tokenController->balance($user);

        if ($amount > $balance) {
            return response()->json([
                'success' => false,
                'error' => 'Your MyAltCoin Balance is not enough to sell.'
            ]);
        }

        $rate = round($rate, 8);
        $amount = round($amount, 8);

        $sellAmount = 0;
        $broadcast = array();

        $settings = Setting::all()->pluck('value', 'name');

        $sellOrders = Order::where([['type', MarketOrderType::BUY], ['rate', $rate]])->orderBy('created_at')->get();

        foreach ($sellOrders as $order) {
            if ($order->amount > $amount) {
                $sellAmount += $amount;

                $feeAmount = round($amount * $settings['exchange_buy_fee'] / 100, 8);

                $btcTransaction = new ExchangeList();
                $btcTransaction->user_id = $order->user_id;
                $btcTransaction->datetime = Carbon::now();
                $btcTransaction->src_currency = 'BTC';
                $btcTransaction->src_amount = round($rate * $amount, 8);
                $btcTransaction->dest_currency = 'TOKEN';
                $btcTransaction->dest_amount = round($amount - $feeAmount, 8);
                $btcTransaction->rate = $rate;
                $btcTransaction->fee = $feeAmount;
                $btcTransaction->fee_rate = $settings['exchange_buy_fee'];
                $btcTransaction->type = TransactionType::EXCHANGE;
                $btcTransaction->exchange_type = MarketOrderType::BUY;
                $btcTransaction->status = TransactionStatus::SUCCESS;
                $btcTransaction->save();

                $order->amount = $order->amount - $amount;
                $order->save();
                $amount = 0;

                break;
            } else {
                $sellAmount += $order->amount;

                $feeAmount = round($order->amount * $settings['exchange_buy_fee'] / 100, 8);

                $btcTransaction = new ExchangeList();
                $btcTransaction->user_id = $order->user_id;
                $btcTransaction->datetime = Carbon::now();
                $btcTransaction->src_currency = 'BTC';
                $btcTransaction->src_amount = round($rate * $order->amount, 8);
                $btcTransaction->dest_currency = 'TOKEN';
                $btcTransaction->dest_amount = round($order->amount - $feeAmount, 8);
                $btcTransaction->rate = $rate;
                $btcTransaction->fee = $feeAmount;
                $btcTransaction->fee_rate = $settings['exchange_buy_fee'];
                $btcTransaction->type = TransactionType::EXCHANGE;
                $btcTransaction->exchange_type = MarketOrderType::BUY;
                $btcTransaction->status = TransactionStatus::SUCCESS;
                $btcTransaction->save();

                $order->delete();
                $amount = $amount - $order->amount;
            }
            if ($amount <= 0) break;
        }

        if ($sellAmount > 0) {
            $feeAmount = round($rate * $sellAmount * $settings['exchange_sell_fee'] / 100, 2);

            $btcTransaction = new ExchangeList();
            $btcTransaction->user_id = $user->id;
            $btcTransaction->datetime = Carbon::now();
            $btcTransaction->src_currency = 'TOKEN';
            $btcTransaction->src_amount = $sellAmount;
            $btcTransaction->dest_currency = 'BTC';
            $btcTransaction->dest_amount = round($rate * $sellAmount - $feeAmount, 2);
            $btcTransaction->rate = $rate;
            $btcTransaction->fee = $feeAmount;
            $btcTransaction->fee_rate = $settings['btc_sell_fee'];
            $btcTransaction->type = TransactionType::EXCHANGE;
            $btcTransaction->exchange_type = MarketOrderType::SELL;
            $btcTransaction->status = TransactionStatus::SUCCESS;
            $btcTransaction->save();
        }

        if ($amount > 0) {
            $sellOrder = new Order();

            $sellOrder->type = MarketOrderType::SELL;
            $sellOrder->user_id = $user->id;
            $sellOrder->rate = $rate;
            $sellOrder->amount = $amount;

            $sellOrder->save();
        }

        return response()->json([
            'success' => true,
        ]);
    }

    public function getBuyOrder(Request $request) {
        $orders = Order::where('type', MarketOrderType::BUY)
            ->groupBy('rate')
            ->orderBy('rate', 'DESC')
            ->select('rate', \DB::raw('SUM(amount) AS amount'))
            ->get();

        return response()->json([
            'success' => true,
            'data' => $orders
        ]);
    }

    public function getSellOrder(Request $request) {
        $orders = Order::where('type', MarketOrderType::SELL)
            ->groupBy('rate')
            ->orderBy('rate', 'ASC')
            ->select('rate', \DB::raw('SUM(amount) AS amount'))
            ->get();

        return response()->json([
            'success' => true,
            'data' => $orders
        ]);
    }

    public function deleteOrder(Request $request) {
        $id = $request->input('id');

        $order = Order::find($id);

        if(!$order) {
            return response()->json([
                'success' => false,
                'error' => 'Order does not exist'
            ]);
        }

        $order->delete();

        return response()->json([
            'success' => true,
        ]);
    }

    public function getOpenOrder(Request $request) {
        $user = Auth::user();

        $orders = Order::where('user_id', $user->id)->orderBy('created_at')->get();

        return response()->json([
            'success' => true,
            'data' => $orders
        ]);
    }

    public function getPastOrder(Request $request) {
        $user = Auth::user();

        $orders = ExchangeList::where([['user_id', $user->id], ['type', TransactionType::EXCHANGE]])->orderBy('created_at', 'desc')->limit(20)->get();

        return response()->json([
            'success' => true,
            'data' => $orders
        ]);
    }
}
