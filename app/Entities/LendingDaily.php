<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class LendingDaily extends Model
{
    //
    protected $table = "lending_daily";
    public $timestamps = false;
}
