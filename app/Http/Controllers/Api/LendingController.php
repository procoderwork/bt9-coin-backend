<?php

namespace App\Http\Controllers\Api;

use App\Entities\BonusList;
use App\Entities\BtcAddress;
use App\Entities\EthAddress;
use App\Entities\ExchangeList;
use App\Entities\ICOAgenda;
use App\Entities\Lending;
use App\Entities\LendingDaily;
use App\Entities\LendingSetting;
use App\Entities\LoginHistory;
use App\Entities\ResetPassword;
use App\Entities\Setting;
use App\Entities\TokenRate;
use App\Entities\TransactionStatus;
use App\Entities\TransactionType;
use App\Events\SendEmailEvent;
use App\Libs\GoogleAuthenticator;
use App\Mail\ForgotPasswordMail;
use App\Mail\LoginNotificationMail;
use App\Mail\VerifyAccountMail;
use App\User;
use BlockCypher\Auth\SimpleTokenCredential;
use BlockCypher\Client\AddressClient;
use BlockCypher\Rest\ApiContext;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Symfony\Component\HttpFoundation\Exception\RequestExceptionInterface;
use tests\Simple;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;
use Jenssegers\Agent\Agent;

class LendingController extends Controller
{
    public function saveSetting(Request $request) {
        $input = $request->input();

        if (isset($input['id']) && $input['id'] > 0) {
            $setting = LendingSetting::find($input['id']);

            if (!$setting) {
                return response()->json([
                    'success' => false,
                    'error' => 'No Exist'
                ]);
            }
        } else {
            $setting = new LendingSetting();
        }

        $setting->min = $input['min'];
        $setting->max = $input['max'];
        $setting->interest = $input['interest'];
        $setting->daily = $input['daily'];
        $setting->after = $input['after'];
        $setting->save();

        return response()->json([
            'success' => true,
        ]);
    }

    public function getSetting(Request $request) {
        $settings = LendingSetting::orderBy('min', 'desc')->get();

        return response()->json([
            'success' => true,
            'data' => $settings
        ]);
    }

    public function deleteSetting(Request $request) {
        $input = $request->input();

        $id = $input['id'];
        $setting = LendingSetting::find($id);

        if (!$setting) {
            return response()->json([
                'success' => false,
                'error' => 'No Exist'
            ]);
        }
        $setting->delete();

        return response()->json([
            'success' => true,
        ]);
    }

    public function save(Request $request) {
        $input = $request->input();

        $user = Auth::user();

        $tokenController = new TokenController();
        $token_balance = $tokenController->balance($user);

        if ($input['token_amount'] > $token_balance) {
            return response()->json([
                'success' => false,
                'error' => 'Your BT9Coin balance is not enough to buy lending'
            ]);
        }

        $lending = new ExchangeList();
        $lending->user_id = $user->id;
        $lending->src_currency = 'TOKEN';
        $lending->src_amount = $input['token_amount'];
        $lending->dest_currency = 'LENDING';
        $lending->dest_amount = $input['amount'];
        $lending->date = Carbon::now();
        $lending->finish_date = Carbon::now()->addDay($input['after']);
        $lending->type = TransactionType::LENDING;
        $lending->status = TransactionStatus::SUCCESS;
        $lending->rate = $input['daily'];
        $lending->save();

        $settings = Setting::all()->pluck('value', 'name');
        if ($settings['lending_start_date'] && $settings['lending_start_date'] < Carbon::now()->toDateString()) {
            $parent_id = $user->parent;
            $bonus = BonusList::pluck('bonus');

            for ($i = 0; $i < 5; $i++) {
                if ($parent_id == 0 || $parent_id == null) {
                    break;
                }

                $parent_user = User::find($parent_id);

                $locked_amount = ExchangeList::where([['dest_currency', 'LENDING'], ['user_id', $parent_user->id], ['finish_date', '>', Carbon::now()]])->sum('dest_amount');

                if ($locked_amount > 0) {
                    $lending = new ExchangeList();
                    $lending->user_id = $user->id;
                    $lending->dest_currency = 'TOKEN';
                    $lending->dest_amount = $input['token_amount'] * $bonus[$i] / 100;
                    $lending->date = Carbon::now();
                    $lending->type = TransactionType::LENDING_BONUS;
                    $lending->status = TransactionStatus::SUCCESS;
                    $lending->rate = $input['daily'];
                    $lending->ref_id = $parent_id;
                    $lending->save();
                }

                $parent_id = $parent_user->parent;
            }
        }

        return response()->json([
            'success' => true,
        ]);
    }

    public function get(Request $request) {
        $input = $request->input();

        $user = Auth::user();
        $lendings = ExchangeList::where([['user_id', $user->id], ['type', TransactionType::LENDING]])->get();

        return response()->json([
            'success' => true,
            'data' => $lendings
        ]);
    }

    public function getAllLending(Request $request) {
        $input = $request->input();

        $user = Auth::user();
        $lendings = ExchangeList::where('type', TransactionType::LENDING)->leftJoin('users', 'users.id', 'exchange_lists.user_id')->select('exchange_lists.*', 'users.email')->get();

        return response()->json([
            'success' => true,
            'data' => $lendings
        ]);
    }

    public function getLendingBalance(Request $request) {
        $input = $request->input();

        $user = Auth::user();

        $locked_amount = ExchangeList::where([['dest_currency', 'LENDING'], ['user_id', $user->id], ['finish_date', '>', Carbon::now()]])->sum('dest_amount');

        $unlocked_amount = ExchangeList::where([['dest_currency', 'LENDING'], ['user_id', $user->id], ['finish_date', '<=', Carbon::now()]])->sum('dest_amount');
        $transfer_amount = ExchangeList::where([['src_currency', 'LENDING'], ['user_id', $user->id]])->sum('src_amount');

        return response()->json([
            'success' => true,
            'locked_amount' => $locked_amount,
            'unlocked_amount' => $unlocked_amount,
            'transfer_amount' => $transfer_amount,
        ]);
    }

    public function getDailyInterest(Request $request) {
        $input = $request->input();

        // $this->fillDaily();
        $interests = LendingDaily::all()->pluck('value', 'date');

        $user = Auth::user();

        $amount = 0;

        $lendings = ExchangeList::where([['user_id', $user->id], ['type', TransactionType::LENDING]])->get();
        foreach ($lendings as $lending) {
            $date = new Carbon($lending->date);
            $finish_date = new Carbon($lending->finish_date);
            $now = Carbon::now();

            $date->addDay(1);
            while ($date <= $finish_date && $date <= $now) {
                $daily = $interests[$date->toDateString()];
                $amount += $lending->dest_amount * ($daily + $lending->rate) * 0.01;
                $date->addDay(1);
            }
        }

        $transfer_amount = ExchangeList::where([['src_currency', 'LENDING_DAILY'], ['user_id', $user->id], ['type', TransactionType::LENDING_TRANSFER]])->sum('src_amount');

        return response()->json([
            'success' => true,
            'amount' => $amount,
            'transfer_amount' => $transfer_amount,
        ]);
    }

    public function transferLending(Request $request) {
        $input = $request->input();
        $user = Auth::user();

        $amount = $input['amount'];
        $token_amount = $input['token_amount'];

        if ($amount == 0) {
            return response()->json([
                'success' => false,
                'error' => "Please enter amount."
            ]);
        }

        $exchangeList = new ExchangeList();
        $exchangeList->user_id = $user->id;
        $exchangeList->src_currency = 'LENDING';
        $exchangeList->src_amount = $amount;
        $exchangeList->dest_currency = 'TOKEN';
        $exchangeList->dest_amount = $token_amount;
        $exchangeList->date = Carbon::now();
        $exchangeList->type = TransactionType::LENDING_TRANSFER;
        $exchangeList->status = TransactionStatus::SUCCESS;
        $exchangeList->rate = isset($input['rate']) ? $input['rate'] : '';
        $exchangeList->save();

        return response()->json([
            'success' => true,
        ]);
    }

    public function transaferDailyInterest(Request $request) {
        $input = $request->input();
        $user = Auth::user();

        $amount = $input['amount'];
        $token_amount = $input['token_amount'];

        if ($amount == 0) {
            return response()->json([
                'success' => false,
                'error' => "Please enter amount."
            ]);
        }

        $exchangeList = new ExchangeList();
        $exchangeList->user_id = $user->id;
        $exchangeList->src_currency = 'LENDING_DAILY';
        $exchangeList->src_amount = $amount;
        $exchangeList->dest_currency = 'TOKEN';
        $exchangeList->dest_amount = $token_amount;
        $exchangeList->date = Carbon::now();
        $exchangeList->type = TransactionType::LENDING_TRANSFER;
        $exchangeList->status = TransactionStatus::SUCCESS;
        $exchangeList->rate = isset($input['rate']) ? $input['rate'] : '';
        $exchangeList->save();

        $settings = Setting::all()->pluck('value', 'name');

        if ($settings['lending_start_date'] && $settings['lending_start_date'] < Carbon::now()->toDateString()) {
            $parent_id = $user->parent;
            $bonus = BonusList::pluck('bonus');

            for ($i = 0; $i < 5; $i++) {
                if ($parent_id == 0 || $parent_id == null) {
                    break;
                }

                $parent_user = User::find($parent_id);

                $locked_amount = ExchangeList::where([['dest_currency', 'LENDING'], ['user_id', $parent_user->id], ['finish_date', '>', Carbon::now()]])->sum('dest_amount');

                if ($locked_amount > 0) {
                    $lending = new ExchangeList();
                    $lending->user_id = $user->id;
                    $lending->dest_currency = 'TOKEN';
                    $lending->dest_amount = $token_amount * $bonus[$i] / 100;
                    $lending->date = Carbon::now();
                    $lending->type = TransactionType::LENDING_BONUS;
                    $lending->status = TransactionStatus::SUCCESS;
                    $lending->rate = $input['daily'];
                    $lending->ref_id = $parent_id;
                    $lending->save();
                }

                $parent_id = $parent_user->parent;
            }
        }

        return response()->json([
            'success' => true,
        ]);
    }

    public function getTx(Request $request) {
        $input = $request->input();
        $user = Auth::user();

        $lendings = ExchangeList::where([['type', TransactionType::LENDING], ['user_id', $user->id], ['date', '<=', Carbon::now()]])->get();
        $transfers = ExchangeList::where([['type', TransactionType::LENDING_TRANSFER], ['user_id', $user->id], ['date', '<=', Carbon::now()]])->get();

        $data = array();
        if (count($lendings) > 0) {
            foreach ($lendings as $list) {
                $list->category = 'lending';
                array_push($data, $list);
            }
        }

        if (count($transfers) > 0) {
            foreach ($transfers as $list) {
                $list->category = 'transfer';
                array_push($data, $list);
            }
        }

        return response()->json([
            'success' => true,
            'data' => $data,
        ]);
    }

    public function fillDaily() {
        $max_date = LendingDaily::max('date');
        $settings = Setting::all()->pluck('value', 'name');

        $min = $settings['lending_interest_min'];
        $max = $settings['lending_interest_max'];

        if ($max_date == null) {
            $date = Carbon::create(2018, 1, 1, 0, 0, 0);
        } else {
            $date = Carbon::createFromFormat('Y-m-d', $max_date);
            $date->setTime(0, 0, 0);
        }

        $date->addDay(1);

        $now = Carbon::now();
        while ($date <= $now) {
            $interest = new LendingDaily();
            $interest->date = $date->toDateString();
            $interest->value = rand($min * 100, $max * 100) / 100;
            $interest->save();

            $date->addDay(1);
        }
    }

    public function getInterest(Request $request) {
        $input = $request->input();

        $this->fillDaily();

        $data = LendingDaily::orderBy('date', 'desc')->limit(6)->get();
        return response()->json([
            'success' => true,
            'data' => $data,
        ]);
    }

    public function releaseAll(Request $request) {
        $input = $request->input();

        $this->fillDaily();

        $interests = LendingDaily::all()->pluck('value', 'date');

        $amount = 0;

        $tokenController = new TokenController();
        $token_usd = $tokenController->currentRate();

        $users = User::where('role', '<>', 'admin')->orWhereNull('role')->get();

        foreach ($users as $user) {
            $locked_amount = ExchangeList::where([['dest_currency', 'LENDING'], ['user_id', $user->id], ['finish_date', '>', Carbon::now()]])->sum('dest_amount');
            $unlocked_amount = ExchangeList::where([['dest_currency', 'LENDING'], ['user_id', $user->id], ['finish_date', '<=', Carbon::now()]])->sum('dest_amount');
            $transfer_amount = ExchangeList::where([['src_currency', 'LENDING'], ['user_id', $user->id]])->sum('src_amount');

            $available_amount = $locked_amount + $unlocked_amount  - $transfer_amount;


            if ($available_amount > 0) {
                $exchangeList = new ExchangeList();
                $exchangeList->user_id = $user->id;
                $exchangeList->src_currency = 'LENDING';
                $exchangeList->src_amount = $available_amount;
                $exchangeList->dest_currency = 'TOKEN';
                $exchangeList->dest_amount = $available_amount / $token_usd;
                $exchangeList->date = Carbon::now();
                $exchangeList->type = TransactionType::LENDING_TRANSFER;
                $exchangeList->status = TransactionStatus::SUCCESS;
                $exchangeList->rate = $token_usd;
                $exchangeList->save();
            }

            $daily_amount = 0;
            $lendings = ExchangeList::where([['user_id', $user->id], ['type', TransactionType::LENDING]])->get();
            foreach ($lendings as $lending) {
                $date = new Carbon($lending->date);
                $finish_date = new Carbon($lending->finish_date);
                $now = Carbon::now();

                $date->addDay(1);
                while ($date <= $finish_date && $date <= $now) {
                    $daily = $interests[$date->toDateString()];
                    $daily_amount += $lending->dest_amount * ($daily + $lending->rate) * 0.01;
                    $date->addDay(1);
                }
            }

            $transfer_amount = ExchangeList::where([['src_currency', 'LENDING_DAILY'], ['user_id', $user->id], ['type', TransactionType::LENDING_TRANSFER]])->sum('src_amount');

            $available_amount = $daily_amount - $transfer_amount;

            if ($available_amount > 0) {
                $exchangeList = new ExchangeList();
                $exchangeList->user_id = $user->id;
                $exchangeList->src_currency = 'LENDING_DAILY';
                $exchangeList->src_amount = $available_amount;
                $exchangeList->dest_currency = 'TOKEN';
                $exchangeList->dest_amount = $available_amount / $token_usd;
                $exchangeList->date = Carbon::now();
                $exchangeList->type = TransactionType::LENDING_TRANSFER;
                $exchangeList->status = TransactionStatus::SUCCESS;
                $exchangeList->rate = $token_usd;
                $exchangeList->save();
            }
        }

        $query = "UPDATE exchange_lists SET finish_date = '".Carbon::now()->toDateTimeString()."' WHERE type = ".TransactionType::LENDING." AND finish_date > '".Carbon::now()->toDateTimeString()."'";
        $result = \DB::update($query);

        return response()->json([
            'success' => true,
        ]);
    }
}
