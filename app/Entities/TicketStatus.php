<?php

namespace App\Entities;

class TicketStatus {
    const OPEN = 0;
    const PENDING = 1;
    const CLOSED = 2;
}
