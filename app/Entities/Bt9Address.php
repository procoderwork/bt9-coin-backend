<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Bt9Address extends Model
{
    //
    protected $table = 'bt9_address';
}
