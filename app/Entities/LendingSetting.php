<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class LendingSetting extends Model
{
    //
    protected $table = "lending_settings";
    public $timestamps = false;
}
