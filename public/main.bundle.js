webpackJsonp(["main"],{

/***/ "../../../../../src/$$_gendir lazy recursive":
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./admin/admin.module": [
		"../../../../../src/app/admin/admin.module.ts",
		"admin.module"
	],
	"./pages/dashboard/dashboard.module": [
		"../../../../../src/app/pages/dashboard/dashboard.module.ts",
		"dashboard.module"
	],
	"./pages/exchange/exchange.module": [
		"../../../../../src/app/pages/exchange/exchange.module.ts",
		"exchange.module"
	],
	"./pages/ico/ico.module": [
		"../../../../../src/app/pages/ico/ico.module.ts",
		"ico.module"
	],
	"./pages/lending/lending.module": [
		"../../../../../src/app/pages/lending/lending.module.ts",
		"lending.module"
	],
	"./pages/referral/referral.module": [
		"../../../../../src/app/pages/referral/referral.module.ts",
		"referral.module"
	],
	"./pages/security/security.module": [
		"../../../../../src/app/pages/security/security.module.ts",
		"security.module"
	],
	"./pages/staking/staking.module": [
		"../../../../../src/app/pages/staking/staking.module.ts",
		"staking.module"
	],
	"./pages/support/support.module": [
		"../../../../../src/app/pages/support/support.module.ts",
		"support.module"
	],
	"./pages/transaction/transaction.module": [
		"../../../../../src/app/pages/transaction/transaction.module.ts",
		"transaction.module"
	],
	"./pages/wallet/wallet.module": [
		"../../../../../src/app/pages/wallet/wallet.module.ts",
		"wallet.module"
	],
	"./ticket/ticket.module": [
		"../../../../../src/app/admin/ticket/ticket.module.ts",
		"ticket.module"
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "../../../../../src/$$_gendir lazy recursive";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "../../../../../src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<loading *ngIf=\"settings.loading\"></loading>\n<router-outlet></router-outlet>\n"

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_filter__ = __webpack_require__("../../../../rxjs/add/operator/filter.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_filter___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_filter__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_platform_browser__ = __webpack_require__("../../../platform-browser/@angular/platform-browser.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_settings_settings_service__ = __webpack_require__("../../../../../src/app/services/settings/settings.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};





var AppComponent = (function () {
    function AppComponent(router, document, settings) {
        this.router = router;
        this.document = document;
        this.settings = settings;
    }
    AppComponent.prototype.ngOnInit = function () {
        $.material.options.autofill = true;
        $.material.init();
        this._router = this.router.events.filter(function (event) { return event instanceof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* NavigationEnd */]; }).subscribe(function (event) {
            if (window.outerWidth > 991) {
                window.document.children[0].scrollTop = 0;
            }
            else {
                window.document.activeElement.scrollTop = 0;
            }
        });
    };
    AppComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-my-app',
            template: __webpack_require__("../../../../../src/app/app.component.html")
        }),
        __param(1, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Inject"])(__WEBPACK_IMPORTED_MODULE_3__angular_platform_browser__["b" /* DOCUMENT */])),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]) === "function" && _a || Object, Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_4__services_settings_settings_service__["a" /* SettingsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__services_settings_settings_service__["a" /* SettingsService */]) === "function" && _b || Object])
    ], AppComponent);
    return AppComponent;
    var _a, _b;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return MaterialModule; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_animations__ = __webpack_require__("../../../platform-browser/@angular/platform-browser/animations.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__sidebar_sidebar_module__ = __webpack_require__("../../../../../src/app/sidebar/sidebar.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__layouts_auth_auth_layout_component__ = __webpack_require__("../../../../../src/app/layouts/auth/auth-layout.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__app_routing__ = __webpack_require__("../../../../../src/app/app.routing.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__login_login_component__ = __webpack_require__("../../../../../src/app/login/login.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__register_register_component__ = __webpack_require__("../../../../../src/app/register/register.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__shared_shared_module__ = __webpack_require__("../../../../../src/app/shared/shared.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__services_services_module__ = __webpack_require__("../../../../../src/app/services/services.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__register_sponser_resolver_service__ = __webpack_require__("../../../../../src/app/register/sponser-resolver.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16_ng_treetable__ = __webpack_require__("../../../../ng-treetable/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17_angular2_recaptcha__ = __webpack_require__("../../../../angular2-recaptcha/angular2-recaptcha.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17_angular2_recaptcha___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_17_angular2_recaptcha__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__reset_password_reset_password_component__ = __webpack_require__("../../../../../src/app/reset_password/reset_password.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__reset_password_reset_password_resolver_service__ = __webpack_require__("../../../../../src/app/reset_password/reset_password-resolver.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__layouts_admin_admin_layout_component__ = __webpack_require__("../../../../../src/app/layouts/admin/admin-layout.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

 // this is needed!



















var MaterialModule = (function () {
    function MaterialModule() {
    }
    MaterialModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            exports: [
                __WEBPACK_IMPORTED_MODULE_6__angular_material__["a" /* MatAutocompleteModule */],
                __WEBPACK_IMPORTED_MODULE_6__angular_material__["b" /* MatButtonModule */],
                __WEBPACK_IMPORTED_MODULE_6__angular_material__["c" /* MatButtonToggleModule */],
                __WEBPACK_IMPORTED_MODULE_6__angular_material__["d" /* MatCardModule */],
                __WEBPACK_IMPORTED_MODULE_6__angular_material__["e" /* MatCheckboxModule */],
                __WEBPACK_IMPORTED_MODULE_6__angular_material__["f" /* MatChipsModule */],
                __WEBPACK_IMPORTED_MODULE_6__angular_material__["A" /* MatStepperModule */],
                __WEBPACK_IMPORTED_MODULE_6__angular_material__["g" /* MatDatepickerModule */],
                __WEBPACK_IMPORTED_MODULE_6__angular_material__["h" /* MatDialogModule */],
                __WEBPACK_IMPORTED_MODULE_6__angular_material__["i" /* MatExpansionModule */],
                __WEBPACK_IMPORTED_MODULE_6__angular_material__["j" /* MatGridListModule */],
                __WEBPACK_IMPORTED_MODULE_6__angular_material__["k" /* MatIconModule */],
                __WEBPACK_IMPORTED_MODULE_6__angular_material__["l" /* MatInputModule */],
                __WEBPACK_IMPORTED_MODULE_6__angular_material__["m" /* MatListModule */],
                __WEBPACK_IMPORTED_MODULE_6__angular_material__["n" /* MatMenuModule */],
                __WEBPACK_IMPORTED_MODULE_6__angular_material__["o" /* MatNativeDateModule */],
                __WEBPACK_IMPORTED_MODULE_6__angular_material__["p" /* MatPaginatorModule */],
                __WEBPACK_IMPORTED_MODULE_6__angular_material__["q" /* MatProgressBarModule */],
                __WEBPACK_IMPORTED_MODULE_6__angular_material__["r" /* MatProgressSpinnerModule */],
                __WEBPACK_IMPORTED_MODULE_6__angular_material__["s" /* MatRadioModule */],
                __WEBPACK_IMPORTED_MODULE_6__angular_material__["t" /* MatRippleModule */],
                __WEBPACK_IMPORTED_MODULE_6__angular_material__["u" /* MatSelectModule */],
                __WEBPACK_IMPORTED_MODULE_6__angular_material__["v" /* MatSidenavModule */],
                __WEBPACK_IMPORTED_MODULE_6__angular_material__["x" /* MatSliderModule */],
                __WEBPACK_IMPORTED_MODULE_6__angular_material__["w" /* MatSlideToggleModule */],
                __WEBPACK_IMPORTED_MODULE_6__angular_material__["y" /* MatSnackBarModule */],
                __WEBPACK_IMPORTED_MODULE_6__angular_material__["z" /* MatSortModule */],
                __WEBPACK_IMPORTED_MODULE_6__angular_material__["B" /* MatTableModule */],
                __WEBPACK_IMPORTED_MODULE_6__angular_material__["C" /* MatTabsModule */],
                __WEBPACK_IMPORTED_MODULE_6__angular_material__["D" /* MatToolbarModule */],
                __WEBPACK_IMPORTED_MODULE_6__angular_material__["E" /* MatTooltipModule */]
            ]
        })
    ], MaterialModule);
    return MaterialModule;
}());

var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_5__angular_common__["b" /* CommonModule */],
                __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_animations__["a" /* BrowserAnimationsModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_forms__["FormsModule"],
                __WEBPACK_IMPORTED_MODULE_2__angular_router__["d" /* RouterModule */].forRoot(__WEBPACK_IMPORTED_MODULE_10__app_routing__["a" /* AppRoutes */]),
                __WEBPACK_IMPORTED_MODULE_3__angular_http__["d" /* HttpModule */],
                MaterialModule,
                __WEBPACK_IMPORTED_MODULE_6__angular_material__["o" /* MatNativeDateModule */],
                __WEBPACK_IMPORTED_MODULE_8__sidebar_sidebar_module__["a" /* SidebarModule */],
                __WEBPACK_IMPORTED_MODULE_13__shared_shared_module__["a" /* SharedModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_14__services_services_module__["a" /* ServicesModule */],
                __WEBPACK_IMPORTED_MODULE_16_ng_treetable__["a" /* TreeTableModule */],
                __WEBPACK_IMPORTED_MODULE_17_angular2_recaptcha__["ReCaptchaModule"]
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_7__app_component__["a" /* AppComponent */],
                __WEBPACK_IMPORTED_MODULE_9__layouts_auth_auth_layout_component__["a" /* AuthLayoutComponent */],
                __WEBPACK_IMPORTED_MODULE_20__layouts_admin_admin_layout_component__["a" /* AdminLayoutComponent */],
                __WEBPACK_IMPORTED_MODULE_11__login_login_component__["a" /* LoginComponent */],
                __WEBPACK_IMPORTED_MODULE_12__register_register_component__["a" /* RegisterComponent */],
                __WEBPACK_IMPORTED_MODULE_18__reset_password_reset_password_component__["a" /* ResetPasswordComponent */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_15__register_sponser_resolver_service__["a" /* SponserResolver */],
                __WEBPACK_IMPORTED_MODULE_19__reset_password_reset_password_resolver_service__["a" /* ResetPasswordResolver */]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_7__app_component__["a" /* AppComponent */]]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ "../../../../../src/app/app.routing.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppRoutes; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__layouts_admin_admin_layout_component__ = __webpack_require__("../../../../../src/app/layouts/admin/admin-layout.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__login_login_component__ = __webpack_require__("../../../../../src/app/login/login.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__register_register_component__ = __webpack_require__("../../../../../src/app/register/register.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_authguard_service__ = __webpack_require__("../../../../../src/app/services/authguard.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__register_sponser_resolver_service__ = __webpack_require__("../../../../../src/app/register/sponser-resolver.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__layouts_auth_auth_layout_component__ = __webpack_require__("../../../../../src/app/layouts/auth/auth-layout.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__reset_password_reset_password_component__ = __webpack_require__("../../../../../src/app/reset_password/reset_password.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__reset_password_reset_password_resolver_service__ = __webpack_require__("../../../../../src/app/reset_password/reset_password-resolver.service.ts");








var AppRoutes = [
    {
        path: '',
        component: __WEBPACK_IMPORTED_MODULE_5__layouts_auth_auth_layout_component__["a" /* AuthLayoutComponent */],
        children: [
            {
                path: '',
                loadChildren: './pages/dashboard/dashboard.module#DashboardModule'
            },
            {
                path: 'dashboard',
                loadChildren: './pages/dashboard/dashboard.module#DashboardModule'
            },
            {
                path: 'ico',
                loadChildren: './pages/ico/ico.module#IcoModule'
            },
            {
                path: 'wallet',
                loadChildren: './pages/wallet/wallet.module#WalletModule'
            },
            {
                path: 'transaction',
                loadChildren: './pages/transaction/transaction.module#TransactionModule'
            },
            {
                path: 'exchange',
                loadChildren: './pages/exchange/exchange.module#ExchangeModule'
            },
            {
                path: 'lending',
                loadChildren: './pages/lending/lending.module#LendingModule'
            },
            {
                path: 'staking',
                loadChildren: './pages/staking/staking.module#StakingModule'
            },
            {
                path: 'referral',
                loadChildren: './pages/referral/referral.module#ReferralModule'
            },
            {
                path: 'security',
                loadChildren: './pages/security/security.module#SecurityModule'
            },
            {
                path: 'support',
                loadChildren: './pages/support/support.module#SupportModule'
            },
        ],
        resolve: {
            user: __WEBPACK_IMPORTED_MODULE_3__services_authguard_service__["a" /* AuthGuard */]
        }
    },
    {
        path: 'admin',
        component: __WEBPACK_IMPORTED_MODULE_0__layouts_admin_admin_layout_component__["a" /* AdminLayoutComponent */],
        children: [
            {
                path: '',
                loadChildren: './admin/admin.module#AdminModule'
            },
        ],
        resolve: {
            AuthGuard: __WEBPACK_IMPORTED_MODULE_3__services_authguard_service__["a" /* AuthGuard */]
        }
    },
    { path: 'login', component: __WEBPACK_IMPORTED_MODULE_1__login_login_component__["a" /* LoginComponent */] },
    { path: 'login/confirmed', component: __WEBPACK_IMPORTED_MODULE_1__login_login_component__["a" /* LoginComponent */] },
    {
        path: 'reset_password/:code',
        component: __WEBPACK_IMPORTED_MODULE_6__reset_password_reset_password_component__["a" /* ResetPasswordComponent */],
        resolve: {
            user_id: __WEBPACK_IMPORTED_MODULE_7__reset_password_reset_password_resolver_service__["a" /* ResetPasswordResolver */]
        }
    },
    { path: 'register', component: __WEBPACK_IMPORTED_MODULE_2__register_register_component__["a" /* RegisterComponent */] },
    {
        path: 'ref/:sponser',
        component: __WEBPACK_IMPORTED_MODULE_2__register_register_component__["a" /* RegisterComponent */],
        resolve: {
            sponser: __WEBPACK_IMPORTED_MODULE_4__register_sponser_resolver_service__["a" /* SponserResolver */]
        }
    },
    { path: '**', redirectTo: 'dashboard' }
];
//# sourceMappingURL=app.routing.js.map

/***/ }),

/***/ "../../../../../src/app/layouts/admin/admin-layout.component.html":
/***/ (function(module, exports) {

module.exports = "\n<div class=\"wrapper\">\n    <div class=\"sidebar\" data-active-color=\"blue\" data-background-color=\"black\" data-image=\"../assets/img/sidebar-2.jpg\">\n        <app-sidebar-cmp></app-sidebar-cmp>\n        <div class=\"sidebar-background\" style=\"background-image: url(assets/img/sidebar-1.jpg)\"></div>\n    </div>\n    <div class=\"main-panel\">\n        <app-navbar-cmp></app-navbar-cmp>\n        <router-outlet></router-outlet>\n        <app-footer-cmp></app-footer-cmp>\n    </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/layouts/admin/admin-layout.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminLayoutComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__md_md_module__ = __webpack_require__("../../../../../src/app/md/md.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_filter__ = __webpack_require__("../../../../rxjs/add/operator/filter.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_filter___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_filter__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__shared_navbar_navbar_component__ = __webpack_require__("../../../../../src/app/shared/navbar/navbar.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_perfect_scrollbar__ = __webpack_require__("../../../../perfect-scrollbar/dist/perfect-scrollbar.esm.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var AdminLayoutComponent = (function () {
    function AdminLayoutComponent(router, location) {
        this.router = router;
        this.location = location;
    }
    AdminLayoutComponent.prototype.ngOnInit = function () {
        var _this = this;
        var elemMainPanel = document.querySelector('.main-panel');
        var elemSidebar = document.querySelector('.sidebar .sidebar-wrapper');
        if (window.matchMedia("(min-width: 960px)").matches && !this.isMac()) {
            var ps = new __WEBPACK_IMPORTED_MODULE_6_perfect_scrollbar__["a" /* default */](elemMainPanel, { wheelSpeed: 2, suppressScrollX: true });
            ps = new __WEBPACK_IMPORTED_MODULE_6_perfect_scrollbar__["a" /* default */](elemSidebar, { wheelSpeed: 2, suppressScrollX: true });
        }
        this._router = this.router.events.filter(function (event) { return event instanceof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* NavigationEnd */]; }).subscribe(function (event) {
            _this.navbar.sidebarClose();
        });
        this.navItems = [
            { type: __WEBPACK_IMPORTED_MODULE_2__md_md_module__["b" /* NavItemType */].NavbarLeft, title: 'Dashboard', iconClass: 'fa fa-dashboard' },
            {
                type: __WEBPACK_IMPORTED_MODULE_2__md_md_module__["b" /* NavItemType */].NavbarRight,
                title: '',
                iconClass: 'fa fa-bell-o',
                numNotifications: 5,
                dropdownItems: [
                    { title: 'Notification 1' },
                    { title: 'Notification 2' },
                    { title: 'Notification 3' },
                    { title: 'Notification 4' },
                    { title: 'Another Notification' }
                ]
            },
            {
                type: __WEBPACK_IMPORTED_MODULE_2__md_md_module__["b" /* NavItemType */].NavbarRight,
                title: '',
                iconClass: 'fa fa-list',
                dropdownItems: [
                    { iconClass: 'pe-7s-mail', title: 'Messages' },
                    { iconClass: 'pe-7s-help1', title: 'Help Center' },
                    { iconClass: 'pe-7s-tools', title: 'Settings' },
                    'separator',
                    { iconClass: 'pe-7s-lock', title: 'Lock Screen' },
                    { iconClass: 'pe-7s-close-circle', title: 'Log Out' }
                ]
            },
            { type: __WEBPACK_IMPORTED_MODULE_2__md_md_module__["b" /* NavItemType */].NavbarLeft, title: 'Search', iconClass: 'fa fa-search' },
            { type: __WEBPACK_IMPORTED_MODULE_2__md_md_module__["b" /* NavItemType */].NavbarLeft, title: 'Account' },
            {
                type: __WEBPACK_IMPORTED_MODULE_2__md_md_module__["b" /* NavItemType */].NavbarLeft,
                title: 'Dropdown',
                dropdownItems: [
                    { title: 'Action' },
                    { title: 'Another action' },
                    { title: 'Something' },
                    { title: 'Another action' },
                    { title: 'Something' },
                    'separator',
                    { title: 'Separated link' },
                ]
            },
            { type: __WEBPACK_IMPORTED_MODULE_2__md_md_module__["b" /* NavItemType */].NavbarLeft, title: 'Log out' }
        ];
    };
    AdminLayoutComponent.prototype.ngAfterViewInit = function () {
        this.runOnRouteChange();
    };
    AdminLayoutComponent.prototype.isMap = function () {
        if (this.location.prepareExternalUrl(this.location.path()) === '/maps/fullscreen') {
            return true;
        }
        else {
            return false;
        }
    };
    AdminLayoutComponent.prototype.runOnRouteChange = function () {
        if (window.matchMedia("(min-width: 960px)").matches && !this.isMac()) {
            var elemMainPanel = document.querySelector('.main-panel');
            var ps = new __WEBPACK_IMPORTED_MODULE_6_perfect_scrollbar__["a" /* default */](elemMainPanel);
            ps.update();
        }
    };
    AdminLayoutComponent.prototype.isMac = function () {
        var bool = false;
        if (navigator.platform.toUpperCase().indexOf('MAC') >= 0 || navigator.platform.toUpperCase().indexOf('IPAD') >= 0) {
            bool = true;
        }
        return bool;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('sidebar'),
        __metadata("design:type", Object)
    ], AdminLayoutComponent.prototype, "sidebar", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_5__shared_navbar_navbar_component__["a" /* NavbarComponent */]),
        __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_5__shared_navbar_navbar_component__["a" /* NavbarComponent */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__shared_navbar_navbar_component__["a" /* NavbarComponent */]) === "function" && _a || Object)
    ], AdminLayoutComponent.prototype, "navbar", void 0);
    AdminLayoutComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-layout',
            template: __webpack_require__("../../../../../src/app/layouts/admin/admin-layout.component.html")
        }),
        __metadata("design:paramtypes", [typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__angular_common__["f" /* Location */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_common__["f" /* Location */]) === "function" && _c || Object])
    ], AdminLayoutComponent);
    return AdminLayoutComponent;
    var _a, _b, _c;
}());

//# sourceMappingURL=admin-layout.component.js.map

/***/ }),

/***/ "../../../../../src/app/layouts/auth/auth-layout.component.html":
/***/ (function(module, exports) {

module.exports = "\n<div class=\"wrapper\">\n  <div class=\"sidebar\" data-active-color=\"blue\" data-background-color=\"black\" data-image=\"../assets/img/sidebar-1.jpg\">\n    <app-sidebar-cmp></app-sidebar-cmp>\n    <div class=\"sidebar-background\" style=\"background-image: url(assets/img/sidebar-1.jpg)\"></div>\n  </div>\n  <div class=\"main-panel\">\n    <app-navbar-cmp></app-navbar-cmp>\n    <router-outlet></router-outlet>\n    <app-footer-cmp></app-footer-cmp>\n  </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/layouts/auth/auth-layout.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthLayoutComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__md_md_module__ = __webpack_require__("../../../../../src/app/md/md.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_filter__ = __webpack_require__("../../../../rxjs/add/operator/filter.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_filter___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_filter__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__shared_navbar_navbar_component__ = __webpack_require__("../../../../../src/app/shared/navbar/navbar.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_perfect_scrollbar__ = __webpack_require__("../../../../perfect-scrollbar/dist/perfect-scrollbar.esm.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var AuthLayoutComponent = (function () {
    function AuthLayoutComponent(router, location) {
        this.router = router;
        this.location = location;
    }
    AuthLayoutComponent.prototype.ngOnInit = function () {
        var _this = this;
        var elemMainPanel = document.querySelector('.main-panel');
        var elemSidebar = document.querySelector('.sidebar .sidebar-wrapper');
        if (window.matchMedia("(min-width: 960px)").matches && !this.isMac()) {
            var ps = new __WEBPACK_IMPORTED_MODULE_6_perfect_scrollbar__["a" /* default */](elemMainPanel, { wheelSpeed: 2, suppressScrollX: true });
            ps = new __WEBPACK_IMPORTED_MODULE_6_perfect_scrollbar__["a" /* default */](elemSidebar, { wheelSpeed: 2, suppressScrollX: true });
        }
        this._router = this.router.events.filter(function (event) { return event instanceof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* NavigationEnd */]; }).subscribe(function (event) {
            _this.navbar.sidebarClose();
        });
        this.navItems = [
            { type: __WEBPACK_IMPORTED_MODULE_2__md_md_module__["b" /* NavItemType */].NavbarLeft, title: 'Dashboard', iconClass: 'fa fa-dashboard' },
            {
                type: __WEBPACK_IMPORTED_MODULE_2__md_md_module__["b" /* NavItemType */].NavbarRight,
                title: '',
                iconClass: 'fa fa-bell-o',
                numNotifications: 5,
                dropdownItems: [
                    { title: 'Notification 1' },
                    { title: 'Notification 2' },
                    { title: 'Notification 3' },
                    { title: 'Notification 4' },
                    { title: 'Another Notification' }
                ]
            },
            {
                type: __WEBPACK_IMPORTED_MODULE_2__md_md_module__["b" /* NavItemType */].NavbarRight,
                title: '',
                iconClass: 'fa fa-list',
                dropdownItems: [
                    { iconClass: 'pe-7s-mail', title: 'Messages' },
                    { iconClass: 'pe-7s-help1', title: 'Help Center' },
                    { iconClass: 'pe-7s-tools', title: 'Settings' },
                    'separator',
                    { iconClass: 'pe-7s-lock', title: 'Lock Screen' },
                    { iconClass: 'pe-7s-close-circle', title: 'Log Out' }
                ]
            },
            { type: __WEBPACK_IMPORTED_MODULE_2__md_md_module__["b" /* NavItemType */].NavbarLeft, title: 'Search', iconClass: 'fa fa-search' },
            { type: __WEBPACK_IMPORTED_MODULE_2__md_md_module__["b" /* NavItemType */].NavbarLeft, title: 'Account' },
            {
                type: __WEBPACK_IMPORTED_MODULE_2__md_md_module__["b" /* NavItemType */].NavbarLeft,
                title: 'Dropdown',
                dropdownItems: [
                    { title: 'Action' },
                    { title: 'Another action' },
                    { title: 'Something' },
                    { title: 'Another action' },
                    { title: 'Something' },
                    'separator',
                    { title: 'Separated link' },
                ]
            },
            { type: __WEBPACK_IMPORTED_MODULE_2__md_md_module__["b" /* NavItemType */].NavbarLeft, title: 'Log out' }
        ];
    };
    AuthLayoutComponent.prototype.ngAfterViewInit = function () {
        this.runOnRouteChange();
    };
    AuthLayoutComponent.prototype.isMap = function () {
        if (this.location.prepareExternalUrl(this.location.path()) === '/maps/fullscreen') {
            return true;
        }
        else {
            return false;
        }
    };
    AuthLayoutComponent.prototype.runOnRouteChange = function () {
        if (window.matchMedia("(min-width: 960px)").matches && !this.isMac()) {
            var elemMainPanel = document.querySelector('.main-panel');
            var ps = new __WEBPACK_IMPORTED_MODULE_6_perfect_scrollbar__["a" /* default */](elemMainPanel);
            ps.update();
        }
    };
    AuthLayoutComponent.prototype.isMac = function () {
        var bool = false;
        if (navigator.platform.toUpperCase().indexOf('MAC') >= 0 || navigator.platform.toUpperCase().indexOf('IPAD') >= 0) {
            bool = true;
        }
        return bool;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('sidebar'),
        __metadata("design:type", Object)
    ], AuthLayoutComponent.prototype, "sidebar", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_5__shared_navbar_navbar_component__["a" /* NavbarComponent */]),
        __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_5__shared_navbar_navbar_component__["a" /* NavbarComponent */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__shared_navbar_navbar_component__["a" /* NavbarComponent */]) === "function" && _a || Object)
    ], AuthLayoutComponent.prototype, "navbar", void 0);
    AuthLayoutComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-auth-layout',
            template: __webpack_require__("../../../../../src/app/layouts/auth/auth-layout.component.html")
        }),
        __metadata("design:paramtypes", [typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__angular_common__["f" /* Location */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_common__["f" /* Location */]) === "function" && _c || Object])
    ], AuthLayoutComponent);
    return AuthLayoutComponent;
    var _a, _b, _c;
}());

//# sourceMappingURL=auth-layout.component.js.map

/***/ }),

/***/ "../../../../../src/app/login/login.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"wrapper wrapper-full-page\">\n    <nav class=\"navbar navbar-primary navbar-transparent navbar-fixed-top\">\n        <div class=\"container\">\n            <div class=\"navbar-header\">\n                <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#menu-example\">\n                    <span class=\"sr-only\">Toggle navigation</span>\n                    <span class=\"icon-bar\"></span>\n                    <span class=\"icon-bar\"></span>\n                    <span class=\"icon-bar\"></span>\n                </button>\n                <a class=\"navbar-brand\" href=\"/#/\">BT9COIN</a>\n            </div>\n            <div class=\"collapse navbar-collapse\" id=\"menu-example\">\n                <ul class=\"nav navbar-nav navbar-right\">\n                    <li class=\"\">\n                        <a href=\"/register\">\n                            <i class=\"material-icons\">person_add</i> Register\n                        </a>\n                    </li>\n                    <li class=\" active \">\n                        <a href=\"/login\">\n                            <i class=\"material-icons\">fingerprint</i> Login\n                        </a>\n                    </li>\n                </ul>\n            </div>\n        </div>\n    </nav>\n    <div class=\"full-page login-page\" filter-color=\"black\">\n        <!--  you can change the color of the filter page using: data-color=\"blue | purple | green | orange | red | rose \" -->\n        <div class=\"content\">\n            <div class=\"container\">\n                <div class=\"row\">\n                    <div class=\"col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3\">\n                        <form class=\"form-horizontal\" [formGroup]=\"login\">\n                            <div class=\"card card-login\">\n                                <div class=\"card-header text-center\">\n                                    <h4 class=\"card-title\">BT9Coin</h4>\n                                </div>\n                                <p class=\"category text-center\">\n                                    Login to your dream\n                                </p>\n                                <p class=\"text-danger text-center\" *ngIf=\"unconfirmed == 1\">\n                                    Your account is not active. Please go to your email and active account. If not receive, please check in spam box or resend email active.\n                                </p>\n                                <div class=\"card-content\">\n                                    <div class=\"input-group\">\n                                        <span class=\"input-group-addon\">\n                                            <i class=\"material-icons\">email</i>\n                                        </span>\n                                        <div class=\"form-group label-floating\" [ngClass]=\"validate.displayFieldCss(login, 'email')\">\n                                            <label class=\"control-label\">Email address</label>\n                                            <input type=\"email\" name=\"email\" class=\"form-control\" formControlName=\"email\" [(ngModel)]=\"user.email\">\n                                            <app-field-error-display [displayError]=\"validate.isFieldValid(login, 'email')\" errorMsg=\"Email is required and format should be john@doe.com.\">\n                                            </app-field-error-display>\n                                        </div>\n                                    </div>\n                                    <div class=\"input-group\">\n                                        <span class=\"input-group-addon\">\n                                            <i class=\"material-icons\">lock_outline</i>\n                                        </span>\n                                        <div class=\"form-group label-floating\" [ngClass]=\"validate.displayFieldCss(login, 'password')\">\n                                            <label class=\"control-label\">Password</label>\n                                            <input type=\"password\"\n                                                   name=\"password\"\n                                                   class=\"form-control\"\n                                                   formControlName=\"password\"\n                                                   [(ngModel)]=\"user.password\">\n                                            <app-field-error-display [displayError]=\"validate.isFieldValid(login, 'password')\" errorMsg=\"Password is required.\">\n                                            </app-field-error-display>\n                                        </div>\n                                    </div>\n                                    <div class=\"text-center captcha\" style=\"margin-top: 20px; margin-left: 30px;\" *ngIf=\"sysSettings.login_recaptcha == 1\">\n                                        <re-captcha site_key=\"6LdPjjoUAAAAADOGFhnrLXE1wvQc7ALjqJn03MLw\" (captchaResponse)=\"handleCorrectCaptcha($event)\"></re-captcha>\n                                    </div>\n                                </div>\n                                <div class=\"footer text-center\">\n                                    <button class=\"btn btn-success\" (click)=\"onLogin()\" *ngIf=\"!loging\">Login\n                                    </button>\n                                    <loader *ngIf=\"loging\"></loader>\n                                    <div class=\"text-center text-rose login-help\">\n                                        <a (click)=\"forgot()\">Forgot Your Password</a>\n                                        <br>\n                                        <a (click)=\"resend()\">Resend activation link</a>\n                                    </div>\n                                </div>\n                            </div>\n                        </form>\n                    </div>\n                </div>\n            </div>\n        </div>\n        <footer class=\"footer\">\n            <div class=\"container\">\n                <p class=\"copyright pull-right\">\n                    &copy; 2018, All Rights Reserved\n                </p>\n            </div>\n        </footer>\n        <div class=\"full-page-background\" style=\"background-image: url(../../assets/img/login.jpeg) \"></div>\n    </div>\n</div>\n\n<modal-dialog [(visible)]=\"showAuthModal\">\n    <form class=\"form-horizontal\">\n        <h2>\n            INPUT YOUR AUTHENTICATOR CODE\n        </h2>\n        <input type=\"text\" class=\"form-control\" style=\"text-align: center; font-size: 20px;\" name=\"authCode\" [(ngModel)]=\"authCode\">\n\n        <div class=\"swal2-validationerror\" style=\"display: block;\" *ngIf=\"!codeMatch\">The two-factor authentication code you specified is incorrect. Please install Google Authentication and add the two-factor code</div>\n\n        <div class=\"text-center\" style=\"margin-top: 20px;\">\n            <button type=\"button\" class=\"btn btn-primary\" (click)=\"submitCode()\">Submit</button>\n            <button type=\"button\" class=\"btn btn-default\" (click)=\"showAuthModal = !showAuthModal\">Cancel</button>\n        </div>\n    </form>\n</modal-dialog>\n\n<modal-dialog [(visible)]=\"showMailModal\" [closable]=\"false\">\n    <form class=\"form-horizontal\">\n        <h2 class=\"text-center\">\n            INPUT YOUR EMAIL\n        </h2>\n        <input type=\"text\" class=\"form-control\" style=\"text-align: center; font-size: 20px;\" name=\"confirm_email\" [(ngModel)]=\"confirm_email\">\n\n        <div class=\"swal2-validationerror text-center text-danger\" style=\"display: block;\" *ngIf=\"error != ''\">{{error}}</div>\n\n        <div class=\"text-center\" style=\"margin-top: 20px;\" *ngIf=\"!sendingEmail\">\n            <button type=\"button\" class=\"btn btn-primary\" (click)=\"submitEmail()\">Submit</button>\n            <button type=\"button\" class=\"btn btn-default\" (click)=\"showMailModal = !showMailModal\">Cancel</button>\n        </div>\n        <loader *ngIf=\"sendingEmail\"></loader>\n    </form>\n</modal-dialog>"

/***/ }),

/***/ "../../../../../src/app/login/login.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".login-page .card-login {\n  padding-top: 80px; }\n\n.login-help a {\n  cursor: pointer;\n  color: #e91e63; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/login/login.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_notifications_service__ = __webpack_require__("../../../../../src/app/services/notifications.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_api_service__ = __webpack_require__("../../../../../src/app/services/api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_settings_settings_service__ = __webpack_require__("../../../../../src/app/services/settings/settings.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_balance_service__ = __webpack_require__("../../../../../src/app/services/balance.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__services_validate_service__ = __webpack_require__("../../../../../src/app/services/validate.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_angular2_recaptcha__ = __webpack_require__("../../../../angular2-recaptcha/angular2-recaptcha.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_angular2_recaptcha___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_angular2_recaptcha__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var LoginComponent = (function () {
    function LoginComponent(element, formBuilder, api, notify, router, settings, balance, validate) {
        this.element = element;
        this.formBuilder = formBuilder;
        this.api = api;
        this.notify = notify;
        this.router = router;
        this.settings = settings;
        this.balance = balance;
        this.validate = validate;
        this.user = {};
        this.showAuthModal = false;
        this.authCode = '';
        this.codeMatch = true;
        this.g2f_code = '';
        this.res = {};
        this.unconfirmed = 0;
        this.showMailModal = false;
        this.mail_type = '';
        this.confirm_email = '';
        this.error = '';
        this.sendingEmail = false;
        this.loging = false;
        this.sysSettings = {};
    }
    LoginComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (this.router.url.includes('/confirmed')) {
            this.notify.showNotification('Your account was confirmed successfully.', 'top', 'center', 'success');
        }
        setTimeout(function () {
            // after 1000 ms we add the class animated to the login/register card
            $('.card').removeClass('card-hidden');
        }, 700);
        this.login = this.formBuilder.group({
            email: [null, [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].pattern("^[a-zA-Z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]],
            password: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required],
        });
        this.api.getSettings().subscribe(function (res) {
            if (res.success) {
                _this.sysSettings = res.data;
            }
        });
    };
    LoginComponent.prototype.successLogin = function () {
        this.settings.user = this.res;
        this.settings.setStorage('token', this.res.token);
        this.balance.init();
        if (this.res.role == 'admin') {
            this.router.navigate(['/admin/dashboard']);
        }
        else {
            this.router.navigate(['/dashboard']);
        }
    };
    LoginComponent.prototype.onLogin = function () {
        var _this = this;
        if (this.login.valid) {
            if (this.sysSettings.login_recaptcha == 1 && (this.captcha.getResponse() == null || this.captcha.getResponse() == '')) {
                this.notify.showNotification('Captcha validation failed.', 'top', 'center', 'danger');
                return;
            }
            this.loging = true;
            this.api.login(this.user).subscribe(function (res) {
                if (res.success) {
                    _this.loging = false;
                    if (res.confirmed == 0 || res.confirmed == null) {
                        _this.unconfirmed = 1;
                        return;
                    }
                    _this.res = res;
                    if (res.allow_g2f == 1) {
                        _this.g2f_code = res.g2f_code;
                        _this.showAuthModal = true;
                    }
                    else {
                        _this.successLogin();
                    }
                }
                else {
                    _this.loging = false;
                    _this.notify.showNotification(res.error, 'top', 'center', 'warning');
                }
            }, function (err) {
                _this.loging = false;
                _this.notify.showNotification('Error', 'top', 'center', 'danger');
            });
        }
        else {
            this.validate.validateAllFormFields(this.login);
        }
    };
    LoginComponent.prototype.submitCode = function () {
        var _this = this;
        if (this.authCode == '') {
            return;
        }
        this.codeMatch = true;
        this.api.confirmG2FCodeWithoutlogin({
            code: this.authCode,
            g2f_code: this.g2f_code
        }).subscribe(function (res) {
            if (res.success) {
                _this.successLogin();
            }
            else {
                _this.codeMatch = false;
            }
        }, function (err) {
            _this.codeMatch = false;
        });
    };
    LoginComponent.prototype.handleCorrectCaptcha = function ($event) {
        console.log($event);
    };
    LoginComponent.prototype.forgot = function () {
        this.mail_type = 'forgot';
        this.showMailModal = true;
    };
    LoginComponent.prototype.resend = function () {
        this.mail_type = 'resend';
        this.showMailModal = true;
    };
    LoginComponent.prototype.submitEmail = function () {
        var _this = this;
        if (this.confirm_email == '') {
            this.notify.showNotification('Please enter email address', 'top', 'center', 'danger');
            return;
        }
        this.error = '';
        if (this.mail_type == 'forgot') {
            this.sendingEmail = true;
            this.api.sendForgotEmail({
                email: this.confirm_email
            }).subscribe(function (res) {
                if (res.success) {
                    _this.sendingEmail = false;
                    _this.showMailModal = false;
                    _this.notify.showNotification('Email was sent successfully. Please check your email.', 'top', 'center', 'success');
                }
                else {
                    _this.sendingEmail = false;
                    _this.error = res.error;
                }
            }, function (err) {
                _this.sendingEmail = false;
                _this.notify.showNotification('Error', 'top', 'center', 'danger');
            });
        }
        else if (this.mail_type == 'resend') {
            this.sendingEmail = true;
            this.api.sendActivateEmail({
                email: this.confirm_email
            }).subscribe(function (res) {
                if (res.success) {
                    _this.sendingEmail = false;
                    _this.showMailModal = false;
                    _this.notify.showNotification('Confirmation email was sent successfully. Please check your email.', 'top', 'center', 'success');
                }
                else {
                    _this.sendingEmail = false;
                    _this.error = res.error;
                }
            }, function (err) {
                _this.sendingEmail = false;
                _this.notify.showNotification('Error', 'top', 'center', 'danger');
            });
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_8_angular2_recaptcha__["ReCaptchaComponent"]),
        __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_8_angular2_recaptcha__["ReCaptchaComponent"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_8_angular2_recaptcha__["ReCaptchaComponent"]) === "function" && _a || Object)
    ], LoginComponent.prototype, "captcha", void 0);
    LoginComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-login-cmp',
            template: __webpack_require__("../../../../../src/app/login/login.component.html"),
            styles: [__webpack_require__("../../../../../src/app/login/login.component.scss")]
        }),
        __metadata("design:paramtypes", [typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormBuilder"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormBuilder"]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_3__services_api_service__["a" /* Api */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services_api_service__["a" /* Api */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_2__services_notifications_service__["a" /* Notifications */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_notifications_service__["a" /* Notifications */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_4__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__angular_router__["c" /* Router */]) === "function" && _f || Object, typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_5__services_settings_settings_service__["a" /* SettingsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__services_settings_settings_service__["a" /* SettingsService */]) === "function" && _g || Object, typeof (_h = typeof __WEBPACK_IMPORTED_MODULE_6__services_balance_service__["a" /* BalanceService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__services_balance_service__["a" /* BalanceService */]) === "function" && _h || Object, typeof (_j = typeof __WEBPACK_IMPORTED_MODULE_7__services_validate_service__["a" /* Validate */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_7__services_validate_service__["a" /* Validate */]) === "function" && _j || Object])
    ], LoginComponent);
    return LoginComponent;
    var _a, _b, _c, _d, _e, _f, _g, _h, _j;
}());

//# sourceMappingURL=login.component.js.map

/***/ }),

/***/ "../../../../../src/app/md/md-chart/md-chart.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/md/md-chart/md-chart.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"card\">\n  <div class=\"header\">\n    <h4 class=\"title\">{{ title }}</h4>\n    <p class=\"category\">{{ subtitle }}</p>\n  </div>\n  <div class=\"content\">\n    <div [attr.id]=\"chartId\" class=\"ct-chart {{ chartClass }}\"></div>\n\n    <div class=\"footer\">\n      <div class=\"legend\">\n        <span *ngFor=\"let item of legendItems\">\n          <i [ngClass]=\"item.imageClass\"></i> {{ item.title }}\n        </span>\n      </div>\n      <hr *ngIf=\"withHr\">\n      <div class=\"stats\">\n        <i [ngClass]=\"footerIconClass\"></i> {{ footerText }}\n      </div>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/md/md-chart/md-chart.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export ChartType */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MdChartComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_chartist__ = __webpack_require__("../../../../chartist/dist/chartist.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_chartist___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_chartist__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ChartType;
(function (ChartType) {
    ChartType[ChartType["Pie"] = 0] = "Pie";
    ChartType[ChartType["Line"] = 1] = "Line";
    ChartType[ChartType["Bar"] = 2] = "Bar";
})(ChartType || (ChartType = {}));
var MdChartComponent = (function () {
    function MdChartComponent() {
    }
    MdChartComponent_1 = MdChartComponent;
    MdChartComponent.prototype.ngOnInit = function () {
        this.chartId = "md-chart-" + MdChartComponent_1.currentId++;
    };
    MdChartComponent.prototype.ngAfterViewInit = function () {
        switch (this.chartType) {
            case ChartType.Pie:
                new __WEBPACK_IMPORTED_MODULE_1_chartist__["Pie"]("#" + this.chartId, this.chartData, this.chartOptions, this.chartResponsive);
                break;
            case ChartType.Line:
                new __WEBPACK_IMPORTED_MODULE_1_chartist__["Line"]("#" + this.chartId, this.chartData, this.chartOptions, this.chartResponsive);
                break;
            case ChartType.Bar:
                new __WEBPACK_IMPORTED_MODULE_1_chartist__["Bar"]("#" + this.chartId, this.chartData, this.chartOptions, this.chartResponsive);
                break;
        }
    };
    MdChartComponent.currentId = 1;
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", String)
    ], MdChartComponent.prototype, "title", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", String)
    ], MdChartComponent.prototype, "subtitle", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", String)
    ], MdChartComponent.prototype, "chartClass", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Number)
    ], MdChartComponent.prototype, "chartType", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], MdChartComponent.prototype, "chartData", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], MdChartComponent.prototype, "chartOptions", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Array)
    ], MdChartComponent.prototype, "chartResponsive", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", String)
    ], MdChartComponent.prototype, "footerIconClass", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", String)
    ], MdChartComponent.prototype, "footerText", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Array)
    ], MdChartComponent.prototype, "legendItems", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Boolean)
    ], MdChartComponent.prototype, "withHr", void 0);
    MdChartComponent = MdChartComponent_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-md-chart',
            template: __webpack_require__("../../../../../src/app/md/md-chart/md-chart.component.html"),
            styles: [__webpack_require__("../../../../../src/app/md/md-chart/md-chart.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], MdChartComponent);
    return MdChartComponent;
    var MdChartComponent_1;
}());

//# sourceMappingURL=md-chart.component.js.map

/***/ }),

/***/ "../../../../../src/app/md/md-table/md-table.component.html":
/***/ (function(module, exports) {

module.exports = "\n  <div class=\"content table-responsive\">\n    <table class=\"table\">\n      <tbody>\n          <tr *ngFor=\"let row of data.dataRows\">\n            <!-- <td *ngFor=\"let cell of row\">{{ cell }}</td> -->\n            <td>\n                <div class=\"flag\">\n                    <img src=\"../../../assets/img/flags/{{row[0]}}.png\" alt=\"\">\n                </div>\n            </td>\n            <td>\n                {{row[1]}}\n            </td>\n            <td class=\"text-right\">\n                {{row[2]}}\n            </td>\n            <td class=\"text-right\">\n                {{row[3]}}\n            </td>\n          </tr>\n      </tbody>\n    </table>\n\n  </div>\n"

/***/ }),

/***/ "../../../../../src/app/md/md-table/md-table.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MdTableComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var MdTableComponent = (function () {
    function MdTableComponent() {
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", String)
    ], MdTableComponent.prototype, "title", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", String)
    ], MdTableComponent.prototype, "subtitle", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", String)
    ], MdTableComponent.prototype, "cardClass", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], MdTableComponent.prototype, "data", void 0);
    MdTableComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-md-table',
            template: __webpack_require__("../../../../../src/app/md/md-table/md-table.component.html"),
            changeDetection: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectionStrategy"].OnPush
        }),
        __metadata("design:paramtypes", [])
    ], MdTableComponent);
    return MdTableComponent;
}());

//# sourceMappingURL=md-table.component.js.map

/***/ }),

/***/ "../../../../../src/app/md/md.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return NavItemType; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MdModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__md_table_md_table_component__ = __webpack_require__("../../../../../src/app/md/md-table/md-table.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__md_chart_md_chart_component__ = __webpack_require__("../../../../../src/app/md/md-chart/md-chart.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var NavItemType;
(function (NavItemType) {
    NavItemType[NavItemType["Sidebar"] = 1] = "Sidebar";
    NavItemType[NavItemType["NavbarLeft"] = 2] = "NavbarLeft";
    NavItemType[NavItemType["NavbarRight"] = 3] = "NavbarRight"; // Right-aligned link on navbar in desktop mode, shown above sidebar items on collapsed sidebar in mobile mode
})(NavItemType || (NavItemType = {}));
var MdModule = (function () {
    function MdModule() {
    }
    MdModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_common__["b" /* CommonModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_router__["d" /* RouterModule */]
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__md_table_md_table_component__["a" /* MdTableComponent */],
                __WEBPACK_IMPORTED_MODULE_4__md_chart_md_chart_component__["a" /* MdChartComponent */]
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_3__md_table_md_table_component__["a" /* MdTableComponent */],
                __WEBPACK_IMPORTED_MODULE_4__md_chart_md_chart_component__["a" /* MdChartComponent */]
            ]
        })
    ], MdModule);
    return MdModule;
}());

//# sourceMappingURL=md.module.js.map

/***/ }),

/***/ "../../../../../src/app/register/register.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"wrapper wrapper-full-page\">\n    <nav class=\"navbar navbar-primary navbar-transparent navbar-absolute\">\n        <div class=\"container\">\n            <div class=\"navbar-header\">\n                <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#menu-example\">\n                    <span class=\"sr-only\">Toggle navigation</span>\n                    <span class=\"icon-bar\"></span>\n                    <span class=\"icon-bar\"></span>\n                    <span class=\"icon-bar\"></span>\n                </button>\n                <a class=\"navbar-brand\" href=\"\">BT9Coin</a>\n            </div>\n            <div class=\"collapse navbar-collapse\" id=\"menu-example\">\n                <ul class=\"nav navbar-nav navbar-right\">\n                    <li class=\" active \">\n                        <a href=\"/register\">\n                            <i class=\"material-icons\">person_add</i> Register\n                        </a>\n                    </li>\n                    <li class=\"\">\n                        <a href=\"/login\">\n                            <i class=\"material-icons\">fingerprint</i> Login\n                        </a>\n                    </li>\n                </ul>\n            </div>\n        </div>\n    </nav>\n    <div class=\"full-page register-page\" filter-color=\"black\">\n        <div class=\"container\">\n            <div class=\"row\">\n                <div class=\"col-md-10 col-md-offset-1\">\n                    <div class=\"card card-signup\">\n                        <h2 class=\"card-title text-center\">Register</h2>\n                        <div class=\"row\">\n                            <div class=\"col-md-5 col-md-offset-1\">\n                                <div class=\"card-content\">\n                                    <div class=\"info info-horizontal\">\n                                        <div class=\"icon icon-rose\">\n                                            <i class=\"material-icons\">timeline</i>\n                                        </div>\n                                        <div class=\"description\">\n                                            <h4 class=\"info-title\">Marketing</h4>\n                                            <p class=\"description\">\n                                                We've created the marketing campaign of the website. It was a very interesting collaboration.\n                                            </p>\n                                        </div>\n                                    </div>\n                                    <div class=\"info info-horizontal\">\n                                        <div class=\"icon icon-primary\">\n                                            <i class=\"material-icons\">code</i>\n                                        </div>\n                                        <div class=\"description\">\n                                            <h4 class=\"info-title\">Fully Coded in HTML5</h4>\n                                            <p class=\"description\">\n                                                We've developed the website with HTML5 and CSS3. The client has access to the code using GitHub.\n                                            </p>\n                                        </div>\n                                    </div>\n                                    <div class=\"info info-horizontal\">\n                                        <div class=\"icon icon-info\">\n                                            <i class=\"material-icons\">group</i>\n                                        </div>\n                                        <div class=\"description\">\n                                            <h4 class=\"info-title\">Built Audience</h4>\n                                            <p class=\"description\">\n                                                There is also a Fully Customizable CMS Admin Dashboard for this product.\n                                            </p>\n                                        </div>\n                                    </div>\n                                </div>\n                            </div>\n                            <div class=\"col-md-5\">\n                                <form class=\"form-horizontal\" [formGroup]=\"signup\">\n                                    <div class=\"card-content\">\n                                        <div class=\"input-group\">\n                                            <span class=\"input-group-addon\">\n                                                <i class=\"material-icons\">face</i>\n                                            </span>\n                                            <div class=\"form-group label-floating\" [ngClass]=\"validate.displayFieldCss(signup, 'fullname')\">\n                                                <label class=\"control-label\"></label>\n                                                <input type=\"text\" name=\"fullname\" class=\"form-control\" formControlName=\"fullname\" placeholder=\"Your full name...\" [(ngModel)]=\"user.fullname\">\n                                                <app-field-error-display [displayError]=\"validate.isFieldValid(signup, 'fullname')\" errorMsg=\"Full name is required and must be with letter.\">\n                                                </app-field-error-display>\n                                            </div>\n                                        </div>\n                                        <div class=\"input-group\">\n                                            <span class=\"input-group-addon\">\n                                                <i class=\"material-icons\">account_box</i>\n                                            </span>\n                                            <div class=\"form-group label-floating\" [ngClass]=\"validate.displayFieldCss(signup, 'username')\">\n                                                <label class=\"control-label\"></label>\n                                                <input type=\"text\" name=\"username\" class=\"form-control\" formControlName=\"username\" placeholder=\"Username...\" [(ngModel)]=\"user.username\">\n                                                <app-field-error-display [displayError]=\"validate.isFieldValid(signup, 'username')\" errorMsg=\"Username is required and must be with letter and number.\">\n                                                </app-field-error-display>\n                                            </div>\n                                        </div>\n                                        <div class=\"input-group\">\n                                            <span class=\"input-group-addon\">\n                                                <i class=\"material-icons\">email</i>\n                                            </span>\n                                            <div class=\"form-group label-floating\"  [ngClass]=\"validate.displayFieldCss(signup, 'email')\">\n                                                <label class=\"control-label\"></label>\n                                                <input type=\"text\" name=\"email\" class=\"form-control\" formControlName=\"email\" placeholder=\"Email...\" [(ngModel)]=\"user.email\">\n                                                <app-field-error-display [displayError]=\"validate.isFieldValid(signup, 'email')\" errorMsg=\"Email is required and format should be john@doe.com.\">\n                                                </app-field-error-display>\n                                            </div>\n                                        </div>\n                                        <div class=\"input-group\">\n                                            <span class=\"input-group-addon\">\n                                                <i class=\"material-icons\">lock_outline</i>\n                                            </span>\n                                            <div class=\"form-group label-floating\" [ngClass]=\"validate.displayFieldCss(signup, 'password')\">\n                                                <label class=\"control-label\"></label>\n                                                <input type=\"password\"\n                                                       name=\"password\"\n                                                       class=\"form-control\"\n                                                       formControlName=\"password\"\n                                                       placeholder=\"Password...\"\n                                                       [(ngModel)]=\"user.password\">\n                                                <app-field-error-display [displayError]=\"validate.isFieldValid(signup, 'password')\" errorMsg=\"Enter a valid password.\">\n                                                </app-field-error-display>\n                                            </div>\n                                        </div>\n                                        <div class=\"input-group\">\n                                            <span class=\"input-group-addon\">\n                                                <i class=\"material-icons\">lock_outline</i>\n                                            </span>\n                                            <div class=\"form-group label-floating\" [ngClass]=\"validate.displayFieldCss(signup, 'confirmPassword')\">\n                                                <label class=\"control-label\"></label>\n                                                <input\n                                                        type=\"password\"\n                                                        class=\"form-control\"\n                                                        name=\"confirmPassword\"\n                                                        formControlName=\"confirmPassword\"\n                                                        placeholder=\"Retype Password...\">\n                                                <app-field-error-display\n                                                        [displayError]=\"validate.isFieldValid(signup, 'confirmPassword')\" errorMsg=\"These passwords don't match. Try again!\">\n                                                </app-field-error-display>\n                                            </div>\n                                        </div>\n                                        <div class=\"input-group\">\n                                            <span class=\"input-group-addon\">\n                                                <i class=\"material-icons\">supervisor_account</i>\n                                            </span>\n                                            <div class=\"form-group\">\n                                                <input type=\"text\" name=\"sponser\" class=\"form-control\" formControlName=\"sponser\" placeholder=\"Sponser...\" readonly value=\"{{user.sponser}}\">\n                                            </div>\n                                        </div>\n                                        <!-- If you want to add a checkbox to this form, uncomment this code -->\n                                        <div class=\"checkbox\">\n                                            <label>\n                                                <input type=\"checkbox\" name=\"optionsCheckboxes\" formControlName=\"agree\"  [(ngModel)]=\"user.agree\"> I agree to the terms and conditions.\n                                            </label>\n                                        </div>\n                                    </div>\n\n                                    <div class=\"text-center captcha\" style=\"margin-top: 20px; margin-left: 30px;\" *ngIf=\"sysSettings.register_recaptcha == 1\">\n                                        <re-captcha site_key=\"6LdPjjoUAAAAADOGFhnrLXE1wvQc7ALjqJn03MLw\" (captchaResponse)=\"handleCorrectCaptcha($event)\"></re-captcha>\n                                    </div>\n                                    <div class=\"footer text-center\">\n                                        <button class=\"btn btn-primary btn-round\" (click)=\"onSignup()\" *ngIf=\"!loading\">Sign up</button>\n                                        <loader *ngIf=\"loading\"></loader>\n                                    </div>\n                                </form>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n        <footer class=\"footer\">\n            <div class=\"container\">\n                <p class=\"copyright pull-right\">\n                    &copy; 2018, All Rights Reserved\n                </p>\n            </div>\n        </footer>\n        <div class=\"full-page-background\" style=\"background-image: url(../../../assets/img/register.jpeg) \"></div>\n    </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/register/register.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/register/register.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegisterComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__shared_components_password_validator_component__ = __webpack_require__("../../../../../src/app/shared/components/password-validator.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_api_service__ = __webpack_require__("../../../../../src/app/services/api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_notifications_service__ = __webpack_require__("../../../../../src/app/services/notifications.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_validate_service__ = __webpack_require__("../../../../../src/app/services/validate.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_angular2_recaptcha__ = __webpack_require__("../../../../angular2-recaptcha/angular2-recaptcha.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_angular2_recaptcha___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_angular2_recaptcha__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var RegisterComponent = (function () {
    function RegisterComponent(formBuilder, router, api, notify, activatedRoute, validate) {
        this.formBuilder = formBuilder;
        this.router = router;
        this.api = api;
        this.notify = notify;
        this.activatedRoute = activatedRoute;
        this.validate = validate;
        this.user = {
            agree: true
        };
        this.sysSettings = {};
        this.loading = false;
    }
    RegisterComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.activatedRoute.params.subscribe(function (params) {
            _this.user.sponser = params['sponser'];
        });
        this.signup = this.formBuilder.group({
            // To add a validator, we must first convert the string value into an array. The first item in the array is the default value if any, then the next item in the array is the validator. Here we are adding a required validator meaning that the firstName attribute must have a value in it.
            username: [null, [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].pattern('^[a-zA-Z0-9]+$')]],
            fullname: [null, [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].pattern('^[a-z A-Z]+$')]],
            email: [null, [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].pattern("^[a-zA-Z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]],
            // We can use more than one validator per field. If we want to use more than one validator we have to wrap our array of validators with a Validators.compose function. Here we are using a required, minimum length and maximum length validator.
            password: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required],
            confirmPassword: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required],
            sponser: [''],
            agree: [''],
        }, {
            validator: __WEBPACK_IMPORTED_MODULE_2__shared_components_password_validator_component__["a" /* PasswordValidation */].MatchPassword // your validation method
        });
        this.api.getSettings().subscribe(function (res) {
            if (res.success) {
                _this.sysSettings = res.data;
            }
        });
    };
    RegisterComponent.prototype.onSignup = function () {
        var _this = this;
        if (this.signup.valid) {
            if (!this.user.agree) {
                this.notify.showNotification('Please check', 'top', 'center', 'danger');
                return;
            }
            if (this.sysSettings.register_recaptcha == 1 && (this.captcha.getResponse() == null || this.captcha.getResponse() == '')) {
                this.notify.showNotification('Captcha validation failed.', 'top', 'center', 'danger');
                return;
            }
            this.loading = true;
            this.api.register(this.user).subscribe(function (res) {
                _this.loading = false;
                if (res.success) {
                    _this.notify.showNotification('Success', 'top', 'center', 'success');
                    _this.router.navigate(['/login']);
                }
                else {
                    _this.notify.showNotification(res.error, 'top', 'center', 'warning');
                }
            }, function (err) {
                _this.loading = false;
                _this.notify.showNotification('Error', 'top', 'center', 'danger');
            });
        }
        else {
            this.validate.validateAllFormFields(this.signup);
        }
    };
    RegisterComponent.prototype.handleCorrectCaptcha = function ($event) {
        console.log($event);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_7_angular2_recaptcha__["ReCaptchaComponent"]),
        __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_7_angular2_recaptcha__["ReCaptchaComponent"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_7_angular2_recaptcha__["ReCaptchaComponent"]) === "function" && _a || Object)
    ], RegisterComponent.prototype, "captcha", void 0);
    RegisterComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-register-cmp',
            template: __webpack_require__("../../../../../src/app/register/register.component.html"),
            styles: [__webpack_require__("../../../../../src/app/register/register.component.scss")]
        }),
        __metadata("design:paramtypes", [typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormBuilder"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormBuilder"]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_5__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__angular_router__["c" /* Router */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_3__services_api_service__["a" /* Api */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services_api_service__["a" /* Api */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_4__services_notifications_service__["a" /* Notifications */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__services_notifications_service__["a" /* Notifications */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_5__angular_router__["a" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__angular_router__["a" /* ActivatedRoute */]) === "function" && _f || Object, typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_6__services_validate_service__["a" /* Validate */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__services_validate_service__["a" /* Validate */]) === "function" && _g || Object])
    ], RegisterComponent);
    return RegisterComponent;
    var _a, _b, _c, _d, _e, _f, _g;
}());

//# sourceMappingURL=register.component.js.map

/***/ }),

/***/ "../../../../../src/app/register/sponser-resolver.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SponserResolver; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_api_service__ = __webpack_require__("../../../../../src/app/services/api.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var SponserResolver = (function () {
    function SponserResolver(api, router) {
        this.api = api;
        this.router = router;
    }
    SponserResolver.prototype.resolve = function (route, state) {
        var _this = this;
        var sponser = route.params['sponser'];
        return new Promise(function (resolve, reject) {
            _this.api.getSponser({ sponser: sponser }).subscribe(function (res) {
                if (res.success) {
                    resolve(res.sponser);
                }
                else {
                    _this.router.navigate(['login']);
                    resolve(null);
                }
            }, function (err) {
                _this.router.navigate(['login']);
                resolve(null);
            });
        });
    };
    SponserResolver = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__services_api_service__["a" /* Api */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_api_service__["a" /* Api */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]) === "function" && _b || Object])
    ], SponserResolver);
    return SponserResolver;
    var _a, _b;
}());

//# sourceMappingURL=sponser-resolver.service.js.map

/***/ }),

/***/ "../../../../../src/app/reset_password/reset_password-resolver.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ResetPasswordResolver; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_api_service__ = __webpack_require__("../../../../../src/app/services/api.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ResetPasswordResolver = (function () {
    function ResetPasswordResolver(api, router) {
        this.api = api;
        this.router = router;
    }
    ResetPasswordResolver.prototype.resolve = function (route, state) {
        var _this = this;
        var confirm_code = route.params['code'];
        return new Promise(function (resolve, reject) {
            _this.api.confirmResetCode({ code: confirm_code }).subscribe(function (res) {
                if (res.success) {
                    resolve(res.user_id);
                }
                else {
                    _this.router.navigate(['login']);
                    resolve(null);
                }
            }, function (err) {
                _this.router.navigate(['login']);
                resolve(null);
            });
        });
    };
    ResetPasswordResolver = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__services_api_service__["a" /* Api */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_api_service__["a" /* Api */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]) === "function" && _b || Object])
    ], ResetPasswordResolver);
    return ResetPasswordResolver;
    var _a, _b;
}());

//# sourceMappingURL=reset_password-resolver.service.js.map

/***/ }),

/***/ "../../../../../src/app/reset_password/reset_password.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"wrapper wrapper-full-page\">\n    <nav class=\"navbar navbar-primary navbar-transparent navbar-fixed-top\">\n        <div class=\"container\">\n            <div class=\"navbar-header\">\n                <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#menu-example\">\n                    <span class=\"sr-only\">Toggle navigation</span>\n                    <span class=\"icon-bar\"></span>\n                    <span class=\"icon-bar\"></span>\n                    <span class=\"icon-bar\"></span>\n                </button>\n                <a class=\"navbar-brand\" href=\"/#/\">MYALTCOIN</a>\n            </div>\n            <div class=\"collapse navbar-collapse\" id=\"menu-example\">\n                <ul class=\"nav navbar-nav navbar-right\">\n                    <li class=\"\">\n                        <a href=\"/register\">\n                            <i class=\"material-icons\">person_add</i> Register\n                        </a>\n                    </li>\n                    <li class=\" active \">\n                        <a href=\"/login\">\n                            <i class=\"material-icons\">fingerprint</i> Login\n                        </a>\n                    </li>\n                </ul>\n            </div>\n        </div>\n    </nav>\n    <div class=\"full-page login-page\" filter-color=\"black\">\n        <!--  you can change the color of the filter page using: data-color=\"blue | purple | green | orange | red | rose \" -->\n        <div class=\"content\">\n            <div class=\"container\">\n                <div class=\"row\">\n                    <div class=\"col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3\">\n                        <form class=\"form-horizontal\" [formGroup]=\"reset\">\n                            <div class=\"card card-login\">\n                                <div class=\"card-header text-center\">\n                                    <h4 class=\"card-title\">MyAltCoin</h4>\n                                </div>\n                                <p class=\"category text-center\">\n                                    Reset Password\n                                </p>\n                                <div class=\"card-content\">\n                                    <div class=\"input-group\">\n                                        <span class=\"input-group-addon\">\n                                            <i class=\"material-icons\">lock_outline</i>\n                                        </span>\n                                        <div class=\"form-group label-floating\" [ngClass]=\"validate.displayFieldCss(reset, 'password')\">\n                                            <label class=\"control-label\">Password</label>\n                                            <input type=\"password\"\n                                                   name=\"password\"\n                                                   class=\"form-control\"\n                                                   formControlName=\"password\"\n                                                   [(ngModel)]=\"user.password\">\n                                            <app-field-error-display [displayError]=\"validate.isFieldValid(reset, 'password')\" errorMsg=\"Password is required.\">\n                                            </app-field-error-display>\n                                        </div>\n                                    </div>\n                                    <div class=\"input-group\">\n                                        <span class=\"input-group-addon\">\n                                            <i class=\"material-icons\">lock_outline</i>\n                                        </span>\n                                        <div class=\"form-group label-floating\" [ngClass]=\"validate.displayFieldCss(reset, 'confirmPassword')\">\n                                            <label class=\"control-label\">Confirm Password</label>\n                                            <input type=\"password\"\n                                                    class=\"form-control\"\n                                                    name=\"confirmPassword\"\n                                                    formControlName=\"confirmPassword\">\n                                            <app-field-error-display\n                                                    [displayError]=\"validate.isFieldValid(reset, 'confirmPassword')\" errorMsg=\"These passwords don't match. Try again!\">\n                                            </app-field-error-display>\n                                        </div>\n                                    </div>\n                                </div>\n                                <div class=\"footer text-center\">\n                                    <button class=\"btn btn-success\" (click)=\"onResetPassword()\">Reset Password</button>\n                                </div>\n                            </div>\n                        </form>\n                    </div>\n                </div>\n            </div>\n        </div>\n        <footer class=\"footer\">\n            <div class=\"container\">\n                <p class=\"copyright pull-right\">\n                    &copy; 2017, All Rights Reserved\n                </p>\n            </div>\n        </footer>\n        <div class=\"full-page-background\" style=\"background-image: url(../../assets/img/login.jpeg) \"></div>\n    </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/reset_password/reset_password.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".login-page .card-login {\n  padding-top: 80px; }\n\n.login-help a {\n  cursor: pointer;\n  color: #e91e63; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/reset_password/reset_password.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ResetPasswordComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_notifications_service__ = __webpack_require__("../../../../../src/app/services/notifications.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_api_service__ = __webpack_require__("../../../../../src/app/services/api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_settings_settings_service__ = __webpack_require__("../../../../../src/app/services/settings/settings.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_balance_service__ = __webpack_require__("../../../../../src/app/services/balance.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__services_validate_service__ = __webpack_require__("../../../../../src/app/services/validate.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__shared_components_password_validator_component__ = __webpack_require__("../../../../../src/app/shared/components/password-validator.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var ResetPasswordComponent = (function () {
    function ResetPasswordComponent(element, formBuilder, api, notify, router, settings, balance, validate, activatedRoute) {
        this.element = element;
        this.formBuilder = formBuilder;
        this.api = api;
        this.notify = notify;
        this.router = router;
        this.settings = settings;
        this.balance = balance;
        this.validate = validate;
        this.activatedRoute = activatedRoute;
        this.user = {};
    }
    ResetPasswordComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sub = this.activatedRoute.data
            .subscribe(function (data) {
            _this.user.id = data.user_id;
        });
        setTimeout(function () {
            $('.card').removeClass('card-hidden');
        }, 700);
        this.reset = this.formBuilder.group({
            password: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required],
            confirmPassword: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required],
        }, {
            validator: __WEBPACK_IMPORTED_MODULE_8__shared_components_password_validator_component__["a" /* PasswordValidation */].MatchPassword // your validation method
        });
    };
    ResetPasswordComponent.prototype.onResetPassword = function () {
        var _this = this;
        if (this.reset.valid) {
            this.api.resetPassword(this.user).subscribe(function (res) {
                if (res.success) {
                    _this.notify.showNotification('Password was changed succesfully', 'top', 'center', 'success');
                    _this.router.navigate(['login']);
                }
                else {
                    _this.notify.showNotification(res.error, 'top', 'center', 'warning');
                }
            }, function (err) {
                _this.notify.showNotification('Error', 'top', 'center', 'danger');
            });
        }
        else {
            this.validate.validateAllFormFields(this.reset);
        }
    };
    ResetPasswordComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-resetpassword-cmp',
            template: __webpack_require__("../../../../../src/app/reset_password/reset_password.component.html"),
            styles: [__webpack_require__("../../../../../src/app/reset_password/reset_password.component.scss")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormBuilder"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormBuilder"]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__services_api_service__["a" /* Api */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services_api_service__["a" /* Api */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2__services_notifications_service__["a" /* Notifications */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_notifications_service__["a" /* Notifications */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_4__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__angular_router__["c" /* Router */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_5__services_settings_settings_service__["a" /* SettingsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__services_settings_settings_service__["a" /* SettingsService */]) === "function" && _f || Object, typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_6__services_balance_service__["a" /* BalanceService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__services_balance_service__["a" /* BalanceService */]) === "function" && _g || Object, typeof (_h = typeof __WEBPACK_IMPORTED_MODULE_7__services_validate_service__["a" /* Validate */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_7__services_validate_service__["a" /* Validate */]) === "function" && _h || Object, typeof (_j = typeof __WEBPACK_IMPORTED_MODULE_4__angular_router__["a" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__angular_router__["a" /* ActivatedRoute */]) === "function" && _j || Object])
    ], ResetPasswordComponent);
    return ResetPasswordComponent;
    var _a, _b, _c, _d, _e, _f, _g, _h, _j;
}());

//# sourceMappingURL=reset_password.component.js.map

/***/ }),

/***/ "../../../../../src/app/services/api.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Api; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs__ = __webpack_require__("../../../../rxjs/Rx.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__settings_settings_service__ = __webpack_require__("../../../../../src/app/services/settings/settings.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/**
 * Created by ApolloYr on 11/17/2017.
 */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var Api = (function () {
    function Api(http, router, settings) {
        this.http = http;
        this.router = router;
        this.settings = settings;
    }
    Api.prototype.createAuthorizationHeader = function (headers) {
        headers.append('Authorization', 'Bearer ' + this.settings.getStorage('token'));
    };
    Api.prototype.get = function (url, data) {
        var _this = this;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]();
        this.createAuthorizationHeader(headers);
        var params = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["g" /* URLSearchParams */]();
        if (data) {
            for (var key in data) {
                params.set(key, data[key]);
            }
        }
        return this.http.get(this.settings.apiUrl + url, {
            headers: headers,
            search: params
        }).map(function (res) { return res.json(); }).catch(function (error) { return _this.handleError(_this, error); });
    };
    Api.prototype.post = function (url, data) {
        var _this = this;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]();
        this.createAuthorizationHeader(headers);
        return this.http.post(this.settings.apiUrl + url, data, {
            headers: headers
        }).map(function (res) { return res.json(); }).catch(function (error) { return _this.handleError(_this, error); });
    };
    Api.prototype.put = function (url, data) {
        var _this = this;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]();
        this.createAuthorizationHeader(headers);
        return this.http.put(this.settings.apiUrl + url, data, {
            headers: headers
        }).map(function (res) { return res.json(); }).catch(function (error) { return _this.handleError(_this, error); });
    };
    Api.prototype.uploadFile = function (file) {
        var _this = this;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]();
        this.createAuthorizationHeader(headers);
        var formData = new FormData();
        formData.append("file", file, file.name);
        return this.http.post(this.settings.apiUrl + '/upload/file', formData, {
            headers: headers
        }).map(function (res) { return res.json(); }).catch(function (error) { return _this.handleError(_this, error); });
    };
    Api.prototype.handleError = function (_parent, error) {
        if ((error.status == 401 || error.status == 400) && error.url && !error.url.endsWith('/login')) {
            //console.log('unauthorized');
            //if (_parent.settings) _parent.settings.setStorage('token', false);
            //if (_parent.settings) _parent.settings.setAppSetting('is_loggedin', false);
            //_parent.router.navigate(['/login']);
        }
        // In a real world app, you might use a remote logging infrastructure
        return __WEBPACK_IMPORTED_MODULE_2_rxjs__["Observable"].throw(error);
    };
    Api.prototype.getAccountInfo = function () {
        return this.get('/account');
    };
    Api.prototype.login = function (data) {
        return this.post('/login', data);
    };
    Api.prototype.resetPassword = function (data) {
        return this.post('/reset_password', data);
    };
    Api.prototype.register = function (data) {
        return this.post('/register', data);
    };
    Api.prototype.getCoinAddress = function (coin, data) {
        return this.get('/' + coin + '/address', data);
    };
    Api.prototype.getCoinBalance = function (coin, data) {
        return this.get('/' + coin + '/balance', data);
    };
    Api.prototype.withdrawCoin = function (coin, data) {
        return this.post('/' + coin + '/withdraw', data);
    };
    Api.prototype.cancelWithdrawCoin = function (coin, data) {
        return this.post('/' + coin + '/withdraw/cancel', data);
    };
    Api.prototype.getSentTxList = function (coin, data) {
        return this.get('/' + coin + '/senttx', data);
    };
    Api.prototype.getReceivedTxList = function (coin, data) {
        return this.get('/' + coin + '/receivedtx', data);
    };
    Api.prototype.getUnconfirmedReceivedTxList = function (coin, data) {
        return this.get('/' + coin + '/unconfirmed/receivedtx', data);
    };
    Api.prototype.getUnconfirmedSentTxList = function (coin, data) {
        return this.get('/' + coin + '/unconfirmed/senttx', data);
    };
    Api.prototype.getTxList = function (coin, data) {
        return this.get('/' + coin + '/gettx', data);
    };
    Api.prototype.getReferral = function (data) {
        return this.get('/get_referral', data);
    };
    Api.prototype.getCoinRate = function (data) {
        return this.get('/rate', data);
    };
    Api.prototype.getTokenBalance = function (data) {
        return this.get('/token_balance', data);
    };
    Api.prototype.getUsdBalance = function (data) {
        return this.get('/usd_balance', data);
    };
    Api.prototype.getTokenRate = function (data) {
        return this.get('/token_rate', data);
    };
    Api.prototype.getBonusList = function (data) {
        return this.get('/bonus_lists', data);
    };
    Api.prototype.getTotalSold = function (data) {
        return this.get('/toten_total', data);
    };
    Api.prototype.buyTokenWithBTC = function (data) {
        return this.post('/buyTokenWithBTC', data);
    };
    Api.prototype.buyTokenWithETH = function (data) {
        return this.post('/buyTokenWithETH', data);
    };
    Api.prototype.getG2FCode = function (data) {
        return this.get('/getG2FCode', data);
    };
    Api.prototype.setG2F = function (data) {
        return this.post('/setG2F', data);
    };
    Api.prototype.confirmG2FCode = function (data) {
        return this.post('/confirmG2FCode', data);
    };
    Api.prototype.sendWithdrawMail = function (data) {
        return this.post('/withdraw/sendmail', data);
    };
    Api.prototype.confirmWithdrawCode = function (data) {
        return this.post('/withdraw/confirmcode', data);
    };
    Api.prototype.confirmG2FCodeWithoutlogin = function (data) {
        return this.post('/confirmG2FCodeWithoutlogin', data);
    };
    Api.prototype.chagePwd = function (data) {
        return this.post('/change_password', data);
    };
    Api.prototype.changeUserInfo = function (data) {
        return this.post('/account/changeLoginEmailStatus', data);
    };
    Api.prototype.changeWithdrawMailStatus = function (data) {
        return this.post('/account/changeWithdrawMailStatus', data);
    };
    Api.prototype.getNetworkFee = function (coin, data) {
        return this.get('/' + coin + '/network_fee', data);
    };
    Api.prototype.getSettings = function () {
        return this.get('/settings', {});
    };
    Api.prototype.getTokenHistory = function () {
        return this.get('/token_history', {});
    };
    Api.prototype.sendForgotEmail = function (data) {
        return this.post('/account/sendForgotEmail', data);
    };
    Api.prototype.sendActivateEmail = function (data) {
        return this.post('/account/sendActivateEmail', data);
    };
    Api.prototype.confirmResetCode = function (data) {
        return this.get('/confirm_resetcode', data);
    };
    Api.prototype.getSponser = function (data) {
        return this.get('/get_sponser', data);
    };
    Api.prototype.getLoginHistory = function (data) {
        return this.get('/account/loginHistory', data);
    };
    /// Admin Apis
    Api.prototype.getAdminUsers = function (data) {
        return this.get('/admin/adminUsers', data);
    };
    Api.prototype.addAdmin = function (data) {
        return this.post('/admin/add/adminUser', data);
    };
    Api.prototype.deleteAdmin = function (data) {
        return this.post('/admin/delete/adminUser', data);
    };
    Api.prototype.userCount = function (data) {
        return this.get('/admin/user/count', data);
    };
    Api.prototype.getSystemCurrency = function (data) {
        return this.get('/admin/sysCurrency', data);
    };
    Api.prototype.getMyAltCoinChart = function () {
        return this.get('/admin/token_chart', {});
    };
    Api.prototype.getSystemCurrencyChart = function () {
        return this.get('/admin/syscurrency_chart', {});
    };
    Api.prototype.getUsers = function () {
        return this.get('/admin/users', {});
    };
    Api.prototype.getUserIDByEmail = function (data) {
        return this.get('/admin/user/getIdByEmail', data);
    };
    Api.prototype.getUser = function (data) {
        return this.get('/admin/user/get', data);
    };
    Api.prototype.deleteUser = function (data) {
        return this.post('/admin/user/delete', data);
    };
    Api.prototype.getAllTokenHistory = function (data) {
        return this.get('/admin/token_history', data);
    };
    Api.prototype.getAdminUserTokenHistory = function (data) {
        return this.get('/admin/user/BT9/history', data);
    };
    Api.prototype.getAdminUserBTCHistory = function (data) {
        return this.get('/admin/user/BTC/history', data);
    };
    Api.prototype.getAdminUserETHHistory = function (data) {
        return this.get('/admin/user/ETH/history', data);
    };
    Api.prototype.getAllTxList = function (coin, data) {
        return this.get('/admin/' + coin + '/transactions', data);
    };
    Api.prototype.saveBonus = function (data) {
        return this.post('/admin/bonus_lists', data);
    };
    Api.prototype.saveRates = function (data) {
        return this.post('/admin/token_rates', data);
    };
    Api.prototype.saveSettings = function (data) {
        return this.post('/admin/settings', data);
    };
    Api.prototype.addMyAltCoin = function (data) {
        return this.post('/admin/exchange/add', data);
    };
    // ICO Agenda
    Api.prototype.getICOAgenda = function (data) {
        return this.get('/ico_agenda', data);
    };
    Api.prototype.saveICOAgenda = function (data) {
        return this.post('/ico_agenda', data);
    };
    Api.prototype.deleteICOAgenda = function (data) {
        return this.post('/ico_agenda/delete', data);
    };
    Api.prototype.getICOAgendaChart = function (data) {
        return this.get('/ico_agenda/chart', data);
    };
    // BTC/MyAltCoin Exchange
    Api.prototype.makeBTCBuyOrder = function (data) {
        return this.post('/order/btc/buy', data);
    };
    Api.prototype.makeBTCSellOrder = function (data) {
        return this.post('/order/btc/sell', data);
    };
    Api.prototype.getBTCBuyOrders = function (data) {
        return this.get('/order/btc/buy', data);
    };
    Api.prototype.getBTCSellOrders = function (data) {
        return this.get('/order/btc/sell', data);
    };
    Api.prototype.getBTCPastOrders = function (data) {
        return this.get('/order/btc/past', data);
    };
    Api.prototype.getBTCOpenOrders = function (data) {
        return this.get('/order/btc/open', data);
    };
    Api.prototype.cancelBTCOrder = function (data) {
        return this.post('/order/btc/delete', data);
    };
    // ETH/MyAltCoin Exchange
    Api.prototype.makeETHBuyOrder = function (data) {
        return this.post('/order/eth/buy', data);
    };
    Api.prototype.makeETHSellOrder = function (data) {
        return this.post('/order/eth/sell', data);
    };
    Api.prototype.getETHBuyOrders = function (data) {
        return this.get('/order/eth/buy', data);
    };
    Api.prototype.getETHSellOrders = function (data) {
        return this.get('/order/eth/sell', data);
    };
    Api.prototype.getETHPastOrders = function (data) {
        return this.get('/order/eth/past', data);
    };
    Api.prototype.getETHOpenOrders = function (data) {
        return this.get('/order/eth/open', data);
    };
    Api.prototype.cancelETHOrder = function (data) {
        return this.post('/order/eth/delete', data);
    };
    // Lending
    Api.prototype.getLendingSetting = function (data) {
        return this.get('/lending/setting', data);
    };
    Api.prototype.saveLendingSetting = function (data) {
        return this.post('/lending/setting', data);
    };
    Api.prototype.getLending = function (data) {
        return this.get('/lending', data);
    };
    Api.prototype.getLendingTx = function (data) {
        return this.get('/lending/tx', data);
    };
    Api.prototype.saveLending = function (data) {
        return this.post('/lending', data);
    };
    Api.prototype.deleteLendingSetting = function (data) {
        return this.post('/lending/setting/delete', data);
    };
    Api.prototype.getAllLending = function (data) {
        return this.get('/lending/admin', data);
    };
    Api.prototype.getLendingBalance = function (data) {
        return this.get('/lending/balance', data);
    };
    Api.prototype.getLendingDaily = function (data) {
        return this.get('/lending/daily', data);
    };
    Api.prototype.transferLending = function (data) {
        return this.post('/lending/transfer', data);
    };
    Api.prototype.transferDailyLending = function (data) {
        return this.post('/lending/transfer/daily', data);
    };
    Api.prototype.getInterestDaily = function (data) {
        return this.get('/lending/interest', data);
    };
    Api.prototype.releaseAllLending = function (data) {
        return this.post('/lending/release/all', data);
    };
    // Staking
    Api.prototype.getStaking = function (data) {
        return this.get('/staking', data);
    };
    Api.prototype.saveStaking = function (data) {
        return this.post('/staking', data);
    };
    Api.prototype.getAllStaking = function (data) {
        return this.get('/staking/admin', data);
    };
    Api.prototype.getStakingBalance = function (data) {
        return this.get('/staking/balance', data);
    };
    Api.prototype.releaseAllStaking = function (data) {
        return this.post('/staking/release/all', data);
    };
    // ICO History
    Api.prototype.getAllICOHistory = function (data) {
        return this.get('/admin/ico/history', data);
    };
    // Admin Withdraw
    Api.prototype.getWithdrawPending = function (coin) {
        return this.get('/admin/withdraw/' + coin + '/pending', {});
    };
    Api.prototype.getWalletTotalBalance = function (coin) {
        return this.get('/admin/wallet/' + coin + '/balance', {});
    };
    Api.prototype.estimateFee = function (coin, data) {
        return this.post('/admin/' + coin + '/withdraw/estimate_fee', data);
    };
    Api.prototype.withdrawPending = function (coin, data) {
        return this.post('/admin/' + coin + '/withdraw', data);
    };
    // Statistics
    Api.prototype.getUserStatistics = function (data) {
        return this.get('/admin/user/statistics', data);
    };
    Api.prototype.getICOStatistics = function (data) {
        return this.get('/admin/ico/statistics', data);
    };
    Api.prototype.getLendingStatistics = function (data) {
        return this.get('/admin/lending/statistics', data);
    };
    Api.prototype.getStakingStatistics = function (data) {
        return this.get('/admin/staking/statistics', data);
    };
    Api.prototype.getFeeStatistics = function (data) {
        return this.get('/admin/fee/statistics', data);
    };
    Api.prototype.getAllTransactionHistory = function (coin, data) {
        return this.get('/admin/' + coin + '/transactions', data);
    };
    Api.prototype.getExchangeStatistics = function (coin, data) {
        return this.get('/exchange/' + coin + '/statistics', data);
    };
    Api.prototype.getExchangeHistory = function (coin, data) {
        return this.get('/exchange/' + coin + '/history', data);
    };
    // Ticket
    Api.prototype.getMyTicket = function (data) {
        return this.get('/ticket', data);
    };
    Api.prototype.getAllTicket = function (data) {
        return this.get('/ticket/all', data);
    };
    Api.prototype.getTicketDetail = function (data) {
        return this.get('/ticket/detail', data);
    };
    Api.prototype.postTicket = function (data) {
        return this.post('/ticket', data);
    };
    Api.prototype.replyTicket = function (data) {
        return this.post('/ticket/reply', data);
    };
    Api.prototype.closeTicket = function (data) {
        return this.post('/ticket/close', data);
    };
    Api = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* Http */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_4__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__angular_router__["c" /* Router */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__settings_settings_service__["a" /* SettingsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__settings_settings_service__["a" /* SettingsService */]) === "function" && _c || Object])
    ], Api);
    return Api;
    var _a, _b, _c;
}());

//# sourceMappingURL=api.service.js.map

/***/ }),

/***/ "../../../../../src/app/services/authguard.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthGuard; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__settings_settings_service__ = __webpack_require__("../../../../../src/app/services/settings/settings.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__api_service__ = __webpack_require__("../../../../../src/app/services/api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__balance_service__ = __webpack_require__("../../../../../src/app/services/balance.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/**
 * Created by ApolloYr on 11/17/2017.
 */





var AuthGuard = (function () {
    function AuthGuard(router, settings, api, balance) {
        this.router = router;
        this.settings = settings;
        this.api = api;
        this.balance = balance;
    }
    AuthGuard.prototype.resolve = function (route, state) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            if (_this.settings.getUserSetting('name')) {
                _this.canAccess(state.url, _this.settings.getUserSetting('role')) ? resolve(true) : reject('no privillege');
            }
            else if (_this.settings.getStorage('token')) {
                _this.api.getAccountInfo().subscribe(function (res) {
                    if (!_this.canAccess(state.url, res.role)) {
                        reject('no privillege');
                    }
                    else {
                        _this.settings.setAppSetting('is_loggedin', true);
                        _this.settings.user = res;
                        _this.balance.init();
                        resolve(true);
                    }
                }, function (err) {
                    reject('information is invalid');
                    _this.settings.setStorage('token', false);
                    _this.router.navigate(['/login']);
                });
            }
            else {
                reject('not logged in');
                _this.router.navigate(['/login']);
            }
        });
    };
    AuthGuard.prototype.canAccess = function (url, role) {
        if (url.startsWith('/admin') && role == 'admin') {
            return true;
        }
        else if (url.startsWith('/admin') && role != 'admin') {
            return false;
        }
        else {
            return true;
        }
    };
    AuthGuard = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__settings_settings_service__["a" /* SettingsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__settings_settings_service__["a" /* SettingsService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__api_service__["a" /* Api */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__api_service__["a" /* Api */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_4__balance_service__["a" /* BalanceService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__balance_service__["a" /* BalanceService */]) === "function" && _d || Object])
    ], AuthGuard);
    return AuthGuard;
    var _a, _b, _c, _d;
}());

//# sourceMappingURL=authguard.service.js.map

/***/ }),

/***/ "../../../../../src/app/services/balance.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BalanceService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__api_service__ = __webpack_require__("../../../../../src/app/services/api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__settings_settings_service__ = __webpack_require__("../../../../../src/app/services/settings/settings.service.ts");
/**
 * Created by ApolloYr on 11/17/2017.
 */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var BalanceService = (function () {
    function BalanceService(api, settings) {
        this.api = api;
        this.settings = settings;
    }
    BalanceService.prototype.init = function () {
        var _this = this;
        if (this.settings.getUserSetting('role') == 'admin') {
            return;
        }
        this.api.getSettings().subscribe(function (res) {
            if (res.success) {
                _this.settings.sys = res.data;
            }
        });
        this.getBtcAddress();
        this.getBt9Address();
        this.getEthAddress();
        this.getTokenBalance();
        // this.getUsdBalance();
    };
    BalanceService.prototype.getBtcAddress = function () {
        var _this = this;
        this.api.getCoinAddress('BTC', {}).subscribe(function (res) {
            if (res.success) {
                _this.settings.setUserSetting('btc_address', res.address);
                _this.getBtcBalance();
                // this.getEthAddress();
            }
        }, function (err) {
        });
    };
    BalanceService.prototype.getBt9Address = function () {
        var _this = this;
        this.api.getCoinAddress('BT9', {}).subscribe(function (res) {
            if (res.success) {
                _this.settings.setUserSetting('bt9_address', res.address);
            }
        }, function (err) {
        });
    };
    BalanceService.prototype.getBtcBalance = function () {
        var _this = this;
        this.api.getCoinBalance('BTC', {}).subscribe(function (res) {
            if (res.success) {
                _this.settings.setUserSetting('btc_balance', res.available_balance);
            }
        }, function (err) {
        });
    };
    BalanceService.prototype.getEthAddress = function () {
        var _this = this;
        this.api.getCoinAddress('ETH', {}).subscribe(function (res) {
            if (res.success) {
                _this.settings.setUserSetting('eth_address', res.address);
                _this.getEthBalance();
            }
        }, function (err) {
        });
    };
    BalanceService.prototype.getEthBalance = function () {
        var _this = this;
        this.api.getCoinBalance('ETH', {}).subscribe(function (res) {
            if (res.success) {
                _this.settings.setUserSetting('eth_balance', res.available_balance);
            }
        }, function (err) {
        });
    };
    BalanceService.prototype.getTokenBalance = function () {
        var _this = this;
        this.api.getTokenBalance({}).subscribe(function (res) {
            if (res.success) {
                _this.settings.setUserSetting('token_balance', res.balance);
            }
        }, function (err) {
        });
    };
    BalanceService.prototype.getUsdBalance = function () {
        var _this = this;
        this.api.getUsdBalance({}).subscribe(function (res) {
            if (res.success) {
                _this.settings.setUserSetting('usd_balance', res.balance);
            }
        }, function (err) {
        });
    };
    BalanceService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__api_service__["a" /* Api */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__api_service__["a" /* Api */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__settings_settings_service__["a" /* SettingsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__settings_settings_service__["a" /* SettingsService */]) === "function" && _b || Object])
    ], BalanceService);
    return BalanceService;
    var _a, _b;
}());

//# sourceMappingURL=balance.service.js.map

/***/ }),

/***/ "../../../../../src/app/services/notifications.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Notifications; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/**
 * Created by ApolloYr on 11/18/2017.
 */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var Notifications = (function () {
    function Notifications() {
    }
    Notifications.prototype.showNotification = function (message, from, align, type) {
        // const type = ['', 'info', 'success', 'warning', 'danger', 'rose', 'primary'];
        var color = Math.floor((Math.random() * 6) + 1);
        $.notify({
            icon: 'notifications',
            message: message
        }, {
            type: type,
            timer: 1000,
            placement: {
                from: from,
                align: align
            }
        });
    };
    Notifications = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], Notifications);
    return Notifications;
}());

//# sourceMappingURL=notifications.service.js.map

/***/ }),

/***/ "../../../../../src/app/services/securehttp.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SecureHttpService; });
/* unused harmony export SecureHttpProvider */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__("../../../../rxjs/Observable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var SecureHttpService = (function (_super) {
    __extends(SecureHttpService, _super);
    function SecureHttpService(backend, defaultOptions) {
        return _super.call(this, backend, defaultOptions) || this;
    }
    SecureHttpService.prototype.request = function (url, options) {
        return _super.prototype.request.call(this, url, options);
    };
    SecureHttpService.prototype.get = function (url, options) {
        // if (url.indexOf('/api')) this.events.publish('ajax:start');
        var _this = this;
        return _super.prototype.get.call(this, url, options).map(function (res) {
            if (url.indexOf('/api')) {
                // this.events.publish('ajax:stop');
                return res.json();
            }
            return res;
        }).catch(function (error) {
            return _this.handleError(url, error);
        });
    };
    SecureHttpService.prototype.post = function (url, options) {
        // if (url.indexOf('/api')) this.events.publish('ajax:start');
        var _this = this;
        return _super.prototype.post.call(this, url, options).map(function (res) {
            // if (url.indexOf('/api')) {
            //     this.events.publish('ajax:stop');
            //     return res.json();
            // }
            return res;
        }).catch(function (error) {
            return _this.handleError(url, error);
        });
    };
    SecureHttpService.prototype.handleError = function (url, error) {
        // if (url.indexOf('/api')) {
        //     this.events.publish('ajax:stop');
        //     this.events.publish('toast:presented', 'Sorry. error occured while connecting server');
        // }
        // return new Observable(observer => {
        //     observer.complete();
        // });
        // In a real world app, you might use a remote logging infrastructure
        var errMsg;
        if (error instanceof __WEBPACK_IMPORTED_MODULE_1__angular_http__["f" /* Response */]) {
            var body = error.json() || '';
            var err = body.error || JSON.stringify(body);
            errMsg = error.status + " - " + (error.statusText || '') + " " + err;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
        return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].throw(errMsg);
    };
    SecureHttpService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* ConnectionBackend */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* ConnectionBackend */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["e" /* RequestOptions */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["e" /* RequestOptions */]) === "function" && _b || Object])
    ], SecureHttpService);
    return SecureHttpService;
    var _a, _b;
}(__WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* Http */]));

function SecureHttpProvider(backend, defaultOptions) {
    return new SecureHttpService(backend, defaultOptions);
}
//# sourceMappingURL=securehttp.service.js.map

/***/ }),

/***/ "../../../../../src/app/services/services.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ServicesModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__settings_settings_service__ = __webpack_require__("../../../../../src/app/services/settings/settings.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__securehttp_service__ = __webpack_require__("../../../../../src/app/services/securehttp.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__api_service__ = __webpack_require__("../../../../../src/app/services/api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__authguard_service__ = __webpack_require__("../../../../../src/app/services/authguard.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__notifications_service__ = __webpack_require__("../../../../../src/app/services/notifications.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__validate_service__ = __webpack_require__("../../../../../src/app/services/validate.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__balance_service__ = __webpack_require__("../../../../../src/app/services/balance.service.ts");
/**
 * Created by ApolloYr on 11/18/2017.
 */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








var ServicesModule = (function () {
    function ServicesModule() {
    }
    ServicesModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [],
            declarations: [],
            providers: [
                __WEBPACK_IMPORTED_MODULE_1__settings_settings_service__["a" /* SettingsService */],
                __WEBPACK_IMPORTED_MODULE_2__securehttp_service__["a" /* SecureHttpService */],
                __WEBPACK_IMPORTED_MODULE_3__api_service__["a" /* Api */],
                __WEBPACK_IMPORTED_MODULE_4__authguard_service__["a" /* AuthGuard */],
                __WEBPACK_IMPORTED_MODULE_5__notifications_service__["a" /* Notifications */],
                __WEBPACK_IMPORTED_MODULE_6__validate_service__["a" /* Validate */],
                __WEBPACK_IMPORTED_MODULE_7__balance_service__["a" /* BalanceService */]
            ],
            exports: []
        })
    ], ServicesModule);
    return ServicesModule;
}());

//# sourceMappingURL=services.module.js.map

/***/ }),

/***/ "../../../../../src/app/services/settings/settings.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SettingsService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/**
 * Created by ApolloYr on 11/17/2017.
 */


var SettingsService = (function () {
    function SettingsService() {
        this.siteUrl = __WEBPACK_IMPORTED_MODULE_1__environments_environment__["a" /* environment */].server;
        this.apiUrl = __WEBPACK_IMPORTED_MODULE_1__environments_environment__["a" /* environment */].server + '/api';
        this.loading = false;
        this.storagePrefix = 'smitty_';
        // User settings
        this.user = {};
        this.sys = {};
        // App Settings
        this.app = {
            name: 'Bitcoin'
        };
    }
    SettingsService.prototype.getUserSetting = function (name) {
        return name ? this.user[name] : this.user;
    };
    SettingsService.prototype.setUserSetting = function (name, value) {
        this.user[name] = value;
    };
    SettingsService.prototype.getAppSetting = function (name) {
        return name ? this.app[name] : this.app;
    };
    SettingsService.prototype.setAppSetting = function (name, value) {
        if (typeof this.app[name] !== 'undefined') {
            this.app[name] = value;
        }
    };
    SettingsService.prototype.getSysSetting = function (name) {
        return this.sys[name] ? this.sys[name] : 0;
    };
    SettingsService.prototype.setSysSetting = function (name, value) {
        this.sys[name] = value;
    };
    SettingsService.prototype.clearUserSetting = function () {
        this.setStorage('user', false);
    };
    SettingsService.prototype.getStorage = function (key, defaultVal) {
        return window.localStorage[this.storagePrefix + key] ?
            JSON.parse(window.localStorage[this.storagePrefix + key]) : defaultVal || false;
    };
    ;
    SettingsService.prototype.setStorage = function (key, val) {
        window.localStorage.setItem(this.storagePrefix + key, JSON.stringify(val));
    };
    SettingsService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], SettingsService);
    return SettingsService;
}());

//# sourceMappingURL=settings.service.js.map

/***/ }),

/***/ "../../../../../src/app/services/validate.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Validate; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/**
 * Created by ApolloYr on 11/17/2017.
 */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var Validate = (function () {
    function Validate() {
    }
    Validate.prototype.isFieldValid = function (form, field) {
        return !form.get(field).valid && form.get(field).touched;
    };
    Validate.prototype.displayFieldCss = function (form, field) {
        return {
            'has-error': this.isFieldValid(form, field),
            'has-feedback': this.isFieldValid(form, field)
        };
    };
    Validate.prototype.validateAllFormFields = function (formGroup) {
        var _this = this;
        Object.keys(formGroup.controls).forEach(function (field) {
            var control = formGroup.get(field);
            if (control instanceof __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormControl"]) {
                control.markAsTouched({ onlySelf: true });
            }
            else if (control instanceof __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormGroup"]) {
                _this.validateAllFormFields(control);
            }
        });
    };
    Validate = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], Validate);
    return Validate;
}());

//# sourceMappingURL=validate.service.js.map

/***/ }),

/***/ "../../../../../src/app/shared/components/dialog/modal.dialog.component.html":
/***/ (function(module, exports) {

module.exports = "<div [@dialog] *ngIf=\"visible\" class=\"dialog\">\r\n    <ng-content></ng-content>\r\n    <button *ngIf=\"closable\" (click)=\"close()\" aria-label=\"Close\" class=\"dialog__close-btn\">X</button>\r\n</div>\r\n<div *ngIf=\"visible\" class=\"overlay\" (click)=\"close()\"></div>"

/***/ }),

/***/ "../../../../../src/app/shared/components/dialog/modal.dialog.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".overlay {\n  position: fixed;\n  top: 0;\n  bottom: 0;\n  left: 0;\n  right: 0;\n  background-color: rgba(0, 0, 0, 0.5);\n  z-index: 899; }\n\n.dialog {\n  z-index: 900;\n  position: fixed;\n  top: 120px;\n  left: 0;\n  right: 0;\n  margin-right: auto;\n  margin-left: auto;\n  width: 650px;\n  max-width: 1200px;\n  max-height: 80%;\n  overflow-y: auto;\n  background-color: #fff;\n  padding: 30px 20px 20px 20px;\n  box-shadow: 0 7px 8px -4px rgba(0, 0, 0, 0.2), 0 13px 19px 2px rgba(0, 0, 0, 0.14), 0 5px 24px 4px rgba(0, 0, 0, 0.12); }\n\n@media (min-width: 768px) {\n  .dialog {\n    top: 120px; } }\n\n.dialog__close-btn {\n  border: 0;\n  background: none;\n  color: #2d2d2d;\n  position: absolute;\n  top: 8px;\n  right: 8px;\n  font-size: 1.2em; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/shared/components/dialog/modal.dialog.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ModalDialogComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_animations__ = __webpack_require__("../../../animations/@angular/animations.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ModalDialogComponent = (function () {
    function ModalDialogComponent() {
        this.closable = true;
        this.visibleChange = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
    }
    ModalDialogComponent.prototype.ngOnInit = function () { };
    ModalDialogComponent.prototype.close = function () {
        if (!this.closable) {
            return;
        }
        this.visible = false;
        this.visibleChange.emit(this.visible);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], ModalDialogComponent.prototype, "closable", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Boolean)
    ], ModalDialogComponent.prototype, "visible", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]) === "function" && _a || Object)
    ], ModalDialogComponent.prototype, "visibleChange", void 0);
    ModalDialogComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'modal-dialog',
            template: __webpack_require__("../../../../../src/app/shared/components/dialog/modal.dialog.component.html"),
            styles: [__webpack_require__("../../../../../src/app/shared/components/dialog/modal.dialog.component.scss")],
            animations: [
                Object(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["k" /* trigger */])('dialog', [
                    Object(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["j" /* transition */])('void => *', [
                        Object(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["i" /* style */])({ transform: 'scale3d(.3, .3, .3)' }),
                        Object(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["e" /* animate */])(100)
                    ]),
                    Object(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["j" /* transition */])('* => void', [
                        Object(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["e" /* animate */])(100, Object(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["i" /* style */])({ transform: 'scale3d(.0, .0, .0)' }))
                    ])
                ])
            ]
        }),
        __metadata("design:paramtypes", [])
    ], ModalDialogComponent);
    return ModalDialogComponent;
    var _a;
}());

//# sourceMappingURL=modal.dialog.component.js.map

/***/ }),

/***/ "../../../../../src/app/shared/components/field-error-display/field-error-display.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".error-msg {\n  color: #a94442;\n}\n.fix-error-icon {\n  top: 27px;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/shared/components/field-error-display/field-error-display.component.html":
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"displayError\">\n    <!-- <span class=\"glyphicon glyphicon-remove form-control-feedback fix-error-icon\"></span> -->\n    <span class=\"sr-only\">(error)</span>\n    <small class=\"text-danger\">\n        {{ errorMsg }}\n    </small>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/shared/components/field-error-display/field-error-display.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FieldErrorDisplayComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var FieldErrorDisplayComponent = (function () {
    function FieldErrorDisplayComponent() {
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", String)
    ], FieldErrorDisplayComponent.prototype, "errorMsg", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Boolean)
    ], FieldErrorDisplayComponent.prototype, "displayError", void 0);
    FieldErrorDisplayComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-field-error-display',
            template: __webpack_require__("../../../../../src/app/shared/components/field-error-display/field-error-display.component.html"),
            styles: [__webpack_require__("../../../../../src/app/shared/components/field-error-display/field-error-display.component.css")]
        })
    ], FieldErrorDisplayComponent);
    return FieldErrorDisplayComponent;
}());

//# sourceMappingURL=field-error-display.component.js.map

/***/ }),

/***/ "../../../../../src/app/shared/components/loader/loader.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"loader\">\r\n    <svg class=\"circular\" viewBox=\"25 25 50 50\">\r\n        <circle class=\"path\" cx=\"50\" cy=\"50\" r=\"20\" fill=\"none\" stroke-width=\"2\" stroke-miterlimit=\"10\"/>\r\n    </svg>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/shared/components/loader/loader.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".loader {\n  margin-top: 40px;\n  position: static;\n  width: 40px; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/shared/components/loader/loader.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoaderComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var LoaderComponent = (function () {
    function LoaderComponent() {
    }
    LoaderComponent.prototype.ngOnInit = function () { };
    LoaderComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'loader',
            template: __webpack_require__("../../../../../src/app/shared/components/loader/loader.component.html"),
            styles: [__webpack_require__("../../../../../src/app/shared/components/loader/loader.component.scss")],
        }),
        __metadata("design:paramtypes", [])
    ], LoaderComponent);
    return LoaderComponent;
}());

//# sourceMappingURL=loader.component.js.map

/***/ }),

/***/ "../../../../../src/app/shared/components/loading/loading.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"loading\">\r\n    <svg class=\"circular\" viewBox=\"25 25 50 50\">\r\n        <circle class=\"path\" cx=\"50\" cy=\"50\" r=\"20\" fill=\"none\" stroke-width=\"2\" stroke-miterlimit=\"10\" />\r\n    </svg>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/shared/components/loading/loading.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".loading {\n  background: rgba(255, 255, 255, 0.5);\n  width: 100%;\n  height: 100%;\n  top: 0px;\n  position: fixed;\n  z-index: 8000 !important; }\n\n.circular {\n  width: 10%; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/shared/components/loading/loading.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoadingComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var LoadingComponent = (function () {
    function LoadingComponent() {
    }
    LoadingComponent.prototype.ngOnInit = function () { };
    LoadingComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'loading',
            template: __webpack_require__("../../../../../src/app/shared/components/loading/loading.component.html"),
            styles: [__webpack_require__("../../../../../src/app/shared/components/loading/loading.component.scss")],
        }),
        __metadata("design:paramtypes", [])
    ], LoadingComponent);
    return LoadingComponent;
}());

//# sourceMappingURL=loading.component.js.map

/***/ }),

/***/ "../../../../../src/app/shared/components/password-validator.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PasswordValidation; });
var PasswordValidation = (function () {
    function PasswordValidation() {
    }
    PasswordValidation.MatchPassword = function (AC) {
        var password = AC.get('password').value; // to get value in input tag
        var confirmPassword = AC.get('confirmPassword').value; // to get value in input tag
        if (password !== confirmPassword) {
            AC.get('confirmPassword').setErrors({ MatchPassword: true });
        }
        else {
            return null;
        }
    };
    return PasswordValidation;
}());

//# sourceMappingURL=password-validator.component.js.map

/***/ }),

/***/ "../../../../../src/app/shared/footer/footer.component.html":
/***/ (function(module, exports) {

module.exports = "<footer class=\"footer\">\n    <div class=\"container-fluid\">\n        <!--<nav class=\"pull-left\">-->\n            <!--<ul>-->\n                <!--<li>-->\n                    <!--<a href=\"#\">-->\n                        <!--Home-->\n                    <!--</a>-->\n                <!--</li>-->\n                <!--<li>-->\n                    <!--<a href=\"#\">-->\n                        <!--Company-->\n                    <!--</a>-->\n                <!--</li>-->\n                <!--<li>-->\n                    <!--<a href=\"#\">-->\n                        <!--Portfolio-->\n                    <!--</a>-->\n                <!--</li>-->\n                <!--<li>-->\n                    <!--<a href=\"#\">-->\n                        <!--Blog-->\n                    <!--</a>-->\n                <!--</li>-->\n            <!--</ul>-->\n        <!--</nav>-->\n        <p class=\"copyright pull-right\">\n            &copy; 2018 BT9Coin, All Rights Reserved\n        </p>\n    </div>\n</footer>\n"

/***/ }),

/***/ "../../../../../src/app/shared/footer/footer.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FooterComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var FooterComponent = (function () {
    function FooterComponent() {
        this.test = new Date();
    }
    FooterComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-footer-cmp',
            template: __webpack_require__("../../../../../src/app/shared/footer/footer.component.html")
        })
    ], FooterComponent);
    return FooterComponent;
}());

//# sourceMappingURL=footer.component.js.map

/***/ }),

/***/ "../../../../../src/app/shared/footer/footer.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FooterModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__footer_component__ = __webpack_require__("../../../../../src/app/shared/footer/footer.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var FooterModule = (function () {
    function FooterModule() {
    }
    FooterModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [__WEBPACK_IMPORTED_MODULE_2__angular_router__["d" /* RouterModule */], __WEBPACK_IMPORTED_MODULE_1__angular_common__["b" /* CommonModule */]],
            declarations: [__WEBPACK_IMPORTED_MODULE_3__footer_component__["a" /* FooterComponent */]],
            exports: [__WEBPACK_IMPORTED_MODULE_3__footer_component__["a" /* FooterComponent */]]
        })
    ], FooterModule);
    return FooterModule;
}());

//# sourceMappingURL=footer.module.js.map

/***/ }),

/***/ "../../../../../src/app/shared/navbar/navbar.component.html":
/***/ (function(module, exports) {

module.exports = "<nav #navbar class=\"navbar navbar-transparent navbar-absolute\">\n    <div class=\"container-fluid\">\n        <div class=\"navbar-minimize\">\n            <button id=\"minimizeSidebar\" class=\"btn btn-round btn-white btn-fill btn-just-icon\">\n                <i class=\"material-icons visible-on-sidebar-regular\">more_vert</i>\n                <i class=\"material-icons visible-on-sidebar-mini\">view_list</i>\n            </button>\n        </div>\n        <div class=\"navbar-header\">\n            <button type=\"button\" class=\"navbar-toggle\" (click)=\"sidebarToggle()\">\n                <span class=\"sr-only\">Toggle navigation</span>\n                <span class=\"icon-bar\"></span>\n                <span class=\"icon-bar\"></span>\n                <span class=\"icon-bar\"></span>\n            </button>\n            <a class=\"navbar-brand\" href=\"{{getPath()}}\"> {{getTitle()}} </a>\n        </div>\n        <div class=\"collapse navbar-collapse\">\n            <div *ngIf=\"isMobileMenu() && settings.getUserSetting('role') != 'admin'\">\n                <div class=\"navbar-right\">\n                    <!--<b class=\" text-black dib header-wallet\">-->\n                        <!--<div class=\"light header-wallet-text\"> Lending Balance</div>-->\n                        <!--<div class=\"w100p dib text-black text-center header-wallet-balance-text\">-->\n                            <!--<i class=\"fa fa-usd\"></i>-->\n                            <!--<span id=\"usd_wallet_balance\">-->\n                                <!--{{settings.getUserSetting('usd_balance') | number: '1.2-2'}}-->\n                            <!--</span>-->\n                        <!--</div>-->\n                    <!--</b>-->\n                    <!--<button class=\"btn btn-success btn-sm btn-nav\">&lt;&lt; Exchange &gt;&gt;</button>-->\n                    <b class=\" text-black dib header-wallet\">\n                        <div class=\"light header-wallet-text\"> BT9Coin Balance</div>\n                        <div class=\"w100p dib text-black text-center header-wallet-balance-text\">\n                            <img src=\"assets/img/B9_black.png\" style=\"height: 18px;\">\n                            <span id=\"ethconnect_wallet_balance\">\n                                {{settings.getUserSetting('token_balance') | number: '1.8-8'}}\n                            </span>\n                        </div>\n                    </b>\n                    <!--<button class=\"btn btn-success btn-sm btn-nav\">&lt;&lt; Exchange &gt;&gt;</button>-->\n                    <b class=\" text-black dib header-wallet\">\n                        <div class=\"light header-wallet-text\"> Bitcoin Balance</div>\n                        <div class=\"w100p dib text-black text-center header-wallet-balance-text\">\n                            <i class=\"fa fa-bitcoin\"></i>\n                            <span>\n                                {{settings.getUserSetting('btc_balance') | number:'1.8-8'}}\n                            </span>\n                        </div>\n                    </b>\n                    <b class=\" text-black dib header-wallet\">\n                        <div class=\"light header-wallet-text\"> Ethereum Balance</div>\n                        <div class=\"w100p dib text-black text-center header-wallet-balance-text\">\n                            <img src=\"assets/img/eth_29x29.png\" style=\"height: 18px;\">\n                            <span>\n                                {{settings.getUserSetting('eth_balance') | number:'1.8-8'}}\n                            </span>\n                        </div>\n                    </b>\n                    <!--<b class=\" text-black dib header-wallet\">-->\n                        <!--<div class=\"light header-wallet-text\"> Ethereum Balance</div>-->\n                        <!--<div class=\"w100p dib text-black text-center header-wallet-balance-text\">-->\n                            <!--<img src=\"/assets/img/eth_29x29.png\" style=\"padding-bottom: 3px;height: 20px;\">-->\n                            <!--<span id=\"ethereum_wallet_balance\">-->\n                                <!--0.00000000-->\n                            <!--</span>-->\n                        <!--</div>-->\n                    <!--</b>-->\n                    <form class=\"admin-form  form-link-ref  mt5  w70p dib\">\n                        <div class=\"form-group mb10\" style=\"margin-right: 5px!important;\">\n                            <span style=\"color: #0b0e16;font-size: 13px\"><b>Earn by referring new members</b></span><br>\n                            <label class=\"field prepend-icon\" style=\"width: auto\">\n                                <input class=\"gui-input gui-input-ref\" id=\"referral-link-header\" name=\"address\" value=\"{{ref_url}}\" readonly=\"\" style=\"min-width: 300px\" type=\"text\">\n                                <a class=\"field-icon field-icon-ref\" style=\"display: inline-block!important;\" title=\"Copy your referral link\" (click)=\"copyRefLink()\">\n                                    <i class=\"fa fa-link copy-link-ref\" style=\"font-size: 13px!important;\"> Copy</i>\n                                </a>\n                                <span class=\"link-copied-message\" id=\"link-copied-message\"> Link copied</span>\n                            </label>\n                        </div>\n                    </form>\n                </div>\n            </div>\n        </div>\n    </div>\n</nav>\n"

/***/ }),

/***/ "../../../../../src/app/shared/navbar/navbar.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "@media only screen and (max-width: 700px) {\n  .header-wallet-balance-text,\n  .header-wallet-text {\n    padding-left: 0px !important;\n    padding-right: 0px !important; } }\n\n.header-wallet {\n  border: 1px solid #c2c8d8;\n  padding: 4px;\n  border-radius: 4px; }\n\n@media (max-width: 991px) {\n  .header-wallet {\n    width: 100%;\n    text-align: center;\n    background-color: #fff; } }\n\n.header-wallet-balance-text {\n  padding-left: 10px;\n  padding-right: 10px; }\n\n.header-wallet-text {\n  padding-left: 10px;\n  padding-right: 10px;\n  font-size: 12px;\n  text-align: center;\n  width: 100%;\n  padding-top: 0; }\n\n.text-black {\n  color: black !important; }\n\n.dib {\n  display: inline-block; }\n\n.gui-input-address {\n  border: 1px solid #cadb47 !important;\n  padding-left: 10px !important;\n  font-size: 11px !important;\n  padding-right: 82px !important;\n  height: 35px !important;\n  color: black !important; }\n\n.gui-input-ref {\n  border: 1px solid rgba(51, 122, 183, 0.28) !important;\n  padding-left: 10px !important;\n  font-size: 11px !important;\n  padding-right: 10px !important;\n  height: 35px !important;\n  color: #626262; }\n\n@media (max-width: 552px) {\n  .gui-input-ref {\n    width: 205px !important; }\n  .form-ref-link {\n    margin-left: 0 !important; } }\n\n.admin-form .append-icon .field-icon, .admin-form .prepend-icon .field-icon {\n  top: 0;\n  z-index: 4;\n  width: 42px;\n  height: 42px;\n  color: inherit;\n  line-height: 42px;\n  position: absolute;\n  text-align: center;\n  transition: all .5s ease-out;\n  pointer-events: none; }\n\n.link-copied-message {\n  background-color: black;\n  padding: 5px;\n  border-radius: 10px;\n  color: white;\n  position: absolute;\n  right: 2px;\n  top: -5px;\n  padding-left: 10px;\n  padding-right: 10px;\n  font-size: 9px;\n  font-weight: bold;\n  display: none; }\n\n.address-btn-link {\n  position: absolute !important;\n  top: 0 !important;\n  bottom: 0 !important;\n  left: 0 !important;\n  right: 0 !important;\n  height: auto !important;\n  background: #cadb47 !important;\n  width: auto !important;\n  color: black !important;\n  padding-top: 13% !important;\n  cursor: pointer !important;\n  border-radius: 0px 5px 5px 0px !important; }\n\n.copy-link-ref {\n  position: absolute !important;\n  margin-top: 21px;\n  margin-bottom: 16px;\n  top: 0 !important;\n  bottom: 0 !important;\n  left: 0 !important;\n  right: 0 !important;\n  height: auto !important;\n  background: #00bcd4 !important;\n  width: auto !important;\n  color: white !important;\n  padding-top: 13% !important;\n  cursor: pointer !important;\n  font-size: 16px !important;\n  border-radius: 0px 5px 5px 0px !important; }\n\n.field-icon-ref {\n  pointer-events: all !important;\n  cursor: pointer !important;\n  width: 75px !important;\n  right: 0 !important;\n  left: auto !important;\n  bottom: 0 !important;\n  height: auto !important; }\n\n.form-link-ref {\n  margin-top: -25px; }\n\n.pt14 {\n  padding-top: 14px !important; }\n\n.w45p {\n  width: 45%; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/shared/navbar/navbar.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NavbarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__sidebar_sidebar_component__ = __webpack_require__("../../../../../src/app/sidebar/sidebar.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_api_service__ = __webpack_require__("../../../../../src/app/services/api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_settings_settings_service__ = __webpack_require__("../../../../../src/app/services/settings/settings.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var misc = {
    navbar_menu_visible: 0,
    active_collapse: true,
    disabled_collapse_init: 0,
};
var NavbarComponent = (function () {
    function NavbarComponent(location, renderer, element, api, settings) {
        this.renderer = renderer;
        this.element = element;
        this.api = api;
        this.settings = settings;
        this.ref_url = "";
        this.location = location;
        this.nativeElement = element.nativeElement;
        this.sidebarVisible = false;
    }
    NavbarComponent.prototype.ngOnInit = function () {
        this.ref_url = this.settings.siteUrl + '/ref/' + this.settings.getUserSetting('username');
        if (this.settings.getUserSetting('role') == 'admin') {
            this.listTitles = __WEBPACK_IMPORTED_MODULE_1__sidebar_sidebar_component__["a" /* ADMIN_ROUTES */].filter(function (listTitle) { return listTitle; });
        }
        else {
            this.listTitles = __WEBPACK_IMPORTED_MODULE_1__sidebar_sidebar_component__["b" /* ROUTES */].filter(function (listTitle) { return listTitle; });
        }
        var navbar = this.element.nativeElement;
        this.toggleButton = navbar.getElementsByClassName('navbar-toggle')[0];
        if ($('body').hasClass('sidebar-mini')) {
            misc.sidebar_mini_active = true;
        }
        if ($('body').hasClass('hide-sidebar')) {
            misc.hide_sidebar_active = true;
        }
        $('#minimizeSidebar').click(function () {
            if (misc.sidebar_mini_active === true) {
                $('body').removeClass('sidebar-mini');
                misc.sidebar_mini_active = false;
            }
            else {
                setTimeout(function () {
                    $('body').addClass('sidebar-mini');
                    misc.sidebar_mini_active = true;
                }, 300);
            }
            // we simulate the window Resize so the charts will get updated in realtime.
            var simulateWindowResize = setInterval(function () {
                window.dispatchEvent(new Event('resize'));
            }, 180);
            // we stop the simulation of Window Resize after the animations are completed
            setTimeout(function () {
                clearInterval(simulateWindowResize);
            }, 1000);
        });
        $('#hideSidebar').click(function () {
            if (misc.hide_sidebar_active === true) {
                setTimeout(function () {
                    $('body').removeClass('hide-sidebar');
                    misc.hide_sidebar_active = false;
                }, 300);
                setTimeout(function () {
                    $('.sidebar').removeClass('animation');
                }, 600);
                $('.sidebar').addClass('animation');
            }
            else {
                setTimeout(function () {
                    $('body').addClass('hide-sidebar');
                    // $('.sidebar').addClass('animation');
                    misc.hide_sidebar_active = true;
                }, 300);
            }
            // we simulate the window Resize so the charts will get updated in realtime.
            var simulateWindowResize = setInterval(function () {
                window.dispatchEvent(new Event('resize'));
            }, 180);
            // we stop the simulation of Window Resize after the animations are completed
            setTimeout(function () {
                clearInterval(simulateWindowResize);
            }, 1000);
        });
    };
    NavbarComponent.prototype.isMobileMenu = function () {
        if ($(window).width() < 991) {
            return false;
        }
        return true;
    };
    NavbarComponent.prototype.sidebarOpen = function () {
        var toggleButton = this.toggleButton;
        var body = document.getElementsByTagName('body')[0];
        setTimeout(function () {
            toggleButton.classList.add('toggled');
        }, 500);
        body.classList.add('nav-open');
        this.sidebarVisible = true;
    };
    ;
    NavbarComponent.prototype.sidebarClose = function () {
        var body = document.getElementsByTagName('body')[0];
        this.toggleButton.classList.remove('toggled');
        this.sidebarVisible = false;
        body.classList.remove('nav-open');
    };
    ;
    NavbarComponent.prototype.sidebarToggle = function () {
        // const toggleButton = this.toggleButton;
        // const body = document.getElementsByTagName('body')[0];
        if (this.sidebarVisible === false) {
            this.sidebarOpen();
        }
        else {
            this.sidebarClose();
        }
    };
    ;
    NavbarComponent.prototype.getTitle = function () {
        var titlee = this.location.prepareExternalUrl(this.location.path());
        for (var i = 0; i < this.listTitles.length; i++) {
            if (this.listTitles[i].type === "link" && this.listTitles[i].path === titlee) {
                return this.listTitles[i].title;
            }
            else if (this.listTitles[i].type === "sub") {
                for (var j = 0; j < this.listTitles[i].children.length; j++) {
                    var subtitle = this.listTitles[i].path + '/' + this.listTitles[i].children[j].path;
                    if (subtitle === titlee) {
                        return this.listTitles[i].children[j].title;
                    }
                }
            }
        }
        return 'Dashboard';
    };
    NavbarComponent.prototype.getPath = function () {
        return this.location.prepareExternalUrl(this.location.path());
    };
    NavbarComponent.prototype.copyRefLink = function () {
        $('#link-copied-message').show();
        setTimeout(function () {
            $('#link-copied-message').hide();
        }, 1000);
        $('#referral-link-header').select();
        document.execCommand('copy');
        // if (typeof document.selection != 'undefined') {
        //     document.selection.empty();
        //     $('#referral-link-header').blur();
        // } else {
        window.getSelection().removeAllRanges();
        // }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('app-navbar-cmp'),
        __metadata("design:type", Object)
    ], NavbarComponent.prototype, "button", void 0);
    NavbarComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-navbar-cmp',
            template: __webpack_require__("../../../../../src/app/shared/navbar/navbar.component.html"),
            styles: [__webpack_require__("../../../../../src/app/shared/navbar/navbar.component.scss")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__angular_common__["f" /* Location */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_common__["f" /* Location */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["Renderer"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["Renderer"]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_3__services_api_service__["a" /* Api */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services_api_service__["a" /* Api */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_4__services_settings_settings_service__["a" /* SettingsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__services_settings_settings_service__["a" /* SettingsService */]) === "function" && _e || Object])
    ], NavbarComponent);
    return NavbarComponent;
    var _a, _b, _c, _d, _e;
}());

//# sourceMappingURL=navbar.component.js.map

/***/ }),

/***/ "../../../../../src/app/shared/navbar/navbar.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NavbarModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__navbar_component__ = __webpack_require__("../../../../../src/app/shared/navbar/navbar.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var NavbarModule = (function () {
    function NavbarModule() {
    }
    NavbarModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [__WEBPACK_IMPORTED_MODULE_2__angular_router__["d" /* RouterModule */], __WEBPACK_IMPORTED_MODULE_1__angular_common__["b" /* CommonModule */]],
            declarations: [__WEBPACK_IMPORTED_MODULE_3__navbar_component__["a" /* NavbarComponent */]],
            exports: [__WEBPACK_IMPORTED_MODULE_3__navbar_component__["a" /* NavbarComponent */]]
        })
    ], NavbarModule);
    return NavbarModule;
}());

//# sourceMappingURL=navbar.module.js.map

/***/ }),

/***/ "../../../../../src/app/shared/shared.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SharedModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__footer_footer_module__ = __webpack_require__("../../../../../src/app/shared/footer/footer.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__navbar_navbar_module__ = __webpack_require__("../../../../../src/app/shared/navbar/navbar.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__components_field_error_display_field_error_display_component__ = __webpack_require__("../../../../../src/app/shared/components/field-error-display/field-error-display.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__components_dialog_modal_dialog_component__ = __webpack_require__("../../../../../src/app/shared/components/dialog/modal.dialog.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__components_loader_loader_component__ = __webpack_require__("../../../../../src/app/shared/components/loader/loader.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__components_loading_loading_component__ = __webpack_require__("../../../../../src/app/shared/components/loading/loading.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var SharedModule = (function () {
    // https://github.com/ocombe/ng2-translate/issues/209
    function SharedModule() {
    }
    SharedModule_1 = SharedModule;
    SharedModule.forRoot = function () {
        return {
            ngModule: SharedModule_1
        };
    };
    SharedModule = SharedModule_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            exports: [
                __WEBPACK_IMPORTED_MODULE_3__footer_footer_module__["a" /* FooterModule */],
                __WEBPACK_IMPORTED_MODULE_4__navbar_navbar_module__["a" /* NavbarModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormsModule"],
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["ReactiveFormsModule"],
                __WEBPACK_IMPORTED_MODULE_5__components_field_error_display_field_error_display_component__["a" /* FieldErrorDisplayComponent */],
                __WEBPACK_IMPORTED_MODULE_6__components_dialog_modal_dialog_component__["a" /* ModalDialogComponent */],
                __WEBPACK_IMPORTED_MODULE_7__components_loader_loader_component__["a" /* LoaderComponent */],
                __WEBPACK_IMPORTED_MODULE_8__components_loading_loading_component__["a" /* LoadingComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_common__["b" /* CommonModule */],
                __WEBPACK_IMPORTED_MODULE_3__footer_footer_module__["a" /* FooterModule */],
                __WEBPACK_IMPORTED_MODULE_4__navbar_navbar_module__["a" /* NavbarModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormsModule"],
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["ReactiveFormsModule"]
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_5__components_field_error_display_field_error_display_component__["a" /* FieldErrorDisplayComponent */],
                __WEBPACK_IMPORTED_MODULE_6__components_dialog_modal_dialog_component__["a" /* ModalDialogComponent */],
                __WEBPACK_IMPORTED_MODULE_7__components_loader_loader_component__["a" /* LoaderComponent */],
                __WEBPACK_IMPORTED_MODULE_8__components_loading_loading_component__["a" /* LoadingComponent */]
            ]
        })
        // https://github.com/ocombe/ng2-translate/issues/209
    ], SharedModule);
    return SharedModule;
    var SharedModule_1;
}());

//# sourceMappingURL=shared.module.js.map

/***/ }),

/***/ "../../../../../src/app/sidebar/sidebar.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"logo\">\n    <div class=\"logo-normal\">\n        <!--<a href=\"\" class=\"simple-text\" *ngIf=\"settings.getUserSetting('role') == 'admin'\">-->\n        <!--Admin-->\n        <!--</a>-->\n        <a href=\"\" class=\"simple-text\" style=\"margin-left: 70px; text-align: left;\">\n            BT9Coin\n        </a>\n    </div>\n\n    <div class=\"logo-img\">\n        <img src=\"/assets/img/B9_color.png\" />\n    </div>\n</div>\n\n<div class=\"sidebar-wrapper\">\n    <div class=\"user\">\n        <div class=\"photo\">\n            <img src=\"assets/img/default-avatar.png\"/>\n        </div>\n        <div class=\"info\">\n            <a data-toggle=\"collapse\" href=\"#collapseExample\" class=\"collapsed\">\n                        <span>\n                            {{fullname}}\n                            <b class=\"caret\"></b>\n                        </span>\n            </a>\n            <div class=\"collapse\" id=\"collapseExample\">\n                <ul class=\"nav\">\n                    <li>\n                        <a href=\"javascript:void(0)\" routerLink=\"/security\">\n                            <span class=\"sidebar-mini\">SR</span>\n                            <span class=\"sidebar-normal\">Security</span>\n                            <span class=\"label label-danger\" style=\"margin-left: 55px; border-radius: 0px;\"\n                                  *ngIf=\"settings.getUserSetting('allow_g2f') == 0\">LOW</span>\n                            <span class=\"label label-success\" style=\"margin-left: 55px; border-radius: 0px;\"\n                                  *ngIf=\"settings.getUserSetting('allow_g2f') == 1\">GOOD</span>\n                        </a>\n                    </li>\n                    <li>\n                        <a href=\"javascript:void(0)\" (click)=\"onLogout()\">\n                            <span class=\"sidebar-mini\">L</span>\n                            <span class=\"sidebar-normal\">Logout</span>\n                        </a>\n                    </li>\n                </ul>\n            </div>\n        </div>\n    </div>\n    <div *ngIf=\"isMobileMenu()\">\n    </div>\n    <div class=\"nav-container\">\n        <ul class=\"nav\">\n            <li routerLinkActive=\"active\" *ngFor=\"let menuitem of menuItems\">\n                <!--If is a single link-->\n                <a [routerLink]=\"[menuitem.path]\" *ngIf=\"menuitem.type === 'link'\" (click)=\"onClickMenu()\">\n                    <i class=\"material-icons\">{{menuitem.icontype}}</i>\n                    <p>{{menuitem.title}}</p>\n                </a>\n                <!--If it have a submenu-->\n                <a data-toggle=\"collapse\" href=\"#{{menuitem.collapse}}\" *ngIf=\"menuitem.type === 'sub'\"\n                   (click)=\"updatePS()\">\n                    <i class=\"material-icons\">{{menuitem.icontype}}</i>\n                    <p>{{menuitem.title}}<b class=\"caret\"></b></p>\n                </a>\n\n                <!--Display the submenu items-->\n                <div id=\"{{menuitem.collapse}}\" class=\"collapse\" *ngIf=\"menuitem.type === 'sub'\">\n                    <ul class=\"nav\">\n                        <li routerLinkActive=\"active\" *ngFor=\"let childitem of menuitem.children\">\n                            <a [routerLink]=\"[menuitem.path, childitem.path]\">\n                                <span class=\"sidebar-mini\">{{childitem.ab}}</span>\n                                <span class=\"sidebar-normal\">{{childitem.title}}</span>\n                            </a>\n                        </li>\n                    </ul>\n                </div>\n            </li>\n        </ul>\n    </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/sidebar/sidebar.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return ROUTES; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ADMIN_ROUTES; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return SidebarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_perfect_scrollbar__ = __webpack_require__("../../../../perfect-scrollbar/dist/perfect-scrollbar.esm.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_settings_settings_service__ = __webpack_require__("../../../../../src/app/services/settings/settings.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




//Menu Items
var ROUTES = [
    {
        path: '/dashboard',
        title: 'Dashboard',
        type: 'link',
        icontype: 'dashboard'
    }, {
        path: '/ico',
        title: 'ICO',
        type: 'link',
        icontype: 'donut_small'
    }, {
        path: '/wallet',
        title: 'Wallet',
        type: 'link',
        icontype: 'account_balance_wallet'
    }, {
        path: '/transaction',
        title: 'Transaction',
        type: 'link',
        icontype: 'assignment'
    }, {
        path: '/exchange',
        title: 'Exchange',
        type: 'sub',
        icontype: 'compare_arrows',
        collapse: 'exchange',
        children: [
            { path: 'btc', title: 'BTC Exchange', ab: '' },
            { path: 'eth', title: 'ETH Exchange', ab: '' },
        ]
    }, {
        path: '/lending',
        title: 'Lending',
        type: 'link',
        icontype: 'device_hub'
    }, {
        path: '/staking',
        title: 'Staking',
        type: 'link',
        icontype: 'play_for_work'
    }, {
        path: '/referral',
        title: 'Referral',
        type: 'link',
        icontype: 'share'
    }, {
        path: '/support',
        title: 'Support',
        type: 'link',
        icontype: 'forum'
    },
];
//Menu Items
var ADMIN_ROUTES = [
    {
        path: '/admin/dashboard',
        title: 'Dashboard',
        type: 'link',
        icontype: 'dashboard'
    },
    {
        path: '/admin/withdraw',
        title: 'Withdraw Pendings',
        type: 'link',
        icontype: 'build'
    },
    {
        path: '/admin/transaction',
        title: 'Transaction History',
        type: 'link',
        icontype: 'restore'
    },
    {
        path: '/admin/agenda',
        title: 'ICO Agenda',
        type: 'link',
        icontype: 'assignment'
    },
    {
        path: '/admin/lending',
        title: 'Lending',
        type: 'link',
        icontype: 'device_hub'
    }, {
        path: '/admin/staking',
        title: 'Staking',
        type: 'link',
        icontype: 'play_for_work'
    },
    {
        path: '/admin/users',
        title: 'Users',
        type: 'link',
        icontype: 'people'
    },
    {
        path: '/admin/settings',
        title: 'Settings',
        type: 'link',
        icontype: 'settings'
    },
    {
        path: '/admin/adminuser',
        title: 'Admin Users',
        type: 'link',
        icontype: 'person_add'
    }, {
        path: '/admin/support',
        title: 'Support',
        type: 'link',
        icontype: 'forum'
    },
];
var SidebarComponent = (function () {
    function SidebarComponent(settings, router) {
        this.settings = settings;
        this.router = router;
    }
    SidebarComponent.prototype.isMobileMenu = function () {
        if ($(window).width() > 991) {
            return false;
        }
        return true;
    };
    ;
    SidebarComponent.prototype.ngOnInit = function () {
        if (this.settings.getUserSetting('role') == 'admin') {
            this.menuItems = ADMIN_ROUTES.filter(function (menuItem) { return menuItem; });
        }
        else {
            this.menuItems = ROUTES.filter(function (menuItem) { return menuItem; });
        }
        this.fullname = this.settings.getUserSetting('fullname');
    };
    SidebarComponent.prototype.updatePS = function () {
        if (window.matchMedia("(min-width: 960px)").matches && !this.isMac()) {
            var elemSidebar = document.querySelector('.sidebar .sidebar-wrapper');
            var ps = new __WEBPACK_IMPORTED_MODULE_1_perfect_scrollbar__["a" /* default */](elemSidebar, { wheelSpeed: 2, suppressScrollX: true });
        }
    };
    SidebarComponent.prototype.isMac = function () {
        var bool = false;
        if (navigator.platform.toUpperCase().indexOf('MAC') >= 0 || navigator.platform.toUpperCase().indexOf('IPAD') >= 0) {
            bool = true;
        }
        return bool;
    };
    SidebarComponent.prototype.onLogout = function () {
        this.settings.setAppSetting('is_loggedin', false);
        this.settings.user = {};
        this.settings.setStorage('token', false);
        this.router.navigate(['/login']);
    };
    SidebarComponent.prototype.onClickMenu = function () {
        $('.main-panel')[0].scrollTop = 0;
    };
    SidebarComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-sidebar-cmp',
            template: __webpack_require__("../../../../../src/app/sidebar/sidebar.component.html"),
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__services_settings_settings_service__["a" /* SettingsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_settings_settings_service__["a" /* SettingsService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_router__["c" /* Router */]) === "function" && _b || Object])
    ], SidebarComponent);
    return SidebarComponent;
    var _a, _b;
}());

//# sourceMappingURL=sidebar.component.js.map

/***/ }),

/***/ "../../../../../src/app/sidebar/sidebar.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SidebarModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__sidebar_component__ = __webpack_require__("../../../../../src/app/sidebar/sidebar.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var SidebarModule = (function () {
    function SidebarModule() {
    }
    SidebarModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [__WEBPACK_IMPORTED_MODULE_2__angular_router__["d" /* RouterModule */], __WEBPACK_IMPORTED_MODULE_1__angular_common__["b" /* CommonModule */]],
            declarations: [__WEBPACK_IMPORTED_MODULE_3__sidebar_component__["c" /* SidebarComponent */]],
            exports: [__WEBPACK_IMPORTED_MODULE_3__sidebar_component__["c" /* SidebarComponent */]]
        })
    ], SidebarModule);
    return SidebarModule;
}());

//# sourceMappingURL=sidebar.module.js.map

/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false,
    server: '',
};
//# sourceMappingURL=environment.js.map

/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("../../../platform-browser-dynamic/@angular/platform-browser-dynamic.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["enableProdMode"])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map