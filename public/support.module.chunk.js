webpackJsonp(["support.module"],{

/***/ "../../../../../src/app/pages/support/support.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\" *ngIf=\"settings.getSysSetting('enable_support') == 0\">\n    <div class=\"container-fluid\">\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n                <div class=\"alert alert-info\">\n                    Support is disabled.\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n\n<div class=\"main-content\" *ngIf=\"settings.getSysSetting('enable_support') == 1\">\n    <div class=\"container-fluid\">\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n                <div class=\"card\">\n                    <div class=\"card-header\">\n                        <h4 class=\"card-title\">My Ticket</h4>\n                    </div>\n                    <div class=\"card-content\">\n                        <div>\n                            <button type=\"button\" class=\"btn btn-info\" (click)=\"submitTicket()\">\n                                Submit a Ticket\n                            </button>\n                        </div>\n                        <div class=\"row\">\n                            <div class=\"col-md-12\">\n                                <div class=\"table-responsive\">\n                                    <table class=\"table\">\n                                        <thead>\n                                        <tr>\n                                            <th>Ticket ID</th>\n                                            <th>Status</th>\n                                            <th>Title</th>\n                                            <th>Last User</th>\n                                            <th style=\"width: 200px;\">Last Update</th>\n                                        </tr>\n                                        </thead>\n                                        <tbody>\n                                        <tr *ngFor=\"let ticket of tickets\" [ngClass]=\"{'table-active': ticket.status == 2}\" (click)=\"showTicket(ticket)\">\n                                            <td>{{ticket.id}}</td>\n                                            <td>\n                                                <!--<label class=\"label label-sm text-uppercase\"-->\n                                                <!--[ngClass]=\"{'label-info': ticket.status==0, 'label-warning': ticket.status==1, 'label-success': ticket.status==2 }\">-->\n                                                {{ ticket.status == 0 ? 'NEW' : (ticket.status == 1 ? 'OPEN' : 'CLOSED')}}\n                                                <!--</label>-->\n                                            </td>\n                                            <td>{{ticket.title}}</td>\n                                            <td>{{ticket.last_user}}</td>\n                                            <td style=\"width: 200px;\">{{ticket.updated_at | date:'yyyy-MM-dd HH:mm:ss'}}</td>\n                                        </tr>\n                                        <tr *ngIf=\"tickets?.length == 0\">\n                                            <td class=\"text-center\" colspan=\"5\">\n                                                No Ticket\n                                            </td>\n                                        </tr>\n                                        </tbody>\n                                    </table>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n\n        <input type=\"file\" name=\"file\" class=\"form-control\" id=\"attachFile\" (change)=\"fileChangeListener($event)\" style=\"display: none;\">\n        <div class=\"row\" *ngIf=\"isNew\">\n            <div class=\"col-md-12\">\n                <div class=\"card\">\n                    <div class=\"card-content\">\n                        <form>\n                            <div class=\"row\">\n                                <div class=\"form-group col-md-12\">\n                                    <label>Title</label>\n                                    <textarea class=\"form-control\" rows=\"3\" id=\"ticket_title\" name=\"title\"\n                                              [(ngModel)]=\"ticket.title\"></textarea>\n                                </div>\n                            </div>\n                            <div class=\"row\">\n                                <div class=\"form-group col-md-12\">\n                                    <label>Message</label>\n                                    <textarea class=\"form-control\" rows=\"10\" name=\"message\"\n                                              [(ngModel)]=\"ticket.message\"></textarea>\n                                </div>\n                            </div>\n                            <div class=\"row\">\n                                <div class=\"form-group col-md-12\">\n                                    <div class=\"pull-left\">\n                                        {{ticket.attach_name}}\n                                    </div>\n                                    <button type=\"button\" class=\"btn btn-primary pull-right\" (click)=\"onAttachFile()\">Add Attach File</button>\n                                </div>\n                            </div>\n                            <div class=\"row\">\n                                <div class=\"col-md-12\">\n                                    <button type=\"button\" class=\"btn btn-info\" (click)=\"sendTicket()\">\n                                        Send\n                                    </button>\n                                </div>\n                            </div>\n                        </form>\n                    </div>\n                </div>\n            </div>\n        </div>\n\n        <div *ngIf=\"details?.length > 0\">\n            <div class=\"row\">\n                <div class=\"col-md-12\">\n                    <div class=\"card\">\n                        <div class=\"card-content\">\n                            <pre>\n                                <h3 class=\"card-title\">{{ticket.title}}</h3>\n                            </pre>\n                            <p>\n                                Created: {{ticket.created_at | date: 'dd MMMM yyyy HH:mm'}}\n                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\n                                Updated: {{ticket.updated_at | date: 'dd MMMM yyyy HH:mm'}}\n                            </p>\n                        </div>\n                    </div>\n                </div>\n            </div>\n            <div class=\"row mb-3\" *ngIf=\"ticket.status != 2\">\n                <div class=\"col-md-12\">\n                    <button type=\"button\" class=\"btn btn-success\" (click)=\"onReply()\">\n                        Reply\n                    </button>\n                </div>\n            </div>\n\n            <div class=\"row\" *ngIf=\"isReply\">\n                <div class=\"col-md-12\">\n                    <div class=\"card\">\n                        <div class=\"card-content\">\n                            <form>\n                                <div class=\"row\">\n                                    <div class=\"form-group col-md-12\">\n                                        <label>Message</label>\n                                        <textarea class=\"form-control\" rows=\"10\" name=\"message\"\n                                                  [(ngModel)]=\"ticket.message\"></textarea>\n                                    </div>\n                                </div>\n                                <div class=\"row\">\n                                    <div class=\"form-group col-md-12\">\n                                        <div class=\"pull-left\">\n                                            {{ticket.attach_name}}\n                                        </div>\n                                        <button type=\"button\" class=\"btn btn-primary pull-right\" (click)=\"onAttachFile()\">Add Attach File</button>\n                                    </div>\n                                </div>\n                                <div class=\"row\">\n                                    <div class=\"col-md-12\">\n                                        <button type=\"button\" class=\"btn btn-info\" (click)=\"sendTicket()\">\n                                            Send\n                                        </button>\n                                    </div>\n                                </div>\n                            </form>\n                        </div>\n                    </div>\n                </div>\n            </div>\n\n            <div class=\"row\" *ngFor=\"let detail of details;\">\n                <div class=\"col-md-12\">\n                    <div class=\"card\">\n                        <div class=\"card-content\">\n                            <div class=\"row\">\n                                <div class=\"col-md-3 border-right-1\">\n                                    <p>{{detail.username}}</p>\n                                </div>\n                                <div class=\"col-md-9\">\n                                    <p class=\"border-bottom-1\">\n                                        Posted on  {{detail.created_at | date: 'dd MMMM yyyy HH:mm'}}\n                                    </p>\n                                    <pre>{{detail.message}}</pre>\n                                    <p *ngIf=\"detail.attach_name != ''\">\n                                        <a target=\"_blank\" href=\"{{detail.attach_url}}\">{{detail.attach_name}}</a>\n                                    </p>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n\n    </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/pages/support/support.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".table tr {\n  cursor: pointer; }\n\npre {\n  box-shadow: none; }\n\n.border-bottom-1 {\n  border-bottom: 1px solid; }\n\n.table-active {\n  background-color: rgba(0, 0, 0, 0.075); }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/pages/support/support.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SupportComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_api_service__ = __webpack_require__("../../../../../src/app/services/api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_settings_settings_service__ = __webpack_require__("../../../../../src/app/services/settings/settings.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_notifications_service__ = __webpack_require__("../../../../../src/app/services/notifications.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SupportComponent = (function () {
    function SupportComponent(api, settings, notify) {
        this.api = api;
        this.settings = settings;
        this.notify = notify;
        this.tickets = [];
        this.details = [];
        this.isNew = false;
        this.isReply = false;
        this.ticket = {};
    }
    SupportComponent.prototype.ngOnInit = function () {
        this.loadMyTicket();
    };
    SupportComponent.prototype.loadMyTicket = function () {
        var _this = this;
        this.api.getMyTicket({}).subscribe(function (res) {
            if (res.success) {
                _this.tickets = res.data;
            }
        });
    };
    SupportComponent.prototype.submitTicket = function () {
        this.isNew = true;
        this.isReply = false;
        this.ticket = {};
    };
    SupportComponent.prototype.onReply = function () {
        this.ticket.message = '';
        this.ticket.attach_url = '';
        this.ticket.attach_name = '';
        this.isNew = false;
        this.isReply = true;
    };
    SupportComponent.prototype.showTicket = function (ticket) {
        var _this = this;
        this.ticket = ticket;
        this.isNew = false;
        this.isReply = false;
        this.settings.loading = true;
        this.api.getTicketDetail({
            ticket_id: ticket.id
        }).subscribe(function (res) {
            _this.settings.loading = false;
            if (res.success) {
                _this.details = res.data;
            }
            else {
                _this.notify.showNotification(res.error, 'top', 'center', 'warning');
            }
        }, function (err) {
            _this.settings.loading = false;
            _this.notify.showNotification('You can not see detail.', 'top', 'center', 'error');
        });
    };
    SupportComponent.prototype.sendTicket = function () {
        var _this = this;
        if (this.isNew) {
            this.settings.loading = true;
            this.api.postTicket(this.ticket).subscribe(function (res) {
                _this.settings.loading = false;
                if (res.success) {
                    _this.notify.showNotification('Your Ticket was posted successfully', 'top', 'center', 'success');
                    _this.loadMyTicket();
                    _this.isNew = false;
                    _this.ticket = {};
                }
                else {
                    _this.notify.showNotification(res.error, 'top', 'center', 'warning');
                }
            }, function (err) {
                _this.settings.loading = false;
                _this.notify.showNotification('Please try again.', 'top', 'center', 'danger');
            });
        }
        else if (this.isReply) {
            this.settings.loading = true;
            this.api.replyTicket(this.ticket).subscribe(function (res) {
                _this.settings.loading = false;
                if (res.success) {
                    _this.notify.showNotification('Your Reply Ticket was posted successfully', 'top', 'center', 'success');
                    _this.isReply = false;
                    _this.showTicket(_this.ticket);
                }
                else {
                    _this.notify.showNotification(res.error, 'top', 'center', 'warning');
                }
            }, function (err) {
                _this.settings.loading = false;
                _this.notify.showNotification('Please try again.', 'top', 'center', 'danger');
            });
        }
    };
    SupportComponent.prototype.onAttachFile = function () {
        $('#attachFile').click();
    };
    SupportComponent.prototype.fileChangeListener = function ($event) {
        var _this = this;
        var image = new Image();
        var file = $event.target.files[0];
        this.settings.loading = true;
        this.api.uploadFile(file).subscribe(function (res) {
            _this.settings.loading = false;
            if (res.success) {
                _this.ticket.attach_url = res.url;
                _this.ticket.attach_name = file.name;
            }
        }, function (err) {
            _this.settings.loading = false;
        });
    };
    SupportComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-support',
            template: __webpack_require__("../../../../../src/app/pages/support/support.component.html"),
            styles: [__webpack_require__("../../../../../src/app/pages/support/support.component.scss")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_api_service__["a" /* Api */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_api_service__["a" /* Api */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__services_settings_settings_service__["a" /* SettingsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_settings_settings_service__["a" /* SettingsService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__services_notifications_service__["a" /* Notifications */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services_notifications_service__["a" /* Notifications */]) === "function" && _c || Object])
    ], SupportComponent);
    return SupportComponent;
    var _a, _b, _c;
}());

//# sourceMappingURL=support.component.js.map

/***/ }),

/***/ "../../../../../src/app/pages/support/support.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SupportModule", function() { return SupportModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__md_md_module__ = __webpack_require__("../../../../../src/app/md/md.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__support_component__ = __webpack_require__("../../../../../src/app/pages/support/support.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__support_routing__ = __webpack_require__("../../../../../src/app/pages/support/support.routing.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








var SupportModule = (function () {
    function SupportModule() {
    }
    SupportModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_2__angular_common__["b" /* CommonModule */],
                __WEBPACK_IMPORTED_MODULE_1__angular_router__["d" /* RouterModule */].forChild(__WEBPACK_IMPORTED_MODULE_7__support_routing__["a" /* SupportRoutes */]),
                __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormsModule"],
                __WEBPACK_IMPORTED_MODULE_4__md_md_module__["a" /* MdModule */],
                __WEBPACK_IMPORTED_MODULE_5__app_module__["b" /* MaterialModule */],
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_6__support_component__["a" /* SupportComponent */]
            ]
        })
    ], SupportModule);
    return SupportModule;
}());

//# sourceMappingURL=support.module.js.map

/***/ }),

/***/ "../../../../../src/app/pages/support/support.routing.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SupportRoutes; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__support_component__ = __webpack_require__("../../../../../src/app/pages/support/support.component.ts");

var SupportRoutes = [
    {
        path: '',
        component: __WEBPACK_IMPORTED_MODULE_0__support_component__["a" /* SupportComponent */]
    }
];
//# sourceMappingURL=support.routing.js.map

/***/ })

});
//# sourceMappingURL=support.module.chunk.js.map