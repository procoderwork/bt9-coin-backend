webpackJsonp(["wallet.module"],{

/***/ "../../../../../src/app/pages/wallet/wallet.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\n    <div class=\"container-fluid\">\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n                <h3 class=\"title text-center\">Deposit/Withdraw Coin</h3>\n                <br/>\n                <div class=\"nav-center\">\n                    <ul class=\"nav nav-pills nav-pills-warning nav-pills-icons\" role=\"tablist\">\n                        <li class=\"active\">\n                            <a href=\"#btc\" role=\"tab\" data-toggle=\"tab\">\n                                <i class=\"fa fa-bitcoin\"></i> BITCOIN</a>\n                        </li>\n                        <li>\n                            <a href=\"#eth\" role=\"tab\" data-toggle=\"tab\">\n                                <i><img src=\"assets/img/eth_29x29.png\"/></i> ETHEREUM\n                            </a>\n                        </li>\n                        <li class=\"\">\n                            <a href=\"#bt9\" role=\"tab\" data-toggle=\"tab\">\n                                <i><img src=\"assets/img/B9_black.png\" style=\"width: 30px;\"/></i> BT9COIN\n                            </a>\n                        </li>\n                    </ul>\n                </div>\n                <div class=\"tab-content\">\n                    <div class=\"tab-pane\" id=\"bt9\">\n                        <div class=\"row\" *ngIf=\"settings.getSysSetting('enable_bt9_wallet') == 0\">\n                            <div class=\"col-md-12\">\n                                <div class=\"alert alert-info\">\n                                    BT9COIN Wallet is disabled.\n                                </div>\n                            </div>\n                        </div>\n                        <div class=\"row\" *ngIf=\"settings.getSysSetting('enable_bt9_wallet') == 1\">\n                            <div class=\"col-md-6\">\n                                <div class=\"card h800\">\n                                    <div class=\"card-header\">\n                                        <h4 class=\"card-title\">Deposit BT9Coin</h4>\n                                    </div>\n                                    <div class=\"card-content\">\n                                        <h3 style=\"color: orange; \">Receive BT9Coin(BT9)</h3>\n                                        <p>Your BT9Coin Deposit Address Is:</p>\n                                        <div>\n                                            <div class=\"form-group\">\n                                                <input type=\"text\" class=\"form-control text-center\" readonly\n                                                       style=\"cursor: pointer; color: black;\"\n                                                       value=\"{{settings.getUserSetting('bt9_address')}}\">\n                                            </div>\n                                            <div class=\"text-center\"\n                                                 *ngIf=\"settings.getUserSetting('bt9_address') != ''\">\n                                                <img class=\"qrCode\"\n                                                     src=\"https://chart.googleapis.com/chart?chs=200x200&chld=L|2&cht=qr&chl=bt9coin:{{settings.getUserSetting('bt9_address')}}\"/>\n                                            </div>\n                                        </div>\n                                        <div class=\"important-note\">\n\n                                        </div>\n                                        <div class=\"important-note\">\n                                            <p>Remember: CHECK the FIRST 4 characters and the LAST 4 characters of the\n                                                BT9Coin address when you PASTE it elsewhere</p>\n                                            <p>Important: This deposit address is for BT9Coin (BT9) only. We will not\n                                                refund an incorrect Deposit.</p>\n                                            <p>- Don't worry. If you don't receive any transaction. Please click\n                                                'Refresh' or 'Don't Receive Your Deposit' or contact support</p>\n                                            <p>- You can deposit BT9Coin from anywhere. Include exchange market </p>\n                                        </div>\n                                        <div>\n                                            <button class=\"btn btn-xs btn-warning send-bitcoin-submit\"\n                                                    (click)=\"loadBT9ReceivedTxList()\" type=\"button\">\n                                                <i class=\"fa fa-refresh\"></i>\n                                                Refresh\n                                            </button>\n                                        </div>\n                                        <div class=\"bs-component\">\n                                            <div class=\"panel-group accordion\">\n                                                <div class=\"panel mbn\" style=\"border: 1px solid #c2c8d8;\">\n                                                    <div class=\"panel-heading\" style=\"height: auto\">\n                                                        <p style=\"border-radius: 0px;background-color:transparent;color:black\">\n                                                            Incoming BT9Coin Transactions\n                                                        </p>\n                                                    </div>\n                                                    <div class=\"panel-collapse collapse in \"\n                                                         style=\"background-color: white\">\n                                                        <div class=\"row\" style=\"margin-top: 0;\">\n                                                            <div class=\"col-sm-12\">\n                                                                <div class=\"table-responsive\">\n                                                                    <table cellspacing=\"0\"\n                                                                           class=\"table table-striped table-hover table-condensed text-black \"\n                                                                           width=\"100%\">\n                                                                        <thead>\n                                                                        <tr>\n                                                                            <th>TXID</th>\n                                                                            <th>BT9</th>\n                                                                            <th>Confirmation</th>\n                                                                        </tr>\n                                                                        </thead>\n                                                                        <tbody>\n                                                                        <tr *ngFor=\"let tx of bt9ReceivedTxList\">\n                                                                            <td>{{tx.txid}}</td>\n                                                                            <td>{{tx.amount}}</td>\n                                                                            <td>{{tx.confirmations}}</td>\n                                                                        </tr>\n                                                                        <!---->\n                                                                        <tr *ngIf=\"bt9ReceivedTxList?.length == 0\">\n                                                                            <td class=\"text-center\" colspan=\"3\">\n                                                                                No Incoming BT9Coin transactions found.\n                                                                            </td>\n                                                                        </tr>\n                                                                        <!---->\n                                                                        </tbody>\n                                                                    </table>\n                                                                </div>\n                                                            </div>\n                                                        </div>\n                                                    </div>\n                                                </div>\n                                            </div>\n                                        </div>\n\n                                        <p>We require minimum 3 confirmations to complete transaction. On average, each\n                                            confirmation takes about 15 minutes. It can take more time depending on the\n                                            BT9Coin network so do not worry and wait quietly.</p>\n                                    </div>\n                                </div>\n                            </div>\n                            <div class=\"col-md-6\" *ngIf=\"settings.getSysSetting('enable_bt9_withdraw') == 0\">\n                                <div class=\"card h800\">\n                                    <div class=\"card-content\">\n                                        <div class=\"alert alert-info\">\n                                            BT9Coin Withdraw is disabled.\n                                        </div>\n                                    </div>\n                                </div>\n                            </div>\n                            <div class=\"col-md-6\" *ngIf=\"settings.getSysSetting('enable_bt9_withdraw') == 1\">\n                                <div class=\"card h800\">\n                                    <div class=\"card-header\">\n                                        <h4 class=\"card-title\">Withdraw BT9Coin</h4>\n                                    </div>\n                                    <div class=\"card-content\">\n                                        <div class=\"p20\" style=\"position: relative\">\n                                            <div style=\"\">\n                                                <h3 class=\"mtn\" style=\"color:orange\">Send BT9Coin (BT9)</h3>\n                                                <p class=\"text-black\">Your BT9Coin wallet balance:\n                                                    <strong class=\"bitcoin_wallet_balance\">{{settings.getUserSetting('token_balance') | number:'1.8-8'}}</strong>\n                                                    BT9\n                                                    <span class=\"bitcoin_wallet_balance_unconfirmed\"></span>\n                                                </p>\n                                                <div class=\"response\"></div>\n                                                <form class=\"form-horizontal\" [formGroup]=\"withdrawBT9Form\">\n                                                    <div class=\"form-group is-empty\">\n                                                        <label class=\"control-label\"> To address </label>\n                                                        <div class=\"input-group\">\n                                                            <div class=\"input-group-btn vat\">\n                                                                <span class=\"btn2 btn2-default w40px\" disabled=\"\">\n                                                                    <i class=\"fa fa-chain\"></i>\n                                                                </span>\n                                                            </div>\n\n                                                            <input autocomplete=\"off\" class=\"form-control\"\n                                                                   formControlName=\"address\"\n                                                                   name=\"toAddress\" style=\"height:34px;\" type=\"text\"\n                                                                   placeholder=\"BT9Coin Address\"\n                                                                   [(ngModel)]=\"bt9coin.address\">\n\n                                                            <app-field-error-display\n                                                                    [displayError]=\"validate.isFieldValid(withdrawBT9Form, 'address')\"\n                                                                    errorMsg=\"Address Ethereum required.\">\n                                                            </app-field-error-display>\n                                                        </div>\n                                                    </div>\n                                                    <div class=\"form-group\">\n                                                        <label class=\"control-label\">Quantity in BT9Coin</label>\n                                                        <div class=\"input-group\">\n                                                            <div class=\"input-group-btn vat\">\n                                                                <span class=\"btn2 btn2-default w40px\" disabled=\"\"\n                                                                      style=\"padding: 5px;\">\n                                                                    <img src=\"assets/img/B9_black.png\"></span>\n                                                            </div>\n\n                                                            <input class=\"form-control\" name=\"amountCoin\"\n                                                                   formControlName=\"amount\"\n                                                                   style=\"height:34px;\" type=\"text\"\n                                                                   placeholder=\"Quantity in BT9Coin\"\n                                                                   [(ngModel)]=\"bt9coin.amount\">\n                                                            <app-field-error-display\n                                                                    [displayError]=\"validate.isFieldValid(withdrawBT9Form, 'amount')\"\n                                                                    errorMsg=\"Amount required.\">\n                                                            </app-field-error-display>\n                                                        </div>\n                                                        <small class=\"text-black\">Minimum Withdrawal : 0.005 BT9\n                                                        </small>\n\n                                                        <div style=\"margin-top: 20px;\">\n                                                            <span style=\"margin-left: 50px; \">Withdraw Fee: {{settings.getSysSetting('withdraw_bt9_fee')}} %</span>\n                                                        </div>\n                                                        <div style=\"margin-top: 10px;\">\n                                                            <span style=\"margin-left: 50px; \">Withdraw Amount: <b>{{(bt9coin.amount * (100 - settings.getSysSetting('withdraw_bt9_fee')) / 100) | number:'1.8-8'}}</b> BT9</span>\n                                                        </div>\n                                                    </div>\n                                                    <!--<div>Receive: <b>0.00000000</b></div>-->\n                                                    <button class=\"btn btn-warning\" type=\"button\"\n                                                            (click)=\"onWithdrawBT9()\">\n                                                        <img src=\"assets/img/B9_color.png\" style=\"width: 20px;\"/>\n                                                        Withdraw from BT9 wallet\n                                                    </button>\n                                                </form>\n                                                <div class=\"bs-component\">\n                                                    <div class=\"panel-group accordion\">\n                                                        <div class=\"panel mbn\" style=\"border: 1px solid #c2c8d8;\">\n                                                            <div class=\"panel-heading\" style=\"height: auto\">\n                                                                <p style=\"border-radius: 0px;background-color:transparent;color:black\">\n                                                                    Pending Withdraw BT9Coin Transactions\n                                                                </p>\n                                                            </div>\n                                                            <div class=\"panel-collapse collapse in \"\n                                                                 style=\"background-color: white\">\n                                                                <div class=\"row\" style=\"margin-top: 0;\">\n                                                                    <div class=\"col-sm-12\">\n                                                                        <div class=\"table-responsive\">\n                                                                            <table cellspacing=\"0\"\n                                                                                   class=\"table table-striped table-hover table-condensed text-black \"\n                                                                                   width=\"100%\">\n                                                                                <thead>\n                                                                                <tr>\n                                                                                    <th>To Address</th>\n                                                                                    <th>BT9</th>\n                                                                                    <th>Status</th>\n                                                                                    <th>Action</th>\n                                                                                </tr>\n                                                                                </thead>\n                                                                                <tbody>\n                                                                                <tr *ngFor=\"let tx of bt9SentTxList\">\n                                                                                    <td>{{tx.address}}</td>\n                                                                                    <td>{{tx.src_amount | number:'1.8-8'}}</td>\n                                                                                    <td>PENDING</td>\n                                                                                    <td>\n                                                                                        <button type=\"button\" class=\"btn btn-danger btn-sm\" (click)=\"cancelWithdraw('BT9', tx.id)\">\n                                                                                            <i class=\"fa fa-close\"></i>\n                                                                                        </button>\n                                                                                    </td>\n                                                                                </tr>\n                                                                                <!---->\n                                                                                <tr *ngIf=\"bt9SentTxList?.length == 0\">\n                                                                                    <td class=\"text-center\" colspan=\"4\">\n                                                                                        No Pending Withdraw BT9Coin\n                                                                                        transactions found.\n                                                                                    </td>\n                                                                                </tr>\n                                                                                <!---->\n                                                                                </tbody>\n                                                                            </table>\n                                                                        </div>\n                                                                    </div>\n                                                                </div>\n                                                            </div>\n                                                        </div>\n                                                    </div>\n                                                </div>\n                                                <p>After request, we will send your transaction to our fraud detection\n                                                    AI system. Your transaction will be processed quickly</p>\n                                            </div>\n                                        </div>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"tab-pane active\" id=\"btc\">\n                        <div class=\"row\" *ngIf=\"settings.getSysSetting('enable_btc_wallet') == 0\">\n                            <div class=\"col-md-12\">\n                                <div class=\"alert alert-info\">\n                                    BITCOIN Wallet is disabled.\n                                </div>\n                            </div>\n                        </div>\n                        <div class=\"row\" *ngIf=\"settings.getSysSetting('enable_btc_wallet') == 1\">\n                            <div class=\"col-md-6\">\n                                <div class=\"card h800\">\n                                    <div class=\"card-header\">\n                                        <h4 class=\"card-title\">Deposit BitCoin</h4>\n                                    </div>\n                                    <div class=\"card-content\">\n                                        <h3 style=\"color: orange; \">Receive BitCoin(BTC)</h3>\n                                        <p>Your BitCoin Deposit Address Is:</p>\n                                        <div>\n                                            <div class=\"form-group\">\n                                                <input type=\"text\" class=\"form-control text-center\" readonly\n                                                       style=\"cursor: pointer; color: black;\"\n                                                       value=\"{{settings.getUserSetting('btc_address')}}\">\n                                            </div>\n                                            <div class=\"text-center\"\n                                                 *ngIf=\"settings.getUserSetting('btc_address') != ''\">\n                                                <img class=\"qrCode\"\n                                                     src=\"https://chart.googleapis.com/chart?chs=200x200&chld=L|2&cht=qr&chl=bitcoin:{{settings.getUserSetting('btc_address')}}\"/>\n                                            </div>\n                                        </div>\n                                        <div class=\"important-note\">\n\n                                        </div>\n                                        <div class=\"important-note\">\n                                            <p>Remember: CHECK the FIRST 4 characters and the LAST 4 characters of the\n                                                Bitcoin address\n                                                when you PASTE it elsewhere</p>\n                                            <p>Important: This deposit address is for BitCoin (BTC) only. We will not\n                                                refund an\n                                                incorrect Deposit.</p>\n                                            <p>- Don't worry. If you don't receive any transaction. Please click\n                                                'Refresh' or 'Don't\n                                                Receive Your Deposit' or contact support</p>\n                                            <p>- You can deposit Bitcoin from anywhere. Include exchange market </p>\n                                        </div>\n                                        <!--<button class=\"btn btn-xs btn-warning send-bitcoin-submit\" disable=\"true\"-->\n                                        <!--type=\"button\">-->\n                                        <!--<i class=\"fa fa-question-circle\"-->\n                                        <!--style=\"background-position: right; filter: brightness(0) invert(1)\"></i>-->\n                                        <!--Don't Receive Your Deposit-->\n                                        <!--</button>-->\n                                        <div>\n                                            <button class=\"btn btn-xs btn-warning send-bitcoin-submit\"\n                                                    (click)=\"loadBTCReceivedTxList()\" type=\"button\">\n                                                <i class=\"fa fa-refresh\"></i>\n                                                Refresh\n                                            </button>\n                                        </div>\n                                        <div class=\"bs-component\">\n                                            <div class=\"panel-group accordion\">\n                                                <div class=\"panel mbn\" style=\"border: 1px solid #c2c8d8;\">\n                                                    <div class=\"panel-heading\" style=\"height: auto\">\n                                                        <p style=\"border-radius: 0px;background-color:transparent;color:black\">\n                                                            Incoming Bitcoin Transactions\n                                                        </p>\n                                                    </div>\n                                                    <div class=\"panel-collapse collapse in \"\n                                                         style=\"background-color: white\">\n                                                        <div class=\"row\" style=\"margin-top: 0;\">\n                                                            <div class=\"col-sm-12\">\n                                                                <div class=\"table-responsive\">\n                                                                    <table cellspacing=\"0\"\n                                                                           class=\"table table-striped table-hover table-condensed text-black \"\n                                                                           width=\"100%\">\n                                                                        <thead>\n                                                                        <tr>\n                                                                            <th>TXID</th>\n                                                                            <th>BTC</th>\n                                                                            <th>Confirmation</th>\n                                                                        </tr>\n                                                                        </thead>\n                                                                        <tbody>\n                                                                        <tr *ngFor=\"let tx of btcReceivedTxList\">\n                                                                            <td>{{tx.txid}}</td>\n                                                                            <td>{{tx.amount}}\n                                                                            </td>\n                                                                            <td>{{tx.confirmations}}</td>\n                                                                        </tr>\n                                                                        <!---->\n                                                                        <tr *ngIf=\"btcReceivedTxList?.length == 0\">\n                                                                            <td class=\"text-center\" colspan=\"3\">\n                                                                                No Incoming Bitcoin transactions found.\n                                                                            </td>\n                                                                        </tr>\n                                                                        <!---->\n                                                                        </tbody>\n                                                                    </table>\n                                                                </div>\n                                                            </div>\n                                                        </div>\n                                                    </div>\n                                                </div>\n                                            </div>\n                                        </div>\n\n                                        <p>We require minimum 3 confirmations to complete transaction. On average, each\n                                            confirmation\n                                            takes about 15 minutes. It can take more time depending on the Bitcoin\n                                            network so do not\n                                            worry and wait quietly.</p>\n                                    </div>\n                                </div>\n                            </div>\n\n                            <div class=\"col-md-6\" *ngIf=\"settings.getSysSetting('enable_btc_withdraw') == 0\">\n                                <div class=\"card h800\">\n                                    <div class=\"card-content\">\n                                        <div class=\"alert alert-info\">\n                                            BITCOIN Withdraw is disabled.\n                                        </div>\n                                    </div>\n                                </div>\n                            </div>\n                            <div class=\"col-md-6\" *ngIf=\"settings.getSysSetting('enable_btc_withdraw') == 1\">\n                                <div class=\"card h800\">\n                                    <div class=\"card-header\">\n                                        <h4 class=\"card-title\">Withdraw BitCoin</h4>\n                                    </div>\n                                    <div class=\"card-content\">\n                                        <div class=\"p20\" id=\"send-bitcoin-area\" style=\"position: relative\">\n                                            <div style=\"\">\n                                                <h3 class=\"mtn\" style=\"color:orange\">Send BitCoin (BTC)</h3>\n                                                <p class=\"text-black\">Your Bitcoin wallet balance: <strong\n                                                        class=\"bitcoin_wallet_balance\">\n                                                    {{settings.getUserSetting('btc_balance') | number:'1.8-8'}}</strong> BTC\n                                                    <span class=\"bitcoin_wallet_balance_unconfirmed\"></span>\n                                                </p>\n                                                <div class=\"response\"></div>\n                                                <form class=\"form-horizontal\" [formGroup]=\"withdrawBTCForm\">\n                                                    <div class=\"form-group is-empty\">\n                                                        <label class=\"control-label\"> To address </label>\n                                                        <div class=\"input-group\">\n                                                            <div class=\"input-group-btn vat\">\n                                                                <span class=\"btn2 btn2-default w40px\" disabled=\"\">\n                                                                    <i class=\"fa fa-chain\"></i></span>\n                                                            </div>\n                                                            <input autocomplete=\"off\" class=\"form-control\"\n                                                                   formControlName=\"address\"\n                                                                   name=\"toAddress\" style=\"height:34px;\" type=\"text\"\n                                                                   placeholder=\"Bitcoin Address\"\n                                                                   [(ngModel)]=\"bitcoin.address\">\n\n                                                            <app-field-error-display\n                                                                    [displayError]=\"validate.isFieldValid(withdrawBTCForm, 'address')\"\n                                                                    errorMsg=\"Address Bitcoin required.\">\n                                                            </app-field-error-display>\n                                                        </div>\n                                                    </div>\n                                                    <div class=\"form-group\">\n                                                        <label class=\"control-label\">Quantity in Bitcoin</label>\n                                                        <div class=\"input-group\">\n                                                            <div class=\"input-group-btn vat\">\n                                                                <span class=\"btn2 btn2-default w40px\" disabled=\"\">\n                                                                    <i class=\"fa fa-btc\"></i></span>\n                                                            </div>\n\n                                                            <input class=\"form-control\" name=\"amountCoin\"\n                                                                   formControlName=\"amount\"\n                                                                   style=\"height:34px;\" type=\"text\"\n                                                                   placeholder=\"Quantity in Bitcoin\"\n                                                                   [(ngModel)]=\"bitcoin.amount\">\n                                                            <app-field-error-display\n                                                                    [displayError]=\"validate.isFieldValid(withdrawBTCForm, 'amount')\"\n                                                                    errorMsg=\"Amount required.\">\n                                                            </app-field-error-display>\n                                                            <!--<div class=\"input-group-btn \">-->\n                                                            <!--<span class=\"btn2 btn2-default mnw100 all_btc_minus_fee_send\">ALL</span>-->\n                                                            <!--</div>-->\n                                                        </div>\n                                                        <!--<small class=\"text-black\">Minimum Withdrawal : 0.0005 BTC, Transaction Fee : 0.0004 BTC</small>-->\n                                                        <small class=\"text-black\">Minimum Withdrawal : 0.001 BTC\n                                                        </small>\n                                                        <div style=\"margin-top: 20px;\">\n                                                            <span style=\"margin-left: 50px; \">Withdraw Fee: {{settings.getSysSetting('withdraw_btc_fee')}} %</span>\n                                                        </div>\n                                                        <div style=\"margin-top: 10px;\">\n                                                            <span style=\"margin-left: 50px; \">Withdraw Amount: <b>{{(bitcoin.amount * (100 - settings.getSysSetting('withdraw_btc_fee')) / 100) | number:'1.8-8'}}</b> BTC</span>\n                                                        </div>\n                                                    </div>\n                                                    <!--<div>Receive: <b>0.00000000</b></div>-->\n                                                    <button class=\"btn btn-warning\" type=\"button\"\n                                                            (click)=\"onWithdrawBTC()\">\n                                                        <i class=\"fa fa-btc  progress-icon fa-lg\"\n                                                           style=\"background-position: right;filter: brightness(0) invert(1)\"></i>\n                                                        Withdraw from BTC wallet\n                                                    </button>\n                                                </form>\n                                                <div class=\"bs-component\">\n                                                    <div class=\"panel-group accordion\">\n                                                        <div class=\"panel mbn\" style=\"border: 1px solid #c2c8d8;\">\n                                                            <div class=\"panel-heading\" style=\"height: auto\">\n                                                                <p style=\"border-radius: 0px;background-color:transparent;color:black\">\n                                                                    Pending Withdraw Bitcoin Transactions\n                                                                </p>\n                                                            </div>\n                                                            <div class=\"panel-collapse collapse in \"\n                                                                 id=\"incoming_bitcoin_transaction\"\n                                                                 style=\"background-color: white\">\n                                                                <div class=\"row\" style=\"margin-top: 0;\">\n                                                                    <div class=\"col-sm-12\">\n                                                                        <div class=\"table-responsive\">\n                                                                            <table cellspacing=\"0\"\n                                                                                   class=\"table table-striped table-hover table-condensed text-black \"\n                                                                                   id=\"incoming-bitcoin-transaction-table\"\n                                                                                   width=\"100%\">\n                                                                                <thead>\n                                                                                <tr>\n                                                                                    <th>To Address</th>\n                                                                                    <th>BTC</th>\n                                                                                    <th>Status</th>\n                                                                                    <th>Action</th>\n                                                                                </tr>\n                                                                                </thead>\n                                                                                <tbody>\n                                                                                <tr *ngFor=\"let tx of btcSentTxList\">\n                                                                                    <td>{{tx.address}}</td>\n                                                                                    <td>{{tx.src_amount | number:'1.8-8'}}</td>\n                                                                                    <td>PENDING</td>\n                                                                                    <td>\n                                                                                        <button type=\"button\" class=\"btn btn-danger btn-sm\" (click)=\"cancelWithdraw('BTC', tx.id)\">\n                                                                                            <i class=\"fa fa-close\"></i>\n                                                                                        </button>\n                                                                                    </td>\n                                                                                </tr>\n                                                                                <!---->\n                                                                                <tr *ngIf=\"btcSentTxList?.length == 0\">\n                                                                                    <td class=\"text-center\" colspan=\"4\">\n                                                                                        No Pending\n                                                                                        Withdraw Bitcoin transactions\n                                                                                        found.\n                                                                                    </td>\n                                                                                </tr>\n                                                                                <!---->\n                                                                                </tbody>\n                                                                            </table>\n                                                                        </div>\n                                                                    </div>\n                                                                </div>\n                                                            </div>\n                                                        </div>\n                                                    </div>\n                                                </div>\n                                                <p>After request, we will send your transaction to our fraud detection\n                                                    AI system. Your\n                                                    transaction will be processed quickly</p>\n                                            </div>\n                                        </div>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"tab-pane\" id=\"eth\">\n                        <div class=\"row\" *ngIf=\"settings.getSysSetting('enable_eth_wallet') == 0\">\n                            <div class=\"col-md-12\">\n                                <div class=\"alert alert-info\">\n                                    ETHEREUM Wallet is disabled.\n                                </div>\n                            </div>\n                        </div>\n                        <div class=\"row\" *ngIf=\"settings.getSysSetting('enable_eth_wallet') == 1\">\n                            <div class=\"col-md-6\">\n                                <div class=\"card h800\">\n                                    <div class=\"card-header\">\n                                        <h4 class=\"card-title\">Deposit Ethereum</h4>\n                                    </div>\n                                    <div class=\"card-content\">\n                                        <h3 style=\"color: orange; \">Receive Ethereum(ETH)</h3>\n                                        <p>Your Ethereum Deposit Address Is:</p>\n                                        <div>\n                                            <div class=\"form-group\">\n                                                <input type=\"text\" class=\"form-control text-center\" readonly\n                                                       style=\"cursor: pointer; color: black;\"\n                                                       value=\"{{settings.getUserSetting('eth_address')}}\">\n                                            </div>\n                                            <div class=\"text-center\"\n                                                 *ngIf=\"settings.getUserSetting('eth_address') != ''\">\n                                                <img class=\"qrCode\"\n                                                     src=\"https://chart.googleapis.com/chart?chs=200x200&chld=L|2&cht=qr&chl=ethereum:{{settings.getUserSetting('eth_address')}}\"/>\n                                            </div>\n                                        </div>\n                                        <div class=\"important-note\">\n\n                                        </div>\n                                        <div class=\"important-note\">\n                                            <p>Remember: CHECK the FIRST 4 characters and the LAST 4 characters of the\n                                                Ethereum address\n                                                when you PASTE it elsewhere</p>\n                                            <p>Important: This deposit address is for Ethereum (ETH) only. We will not\n                                                refund\n                                                an\n                                                incorrect Deposit.</p>\n                                            <p>- Don't worry. If you don't receive any transaction. Please click\n                                                'Refresh'\n                                                or 'Don't\n                                                Receive Your Deposit' or contact support</p>\n                                            <p>- You can deposit Ethereum from anywhere. Include exchange market </p>\n                                        </div>\n                                        <div>\n                                            <button class=\"btn btn-xs btn-warning send-bitcoin-submit\"\n                                                    (click)=\"loadETHReceivedTxList()\" type=\"button\">\n                                                <i class=\"fa fa-refresh\"></i>\n                                                Refresh\n                                            </button>\n                                        </div>\n                                        <div class=\"bs-component\">\n                                            <div class=\"panel-group accordion\">\n                                                <div class=\"panel mbn\" style=\"border: 1px solid #c2c8d8;\">\n                                                    <div class=\"panel-heading\" style=\"height: auto\">\n                                                        <p style=\"border-radius: 0px;background-color:transparent;color:black\">\n                                                            Incoming Ethereum Transactions\n                                                        </p>\n                                                    </div>\n                                                    <div class=\"panel-collapse collapse in \"\n                                                         style=\"background-color: white\">\n                                                        <div class=\"row\" style=\"margin-top: 0;\">\n                                                            <div class=\"col-sm-12\">\n                                                                <div class=\"table-responsive\">\n                                                                    <table cellspacing=\"0\"\n                                                                           class=\"table table-striped table-hover table-condensed text-black \"\n                                                                           width=\"100%\">\n                                                                        <thead>\n                                                                        <tr>\n                                                                            <th>TXID</th>\n                                                                            <th>ETH</th>\n                                                                            <th>Confirmation</th>\n                                                                        </tr>\n                                                                        </thead>\n                                                                        <tbody>\n                                                                        <tr *ngFor=\"let tx of ethReceivedTxList\">\n                                                                            <td>{{tx.txid}}</td>\n                                                                            <td>{{tx.amount}}</td>\n                                                                            <td>{{tx.confirmations}}</td>\n                                                                        </tr>\n                                                                        <!---->\n                                                                        <tr *ngIf=\"ethReceivedTxList?.length == 0\">\n                                                                            <td class=\"text-center\" colspan=\"3\">\n                                                                                No Incoming Ethereum transactions found.\n                                                                            </td>\n                                                                        </tr>\n                                                                        <!---->\n                                                                        </tbody>\n                                                                    </table>\n                                                                </div>\n                                                            </div>\n                                                        </div>\n                                                    </div>\n                                                </div>\n                                            </div>\n                                        </div>\n\n                                        <p>We require minimum 3 confirmations to complete transaction. On average, each\n                                            confirmation takes about 15 minutes. It can take more time depending on the\n                                            Ethereum network so do not worry and wait quietly.</p>\n                                    </div>\n                                </div>\n                            </div>\n                            <div class=\"col-md-6\" *ngIf=\"settings.getSysSetting('enable_eth_withdraw') == 0\">\n                                <div class=\"card h800\">\n                                    <div class=\"card-content\">\n                                        <div class=\"alert alert-info\">\n                                            ETHEREUM Withdraw is disabled.\n                                        </div>\n                                    </div>\n                                </div>\n                            </div>\n                            <div class=\"col-md-6\" *ngIf=\"settings.getSysSetting('enable_eth_withdraw') == 1\">\n                                <div class=\"card h800\">\n                                    <div class=\"card-header\">\n                                        <h4 class=\"card-title\">Withdraw Ethereum</h4>\n                                    </div>\n                                    <div class=\"card-content\">\n                                        <div class=\"p20\" style=\"position: relative\">\n                                            <div style=\"\">\n                                                <h3 class=\"mtn\" style=\"color:orange\">Send Ethereum (ETH)</h3>\n                                                <p class=\"text-black\">Your Ethereum wallet balance:\n                                                    <strong class=\"bitcoin_wallet_balance\">{{settings.getUserSetting('eth_balance') | number:'1.8-8'}}</strong>\n                                                    ETH\n                                                    <span class=\"bitcoin_wallet_balance_unconfirmed\"></span>\n                                                </p>\n                                                <div class=\"response\"></div>\n                                                <form class=\"form-horizontal\" [formGroup]=\"withdrawETHForm\">\n                                                    <div class=\"form-group is-empty\">\n                                                        <label class=\"control-label\"> To address </label>\n                                                        <div class=\"input-group\">\n                                                            <div class=\"input-group-btn vat\">\n                                                                <span class=\"btn2 btn2-default w40px\" disabled=\"\">\n                                                                    <i class=\"fa fa-chain\"></i>\n                                                                </span>\n                                                            </div>\n\n                                                            <input autocomplete=\"off\" class=\"form-control\"\n                                                                   formControlName=\"address\"\n                                                                   name=\"toAddress\" style=\"height:34px;\" type=\"text\"\n                                                                   placeholder=\"Ethereum Address\"\n                                                                   [(ngModel)]=\"ethereum.address\">\n\n                                                            <app-field-error-display\n                                                                    [displayError]=\"validate.isFieldValid(withdrawETHForm, 'address')\"\n                                                                    errorMsg=\"Address Ethereum required.\">\n                                                            </app-field-error-display>\n                                                        </div>\n                                                    </div>\n                                                    <div class=\"form-group\">\n                                                        <label class=\"control-label\">Quantity in Ethereum</label>\n                                                        <div class=\"input-group\">\n                                                            <div class=\"input-group-btn vat\">\n                                                                <span class=\"btn2 btn2-default w40px\" disabled=\"\"\n                                                                      style=\"padding: 5px;\">\n                                                                    <img src=\"assets/img/eth.png\" style=\"width: 15px;\"></span>\n                                                            </div>\n\n                                                            <input class=\"form-control\" name=\"amountCoin\"\n                                                                   formControlName=\"amount\"\n                                                                   style=\"height:34px;\" type=\"text\"\n                                                                   placeholder=\"Quantity in Ethereum\"\n                                                                   [(ngModel)]=\"ethereum.amount\">\n                                                            <app-field-error-display\n                                                                    [displayError]=\"validate.isFieldValid(withdrawETHForm, 'amount')\"\n                                                                    errorMsg=\"Amount required.\">\n                                                            </app-field-error-display>\n                                                        </div>\n                                                        <small class=\"text-black\">Minimum Withdrawal : 0.005 ETH\n                                                        </small>\n\n                                                        <div style=\"margin-top: 20px;\">\n                                                            <span style=\"margin-left: 50px; \">Withdraw Fee: {{settings.getSysSetting('withdraw_eth_fee')}} %</span>\n                                                        </div>\n                                                        <div style=\"margin-top: 10px;\">\n                                                            <span style=\"margin-left: 50px; \">Withdraw Amount: <b>{{(ethereum.amount * (100 - settings.getSysSetting('withdraw_eth_fee')) / 100) | number:'1.8-8'}}</b> ETH</span>\n                                                        </div>\n                                                    </div>\n                                                    <!--<div>Receive: <b>0.00000000</b></div>-->\n                                                    <button class=\"btn btn-warning\" type=\"button\"\n                                                            (click)=\"onWithdrawETH()\">\n                                                        <img src=\"assets/img/eth.png\" style=\"width: 15px;\">\n                                                        Withdraw from ETH wallet\n                                                    </button>\n                                                </form>\n                                                <div class=\"bs-component\">\n                                                    <div class=\"panel-group accordion\">\n                                                        <div class=\"panel mbn\" style=\"border: 1px solid #c2c8d8;\">\n                                                            <div class=\"panel-heading\" style=\"height: auto\">\n                                                                <p style=\"border-radius: 0px;background-color:transparent;color:black\">\n                                                                    Pending Withdraw Ethereum Transactions\n                                                                </p>\n                                                            </div>\n                                                            <div class=\"panel-collapse collapse in \"\n                                                                 style=\"background-color: white\">\n                                                                <div class=\"row\" style=\"margin-top: 0;\">\n                                                                    <div class=\"col-sm-12\">\n                                                                        <div class=\"table-responsive\">\n                                                                            <table cellspacing=\"0\"\n                                                                                   class=\"table table-striped table-hover table-condensed text-black \"\n                                                                                   width=\"100%\">\n                                                                                <thead>\n                                                                                <tr>\n                                                                                    <th>To Address</th>\n                                                                                    <th>ETH</th>\n                                                                                    <th>Status</th>\n                                                                                    <th>Action</th>\n                                                                                </tr>\n                                                                                </thead>\n                                                                                <tbody>\n                                                                                <tr *ngFor=\"let tx of ethSentTxList\">\n                                                                                    <td>{{tx.address}}</td>\n                                                                                    <td>{{tx.src_amount | number:'1.8-8'}}</td>\n                                                                                    <td>PENDING</td>\n                                                                                    <td>\n                                                                                        <button type=\"button\" class=\"btn btn-danger btn-sm\" (click)=\"cancelWithdraw('ETH', tx.id)\">\n                                                                                            <i class=\"fa fa-close\"></i>\n                                                                                        </button>\n                                                                                    </td>\n                                                                                </tr>\n                                                                                <!---->\n                                                                                <tr *ngIf=\"ethSentTxList?.length == 0\">\n                                                                                    <td class=\"text-center\" colspan=\"4\">\n                                                                                        No Pending Withdraw Ethereum\n                                                                                        transactions found.\n                                                                                    </td>\n                                                                                </tr>\n                                                                                <!---->\n                                                                                </tbody>\n                                                                            </table>\n                                                                        </div>\n                                                                    </div>\n                                                                </div>\n                                                            </div>\n                                                        </div>\n                                                    </div>\n                                                </div>\n                                                <p>After request, we will send your transaction to our fraud detection\n                                                    AI system. Your transaction will be processed quickly</p>\n                                            </div>\n                                        </div>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n\n\n<modal-dialog [(visible)]=\"showAuthModal\" [closable]=\"false\">\n    <form class=\"form-horizontal\">\n        <h2>\n            INPUT YOUR AUTHENTICATOR CODE\n        </h2>\n\n        <div class=\"swal2-validationerror\" style=\"display: block;\">\n            Please check your email.\n        </div>\n\n        <input type=\"text\" class=\"form-control\" style=\"text-align: center; font-size: 20px;\" name=\"authCode\" [(ngModel)]=\"authCode\">\n\n        <div class=\"swal2-validationerror\" style=\"display: block;\" *ngIf=\"!codeMatch\">\n            The authentication code you specified is incorrect. Please check your email again.\n        </div>\n\n        <div class=\"text-center\" style=\"margin-top: 20px;\">\n            <button type=\"button\" class=\"btn btn-primary\" (click)=\"submitCode()\">Submit</button>\n            <button type=\"button\" class=\"btn btn-default\" (click)=\"showAuthModal = !showAuthModal\">Cancel</button>\n        </div>\n    </form>\n</modal-dialog>"

/***/ }),

/***/ "../../../../../src/app/pages/wallet/wallet.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".important-note {\n  color: #ff4c4c !important;\n  font-size: 11px;\n  font-weight: 700;\n  font-style: italic; }\n\n.vat {\n  vertical-align: top !important; }\n\n.btn2 {\n  margin-right: -5px;\n  display: inline-block;\n  padding: 6px 12px;\n  margin-bottom: 0;\n  font-size: 14px;\n  font-weight: normal;\n  line-height: 1.42857143;\n  text-align: center;\n  white-space: nowrap;\n  vertical-align: middle;\n  cursor: pointer;\n  -webkit-user-select: none;\n  -moz-user-select: none;\n  -ms-user-select: none;\n  user-select: none;\n  background-image: none;\n  border: 1px solid transparent;\n  border-radius: 4px; }\n\n.btn2-default {\n  color: #333;\n  background-color: #e5eaee;\n  border-color: #d5dadd; }\n\n.qrCode {\n  width: 160px; }\n\n.panel-group .panel-heading {\n  position: relative;\n  display: block;\n  width: 100%; }\n\n.panel-heading {\n  height: 52px;\n  line-height: 49px;\n  letter-spacing: .2px;\n  color: #a9acbd;\n  font-size: 15px;\n  font-weight: 400;\n  padding: 0 13px;\n  background: #fafafa;\n  border: 1px solid #e5eaee;\n  border-bottom-left-radius: 4px;\n  border-top-left-radius: 4px; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/pages/wallet/wallet.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WalletComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_settings_settings_service__ = __webpack_require__("../../../../../src/app/services/settings/settings.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_api_service__ = __webpack_require__("../../../../../src/app/services/api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_notifications_service__ = __webpack_require__("../../../../../src/app/services/notifications.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_validate_service__ = __webpack_require__("../../../../../src/app/services/validate.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_balance_service__ = __webpack_require__("../../../../../src/app/services/balance.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var WalletComponent = (function () {
    function WalletComponent(formBuilder, settings, api, notify, validate, balance) {
        this.formBuilder = formBuilder;
        this.settings = settings;
        this.api = api;
        this.notify = notify;
        this.validate = validate;
        this.balance = balance;
        this.bitcoin = {
            amount: 0
        };
        this.ethereum = {
            amount: 0
        };
        this.bt9coin = {
            amount: 0
        };
        this.btcSentTxList = [];
        this.btcReceivedTxList = [];
        this.bt9SentTxList = [];
        this.bt9ReceivedTxList = [];
        this.ethSentTxList = [];
        this.ethReceivedTxList = [];
        this.authCode = '';
        this.codeMatch = true;
        this.showAuthModal = false;
        this.withdrawCoin = 'BTC';
    }
    WalletComponent.prototype.ngOnInit = function () {
        this.loadBTCSentTxList();
        this.loadBTCReceivedTxList();
        this.loadETHSentTxList();
        this.loadETHReceivedTxList();
        this.loadBT9SentTxList();
        this.loadBT9ReceivedTxList();
        this.withdrawBTCForm = this.formBuilder.group({
            address: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required],
            amount: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required],
        });
        this.withdrawETHForm = this.formBuilder.group({
            address: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required],
            amount: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required],
        });
        this.withdrawBT9Form = this.formBuilder.group({
            address: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required],
            amount: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required],
        });
    };
    WalletComponent.prototype.loadBTCSentTxList = function () {
        var _this = this;
        this.api.getUnconfirmedSentTxList('BTC', {}).subscribe(function (res) {
            if (res.success) {
                _this.btcSentTxList = res.data;
            }
        }, function (err) {
        });
    };
    WalletComponent.prototype.loadBTCReceivedTxList = function () {
        var _this = this;
        this.api.getUnconfirmedReceivedTxList('BTC', {}).subscribe(function (res) {
            if (res.success) {
                _this.btcReceivedTxList = res.data;
            }
        }, function (err) {
        });
    };
    WalletComponent.prototype.loadBT9SentTxList = function () {
        var _this = this;
        this.api.getUnconfirmedSentTxList('BT9', {}).subscribe(function (res) {
            if (res.success) {
                _this.bt9SentTxList = res.data;
            }
        }, function (err) {
        });
    };
    WalletComponent.prototype.loadBT9ReceivedTxList = function () {
        var _this = this;
        this.api.getUnconfirmedReceivedTxList('BT9', {}).subscribe(function (res) {
            if (res.success) {
                _this.bt9ReceivedTxList = res.data;
            }
        }, function (err) {
        });
    };
    WalletComponent.prototype.loadETHSentTxList = function () {
        var _this = this;
        this.api.getUnconfirmedSentTxList('ETH', {}).subscribe(function (res) {
            if (res.success) {
                _this.ethSentTxList = res.data;
            }
        }, function (err) {
        });
    };
    WalletComponent.prototype.loadETHReceivedTxList = function () {
        var _this = this;
        this.api.getUnconfirmedReceivedTxList('ETH', {}).subscribe(function (res) {
            if (res.success) {
                _this.ethReceivedTxList = res.data;
            }
        }, function (err) {
        });
    };
    WalletComponent.prototype.ethAddressValidate = function (address) {
        if (!/^(0x)?[0-9a-f]{40}$/i.test(address)) {
            // check if it has the basic requirements of an address
            return false;
        }
        else if (/^(0x)?[0-9a-f]{40}$/.test(address) || /^(0x)?[0-9A-F]{40}$/.test(address)) {
            // If it's all small caps or all all caps, return true
            return true;
        }
        else {
            // Otherwise check each case
            return this.isChecksumAddress(address);
        }
    };
    WalletComponent.prototype.isChecksumAddress = function (address) {
        // // Check each case
        // address = address.replace('0x', '');
        //
        // let addressHash = web3.sha3(address.toLowerCase());
        //
        // for (let i = 0; i < 40; i++ ) {
        //     // the nth letter should be uppercase if the nth digit of casemap is 1
        //     if ((parseInt(addressHash[i], 16) > 7 && address[i].toUpperCase() !== address[i]) || (parseInt(addressHash[i], 16) <= 7 && address[i].toLowerCase() !== address[i])) {
        //         return false;
        //     }
        // }
        return true;
    };
    WalletComponent.prototype.cancelWithdraw = function (coin, id) {
        var _this = this;
        this.settings.loading = true;
        this.api.cancelWithdrawCoin(coin, {
            id: id
        }).subscribe(function (res) {
            _this.settings.loading = false;
            if (res.success) {
                swal({
                    type: 'success',
                    title: 'Success',
                    text: 'Your withdraw has canceled successfully',
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-success'
                });
                if (coin == 'BTC') {
                    _this.balance.getBtcBalance();
                    _this.loadBTCSentTxList();
                }
                else if (coin == 'ETH') {
                    _this.balance.getEthBalance();
                    _this.loadETHSentTxList();
                }
                else if (coin == 'BT9') {
                    _this.balance.getTokenBalance();
                    _this.loadBT9SentTxList();
                }
            }
            else {
                swal({
                    type: 'warning',
                    title: 'Error',
                    text: res.error,
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-warning'
                });
            }
        }, function (err) {
            _this.settings.loading = false;
            swal({
                type: 'warning',
                title: 'Error',
                text: err._body,
                buttonsStyling: false,
                confirmButtonClass: 'btn btn-warning'
            });
        });
    };
    WalletComponent.prototype.withdraw = function (coin) {
        var _this = this;
        this.bitcoin.fee = this.settings.getSysSetting('withdraw_btc_fee');
        this.ethereum.fee = this.settings.getSysSetting('withdraw_eth_fee');
        this.bt9coin.fee = this.settings.getSysSetting('withdraw_bt9_fee');
        var params = this.bitcoin;
        if (coin == 'ETH') {
            params = this.ethereum;
        }
        else if (coin == 'BT9') {
            params = this.bt9coin;
        }
        this.settings.loading = true;
        this.api.withdrawCoin(coin, params).subscribe(function (res1) {
            _this.settings.loading = false;
            if (res1.success) {
                swal({
                    type: 'success',
                    title: 'Success',
                    text: 'Your withdraw has completed successfully',
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-success'
                });
                if (coin == 'BTC') {
                    _this.balance.getBtcBalance();
                    _this.loadBTCSentTxList();
                }
                else if (coin == 'ETH') {
                    _this.balance.getEthBalance();
                    _this.loadETHSentTxList();
                }
                else if (coin == 'BT9') {
                    _this.balance.getTokenBalance();
                    _this.loadBT9SentTxList();
                }
            }
            else {
                swal({
                    type: 'warning',
                    title: 'Error',
                    text: res1.error,
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-warning'
                });
            }
        }, function (err) {
            _this.settings.loading = false;
            swal({
                type: 'warning',
                title: 'Error',
                text: err._body,
                buttonsStyling: false,
                confirmButtonClass: 'btn btn-warning'
            });
        });
    };
    WalletComponent.prototype.onWithdrawBTC = function () {
        var _parent = this;
        if (this.withdrawBTCForm.valid) {
            if (Number(this.bitcoin.amount) < 0.001) {
                swal({
                    type: 'warning',
                    title: 'Error',
                    text: 'Minimum Withdrawal is 0.001 BTC',
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-success'
                });
                return;
            }
            if (this.settings.getUserSetting('enable_withdrawmail') == 1) {
                this.withdrawCoin = 'BTC';
                this.sendMail();
            }
            else {
                this.withdraw('BTC');
            }
        }
        else {
            this.validate.validateAllFormFields(this.withdrawBTCForm);
        }
    };
    WalletComponent.prototype.onWithdrawETH = function () {
        var _parent = this;
        if (this.withdrawETHForm.valid) {
            if (Number(this.ethereum.amount) < 0.005) {
                swal({
                    type: 'warning',
                    title: 'Error',
                    text: 'Minimum Withdrawal is 0.005 ETH',
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-success'
                });
                return;
            }
            if (!this.ethAddressValidate(this.ethereum.address)) {
                swal({
                    type: 'warning',
                    title: 'Error',
                    text: 'Ethereum Address is not validate',
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-warning'
                });
                return;
            }
            if (this.settings.getUserSetting('enable_withdrawmail') == 1) {
                this.sendMail();
                this.withdrawCoin = 'ETH';
            }
            else {
                this.withdraw('ETH');
            }
        }
        else {
            this.validate.validateAllFormFields(this.withdrawETHForm);
        }
    };
    WalletComponent.prototype.onWithdrawBT9 = function () {
        var _parent = this;
        if (this.withdrawBT9Form.valid) {
            if (Number(this.bt9coin.amount) < 0.005) {
                swal({
                    type: 'warning',
                    title: 'Error',
                    text: 'Minimum Withdrawal is 0.005 BT9',
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-success'
                });
                return;
            }
            if (this.settings.getUserSetting('enable_withdrawmail') == 1) {
                this.sendMail();
                this.withdrawCoin = 'BT9';
            }
            else {
                this.withdraw('BT9');
            }
        }
        else {
            this.validate.validateAllFormFields(this.withdrawBT9Form);
        }
    };
    WalletComponent.prototype.sendMail = function () {
        var _this = this;
        this.settings.loading = true;
        this.api.sendWithdrawMail({}).subscribe(function (res) {
            _this.settings.loading = false;
            if (res.success) {
                _this.showAuthModal = true;
                _this.notify.showNotification('Confirm Email was send successfully. Please check your email and input code.', 'top', 'center', 'success');
            }
        }, function (err) {
            _this.settings.loading = false;
        });
    };
    WalletComponent.prototype.submitCode = function () {
        var _this = this;
        if (this.authCode == '') {
            return;
        }
        this.codeMatch = true;
        this.api.confirmWithdrawCode({
            code: this.authCode
        }).subscribe(function (res) {
            if (res.success) {
                _this.withdraw(_this.withdrawCoin);
                _this.showAuthModal = false;
            }
            else {
                _this.codeMatch = false;
            }
        }, function (err) {
            _this.codeMatch = false;
        });
    };
    WalletComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-wallet',
            template: __webpack_require__("../../../../../src/app/pages/wallet/wallet.component.html"),
            styles: [__webpack_require__("../../../../../src/app/pages/wallet/wallet.component.scss")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormBuilder"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormBuilder"]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__services_settings_settings_service__["a" /* SettingsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_settings_settings_service__["a" /* SettingsService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__services_api_service__["a" /* Api */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services_api_service__["a" /* Api */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_4__services_notifications_service__["a" /* Notifications */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__services_notifications_service__["a" /* Notifications */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_5__services_validate_service__["a" /* Validate */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__services_validate_service__["a" /* Validate */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_6__services_balance_service__["a" /* BalanceService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__services_balance_service__["a" /* BalanceService */]) === "function" && _f || Object])
    ], WalletComponent);
    return WalletComponent;
    var _a, _b, _c, _d, _e, _f;
}());

//# sourceMappingURL=wallet.component.js.map

/***/ }),

/***/ "../../../../../src/app/pages/wallet/wallet.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WalletModule", function() { return WalletModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__md_md_module__ = __webpack_require__("../../../../../src/app/md/md.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__wallet_component__ = __webpack_require__("../../../../../src/app/pages/wallet/wallet.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__wallet_routing__ = __webpack_require__("../../../../../src/app/pages/wallet/wallet.routing.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__shared_shared_module__ = __webpack_require__("../../../../../src/app/shared/shared.module.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var WalletModule = (function () {
    function WalletModule() {
    }
    WalletModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_2__angular_common__["b" /* CommonModule */],
                __WEBPACK_IMPORTED_MODULE_1__angular_router__["d" /* RouterModule */].forChild(__WEBPACK_IMPORTED_MODULE_7__wallet_routing__["a" /* WalletRoutes */]),
                __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormsModule"],
                __WEBPACK_IMPORTED_MODULE_4__md_md_module__["a" /* MdModule */],
                __WEBPACK_IMPORTED_MODULE_5__app_module__["b" /* MaterialModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_forms__["ReactiveFormsModule"],
                __WEBPACK_IMPORTED_MODULE_8__shared_shared_module__["a" /* SharedModule */]
            ],
            declarations: [__WEBPACK_IMPORTED_MODULE_6__wallet_component__["a" /* WalletComponent */]]
        })
    ], WalletModule);
    return WalletModule;
}());

//# sourceMappingURL=wallet.module.js.map

/***/ }),

/***/ "../../../../../src/app/pages/wallet/wallet.routing.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WalletRoutes; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__wallet_component__ = __webpack_require__("../../../../../src/app/pages/wallet/wallet.component.ts");

var WalletRoutes = [
    {
        path: '',
        component: __WEBPACK_IMPORTED_MODULE_0__wallet_component__["a" /* WalletComponent */]
    }
];
//# sourceMappingURL=wallet.routing.js.map

/***/ })

});
//# sourceMappingURL=wallet.module.chunk.js.map