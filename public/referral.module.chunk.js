webpackJsonp(["referral.module"],{

/***/ "../../../../../src/app/pages/referral/referral.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\n    <div class=\"container-fluid\">\n        <div class=\"row\">\n            <div class=\"col-lg-12\">\n                <div class=\"card\">\n                    <div class=\"card-header card-title\">\n                        REFERRAL MEMBER\n                    </div>\n                    <div class=\"card-content\">\n                        <ay-treeTable [value]=\"referraldata\" [rows]=\"10\" [paginator]=\"true\" [rowsPerPageOptions]=\"[5,10,20]\">\n                            <ay-column field=\"username\" header=\"Username\"></ay-column>\n                            <ay-column field=\"token_amount\" header=\"Purchased BT9Coin\"></ay-column>\n                            <ay-column field=\"bonus_percent\" header=\"MLM Bonus(%)\"></ay-column>\n                            <ay-column field=\"bonus_amount\" header=\"MLM Bonus(BT9)\"></ay-column>\n                            <ay-column field=\"lending_bonus\" header=\"LENDING Bonus(BT9)\"></ay-column>\n                            <ay-column field=\"level\" header=\"Ref Level\"></ay-column>\n                        </ay-treeTable>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/pages/referral/referral.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReferralComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_api_service__ = __webpack_require__("../../../../../src/app/services/api.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ReferralComponent = (function () {
    function ReferralComponent(api) {
        this.api = api;
        this.referraldata = [];
    }
    ReferralComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.api.getReferral({}).subscribe(function (res) {
            if (res.success) {
                _this.referraldata = res.data;
            }
        }, function (err) {
        });
    };
    ReferralComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-stareferralking',
            template: __webpack_require__("../../../../../src/app/pages/referral/referral.component.html"),
            encapsulation: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewEncapsulation"].None
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_api_service__["a" /* Api */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_api_service__["a" /* Api */]) === "function" && _a || Object])
    ], ReferralComponent);
    return ReferralComponent;
    var _a;
}());

//# sourceMappingURL=referral.component.js.map

/***/ }),

/***/ "../../../../../src/app/pages/referral/referral.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReferralModule", function() { return ReferralModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__md_md_module__ = __webpack_require__("../../../../../src/app/md/md.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__referral_component__ = __webpack_require__("../../../../../src/app/pages/referral/referral.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__referral_routing__ = __webpack_require__("../../../../../src/app/pages/referral/referral.routing.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_ng_treetable__ = __webpack_require__("../../../../ng-treetable/index.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var ReferralModule = (function () {
    function ReferralModule() {
    }
    ReferralModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_2__angular_common__["b" /* CommonModule */],
                __WEBPACK_IMPORTED_MODULE_1__angular_router__["d" /* RouterModule */].forChild(__WEBPACK_IMPORTED_MODULE_7__referral_routing__["a" /* ReferralRoutes */]),
                __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormsModule"],
                __WEBPACK_IMPORTED_MODULE_4__md_md_module__["a" /* MdModule */],
                __WEBPACK_IMPORTED_MODULE_5__app_module__["b" /* MaterialModule */],
                __WEBPACK_IMPORTED_MODULE_8_ng_treetable__["a" /* TreeTableModule */]
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_6__referral_component__["a" /* ReferralComponent */]
            ],
            bootstrap: [
                __WEBPACK_IMPORTED_MODULE_6__referral_component__["a" /* ReferralComponent */]
            ]
        })
    ], ReferralModule);
    return ReferralModule;
}());

//# sourceMappingURL=referral.module.js.map

/***/ }),

/***/ "../../../../../src/app/pages/referral/referral.routing.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReferralRoutes; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__referral_component__ = __webpack_require__("../../../../../src/app/pages/referral/referral.component.ts");

var ReferralRoutes = [
    {
        path: '',
        component: __WEBPACK_IMPORTED_MODULE_0__referral_component__["a" /* ReferralComponent */]
    }
];
//# sourceMappingURL=referral.routing.js.map

/***/ })

});
//# sourceMappingURL=referral.module.chunk.js.map