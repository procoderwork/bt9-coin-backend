<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

interface MarketOrderType {
    const BUY = 0;
    const SELL = 1;
}

class Order extends Model
{
    //
    protected $table = 'orders';
}
