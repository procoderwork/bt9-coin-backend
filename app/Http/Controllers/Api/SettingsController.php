<?php

namespace App\Http\Controllers\Api;

use App\Entities\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SettingsController extends Controller
{
    //
    public function all() {
        $settings = Setting::all()->pluck('value', 'name');

        return response()->json([
            'success' => true,
            'data' => $settings
        ]);
    }
}
