<?php

namespace App\Http\Controllers\Api;

use App\Entities\BonusList;
use App\Entities\BtcAddress;
use App\Entities\EthAddress;
use App\Entities\ExchangeList;
use App\Entities\ICOAgenda;
use App\Entities\Lending;
use App\Entities\LendingSetting;
use App\Entities\LoginHistory;
use App\Entities\ResetPassword;
use App\Entities\Setting;
use App\Entities\TokenRate;
use App\Entities\TransactionStatus;
use App\Entities\TransactionType;
use App\Events\SendEmailEvent;
use App\Libs\GoogleAuthenticator;
use App\Mail\ForgotPasswordMail;
use App\Mail\LoginNotificationMail;
use App\Mail\VerifyAccountMail;
use App\User;
use BlockCypher\Auth\SimpleTokenCredential;
use BlockCypher\Client\AddressClient;
use BlockCypher\Rest\ApiContext;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Symfony\Component\HttpFoundation\Exception\RequestExceptionInterface;
use tests\Simple;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;
use Jenssegers\Agent\Agent;

class StakingController extends Controller
{
    public function save(Request $request) {
        $input = $request->input();

        $user = Auth::user();

        $tokenController = new TokenController();
        $token_balance = $tokenController->balance($user);

        if ($input['amount'] > $token_balance) {
            return response()->json([
                'success' => false,
                'error' => 'Your BT9Coin balance is not enough to buy lending'
            ]);
        }

        $stacking = new ExchangeList();
        $stacking->user_id = $user->id;
        $stacking->src_currency = 'TOKEN';
        $stacking->src_amount = $input['amount'];
        $stacking->date = Carbon::now();
        $stacking->finish_date = Carbon::now()->addDay($input['after']);
        $stacking->type = TransactionType::STAKING;
        $stacking->status = TransactionStatus::SUCCESS;
        $stacking->rate = round($input['daily'], 2);
        $stacking->save();

        $stacking = new ExchangeList();
        $stacking->user_id = $user->id;
        $stacking->dest_currency = 'TOKEN';
        $stacking->dest_amount = $input['amount'];
        $stacking->date = Carbon::now()->addDay($input['after']);
        $stacking->type = TransactionType::STAKING_TRANSFER;
        $stacking->status = TransactionStatus::SUCCESS;
        $stacking->rate = round($input['daily'], 2);
        $stacking->save();

        return response()->json([
            'success' => true,
        ]);
    }

    public function get(Request $request) {
        $input = $request->input();

        $user = Auth::user();
        $lendings = ExchangeList::where([['user_id', $user->id], ['type', TransactionType::STAKING]])->get();

        return response()->json([
            'success' => true,
            'data' => $lendings
        ]);
    }

    public function getAllStaking(Request $request) {
        $input = $request->input();

        $user = Auth::user();
        $stakings = ExchangeList::where('type', TransactionType::STAKING)->leftJoin('users', 'users.id', 'exchange_lists.user_id')->select('exchange_lists.*', 'users.email')->get();

        return response()->json([
            'success' => true,
            'data' => $stakings
        ]);
    }

    public function getStakingBalance(Request $request) {
        $input = $request->input();

        $user = Auth::user();

        $locked_amount = ExchangeList::where([['user_id', $user->id], ['type', TransactionType::STAKING], ['user_id', $user->id], ['finish_date', '>', Carbon::now()]])->sum('src_amount');
        $total_amount = ExchangeList::where([['user_id', $user->id], ['type', TransactionType::STAKING], ['user_id', $user->id]])->sum('src_amount');

        $earned_amount = 0;

        $stakings = ExchangeList::where([['user_id', $user->id], ['type', TransactionType::STAKING]])->get();
        foreach ($stakings as $staking) {
            $date = new Carbon($staking->date);
            $finish_date = new Carbon($staking->finish_date);
            $now = Carbon::now();

            $date->addDay(1);
            while ($date <= $finish_date && $date <= $now) {
                $earned_amount += $staking->src_amount * $staking->rate * 0.01;
                $date->addDay(1);
            }
        }

        return response()->json([
            'success' => true,
            'locked_amount' => $locked_amount,
            'total_amount' => $total_amount,
            'earned_amount' => $earned_amount
        ]);
    }


    public function releaseAll(Request $request) {
        $input = $request->input();

        $query = "UPDATE exchange_lists SET finish_date = '".Carbon::now()->toDateTimeString()."' WHERE type = ".TransactionType::STAKING." AND finish_date > '".Carbon::now()->toDateTimeString()."'";
        $result = \DB::update($query);

        $query = "UPDATE exchange_lists SET date = '".Carbon::now()->toDateTimeString()."' WHERE type = ".TransactionType::STAKING_TRANSFER." AND date > '".Carbon::now()->toDateTimeString()."'";
        $result = \DB::update($query);

        return response()->json([
            'success' => true,
        ]);
    }
}
