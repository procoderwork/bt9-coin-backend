<?php

namespace App\Http\Controllers\Api;

use App\Entities\BonusList;
use App\Entities\Bt9Address;
use App\Entities\BTCOrder;
use App\Entities\BtcTestAddress;
use App\Entities\CoinWithdrawHistory;
use App\Entities\ETHOrder;
use App\Entities\ExchangeList;
use App\Entities\ICOAgenda;
use App\Entities\Lending;
use App\Entities\MarketOrderType;
use App\Entities\Order;
use App\Entities\Setting;
use App\Entities\TokenRate;
use App\Entities\TransactionStatus;
use App\Entities\TransactionType;
use App\Libs\EasyBt9Coin;
use App\Libs\JsonRPCClient;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class TokenController extends Controller
{
    public $BIT_SATOSHI = 100000000;

    public function withdrawCron(Request $request) {

        \Log::info('start schedule');

        $btcController = new BTCController();
        $btcController->withdrawSchedule();

        $this->withdrawSchedule();

        return response()->json([
            'success' => true
        ]);
    }
    public function test() {
        $bt9coin = new EasyBt9Coin('rpc_user7','cbc7jk3n6z8yhzfmkhtq6qfm','188.226.153.110','31686');

        $hex = '010000003402655a0003008d380c010000001976a914bd16715ae743a5dea17b7aea277cd4cf948f49e988ac00ab9041000000001976a91437f47c45e53c52412455e31ec9512fd6d83abc5288ac0057d347010000001976a9144e78dac8d2cd094a186616bd606fe3d34b217a1288ac00000000';
        $txid = '576f77ac05a5cc9722c5b4c9b9bcf51451c7c382849601d7077c09c80c785834';

        // $info = $bt9coin->sendmany('', $outputs);
        $info = $bt9coin->validateaddress('B4q4JrdyLjkVqDCeYZ29T1Yw3iSRKbZP8M');

        var_dump($info);
        //$info = $bt9coin->listaccounts();
        //var_dump($info);

        echo $bt9coin->error.'<br>';

        echo $bt9coin->status;
    }

    public function getRPCUrl($settings) {
        $rpcUrl = 'http://'.$settings['bt9_rpcuser'].':'.$settings['bt9_rpcpassword'].'@'.$settings['bt9_rpcserver'].':'.$settings['bt9_rpcport'].'/';

        return $rpcUrl;
    }


    public function getCurrentRate(Request $request) {
        try {
            $rates = json_decode(file_get_contents('https://api.cryptowat.ch/markets/prices'), true);
//            $rates = file_get_contents('https://blockchain.info/ticker');
        } catch (\Exception $e) {
            return response($e->getMessage(), 403);
        }

        return response()->json([
            'success' => true,
            'btc_rate' => $rates['result']['poloniex:btcusdt'],
            'eth_rate' => $rates['result']['poloniex:ethusdt']
        ]);
    }

    public function currentRate() {
        $agendas = ICOAgenda::get();

        $now = Carbon::now();

        $token_usd = 0;
        foreach ($agendas as $agenda) {
            $from = new Carbon($agenda->from);
            $to = new Carbon($agenda->to);

            $to->addDay(1);

            if ($from <= $now && $to > $now) {
                $token_usd = $agenda->price;
                break;
            }
        }

        if ($token_usd == 0) {
            $last_price = ExchangeList::where([['type', TransactionType::EXCHANGE], ['status', TransactionStatus::SUCCESS], ['dest_currency', 'BTC']])->orderBy('created_at', 'desc')->first();

            $bt9_btc = $last_price ? $last_price['rate'] : 0;

            $btc_usd = 0;
            try {
                $rates = json_decode(file_get_contents('https://api.cryptowat.ch/markets/prices'), true);
                $btc_usd = $rates['result']['poloniex:btcusdt'];
            } catch (\Exception $e) {
                $btc_usd = 0;
            }

            $token_usd = $bt9_btc * $btc_usd;
        }
        return $token_usd;
    }

    public function getTokenRate(Request $request) {

        return response()->json([
            'success' => true,
            'rate' => $this->currentRate()
        ]);
    }
    public function balance($user) {
        $sent = ExchangeList::where([['src_currency', 'TOKEN'], ['user_id', $user->id], ['date', '<=', Carbon::now()]])->sum('src_amount');

        $receive = ExchangeList::where([['dest_currency', 'TOKEN'], ['user_id', $user->id], ['date', '<=', Carbon::now()]])->sum('dest_amount');

        $btc_buy = BTCOrder::where([['user_id', $user->id], ['type', MarketOrderType::SELL]])->sum(\DB::raw('amount'));
        $eth_buy = ETHOrder::where([['user_id', $user->id], ['type', MarketOrderType::SELL]])->sum(\DB::raw('amount'));

        $earned_amount = 0;

        $stakings = ExchangeList::where([['user_id', $user->id], ['type', TransactionType::STAKING]])->get();
        foreach ($stakings as $staking) {
            $date = new Carbon($staking->date);
            $finish_date = new Carbon($staking->finish_date);
            $now = Carbon::now();

            $date->addDay(1);
            while ($date <= $finish_date && $date <= $now) {
                $earned_amount += $staking->src_amount * $staking->rate * 0.01;
                $date->addDay(1);
            }
        }

        $balance = (-1) * $sent + $receive - $btc_buy - $eth_buy + $earned_amount;

        $settings = Setting::all()->pluck('value', 'name');

        if ($settings['enable_bt9_wallet'] == 1) {
            $rpcUrl = $this->getRPCUrl($settings);

            $jsonRpc = new JsonRPCClient($rpcUrl);

            $label = $user->email;

            try {
                $wallet_balance = $jsonRpc->getreceivedbyaccount($label);
            } catch (\Exception $e) {
                $wallet_balance = 0;
            }
            $balance += $wallet_balance;
        }

        return $balance;
    }

    public function usdbalance($user) {
        $sent = ExchangeList::where([['src_currency', 'USD'], ['user_id', $user->id]])->sum('src_amount');
        $receive = ExchangeList::where([['dest_currency', 'USD'], ['user_id', $user->id]])->sum('dest_amount');

        // $lending = Lending::where('user_id', $user->id)->sum('amount');

        $balance = $sent + $receive;

        return $balance;
    }

    public function getBalance(Request $request) {
        $user = Auth::user();

        if (!$user) {
            return response('Not Authorized', 403);
        }

        return response()->json([
            'success' => true,
            'balance' => $this->balance($user)
        ]);
    }

    public function totalBalance(Request $request) {
        $total = ExchangeList::where('dest_currency', 'TOKEN')->sum('dest_amount');

        return response()->json([
            'success' => true,
            'balance' => $total
        ]);
    }

    public function BuyTokenWithBTC(Request $request) {
        $input = $request->input();

        $user = Auth::user();

        $amount = $input['amount'];
        $token_btc = $input['token_btc'];
        $token_usd = $input['token_usd'];
        // $bonus = $input['bonus'];

        if ($amount == 0) {
            return response()->json([
                "success" => false,
                "error" => "Amount should not be zero."
            ]);
        }

        $settings = Setting::all()->pluck('value', 'name');

        $daily_amount = ExchangeList::where([['user_id', $user->id], ['type', TransactionType::BUYTOKEN]])->whereDate('date', Carbon::now()->toDateString())->sum('dest_amount');
        $total_amount = ExchangeList::where([['type', TransactionType::BUYTOKEN]])->whereDate('date', Carbon::now()->toDateString())->sum('dest_amount');

        $agendas = ICOAgenda::get();

        $now = Carbon::now();

        $per_account = 0;
        $per_day = 0;
        foreach ($agendas as $agenda) {
            $from = new Carbon($agenda->from);
            $to = new Carbon($agenda->to);

            $to->addDay(1);

            if ($from <= $now && $to > $now) {
                $per_account = $agenda->per_account;
                $per_day = $agenda->total;
                break;
            }
        }

        if ($per_account == 0) {
            return response()->json([
                "success" => false,
                "error" => "You can't buy BT9."
            ]);
        }

        if ($daily_amount + $amount > $per_account) {
            return response()->json([
                "success" => false,
                "error" => "You can't buy BT9. Maximum amount per day per account is ".$per_account." BT9"
            ]);
        }

        if ($total_amount + $amount > $per_day) {
            return response()->json([
                "success" => false,
                "error" => "You can't buy BT9. Maximum amount per day is ".$per_day." BT9. Available BT9 is ".($per_day - $total_amount)
            ]);
        }

        $btc_amount = $amount * $token_btc;

        $btcController = new BTCController();
        $btc_balance = $btcController->balance($user);

        if ($btc_amount > $btc_balance) {
            return response()->json([
                "success" => false,
                "error" => "BTC Balance is not enough."
            ]);
        }

        $exchange = new ExchangeList();

        $exchange->date = Carbon::now();
        $exchange->user_id = $user->id;
        $exchange->src_currency = 'BTC';
        $exchange->dest_currency = 'TOKEN';
        $exchange->src_amount = $btc_amount;
        $exchange->dest_amount = $amount;
        $exchange->rate = $token_btc;
        $exchange->usd_rate = $token_usd;
        $exchange->status = TransactionStatus::SUCCESS;
        $exchange->type = TransactionType::BUYTOKEN;
        $exchange->save();

        $parent_id = $user->parent;
        $bonus = BonusList::pluck('bonus');

        for ($i = 0; $i < 5; $i ++) {
            if ($parent_id == 0 || $parent_id == null) {
                break;
            }

            $parent_user = User::find($parent_id);

            $exchange = new ExchangeList();

            $exchange->date = Carbon::now();
            $exchange->user_id = $parent_id;
            $exchange->dest_currency = 'TOKEN';
            $exchange->dest_amount = $amount * $bonus[$i] / 100;
            $exchange->note = 'BONUS';
            $exchange->ref_id = $user->id;
            $exchange->ref_lvl = ($i + 1);
            $exchange->usd_rate = $token_usd;

            $exchange->status = TransactionStatus::SUCCESS;
            $exchange->type = TransactionType::TOKEN_BONUS;
            $exchange->save();

            $parent_id = $parent_user->parent;
        }

        return response()->json([
            'success' => true
        ]);
    }

    public function BuyTokenWithETH(Request $request) {
        $input = $request->input();

        $user = Auth::user();

        $amount = $input['amount'];
        $token_eth = $input['token_eth'];
        $token_usd = $input['token_usd'];
        // $bonus = $input['bonus'];

        if ($amount == 0) {
            return response()->json([
                "success" => false,
                "error" => "Amount should not be zero."
            ]);
        }

        $daily_amount = ExchangeList::where([['user_id', $user->id], ['type', TransactionType::BUYTOKEN]])->whereDate('date', Carbon::now()->toDateString())->sum('dest_amount');
        $total_amount = ExchangeList::where([['type', TransactionType::BUYTOKEN]])->whereDate('date', Carbon::now()->toDateString())->sum('dest_amount');

        $agendas = ICOAgenda::get();

        $now = Carbon::now();

        $per_account = 0;
        $per_day = 0;
        foreach ($agendas as $agenda) {
            $from = new Carbon($agenda->from);
            $to = new Carbon($agenda->to);

            $to->addDay(1);

            if ($from <= $now && $to > $now) {
                $per_account = $agenda->per_account;
                $per_day = $agenda->total;
                break;
            }
        }

        if ($per_account == 0) {
            return response()->json([
                "success" => false,
                "error" => "You can't buy BT9."
            ]);
        }

        if ($daily_amount + $amount > $per_account) {
            return response()->json([
                "success" => false,
                "error" => "You can't buy BT9. Maximum amount per day per account is ".$per_account." BT9"
            ]);
        }

        if ($total_amount + $amount > $per_day) {
            return response()->json([
                "success" => false,
                "error" => "You can't buy BT9. Maximum amount per day is ".$per_day." BT9. Available BT9 is ".($per_day - $total_amount)
            ]);
        }

        $eth_amount = $amount * $token_eth;

        $ethController = new ETHController();
        $eth_balance = $ethController->balance($user);

        if ($eth_amount > $eth_balance) {
            return response()->json([
                "success" => false,
                "error" => "ETH Balance is not enough."
            ]);
        }

        // save to db

        $exchange = new ExchangeList();

        $exchange->date = Carbon::now();
        $exchange->user_id = $user->id;
        $exchange->src_currency = 'ETH';
        $exchange->dest_currency = 'TOKEN';
        $exchange->src_amount = $eth_amount;
        $exchange->dest_amount = $amount;
        $exchange->rate = $token_eth;
        $exchange->usd_rate = $token_usd;
        $exchange->status = TransactionStatus::SUCCESS;
        $exchange->type = TransactionType::BUYTOKEN;
        $exchange->save();

        $parent_id = $user->parent;
        $bonus = BonusList::pluck('bonus');

        for ($i = 0; $i < 5; $i ++) {
            if ($parent_id == 0 || $parent_id == null) {
                break;
            }

            $parent_user = User::find($parent_id);

            $exchange = new ExchangeList();

            $exchange->date = Carbon::now();
            $exchange->user_id = $parent_id;
            $exchange->dest_currency = 'TOKEN';
            $exchange->dest_amount = $amount * $bonus[$i] / 100;
            $exchange->note = 'BONUS';
            $exchange->ref_id = $user->id;
            $exchange->ref_lvl = ($i + 1);
            $exchange->usd_rate = $token_usd;
            $exchange->status = TransactionStatus::SUCCESS;
            $exchange->type = TransactionType::TOKEN_BONUS;
            $exchange->save();

            $parent_id = $parent_user->parent;
        }

        return response()->json([
            'success' => true
        ]);
    }

    public function getSystemCurrency(Request $request) {
        $btc_total = ExchangeList::where('src_currency', 'BTC')->sum('src_amount');
        $eth_total = ExchangeList::where('src_currency', 'ETH')->sum('src_amount');

        return response()->json([
            'success' => true,
            'btc_total' => $btc_total,
            'eth_total' => $eth_total,
        ]);
    }

    public function getUserHistory($user) {
        $send_lists = ExchangeList::where([['src_currency', 'TOKEN'], ['user_id', $user->id], ['date', '<=', Carbon::now()]])->get();
        $receive_lists = ExchangeList::where([['dest_currency', 'TOKEN'], ['user_id', $user->id], ['date', '<=', Carbon::now()]])->get();

        $btc_open_lists = BTCOrder::where([['user_id', $user->id], ['type', MarketOrderType::SELL]])->get();
        $eth_open_lists = ETHOrder::where([['user_id', $user->id], ['type', MarketOrderType::SELL]])->get();

        $settings = Setting::all()->pluck('value', 'name');

        if ($settings['enable_bt9_wallet'] == 1) {
            $rpcUrl = $this->getRPCUrl($settings);

            $jsonRpc = new JsonRPCClient($rpcUrl);

            $label = $user->email;

            try {
                $walletlist = $jsonRpc->listtransactions($label);
            } catch (\Exception $e) {
                $walletlist = array();
            }
        }

        $data = array();
        if (count($send_lists) > 0) {
            foreach ($send_lists as $list) {
                $list->category = 'send';
                array_push($data, $list);
            }
        }

        if (count($receive_lists) > 0) {
            foreach ($receive_lists as $list) {
                $list->category = 'receive';
                array_push($data, $list);
            }
        }

        if (count($btc_open_lists) > 0) {
            foreach ($btc_open_lists as $list) {
                array_push($data, array(
                    'date' => $list->created_at->toDateTimeString(),
                    'src_amount' => $list->amount,
                    'is_open' => 1,
                    'type' => TransactionType::EXCHANGE,
                    'exchange_type' => MarketOrderType::SELL,
                    'status' => TransactionStatus::OPEN,
                    'category' => 'send'
                ));
            }
        }

        if (count($eth_open_lists) > 0) {
            foreach ($eth_open_lists as $list) {
                array_push($data, array(
                    'date' => $list->created_at->toDateTimeString(),
                    'src_amount' => $list->amount,
                    'is_open' => 1,
                    'type' => TransactionType::EXCHANGE,
                    'exchange_type' => MarketOrderType::SELL,
                    'status' => TransactionStatus::OPEN,
                    'category' => 'send'
                ));
            }
        }

        if (count($walletlist) > 0) {
            foreach ($walletlist as $list) {
                if ($list['category'] == 'receive') {
                    array_push($data, array(
                        'category' => 'receive',
                        'date' => $list['time'] * 1000,
                        'dest_amount' => $list['amount'],
                        'type' => TransactionType::DEPOSIT,
                        'txid' => $list['txid']
                    ));
                }
            }
        }

        $earned_amount = 0;
        $stakings = ExchangeList::where([['user_id', $user->id], ['type', TransactionType::STAKING]])->get();
        foreach ($stakings as $staking) {
            $earned_amount = 0;

            $date = new Carbon($staking->date);
            $finish_date = new Carbon($staking->finish_date);
            $now = Carbon::now();

            $date->addDay(1);
            while ($date <= $finish_date && $date <= $now) {
                $earned_amount += $staking->src_amount * $staking->rate * 0.01;
                $date->addDay(1);
            }
        }

        if ($earned_amount > 0) {
            array_push($data, array(
                'category' => 'receive',
                'date' => Carbon::now()->toDateTimeString(),
                'dest_amount' => $earned_amount,
                'type' => TransactionType::STAKING_EARN
            ));
        }

        return $data;
    }

    public function getTokenHistory(Request $request) {
        $user = Auth::user();

        $data = $this->getUserHistory($user);

        return response()->json([
            'success' => true,
            'data' => $data,
        ]);
    }



    public function getAllList(Request $request) {
        $input = $request->input();

        if (isset($input['user_id'])) {
            $result = ExchangeList::where('dest_currency', 'TOKEN')->where('user_id', $input['user_id'])->leftJoin('users', 'exchange_lists.user_id', '=', 'users.id')->select('exchange_lists.*', 'users.email')->get();
        } else {
            $result = ExchangeList::where('dest_currency', 'TOKEN')->leftJoin('users', 'exchange_lists.user_id', '=', 'users.id')->select('exchange_lists.*', 'users.email')->get();
        }

        return response()->json([
            'success' => true,
            'data' => $result,
        ]);
    }

    public function addMyAltCoin(Request $request) {
        $input = $request->input();

        $exchange = new ExchangeList();

        $exchange->date = $input['date'];
        $exchange->user_id = $input['user_id'];
        $exchange->src_currency = $input['src_currency'];
        $exchange->dest_currency = 'TOKEN';
        $exchange->src_amount = $input['src_amount'];
        $exchange->dest_amount = $input['dest_amount'];
        $exchange->save();

        return response()->json([
            'success' => true,
        ]);
    }

    public function getBonusLists(Request $request) {
        $data = BonusList::orderBy('id')->get();

        return response()->json([
            'success' => true,
            'data' => $data
        ]);
    }

    public function getUsdBalance(Request $request) {
        $user = Auth::user();

        $balance = $this->usdbalance($user);

        return response()->json([
            'success' => true,
            'balance' => $balance
        ]);
    }

    public function getAddress(Request $request) {
        $user = Auth::user();

        $settings = Setting::all()->pluck('value', 'name');

        $rpcUrl = $this->getRPCUrl($settings);

        $coin_address = Bt9Address::where('user_id', $user->id)->first();

        if ($coin_address && $coin_address->address) {
            return response()->json([
                'success' => true,
                'address' => $coin_address->address
            ]);
        }

        if (!$coin_address) {
            $coin_address = new Bt9Address();
            $coin_address->user_id = $user->id;
        }

        $label = $user->email;

        $jsonRpc = new JsonRPCClient($rpcUrl);

        try {
            $result = $jsonRpc->getaccountaddress($label);
        } catch (\Exception $e) {
            return response($e->getMessage(), 403);
        }

        $coin_address->address = $result;
        $coin_address->save();

        return response()->json([
            'success' => true,
            'address' => $result
        ]);
    }

    public function getUnconfirmedSentTxList(Request $request) {
        $user = Auth::user();

        $settings = Setting::all()->pluck('value', 'name');
        $rpcUrl = $this->getRPCUrl($settings);

        $jsonRpc = new JsonRPCClient($rpcUrl);

        $label = $user->email;

        try {
            $result = $jsonRpc->listtransactions($label);
        } catch (\Exception $e) {
            return response($e->getMessage(), 403);
        }

        $sent = array();
        if (count($result) > 0) {
            foreach ($result as $tran) {
                if ($tran['category'] == 'send' && $tran['confirmations'] == 0) {
                    array_push($sent, $tran);
                }
            }
        }

        return response()->json([
            'success' => true,
            'data' => $sent
        ]);
    }


    public function getUnconfirmedReceivedTxList(Request $request) {
        $user = Auth::user();

        $settings = Setting::all()->pluck('value', 'name');
        $rpcUrl = $this->getRPCUrl($settings);

        $jsonRpc = new JsonRPCClient($rpcUrl);

        $label = $user->email;

        try {
            $result = $jsonRpc->listtransactions($label);
        } catch (\Exception $e) {
            return response($e->getMessage(), 403);
        }

        $sent = array();
        if (count($result) > 0) {
            foreach ($result as $tran) {
                if ($tran['category'] == 'receive' && $tran['confirmations'] == 0) {
                    array_push($sent, $tran);
                }
            }
        }

        return response()->json([
            'success' => true,
            'data' => $sent
        ]);
    }

    public function withdraw(Request $request) {
        $input = $request->input();

        $user = Auth::user();

        if (!$user) {
            return response('Not Authorized', 403);
        }

        $settings = Setting::all()->pluck('value', 'name');
        $rpcUrl = $this->getRPCUrl($settings);

        $jsonRpc = new JsonRPCClient($rpcUrl);

        $label = $user->email;

        try {
            $balance = $jsonRpc->getbalance($label);
        } catch (\Exception $e) {
            return response($e->getMessage(), 403);
        }

        if ($balance < $input['amount']) {
            return response("Your balance is not enough to withdraw", 403);
        }

        $jsonRpc->settxfee('0.01');

        try {
            $hex = $jsonRpc->createrawtransaction(array(), array($input['address'] => round($input['amount'], 8)));
            $fund = $jsonRpc->fundrawtransaction($hex);
        } catch (\Exception $e) {
            return response($e->getMessage(), 403);
        }

        if ($fund['fee'] + $input['amount'] > $balance) {
            return response("Withdraw Fee: ".$fund['fee'].". Your balance is not enough. ", 403);
        }

        try {
            $result = $jsonRpc->sendfrom($label, $input['address'], $input['amount']);
        } catch (\Exception $e) {
            return response($e->getMessage(), 403);
        }

        return response()->json([
            'success' => true,
            'data' => $result
        ]);
    }

    public function getTotalBalance(Request $request) {
        $settings = Setting::all()->pluck('value', 'name');
        $rpcUrl = $this->getRPCUrl($settings);

        $jsonRpc = new JsonRPCClient($rpcUrl);

        try {
            $balance = $jsonRpc->getbalance();
        } catch (\Exception $e) {
            $balance = 0;
        }

        return response()->json([
            'success' => true,
            'balance' => $balance
        ]);
    }

    public function estimateFee(Request $request) {
        $input = $request->input();

        $priority = $input['priority'];

        $settings = Setting::all()->pluck('value', 'name');

        if ($priority == 2) {
            $block = 2;
        } else if ($priority == 1) {
            $block = 5;
        } else {
            $block = 11;
        }

        $trans = ExchangeList::where([['type', TransactionType::WITHDRAW], ['status', TransactionStatus::PENDING], ['src_currency', 'TOKEN']])->get();

        $outputs = array();
        foreach ($trans as $tran) {
            $tran->estimated = 1;
            $tran->save();
            $outputs[$tran->address] = round($tran->src_amount - $tran->fee, 8);
        }
        $rpcUrl = $this->getRPCUrl($settings);

        $jsonRpc = new JsonRPCClient($rpcUrl);

        try {
            $totalBalance = $jsonRpc->getbalance();
            // $fee = $jsonRpc->estimatefee($block);
        } catch (\Exception $e) {
            //return response('Error! Please try again.', 403);
            return response($e->getMessage(), 403);
        }

        // $jsonRpc->settxfee($fee);

        try {
            $hex = $jsonRpc->createrawtransaction(array(), $outputs);
//            $fund = $jsonRpc->decoderawtransaction($hex);
//            var_dump($fund);
        } catch (\Exception $e) {
            //return response('Error! Please try again.', 403);
            return response($e->getMessage(), 403);
        }

        \Log::info($hex);
        return response()->json([
            'success' => true,
            //'fee' => $fund['fee'],
            'hex' => $hex
        ]);
    }

    public function withdrawPending(Request $request) {
        $input = $request->input();

        // $hex = $input['hex'];
        $fee = isset($input['fee']) ? $input['fee'] : 0;

        $settings = Setting::all()->pluck('value', 'name');
        $rpcUrl = $this->getRPCUrl($settings);

        $jsonRpc = new JsonRPCClient($rpcUrl);

        $trans = ExchangeList::where([['type', TransactionType::WITHDRAW], ['status', TransactionStatus::PENDING], ['src_currency', 'TOKEN']])->get();

        if (count($trans) == 0) {
            return response()->json([
                'success' => false,
                'error' => 'No pendings'
            ]);
        }

        $error = 0;
        $outputs = array();
        foreach ($trans as $tran) {
            $amount = round($tran->src_amount - $tran->fee, 8);

            // $outputs[$tran->address] = round($tran->src_amount - $tran->fee, 8);
            try {
                $result = $jsonRpc->sendtoaddress($tran->address, $amount);
            } catch (\Exception $e) {
                //return response('Error! Please try again.', 403);
                $error ++;
                continue;
            }

            $tran->estimated = 0;
            $tran->status = TransactionStatus::SUCCESS;
            $tran->txid = $result;
            $tran->save();

            \Log::info($result);

            $withdraw = new CoinWithdrawHistory();
            $withdraw->coin = 'TOKEN';
            $withdraw->datetime = Carbon::now();
            $withdraw->txid = $result;
            $withdraw->fee = $fee;
            $withdraw->save();
        }

        if ($error == 0) {
            return response()->json([
                'success' => true,
            ]);
        } else {
            return response()->json([
                'success' => false,
                'error' => $error.' transactions can not withdraw.'
            ]);
        }
    }



    public function withdrawSchedule() {
        $fee = 0;

        $settings = Setting::all()->pluck('value', 'name');
        $rpcUrl = $this->getRPCUrl($settings);

        $jsonRpc = new JsonRPCClient($rpcUrl);

        $trans = ExchangeList::where([['type', TransactionType::WITHDRAW], ['status', TransactionStatus::PENDING], ['src_currency', 'TOKEN']])->get();

        if (count($trans) == 0) {
            return response()->json([
                'success' => false,
                'error' => 'No pendings'
            ]);
        }

        $error = 0;
        $outputs = array();
        foreach ($trans as $tran) {
            $amount = round($tran->src_amount - $tran->fee, 8);

            // $outputs[$tran->address] = round($tran->src_amount - $tran->fee, 8);
            try {
                $result = $jsonRpc->sendtoaddress($tran->address, $amount);
            } catch (\Exception $e) {
                //return response('Error! Please try again.', 403);
                $error ++;
                continue;
            }

            $tran->estimated = 0;
            $tran->status = TransactionStatus::SUCCESS;
            $tran->txid = $result;
            $tran->save();

            \Log::info($result);

            $withdraw = new CoinWithdrawHistory();
            $withdraw->coin = 'TOKEN';
            $withdraw->datetime = Carbon::now();
            $withdraw->txid = $result;
            $withdraw->fee = $fee;
            $withdraw->save();
        }
    }

    public function getAllTx(Request $request) {
        $settings = Setting::all()->pluck('value', 'name');

        $rpcUrl = $this->getRPCUrl($settings);

        $jsonRpc = new JsonRPCClient($rpcUrl);

        try {
            $data = $jsonRpc->listtransactions();
            // $fee = $jsonRpc->estimatefee($block);
        } catch (\Exception $e) {
            $data = array();
        }

        return response()->json([
            'success' => true,
            'data' => $data
        ]);
    }
}
