webpackJsonp(["ticket.module"],{

/***/ "../../../../../src/app/admin/ticket/detail/detail.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\n    <div class=\"container-fluid\">\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n                <div class=\"card\">\n                    <div class=\"card-content\">\n                    <pre>\n                        <h3 class=\"card-title\">{{ticket.title}}</h3>\n                    </pre>\n                        <p>\n                            Created: {{ticket.created_at | date: 'dd MMMM yyyy HH:mm'}}\n                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\n                            Updated: {{ticket.updated_at | date: 'dd MMMM yyyy HH:mm'}}\n                        </p>\n                    </div>\n                </div>\n            </div>\n        </div>\n\n        <div class=\"row mb-3\" *ngIf=\"ticket.status != 2\">\n            <div class=\"col-md-12\">\n                <button type=\"button\" class=\"btn btn-success\" (click)=\"onReply()\">\n                    Reply\n                </button>\n                <button type=\"button\" class=\"btn btn-danger\" (click)=\"closeTicket()\">\n                    Close Ticket\n                </button>\n            </div>\n        </div>\n\n        <input type=\"file\" name=\"file\" class=\"form-control\" id=\"attachFile\" (change)=\"fileChangeListener($event)\"\n               style=\"display: none;\">\n        <div class=\"row\" *ngIf=\"isReply\">\n            <div class=\"col-md-12\">\n                <div class=\"card\">\n                    <div class=\"card-content\">\n                        <form>\n                            <div class=\"row\">\n                                <div class=\"form-group col-md-12\">\n                                    <label>Message</label>\n                                    <textarea class=\"form-control\" rows=\"10\" name=\"message\"\n                                              [(ngModel)]=\"ticket.message\"></textarea>\n                                </div>\n                            </div>\n                            <div class=\"row\">\n                                <div class=\"form-group col-md-12\">\n                                    <div class=\"pull-left\">\n                                        {{ticket.attach_name}}\n                                    </div>\n                                    <button type=\"button\" class=\"btn btn-primary pull-right\" (click)=\"onAttachFile()\">\n                                        Add Attach File\n                                    </button>\n                                </div>\n                            </div>\n                            <div class=\"row\">\n                                <div class=\"col-md-12\">\n                                    <button type=\"button\" class=\"btn btn-info\" (click)=\"sendTicket()\">\n                                        SEND\n                                    </button>\n                                </div>\n                            </div>\n                        </form>\n                    </div>\n                </div>\n            </div>\n        </div>\n\n        <div class=\"row\" *ngFor=\"let detail of details;\">\n            <div class=\"col-md-12\">\n                <div class=\"card\">\n                    <div class=\"card-content\">\n                        <div class=\"row\">\n                            <div class=\"col-md-3 border-right-1\">\n                                <p>{{detail.username}}</p>\n                                <!--<p>{{(detail.user_type == 0 ? 'LABEL.USER' : 'LABEL.ADMIN') | translate}}</p>-->\n                            </div>\n                            <div class=\"col-md-9\">\n                                <p class=\"border-bottom-1\">\n                                    Posted on {{detail.created_at | date: 'dd MMMM yyyy HH:mm'}}\n                                </p>\n                                <pre>{{detail.message}}</pre>\n                                <p *ngIf=\"detail.attach_name != ''\">\n                                    <a target=\"_blank\" href=\"{{detail.attach_url}}\">{{detail.attach_name}}</a>\n                                </p>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/admin/ticket/detail/detail.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "pre {\n  box-shadow: none; }\n\n.border-bottom-1 {\n  border-bottom: 1px solid; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/admin/ticket/detail/detail.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminTicketDetailComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_settings_settings_service__ = __webpack_require__("../../../../../src/app/services/settings/settings.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_api_service__ = __webpack_require__("../../../../../src/app/services/api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_balance_service__ = __webpack_require__("../../../../../src/app/services/balance.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_notifications_service__ = __webpack_require__("../../../../../src/app/services/notifications.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var AdminTicketDetailComponent = (function () {
    function AdminTicketDetailComponent(api, settings, balance, router, notify, activatedRoute) {
        this.api = api;
        this.settings = settings;
        this.balance = balance;
        this.router = router;
        this.notify = notify;
        this.activatedRoute = activatedRoute;
        this.ticket = {};
        this.details = [];
        this.isReply = false;
        this.ticket_id = null;
    }
    AdminTicketDetailComponent.prototype.ngOnInit = function () {
        this.ticket_id = this.activatedRoute.snapshot.params['id'];
        this.loadTicket();
    };
    AdminTicketDetailComponent.prototype.loadTicket = function () {
        var _this = this;
        this.api.getTicketDetail({
            ticket_id: this.ticket_id
        }).subscribe(function (res) {
            if (res.success) {
                _this.ticket = res.ticket;
                _this.details = res.data;
            }
            else {
                _this.router.navigate(['/admin/ticket']);
            }
        }, function (err) {
            _this.router.navigate(['/admin/ticket']);
        });
    };
    AdminTicketDetailComponent.prototype.ngAfterViewInit = function () {
    };
    AdminTicketDetailComponent.prototype.onReply = function () {
        this.ticket.message = '';
        this.ticket.attach_url = '';
        this.ticket.attach_name = '';
        this.isReply = true;
    };
    AdminTicketDetailComponent.prototype.sendTicket = function () {
        var _this = this;
        this.settings.loading = true;
        this.api.replyTicket(this.ticket).subscribe(function (res) {
            _this.settings.loading = false;
            if (res.success) {
                _this.notify.showNotification('Your Reply Ticket was posted successfully', 'top', 'center', 'success');
                _this.isReply = false;
                _this.loadTicket();
            }
            else {
                _this.notify.showNotification(res.error, 'top', 'center', 'warning');
            }
        }, function (err) {
            _this.settings.loading = false;
            _this.notify.showNotification('Please try again.', 'top', 'center', 'danger');
        });
    };
    AdminTicketDetailComponent.prototype.onAttachFile = function () {
        $('#attachFile').click();
    };
    AdminTicketDetailComponent.prototype.fileChangeListener = function ($event) {
        var _this = this;
        var image = new Image();
        var file = $event.target.files[0];
        this.settings.loading = true;
        this.api.uploadFile(file).subscribe(function (res) {
            _this.settings.loading = false;
            if (res.success) {
                _this.ticket.attach_url = res.url;
                _this.ticket.attach_name = file.name;
            }
        }, function (err) {
            _this.settings.loading = false;
        });
    };
    AdminTicketDetailComponent.prototype.closeTicket = function () {
        var _this = this;
        this.settings.loading = true;
        this.api.closeTicket({
            id: this.ticket_id
        }).subscribe(function (res) {
            _this.settings.loading = false;
            if (res.success) {
                _this.notify.showNotification('Ticket was closed successfully', 'top', 'center', 'success');
                _this.loadTicket();
            }
            else {
                _this.notify.showNotification(res.error, 'top', 'center', 'warning');
            }
        }, function (err) {
            _this.settings.loading = false;
            _this.notify.showNotification('Please try again.', 'top', 'center', 'danger');
        });
    };
    AdminTicketDetailComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            template: __webpack_require__("../../../../../src/app/admin/ticket/detail/detail.component.html"),
            styles: [__webpack_require__("../../../../../src/app/admin/ticket/detail/detail.component.scss")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__services_api_service__["a" /* Api */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_api_service__["a" /* Api */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__services_settings_settings_service__["a" /* SettingsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_settings_settings_service__["a" /* SettingsService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__services_balance_service__["a" /* BalanceService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services_balance_service__["a" /* BalanceService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_4__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__angular_router__["c" /* Router */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_5__services_notifications_service__["a" /* Notifications */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__services_notifications_service__["a" /* Notifications */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_4__angular_router__["a" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__angular_router__["a" /* ActivatedRoute */]) === "function" && _f || Object])
    ], AdminTicketDetailComponent);
    return AdminTicketDetailComponent;
    var _a, _b, _c, _d, _e, _f;
}());

//# sourceMappingURL=detail.component.js.map

/***/ }),

/***/ "../../../../../src/app/admin/ticket/ticket.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\n    <div class=\"container-fluid\">\n        <div class=\"row\">\n            <div class=\"col-12\">\n                <div class=\"card\">\n                    <div class=\"card-content\">\n                        <ul class=\"nav nav-pills nav-menu-security nav-pills-warning\">\n                            <li class=\"active\">\n                                <a data-toggle=\"tab\" href=\"#open\">Open Tickets</a>\n                            </li>\n                            <li>\n                                <a data-toggle=\"tab\" href=\"#close\">Closed Tickets</a>\n                            </li>\n                        </ul>\n\n                        <div class=\"tab-content\">\n                            <div class=\"tab-pane active\" id=\"open\" role=\"tabpanel\">\n                                <div class=\"table-responsive\">\n                                    <table class=\"table\" id=\"opentable\">\n                                        <thead>\n                                        <tr>\n                                            <th style=\"width: 150px;\">Ticket ID</th>\n                                            <th>TITLE</th>\n                                            <th>LAST USER</th>\n                                            <th style=\"width: 200px;\">LAST UPDATE</th>\n                                        </tr>\n                                        </thead>\n                                        <tbody>\n                                        <tr *ngFor=\"let ticket of openTickets; let i=index;\"\n                                            [ngClass]=\"{'table-active': ticket.last_user == 'Admin'}\"\n                                            (click)=\"showTicket(ticket)\">\n                                            <td  style=\"width: 150px;\">{{ticket.id}}</td>\n                                            <td>{{ticket.title}}</td>\n                                            <td>{{ticket.last_user}}</td>\n                                            <td style=\"width: 200px;\">{{ticket.updated_at | date:'yyyy-MM-dd HH:mm:ss'}}\n                                            </td>\n                                        </tr>\n                                        <tr *ngIf=\"openTickets?.length == 0\">\n                                            <td class=\"text-center\" colspan=\"5\">\n                                                No Open Ticket\n                                            </td>\n                                        </tr>\n                                        </tbody>\n                                    </table>\n                                </div>\n                            </div>\n                            <div class=\"tab-pane\" id=\"close\" role=\"tabpanel\">\n                                <div class=\"table-responsive\">\n                                    <table class=\"table\" id=\"closetable\">\n                                        <thead>\n                                        <tr>\n                                            <th style=\"width: 150px;\">Ticket ID</th>\n                                            <th>TITLE</th>\n                                            <th>LAST USER</th>\n                                            <th style=\"width: 200px;\">LAST UPDATE</th>\n                                        </tr>\n                                        </thead>\n                                        <tbody>\n                                        <tr *ngFor=\"let ticket of closeTickets\" (click)=\"showTicket(ticket)\">\n                                            <td  style=\"width: 150px;\">{{ticket.id}}</td>\n                                            <td>{{ticket.title}}</td>\n                                            <td>{{ticket.last_user}}</td>\n                                            <td style=\"width: 200px;\">{{ticket.updated_at | date:'yyyy-MM-dd HH:mm:ss'}}\n                                            </td>\n                                        </tr>\n                                        <tr *ngIf=\"closeTickets?.length == 0\">\n                                            <td class=\"text-center\" colspan=\"4\">\n                                                No Closed Ticket\n                                            </td>\n                                        </tr>\n                                        </tbody>\n                                    </table>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/admin/ticket/ticket.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".table tr {\n  cursor: pointer;\n  height: 40px; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/admin/ticket/ticket.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminTicketComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_settings_settings_service__ = __webpack_require__("../../../../../src/app/services/settings/settings.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_api_service__ = __webpack_require__("../../../../../src/app/services/api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_balance_service__ = __webpack_require__("../../../../../src/app/services/balance.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AdminTicketComponent = (function () {
    function AdminTicketComponent(api, settings, balance, router) {
        this.api = api;
        this.settings = settings;
        this.balance = balance;
        this.router = router;
        this.openTickets = [];
        this.closeTickets = [];
        this.closedatatable = null;
        this.opendatatable = null;
    }
    AdminTicketComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.api.getAllTicket({
            open: true
        }).subscribe(function (res) {
            if (res.success) {
                _this.openTickets = res.data;
                var _parent = _this;
                if (res.data.length > 0) {
                    var timer = setInterval(function () {
                        if (res.data.length == $('#opentable tbody tr').length) {
                            _parent.opendatatable = $('#opentable').DataTable();
                            clearInterval(timer);
                        }
                    }, 100);
                }
                _this.api.getAllTicket({
                    close: true
                }).subscribe(function (res) {
                    if (res.success) {
                        _this.closeTickets = res.data;
                        if (res.data.length > 0) {
                            var timer1 = setInterval(function () {
                                if (res.data.length == $('#closetable tbody tr').length) {
                                    _parent.closedatatable = $('#closetable').DataTable();
                                    clearInterval(timer1);
                                }
                            }, 100);
                        }
                    }
                });
            }
        });
    };
    AdminTicketComponent.prototype.ngAfterViewInit = function () { };
    AdminTicketComponent.prototype.showTicket = function (ticket) {
        this.router.navigate(['admin/support/detail/' + ticket.id]);
    };
    AdminTicketComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            template: __webpack_require__("../../../../../src/app/admin/ticket/ticket.component.html"),
            styles: [__webpack_require__("../../../../../src/app/admin/ticket/ticket.component.scss")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__services_api_service__["a" /* Api */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_api_service__["a" /* Api */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__services_settings_settings_service__["a" /* SettingsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_settings_settings_service__["a" /* SettingsService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__services_balance_service__["a" /* BalanceService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services_balance_service__["a" /* BalanceService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_4__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__angular_router__["c" /* Router */]) === "function" && _d || Object])
    ], AdminTicketComponent);
    return AdminTicketComponent;
    var _a, _b, _c, _d;
}());

//# sourceMappingURL=ticket.component.js.map

/***/ }),

/***/ "../../../../../src/app/admin/ticket/ticket.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminTicketModule", function() { return AdminTicketModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared_shared_module__ = __webpack_require__("../../../../../src/app/shared/shared.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ticket_component__ = __webpack_require__("../../../../../src/app/admin/ticket/ticket.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ticket_routing__ = __webpack_require__("../../../../../src/app/admin/ticket/ticket.routing.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__detail_detail_component__ = __webpack_require__("../../../../../src/app/admin/ticket/detail/detail.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var AdminTicketModule = (function () {
    function AdminTicketModule() {
    }
    AdminTicketModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_common__["b" /* CommonModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_router__["d" /* RouterModule */].forChild(__WEBPACK_IMPORTED_MODULE_5__ticket_routing__["a" /* AdminTicketRoutes */]),
                __WEBPACK_IMPORTED_MODULE_3__shared_shared_module__["a" /* SharedModule */]
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_4__ticket_component__["a" /* AdminTicketComponent */],
                __WEBPACK_IMPORTED_MODULE_6__detail_detail_component__["a" /* AdminTicketDetailComponent */]
            ]
        })
    ], AdminTicketModule);
    return AdminTicketModule;
}());

//# sourceMappingURL=ticket.module.js.map

/***/ }),

/***/ "../../../../../src/app/admin/ticket/ticket.routing.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminTicketRoutes; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ticket_component__ = __webpack_require__("../../../../../src/app/admin/ticket/ticket.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__detail_detail_component__ = __webpack_require__("../../../../../src/app/admin/ticket/detail/detail.component.ts");


var AdminTicketRoutes = [
    {
        path: '',
        component: __WEBPACK_IMPORTED_MODULE_0__ticket_component__["a" /* AdminTicketComponent */]
    },
    {
        path: 'detail/:id',
        component: __WEBPACK_IMPORTED_MODULE_1__detail_detail_component__["a" /* AdminTicketDetailComponent */]
    }
];
//# sourceMappingURL=ticket.routing.js.map

/***/ })

});
//# sourceMappingURL=ticket.module.chunk.js.map