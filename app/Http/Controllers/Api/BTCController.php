<?php

namespace App\Http\Controllers\Api;

use App\Entities\BtcAddress;
use App\Entities\BTCOrder;
use App\Entities\BtcTestAddress;
use App\Entities\CoinWithdrawHistory;
use App\Entities\EthAddress;
use App\Entities\ETHOrder;
use App\Entities\ExchangeList;
use App\Entities\MarketOrderType;
use App\Entities\Order;
use App\Entities\Setting;
use App\Entities\TransactionStatus;
use App\Entities\TransactionType;
use App\Libs\BitcoinAddressValidator;
use App\Libs\JsonRPCClient;
use App\User;
use BitWasp\Bitcoin\Transaction\Transaction;
use BlockCypher\Api\TX;
use BlockCypher\Api\Wallet;
use BlockCypher\Auth\SimpleTokenCredential;
use BlockCypher\Client\AddressClient;
use BlockCypher\Client\BlockchainClient;
use BlockCypher\Client\WalletClient;
use BlockCypher\Rest\ApiContext;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class BTCController extends Controller
{
    //
    public function test() {
        $settings = Setting::all()->pluck('value', 'name');

        $rpcUrl = $this->getRPCUrl($settings);
        $jsonRpc = new JsonRPCClient($rpcUrl);

        try {
            $result = $jsonRpc->validateaddress('n39LBeRPM4xjQwJAYhDovr7QGzRRvjwYau');
        } catch (\Exception $e) {
            return response($e->getMessage(), 403);
        }

        var_dump($result);
    }

    public function getRPCUrl($settings) {
        $rpcUrl = 'http://'.$settings['btc_rpcuser'].':'.$settings['btc_rpcpassword'].'@'.$settings['btc_rpcserver'].':'.$settings['btc_rpcport'].'/';

        return $rpcUrl;
    }

    public function getCoinAddress($user) {
        return BtcAddress::where('user_id', $user->id)->first();
    }

    //
    public function getAddress(Request $request) {
        $user = Auth::user();

        if (!$user) {
            return response('Not Authorized', 403);
        }

        $settings = Setting::all()->pluck('value', 'name');

        $rpcUrl = $this->getRPCUrl($settings);

        $coin_address = $this->getCoinAddress($user);
        if ($coin_address && $coin_address->address) {
            return response()->json([
                'success' => true,
                'address' => $coin_address->address
            ]);
        }

        if (!$coin_address) {
            $coin_address = new BtcAddress();
            $coin_address->user_id = $user->id;
        }

        $label = $user->email;

        $jsonRpc = new JsonRPCClient($rpcUrl);

        try {
            $result = $jsonRpc->getaccountaddress($label);
        } catch (\Exception $e) {
            return response($e->getMessage(), 403);
        }

        $coin_address->address = $result;
        $coin_address->save();

        return response()->json([
            'success' => true,
            'address' => $result
        ]);
    }

    public function balance($user) {
        $settings = Setting::all()->pluck('value', 'name');

        $sent = ExchangeList::where([['src_currency', 'BTC'], ['user_id', $user->id]])->sum('src_amount');
        $receive = ExchangeList::where([['dest_currency', 'BTC'], ['user_id', $user->id]])->sum('dest_amount');

        $sell = BTCOrder::where([['user_id', $user->id], ['type', MarketOrderType::BUY]])->sum(\DB::raw('rate * amount'));

        $balance = (-1) * $sent + $receive - $sell;


        if ($settings['enable_btc_wallet'] == 1) {
            $rpcUrl = $this->getRPCUrl($settings);

            $jsonRpc = new JsonRPCClient($rpcUrl);

            $label = $user->email;

            try {
                $wallet_balance = $jsonRpc->getreceivedbyaccount($label);
            } catch (\Exception $e) {
                $wallet_balance = 0;
            }
            $balance += $wallet_balance;
        }

        return $balance;
    }

    public function getBalance(Request $request) {
        $user = Auth::user();

        if (!$user) {
            return response('Not Authorized', 403);
        }

        return response()->json([
            'success' => true,
            'available_balance' => $this->balance($user)
        ]);
    }

    public function getUserHistory($user) {
        $settings = Setting::all()->pluck('value', 'name');

        if ($settings['enable_btc_wallet'] == 1) {
            $rpcUrl = $this->getRPCUrl($settings);

            $jsonRpc = new JsonRPCClient($rpcUrl);

            $label = $user->email;

            try {
                $walletlist = $jsonRpc->listtransactions($label);
            } catch (\Exception $e) {
                $walletlist = array();
            }
        }
        $data = array();

        $send_lists = ExchangeList::where([['src_currency', 'BTC'], ['user_id', $user->id], ['date', '<=', Carbon::now()]])->get();
        $receive_lists = ExchangeList::where([['dest_currency', 'BTC'], ['user_id', $user->id], ['date', '<=', Carbon::now()]])->get();

        $open_lists = BTCOrder::where([['user_id', $user->id], ['type', MarketOrderType::BUY]])->get();

        if (count($send_lists) > 0) {
            foreach ($send_lists as $list) {
                $list->category = 'send';
                array_push($data, $list);
            }
        }

        if (count($receive_lists) > 0) {
            foreach ($receive_lists as $list) {
                $list->category = 'receive';
                array_push($data, $list);
            }
        }

        if (count($open_lists) > 0) {
            foreach ($open_lists as $list) {
                array_push($data, array(
                    'date' => $list->created_at->toDateTimeString(),
                    'src_amount' => $list->amount * $list->rate,
                    'is_open' => 1,
                    'type' => TransactionType::EXCHANGE,
                    'exchange_type' => MarketOrderType::BUY,
                    'status' => TransactionStatus::OPEN,
                    'category' => 'send'
                ));
            }
        }

        if (count($walletlist) > 0) {
            foreach ($walletlist as $list) {
                if ($list['category'] == 'receive') {
                    array_push($data, array(
                        'category' => 'receive',
                        'date' => $list['time'] * 1000,
                        'dest_amount' => $list['amount'],
                        'type' => TransactionType::DEPOSIT,
                        'txid' => $list['txid']
                    ));
                }
            }
        }

        return $data;
    }

    public function getTxList(Request $request) {
        $user = Auth::user();

        if (!$user) {
            return response('Not Authorized', 403);
        }

        $data = $this->getUserHistory($user);

        return response()->json([
            'success' => true,
            'data' => $data
        ]);
    }

    public function getUnconfirmedReceivedTxList(Request $request) {
        $user = Auth::user();

        $settings = Setting::all()->pluck('value', 'name');
        $rpcUrl = $this->getRPCUrl($settings);

        $jsonRpc = new JsonRPCClient($rpcUrl);

        $label = $user->email;

        try {
            $result = $jsonRpc->listtransactions($label);
        } catch (\Exception $e) {
            return response($e->getMessage(), 403);
        }

        $sent = array();
        if (count($result) > 0) {
            foreach ($result as $tran) {
                if ($tran['category'] == 'receive' && $tran['confirmations'] == 0) {
                    array_push($sent, $tran);
                }
            }
        }

        return response()->json([
            'success' => true,
            'data' => $sent
        ]);
    }


    public function getTotalBalance(Request $request) {
        $settings = Setting::all()->pluck('value', 'name');
        $rpcUrl = $this->getRPCUrl($settings);

        $jsonRpc = new JsonRPCClient($rpcUrl);

        try {
            $balance = $jsonRpc->getbalance();
        } catch (\Exception $e) {
            $balance = 0;
        }

        return response()->json([
            'success' => true,
            'balance' => $balance
        ]);
    }

    public function estimateFee(Request $request) {
        $input = $request->input();

        $priority = $input['priority'];

        $settings = Setting::all()->pluck('value', 'name');

        if ($priority == 2) {
            $block = 2;
        } else if ($priority == 1) {
            $block = 5;
        } else {
            $block = 11;
        }

        $trans = ExchangeList::where([['type', TransactionType::WITHDRAW], ['status', TransactionStatus::PENDING], ['src_currency', 'BTC']])->get();

        $outputs = array();
        foreach ($trans as $tran) {
            $tran->estimated = 1;
            $tran->save();
            $outputs[$tran->address] = round($tran->src_amount - $tran->fee, 8);
        }

        $rpcUrl = $this->getRPCUrl($settings);

        $jsonRpc = new JsonRPCClient($rpcUrl);

        try {
            // $totalBalance = $jsonRpc->getbalance();
            $fee = $jsonRpc->estimatefee($block);
        } catch (\Exception $e) {
            //return response('Error! Please try again.', 403);
            return response($e->getMessage(), 403);
        }

        $jsonRpc->settxfee($fee);

        try {
            $hex = $jsonRpc->createrawtransaction(array(), $outputs);
            $fund = $jsonRpc->fundrawtransaction($hex);
        } catch (\Exception $e) {
            //return response('Error! Please try again.', 403);
            return response($e->getMessage(), 403);
        }

        return response()->json([
            'success' => true,
            'fee' => $fund['fee'],
            'hex' => $fund['hex']
        ]);
    }

    public function withdrawPending(Request $request) {
        $input = $request->input();

        $hex = $input['hex'];
        $fee = $input['fee'];

        $settings = Setting::all()->pluck('value', 'name');
        $rpcUrl = $this->getRPCUrl($settings);

        $jsonRpc = new JsonRPCClient($rpcUrl);
        try {
            $result = $jsonRpc->signrawtransaction($hex);
            $result = $jsonRpc->sendrawtransaction($result['hex']);
        } catch (\Exception $e) {
            //return response('Error! Please try again.', 403);
            return response($e->getMessage(), 403);
        }

        $trans = ExchangeList::where([['estimated', 1], ['src_currency', 'BTC'], ['type', TransactionType::WITHDRAW]])->get();
        foreach ($trans as $tran) {
            $tran->estimated = 0;
            $tran->status = TransactionStatus::SUCCESS;
            $tran->txid = $result;
            $tran->save();
        }

        $withdraw = new CoinWithdrawHistory();
        $withdraw->coin = 'BTC';
        $withdraw->datetime = Carbon::now();
        $withdraw->txid = $result;
        $withdraw->fee = $fee;
        $withdraw->save();

        return response()->json([
            'success' => true,
        ]);
    }

    public function withdrawSchedule() {
        $priority = 2;

        $settings = Setting::all()->pluck('value', 'name');

        if ($priority == 2) {
            $block = 2;
        } else if ($priority == 1) {
            $block = 5;
        } else {
            $block = 11;
        }

        $trans = ExchangeList::where([['type', TransactionType::WITHDRAW], ['status', TransactionStatus::PENDING], ['src_currency', 'BTC']])->get();

        $outputs = array();
        foreach ($trans as $tran) {
            $tran->estimated = 1;
            $tran->save();
            $outputs[$tran->address] = round($tran->src_amount - $tran->fee, 8);
        }

        $rpcUrl = $this->getRPCUrl($settings);

        $jsonRpc = new JsonRPCClient($rpcUrl);

        try {
            // $totalBalance = $jsonRpc->getbalance();
            $fee = $jsonRpc->estimatefee($block);
        } catch (\Exception $e) {
            //return response('Error! Please try again.', 403);
            return response($e->getMessage(), 403);
        }

        $jsonRpc->settxfee($fee);

        try {
            $hex = $jsonRpc->createrawtransaction(array(), $outputs);
            $fund = $jsonRpc->fundrawtransaction($hex);
        } catch (\Exception $e) {
            //return response('Error! Please try again.', 403);
            return response($e->getMessage(), 403);
        }

        $hex = $fund['hex'];
        $fee = $fund['fee'];

        $settings = Setting::all()->pluck('value', 'name');
        $rpcUrl = $this->getRPCUrl($settings);

        $jsonRpc = new JsonRPCClient($rpcUrl);
        try {
            $result = $jsonRpc->signrawtransaction($hex);
            $result = $jsonRpc->sendrawtransaction($result['hex']);
        } catch (\Exception $e) {
            //return response('Error! Please try again.', 403);
            return response($e->getMessage(), 403);
        }

        $trans = ExchangeList::where([['estimated', 1], ['src_currency', 'BTC'], ['type', TransactionType::WITHDRAW]])->get();
        foreach ($trans as $tran) {
            $tran->estimated = 0;
            $tran->status = TransactionStatus::SUCCESS;
            $tran->txid = $result;
            $tran->save();
        }

        $withdraw = new CoinWithdrawHistory();
        $withdraw->coin = 'BTC';
        $withdraw->datetime = Carbon::now();
        $withdraw->txid = $result;
        $withdraw->fee = $fee;
        $withdraw->save();
    }

    public function getAllTx(Request $request) {
        $settings = Setting::all()->pluck('value', 'name');

        $rpcUrl = $this->getRPCUrl($settings);

        $jsonRpc = new JsonRPCClient($rpcUrl);

        try {
            $data = $jsonRpc->listtransactions(null, 100);
            // $fee = $jsonRpc->estimatefee($block);
        } catch (\Exception $e) {
            $data = array();
        }

        return response()->json([
            'success' => true,
            'data' => $data
        ]);
    }

    public function getStatistics(Request $request) {
        $now = Carbon::now();

        $last = BTCOrder::where('type', MarketOrderType::BUY)->orderBy('created_at', 'desc')->first();
        $lastBid = 0;
        if ($last) {
            $lastBid = $last->rate;
        }

        $last = BTCOrder::where('type', MarketOrderType::SELL)->orderBy('created_at', 'desc')->first();
        $lastAsk = 0;
        if ($last) {
            $lastAsk = $last->rate;
        }

        $last = ExchangeList::where([['type', TransactionType::EXCHANGE], ['src_currency', 'BTC']])->orderBy('date', 'desc')->first();
        $lastPrice = 0;
        if ($last) {
            $lastPrice = $last->rate;
        }

        $before = Carbon::now()->addDay(-1);
        $high = BTCOrder::where('created_at', '>=', $before)->max('rate');
        $low = BTCOrder::where('created_at', '>=', $before)->max('rate');

        $volume = ExchangeList::where([['type', TransactionType::EXCHANGE], ['src_currency', 'BTC'], ['date', '>=', $before]])->sum('src_amount');

        return response()->json([
            'success' => true,
            'data' => array(
                'lastBid' => $lastBid,
                'lastAsk' => $lastAsk,
                'lastPrice' => $lastPrice,
                'high' => $high ? $high: 0,
                'low' => $low ? $low : 0,
                'volume' => $volume ? $volume : 0
            )
        ]);
    }

    public function getHistory(Request $request) {
//        $query =    "SELECT DATE(date) AS date, HOUR(date) AS hour, ".
//                        "MAX(rate) open, MIN(rate) close, MAX(rate) high, MIN(rate) low, SUM(IF(src_currency = 'BTC', src_amount, 0)) volume ".
//                    "FROM exchange_lists WHERE src_currency = 'BTC' and type = ".TransactionType::EXCHANGE." ".
//                    "GROUP BY HOUR(date), DATE(date) ".
//                    " UNION ALL ".
//                    "SELECT DATE(created_at) AS date, HOUR(created_at) AS hour, ".
//                        "MAX(rate) open, MIN(rate) close, MAX(rate) high, MIN(rate) low, 0 volume ".
//                    "FROM btc_orders ".
//                    "GROUP BY  HOUR(created_at), DATE(created_at) ";
//
//        $query =    "SELECT date, hour, MAX(open) open, MAX(high) high, MIN(low) low, MIN(close) close, SUM(volume) volume ".
//                    "FROM (".$query.") AS t1 ".
//                    "GROUP BY date, hour ORDER BY date";
//
//        $data = \DB::select($query);
//
//        foreach ($data as $rec) {
//            $rec->date = (strtotime($rec->date) + $rec->hour * 3600) * 1000;
//        }
//
        $query =    "SELECT date, ".
                        "MAX(rate) open, MIN(rate) close, MAX(rate) high, MIN(rate) low, SUM(IF(src_currency = 'BTC', src_amount, 0)) volume ".
                    "FROM exchange_lists WHERE src_currency = 'BTC' and type = ".TransactionType::EXCHANGE." ".
                    "GROUP BY date ".
                    " UNION ALL ".
                    "SELECT created_at AS date, ".
                        "MAX(rate) open, MIN(rate) close, MAX(rate) high, MIN(rate) low, 0 volume ".
                    "FROM btc_orders ".
                    "GROUP BY  created_at ";

        $query =    "SELECT date, MAX(open) open, MAX(high) high, MIN(low) low, MIN(close) close, SUM(volume) volume ".
                    "FROM (".$query.") AS t1 ".
                    "GROUP BY date ORDER BY date";

        $data = \DB::select($query);

        foreach ($data as $rec) {
            $rec->date = strtotime($rec->date) * 1000;
        }
        return response()->json([
            'success' => true,
            'data' => $data
        ]);
    }
}
