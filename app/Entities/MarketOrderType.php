<?php

namespace App\Entities;


interface MarketOrderType {
    const BUY = 0;
    const SELL = 1;
}
