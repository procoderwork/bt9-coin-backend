<?php

namespace App\Http\Controllers\Api;

use App\Entities\BonusList;
use App\Entities\LoginHistory;
use App\Entities\TransactionType;
use App\Events\SendEmailEvent;
use App\Mail\VerifyWithdrawMail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Jenssegers\Agent\Agent;

class AccountController extends Controller
{
    //
    public function getInfo(Request $request) {
        $user = Auth::user();

        return response()->json([
            'id' => $user->id,
            'email' => $user->email,
            'username' => $user->username,
            'fullname' => $user->fullname,
            'allow_g2f' => $user->allow_g2f,
            'g2f_code' => $user->g2f_code,
            'role' => $user->role,
            'permission' => $user->permission,
            'enable_loginmsg' => $user->enable_loginmsg,
            'enable_withdrawmail' => $user->enable_withdrawmail ? $user->enable_withdrawmail : 0
        ]);
    }

    public function getReferral(Request $request) {
        $user = Auth::user();

        if (!$user) {
            return response('Not Authorized', 403);
        }

        $data = array();
        $children = array();

        $parent_ids = array($user->id);
        $children[$user->id] = &$data;

        $bonusLists = BonusList::all()->pluck('bonus', 'id');
        for ($i = 0; $i < 5; $i ++) {
            $parent_str  = implode($parent_ids, ',');

            $query =    "SELECT users.*, IFNULL(tokens.amount, '0') token_amount ".
                        "FROM users ".
                            "LEFT JOIN (SELECT user_id, SUM(dest_amount) amount FROM exchange_lists WHERE dest_currency = 'TOKEN' AND type = ".TransactionType::BUYTOKEN." GROUP BY user_id) tokens ON users.id = tokens.user_id ".
                        "WHERE parent IN ($parent_str) ";
            $result = \DB::select($query);

            if (count($result) == 0) break;

            $parent_ids = array();

            foreach ($result as $record) {
                $record->token_amount = (float)($record->token_amount);
                array_push($parent_ids, $record->id);

                $item = array();
                $record->level = 'LVL '.($i + 1);
                $record->bonus_percent = $bonusLists[$i + 1];
                $record->bonus_amount = $record->token_amount * $bonusLists[$i + 1] / 100;

                $lendingQuery = "SELECT SUM(dest_amount) bonus FROM exchange_lists WHERE user_id = ".$user->id." AND ref_id = ".$record->id. " ";
                $res = \DB::select($lendingQuery);

                if (count($result) > 0) {
                    $record->lending_bonus = $res[0]['bonus'];
                } else {
                    $record->lending_bonus = 0;
                }

                $item['data'] = $record;

                if (!is_array($children[$record->parent])) {
                    $children[$record->parent]= array();
                }
                if (!isset($children[$record->parent]["children"])) {
                    $children[$record->parent]["children"] = array();
                }

                array_push($children[$record->parent]["children"], $item);
                $children[$record->id] = &$children[$record->parent]["children"][count($children[$record->parent]["children"]) - 1];
            }
        }

        return response()->json([
            'success' => true,
            'data' => isset($data['children']) ? $data['children'] : array()
        ]);
    }

    public function loginHistory(Request $request) {
        $user = Auth::user();

        if (!$user) {
            return response('Not Authorized', 403);
        }

        $loginHistory = LoginHistory::where('user_id', $user->id)->orderBy('created_at', 'desc')->get();

        return response()->json([
            'success' => true,
            'data' => $loginHistory
        ]);
    }

    public function changeLoginEmailStatus(Request $request) {
        $status = $request->input('status');

        $user = Auth::user();

        $user->enable_loginmsg = $status;
        $user->save();


        return response()->json([
            'success' => true,
        ]);
    }


    public function changeWithdrawMailStatus(Request $request) {
        $status = $request->input('status');

        $user = Auth::user();

        $user->enable_withdrawmail = $status;
        $user->save();

        return response()->json([
            'success' => true,
        ]);
    }

    public function sentWithdrawMail(Request $request) {
        $user = Auth::user();

        $code = str_random(10);
        $user->confirmation_code = $code;
        $user->save();

        event(new SendEmailEvent($user, new VerifyWithdrawMail($user)));

        return response()->json([
            'success' => true,
        ]);
    }

    public function confirmWithdrawCode(Request $request) {
        $input = $request->input();

        $code = $input['code'];
        $user = Auth::user();

        if ($code == $user->confirmation_code) {
            $user->confirmation_code = '';
            $user->save();

            return response()->json([
                'success' => true,
            ]);
        } else {
            return response()->json([
                'success' => false,
            ]);
        }
    }
}
