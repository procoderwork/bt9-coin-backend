<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class BonusList extends Model
{
    //
    protected $table = "bonus_lists";
    public $timestamps = false;
}
