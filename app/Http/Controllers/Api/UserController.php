<?php

namespace App\Http\Controllers\Api;

use App\Entities\LoginHistory;
use App\Entities\ResetPassword;
use App\Entities\Setting;
use App\Events\SendEmailEvent;
use App\Libs\GoogleAuthenticator;
use App\Mail\ForgotPasswordMail;
use App\Mail\LoginNotificationMail;
use App\Mail\VerifyAccountMail;
use App\User;
use Carbon\Carbon;
use Hamcrest\Core\Set;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Symfony\Component\HttpFoundation\Exception\RequestExceptionInterface;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;
use Jenssegers\Agent\Agent;

class UserController extends Controller
{
    //
    public function register(Request $request) {
        $input = $request->input();

        if (User::where('email', $input['email'])->count() > 0) {
            return response()->json(['success' => false, 'error' => 'Email exist']);
        }

        if (User::where('username', $input['username'])->count() > 0) {
            return response()->json(['success' => false, 'error' => 'Username exist']);
        }

        $settings = Setting::all()->pluck('value', 'name');

        $user = new User;

        $confirmation_code = str_random(30);

        $user->username = $input['username'];
        $user->fullname = $input['fullname'];
        $user->email = $input['email'];
        $user->password = bcrypt($input['password']);
        if ($settings['enable_reg_notification'] == 1) {
            $user->confirmed = 0;
            $user->confirmation_code = $confirmation_code;
        } else {
            $user->confirmed = 1;
        }

        if (isset($input['sponser']) && $input['sponser'] != '') {
            $sponser = User::where('username', $input['sponser'])->first();
            if ($sponser) {
                $user->parent = $sponser->id;
            }
        }
        $user->save();

        if ($settings['enable_reg_notification'] == 1) {
            event(new SendEmailEvent($user, new VerifyAccountMail($user)));
        }

        return response()->json(['success' => true, 'user_id' => $user->id]);
    }

    public function login(Request $request) {
        $credentials = $request->only('email', 'password');

        $setting = Setting::all()->pluck('value', 'name');
        JWTAuth::factory()->setTTL($setting['disconnect_time']);

        try {
            // verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'Email or password is wrong', 'success' => false]);
            }
        } catch (JWTException $e) {
            // something went wrong
            return response()->json(['error' => 'token error'], 500);
        }

        $user = Auth::user();

        $agent = new Agent();
        $loginHistroy = new LoginHistory();

        $loginHistroy->user_id = $user->id;
        $loginHistroy->user_agent = $agent->getUserAgent();
        $loginHistroy->ip_address = $request->ip();
        $loginHistroy->ip_location = isset(\Location::get($request->ip())->countryCode) ? \Location::get($request->ip())->countryCode : '';
        $loginHistroy->browser = $agent->browser()." ".$agent->version($agent->browser());
        $loginHistroy->device = $agent->platform()." ".$agent->version($agent->platform());
        $loginHistroy->save();

        if ($user->enable_loginmsg == 1 && $user->confirmed == 1) {
            //event(new SendEmailEvent($user, new LoginNotificationMail($user, $loginHistroy)));
        }

        return response()->json([
            'success' => true,
            'id' => $user->id,
            'email' => $user->email,
            'username' => $user->username,
            'fullname' => $user->fullname,
            'allow_g2f' => $user->allow_g2f,
            'g2f_code' => $user->g2f_code,
            'token' => $token,
            'role' => $user->role,
            'permission' => $user->permission,
            'confirmed' => $user->confirmed,
            'enable_loginmsg' => $user->enable_loginmsg,
            'enable_withdrawmail' => $user->enable_withdrawmail ? $user->enable_withdrawmail : 0
        ]);
    }

    public function verifyAccount($confirmation_code, Request $request) {
        if (!$confirmation_code) {
            return response('Confirmation code is wrong');
        }

        $user = User::where('confirmation_code', $confirmation_code)->first();

        if (!$user) {
            return response('Confirmation code is wrong');
        }

        $user->confirmation_code = '';
        $user->confirmed = 1;

        $user->save();

        return redirect(env('CLIENT_URL').'/login/confirmed');
    }

    public function getSponser(Request $request) {
        $sponser = $request->input('sponser');

        if (isset($sponser) && $sponser != '') {
            $user = User::where('username', $sponser)->first();

            if ($user) {
                return response()->json([
                    'success' => true,
                    'user' => $user,
                ]);
            } else {
                return response()->json([
                    'success' => false,
                ]);
            }
        }
    }

    public function getG2FCode(Request $request) {
        $user = Auth::user();

        if (!$user) {
            return response('Not Authorized', 403);
        }

        if (empty($user->g2f_code)) {
            $gAuth = new GoogleAuthenticator();
            $code = $gAuth->generateSecret();

            $user->g2f_code = $code;
            $user->save();
        }

        return response()->json([
            'success' => true,
            'code' => $user->g2f_code
        ]);
    }

    public function setG2F(Request $request) {
        $input = $request->input();

        $user = Auth::user();
        $user->allow_g2f = $input['value'];
        if ($user->allow_g2f == 0) {
            $user->g2f_code = null;
        }

        $user->save();

        return response()->json([
            'success' => true,
        ]);
    }

    public function confirmG2FCode(Request $request) {
        $input = $request->input();

        if (!isset($input['code'])) {
            return response()->json([
                'success' => false,
            ]);
        }

        $user = Auth::user();

        $code = $input['code'];

        $g2f_code = $user->g2f_code;

        $gAuth = new GoogleAuthenticator();

        if ($gAuth->checkCode($g2f_code, $code)) {
            return response()->json([
                'success' => true,
            ]);
        } else {
            return response()->json([
                'success' => false,
            ]);
        }
    }

    public function confirmG2FCodeWithoutlogin(Request $request) {
        $input = $request->input();

        if (!isset($input['code'])) {
            return response()->json([
                'success' => false,
            ]);
        }

        $code = $input['code'];

        $g2f_code = $input['g2f_code'];

        $gAuth = new GoogleAuthenticator();

        if ($gAuth->checkCode($g2f_code, $code)) {
            return response()->json([
                'success' => true,
            ]);
        } else {
            return response()->json([
                'success' => false,
            ]);
        }
    }
    public function changePassword(Request $request) {
        $input = $request->input();

        $user = Auth::user();
        if (\Hash::check($input['oldPassword'], $user->password)) {
            $user->password = bcrypt($input['password']);
            $user->save();

            return response()->json(['success' => true]);
        } else {
            return response()->json(['success' => false, 'error' => 'Current password is wrong.']);
        }
    }

    public function sendActivateEmail(Request $request) {
        $input = $request->input();

        $email = $input['email'];

        $user = User::where('email', $email)->first();

        if (!$user) {
            return response()->json(['success' => false, 'error' => 'Wrong Email']);
        }

        if ($user->confirmed == 1) {
            return response()->json(['success' => false, 'error' => 'Your email was already confirmed.']);
        }

        if (empty($user->confirmation_code)) {
            $confirmation_code = str_random(30);
            $user->confirmation_code = $confirmation_code;
            $user->save();
        }

        event(new SendEmailEvent($user, new VerifyAccountMail($user)));

        return response()->json(['success' => true]);
    }

    public function sendForgotEmail(Request $request) {
        $input = $request->input();

        $email = $input['email'];

        $user = User::where('email', $email)->first();

        if (!$user) {
            return response()->json(['success' => false, 'error' => 'Wrong Email']);
        }

        $resetPassword = ResetPassword::where('user_id', $user->id)->first();
        if (!$resetPassword) {
            $resetPassword = new ResetPassword();
            $resetPassword->user_id = $user->id;
        }

        $resetPassword->confirm_code = str_random(50);
        $resetPassword->validate_time = Carbon::now()->addHour(2);

        $resetPassword->save();

        event(new SendEmailEvent($user, new ForgotPasswordMail($user, $resetPassword->confirm_code)));

        return response()->json(['success' => true]);
    }

    public function confirmResetCode(Request $request) {
        $input = $request->input();

        $code = $input['code'];

        if (!$code) {
            return response()->json(['success' => false]);
        }

        $resetPassword = ResetPassword::where([['confirm_code', $code], ['validate_time', '>=', Carbon::now()]])->first();

        if (!$resetPassword) {
            return response()->json(['success' => false]);
        }

        $user_id = $resetPassword->user_id;
        //$resetPassword->delete();

        return response()->json(['success' => true, 'user_id' => $user_id]);
    }

    public function resetPassword(Request $request) {
        $input = $request->input();

        $user = User::find($input['id']);
        if(!$user) {
            return response()->json(['success' => false, 'error' => 'User Not Exist.']);
        }

        $user->password = bcrypt($input['password']);
        $user->save();

        return response()->json(['success' => true]);
    }
}
