<!DOCTYPE html>
<html>
<head>
    <title>Verify your account</title>
</head>
<style>
    body {
        font-size: 16px;
    }
    .button {
        background-color: #4caf50;
        color: #ffffff;
        border-radius: 5px;
        cursor: pointer;
        display: inline-block;
        font-size: 14px;
        font-weight: bold;
        margin: 0;
        padding: 12px 25px;
        text-decoration: none;
        text-transform: capitalize;
        height: 15px;
    }
</style>
<body>
    <b>Password reset</b>
    <br>
    <br>
    This is verification e-mail for your forgotten password. By clicking on the following URL, your old password will be replaced with your new one.
    <br>
    <br>
    The URL will expire in 2 hours
    <br>
    <br>
    You can reset password by clicking the button or going to
    <a href="{{$user->reset_url}}">{{$user->reset_url}}</a>
    <br>
    <br>
    <a href="{{$user->reset_url}}" class="button">
        Reset Password Now
    </a>
</body>
</html>