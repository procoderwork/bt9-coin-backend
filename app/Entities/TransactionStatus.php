<?php

namespace App\Entities;


interface TransactionStatus {
    const PENDING = 0;
    const SUCCESS = 1;
    const FAIL = 2;
    const RECORD = 3;
    const OPEN = 4;
}