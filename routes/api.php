<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['middleware' => ['cors']], function(){

    Route::get('/bt9_test', 'Api\TokenController@test');
    Route::get('/btc_test', 'Api\BTCController@test');
    Route::get('/eth_test', 'Api\ETHController@test');


    Route::post('/register', 'Api\UserController@register');
    Route::post('/login', 'Api\UserController@login');
    Route::get('/get_sponser', 'Api\UserController@getSponser');
    Route::post('/confirmG2FCodeWithoutlogin', 'Api\UserController@confirmG2FCodeWithoutlogin');

    Route::get('/account/verify/{confirmation_code}', 'Api\UserController@verifyAccount');

    // resend activation link
    Route::post('/account/sendActivateEmail', 'Api\UserController@sendActivateEmail');
    // send reset password mail
    Route::post('/account/sendForgotEmail', 'Api\UserController@sendForgotEmail');
    // confirm reset code
    Route::get('/confirm_resetcode', 'Api\UserController@confirmResetCode');
    // reset password
    Route::post('/reset_password', 'Api\UserController@ResetPassword');

    Route::get('/rate', 'Api\TokenController@getCurrentRate');
    Route::get('/token_rate', 'Api\TokenController@getTokenRate');

    Route::post('/withdraw/schedule', 'Api\TokenController@withdrawCron');
    Route::get('/withdraw/schedule', 'Api\TokenController@withdrawCron');

    Route::get('/test_email',function() {
        return View('emails.forgot');
    });

    // Settings
    Route::get('/settings', 'Api\SettingsController@all');

    Route::group(['middleware' => ['jwt.auth']], function()
    {
        Route::get('/account', 'Api\AccountController@getInfo');
        Route::get('/account/loginHistory', 'Api\AccountController@loginHistory');
        Route::post('/account/changeLoginEmailStatus', 'Api\AccountController@changeLoginEmailStatus');
        Route::post('/account/changeWithdrawMailStatus', 'Api\AccountController@changeWithdrawMailStatus');

        Route::post('/upload/file', 'Api\UploadController@upload');

        Route::get('/getG2FCode', 'Api\UserController@getG2FCode');
        Route::post('/setG2F', 'Api\UserController@setG2F');
        Route::post('/change_password', 'Api\UserController@changePassword');
        Route::post('/confirmG2FCode', 'Api\UserController@confirmG2FCode');

        Route::get('/get_referral', 'Api\AccountController@getReferral');
        // Coin API
        Route::post('/{coin}/withdraw', 'Api\CoinController@withdraw')->where('coin', 'BTC|ETH|BT9');
        Route::post('/{coin}/withdraw/cancel', 'Api\CoinController@cancelWithdraw')->where('coin', 'BTC|ETH|BT9');

        Route::get('/{coin}/unconfirmed/senttx', 'Api\CoinController@getUnconfirmedSentTxList')->where('coin', 'BTC|ETH|BT9');

        Route::post('/withdraw/sendmail', 'Api\AccountController@sentWithdrawMail');
        Route::post('/withdraw/confirmcode', 'Api\AccountController@confirmWithdrawCode');

        // BTC API
        Route::get('/BTC/address', 'Api\BTCController@getAddress');
        Route::get('/BTC/balance', 'Api\BTCController@getBalance');
        Route::get('/BTC/gettx', 'Api\BTCController@getTxList');
        Route::get('/BTC/unconfirmed/receivedtx', 'Api\BTCController@getUnconfirmedReceivedTxList');

        // ETH API
        Route::get('/ETH/address', 'Api\ETHController@getAddress');
        Route::get('/ETH/balance', 'Api\ETHController@getBalance');
        Route::get('/ETH/gettx', 'Api\ETHController@getTxList');
        Route::get('/ETH/unconfirmed/receivedtx', 'Api\ETHController@getUnconfirmedReceivedTxList');

        // bt9coind API
        Route::get('/BT9/address', 'Api\TokenController@getAddress');
        Route::get('/BT9/unconfirmed/receivedtx', 'Api\TokenController@getUnconfirmedReceivedTxList');

        // Token API
        Route::get('/token_balance', 'Api\TokenController@getBalance');
        Route::get('/toten_total', 'Api\TokenController@totalBalance');
        Route::get('/token_history', 'Api\TokenController@getTokenHistory');

        // Usd API
        Route::get('/usd_balance', 'Api\TokenController@getUsdBalance');


        Route::get('/bonus_lists', 'Api\TokenController@getBonusLists');

        Route::post('/buyTokenWithBTC', 'Api\TokenController@BuyTokenWithBTC');
        Route::post('/buyTokenWithETH', 'Api\TokenController@BuyTokenWithETH');

        // ICO Agenda
        Route::get('/ico_agenda', 'Api\ICOAgendaController@getICOAgenda');
        Route::get('/ico_agenda/chart', 'Api\ICOAgendaController@getICOAgendaChartData');

        // Orders BTC/MyAltCoin
        Route::post('/order/btc/buy', 'Api\BTCOrderController@buyOrder');
        Route::post('/order/btc/sell', 'Api\BTCOrderController@sellOrder');
        Route::post('/order/btc/delete', 'Api\BTCOrderController@deleteOrder');


        Route::get('/order/btc/buy', 'Api\BTCOrderController@getBuyOrder');
        Route::get('/order/btc/sell', 'Api\BTCOrderController@getSellOrder');

        Route::get('/order/btc/open', 'Api\BTCOrderController@getOpenOrder');
        Route::get('/order/btc/past', 'Api\BTCOrderController@getPastOrder');

        // Orders ETH/MyAltCoin
        Route::post('/order/eth/buy', 'Api\ETHOrderController@buyOrder');
        Route::post('/order/eth/sell', 'Api\ETHOrderController@sellOrder');
        Route::post('/order/eth/delete', 'Api\ETHCOrderController@deleteOrder');


        Route::get('/order/eth/buy', 'Api\ETHOrderController@getBuyOrder');
        Route::get('/order/eth/sell', 'Api\ETHOrderController@getSellOrder');

        Route::get('/order/eth/open', 'Api\ETHOrderController@getOpenOrder');
        Route::get('/order/eth/past', 'Api\ETHOrderController@getPastOrder');

        // Lending
        Route::get('/lending/setting', 'Api\LendingController@getSetting');
        Route::get('/lending', 'Api\LendingController@get');
        Route::post('/lending', 'Api\LendingController@save');
        Route::get('/lending/balance', 'Api\LendingController@getLendingBalance');
        Route::get('/lending/daily', 'Api\LendingController@getDailyInterest');
        Route::post('/lending/transfer', 'Api\LendingController@transferLending');
        Route::post('/lending/transfer/daily', 'Api\LendingController@transaferDailyInterest');
        Route::get('/lending/tx', 'Api\LendingController@getTx');
        Route::get('/lending/interest', 'Api\LendingController@getInterest');

        // Staking
        Route::get('/staking', 'Api\StakingController@get');
        Route::post('/staking', 'Api\StakingController@save');
        Route::get('/staking/balance', 'Api\StakingController@getStakingBalance');

        // Exchange
        Route::get('/exchange/BTC/statistics', 'Api\BTCController@getStatistics');
        Route::get('/exchange/BTC/history', 'Api\BTCController@getHistory');
        Route::get('/exchange/ETH/statistics', 'Api\ETHController@getStatistics');
        Route::get('/exchange/ETH/history', 'Api\ETHController@getHistory');

        // Ticket
        Route::get('/ticket', 'Api\TicketController@getTicket');
        Route::get('/ticket/detail', 'Api\TicketController@getDetail');
        Route::post('/ticket', 'Api\TicketController@postTicket');
        Route::post('/ticket/reply', 'Api\TicketController@replyTicket');

        // Admin Api
        Route::group(['middleware' => ['role:admin']], function () {

            Route::get('/admin/adminUsers', 'Api\AdminController@getAdminUsers');
            Route::post('/admin/add/adminUser', 'Api\AdminController@addAdminUser');
            Route::post('/admin/delete/adminUser', 'Api\AdminController@deleteAdminUser');
            Route::get('/admin/user/count', 'Api\AdminController@getUserCount');
            Route::get('/admin/users', 'Api\AdminController@getUsers');
            Route::post('/admin/user/delete', 'Api\AdminController@deleteUser');

            Route::get('/admin/user/get', 'Api\AdminController@getUser');
            Route::get('/admin/user/getIdByEmail', 'Api\AdminController@getUserIDByEmail');

            Route::get('/admin/sysCurrency', 'Api\AdminController@getSystemCurrency');
            Route::get('/admin/token_chart', 'Api\AdminController@getTokenChartData');
            Route::get('/admin/syscurrency_chart', 'Api\AdminController@getSysCurrencyChartData');

            // get Token List
            Route::get('/admin/user/{coin}/history', 'Api\AdminController@getUserCoinHistory');

            // get Transaction History
            Route::get('/admin/BT9/transactions', 'Api\TokenController@getAllTx');
            Route::get('/admin/BTC/transactions', 'Api\BTCController@getAllTx');
            Route::get('/admin/ETH/transactions', 'Api\ETHController@getAllTx');

            // save bonus
            Route::post('/admin/bonus_lists', 'Api\AdminController@saveBonus');

            // save token rates
            Route::post('/admin/token_rates', 'Api\AdminController@saveTokenRates');

            // save system settings
            Route::post('/admin/settings', 'Api\AdminController@saveSettings');

            // add myaltcoin
            Route::post('/admin/exchange/add', 'Api\TokenController@addMyAltCoin');

            // ICO Agenda
            Route::post('/ico_agenda', 'Api\ICOAgendaController@saveICOAgenda');
            Route::post('/ico_agenda/delete', 'Api\ICOAgendaController@deleteICOAgenda');

            // Lending
            Route::post('/lending/setting', 'Api\LendingController@saveSetting');
            Route::post('/lending/setting/delete', 'Api\LendingController@deleteSetting');
            Route::get('/lending/admin', 'Api\LendingController@getAllLending');
            Route::post('/lending/release/all', 'Api\LendingController@releaseAll');

            // ICO Histroy
            Route::get('/admin/ico/history', 'Api\AdminController@getICOHistory');

            // Withdraw
            Route::get('/admin/withdraw/{coin}/pending', 'Api\CoinController@getWithdrawPending')->where('coin', 'BTC|ETH|BT9');
            Route::get('/admin/wallet/BT9/balance', 'Api\TokenController@getTotalBalance');
            Route::get('/admin/wallet/BTC/balance', 'Api\BTCController@getTotalBalance');
            Route::get('/admin/wallet/ETH/balance', 'Api\ETHController@getTotalBalance');

            Route::post('/admin/BT9/withdraw/estimate_fee', 'Api\TokenController@estimateFee');
            Route::post('/admin/BTC/withdraw/estimate_fee', 'Api\BTCController@estimateFee');
            Route::post('/admin/ETH/withdraw/estimate_fee', 'Api\ETHController@estimateFee');

            Route::post('/admin/BT9/withdraw', 'Api\TokenController@withdrawPending');
            Route::post('/admin/BTC/withdraw', 'Api\BTCController@withdrawPending');

            Route::get('/admin/user/statistics', 'Api\AdminController@getUserStatistics');
            Route::get('/admin/ico/statistics', 'Api\AdminController@getICOStatistics');
            Route::get('/admin/lending/statistics', 'Api\AdminController@getLendingStatistics');
            Route::get('/admin/staking/statistics', 'Api\AdminController@getStakingStatistics');
            Route::get('/admin/fee/statistics', 'Api\AdminController@getFeeStatistics');

            // Ticket
            Route::get('/ticket/all', 'Api\TicketController@getAllTicket');
            Route::post('/ticket/close', 'Api\TicketController@closeTicket');

            // Staking
            Route::get('/staking/admin', 'Api\StakingController@getAllStaking');
            Route::post('/staking/release/all', 'Api\StakingController@releaseAll');
        });
    });
});
