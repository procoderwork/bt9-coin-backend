<?php

namespace App\Http\Controllers\Api;

use App\Entities\BonusList;
use App\Entities\BtcAddress;
use App\Entities\EthAddress;
use App\Entities\ExchangeList;
use App\Entities\LendingDaily;
use App\Entities\LoginHistory;
use App\Entities\ResetPassword;
use App\Entities\Setting;
use App\Entities\TokenRate;
use App\Entities\TransactionStatus;
use App\Entities\TransactionType;
use App\Events\SendEmailEvent;
use App\Libs\GoogleAuthenticator;
use App\Mail\ForgotPasswordMail;
use App\Mail\LoginNotificationMail;
use App\Mail\VerifyAccountMail;
use App\User;
use BlockCypher\Auth\SimpleTokenCredential;
use BlockCypher\Client\AddressClient;
use BlockCypher\Rest\ApiContext;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Symfony\Component\HttpFoundation\Exception\RequestExceptionInterface;
use tests\Simple;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;
use Jenssegers\Agent\Agent;

class AdminController extends Controller
{
    //

    public function getBlockCypherToken()
    {
        $token = Setting::where('name', 'blockcypher_token')->first();

        if ($token) {
            return $token->value;
        } else {
            return env('BLOCKCYPHER_TOKEN');
        }
    }

    public function getAdminUsers(Request $request)
    {
        $admins = User::where('role', 'admin')->orderBy('created_at', 'desc')->get();

        return response()->json([
            'success' => true,
            'data' => $admins
        ]);
    }

    public function addAdminUser(Request $request)
    {
        $input = $request->input();

        if (isset($input['id']) && $input['id'] > 0) {
            $user = User::find($input['id']);

            if (!$user) {
                return response()->json(['success' => false, 'error' => 'User not exist']);
            }
            $user->permission = $input['permission'];
            $user->save();

        } else {
            if (User::where('email', $input['email'])->count() > 0) {
                return response()->json(['success' => false, 'error' => 'Email exist']);
            }

            if (User::where('username', $input['username'])->count() > 0) {
                return response()->json(['success' => false, 'error' => 'Username exist']);
            }

            $user = new User;

            $user->username = $input['username'];
            $user->fullname = $input['fullname'];
            $user->email = $input['email'];
            $user->password = bcrypt($input['password']);
            $user->permission = $input['permission'];
            $user->confirmed = 1;
            $user->role = 'admin';

            $user->save();
        }

        return response()->json(['success' => true]);
    }

    public function deleteAdminUser(Request $request)
    {
        $input = $request->input();

        if (isset($input['id'])) {
            $user = User::find($input['id']);

            if (!$user) {
                return response()->json(['success' => false, 'error' => 'User not exist']);
            }

            $user->delete();

        } else {
            return response()->json(['success' => false, 'error' => 'User not exist']);
        }

        return response()->json(['success' => true]);
    }

    public function getUserCount(Request $request)
    {
        $count = User::where('role', '<>', 'admin')->orWhereNull('role')->count();

        return response()->json(['success' => true, 'count' => $count]);
    }

    public function getSystemCurrency(Request $request)
    {
        $btc_total = ExchangeList::where('src_currency', 'BTC')->sum('src_amount');
        $eth_total = ExchangeList::where('src_currency', 'ETH')->sum('src_amount');

        return response()->json([
            'success' => true,
            'btc_total' => $btc_total,
            'eth_total' => $eth_total,
        ]);
    }

    public function getTokenChartData(Request $request)
    {
        $now = Carbon::now();
        $start = Carbon::now()->addWeek(-1);

        $query = "SELECT DATE(created_at) date, SUM(dest_amount) amount FROM exchange_lists WHERE dest_currency = 'TOKEN' AND DATE(created_at) > '$start' GROUP BY DATE(created_at) ORDER BY date";
        $result = \DB::select($query);

        return response()->json(['success' => true, 'data' => $result]);
    }

    public function getSysCurrencyChartData(Request $request)
    {
        $now = Carbon::now();
        $start = Carbon::now()->addWeek(-1);

        $query = "SELECT DATE(created_at) date, SUM(src_amount) amount FROM exchange_lists WHERE src_currency = 'BTC' AND DATE(created_at) > '$start' GROUP BY DATE(created_at) ORDER BY date";
        $btc_data = \DB::select($query);

        $query = "SELECT DATE(created_at) date, SUM(src_amount) amount FROM exchange_lists WHERE src_currency = 'ETH' AND DATE(created_at) > '$start' GROUP BY DATE(created_at) ORDER BY date";
        $eth_data = \DB::select($query);

        return response()->json([
            'success' => true,
            'btc_data' => $btc_data,
            'eth_data' => $eth_data
        ]);
    }

    public function getUsers()
    {
        $users = User::where('role', '<>', 'admin')
            ->orWhereNull('role')
            // ->leftJoin(\DB::raw("(SELECT user_id, SUM(dest_amount) amount FROM exchange_lists WHERE dest_currency = 'TOKEN' GROUP BY user_id) AS exchanges"), 'users.id', '=', 'exchanges.user_id')
            // ->select('users.*', 'exchanges.amount AS token_amount')
            ->get();

        return response()->json([
            'success' => true,
            'data' => $users,
        ]);
    }

    public function getUserIDByEmail(Request $request)
    {
        $email = $request->input('email');

        if (!isset($email)) {
            return response()->json([
                'success' => false,
            ]);
        }

        $user = User::where('email', $email)->first();
        if (!$user) {
            return response()->json([
                'success' => false,
            ]);
        }

        return response()->json([
            'success' => true,
            'id' => $user->id
        ]);
    }

    public function getUser(Request $request)
    {
        $id = $request->input('id');

        if (!isset($id)) {
            return response()->json([
                'success' => false,
            ]);
        }

        $user = User::find($id);
        if (!$user) {
            return response()->json([
                'success' => false,
            ]);
        }

        $tokenController = new TokenController();
        $user->token_balance = $tokenController->balance($user);

        $btcController = new BTCController();
        $user->btc_balance = $btcController->balance($user);

        $ethController = new ETHController();
        $user->eth_balance = $ethController->balance($user);

        return response()->json([
            'success' => true,
            'data' => $user
        ]);
    }

    public function deleteUser(Request $request)
    {
        $email = $request->input('email');

        if (!isset($email)) {
            return response()->json([
                'success' => false,
            ]);
        }

        $user = User::where('email', $email)->first();
        if (!$user) {
            return response()->json([
                'success' => false,
            ]);
        }

        $user->delete();

        return response()->json([
            'success' => true,
            'id' => $user->id
        ]);
    }

    public function saveBonus(Request $request)
    {
        $input = $request->input();

        foreach ($input as $record) {
            $bonus = BonusList::find($record['id']);

            if ($bonus) {
                $bonus->bonus = $record['bonus'];
                $bonus->save();
            }
        }

        return response()->json([
            'success' => true,
        ]);
    }

    public function saveTokenRates(Request $request)
    {
        $input = $request->input();

        foreach ($input as $record) {
            $rate = TokenRate::find($record['id']);

            if ($rate) {
                $rate->rate = $record['rate'];
                $rate->bonus = $record['bonus'];
                $rate->save();
            }
        }

        return response()->json([
            'success' => true,
        ]);
    }

    public function saveSettings(Request $request)
    {
        $input = $request->input();

        foreach ($input as $key => $value) {
            $setting = Setting::where('name', $key)->first();

            if (!$setting) {
                $setting = new Setting();
                $setting->name = $key;
            }

            if ($setting) {
                $setting->value = $value;
                $setting->save();
            }
        }

        return response()->json([
            'success' => true,
        ]);
    }

    public function getUSDBalance(Request $request) {
        $input = $request->input();


    }

    public function getICOHistory(Request $request) {
        $input = $request->input();

        $lists = ExchangeList::where([['type', TransactionType::BUYTOKEN], ['status', TransactionStatus::SUCCESS]])
            ->leftJoin('users', 'users.id', 'exchange_lists.user_id')
            ->select('exchange_lists.*', 'users.email')
            ->orderBy('exchange_lists.date')
            ->get();

        return response()->json([
            'success' => true,
            'data' => $lists
        ]);
    }

    public function getUserCoinHistory($coin, Request $request) {
        $input = $request->input();

        $user_id = isset($input['user_id']) ? $input['user_id'] : 0;

        $user = User::find($user_id);

        if (!$user) {
            return response()->json([
                'success' => false,
                'error' => 'User Not Found'
            ]);
        }

        $data = array();
        if ($coin == 'BTC') {
            $btcController = new BTCController();
            $data = $btcController->getUserHistory($user);
        } else if ($coin == 'ETH') {
            $ethController = new ETHController();
            $data = $ethController->getUserHistory($user);
        } else {
            $tokenController = new TokenController();
            $data = $tokenController->getUserHistory($user);
        }

        return response()->json([
            'success' => true,
            'data' => $data
        ]);
    }

    public function getUserStatistics(Request $request) {
        $total_user = User::where('role', '<>', 'admin')->orWhereNull('role')->count();
        $confirmed_user = User::where('confirmed', '1')->where(function($query) {
            $query->where('role', '<>', 'admin');
            $query->orWhereNull('role');
        })->count();

        return response()->json([
            'success' => true,
            'data' => array(
                'total_user' => $total_user,
                'confirmed_user' => $confirmed_user,
                'unconfirmed_user'=> $total_user - $confirmed_user
            )
        ]);
    }

    public function getICOStatistics(Request $request) {
        $total_sold = ExchangeList::where('type', TransactionType::BUYTOKEN)->sum('dest_amount');

        $btc_purchase = ExchangeList::where([['type', TransactionType::BUYTOKEN], ['src_currency', 'BTC']])->sum('src_amount');
        $eth_purchase = ExchangeList::where([['type', TransactionType::BUYTOKEN], ['src_currency', 'ETH']])->sum('src_amount');

        return response()->json([
            'success' => true,
            'data' => array(
                'total_sold' => $total_sold,
                'btc_purchase' => $btc_purchase,
                'eth_purchase'=> $eth_purchase
            )
        ]);
    }

    public function getLendingStatistics(Request $request) {
        $total_lending = ExchangeList::where('dest_currency', 'LENDING')->sum('dest_amount');
        $locked_amount = ExchangeList::where([['dest_currency', 'LENDING'], ['finish_date', '>', Carbon::now()]])->sum('dest_amount');

        $daily_amount = 0;

        $interests = LendingDaily::all()->pluck('value', 'date');

        $lendings = ExchangeList::where('type', TransactionType::LENDING)->get();
        foreach ($lendings as $lending) {
            $date = new Carbon($lending->date);
            $finish_date = new Carbon($lending->finish_date);
            $now = Carbon::now();

            $date->addDay(1);
            while ($date <= $finish_date && $date <= $now) {
                $daily = $interests[$date->toDateString()];
                $daily_amount += $lending->dest_amount * ($daily + $lending->rate) * 0.01;
                $date->addDay(1);
            }
        }

        return response()->json([
            'success' => true,
            'data' => array(
                'total_amount' => $total_lending,
                'locked_amount' => $locked_amount,
                'daily_amount'=> $daily_amount
            )
        ]);
    }

    public function getStakingStatistics(Request $request) {
        $locked_amount = ExchangeList::where([['type', TransactionType::STAKING], ['finish_date', '>', Carbon::now()]])->sum('src_amount');
        $total_staking = ExchangeList::where('type', TransactionType::STAKING)->sum('src_amount');

        $earned_amount = 0;

        $stakings = ExchangeList::where('type', TransactionType::STAKING)->get();
        foreach ($stakings as $staking) {
            $date = new Carbon($staking->date);
            $finish_date = new Carbon($staking->finish_date);
            $now = Carbon::now();

            $date->addDay(1);
            while ($date <= $finish_date && $date <= $now) {
                $earned_amount += $staking->src_amount * $staking->rate * 0.01;
                $date->addDay(1);
            }
        }

        return response()->json([
            'success' => true,
            'data' => array(
                'total_amount' => $total_staking,
                'locked_amount' => $locked_amount,
                'earned_amount'=> $earned_amount
            )
        ]);
    }

    public function getFeeStatistics(Request $request) {
        $bt9_withdrawfee = ExchangeList::where([['type', TransactionType::WITHDRAW], ['src_currency', 'TOKEN'], ['status', TransactionStatus::SUCCESS]])->sum('fee');
        $bt9_exchangefee = ExchangeList::where([['type', TransactionType::EXCHANGE], ['dest_currency', 'TOKEN'], ['status', TransactionStatus::SUCCESS]])->sum('fee');

        $bt9_fee = $bt9_withdrawfee + $bt9_exchangefee;

        $btc_withdrawfee = ExchangeList::where([['type', TransactionType::WITHDRAW], ['src_currency', 'BTC'], ['status', TransactionStatus::SUCCESS]])->sum('fee');
        $btc_exchangefee = ExchangeList::where([['type', TransactionType::EXCHANGE], ['dest_currency', 'BTC'], ['status', TransactionStatus::SUCCESS]])->sum('fee');

        $btc_fee = $btc_withdrawfee + $btc_exchangefee;


        $eth_withdrawfee = ExchangeList::where([['type', TransactionType::WITHDRAW], ['src_currency', 'ETH'], ['status', TransactionStatus::SUCCESS]])->sum('fee');
        $eth_exchangefee = ExchangeList::where([['type', TransactionType::EXCHANGE], ['dest_currency', 'ETH'], ['status', TransactionStatus::SUCCESS]])->sum('fee');

        $eth_fee = $eth_withdrawfee + $eth_exchangefee;

        return response()->json([
            'success' => true,
            'data' => array(
                'btc_fee' => $btc_fee,
                'bt9_fee' => $bt9_fee,
                'eth_fee'=> $eth_fee
            )
        ]);
    }
}
