<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class BtcTestAddress extends Model
{
    //
    protected $table = 'btctest_address';
}
