<!DOCTYPE html>
<html>
<head>
    <title>Verify your withdraw</title>
</head>
<style>
    body {
        font-size: 16px;
    }
    .button {
        background-color: #4caf50;
        color: #ffffff;
        border-radius: 5px;
        cursor: pointer;
        display: inline-block;
        font-size: 14px;
        font-weight: bold;
        margin: 0;
        padding: 12px 25px;
        text-decoration: none;
        text-transform: capitalize;
        height: 15px;
    }
</style>
<body>
    Hi <b>{{$user->fullname}}</b>
    <br>
    <br>
    Thank you for your withdraw!
    <br>
    <br>
    In order to verify your withdraw, please enter this code.
    <br>
    <br>
    <b>{{$user->confirmation_code}}</b>
    <br>
    <br>
</body>
</html>