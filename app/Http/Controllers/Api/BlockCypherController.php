<?php

namespace App\Http\Controllers\Api;

use App\Entities\BtcAddress;
use App\Entities\EthAddress;
use App\Entities\ExchangeList;
use App\Entities\MarketOrderType;
use App\Entities\Order;
use App\Entities\Setting;
use App\User;
use BitWasp\Bitcoin\Transaction\Transaction;
use BlockCypher\Api\TX;
use BlockCypher\Api\Wallet;
use BlockCypher\Auth\SimpleTokenCredential;
use BlockCypher\Client\AddressClient;
use BlockCypher\Client\BlockchainClient;
use BlockCypher\Client\WalletClient;
use BlockCypher\Rest\ApiContext;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class BlockCypherController extends Controller
{
    //
    public $BIT_SATOSHI = 100000000;

    public function getBlockCypherToken() {
        $token = Setting::where('name', 'blockcypher_token')->first();
        
        if ($token) {
            return $token->value;
        } else {
            return env('BLOCKCYPHER_TOKEN');
        }
        
    }
    public function getAccounts() {
        $apiContext = ApiContext::create(
            'main', 'btc', 'v1',
            new SimpleTokenCredential($this->getBlockCypherToken()),
            array('log.LogEnabled' => true, 'log.FileName' => 'BlockCypher.log', 'log.LogLevel' => 'DEBUG')
        );


        $addressClient = new \BlockCypher\Client\AddressClient($apiContext);

        $addresses = array(
            '18pmY9JJD8dfWQZaZgiEZzmfj8DinoHDDe',
            '1HX2CMsNqiyQJGG2YCjPXz5zy3cYGfQ1t7'
        );

        try {
            $fullAddress = $addressClient->getMultipleFullAddresses($addresses);

            //$addressBalance = $addressClient->getBalance($walletName);
        } catch (\Exception $ex) {
            return response()->json('Failed', 403);
        }

        var_dump($fullAddress);
    }
    public function getCoinAddress($user, $coin) {
        if ($coin == 'btc') {
            return BtcAddress::where('user_id', $user->id)->first();
        } else if ($coin == 'ltc') {
            return LtcAddress::where('user_id', $user->id)->first();
        } else if ($coin == 'eth') {
            return EthAddress::where('user_id', $user->id)->first();
        }
    }

    //
    public function getAddress($coin, Request $request) {
        $user = Auth::user();

        if (!$user) {
            return response('Not Authorized', 403);
        }

        $coin = strtolower($coin);

        $apiContext = ApiContext::create(
            'main', $coin, 'v1',
            new SimpleTokenCredential($this->getBlockCypherToken()),
            array('log.LogEnabled' => true, 'log.FileName' => 'BlockCypher.log', 'log.LogLevel' => 'DEBUG')
        );

        $coin_address = $this->getCoinAddress($user, $coin);
        if ($coin_address && $coin_address->address) {
            return response()->json([
                'success' => true,
                'address' => $coin_address->address
            ]);
        }

        if (!$coin_address) {
            if ($coin == 'btc') {
                $coin_address = new BtcAddress();
                $coin_address->user_id = $user->id;
            } else if ($coin == 'ltc') {
                $coin_address = new LtcAddress();
                $coin_address->user_id = $user->id;
            } else if ($coin == 'eth') {
                $coin_address = new EthAddress();
                $coin_address->user_id = $user->id;
            }
        }
        $label = "USER_".env('WALLET_PREFIX').$user->id;

        try {
            $addressClient = new AddressClient($apiContext);
            $addressKeyChain = $addressClient->generateAddress();

            $newAddress = $addressKeyChain->getAddress();

            $coin_address->address = $newAddress;
            $coin_address->privatekey = $addressKeyChain->getPrivate();
            $coin_address->publickey = $addressKeyChain->getPublic();
            $coin_address->wif = $addressKeyChain->getWif();
            $coin_address->save();

            return response()->json([
                'success' => true,
                'address' => $newAddress
            ]);

//            if ($coin == 'eth') {
//            } else {
//                $wallet = new Wallet();
//                $wallet->setName($label);
//                $wallet->setAddresses(array(
//                    $newAddress
//                ));
//
//                $walletClient = new WalletClient($apiContext);
//                $createdWallet = $walletClient->create($wallet);
//            }
        } catch (\Exception $e) {
            return response($e->getMessage(), 403);
        }

        return response('Failed', 403);
    }

    public function balance($coin, $user) {
        $coin = strtolower($coin);

        $apiContext = ApiContext::create(
            'main', $coin, 'v1',
            new SimpleTokenCredential($this->getBlockCypherToken()),
            array('log.LogEnabled' => true, 'log.FileName' => 'BlockCypher.log', 'log.LogLevel' => 'DEBUG')
        );

        $coin_address = $this->getCoinAddress($user, $coin);
        if (!$coin_address) {
            return response()->json('Failed', 403);
        }

        $addressClient = new AddressClient($apiContext);

        try {
            $addressBalance = $addressClient->getBalance($coin_address->address);
            $balance = $addressBalance->final_balance / $this->BIT_SATOSHI;
        } catch (\Exception $e) {
            $balance = 0;
        }

        if ($coin == 'BTC') {
            $sent = ExchangeList::where([['src_currency', 'BTC'], ['user_id', $user->id]])->sum('src_amount');
            $receive = ExchangeList::where([['dest_currency', 'BTC'], ['user_id', $user->id]])->sum('dest_amount');

            $sell = Order::where([['user_id', $user->id], ['type', MarketOrderType::SELL]])->sum('amount');

            $balance = $balance - $sent + $receive - $sell;
        }
        return $balance;
    }
    public function getBalance($coin, Request $request) {
        $user = Auth::user();

        if (!$user) {
            return response('Not Authorized', 403);
        }



        return response()->json([
            'success' => true,
            'available_balance' => $this->balance($coin, $user)
        ]);
    }

    public function withdraw($coin, Request $request) {
        $input = $request->input();

        $user = Auth::user();

        if (!$user) {
            return response('Not Authorized', 403);
        }

        $coin = strtolower($coin);

        $apiContext = ApiContext::create(
            'main', $coin, 'v1',
            new SimpleTokenCredential($this->getBlockCypherToken()),
            array('log.LogEnabled' => true, 'log.FileName' => 'BlockCypher.log', 'log.LogLevel' => 'DEBUG')
        );

        $coin_address = $this->getCoinAddress($user, $coin);
        if (!$coin_address) {
            return response()->json('Failed', 403);
        }

        $addressClient = new AddressClient($apiContext);

        try {
            $addressBalance = $addressClient->getBalance($coin_address->address);
        } catch (\Exception $e) {
            return response($e->getMessage(), 403);
        }

        if ($addressBalance->getBalance() < round($input['amount'] * $this->BIT_SATOSHI, 0)) {
            return response("Your balance is not enough to withdraw", 403);
        }

        $txInput = new \BlockCypher\Api\TXInput();
        $txInput->addAddress($coin_address->address);

        $txOutput = new \BlockCypher\Api\TXOutput();
        $txOutput->addAddress($input['address']);
        $txOutput->setValue(round($input['amount'] * $this->BIT_SATOSHI, 0));

        $tx = new \BlockCypher\Api\TX();
        $tx->addInput($txInput);
        $tx->addOutput($txOutput);
        $tx->setPreference('low');

        $txClient = new \BlockCypher\Client\TXClient($apiContext);

        try {
            $output = $txClient->create($tx);
        } catch (\Exception $e) {
            return response("You can't withdraw money. Please try again.", 403);
        }

        $privateKeys = array(
            $coin_address->privatekey
        );

        try {
            $txSkeleton = $txClient->sign($output, $privateKeys);
            $txSkeleton = $txClient->send($txSkeleton);
        } catch (\Exception $e) {
            return response("You can't withdraw money. Please try again.", 403);
        }

        // send microTransaction
//        $microTXClient = new \BlockCypher\Client\MicroTXClient($apiContext);
//
//        try {
//            /// Create, Sign and Send a MicroTX (server-side signing)
//            $microTX = $microTXClient->sendWithPrivateKey(
//                $coin_address->privatekey, // private key
//                $input['address'], // to address
//                round($input['amount'] * $this->BIT_SATOSHI, 0) // value (satoshis)
//            );
//        } catch (\Exception $e) {
//            return response($e->getMessage(), 403);
//        }

        return response()->json([
            'success' => true,
            'data' => $txSkeleton
        ]);
    }

    public function getSentTxList($coin, Request $request) {
        $user = Auth::user();

        if (!$user) {
            return response('Not Authorized', 403);
        }

        $coin = strtolower($coin);

        $apiContext = ApiContext::create(
            'main', $coin, 'v1',
            new SimpleTokenCredential($this->getBlockCypherToken()),
            array('log.LogEnabled' => true, 'log.FileName' => 'BlockCypher.log', 'log.LogLevel' => 'DEBUG')
        );

        $coin_address = $this->getCoinAddress($user, $coin);
        if (!$coin_address) {
            return response()->json('Failed', 403);
        }

        $addressClient = new \BlockCypher\Client\AddressClient($apiContext);

        try {
            $fullAddress = $addressClient->getFullAddress($coin_address->address);
        } catch (\Exception $ex) {
            return response()->json('Failed', 403);
        }

        $txs = $fullAddress->getTxs();

        $result = array();
        if (is_null($txs)) {
            return response()->json([
                'success' => true,
                'data' => $result
            ]);
        }

        foreach($txs as $tx) {
            $txInputs = $tx->getInputs();

            foreach ($txInputs as $txInput) {
                $addresses = $txInput->getAddresses();

                foreach ($addresses as $address) {
                    if ($coin_address->address == $address) {
                        array_push($result, array(
                            'txid' => $tx->getHash(),
                            'amount' => $txInput->getOutputValue() / $this->BIT_SATOSHI,
                            'confirmations' => $tx->getConfirmations(),
                            'address' => $tx->getOutputs()[0]->getAddresses()[0]
                        ));
                    }
                }
            }
        }

        return response()->json([
            'success' => true,
            'data' => $result
        ]);
    }

    public function getReceivedTxList($coin, Request $request) {
        $user = Auth::user();

        if (!$user) {
            return response('Not Authorized', 403);
        }

        $coin = strtolower($coin);

        $apiContext = ApiContext::create(
            'main', $coin, 'v1',
            new SimpleTokenCredential($this->getBlockCypherToken()),
            array('log.LogEnabled' => true, 'log.FileName' => 'BlockCypher.log', 'log.LogLevel' => 'DEBUG')
        );

        $coin_address = $this->getCoinAddress($user, $coin);
        if (!$coin_address) {
            return response()->json('Failed', 403);
        }

        $addressClient = new \BlockCypher\Client\AddressClient($apiContext);

        try {
            $fullAddress = $addressClient->getFullAddress($coin_address->address);
        } catch (\Exception $ex) {
            return response()->json('Failed', 403);
        }

        $txs = $fullAddress->getTxs();

        $result = array();
        if (is_null($txs)) {
            return response()->json([
                'success' => true,
                'data' => $result
            ]);
        }

        foreach($txs as $tx) {
            $txOutputs = $tx->getOutputs();

            foreach ($txOutputs as $txOutput) {
                $addresses = $txOutput->getAddresses();

                foreach ($addresses as $address) {
                    if ($coin_address->address == $address) {
                        array_push($result, array(
                            'txid' => $tx->getHash(),
                            'amount' => $txOutput->getValue() / $this->BIT_SATOSHI,
                            'confirmations' => $tx->getConfirmations()
                        ));
                    }
                }
            }
        }

        return response()->json([
            'success' => true,
            'data' => $result
        ]);
    }

    public function getTxList($coin, Request $request) {
        $user = Auth::user();

        if (!$user) {
            return response('Not Authorized', 403);
        }

        $coin = strtolower($coin);

        $apiContext = ApiContext::create(
            'main', $coin, 'v1',
            new SimpleTokenCredential($this->getBlockCypherToken()),
            array('log.LogEnabled' => true, 'log.FileName' => 'BlockCypher.log', 'log.LogLevel' => 'DEBUG')
        );

        $coin_address = $this->getCoinAddress($user, $coin);
        if (!$coin_address) {
            return response()->json('Failed', 403);
        }

        $addressClient = new \BlockCypher\Client\AddressClient($apiContext);

        try {
            $fullAddress = $addressClient->getFullAddress($coin_address->address);
        } catch (\Exception $ex) {
            return response()->json('Failed', 403);
        }

        $txs = $fullAddress->getTxs();

        $result = array();
        if (is_null($txs)) {
            return response()->json([
                'success' => true,
                'data' => $result
            ]);
        }

        foreach($txs as $tx) {
            $txOutputs = $tx->getOutputs();

            foreach ($txOutputs as $txOutput) {
                $addresses = $txOutput->getAddresses();

                foreach ($addresses as $address) {
                    if ($coin_address->address == $address) {
                        array_push($result, array(
                            'txid' => $tx->getHash(),
                            'amount' => $txOutput->getValue() / $this->BIT_SATOSHI,
                            'confirmations' => $tx->getConfirmations(),
                            'time' => $tx->getReceived(),
                        ));
                    }
                }
            }

            $txInputs = $tx->getInputs();

            foreach ($txInputs as $txInput) {
                $addresses = $txInput->getAddresses();

                foreach ($addresses as $address) {
                    if ($coin_address->address == $address) {
                        array_push($result, array(
                            'txid' => $tx->getHash(),
                            'amount' => (-1) * $txInput->getOutputValue() / $this->BIT_SATOSHI,
                            'confirmations' => $tx->getConfirmations(),
                            'time' => $tx->getReceived(),
                        ));
                    }
                }
            }
        }

        return response()->json([
            'success' => true,
            'data' => $result
        ]);
    }

    public function getCurrentRate(Request $request) {
        try {
            $rates = json_decode(file_get_contents('https://api.cryptowat.ch/markets/prices'), true);
//            $rates = file_get_contents('https://blockchain.info/ticker');
        } catch (\Exception $e) {
            return response($e->getMessage(), 403);
        }

        return response()->json([
            'success' => true,
            'btc_rate' => $rates['result']['kraken:btcusd'],
            'eth_rate' => $rates['result']['kraken:ethusd']
        ]);
    }

    public function getTransactionList($coin, Request $request) {
        $type = $request->input('type');
        $user_id = $request->input('user_id');

        $coin = strtolower($coin);

        $apiContext = ApiContext::create(
            'main', $coin, 'v1',
            new SimpleTokenCredential($this->getBlockCypherToken()),
            array('log.LogEnabled' => true, 'log.FileName' => 'BlockCypher.log', 'log.LogLevel' => 'DEBUG')
        );

        if (isset($user_id)) {
            $user = User::find($user_id);
            $coin_address = $this->getCoinAddress($user, $coin);
            if (!$coin_address) {
                return response()->json('Failed', 403);
            }

            $addressClient = new \BlockCypher\Client\AddressClient($apiContext);

            try {
                $fullAddress = $addressClient->getFullAddress($coin_address->address);
            } catch (\Exception $ex) {
                return response()->json('Failed', 403);
            }

            $txs = $fullAddress->getTxs();

            $result = array();
            if (is_null($txs)) {
                return response()->json([
                    'success' => true,
                    'data' => $result
                ]);
            }

            foreach($txs as $tx) {
                $txInput = $tx->getInputs()[0];
                $addresses = $txInput->getAddresses();

                $sent = false;
                foreach ($addresses as $address) {
                    if ($coin_address->address == $address) {
                        $sent = true;
                        break;
                    }
                }
                if ($type == 'sent' && $sent) {
                    array_push($result, array(
                        'time' => $tx->getReceived(),
                        'txid' => $tx->getHash(),
                        'amount' => $tx->getOutputs()[0]->getValue() / $this->BIT_SATOSHI,
                        'confirmations' => $tx->getConfirmations()
                    ));
                } else if ($type == 'received' && !$sent) {
                    array_push($result, array(
                        'time' => $tx->getReceived(),
                        'txid' => $tx->getHash(),
                        'amount' => $tx->getOutputs()[0]->getValue() / $this->BIT_SATOSHI,
                        'confirmations' => $tx->getConfirmations()
                    ));
                }
            }

            return response()->json([
                'success' => true,
                'data' => $result
            ]);
        }

        return response()->json([
            'success' => true,
            'data' => array()
        ]);
    }

    public function getUnconfirmedReceivedTxList($coin, Request $request) {
        $user = Auth::user();

        if (!$user) {
            return response('Not Authorized', 403);
        }

        $coin = strtolower($coin);

        $coin_address = $this->getCoinAddress($user, $coin);
        if (!$coin_address) {
            return response()->json('Failed', 403);
        }

        $apiContext = ApiContext::create(
            'main', $coin, 'v1',
            new SimpleTokenCredential($this->getBlockCypherToken()),
            array('log.LogEnabled' => true, 'log.FileName' => 'BlockCypher.log', 'log.LogLevel' => 'DEBUG')
        );

        $addressClient = new \BlockCypher\Client\AddressClient($apiContext);

        try {
            $fullAddress = $addressClient->getFullAddress($coin_address->address);
        } catch (\Exception $ex) {
            return response()->json('Failed', 403);
        }

        $txs = $fullAddress->getTxs();

        $result = array();
        if (is_null($txs)) {
            return response()->json([
                'success' => true,
                'data' => $result
            ]);
        }

        foreach($txs as $tx) {
            if ($tx->getConfirmations() > 0) continue;

            $txOutputs = $tx->getOutputs();

            foreach ($txOutputs as $txOutput) {
                $addresses = $txOutput->getAddresses();

                foreach ($addresses as $address) {
                    if ($coin_address->address == $address) {
                        array_push($result, array(
                            'txid' => $tx->getHash(),
                            'amount' => $txOutput->getValue() / $this->BIT_SATOSHI,
                            'confirmations' => $tx->getConfirmations()
                        ));
                    }
                }
            }
        }

        return response()->json([
            'success' => true,
            'data' => $result
        ]);
    }


    public function getUnconfirmedSentTxList($coin, Request $request) {
        $user = Auth::user();

        if (!$user) {
            return response('Not Authorized', 403);
        }

        $coin = strtolower($coin);

        $coin_address = $this->getCoinAddress($user, $coin);
        if (!$coin_address) {
            return response()->json('Failed', 403);
        }

        $apiContext = ApiContext::create(
            'main', $coin, 'v1',
            new SimpleTokenCredential($this->getBlockCypherToken()),
            array('log.LogEnabled' => true, 'log.FileName' => 'BlockCypher.log', 'log.LogLevel' => 'DEBUG')
        );

        $addressClient = new \BlockCypher\Client\AddressClient($apiContext);

        try {
            $fullAddress = $addressClient->getFullAddress($coin_address->address);
        } catch (\Exception $ex) {
            return response()->json('Failed', 403);
        }

        $txs = $fullAddress->getTxs();

        $result = array();
        if (is_null($txs)) {
            return response()->json([
                'success' => true,
                'data' => $result
            ]);
        }

        foreach($txs as $tx) {
            if ($tx->getConfirmations() > 0) continue;

            $txInputs = $tx->getInputs();

            foreach ($txInputs as $txInput) {
                $addresses = $txInput->getAddresses();

                foreach ($addresses as $address) {
                    if ($coin_address->address == $address) {
                        array_push($result, array(
                            'txid' => $tx->getHash(),
                            'amount' => $txInput->getOutputValue() / $this->BIT_SATOSHI,
                            'confirmations' => $tx->getConfirmations(),
                            'address' => $tx->getOutputs()[0]->getAddresses()[0]
                        ));
                    }
                }
            }
        }

        return response()->json([
            'success' => true,
            'data' => $result
        ]);
    }
}
